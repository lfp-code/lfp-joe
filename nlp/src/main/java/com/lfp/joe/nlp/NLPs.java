package com.lfp.joe.nlp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.time.Duration;
import java.util.Objects;
import java.util.Properties;

import org.threadly.concurrent.future.ListenableFuture;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.lock.FileLocks;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.utils.Utils;

import edu.stanford.nlp.pipeline.StanfordCoreNLP;

public class NLPs {
	private static final String DOWNLOAD_URL_TEMPLATE = "https://nlp.stanford.edu/software/stanford-corenlp-%s-models.jar";
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 0;
	private static final  ResourcePool<StanfordCoreNLP> RESOURCE_POOL = ResourcePool.create(() -> createNLP());
	private static final MemoizedSupplier<Void> MODELS_INIT = Utils.Functions.memoize(() -> {
		setupModels();
	}, null);

	public static <X, E extends Exception> ListenableFuture<X> access(
			ThrowingFunction<StanfordCoreNLP, X, E> accessor) {
		Objects.requireNonNull(accessor);
		return RESOURCE_POOL.access(nlp -> {
			return accessor.apply(nlp);
		});
	}

	private static StanfordCoreNLP createNLP() {
		MODELS_INIT.get();
		// set up pipeline properties
		Properties props = new Properties();
		// set the list of annotators to run
		props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner,gender");
		// set a property for an annotator, in this case the coref annotator is being
		// set to use the neural algorithm
		props.setProperty("coref.algorithm", "neural");
		// build pipeline
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
		return pipeline;
	}

	private static void setupModels() throws IOException {
		var definedPackage = StanfordCoreNLP.class.getPackage();
		var uri = URI.create(String.format(DOWNLOAD_URL_TEMPLATE, definedPackage.getImplementationVersion()));
		File temp = MachineConfig.getTempDirectory(true);
		var directory = new File(new File(new File(temp, THIS_CLASS.getName()), Objects.toString(VERSION)),
				Utils.Crypto.hashMD5(uri).encodeHex());
		directory.mkdirs();
		var modelsFile = new File(directory, "models.jar");
		var lockFile = new File(directory, "lock.lfp");
		var completeFile = new File(directory, "complete.lfp");
		FileLocks.write(lockFile, (nil1, nil2) -> {
			if (completeFile.exists()) {
				long elapsed = System.currentTimeMillis() - completeFile.lastModified();
				if (Duration.ofDays(14).toMillis() < elapsed)
					completeFile.delete();
			}
			if (completeFile.exists())
				return;
			logger.info("downloading models (~1 min):{}", uri);
			modelsFile.delete();
			var response = HttpClients.getDefault().get(uri.toString()).ensureSuccess().asStream();
			try (var is = response.getBody(); var fos = new FileOutputStream(modelsFile)) {
				Utils.Bits.copy(is, fos);
			}
			Files.write(completeFile.toPath(), Boolean.TRUE.toString().getBytes());
		});
		JavaCode.Reflections.addJarToClassPath(modelsFile);
	}

}
