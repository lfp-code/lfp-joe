package com.lfp.joe.nlp;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

import org.threadly.concurrent.future.ListenableFuture;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Asserts;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.threads.executor.SubmitterSchedulerLFP;

public abstract class ResourcePool<X> {

	public static <X> ResourcePool<X> create(Supplier<X> loader) {
		return create(MachineConfig.logicalCoreCount(), loader);
	}

	public static <X> ResourcePool<X> create(int maxConcurrency, Supplier<X> loader) {
		Objects.requireNonNull(loader);
		return new ResourcePool<X>(maxConcurrency) {

			@Override
			protected X createSlot() {
				return loader.get();
			}
		};
	}

	private final List<X> slots = new ArrayList<>();
	private final SubmitterSchedulerLFP pool;
	private int remainingSLots;

	public ResourcePool(int maxConcurrency) {
		this.pool = Threads.Pools.centralPool().limit(maxConcurrency);
		this.remainingSLots = maxConcurrency;
	}

	public <Y, E extends Exception> ListenableFuture<Y> access(ThrowingFunction<X, Y, E> accessor) {
		return this.pool.submit(() -> {
			return accessInternal(accessor);
		});
	}

	private <Y, E extends Exception> Y accessInternal(ThrowingFunction<X, Y, E> accessor) throws E {
		var slot = claim();
		try {
			return accessor.apply(slot);
		} finally {
			release(slot);
		}
	}

	private X claim() {
		synchronized (slots) {
			if (remainingSLots > 0) {
				remainingSLots--;
				return createSlot();
			}
			Asserts.isTrue(slots.size() > 0, "could not claim slot");
			return slots.remove(0);
		}
	}

	private void release(X slot) {
		synchronized (slots) {
			slots.add(slot);
		}
	}

	protected abstract X createSlot();
}
