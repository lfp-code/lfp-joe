package test;

import java.time.Duration;
import java.util.Date;

import com.github.throwable.beanref.BeanRef;
import com.lfp.joe.validation.Validators;

public class ValidationTest {

	public static void main(String[] args) {
		var bp = BeanRef.$(Date::getTime);
		var validator = Validators.<Date>builder()
				.constraint(bp::get,
						bp.getName(),
						v -> v.greaterThan(System.currentTimeMillis() - Duration.ofDays(1).toMillis()))
				.constraint(bp::get,
						bp.getName(),
						v -> v.lessThan(0l))
				.build();
		validator.validate(new Date(1)).throwIfInvalid(Validators.toException());
		System.out.println("done");
	}
}
