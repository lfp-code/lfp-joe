package com.lfp.joe.validation;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.lfp.joe.core.classpath.ImmutableMemberCache.ConstructorRequest;
import com.lfp.joe.core.classpath.MemberCache;

import am.ik.yavi.builder.ValidatorBuilder;
import am.ik.yavi.core.ConstraintViolations;

public class Validators {

	protected Validators() {}

	public static <U> ValidatorBuilder<U> builder() {
		return ValidatorBuilder.<U>of();
	}

	public static Function<ConstraintViolations, IllegalArgumentException> toException() {
		return toException(IllegalArgumentException.class);
	}

	@SuppressWarnings("unchecked")
	public static <E extends RuntimeException> Function<ConstraintViolations, E> toException(
			Class<E> exceptionClassType) {
		Objects.requireNonNull(exceptionClassType);
		Function<String, E> exceptionFunction;
		if (IllegalArgumentException.class.equals(exceptionClassType))
			exceptionFunction = v -> (E) new IllegalArgumentException(v);
		else {
			var instantiator = MemberCache
					.getConstructorInstantiator(ConstructorRequest.of(exceptionClassType, String.class));
			exceptionFunction = v -> instantiator.newInstance(v);
		}
		return violations -> {
			var message = Optional.ofNullable(violations)
					.map(v -> v.details())
					.stream()
					.flatMap(Collection::stream)
					.filter(Objects::nonNull)
					.map(detail -> {
						var detailMessage = detail.getDefaultMessage();
						var inputSummary = Stream.ofNullable(detail.getArgs())
								.flatMap(Stream::of)
								.reduce((f, s) -> s)
								.map(v -> String.format(" [input=%s]", v))
								.orElse(null);
						if (inputSummary != null)
							detailMessage = String.format("%s%s", detailMessage, inputSummary);
						return detailMessage;
					})
					.collect(Collectors.joining(", "));
			return exceptionFunction.apply(message);
		};
	}

}