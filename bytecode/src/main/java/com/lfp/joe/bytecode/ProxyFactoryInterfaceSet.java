package com.lfp.joe.bytecode;

import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

import com.lfp.joe.core.classpath.InterfaceSet;

import javassist.util.proxy.Proxy;
import javassist.util.proxy.ProxyFactory;
import javassist.util.proxy.ProxyObject;

public class ProxyFactoryInterfaceSet extends InterfaceSet {

	public ProxyFactoryInterfaceSet(ProxyFactory proxyFactory) {
		super(createFilter(proxyFactory));
		Stream.ofNullable(proxyFactory.getInterfaces())
				.flatMap(Stream::of)
				.filter(Objects::nonNull)
				.distinct()
				.forEach(this::add);
	}

	private static Predicate<Class<?>> createFilter(ProxyFactory proxyFactory) {
		Objects.requireNonNull(proxyFactory);
		return ct -> {
			if (Proxy.class.isAssignableFrom(ct))
				return false;
			if (ProxyObject.class.isAssignableFrom(ct))
				return false;
			if (java.lang.reflect.Proxy.class.isAssignableFrom(ct))
				return false;
			var superclass = proxyFactory.getSuperclass();
			if (superclass != null && superclass.isAssignableFrom(ct))
				return false;
			return true;
		};
	}

}
