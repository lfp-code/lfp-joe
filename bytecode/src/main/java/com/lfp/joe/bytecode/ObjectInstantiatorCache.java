package com.lfp.joe.bytecode;

import java.lang.ref.SoftReference;
import java.util.WeakHashMap;

import org.objenesis.instantiator.ObjectInstantiator;

import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.core.function.ComputeStore;

public interface ObjectInstantiatorCache {

	<T> ObjectInstantiator<T> get(Class<T> classType);

	public static ObjectInstantiatorCache instance() {
		return Const.INSTANCE;
	}

	static enum Const {
		;

		private static final ObjectInstantiatorCache INSTANCE = Instances.streamAutowired(ObjectInstantiatorCache.class)
				.findFirst()
				.orElseGet(() -> {
					return new ObjectInstantiatorCache() {

						private final ComputeStore<Class<?>, ObjectInstantiator<?>> store = ComputeStore
								.from(new WeakHashMap<>(), SoftReference::new);

						@SuppressWarnings("unchecked")
						@Override
						public <T> ObjectInstantiator<T> get(Class<T> classType) {
							return (ObjectInstantiator<T>) store.computeIfAbsent(classType,
									() -> Objenesi.getDefault().getInstantiatorOf(classType));
						}
					};
				});
	}

}
