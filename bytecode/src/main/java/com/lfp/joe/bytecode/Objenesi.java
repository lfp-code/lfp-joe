package com.lfp.joe.bytecode;

import org.objenesis.Objenesis;
import org.objenesis.ObjenesisStd;

import com.lfp.joe.core.function.Once.SupplierOnce;

public class Objenesi {

	protected Objenesi() {}

	private static final SupplierOnce<Objenesis> INSTANCE_ONCE = SupplierOnce.of(ObjenesisStd::new);

	public static Objenesis getDefault() {
		return INSTANCE_ONCE.get();
	}

}
