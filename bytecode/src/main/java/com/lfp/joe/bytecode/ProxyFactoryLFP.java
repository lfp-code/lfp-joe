package com.lfp.joe.bytecode;

import java.lang.ref.SoftReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.WeakHashMap;
import java.util.stream.Stream;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.ConstructorRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.classpath.Primitives;
import com.lfp.joe.core.function.ComputeStore;
import com.lfp.joe.core.function.EmptyArrays;
import com.lfp.joe.core.function.Throws;

import javassist.util.proxy.MethodHandler;
import javassist.util.proxy.Proxy;
import javassist.util.proxy.ProxyFactory;

@SuppressWarnings("rawtypes")
public class ProxyFactoryLFP extends ProxyFactory {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final ComputeStore<String, Optional<ClassLoader>> CLASS_NAME_TO_CLASS_LOADER_CACHE = ComputeStore
			.from(new WeakHashMap<>(), SoftReference::new);

	{
		setUseCache(true);
	}

	public Object create(Object... args) throws NoSuchMethodException, IllegalArgumentException, InstantiationException,
			IllegalAccessException, InvocationTargetException {
		return create(null, args);
	}

	@Override
	public Object create(Class[] paramTypes, Object[] args) throws NoSuchMethodException, IllegalArgumentException,
			InstantiationException, IllegalAccessException, InvocationTargetException {
		var instance = createInstance(paramTypes, args);
		((Proxy) instance).setHandler(new MethodHandler() {

			@Override
			public Object invoke(Object self, Method thisMethod, Method proceed, Object[] args) throws Throwable {
				if (proceed != null)
					return proceed.invoke(self, args);
				throw new UnsupportedOperationException("method handler not implemented:" + thisMethod);
			}
		});
		return instance;
	}

	protected Object createInstance(Class[] paramTypes, Object[] args) throws NoSuchMethodException,
			IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
		if (paramTypes == null) {
			if (args == null || args.length == 0)
				paramTypes = EmptyArrays.of(Class.class);
			else
				paramTypes = Stream.of(args).map(v -> v == null ? null : v.getClass()).toArray(Class<?>[]::new);
		}
		if (args == null) {
			if (paramTypes.length == 0)
				args = EmptyArrays.of(Object.class);
			else
				args = Stream.of(paramTypes).map(v -> v.isPrimitive() ? Primitives.getDefaultValue(v) : null).toArray();
		}
		var superclass = getSuperclass();
		if (superclass != null)
			CoreReflections.moduleAddOpens(superclass, THIS_CLASS);
		var classType = createClass();
		var ctor = MemberCache.tryGetConstructor(ConstructorRequest.of(classType, paramTypes).withAllTypes(false), true)
				.orElse(null);
		Throwable failure = null;
		if (ctor != null) {
			try {
				return ctor.newInstance(args);
			} catch (Throwable t) {
				failure = Throws.combine(failure, t);
			}
		}
		if (args.length == 0)
			try {
				var instantiator = ObjectInstantiatorCache.instance().get(classType);
				return instantiator.newInstance();
			} catch (Throwable t) {
				failure = Throws.combine(failure, t);
			}
		if (failure == null)
			failure = new NoSuchMethodError(
					String.format("superclass:%s interfaces:%s paramTypes:%s args:%s", getSuperclass(),
							Arrays.toString(getInterfaces()), Arrays.toString(paramTypes), Arrays.toString(args)));
		if (failure instanceof NoSuchMethodException)
			throw (NoSuchMethodException) failure;
		if (failure instanceof IllegalArgumentException)
			throw (IllegalArgumentException) failure;
		if (failure instanceof InstantiationException)
			throw (InstantiationException) failure;
		if (failure instanceof IllegalAccessException)
			throw (IllegalAccessException) failure;
		if (failure instanceof InvocationTargetException)
			throw (InvocationTargetException) failure;
		throw new RuntimeException(failure);

	}

	@Override
	protected ClassLoader getClassLoader() {
		var ctStream = Stream.ofNullable(getInterfaces()).flatMap(Stream::of);
		ctStream = Stream.concat(Stream.ofNullable(getSuperclass()), ctStream);
		ctStream = ctStream.filter(Objects::nonNull);
		var clStream = ctStream.map(ct -> {
			var className = ct.getName();
			var classLoader = CLASS_NAME_TO_CLASS_LOADER_CACHE.computeIfAbsent(ct.getName(), () -> {
				return CoreReflections.streamDefaultClassLoaders()
						.filter(v -> CoreReflections.findLoadedClass(v, className).isPresent())
						.findFirst();
			}).orElse(null);
			return classLoader;
		});
		clStream = clStream.filter(Objects::nonNull);
		return clStream.findFirst().orElseGet(() -> super.getClassLoader());
	}

	@Override
	public void setSuperclass(Class<?> clazz) {
		if (clazz != null)
			clazz = CoreReflections.getNamedClassType(clazz);
		if (clazz.isInterface())
			throw new IllegalArgumentException("superclass must not be interface:" + clazz);
		super.setSuperclass(clazz);
	}

	@Override
	public void setInterfaces(Class<?>[] interfaces) {
		setInterfaces(interfaces == null ? null : interfaces);
	}

	public void setInterfaces(Iterable<Class<?>> interfaces) {
		super.setInterfaces(EmptyArrays.of(Class.class));
		addInterfaces(interfaces);
	}

	public boolean addInterfaces(Class<?>... interfaces) {
		return addInterfaces(interfaces == null ? null : Arrays.asList(interfaces));
	}

	public boolean addInterfaces(Iterable<Class<?>> interfaces) {
		boolean mod = false;
		ProxyFactoryInterfaceSet ifaceSet = null;
		if (interfaces != null)
			for (var iface : interfaces) {
				if (iface == null)
					continue;
				if (ifaceSet == null)
					ifaceSet = new ProxyFactoryInterfaceSet(this);
				if (ifaceSet.add(iface))
					mod = true;
			}
		if (!mod)
			return false;
		super.setInterfaces(ifaceSet.toArray());
		return true;
	}

}
