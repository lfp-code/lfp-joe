package com.lfp.joe.bytecode;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.immutables.value.Value;

import com.lfp.joe.bytecode.ImmutableMethodHandlerLFP.MethodHandlerRequest;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.InvocationHandlerLFP.InvocationHandlerRequestDef;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.EmptyArrays;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

import javassist.util.proxy.MethodHandler;
import javassist.util.proxy.ProxyObject;

@ValueLFP.Style
@Value.Enclosing
public interface MethodHandlerLFP extends MethodHandler {

	@Override
	default Object invoke(Object self, Method thisMethod, Method proceed, Object[] args) throws Throwable {
		if (args == null)
			return invoke(self, thisMethod, proceed, EmptyArrays.of(Object.class));
		var request = MethodHandlerRequest.builder()
				.proxy(self)
				.method(thisMethod)
				.proceedMethod(Optional.ofNullable(proceed))
				.args(args)
				.build();
		return invoke(request);
	}

	Object invoke(MethodHandlerRequest request) throws Throwable;

	public static Optional<MethodHandlerLFP> tryGet(Object object) {
		return CoreReflections.tryCast(object, ProxyObject.class)
				.map(ProxyObject::getHandler)
				.flatMap(v -> CoreReflections.tryCast(v, MethodHandlerLFP.class));
	}

	@Value.Immutable
	static abstract class AbstractMethodHandlerRequest implements InvocationHandlerRequestDef {

		public abstract Optional<Method> proceedMethod();

		@Override
		@Value.Lazy
		public Map<Object, Object> attributes() {
			return new ConcurrentHashMap<>();
		}

		public Object proceedInvoke() throws Throwable {
			return proceedInvokeOrElseGet(null);
		}

		public Object proceedInvokeOrElseGet(ThrowingSupplier<?, ?> loader) throws Throwable {
			if (proceedMethod().isPresent())
				try {
					return proceedMethod().get().invoke(proxy(), args());
				} catch (InvocationTargetException e) {
					throw Optional.ofNullable(e.getCause()).orElse(e);
				}
			if (loader != null)
				return loader.get();
			throw new UnsupportedOperationException(summary());
		}

	}

}
