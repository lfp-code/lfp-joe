package test;

import java.io.Serializable;
import java.util.Date;

import com.lfp.joe.bytecode.MethodHandlerLFP;
import com.lfp.joe.bytecode.ProxyGenerator;

public class DirectDelegateTest {

	public static void main(String[] args) {
		var delegate = new Date();
		var mh = MethodHandlerLFP.createDelegating(delegate);
		Date date2 = ProxyGenerator.builder().superclass(Date.class).addInterfaceSources(Serializable.class).build()
				.create(mh);
		System.out.println(date2);
		Date date3 = ProxyGenerator.builder().superclass(Date.class).addInterfaceSources(Serializable.class).build()
				.create(mh, 69420l);
		System.out.println(date3);
	}

}
