package com.lfp.joe.certigo.impl;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.zeroturnaround.exec.ProcessResult;

import com.lfp.joe.certigo.service.CertificateInfo;
import com.lfp.joe.certigo.service.CertigoService;
import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.process.OutputType;
import com.lfp.joe.process.Procs;
import com.lfp.joe.process.Which;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.Requires;

public class CertigoServiceImpl implements CertigoService {

	public static CertigoServiceImpl get() {
		return Instances.get(CertigoServiceImpl.class);
	}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String EXEC_NAME = "certigo";
	private static final URI PACKAGE_URI = URI.create("https://github.com/square/certigo");
	private final File executable;

	protected CertigoServiceImpl() throws IOException {
		Requires.notBlank(EXEC_NAME);
		Objects.requireNonNull(PACKAGE_URI);
		var executable = Which.get(EXEC_NAME).orElse(null);
		if (executable == null && MachineConfig.isDeveloper()) {
			var lockDir = Utils.Files.tempFile(THIS_CLASS, Utils.Crypto.hashMD5(PACKAGE_URI).encodeHex());
			lockDir.mkdirs();
			Utils.Files.configureDirectoryLocked(lockDir, (nil1, nil2) -> {
				return Which.get(EXEC_NAME).isPresent();
			}, nil -> {
				var packageName = Utils.Strings.substringAfter(PACKAGE_URI.toString(), "://");
				Procs.execute(String.format("go install %s", packageName));
			});
			executable = Which.get(EXEC_NAME).orElse(null);
		}
		this.executable = Requires.notNull(executable, "package not found. name:%s uri:%s", EXEC_NAME, PACKAGE_URI);
	}

	@Override
	public CertificateInfo connect(InetSocketAddress address) throws IOException {
		return Serials.Gsons.get().fromJson(connectRaw(address), CertificateInfo.class);
	}

	public String connectRaw(InetSocketAddress address) throws IOException {
		Objects.requireNonNull(address, "address is required");
		var output = execute("connect", "--json", String.format("%s:%s", address.getHostString(), address.getPort()));
		return output;
	}

	@Override
	public CertificateInfo dump(File pem) throws IOException {
		return Serials.Gsons.get().fromJson(dumpRaw(pem), CertificateInfo.class);
	}

	public String dumpRaw(File pem) throws IOException {
		Objects.requireNonNull(pem, "pem file is required");
		var output = execute("dump", "--json", pem.getAbsolutePath());
		return output;
	}

	protected String execute(String... arguments) throws IOException {
		try {
			List<String> outLines = new ArrayList<>();
			List<String> errLines = new ArrayList<>();
			String command = Utils.Lots.stream(arguments).prepend(this.executable.getAbsolutePath()).joining(" ");
			ProcessResult pr = Procs.execute(command, pe -> {
				pe.exitValueAny();
				pe.disableOutputLog();
				pe.onOutput(OutputType.out, outLines::add);
				pe.onOutput(OutputType.err, errLines::add);
			});
			var output = Utils.Strings.joinNewLine(outLines);
			if (Utils.Strings.isBlank(output))
				output = Utils.Strings.joinNewLine(errLines);
			Requires.notBlank(output, "command output blank. exitValue:%s", pr.getExitValue());
			Requires.isTrue(pr.getExitValue() == 0, "command failed. exitValue:%s output:\n%s", pr.getExitValue(),
					output);
			return output;
		} catch (Throwable t) {
			throw Utils.Exceptions.as(t, IOException.class);
		}
	}

	public static void main(String[] args) throws IOException {
		var info = CertigoServiceImpl.get().connect(InetSocketAddress.createUnresolved("portainer.lasso.tm", 443));
		System.out.println(Serials.Gsons.getPretty().toJson(info));
	}

}
