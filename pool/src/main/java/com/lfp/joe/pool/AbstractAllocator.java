package com.lfp.joe.pool;

import java.util.function.Consumer;
import java.util.function.Supplier;

import org.bbottema.genericobjectpool.Allocator;
import org.immutables.value.Value;

@Value.Immutable
public abstract class AbstractAllocator<T> extends Allocator<T> {

	abstract Supplier<T> allocator();

	@Value.Default
	Consumer<T> reuseAllocator() {
		return nil -> {};
	}

	@Value.Default
	Consumer<T> reuseDeallocator() {
		return nil -> {};
	}

	@Value.Default
	Consumer<T> deallocator() {
		return nil -> {};
	}

	@Override
	public T allocate() {
		return allocator().get();
	}

	@Override
	public void allocateForReuse(T object) {
		reuseAllocator().accept(object);
	}

	@Override
	public void deallocateForReuse(T object) {
		reuseDeallocator().accept(object);
	}

	@Override
	public void deallocate(T object) {
		deallocator().accept(object);
	}
}
