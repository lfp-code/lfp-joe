package com.lfp.joe.pool;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

import org.bbottema.clusteredobjectpool.core.ClusterConfig;
import org.bbottema.clusteredobjectpool.core.ResourceClusters;
import org.bbottema.clusteredobjectpool.core.api.ResourceKey;
import org.bbottema.clusteredobjectpool.core.api.ResourceKey.ResourceClusterAndPoolKey;
import org.bbottema.genericobjectpool.ExpirationPolicy;

public class ResourceClustersLFP<ClusterKey, PoolKey, T> extends ResourceClusters<ClusterKey, PoolKey, T> {

	public ResourceClustersLFP(ClusterConfig<PoolKey, T> clusterConfig) {
		super(clusterConfig);
	}

	public @Nullable Poolo<T, T> claimFromCluster(ClusterKey clusterKey) throws InterruptedException {
		var po = super.claimResourceFromCluster(clusterKey);
		return Poolo.from(po);
	}

	public @Nullable Poolo<T, T> claimFromPool(ClusterKey clusterKey, PoolKey poolKey) throws InterruptedException {
		return claimFromPool(new ResourceClusterAndPoolKey<>(clusterKey, poolKey));
	}

	public @Nullable Poolo<T, T> claimFromPool(ResourceKey<ClusterKey, PoolKey> key) throws InterruptedException {
		var po = super.claimResourceFromPool(key);
		return Poolo.from(po);
	}

	public void registerResourcePool(ClusterKey clusterKey, PoolKey poolKey) {
		super.registerResourcePool(new ResourceClusterAndPoolKey<>(clusterKey, poolKey));
	}

	public void registerResourcePool(ClusterKey clusterKey, PoolKey poolKey,
			@NotNull final ExpirationPolicy<T> expirationPolicy, final int corePoolSize, final int maxPoolSize)
			throws IllegalArgumentException {
		super.registerResourcePool(new ResourceClusterAndPoolKey<>(clusterKey, poolKey), expirationPolicy, corePoolSize,
				maxPoolSize);
	}
}
