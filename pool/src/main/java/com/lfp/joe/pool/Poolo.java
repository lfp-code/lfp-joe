package com.lfp.joe.pool;

import java.util.Date;
import java.util.Objects;
import java.util.function.Function;

import javax.annotation.Nullable;

import org.bbottema.genericobjectpool.GenericObjectPool;
import org.bbottema.genericobjectpool.PoolableObject;
import org.immutables.value.Value;

import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.MemberCache;

@Value.Immutable
@Value.Style(stagedBuilder = true)
public interface Poolo<T, X> extends AutoCloseable {

	public static <U> Poolo<U, U> from(PoolableObject<U> poolableObject) {
		if (poolableObject == null)
			return null;
		return new Poolo<U, U>() {

			@Override
			public PoolableObject<U> getPoolableObject() {
				return poolableObject;
			}

			@Override
			public U getAllocatedObject() {
				return poolableObject.getAllocatedObject();
			}
		};
	}

	PoolableObject<X> getPoolableObject();

	/**
	 * @return
	 * @see org.bbottema.genericobjectpool.PoolableObject#getAllocatedObject()
	 */
	@Nullable
	T getAllocatedObject();

	default GenericObjectPool<T> pool() {
		return MemberCache.getFieldValue(FieldRequest.of(PoolableObject.class, GenericObjectPool.class, "pool"),
				getPoolableObject());
	}

	@SuppressWarnings("unchecked")
	default <U> Poolo<U, X> map(Function<? super T, U> mapper) {
		Objects.requireNonNull(mapper);
		var allocatedObject = getAllocatedObject();
		var allocatedObjectUpdated = mapper.apply(allocatedObject);
		if (allocatedObject == allocatedObjectUpdated)
			return (Poolo<U, X>) this;
		return ImmutablePoolo.<U, X>builder()
				.poolableObject(getPoolableObject())
				.allocatedObject(allocatedObjectUpdated)
				.build();
	}

	@Override
	default void close() {
		this.getPoolableObject().release();
	}

	// **********************delegates

	/**
	 * @return
	 * @see org.bbottema.genericobjectpool.PoolableObject#ageMs()
	 */
	default long ageMs() {
		return getPoolableObject().ageMs();
	}

	/**
	 * @return
	 * @see org.bbottema.genericobjectpool.PoolableObject#allocationAgeMs()
	 */
	default long allocationAgeMs() {
		return getPoolableObject().ageMs();
	}

	/**
	 * @return
	 * @see org.bbottema.genericobjectpool.PoolableObject#getCreatedOn()
	 */
	default Date getCreatedOn() {
		return getPoolableObject().getCreatedOn();
	}

	/**
	 * 
	 * @see org.bbottema.genericobjectpool.PoolableObject#release()
	 */
	default void release() {
		getPoolableObject().release();
	}

	/**
	 * 
	 * @see org.bbottema.genericobjectpool.PoolableObject#invalidate()
	 */
	default void invalidate() {
		getPoolableObject().invalidate();
	}

}