package com.lfp.joe.pool;

import java.time.Duration;
import java.util.Objects;

import org.bbottema.genericobjectpool.util.ForeverTimeout;
import org.bbottema.genericobjectpool.util.Timeout;

import com.lfp.joe.core.function.Durations;

public class Timeouts {

	protected Timeouts() {}

	private static final Duration _WAIT_FOREVER_DURATION = toDuration(forever());

	public static Timeout forever() {
		return ForeverTimeout.WAIT_FOREVER;
	}

	public static Duration foreverDuration() {
		return _WAIT_FOREVER_DURATION;
	}

	public static Timeout from(Duration duration) {
		Objects.requireNonNull(duration);
		if (Durations.isMax(duration) || foreverDuration().compareTo(duration) <= 0)
			return forever();
		var entry = Durations.toTimeUnitEntry(duration);
		return new Timeout(entry.getKey(), entry.getValue());
	}

	public static Duration toDuration(Timeout timeout) {
		Objects.requireNonNull(timeout);
		if (forever().equals(timeout))
			return Durations.max();
		return Durations.from(timeout.getDuration(), timeout.getTimeUnit());
	}

}
