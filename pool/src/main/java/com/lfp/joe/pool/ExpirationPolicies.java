package com.lfp.joe.pool;

import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;

import org.bbottema.genericobjectpool.ExpirationPolicy;
import org.bbottema.genericobjectpool.expirypolicies.CombinedExpirationPolicies;
import org.bbottema.genericobjectpool.expirypolicies.SpreadedTimeoutSinceCreationExpirationPolicy;
import org.bbottema.genericobjectpool.expirypolicies.SpreadedTimeoutSinceLastAllocationExpirationPolicy;
import org.bbottema.genericobjectpool.expirypolicies.TimeoutSinceCreationExpirationPolicy;
import org.bbottema.genericobjectpool.expirypolicies.TimeoutSinceLastAllocationExpirationPolicy;

import com.lfp.joe.core.function.Durations;
import com.lfp.joe.core.function.NumeralFunctions.TriFunction;
import com.lfp.joe.stream.Streams;

@SuppressWarnings("unchecked")
public class ExpirationPolicies {

	@SuppressWarnings("rawtypes")
	private static final ExpirationPolicy NEVER = v -> false;

	@SafeVarargs
	public static <T> ExpirationPolicy<T> or(ExpirationPolicy<? extends T>... expirationPolicies) {
		return or(Streams.of(expirationPolicies));
	}

	public static <T> ExpirationPolicy<T> or(Iterable<? extends ExpirationPolicy<? extends T>> expirationPolicies) {
		return new CombinedExpirationPolicies<T>(
				Streams.of(expirationPolicies).nonNull().map(v -> (ExpirationPolicy<T>) v).toImmutableSet());
	}

	public static <T> ExpirationPolicy<T> never() {
		return NEVER;
	}

	public static <T> ExpirationPolicy<T> timeoutSinceLastAllocation(Duration duration) {
		return timeoutSinceLastAllocation(duration, null);
	}

	public static <T> ExpirationPolicy<T> timeoutSinceLastAllocation(Duration duration1, Duration duration2) {
		return timeout(duration1, duration2, TimeoutSinceLastAllocationExpirationPolicy::new,
				SpreadedTimeoutSinceLastAllocationExpirationPolicy::new);
	}

	public static <T> ExpirationPolicy<T> timeoutSinceCreation(Duration duration) {
		return timeoutSinceLastAllocation(duration, null);
	}

	public static <T> ExpirationPolicy<T> timeoutSinceCreation(Duration duration1, Duration duration2) {
		return timeout(duration1, duration2, TimeoutSinceCreationExpirationPolicy::new,
				SpreadedTimeoutSinceCreationExpirationPolicy::new);
	}

	public static <T> ExpirationPolicy<T> timeout(Duration duration1, Duration duration2,
			BiFunction<Long, TimeUnit, ExpirationPolicy<T>> durationFunction,
			TriFunction<Long, Long, TimeUnit, ExpirationPolicy<T>> durationSpreadFunction) {
		if (duration1 == null && duration2 == null)
			return NEVER;
		if (duration1 == null || duration2 == null || duration1.equals(duration2)) {
			var duration = Optional.ofNullable(duration1).orElse(duration2);
			var entry = Durations.toTimeUnitEntry(duration);
			return durationFunction.apply(entry.getKey(), entry.getValue());
		}
		var timeUnit = Durations.getTimeUnit(duration1, duration2);
		return durationSpreadFunction.apply(timeUnit.convert(duration1), timeUnit.convert(duration2), timeUnit);
	}

}
