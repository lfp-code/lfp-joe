package com.lfp.joe.pool;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

import org.bbottema.clusteredobjectpool.core.ClusterConfig;
import org.bbottema.genericobjectpool.ExpirationPolicy;

import com.lfp.joe.core.function.Nada;

public class ResourceCluster<PoolKey, T> extends ResourceClustersLFP<Nada, PoolKey, T> {

	public ResourceCluster(ClusterConfig<PoolKey, T> clusterConfig) {
		super(clusterConfig);
	}

	@Nullable
	public Poolo<T, T> claimFromCluster() throws InterruptedException {
		return claimFromCluster(Nada.get());
	}

	@Nullable
	public Poolo<T, T> claimFromPool(PoolKey poolKey) throws InterruptedException {
		return claimFromPool(Nada.get(), poolKey);
	}

	public void registerResourcePool(PoolKey poolKey) {
		registerResourcePool(Nada.get(), poolKey);
	}

	public void registerResourcePool(PoolKey poolKey, @NotNull final ExpirationPolicy<T> expirationPolicy,
			final int corePoolSize, final int maxPoolSize) throws IllegalArgumentException {
		registerResourcePool(Nada.get(), poolKey, expirationPolicy, corePoolSize, maxPoolSize);
	}

}
