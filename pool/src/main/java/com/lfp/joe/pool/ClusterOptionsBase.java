package com.lfp.joe.pool;

import java.time.Duration;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

import org.bbottema.clusteredobjectpool.core.ClusterConfig;
import org.bbottema.clusteredobjectpool.core.api.AllocatorFactory;
import org.bbottema.clusteredobjectpool.core.api.LoadBalancingStrategy;
import org.bbottema.clusteredobjectpool.cyclingstrategies.RoundRobinLoadBalancing;
import org.bbottema.genericobjectpool.Allocator;
import org.bbottema.genericobjectpool.ExpirationPolicy;
import org.immutables.value.Value;

import com.lfp.joe.core.config.ValueLFP;

@Value.Enclosing
@ValueLFP.Style
public interface ClusterOptionsBase<PoolKey, T>
		extends Function<AllocatorFactory<PoolKey, T>, ClusterConfig<PoolKey, T>> {

	@Value.Default
	default ExpirationPolicy<T> defaultExpirationPolicy() {
		return ExpirationPolicies.never();
	}

	@Value.Default
	default int defaultMaxPoolSize() {
		return Integer.MAX_VALUE;
	}

	@Value.Default
	default int defaultCorePoolSize() {
		return 0;
	}

	@Value.Default
	default LoadBalancingStrategy<T, ?> loadBalancingStrategy() {
		return new RoundRobinLoadBalancing<T>();
	}

	@Value.Default
	default Duration claimTimeout() {
		return Timeouts.foreverDuration();
	}

	List<ExpirationPolicy<T>> expirationPolicies();

	default ClusterConfig<PoolKey, T> apply(Allocator<T> allocator) {
		Objects.requireNonNull(allocator);
		return apply(nil -> allocator);
	}

	@Override
	default ClusterConfig<PoolKey, T> apply(AllocatorFactory<PoolKey, T> allocatorFactory) {
		var clusterConfigBuilder = ClusterConfig.<PoolKey, T>builder();
		clusterConfigBuilder.defaultExpirationPolicy(defaultExpirationPolicy());
		clusterConfigBuilder.defaultMaxPoolSize(defaultMaxPoolSize());
		clusterConfigBuilder.defaultCorePoolSize(defaultCorePoolSize());
		clusterConfigBuilder.loadBalancingStrategy(loadBalancingStrategy());
		clusterConfigBuilder.claimTimeout(Timeouts.from(claimTimeout()));
		clusterConfigBuilder.allocatorFactory(allocatorFactory);
		clusterConfigBuilder.defaultExpirationPolicy(ExpirationPolicies.or(expirationPolicies()));
		return clusterConfigBuilder.build();
	}

	@Value.Immutable
	public abstract class AbstractClusterOptions<PoolKey, T> implements ClusterOptionsBase<PoolKey, T> {}
}
