package test;

import java.util.Optional;
import java.util.concurrent.CountDownLatch;

import org.bbottema.clusteredobjectpool.core.ClusterConfig;
import org.bbottema.clusteredobjectpool.core.ResourceClusters;
import org.bbottema.clusteredobjectpool.core.api.ResourceKey.ResourceClusterAndPoolKey;
import org.bbottema.genericobjectpool.Allocator;

public class PoolTest {

	public static void main(String[] args) throws InterruptedException {
		ClusterConfig<Integer, String> clusterConfig = ClusterConfig.<Integer, String>builder()
				.allocatorFactory(pk -> new Allocator<String>() {

					private int index;

					@Override
					public String allocate() {
						return String.format("%s-%s", pk, index++);
					}
				})
				.defaultCorePoolSize(1)
				.defaultMaxPoolSize(Integer.MAX_VALUE)
				.build();

		ResourceClusters<Optional<Void>, Integer, String> clusters = new ResourceClusters<>(clusterConfig);
		var poolCount = 4;
		for (int i = 0; i < poolCount; i++)
			clusters.registerResourcePool(new ResourceClusterAndPoolKey<>(Optional.empty(), i));

		var claimCount = 25;
		var latch = new CountDownLatch(claimCount);
		for (int i = 0; i < claimCount; i++) {
			new Thread(() -> {
				try {
					// claim from cluster, blocking until a resource becomes available
					var resource = clusters.claimResourceFromCluster(Optional.empty());
					try {
						System.out.println(resource.getAllocatedObject());
					} finally {
						resource.release();
					}
				} catch (Throwable t) {
					t.printStackTrace();
				} finally {
					latch.countDown();
				}
			}).start();
		}
		latch.await();
	}
}
