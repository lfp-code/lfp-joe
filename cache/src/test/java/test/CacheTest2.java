package test;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.threadly.concurrent.SubmitterExecutor;
import org.threadly.concurrent.future.FutureUtils;
import org.threeten.bp.Duration;

import com.lfp.joe.cache.Caches;
import com.lfp.joe.cache.caffeine.CacheLoaderLFP;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.threads.executor.SubmitterSchedulerLFP;

import one.util.streamex.LongStreamEx;

public class CacheTest2 {

	public static void main(String[] args) throws InterruptedException {
		var cache = Caches.<Long, String>newCaffeineBuilder()
				.maximumSize(0)
				.executor(Threads.Pools.centralPool().limit())
				.removalListener((key, value, cause) -> {
					System.out.println(String.format("removal. key:%s value:%s cause:%s", key, value, cause));
				})
				.buildAsync(new CacheLoaderLFP<Long, String>() {

					@Override
					public @NonNull Future<Map<Long, String>> asyncLoadAll(@NonNull Iterable<? extends Long> keys,
							@NonNull SubmitterExecutor executor) {
						return SubmitterSchedulerLFP.from(executor).submitScheduled(() -> {
							return process(keys);
						}, Duration.ofSeconds(5).toMillis());
					}

				});
		for (int i = 0; i < 2; i++) {
			System.out.println(cache.asMap().size());
			var entries = LongStreamEx.range(50).boxed().chain(Streams.buffer(10)).mapToEntry(v -> {
				return Threads.Futures.asListenable(cache.getAll(v));
			}).toList();
			for (var ent : entries) {
				var index = ent.getKey();
				var future = ent.getValue();
				Threads.Futures.logFailureError(future, true, "error index:{}", index);
				future.resultCallback(map -> {
					System.out.println(String.format("%s - %s", index, Serials.Gsons.get().toJson(map)));
				});
			}
			FutureUtils.blockTillAllComplete(EntryStreams.of(entries).values().toList());

			System.out.println(cache.asMap().size());
		}
		cache.p
		cache.put(69l, CompletableFuture.completedFuture("69420"));
		cache.asMap().remove(1l);
		cache.asMap().remove(69l);
	}

	private static Map<Long, String> process(@NonNull Iterable<? extends Long> keys) {
		System.out.println("loading:" + keys + " thread:" + Thread.currentThread().getName());
		return Streams.<Long>of(keys).mapToEntry(v -> "value_" + v).toMap();
	}

	private static class Test<C extends CompletionStage<Date>> implements CompletionStage<Date> {

		@Override
		public <U> CompletionStage<U> thenApply(Function<? super Date, ? extends U> fn) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <U> CompletionStage<U> thenApplyAsync(Function<? super Date, ? extends U> fn) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <U> CompletionStage<U> thenApplyAsync(Function<? super Date, ? extends U> fn, Executor executor) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public CompletionStage<Void> thenAccept(Consumer<? super Date> action) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public CompletionStage<Void> thenAcceptAsync(Consumer<? super Date> action) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public CompletionStage<Void> thenAcceptAsync(Consumer<? super Date> action, Executor executor) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public CompletionStage<Void> thenRun(Runnable action) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public CompletionStage<Void> thenRunAsync(Runnable action) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public CompletionStage<Void> thenRunAsync(Runnable action, Executor executor) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <U, V> CompletionStage<V> thenCombine(CompletionStage<? extends U> other,
				BiFunction<? super Date, ? super U, ? extends V> fn) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <U, V> CompletionStage<V> thenCombineAsync(CompletionStage<? extends U> other,
				BiFunction<? super Date, ? super U, ? extends V> fn) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <U, V> CompletionStage<V> thenCombineAsync(CompletionStage<? extends U> other,
				BiFunction<? super Date, ? super U, ? extends V> fn, Executor executor) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <U> CompletionStage<Void> thenAcceptBoth(CompletionStage<? extends U> other,
				BiConsumer<? super Date, ? super U> action) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <U> CompletionStage<Void> thenAcceptBothAsync(CompletionStage<? extends U> other,
				BiConsumer<? super Date, ? super U> action) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <U> CompletionStage<Void> thenAcceptBothAsync(CompletionStage<? extends U> other,
				BiConsumer<? super Date, ? super U> action, Executor executor) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public CompletionStage<Void> runAfterBoth(CompletionStage<?> other, Runnable action) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public CompletionStage<Void> runAfterBothAsync(CompletionStage<?> other, Runnable action) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public CompletionStage<Void> runAfterBothAsync(CompletionStage<?> other, Runnable action, Executor executor) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <U> CompletionStage<U> applyToEither(CompletionStage<? extends Date> other,
				Function<? super Date, U> fn) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <U> CompletionStage<U> applyToEitherAsync(CompletionStage<? extends Date> other,
				Function<? super Date, U> fn) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <U> CompletionStage<U> applyToEitherAsync(CompletionStage<? extends Date> other,
				Function<? super Date, U> fn, Executor executor) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public CompletionStage<Void> acceptEither(CompletionStage<? extends Date> other,
				Consumer<? super Date> action) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public CompletionStage<Void> acceptEitherAsync(CompletionStage<? extends Date> other,
				Consumer<? super Date> action) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public CompletionStage<Void> acceptEitherAsync(CompletionStage<? extends Date> other,
				Consumer<? super Date> action, Executor executor) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public CompletionStage<Void> runAfterEither(CompletionStage<?> other, Runnable action) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public CompletionStage<Void> runAfterEitherAsync(CompletionStage<?> other, Runnable action) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public CompletionStage<Void> runAfterEitherAsync(CompletionStage<?> other, Runnable action, Executor executor) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <U> CompletionStage<U> thenCompose(Function<? super Date, ? extends CompletionStage<U>> fn) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <U> CompletionStage<U> thenComposeAsync(Function<? super Date, ? extends CompletionStage<U>> fn) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <U> CompletionStage<U> thenComposeAsync(Function<? super Date, ? extends CompletionStage<U>> fn,
				Executor executor) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <U> CompletionStage<U> handle(BiFunction<? super Date, Throwable, ? extends U> fn) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <U> CompletionStage<U> handleAsync(BiFunction<? super Date, Throwable, ? extends U> fn) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <U> CompletionStage<U> handleAsync(BiFunction<? super Date, Throwable, ? extends U> fn,
				Executor executor) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public CompletionStage<Date> whenComplete(BiConsumer<? super Date, ? super Throwable> action) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public CompletionStage<Date> whenCompleteAsync(BiConsumer<? super Date, ? super Throwable> action) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public CompletionStage<Date> whenCompleteAsync(BiConsumer<? super Date, ? super Throwable> action,
				Executor executor) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public CompletionStage<Date> exceptionally(Function<Throwable, ? extends Date> fn) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public CompletableFuture<Date> toCompletableFuture() {
			// TODO Auto-generated method stub
			return null;
		}

	}

}
