package test;

import java.time.Duration;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.function.Predicate;

import org.apache.commons.lang3.time.StopWatch;
import org.ehcache.config.units.MemoryUnit;

import com.google.common.hash.Hashing;
import com.google.common.reflect.TypeToken;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.cache.caffeine.LayerCache;
import com.lfp.joe.cache.ehcache.EhcacheOptions;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.IntStreamEx;

public class PredicateTest {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) {
		var blder = EhcacheOptions.builder(TypeToken.of(Bytes.class), TypeToken.of(Boolean.class), THIS_CLASS, "v1");
		blder.resourcePoolConfiguration(rpb -> {
			rpb = rpb.heap(128, MemoryUnit.MB);
			rpb = rpb.offheap(256, MemoryUnit.MB);
			rpb = rpb.disk(10, MemoryUnit.GB, true);
			return rpb;
		});
		var ehcache = Caches.newEhcache(blder.build());
		var cache = LayerCache.create(Caches.newCaffeineBuilder(500_000, null, Duration.ofMinutes(1)), ehcache);
		Predicate<? super String> predicateBase = v -> v.contains("9");
		var list = IntStreamEx.range(50_000).mapToObj(v -> Utils.Crypto.getRandomString()).toList();
		Function<String, Bytes> hashFunction = key -> {
			var hasher = Hashing.farmHashFingerprint64().newHasher();
			hasher.putBytes(Utils.Bits.from(key).array());
			return Utils.Bits.from(hasher.hash().asBytes());
		};
		Map<Boolean, Predicate<? super String>> predicates = new LinkedHashMap<>();
		predicates.put(true, Caches.predicate(cache, hashFunction, predicateBase));
		predicates.put(false, predicateBase);
		cache.getIfPresent(UUID.randomUUID().toString());
		System.out.println("starting");
		for (int i = 0; i < 5; i++) {
			for (var ent : predicates.entrySet()) {
				var cached = ent.getKey();
				var predicate = ent.getValue();
				var sw = StopWatch.createStarted();

			}
		}
		System.err.println("done");
	}

}
