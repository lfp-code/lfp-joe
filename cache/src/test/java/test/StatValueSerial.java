package test;

import java.util.Date;

import com.lfp.joe.cache.StatValue;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

public class StatValueSerial {

	public static void main(String[] args) {
		var test = new Test();
		test.value = "COOL FUN! " + Utils.Crypto.getRandomString();
		var statValue = StatValue.build(test);
		System.out.println(Serials.Gsons.get().toJson(statValue));
	}

	private static class Test {

		public Date createdAt = new Date();
		public String value;

	}

}
