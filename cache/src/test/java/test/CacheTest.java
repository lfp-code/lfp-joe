package test;

import java.util.Date;

import org.threadly.concurrent.SameThreadSubmitterExecutor;

import com.lfp.joe.cache.Caches;

public class CacheTest {

	public static void main(String[] args) {
		var cache = Caches.<String, Date>newCaffeineBuilder()
				.maximumSize(0)
				.executor(SameThreadSubmitterExecutor.instance())
				.buildAsync();

		System.out.println(cache.get("123", nil -> {
			cache.get("1234", nil2 -> new Date());
			return new Date();
		}).join());

	}
}
