package test;

import java.time.Duration;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import org.apache.commons.lang3.time.StopWatch;
import org.ehcache.config.units.MemoryUnit;

import com.github.benmanes.caffeine.cache.Cache;
import com.google.common.hash.Hashing;
import com.google.common.reflect.TypeToken;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.cache.caffeine.LayerCache;
import com.lfp.joe.cache.ehcache.EhcacheOptions;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.IntStreamEx;

public class ComparatorTest {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) {
		var blder = EhcacheOptions.builder(TypeToken.of(Bytes.class), TypeToken.of(Integer.class), THIS_CLASS, "v1");
		blder.resourcePoolConfiguration(rpb -> {
			rpb = rpb.heap(128, MemoryUnit.MB);
			rpb = rpb.offheap(256, MemoryUnit.MB);
			rpb = rpb.disk(10, MemoryUnit.GB, true);
			return rpb;
		});
		var ehcache = Caches.newEhcache(blder.build());
		var cache = LayerCache.create(Caches.newCaffeineBuilder(500_000, null, Duration.ofMinutes(1)), ehcache);
		var list = IntStreamEx.range(5_000).mapToObj(v -> Utils.Crypto.getRandomString()).toList();
		Map<String, Bytes> hashCache = new ConcurrentHashMap<>();
		Function<String, Bytes> hashFunction = key -> {
			return hashCache.computeIfAbsent(key, nil -> {
				var hasher = Hashing.farmHashFingerprint64().newHasher();
				hasher.putBytes(Utils.Bits.from(key).array());
				return Utils.Bits.from(hasher.hash().asBytes());
			});
		};
		Comparator<? super String> sorterBase = Utils.Strings::compareIgnoreCase;
		Map<String, Comparator<? super String>> sorters = new LinkedHashMap<>();
		sorters.put("multi-cache", Caches.comparator(cache, (v1, v2) -> {
			return hashFunction.apply(v1).append(hashFunction.apply(v2));
		}, sorterBase));
		Cache<Bytes, Integer> testCache = Caches.newCaffeineBuilder().build();
		sorters.put("test-cache", (v1, v2) -> {
			var key = hashFunction.apply(v1).append(hashFunction.apply(v2));
			return testCache.get(key, nil -> sorterBase.compare(v1, v2));
		});
		sorters.put("base", sorterBase);
		cache.getIfPresent(UUID.randomUUID().toString());
		System.out.println("starting");
		for (var minimumArrayLength : Arrays.asList(Utils.Machine.logicalProcessorCount() * 10, 1 << 13)) {
			for (int i = 0; i < 5; i++) {
				for (var ent : sorters.entrySet()) {
					var cached = ent.getKey();
					var sorter = ent.getValue();
					var sw = StopWatch.createStarted();

				}
			}
		}
		System.err.println("done");
		System.exit(0);
		System.err.println("done");
	}

}
