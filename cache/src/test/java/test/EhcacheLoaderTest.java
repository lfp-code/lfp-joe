package test;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

import org.ehcache.config.units.MemoryUnit;
import org.ehcache.spi.loaderwriter.BulkCacheLoadingException;
import org.ehcache.spi.loaderwriter.CacheLoaderWriter;

import com.lfp.joe.cache.ehcache.EhcacheOptions;
import com.lfp.joe.cache.ehcache.Ehcaches;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.function.KeyGenerator;

public class EhcacheLoaderTest {

	public static void main(String[] args) {
		var options = EhcacheOptions.of(String.class, Date.class).withResourcePoolsBuilderConfiguration(rpb -> {
			return rpb.disk(10, MemoryUnit.GB);
		});
		var ccb = Ehcaches.createCacheConfigurationBuilder(options)
				.withLoaderWriter(new CacheLoaderWriter<String, Date>() {

					@Override
					public void write(String key, Date value) throws Exception {
						System.out.println("write");

					}

					@Override
					public Date load(String key) throws Exception {
						var map = loadAll(Arrays.asList(key));
						return Optional.ofNullable(map).map(v -> v.get(key)).orElse(null);
					}

					@Override
					public void delete(String key) throws Exception {
						System.out.println("delete");

					}

					@Override
					public Map<String, Date> loadAll(Iterable<? extends String> keys)
							throws BulkCacheLoadingException, Exception {
						return Streams.of(keys).distinct().mapToEntry(v -> (String) v, nil -> new Date()).toMap();
					}
				});
		var cache = Ehcaches.defaultCacheManager().createCache(KeyGenerator.apply(options.idParts()), ccb);
		var value = cache.get("neat");
		System.out.println(value);
	}
}
