package test;

import java.time.Duration;

import com.lfp.joe.cache.Expiry;

public class ExpiryTest {

	public static void main(String[] args) {
		var expiry = Expiry.<String, String>builder().build();
		expiry = expiry.or(Expiry.builder()
				.expireAfterAccess(Duration.ofSeconds(30))
				.expireAfterWrite(Duration.ofMinutes(1))
				.build());
		System.out.println(expiry.getExpiryForCreation("1", "2"));
		System.out.println(expiry.getExpiryForAccess("1", () -> "2"));
	}

}
