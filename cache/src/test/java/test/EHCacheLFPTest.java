package test;

import java.util.Date;

import com.lfp.joe.cache.Caches;
import com.lfp.joe.cache.ehcache.EhcacheLFP;
import com.lfp.joe.cache.ehcache.EhcacheOptions;

public class EHCacheLFPTest {

	public static void main(String[] args) {
		var options = EhcacheOptions.of(Date.class, String.class, "neat");
		var cache = Caches.newEhcache(options);
		cache = EhcacheLFP.from(cache);
		System.out.println(cache.getStatus());
	}
}
