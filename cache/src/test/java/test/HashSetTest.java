package test;

import java.util.Date;
import java.util.HashSet;
import java.util.stream.Stream;

import com.lfp.joe.core.function.DistinctInstancePredicate;

public class HashSetTest {

	public static void main(String[] args) {
		var d1 = new Date1(0);
		var d2 = new Date1(0);
		var set = new HashSet<Object>();
		set.add(d1);
		set.add(d2);
		System.out.println(set.size());
		var predicate = DistinctInstancePredicate.create();
		var count = Stream.of(d1, d2, d2, d1, new Date1(0)).filter(predicate).count();
		System.out.println(count);

	}

	private static class Date1 extends Date {

		public Date1(long date) {
			super(date);
		}

	}

}
