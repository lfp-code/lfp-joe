package test;

import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.Collection;
import java.util.Map.Entry;

import com.lfp.joe.stream.Streams;

public class FunctionTest {

	public static void main(String[] args) {

	}

	private static class TestEntry<K, V> extends SimpleImmutableEntry<K, V> {

		public TestEntry(K key, V value) {
			super(key, value);
		}

		public String print(Collection<? extends Entry<? extends K, ? extends V>> append) {
			var stream = Streams.<Entry<K, V>>of(this).append(append);
			return stream.joining(", ");
		}
	}

}
