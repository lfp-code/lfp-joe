package com.lfp.joe.cache.caffeine.writerloader;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.Duration;
import java.util.Arrays;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.CacheLoader;
import com.github.benmanes.caffeine.cache.CacheWriter;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.github.benmanes.caffeine.cache.RemovalCause;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

import javassist.util.proxy.MethodHandler;

public abstract class CacheWriterLoader<K, V> implements CacheLoader<K, V>, CacheWriter<K, V> {

	private static final Method INVALIDATE_KEY_METHOD;
	static {
		INVALIDATE_KEY_METHOD = Utils.Functions.unchecked(() -> Cache.class.getMethod("invalidate", Object.class));
	}
	private static final Method INVALIDATE_KEYS_METHOD;
	static {
		INVALIDATE_KEYS_METHOD = Utils.Functions
				.unchecked(() -> Cache.class.getMethod("invalidateAll", Iterable.class));
	}
	private static final Method INVALIDATE_ALL_METHOD;
	static {
		INVALIDATE_ALL_METHOD = Utils.Functions.unchecked(() -> Cache.class.getMethod("invalidateAll"));
	}

	@Override
	public void write(@NonNull K key, @NonNull V value) {
		Utils.Functions.unchecked(() -> storageUpdate(key, value));
	}

	@Override
	public void delete(@NonNull K key, @Nullable V value, @NonNull RemovalCause cause) {
		if (RemovalCause.EXPLICIT.equals(cause))
			Utils.Functions.unchecked(() -> storageUpdate(key, null));

	}

	@Override
	public @Nullable V load(@NonNull K key) throws Exception {
		{// load from storage without load or sync
			var value = storageGetNormalize(key);
			if (value != null)
				return value;
		}
		// optionally create sync access
		var loadAccessor = getStorageLockAccessor(key);
		if (loadAccessor == null) {
			return storageUpdate(key, loadFresh(key));
		} else
			return loadAccessor.apply(() -> {
				// re-read if in sync access
				var value = storageGetNormalize(key);
				if (value != null)
					return value;
				return storageUpdate(key, loadFresh(key));
			});

	}

	public LoadingCache<K, V> buildCache() {
		return buildCache(Caches.newCaffeineBuilder());
	}

	public LoadingCache<K, V> buildCache(Caffeine<K, V> cacheBuilder) {
		return buildCache(cacheBuilder, false);
	}

	public LoadingCache<K, V> buildCache(Caffeine<K, V> cacheBuilder, boolean disableStorageUpdateOnInvalidate) {
		return Utils.Functions.unchecked(() -> buildCacheInternal(cacheBuilder, disableStorageUpdateOnInvalidate));
	}

	protected LoadingCache<K, V> buildCacheInternal(Caffeine<K, V> cacheBuilder,
			boolean disableStorageUpdateOnInvalidate) throws NoSuchMethodException, IllegalArgumentException,
			InstantiationException, IllegalAccessException, InvocationTargetException {
		java.util.Objects.requireNonNull(cacheBuilder);
		cacheBuilder = cacheBuilder.writer(this);
		var delegate = cacheBuilder.build(this);
		if (disableStorageUpdateOnInvalidate)
			return delegate;
		var proxyFactory = JavaCode.Proxies.createFactory(LoadingCache.class);
		return JavaCode.Proxies.createInstance(proxyFactory, new MethodHandler() {

			@SuppressWarnings("unchecked")
			@Override
			public Object invoke(Object self, Method thisMethod, Method proceed, Object[] args) throws Throwable {
				Iterable<K> invalidateKeys;
				if (INVALIDATE_KEY_METHOD.equals(thisMethod))
					invalidateKeys = Arrays.asList((K) args[0]);
				else if (INVALIDATE_KEYS_METHOD.equals(thisMethod))
					invalidateKeys = (Iterable<K>) args[0];
				else if (INVALIDATE_ALL_METHOD.equals(thisMethod))
					invalidateKeys = delegate.asMap().keySet();
				else
					invalidateKeys = null;
				if (invalidateKeys != null) {
					for (var invalidateKey : invalidateKeys) {
						if (invalidateKey == null)
							continue;
						delete(invalidateKey, null, RemovalCause.EXPLICIT);
					}
				}
				return thisMethod.invoke(delegate, args);
			}
		});
	}

	protected V storageUpdate(@NonNull K key, @Nullable V value) throws Exception {
		value = normalizeValue(key, value);
		var ttl = value == null ? null : getStorageTTL(key, value);
		storageUpdate(key, value, ttl);
		return value;
	}

	protected V storageGetNormalize(@NonNull K key) throws Exception {
		return normalizeValue(key, storageGet(key));
	}

	protected V normalizeValue(@NonNull K key, @Nullable V value) {
		if (value == null)
			return null;
		var ttl = getStorageTTL(key, value);
		if (ttl == null)
			return value;
		if (ttl.toMillis() <= 0)
			return null;
		return value;
	}

	protected abstract @Nullable V loadFresh(@NonNull K key) throws Exception;

	protected abstract @Nullable ThrowingFunction<ThrowingSupplier<V, Exception>, V, Exception> getStorageLockAccessor(
			@NonNull K key);

	protected abstract @Nullable Duration getStorageTTL(@NonNull K key, @NonNull V result);

	protected abstract @Nullable V storageGet(@NonNull K key) throws Exception;

	protected abstract void storageUpdate(@NonNull K key, @Nullable V value, @Nullable Duration ttl) throws Exception;

}
