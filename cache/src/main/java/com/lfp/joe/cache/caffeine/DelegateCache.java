package com.lfp.joe.cache.caffeine;

import java.util.Map;
import java.util.concurrent.ConcurrentMap;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.github.benmanes.caffeine.cache.Policy;

public interface DelegateCache<K, V> extends WrapperCache<K, K, V, V> {

	@Override
	default K wrapKey(K key) {
		return key;
	}

	@Override
	default K unwrapKey(K wrappedKey) {
		return wrappedKey;
	}

	@Override
	default V wrapValue(V value) {
		return value;
	}

	@Override
	default V unwrapValue(V wrappedValue) {
		return wrappedValue;
	}

	@Override
	default @Nullable V getIfPresent(@NonNull Object key) {
		return getDelegate().getIfPresent(key);
	}

	@Override
	default void invalidate(@NonNull Object key) {
		getDelegate().invalidate(key);
	}

	@Override
	default @NonNull Map<@NonNull K, @NonNull V> getAllPresent(@NonNull Iterable<@NonNull ?> keys) {
		return getDelegate().getAllPresent(keys);
	}

	@Override
	default void invalidateAll(@NonNull Iterable<?> keys) {
		getDelegate().invalidateAll(keys);
	}

	@Override
	default @NonNull Policy<K, V> policy() {
		return getDelegate().policy();
	}

	@Override
	default @NonNull ConcurrentMap<@NonNull K, @NonNull V> asMap() {
		return getDelegate().asMap();
	}

	public static interface Loading<K, V> extends DelegateCache<K, V>, WrapperCache.Loading<K, K, V, V> {

		@Override
		default @NonNull Map<@NonNull K, @NonNull V> getAll(@NonNull Iterable<? extends @NonNull K> keys) {
			return getDelegate().getAll(keys);
		}

	}

}
