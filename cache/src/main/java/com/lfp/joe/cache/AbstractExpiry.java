package com.lfp.joe.cache;

import java.time.Duration;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

import org.checkerframework.checker.index.qual.NonNegative;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.ehcache.core.spi.store.Store.ValueHolder;
import org.ehcache.expiry.ExpiryPolicy;
import org.immutables.value.Value;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.Durations;
import com.lfp.joe.core.function.NumeralFunctions.TriFunction;
import com.lfp.joe.stream.Streams;

/*
 * this gets way too messy with caffeine, just use the Expiry policy or what's built into caffeine
 */

@Deprecated
@SuppressWarnings("unchecked")
@Value.Immutable
@ValueLFP.Style
abstract class AbstractExpiry<K, V> implements com.github.benmanes.caffeine.cache.Expiry<K, V>, ExpiryPolicy<K, V> {

	private static final BiFunction<?, ?, Duration> EXPIRE_AFTER_CREATE_DEFAULT = (k, v) -> null;
	private static final TriFunction<?, ?, Optional<Duration>, Duration> EXPIRE_AFTER_UPDATE_DEFAULT = (k, v,
			nil) -> null;
	private static final TriFunction<?, ?, Optional<Duration>, Duration> EXPIRE_AFTER_READ_DEFAULT = (k, v,
			nil) -> null;

	abstract Optional<Duration> expireAfterAccess();

	abstract Optional<Duration> expireAfterWrite();

	@Value.Default
	public BiFunction<K, V, Duration> expireAfterCreate() {
		return (BiFunction<K, V, Duration>) EXPIRE_AFTER_CREATE_DEFAULT;
	}

	@Value.Default
	public TriFunction<K, V, Optional<Duration>, Duration> expireAfterUpdate() {
		return (TriFunction<K, V, Optional<Duration>, Duration>) EXPIRE_AFTER_UPDATE_DEFAULT;
	}

	@Value.Default
	public TriFunction<K, Supplier<? extends V>, Optional<Duration>, Duration> expireAfterRead() {
		return (TriFunction<K, Supplier<? extends V>, Optional<Duration>, Duration>) EXPIRE_AFTER_READ_DEFAULT;
	}

	public Expiry<K, V> or(Expiry<? super K, ? super V> other) {
		var thisExpiry = Expiry.copyOf(this);
		if (other == null)
			return thisExpiry;
		return thisExpiry.withExpireAfterCreate((k, v) -> {
			Function<Expiry<? super K, ? super V>, Duration> durationFunction = expiry -> expiry.expireAfterCreate()
					.apply(k, v);
			return normalize(durationFunction.apply(thisExpiry), durationFunction.apply(other)).orElse(null);
		}).withExpireAfterUpdate((k, v, currentTTLOp) -> {
			Function<Expiry<? super K, ? super V>, Duration> durationFunction = expiry -> expiry.expireAfterUpdate()
					.apply(k, v, currentTTLOp);
			return normalize(durationFunction.apply(thisExpiry), durationFunction.apply(other)).orElse(null);
		}).withExpireAfterRead((k, valueSupplier, currentTTLOp) -> {
			Function<Expiry<? super K, ? super V>, Duration> durationFunction = expiry -> expiry.expireAfterRead()
					.apply(k, valueSupplier, currentTTLOp);
			return normalize(durationFunction.apply(thisExpiry), durationFunction.apply(other)).orElse(null);
		});
	}

	@Value.Check
	AbstractExpiry<K, V> check() {
		if (expireAfterAccess().isPresent()) {
			var ttl = expireAfterAccess().get();
			var append = Expiry.<K, V>builder()
					.expireAfterCreate((k, v) -> ttl)
					.expireAfterUpdate((k, v, currentTTLOp) -> ttl)
					.expireAfterRead((k, valueSupplier, currentTTLOp) -> ttl)
					.build();
			return Expiry.copyOf(this).withExpireAfterAccess(Optional.empty()).or(append);
		}
		if (expireAfterWrite().isPresent()) {
			var ttl = expireAfterWrite().get();
			var append = Expiry.<K, V>builder()
					.expireAfterCreate((k, v) -> ttl)
					.expireAfterUpdate((k, v, currentTTLOp) -> ttl)
					.build();
			return Expiry.copyOf(this).withExpireAfterWrite(Optional.empty()).or(append);
		}
		return this;
	}

	@Override
	public long expireAfterCreate(@NonNull K key, @NonNull V value, long currentTime) {
		return normalize(expireAfterCreate().apply(key, value)).map(Duration::toNanos)
				.orElseGet(ExpiryPolicy.INFINITE::toNanos);
	}

	@Override
	public long expireAfterUpdate(@NonNull K key, @NonNull V value, long currentTime,
			@NonNegative long currentDuration) {
		return normalize(expireAfterUpdate().apply(key, value, Optional.of(Duration.ofNanos(currentDuration))))
				.map(Duration::toNanos)
				.orElse(currentDuration);
	}

	@Override
	public long expireAfterRead(@NonNull K key, @NonNull V value, long currentTime, @NonNegative long currentDuration) {
		return normalize(expireAfterRead().apply(key, () -> value, Optional.of(Duration.ofNanos(currentDuration))))
				.map(Duration::toNanos)
				.orElse(currentDuration);
	}

	@Override
	public Duration getExpiryForCreation(K key, V value) {
		return normalize(expireAfterCreate().apply(key, value)).orElse(ExpiryPolicy.INFINITE);
	}

	@Override
	public Duration getExpiryForUpdate(K key, Supplier<? extends V> oldValue, V newValue) {
		var currentTTLOp = tryGetCurrentTTL(oldValue);
		return normalize(expireAfterUpdate().apply(key, newValue, currentTTLOp)).orElse(currentTTLOp.orElse(null));
	}

	@Override
	public Duration getExpiryForAccess(K key, Supplier<? extends V> value) {
		var currentTTLOp = tryGetCurrentTTL(value);
		return normalize(expireAfterRead().apply(key, value, currentTTLOp)).orElse(currentTTLOp.orElse(null));
	}

	protected static Optional<Duration> tryGetCurrentTTL(Supplier<?> valueSupplier) {
		return CoreReflections.tryCast(valueSupplier, ValueHolder.class).map(v -> {
			var expiration = v.expirationTime();
			if (Objects.equals(expiration, ValueHolder.NO_EXPIRE))
				return null;
			var ttlMillis = expiration - System.currentTimeMillis();
			var ttl = Duration.ofMillis(ttlMillis);
			return normalize(ttl).orElse(null);
		});
	}

	protected static Optional<Duration> normalize(Duration... durations) {
		return Streams.of(durations).nonNull().map(v -> {
			if (Durations.isNegativeOrZero(v))
				return Duration.ZERO;
			if (Durations.isGreaterThanOrEqual(v, ExpiryPolicy.INFINITE))
				return ExpiryPolicy.INFINITE;
			return v;
		}).sorted().findFirst();
	}

}
