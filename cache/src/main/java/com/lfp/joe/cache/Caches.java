package com.lfp.joe.cache;

import java.time.Duration;
import java.util.Comparator;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

import org.apache.commons.lang3.Validate;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.lfp.joe.cache.ehcache.EhcacheLFP;
import com.lfp.joe.cache.ehcache.EhcacheOptions;
import com.lfp.joe.cache.ehcache.Ehcaches;

@SuppressWarnings("unchecked")
public class Caches {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static <K, V> Caffeine<K, V> newCaffeineBuilder() {
		return newCaffeineBuilder(-1, null, null);
	}

	public static <K, V> Caffeine<K, V> newCaffeineBuilder(long maximumSize, Duration expireAfterWrite,
			Duration expireAfterAccess) {
		Validate.isTrue(maximumSize >= -1, "invalid maximumSize:%s", maximumSize);
		Caffeine<K, V> builder = (Caffeine<K, V>) Caffeine.newBuilder();
		if (maximumSize >= 0)
			builder = builder.maximumSize(maximumSize);
		if (expireAfterWrite != null)
			builder.expireAfterWrite(expireAfterWrite);
		if (expireAfterAccess != null)
			builder.expireAfterAccess(expireAfterAccess);
		return builder;
	}

	public static <K, V> EhcacheLFP<K, V> newEhcache(EhcacheOptions<K, V> ehcacheOptions) {
		Objects.requireNonNull(ehcacheOptions);
		return Ehcaches.createCache(Ehcaches.defaultCacheManager(), ehcacheOptions);
	}

	public static <T, K> Comparator<T> comparator(Cache<K, Integer> cache, BiFunction<T, T, K> keyFunction,
			Comparator<? super T> comparator) {
		Objects.requireNonNull(cache);
		Objects.requireNonNull(keyFunction);
		Objects.requireNonNull(comparator);
		return (v1, v2) -> {
			var result = cache.getIfPresent(keyFunction.apply(v2, v1));
			if (result != null)
				return result * -1;
			return cache.get(keyFunction.apply(v1, v2), nil -> {
				return comparator.compare(v1, v2);
			});
		};
	}

	public static <T, K> Predicate<T> predicate(Cache<K, Boolean> cache, Function<T, K> keyFunction,
			Predicate<? super T> predicate) {
		Objects.requireNonNull(cache);
		Objects.requireNonNull(keyFunction);
		Objects.requireNonNull(predicate);
		return v -> {
			return cache.get(keyFunction.apply(v), nil -> {
				var result = predicate.test(v);
				return result;
			});
		};
	}

}
