package com.lfp.joe.cache;

import java.time.Duration;

import org.checkerframework.checker.nullness.qual.NonNull;

public interface ExpirationFunction<KK, VV> {

	public Duration durationToLive(@NonNull KK key, @NonNull VV value, long currentTimeNanos,
			@NonNull Duration currentDuration);
}
