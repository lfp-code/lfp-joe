package com.lfp.joe.cache.caffeine;

import java.time.Duration;
import java.util.function.Supplier;

import org.checkerframework.checker.index.qual.NonNegative;
import org.checkerframework.checker.nullness.qual.NonNull;

import com.github.benmanes.caffeine.cache.Expiry;

public interface TTLExpiry<K, V> extends Expiry<K, V> {

	@Override
	default long expireAfterCreate(@NonNull K key, @NonNull V value, long currentTime) {
		return timeToLiveNanos(timeToLive(true, key, value, () -> Caffeines.MAX_TTL));
	}

	@Override
	default long expireAfterUpdate(@NonNull K key, @NonNull V value, long currentTime,
			@NonNegative long currentDuration) {
		return timeToLiveNanos(timeToLive(true, key, value, () -> Duration.ofNanos(currentDuration)));
	}

	@Override
	default long expireAfterRead(@NonNull K key, @NonNull V value, long currentTime,
			@NonNegative long currentDuration) {
		return timeToLiveNanos(timeToLive(false, key, value, () -> Duration.ofNanos(currentDuration)));
	}

	private static long timeToLiveNanos(Duration ttlDuration) {
		if (ttlDuration == null)
			return 0l;
		var nanos = ttlDuration.toNanos();
		if (nanos <= 0)
			return 0l;
		return nanos;
	}

	// currentDuration is current TIME TO LIVE not duration of existence
	// return currentDuration for no change, aka read
	Duration timeToLive(boolean write, @NonNull K key, @NonNull V value, @NonNull Supplier<Duration> noChangeSupplier);

}
