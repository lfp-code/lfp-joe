package com.lfp.joe.cache.caffeine.writerloader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.lfp.joe.cache.StatValue;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import org.apache.commons.lang3.StringEscapeUtils;

import one.util.streamex.StreamEx;

public class ByteStreamSerializers {

	public static <K, V> ByteStreamWriterLoader.ByteStreamSerializer<K, StatValue<V>> gson() {
		return gson(Serials.Gsons.get());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <K, V> ByteStreamWriterLoader.ByteStreamSerializer<K, StatValue<V>> gson(Gson gson) {
		return gson(Serials.Gsons.get(), (TypeToken) TypeToken.of(StatValue.class));
	}

	public static <K, V> ByteStreamWriterLoader.ByteStreamSerializer<K, V> gson(TypeToken<V> typeToken) {
		return gson(Serials.Gsons.get(), typeToken);
	}

	public static <K, V> ByteStreamWriterLoader.ByteStreamSerializer<K, V> gson(Gson gson, TypeToken<V> typeToken) {
		Objects.requireNonNull(gson);
		Objects.requireNonNull(typeToken);
		return new ByteStreamWriterLoader.ByteStreamSerializer<K, V>() {

			@Override
			public V deserialize(K key, InputStream inputStream) throws IOException {
				var result = Serials.Gsons.fromStream(gson, inputStream, typeToken);
				return result;
			}

			@Override
			public void serialize(K key, V value, OutputStream outputStream) throws IOException {
				if (value == null)
					return;
				Serials.Gsons.toStream(gson, value, outputStream);
			}
		};
	}

	public static <K, V> ByteStreamWriterLoader.ByteStreamSerializer<K, StatValue<List<String>>> stringList() {
		return stringList(Serials.Gsons.get());
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public static <K, V> ByteStreamWriterLoader.ByteStreamSerializer<K, StatValue<List<String>>> stringList(Gson gson) {
		Objects.requireNonNull(gson);
		return new ByteStreamWriterLoader.ByteStreamSerializer<K, StatValue<List<String>>>() {

			@Override
			public StatValue<List<String>> deserialize(K key, InputStream inputStream) throws IOException {
				var liter = Utils.Strings.streamLines(inputStream).map(StringEscapeUtils::unescapeJava).iterator();
				if (!liter.hasNext())
					return null;
				StatValue<List<String>> statValueMeta = gson.fromJson(liter.next(), StatValue.class);
				if (statValueMeta == null)
					return null;
				var value = Utils.Lots.stream(liter).toList();
				return statValueMeta.toBuilder().value(value).build();
			}

			@Override
			public void serialize(K key, StatValue<List<String>> statValue, OutputStream outputStream)
					throws IOException {
				if (statValue == null)
					return;
				var value = statValue.getValue();
				statValue = statValue.toBuilder().value(null).build();
				StreamEx<String> lineStream = StreamEx.of(gson.toJson(statValue));
				lineStream = lineStream.append(Utils.Lots.stream(value));
				try (var bw = Utils.Bits.bufferedWriter(outputStream)) {
					var liter = lineStream.iterator();
					while (liter.hasNext()) {
						var line = liter.next();
						line = StringEscapeUtils.escapeJava(line);
						if (liter.hasNext())
							line += Utils.Strings.newLine();
						bw.append(line);
					}
				}
			}
		};
	}

}
