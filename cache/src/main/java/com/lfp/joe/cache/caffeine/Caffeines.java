package com.lfp.joe.cache.caffeine;

import java.time.Duration;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.Expiry;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.classpath.MemberCache.Accessor;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class Caffeines {

	public static final Duration MAX_TTL = Duration.ofNanos(Long.MAX_VALUE);

	private static final Accessor<Caffeine, Expiry> Caffeine_expiry_ACCESSOR = MemberCache
			.getFieldAccessor(FieldRequest.of(Caffeine.class, Expiry.class, "expiry"));

	public static <K, V> Optional<Expiry<K, V>> getExpiry(Caffeine<K, V> caffeine) {
		Objects.requireNonNull(caffeine, "caffeine required");
		var expiry = Caffeine_expiry_ACCESSOR.apply(caffeine);
		return Optional.ofNullable(expiry);
	}

	public static <K, V> @NonNull Caffeine<K, V> setExpiry(Caffeine<K, V> caffeine, Expiry<K, V> expiry) {
		Objects.requireNonNull(expiry, "expiry required");
		expiry = merge(expiry, getExpiry(caffeine).orElse(null));
		Caffeine_expiry_ACCESSOR.accept(caffeine, expiry);
		return caffeine;
	}

	private static <K, V> Expiry<K, V> merge(Expiry<K, V> expiry, Expiry<K, V> currentExpiry) {
		if (currentExpiry == null || Objects.equals(expiry, currentExpiry))
			return expiry;
		Supplier<IllegalArgumentException> orElseThrow = () -> {
			var message = String
					.format("expiry merge failed. new:%s current:%s", expiry, currentExpiry);
			return new IllegalArgumentException(message);
		};
		return CoreReflections.tryCast(expiry, com.lfp.joe.cache.Expiry.class)
				.orElseThrow(orElseThrow)
				.or(CoreReflections.tryCast(currentExpiry, com.lfp.joe.cache.Expiry.class)
						.orElseThrow(orElseThrow));
	}

}
