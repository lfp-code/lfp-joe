package com.lfp.joe.cache.ehcache;

import java.lang.reflect.Field;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.checkerframework.checker.index.qual.NonNegative;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.ehcache.Cache;
import org.ehcache.Status;
import org.ehcache.UserManagedCache;
import org.ehcache.config.CacheRuntimeConfiguration;
import org.ehcache.config.SizedResourcePool;
import org.ehcache.core.EhcacheBase;
import org.ehcache.core.spi.store.Store;
import org.ehcache.core.spi.store.Store.ValueHolder;
import org.ehcache.spi.resilience.StoreAccessException;
import org.immutables.value.Value;
import org.threadly.concurrent.SubmitterExecutor;

import com.github.benmanes.caffeine.cache.CacheWriter;
import com.github.benmanes.caffeine.cache.Policy;
import com.github.benmanes.caffeine.cache.RemovalCause;
import com.github.benmanes.caffeine.cache.stats.CacheStats;
import com.lfp.joe.bytecode.MethodHandlerLFP;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.cache.caffeine.CacheLoaderLFP;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.Delegating;
import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.ImmutableMemberCache.MethodRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.Registration;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.lock.KeyLock;
import com.lfp.joe.reflections.Proxies;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.stream.Streams;

import javassist.util.proxy.Proxy;
import one.util.streamex.EntryStream;

@ValueLFP.Style
@Value.Enclosing
@SuppressWarnings("deprecation")
public abstract class EhcacheLFP<K, V> implements Cache<K, V>, com.github.benmanes.caffeine.cache.Cache<K, V>,
		CacheWriter<K, V>, CacheLoaderLFP<K, V>, Delegating, Scrapable {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final Object AS_MAP_ATTRIBUTE_KEY = new Object();
	private static final Object KEY_LOCK_ATTRIBUTE_KEY = new Object();
	private static final Field EhcacheBase_store_FIELD = Throws
			.unchecked(() -> MemberCache.getField(FieldRequest.of(EhcacheBase.class, Store.class, "store")));

	@SuppressWarnings("unchecked")
	public static <K, V> EhcacheLFP<K, V> from(Cache<K, V> cache) {
		if (cache instanceof EhcacheLFP)
			return (EhcacheLFP<K, V>) cache;
		return create(cache, EhcacheLFP.class);
	}

	@SuppressWarnings("unchecked")
	protected static <K, V, C extends EhcacheLFP<K, V>> C create(Cache<K, V> cache, Class<C> superClass,
			Object... constructorArguments) {
		Objects.requireNonNull(cache);
		Objects.requireNonNull(superClass);
		var factory = Proxies.createFactory(superClass, com.lfp.joe.core.classpath.Delegating.class);
		C ehcache = (C) Throws.unchecked(() -> factory.create(constructorArguments));
		MethodHandlerLFP methodHandler = request -> {
			return request.proceedInvokeOrElseGet(() -> {
				if (com.lfp.joe.core.classpath.Delegating.isGetTargetsMethod(request.method()))
					return List.of(cache);
				var targets = com.lfp.joe.core.classpath.Delegating.streamTargets(request.proxy());
				var target = targets.filter(request.method().getDeclaringClass()::isInstance).findFirst().orElse(null);
				if (target != null)
					return request.invoke(target);
				return null;
			});
		};
		((Proxy) ehcache).setHandler(methodHandler);
		ehcache.onScrap(cache::clear);
		if (cache instanceof AutoCloseable)
			ehcache.onScrap((AutoCloseable) cache, Throws.consumerUnchecked(AutoCloseable::close));
		else {
			var closeMethod = MemberCache.tryGetMethod(MethodRequest.of(cache.getClass(), null, "close"), true)
					.orElse(null);
			if (closeMethod != null)
				ehcache.onScrap(Throws.runnableUnchecked(() -> closeMethod.invoke(cache)));
		}
		return ehcache;
	}

	private AtomicReference<Map<Object, Object>> attributesReference = new AtomicReference<>();

	public Optional<Store<K, V>> tryGetStore() {
		return streamTargets(EhcacheBase.class).findFirst()
				.map(Throws.functionUnchecked(EhcacheBase_store_FIELD::get))
				.map(Store.class::cast);
	}

	public EntryStream<K, Supplier<V>> stream() {
		return Streams.of(() -> Streams.of(this.iterator()))
				.mapToEntry(Cache.Entry::getKey)
				.invert()
				.mapValues(ent -> () -> ent.getValue());
	}

	public Optional<ValueHolder<V>> tryGetValueHolder(K key) {
		if (key == null)
			return Optional.empty();
		var store = tryGetStore().orElse(null);
		if (store == null)
			return Optional.empty();
		ValueHolder<V> valueHolder;
		try {
			valueHolder = store.get(key);
		} catch (StoreAccessException e) {
			return Optional.empty();
		}
		return Optional.ofNullable(valueHolder);
	}

	public OptionalLong tryGetCreationTime(K key) {
		return tryGetValueHolder(key).map(v -> OptionalLong.of(v.creationTime())).orElse(OptionalLong.empty());
	}

	public OptionalLong tryGetExpiration(K key) {
		return tryGetValueHolder(key).map(v -> OptionalLong.of(v.expirationTime())).orElse(OptionalLong.empty());
	}

	public Status getStatus() {
		return streamTargets(UserManagedCache.class).findFirst()
				.map(UserManagedCache::getStatus)
				.orElse(Status.AVAILABLE);
	}

	public Optional<K> asKey(Object key) {
		if (key == null)
			return null;
		var keyType = Optional.ofNullable(this.getRuntimeConfiguration())
				.map(CacheRuntimeConfiguration::getKeyType)
				.orElse(null);
		return CoreReflections.tryCast(key, keyType);
	}

	public Optional<V> asValue(Object value) {
		if (value == null)
			return null;
		var valueType = Optional.ofNullable(this.getRuntimeConfiguration())
				.map(CacheRuntimeConfiguration::getValueType)
				.orElse(null);
		return CoreReflections.tryCast(value, valueType);
	}

	@Override
	public void write(@NonNull K key, @NonNull V value) {
		this.put(key, value);
	}

	@Override
	public void delete(@NonNull K key, @Nullable V value, @NonNull RemovalCause cause) {
		if (RemovalCause.EXPLICIT == cause || RemovalCause.EXPIRED == cause)
			this.remove(key);
	}

	@SuppressWarnings("unchecked")
	@Override
	public @NonNull Future<Map<K, V>> asyncLoadAll(@NonNull Iterable<? extends K> keys,
			@NonNull SubmitterExecutor executor) {
		return executor.submit(() -> {
			Set<K> keySet = CoreReflections.tryCast(keys, Set.class)
					.orElseGet(() -> Streams.of(keys).nonNull().toSet());
			var map = this.getAll(keySet);
			return map;
		});
	}

	public CacheLoaderLFP<K, V> appendAndWrite(CacheLoaderLFP<? extends K, ? extends V> next) {
		next = CacheLoaderLFP.from(next).withLoadNotifier(kvMap -> {
			Set<K> removeKeys = new HashSet<>();
			var putMap = EntryStreams.of(kvMap).filter(ent -> {
				if (ent.getValue() != null)
					return true;
				removeKeys.add(ent.getKey());
				return false;
			}).toMap();
			if (!removeKeys.isEmpty())
				this.removeAll(removeKeys);
			if (!putMap.isEmpty())
				this.putAll(putMap);

		});
		return append(next);
	}

	@Override
	public @Nullable V get(@NonNull K key, @NonNull Function<? super K, ? extends V> mappingFunction) {
		var kvMap = getAll(Streams.of(key), mappingFunction == null ? null : keys -> {
			return Streams.<K>of(keys).<V>mapToEntry(mappingFunction).toMap();
		});
		return kvMap.get(key);
	}

	@Override
	public @NonNull Map<K, V> getAll(@NonNull Iterable<? extends @NonNull K> keys,
			@NonNull Function<Iterable<? extends @NonNull K>, @NonNull Map<K, V>> mappingFunction) {
		Objects.requireNonNull(mappingFunction);
		var keySet = Streams.<K>of(keys).nonNull().toCollection(LinkedHashSet::new);
		if (keySet.isEmpty())
			return Collections.emptyMap();
		Map<K, V> resultMap = new LinkedHashMap<>(keySet.size());
		keySet.forEach(v -> resultMap.put(v, null));
		BiConsumer<Map<K, V>, Boolean> resultMapUpdate = (map, write) -> {
			if (map == null || map.isEmpty())
				return;
			for (var ent : map.entrySet()) {
				var key = ent.getKey();
				var value = ent.getValue();
				if (value != null && resultMap.containsKey(key)) {
					resultMap.put(key, value);
					keySet.remove(key);
				}
				if (write) {
					if (value == null)
						this.remove(key);
					else
						this.put(key, value);
				}
			}
		};
		resultMapUpdate.accept(getAll(keySet), false);
		if (!keySet.isEmpty()) {
			try (var r = acquireKeyLocks(keySet)) {
				resultMapUpdate.accept(getAll(keySet), false);
				if (!keySet.isEmpty())
					resultMapUpdate.accept(mappingFunction.apply(keySet), true);
			}
		}
		return Collections.unmodifiableMap(resultMap);
	}

	@Override
	public @Nullable V getIfPresent(@NonNull Object key) {
		return asKey(key).map(this::get).orElse(null);
	}

	@Override
	public @NonNull Map<@NonNull K, @NonNull V> getAllPresent(@NonNull Iterable<@NonNull ?> keys) {
		var keySet = Streams.of(keys).mapPartial(v -> asKey(v)).nonNull().toSet();
		return getAll(keySet);
	}

	@Override
	public void invalidate(@NonNull Object key) {
		Objects.requireNonNull(key);
		asKey(key).ifPresent(this::remove);
	}

	@Override
	public void invalidateAll(@NonNull Iterable<@NonNull ?> keys) {
		Streams.of(keys).map(Objects::requireNonNull).mapPartial(this::asKey).chain(Streams.buffer()).forEach(list -> {
			this.removeAll(Set.copyOf(list));
		});
	}

	@Override
	public void invalidateAll() {
		this.clear();
	}

	@Override
	public @NonNegative long estimatedSize() {
		var resourcePools = getRuntimeConfiguration().getResourcePools();
		if (resourcePools == null)
			return 0;
		var resourceTypeSet = resourcePools.getResourceTypeSet();
		if (resourceTypeSet == null)
			return 0;
		var total = 0;
		for (var resourceType : resourceTypeSet) {
			if (resourceType == null)
				continue;
			var pool = resourcePools.getPoolForResource(resourceType);
			if (!(pool instanceof SizedResourcePool))
				continue;
			total += ((SizedResourcePool) pool).getSize();
		}
		return total;
	}

	@Override
	public @NonNull ConcurrentMap<@NonNull K, @NonNull V> asMap() {
		return computeAttributeIfAbsent(AS_MAP_ATTRIBUTE_KEY, nil -> {
			return new AbstractConcurrentMap<K, V>() {

				@Override
				public boolean replace(K key, V oldValue, V newValue) {
					return EhcacheLFP.this.replace(key, oldValue, newValue);
				}

				@Override
				public V replace(K key, V value) {
					return EhcacheLFP.this.replace(key, value);
				}

				@Override
				public boolean remove(Object key, Object value) {
					var cacheKey = EhcacheLFP.this.asKey(key).orElse(null);
					var cacheValue = EhcacheLFP.this.asValue(value).orElse(null);
					return EhcacheLFP.this.remove(cacheKey, cacheValue);
				}

				@Override
				public V putIfAbsent(K key, V value) {
					return EhcacheLFP.this.putIfAbsent(key, value);
				}

				@Override
				public Set<java.util.Map.Entry<K, V>> entrySet() {
					return new AbstractSet<Map.Entry<K, V>>() {

						@Override
						public java.util.Iterator<Entry<K, V>> iterator() {
							return Streams.of(EhcacheLFP.this.iterator())
									.mapToEntry(v -> v.getKey(), v -> v.getValue())
									.iterator();
						}

						@Override
						public int size() {
							return (int) Math.min(EhcacheLFP.this.estimatedSize(), Integer.MAX_VALUE);
						}

						@Override
						public boolean isEmpty() {
							if (size() > 0)
								return false;
							return !EhcacheLFP.this.iterator().hasNext();
						}

					};
				}
			};
		});
	}

	public Registration acquireKeyLocks(Iterable<? extends K> keys) {
		var keyIter = Streams.of(keys).distinct().iterator();
		if (!keyIter.hasNext())
			return Registration.complete();
		Registration registration = Registration.complete();
		try {
			while (keyIter.hasNext()) {
				var key = keyIter.next();
				var lock = getKeyLock().writeLock(key);
				lock.lock();
				registration = registration.with(lock::unlock);
			}
		} catch (Throwable t) {
			registration.close();
			throw t;
		}
		return registration;
	}

	protected KeyLock<K> getKeyLock() {
		return computeAttributeIfAbsent(KEY_LOCK_ATTRIBUTE_KEY, nil -> KeyLock.create());
	}

	@SuppressWarnings("unchecked")
	protected <X, Y> Y computeAttributeIfAbsent(X key, Function<? super X, ? extends Y> mappingFunction) {
		var attributes = attributesReference
				.updateAndGet(v -> Optional.ofNullable(v).orElseGet(ConcurrentHashMap::new));
		return (Y) attributes.computeIfAbsent(key, mappingFunction == null ? null : k -> mappingFunction.apply((X) k));
	}
	/*
	 * Unsupported
	 */

	@Deprecated
	@Override
	public @NonNull CacheStats stats() {
		throw new UnsupportedOperationException();
	}

	@Deprecated
	@Override
	public void cleanUp() {
		throw new UnsupportedOperationException();
	}

	@Deprecated
	@Override
	public @NonNull Policy<K, V> policy() {
		throw new UnsupportedOperationException();
	}

	private static abstract class AbstractConcurrentMap<K, V> extends AbstractMap<K, V>
			implements ConcurrentMap<K, V> {}

	public static void main(String[] args) throws InterruptedException {
		var options = EhcacheOptions.of(Date.class, String.class, "neat1");
		var cache = Caches.newEhcache(options);
		if (true) {
			// cache.cleanUp();
		}
		System.out.println(cache.getStatus());
		Function<Date, String> loader = nil -> {
			System.out.println("loading!");
			return "hello there";
		};
		var value = cache.get(new Date(0), loader);
		System.out.println(value);
		value = cache.get(new Date(0), loader);
		System.out.println(value);
	}

}
