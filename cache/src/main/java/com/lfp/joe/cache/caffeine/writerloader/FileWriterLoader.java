package com.lfp.joe.cache.caffeine.writerloader;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.Duration;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.github.benmanes.caffeine.cache.Cache;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.lock.FileLocks;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingConsumer;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

public abstract class FileWriterLoader<K, V> extends ByteStreamWriterLoader<K, V> {

	private static final Cache<String, Nada> PARENT_FILE_MKDIRS_CACHE = Caches
			.newCaffeineBuilder(-1, Duration.ofSeconds(5), null).build();

	@Override
	protected <X> X accessInputStream(@NonNull K key, ThrowingFunction<InputStream, X, IOException> accessor)
			throws IOException {
		File file = getFile(key);
		if (!file.exists())
			return accessor.apply(null);
		return FileLocks.read(file, (fc, is) -> {
			try (var bis = new BufferedInputStream(is, Utils.Bits.getDefaultBufferSize())) {
				bis.mark(1);
				var empty = bis.read() == -1;
				if (empty)
					return accessor.apply(null);
				else {
					bis.reset();
					return accessor.apply(bis);
				}
			}
		});

	}

	@Override
	protected void accessOutputStream(@NonNull K key, @Nullable V value,
			ThrowingConsumer<OutputStream, IOException> accessor) throws IOException {
		File file = getFile(key);

		if (file == null) {
			accessor.accept(null);
			return;
		}
		var parentFile = file.getParentFile();
		PARENT_FILE_MKDIRS_CACHE.get(parentFile.getAbsolutePath(), nil -> {
			parentFile.mkdirs();
			return Nada.get();
		});
		FileLocks.write(file, (fc, os) -> {
			fc.truncate(0);
			if (value == null)
				accessor.accept(null);
			else
				accessor.accept(os);
		});
	}

	@Override
	protected ThrowingFunction<ThrowingSupplier<V, Exception>, V, Exception> getStorageLockAccessor(@NonNull K key) {
		// handled in file locks
		return null;
	}

	protected abstract File getFile(@NonNull K key) throws IOException;

}
