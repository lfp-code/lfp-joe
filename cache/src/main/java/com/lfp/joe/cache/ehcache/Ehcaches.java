package com.lfp.joe.cache.ehcache;

import java.io.File;
import java.io.Serializable;
import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;

import org.ehcache.CacheManager;
import org.ehcache.PersistentCacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.PooledExecutionServiceConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.core.internal.statistics.DefaultStatisticsService;
import org.ehcache.impl.config.executor.PooledExecutionServiceConfiguration;
import org.ehcache.impl.serialization.PlainJavaSerializer;
import org.ehcache.spi.serialization.Serializer;

import com.google.common.reflect.TypeToken;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.process.ShutdownNotifier;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;

public class Ehcaches {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();

	public static DefaultStatisticsService defaultStatisticsService() {
		return defaultInstance(DefaultStatisticsService.class, () -> {
			return new DefaultStatisticsService();
		});
	}

	public static PooledExecutionServiceConfiguration defaultPooledExecutionServiceConfiguration() {
		return defaultInstance(PooledExecutionServiceConfiguration.class, () -> {
			return PooledExecutionServiceConfigurationBuilder.newPooledExecutionServiceConfigurationBuilder()
					.defaultPool("ehcache_default_pool", 0, Utils.Machine.logicalProcessorCount() * 2)
					.build();
		});

	}

	public static CacheManager defaultCacheManager() {
		return defaultInstance(CacheManager.class, () -> {
			var pathParts = Streams.<Object>empty();
			{// find protected domain
				ProtectionDomain protectionDomain = THIS_CLASS.getProtectionDomain();
				var pathOp = Optional.ofNullable(protectionDomain)
						.map(ProtectionDomain::getCodeSource)
						.map(CodeSource::getLocation)
						.map(URL::getPath);
				pathParts = pathParts.append(pathOp.stream());
			}
			pathParts = pathParts.prepend(THIS_CLASS, "disk-persistence");
			File file;
			if (MachineConfig.isDeveloper()) {
				pathParts = pathParts.append(MachineConfig.getMainClass().stream());
				file = Utils.Files.tempFile(pathParts.toArray());
			} else
				file = Utils.Files.tempFileRoot(pathParts.toArray());
			file.mkdirs();
			CacheManagerBuilder<PersistentCacheManager> cb = CacheManagerBuilder.newCacheManagerBuilder()
					.using(defaultPooledExecutionServiceConfiguration())
					.using(defaultStatisticsService())
					.with(CacheManagerBuilder.persistence(file));
			PersistentCacheManager cacheManager = cb.build();
			ShutdownNotifier.INSTANCE.addListener(cacheManager);
			cacheManager.init();
			return cacheManager;
		});
	}

	public static <K, V> EhcacheLFP<K, V> createCache(CacheManager cacheManager, EhcacheOptions<K, V> options) {
		Objects.requireNonNull(cacheManager);
		Objects.requireNonNull(options);
		var ccBuilder = createCacheConfigurationBuilder(options);
		return EhcacheLFP.from(cacheManager.createCache(options.id(), ccBuilder));
	}

	@SuppressWarnings("unchecked")
	public static <K, V> CacheConfigurationBuilder<K, V> createCacheConfigurationBuilder(EhcacheOptions<K, V> options) {
		Class<K> keyClassType = (Class<K>) Utils.Types.toClass(options.keyTypeToken().getType());
		Class<V> valueClassType = (Class<V>) Utils.Types.toClass(options.valueTypeToken().getType());
		ResourcePoolsBuilder resourcePoolsBuilder = Optional
				.ofNullable(options.resourcePoolsBuilderConfiguration()
						.apply(ResourcePoolsBuilder.newResourcePoolsBuilder()))
				.orElseGet(() -> ResourcePoolsBuilder.heap(Long.MAX_VALUE));
		CacheConfigurationBuilder<K, V> cfgBuilder = CacheConfigurationBuilder
				.<K, V>newCacheConfigurationBuilder(keyClassType, valueClassType, resourcePoolsBuilder);
		cfgBuilder = cfgBuilder
				.withKeySerializer(options.keySerializer().orElseGet(() -> createSerializer(options.keyTypeToken())));
		cfgBuilder = cfgBuilder.withValueSerializer(
				options.valueSerializer().orElseGet(() -> createSerializer(options.valueTypeToken())));
		cfgBuilder = cfgBuilder.withExpiry(options.expiryPolicy());
		cfgBuilder = cfgBuilder.withSizeOfMaxObjectGraph(options.sizeOfMaxObjectGraph())
				.withSizeOfMaxObjectSize(options.sizeOfMaxObjectSize(), options.sizeOfMaxObjectSizeMemoryUnit());
		return cfgBuilder;
	}

	@SuppressWarnings("unchecked")
	protected static <U> Serializer<U> createSerializer(TypeToken<U> typeToken) {
		Class<?> classType = Utils.Types.toClass(typeToken.getType());
		if (Bytes.class.isAssignableFrom(classType))
			return (Serializer<U>) BytesSerializer.get();
		else if (Serializable.class.isAssignableFrom(classType))
			return new PlainJavaSerializer<U>(CoreReflections.getDefaultClassLoader());
		else
			return new GsonSerializer<U>(typeToken, Serials.Gsons.get());
	}

	private static <U> U defaultInstance(Class<U> classType, Supplier<? extends U> loader) {
		return Instances.get(List.of(THIS_CLASS, classType), loader::get);
	}

	public static void main(String[] args) throws InterruptedException, ExecutionException {

	}

}
