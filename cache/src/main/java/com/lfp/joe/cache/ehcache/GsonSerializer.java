package com.lfp.joe.cache.ehcache;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Objects;

import org.apache.commons.lang3.Validate;
import org.ehcache.spi.serialization.Serializer;
import org.ehcache.spi.serialization.SerializerException;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.bytes.ByteBufferBackedInputStream;

import at.favre.lib.bytes.Bytes;

public class GsonSerializer<X> implements Serializer<X> {
	private final Gson gson;
	private final TypeToken<X> typeToken;

	public GsonSerializer(TypeToken<X> typeToken) {
		this(typeToken, null);
	}

	public GsonSerializer(TypeToken<X> typeToken, Gson gson) {
		this.gson = gson != null ? gson : Serials.Gsons.get();
		this.typeToken = Validate.notNull(typeToken);
	}

	@Override
	public ByteBuffer serialize(X object) throws SerializerException {
		Bytes bytes = Serials.Gsons.toBytes(gson, object);
		return bytes.buffer();
	}

	@Override
	public X read(ByteBuffer binary) throws ClassNotFoundException, SerializerException {
		X object;
		try (var inputStream = new ByteBufferBackedInputStream(binary)) {
			object = Serials.Gsons.fromStream(gson, inputStream, typeToken);
		} catch (IOException e) {
			throw new SerializerException(e);
		}
		return object;
	}

	@Override
	public boolean equals(X object, ByteBuffer binary) throws ClassNotFoundException, SerializerException {
		return Objects.equals(object, read(binary));
	}

}
