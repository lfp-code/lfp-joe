package com.lfp.joe.cache.ehcache;

import java.time.Duration;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import org.ehcache.core.spi.store.Store.ValueHolder;
import org.ehcache.expiry.ExpiryPolicy;
import org.immutables.value.Value;

import com.lfp.joe.cache.caffeine.Caffeines;
import com.lfp.joe.cache.ehcache.ExpiryPolicyLFP.TTLRequest;
import com.lfp.joe.cache.ehcache.ExpiryPolicyLFP.TTLRequest.Action;
import com.lfp.joe.cache.ehcache.ImmutableExpiryPolicyLFP.FixedTTLRequest;
import com.lfp.joe.cache.ehcache.ImmutableExpiryPolicyLFP.LoadingTTLRequest;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.Durations;
import com.lfp.joe.stream.Streams;

import one.util.streamex.StreamEx;

@ValueLFP.Style
@Value.Enclosing
public interface ExpiryPolicyLFP<K, V>
		extends ExpiryPolicy<K, V>, Function<TTLRequest<K, ? extends V>, Duration> {

	public static <K, V> ExpiryPolicyLFP<K, V> empty() {
		return of((Duration) null);
	}

	public static <K, V> ExpiryPolicyLFP<K, V> of(Duration ttl) {
		return of(ttl == null ? null : nil -> ttl);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <K, V> ExpiryPolicyLFP<K, V> of(
			Function<TTLRequest<K, ? extends V>, Duration> ttlFunction) {
		if (ttlFunction == null)
			return Const.EMPTY;
		if (ttlFunction instanceof ExpiryPolicyLFP)
			return (ExpiryPolicyLFP) ttlFunction;
		return ttlFunction::apply;
	}

	@Override
	default Duration getExpiryForCreation(K key, V value) {
		var request = FixedTTLRequest.of(Action.CREATION, key, value);
		return tryApply(request, ExpiryPolicy.INFINITE).orElse(ExpiryPolicy.INFINITE);
	}

	@Override
	default Duration getExpiryForAccess(K key, Supplier<? extends V> valueLoader) {
		var request = LoadingTTLRequest.<K, V>of(Action.ACCESS, key, valueLoader);
		return tryApply(request, ExpiryPolicy.INFINITE).orElse(null);
	}

	@Override
	default Duration getExpiryForUpdate(K key, Supplier<? extends V> oldValueLoader, V newValue) {
		var request = LoadingTTLRequest.of(Action.UPDATE, key, () -> newValue).withPreviousValueLoader(oldValueLoader);
		return tryApply(request, ExpiryPolicy.INFINITE).orElse(null);
	}

	default ExpiryPolicyLFP<K, V> append(
			Function<TTLRequest<K, ? extends V>, Duration> ttlFunction) {
		var ep = ExpiryPolicyLFP.of(ttlFunction);
		if (Const.EMPTY.equals(ep))
			return this;
		if (Const.EMPTY.equals(this))
			return ep;
		return request -> {
			var ttlOp = Streams.of(this, ep)
					.map(v -> v.apply(request))
					.mapPartial(v -> normalize(v, Durations.max()))
					.takeWhileInclusive(Predicate.not(Durations::isNegativeOrZero))
					.sorted()
					.findFirst();
			return ttlOp.orElse(null);
		};
	}

	@SuppressWarnings("unchecked")
	default ExpiryPolicyLFP<K, V> append(Function<TTLRequest<K, ? extends V>, Duration>... ttlFunctions) {
		return Streams.of(ttlFunctions)
				.<ExpiryPolicyLFP<K, V>>map(ExpiryPolicyLFP::of)
				.reduce((f, s) -> f.append(s))
				.orElse(empty());
	}

	private Optional<Duration> tryApply(
			TTLRequest<K, V> request, Duration infinite) {
		return normalize(this.apply(request), infinite);
	}

	private static Optional<Duration> normalize(Duration duration, Duration infinite) {
		Objects.requireNonNull(infinite);
		if (duration == null)
			return Optional.empty();
		if (Durations.isNegativeOrZero(duration))
			return Optional.of(Duration.ZERO);
		if (Streams.of(Durations.max(), ExpiryPolicy.INFINITE, Caffeines.MAX_TTL)
				.append(infinite)
				.distinct()
				.anyMatch(v -> Durations.isGreaterThan(duration, v)))
			return Optional.of(infinite);
		return Optional.of(duration);
	}

	public static interface TTLRequest<K, V> {

		public static enum Action {
			CREATION, ACCESS, UPDATE;
		}

		Action action();

		K key();

		Optional<V> value();

		Optional<V> previousValue();

		Optional<Duration> elapsed();

		Optional<Duration> timeToLive();
	}

	@Value.Immutable
	static abstract class AbstractFixedTTLRequest<K, V> implements TTLRequest<K, V> {

		public static <K, V> FixedTTLRequest<K, V> of(Action action, K key, V value) {
			return FixedTTLRequest.<K, V>builder().action(action).key(key).value(Optional.ofNullable(value)).build();
		}

	}

	@Value.Immutable
	static abstract class AbstractLoadingTTLRequest<K, V> implements TTLRequest<K, V> {

		@Override
		@Value.Parameter
		public abstract Action action();

		@Override
		@Value.Parameter
		public abstract K key();

		@Nullable
		@Value.Parameter
		abstract Supplier<? extends V> valueLoader();

		@Nullable
		abstract Supplier<? extends V> previousValueLoader();

		@Override
		public Optional<V> value() {
			return tryLoad(valueLoader());
		}

		@Override
		public Optional<V> previousValue() {
			return tryLoad(previousValueLoader());
		}

		@Override
		public Optional<Duration> elapsed() {
			var durationOp = valueHolders().map(v -> v.creationTime())
					.filter(v -> v >= 0)
					.map(v -> System.currentTimeMillis() - v)
					.chain(v -> findFirst(v));
			return durationOp;
		}

		@Override
		public Optional<Duration> timeToLive() {
			var durationOp = valueHolders().map(v -> v.expirationTime())
					.filter(v -> !v.equals(ValueHolder.NO_EXPIRE))
					.map(v -> v - System.currentTimeMillis())
					.chain(v -> findFirst(v));
			return durationOp;
		}

		protected StreamEx<ValueHolder<?>> valueHolders() {
			return Streams.of(valueLoader(), previousValueLoader())
					.select(ValueHolder.class)
					.<ValueHolder<?>>map(v -> v)
					.distinct();
		}

		private static Optional<Duration> findFirst(Iterable<Long> millis) {
			return Streams.of(millis)
					.nonNull()
					.map(Duration::ofMillis)
					.mapPartial(v -> normalize(v, Durations.max()))
					.distinct()
					.sorted()
					.findFirst();
		}

		protected static <U> Optional<U> tryLoad(Supplier<? extends U> loader) {
			return Optional.ofNullable(loader).map(Supplier::get);
		}

	}

	static enum Const {
		;

		@SuppressWarnings("rawtypes")
		private static final ExpiryPolicyLFP EMPTY = nil -> null;

	}

}
