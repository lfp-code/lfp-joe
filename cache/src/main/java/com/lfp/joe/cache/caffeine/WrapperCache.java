package com.lfp.joe.cache.caffeine;

import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;

import org.checkerframework.checker.index.qual.NonNegative;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.github.benmanes.caffeine.cache.Policy;
import com.github.benmanes.caffeine.cache.stats.CacheStats;
import com.lfp.joe.utils.Utils;

@SuppressWarnings("unchecked")
public interface WrapperCache<K, WK, V, WV> extends Cache<K, V> {

	Cache<WK, WV> getDelegate();

	WK wrapKey(K key);

	K unwrapKey(WK wrappedKey);

	WV wrapValue(V value);

	V unwrapValue(WV wrappedValue);

	@Override
	default @Nullable V get(@NonNull K key, @NonNull Function<? super K, ? extends V> mappingFunction) {
		Function<? super WK, ? extends WV> mappingFunctionWrapped = mappingFunction == null ? null : wk -> {
			var k = unwrapKey(wk);
			var v = mappingFunction.apply(k);
			return wrapValue(v);
		};
		return unwrapValue(getDelegate().get(wrapKey(key), mappingFunctionWrapped));
	}

	@Override
	default void put(@NonNull K key, @NonNull V value) {
		getDelegate().put(wrapKey(key), wrapValue(value));
	}

	@Override
	default void invalidateAll() {
		getDelegate().invalidateAll();
	}

	@Override
	default @NonNegative long estimatedSize() {
		return getDelegate().estimatedSize();
	}

	@Override
	default @NonNull CacheStats stats() {
		return getDelegate().stats();
	}

	@Override
	default void cleanUp() {
		getDelegate().cleanUp();
	}

	@Override
	default @Nullable V getIfPresent(@NonNull Object key) {
		return unwrapValue(getDelegate().getIfPresent(wrapKey((K) key)));
	}

	@Override
	default void invalidate(@NonNull Object key) {
		getDelegate().invalidate(wrapKey((K) key));
	}

	@Override
	default void putAll(@NonNull Map<? extends @NonNull K, ? extends @NonNull V> map) {
		Map<? extends @NonNull WK, ? extends @NonNull WV> mapWrapped = wrapMap(map);
		getDelegate().putAll(mapWrapped);
	}

	@Override
	default @NonNull Map<@NonNull K, @NonNull V> getAllPresent(@NonNull Iterable<@NonNull ?> keys) {
		var keysWrapped = wrapKeys(keys, this::wrapKey);
		var map = getDelegate().getAllPresent(keysWrapped);
		return unwrapMap(map, this::unwrapKey, this::unwrapValue);
	}

	@Override
	default void invalidateAll(@NonNull Iterable<?> keys) {
		var keysWrapped = wrapKeys(keys, this::wrapKey);
		getDelegate().invalidateAll(keysWrapped);
	}

	@Deprecated
	@Override
	default @NonNull Policy<K, V> policy() {
		throw new UnsupportedOperationException("use getDelegate() to access policy");
	}

	@Deprecated
	@Override
	default @NonNull ConcurrentMap<@NonNull K, @NonNull V> asMap() {
		throw new UnsupportedOperationException("use getDelegate() to access asMap");
	}

	private Map<? extends @NonNull WK, ? extends @NonNull WV> wrapMap(
			@NonNull Map<? extends @NonNull K, ? extends @NonNull V> map) {
		if (map == null)
			return null;
		return Utils.Lots.stream(map).mapKeys(this::wrapKey).mapValues(this::wrapValue).toMap();
	}

	private static <K, WK> Iterable<? extends WK> wrapKeys(@NonNull Iterable<?> keys, Function<K, WK> wrapFunction) {
		if (keys == null)
			return null;
		Iterable<? extends WK> ible = () -> {
			var iter = keys.iterator();
			if (iter == null)
				return null;
			return Utils.Lots.stream(iter).map(k -> (K) k).map(wrapFunction).iterator();
		};
		return ible;
	}

	private static <K, WK, V, WV> @NonNull Map<@NonNull K, @NonNull V> unwrapMap(
			@NonNull Map<@NonNull WK, @NonNull WV> map, Function<WK, K> unwrapKeyFunction,
			Function<WV, V> unwrapValueFunction) {
		if (map == null)
			return null;
		return Utils.Lots.stream(map).mapKeys(unwrapKeyFunction).mapValues(unwrapValueFunction).toMap();
	}

	public static interface Loading<K, WK, V, WV> extends WrapperCache<K, WK, V, WV>, LoadingCache<K, V> {

		@Override
		LoadingCache<WK, WV> getDelegate();

		@Override
		default @Nullable V get(@NonNull K key) {
			return unwrapValue(getDelegate().get(wrapKey(key)));
		}

		@Override
		default void refresh(@NonNull K key) {
			getDelegate().refresh(wrapKey(key));
		}

		@Override
		default @NonNull Map<@NonNull K, @NonNull V> getAll(@NonNull Iterable<? extends @NonNull K> keys) {
			Iterable<? extends WK> keysWrapped = wrapKeys(keys, this::wrapKey);
			return unwrapMap(getDelegate().getAll(keysWrapped), this::unwrapKey, this::unwrapValue);
		}

	}
}
