package com.lfp.joe.cache.ehcache;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Objects;

import org.ehcache.spi.serialization.Serializer;
import org.ehcache.spi.serialization.SerializerException;

import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.bytes.ByteBufferBackedInputStream;

import at.favre.lib.bytes.Bytes;

public class BytesSerializer implements Serializer<Bytes> {

	public static BytesSerializer get() {
		return Instances.get();
	}

	private BytesSerializer() {

	}

	@Override
	public ByteBuffer serialize(Bytes bytes) throws SerializerException {
		if (bytes == null)
			return null;
		return bytes.buffer();
	}

	@Override
	public Bytes read(ByteBuffer binary) throws ClassNotFoundException, SerializerException {
		if (binary == null)
			return null;
		return Utils.Bits.from(binary);
	}

	@Override
	public boolean equals(Bytes bytes, ByteBuffer binary) throws ClassNotFoundException, SerializerException {
		if (bytes == null || binary == null)
			return bytes == null && binary == null;
		if (binary.hasArray()) {
			var barr = binary.array();
			if (bytes.length() != barr.length)
				return false;
			for (int i = 0; i < barr.length; i++)
				if (!Objects.equals(bytes.byteAt(i), barr[i]))
					return false;
			return true;
		}
		try (var is = new ByteBufferBackedInputStream(binary)) {
			var index = -1;
			while (true) {
				var b = is.read();
				if (b == -1)
					break;
				index++;
				if (!Objects.equals(bytes.byteAt(index), (byte) b))
					return false;
			}
			return bytes.length() == (index + 1);
		} catch (IOException e) {
			throw new SerializerException(e);
		}

	}

}
