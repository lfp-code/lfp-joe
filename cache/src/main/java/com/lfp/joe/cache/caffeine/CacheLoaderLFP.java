package com.lfp.joe.cache.caffeine;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.BaseStream;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.threadly.concurrent.SameThreadSubmitterExecutor;
import org.threadly.concurrent.SubmitterExecutor;
import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.wrapper.SubmitterExecutorAdapter;

import com.github.benmanes.caffeine.cache.CacheLoader;
import com.lfp.joe.core.process.future.CSFuture;
import com.lfp.joe.core.process.future.Completion;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;

public interface CacheLoaderLFP<K, V> extends CacheLoader<K, V> {

	@Override
	default @Nullable V load(@NonNull K key) throws Exception {
		var map = loadAll(Arrays.asList(key));
		return Optional.ofNullable(map).map(v -> v.get(key)).orElse(null);
	}

	@Override
	default @NonNull Map<@NonNull K, @NonNull V> loadAll(@NonNull Iterable<? extends @NonNull K> keys)
			throws Exception {
		var mapFuture = asyncLoadAll(keys, SameThreadSubmitterExecutor.instance());
		if (mapFuture == null)
			return null;
		var completion = Completion.join(mapFuture);
		var failure = completion.failure();
		if (failure != null) {
			if (failure instanceof Exception)
				throw (Exception) failure;
			throw new Exception(failure);
		}
		return completion.result();
	}

	@Override
	default @NonNull CompletableFuture<V> asyncLoad(@NonNull K key, @NonNull Executor executor) {
		var mapFuture = asyncLoadAll(Arrays.asList(key), SubmitterExecutorAdapter.adaptExecutor(executor));
		if (mapFuture == null)
			return null;
		Function<Map<K, V>, V> mapper = map -> Optional.ofNullable(map).map(v -> v.get(key)).orElse(null);
		Future<V> future;
		if (mapFuture instanceof CompletableFuture && !(mapFuture instanceof ListenableFuture)) {
			CompletableFuture<Map<K, V>> cMapFuture = (CompletableFuture<Map<K, V>>) mapFuture;
			future = cMapFuture.thenApply(mapper);
		} else
			future = Threads.Futures.asListenable(mapFuture).map(mapper);
		return asCompletableFuture(future);
	}

	@Override
	default @NonNull CompletableFuture<Map<@NonNull K, @NonNull V>> asyncLoadAll(
			@NonNull Iterable<? extends @NonNull K> keys, @NonNull Executor executor) {
		var mapFuture = asyncLoadAll(keys, SubmitterExecutorAdapter.adaptExecutor(executor));
		if (mapFuture == null)
			return null;
		return asCompletableFuture(mapFuture);
	}

	@NonNull
	Future<Map<K, V>> asyncLoadAll(@NonNull Iterable<? extends K> keys, @NonNull SubmitterExecutor executor);

	default CacheLoaderLFP<K, V> append(CacheLoaderLFP<? extends K, ? extends V> cacheLoader) {
		return CacheLoaderLFP.concat(this, cacheLoader);
	}

	default CacheLoaderLFP<K, V> prepend(CacheLoaderLFP<? extends K, ? extends V> cacheLoader) {
		return CacheLoaderLFP.concat(cacheLoader, this);
	}

	default CacheLoaderLFP<K, V> withLoadNotifier(Consumer<Map<K, V>> loadNotifier) {
		return CacheLoaderLFP.concat(Arrays.asList(this), loadNotifier);
	}

	@SafeVarargs
	public static <K, V> CacheLoaderLFP<K, V> concat(CacheLoader<? extends K, ? extends V>... cacheLoaders) {
		return concat(Streams.of(cacheLoaders), null);
	}

	private static <K, V> CacheLoaderLFP<K, V> concat(Iterable<CacheLoader<? extends K, ? extends V>> cacheLoaders,
			Consumer<Map<K, V>> loadNotifier) {
		var cacheLoaderList = Streams.of(cacheLoaders)
				.map(v -> CacheLoaderLFP.<K, V>from(v))
				.filter(Predicate.not(empty()::equals))
				.toList();
		if (cacheLoaderList.isEmpty())
			return empty();
		if (loadNotifier == null && cacheLoaderList.size() == 1)
			return cacheLoaderList.get(0);
		return new CacheLoaderLFP<K, V>() {

			@Override
			public CompletableFuture<V> asyncLoad(@NonNull K key, @NonNull Executor executor) {
				var cacheLoaderIter = cacheLoaderList.iterator();
				var cacheLoader = cacheLoaderIter.next();
				return asCompletableFuture(asyncLoad(cacheLoader, cacheLoaderIter, key, executor));
			}

			private ListenableFuture<V> asyncLoad(CacheLoaderLFP<K, V> cacheLoader,
					Iterator<CacheLoaderLFP<K, V>> cacheLoaderIter, @NonNull K key, @NonNull Executor executor) {
				var valueFuture = cacheLoader.asyncLoad(key, executor);
				if (valueFuture == null) {
					if (cacheLoaderIter.hasNext())
						return asyncLoad(cacheLoaderIter.next(), cacheLoaderIter, key, executor);
					return FutureUtils.immediateResultFuture(null);
				}
				return asListenableFuture(valueFuture).flatMap(value -> {
					if (loadNotifier != null)
						loadNotifier.accept(EntryStreams.of(key, value).toImmutableMap());
					if (value != null || !cacheLoaderIter.hasNext())
						return FutureUtils.immediateResultFuture(value);
					return asyncLoad(cacheLoaderIter.next(), cacheLoaderIter, key, executor);
				});
			}

			@Override
			public @NonNull Future<Map<K, V>> asyncLoadAll(@NonNull Iterable<? extends K> keys,
					@NonNull SubmitterExecutor executor) {
				keys = normalizeKeys(keys);
				var cacheLoaderIter = cacheLoaderList.iterator();
				var cacheLoader = cacheLoaderIter.next();
				return asyncLoadAll(cacheLoader, cacheLoaderIter, keys, executor);
			}

			private ListenableFuture<Map<K, V>> asyncLoadAll(CacheLoaderLFP<K, V> cacheLoader,
					Iterator<CacheLoaderLFP<K, V>> cacheLoaderIter, Iterable<? extends K> keys,
					SubmitterExecutor executor) {
				Future<Map<K, V>> kvMapFuture = cacheLoader.asyncLoadAll(keys, executor);
				if (kvMapFuture == null) {
					if (cacheLoaderIter.hasNext())
						return asyncLoadAll(cacheLoaderIter.next(), cacheLoaderIter, keys, executor);
					return FutureUtils.immediateResultFuture(Collections.emptyMap());
				}
				return asListenableFuture(kvMapFuture).map(kvMap -> {
					return kvMap == null ? Collections.<K, V>emptyMap() : kvMap;
				}).flatMap(kvMap -> {
					if (loadNotifier != null)
						loadNotifier.accept(Collections.unmodifiableMap(kvMap));
					if (!cacheLoaderIter.hasNext())
						return FutureUtils.immediateResultFuture(kvMap);
					Iterable<? extends K> missingKeys = missingKeys(keys, kvMap);
					if (!missingKeys.iterator().hasNext())
						return FutureUtils.immediateResultFuture(kvMap);
					var nextFuture = asyncLoadAll(cacheLoaderIter.next(), cacheLoaderIter, missingKeys, executor);
					return asListenableFuture(nextFuture).map(v -> concat(kvMap, v));
				});
			}

		};
	}

	@SuppressWarnings("unchecked")
	public static <K, V> CacheLoaderLFP<K, V> from(CacheLoader<? extends K, ? extends V> cacheLoader) {
		if (cacheLoader == null)
			return empty();
		if (cacheLoader instanceof CacheLoaderLFP)
			return (CacheLoaderLFP<K, V>) cacheLoader;
		CacheLoader<K, V> delegate = (CacheLoader<K, V>) cacheLoader;
		return new CacheLoaderLFP<K, V>() {

			@Override
			public @NonNull Future<Map<K, V>> asyncLoadAll(@NonNull Iterable<? extends K> keys,
					@NonNull SubmitterExecutor executor) {
				return delegate.asyncLoadAll(keys, executor);
			}

			@Override
			public @Nullable V load(@NonNull K key) throws Exception {
				return delegate.load(key);
			}

			@Override
			public @NonNull Map<@NonNull K, @NonNull V> loadAll(@NonNull Iterable<? extends @NonNull K> keys)
					throws Exception {
				return delegate.loadAll(keys);
			}

			@Override
			public @NonNull CompletableFuture<V> asyncLoad(@NonNull K key, @NonNull Executor executor) {
				return delegate.asyncLoad(key, executor);
			}

			@Override
			public @NonNull CompletableFuture<Map<@NonNull K, @NonNull V>> asyncLoadAll(
					@NonNull Iterable<? extends K> keys, @NonNull Executor executor) {
				return delegate.asyncLoadAll(keys, executor);
			}

			@Override
			public @Nullable V reload(@NonNull K key, @NonNull V oldValue) throws Exception {
				return delegate.reload(key, oldValue);
			}

			@Override
			public @NonNull CompletableFuture<V> asyncReload(@NonNull K key, @NonNull V oldValue,
					@NonNull Executor executor) {
				return delegate.asyncReload(key, oldValue, executor);
			}

		};
	}

	@SuppressWarnings("unchecked")
	public static <K, V> CacheLoaderLFP<K, V> empty() {
		return Const.EMPTY;
	}

	private static <U> CSFuture.Completable<U> asCompletableFuture(Future<U> future) {
		if (future == null)
			return null;
		if (future instanceof CSFuture.Completable)
			return ((CSFuture.Completable<U>) future).withOriginCancellation(true);
		var csFuture = new CSFuture.Completable<U>();
		Threads.Futures.linkCompletion(future, csFuture);
		return csFuture;
	}

	private static <U> ListenableFuture<U> asListenableFuture(Future<U> future) {
		if (future == null)
			return null;
		return Threads.Futures.asListenable(future);
	}

	private static <U> Iterable<? extends U> normalizeKeys(Iterable<? extends U> keys) {
		if (keys == null)
			return Collections.emptyList();
		if (keys instanceof BaseStream) {
			var streamSupplier = Streams.<U>of(keys).chain(Streams.cached());
			return () -> streamSupplier.get().iterator();
		}
		return keys;
	}

	private static <K, V> Iterable<? extends K> missingKeys(Iterable<? extends K> keys,
			Map<? extends K, ? extends V> kvMap) {
		{
			var nkeys = normalizeKeys(keys);
			if (nkeys != keys)
				return missingKeys(nkeys, kvMap);
		}
		if (kvMap == null || kvMap.isEmpty())
			return keys;
		return () -> Streams.<K>of(keys).filter(v -> kvMap.get(v) == null).iterator();
	}

	@SafeVarargs
	private static <K, V> Map<K, V> concat(Map<? extends K, ? extends V>... maps) {
		var keys = Streams.of(maps).nonNull().flatMap(v -> Streams.of(v.keySet())).distinct();
		var kvMap = keys.mapToEntry(key -> {
			return Streams.of(maps).nonNull().map(v -> v.get(key)).nonNull().findFirst().orElse(null);
		}).toCustomMap(LinkedHashMap::new);
		return Collections.unmodifiableMap(kvMap);
	}

	static enum Const {
		;

		@SuppressWarnings("rawtypes")
		private static final CacheLoaderLFP EMPTY = new CacheLoaderLFP() {

			@Override
			public @NonNull Future asyncLoadAll(@NonNull Iterable keys, @NonNull SubmitterExecutor executor) {
				return FutureUtils.immediateResultFuture(Collections.emptyMap());
			}
		};
	}

}
