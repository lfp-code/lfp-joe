package com.lfp.joe.cache.caffeine.writerloader;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.Validate;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.encryptor4j.Encryptor;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.ReadWriteAccessor;
import com.lfp.joe.core.function.Throws.ThrowingBiFunction;
import com.lfp.joe.core.function.Throws.ThrowingConsumer;
import com.lfp.joe.core.function.Throws.ThrowingFunction;

public abstract class ByteStreamWriterLoader<K, V> extends CacheWriterLoader<K, V> {

	private final ReadWriteAccessor<List<ThrowingBiFunction<K, InputStream, InputStream, IOException>>> inputStreamInterceptors = new ReadWriteAccessor<>(
			new ArrayList<>());
	private final ReadWriteAccessor<List<ThrowingBiFunction<K, OutputStream, OutputStream, IOException>>> outputStreamInterceptors = new ReadWriteAccessor<>(
			new ArrayList<>());
	private final MemoizedSupplier<ByteStreamSerializer<K, V>> byteStreamSerializerSupplier = Utils.Functions
			.memoize(() -> {
				var result = createByteStreamSerializer();
				return Objects.requireNonNull(result, "byteStreamSerializer required");
			});

	@Override
	protected V storageGet(@NonNull K key) throws Exception {
		return this.accessInputStream(key, is -> {
			if (is == null)
				return null;
			try (var bufferedInputStream = new BufferedInputStream(is, Utils.Bits.getDefaultBufferSize());) {
				bufferedInputStream.mark(1);
				var empty = bufferedInputStream.read() == -1;
				if (empty)
					return null;
				else {
					bufferedInputStream.reset();
					return storageGet(key, bufferedInputStream);
				}
			}
		});
	}

	@Override
	protected void storageUpdate(@NonNull K key, @Nullable V value, Duration ttl) throws Exception {
		this.accessOutputStream(key, value, os -> {
			if (os == null)
				return;
			storageUpdate(key, value, os);
		});
	}

	public Scrapable addInputStreamInterceptor(
			ThrowingBiFunction<K, InputStream, InputStream, IOException> interceptor) {
		return accessorAdd(this.inputStreamInterceptors, interceptor);
	}

	public Scrapable addOutputStreamInterceptor(
			ThrowingBiFunction<K, OutputStream, OutputStream, IOException> interceptor) {
		return accessorAdd(this.outputStreamInterceptors, interceptor);
	}

	public Scrapable addEncryption(Function<K, Encryptor> encryptorGenerator) {
		Objects.requireNonNull(encryptorGenerator);
		var isScrapable = this.addInputStreamInterceptor((k, is) -> {
			Encryptor encryptor = encryptorGenerator.apply(k);
			if (encryptor == null)
				return is;
			try {
				return encryptor.wrapInputStream(is);
			} catch (GeneralSecurityException e) {
				throw Utils.Exceptions.as(e, IOException.class);
			}
		});
		var osScrapable = this.addOutputStreamInterceptor((k, os) -> {
			Encryptor encryptor = encryptorGenerator.apply(k);
			if (encryptor == null)
				return os;
			try {
				return encryptor.wrapOutputStream(os);
			} catch (GeneralSecurityException e) {
				throw Utils.Exceptions.as(e, IOException.class);
			}
		});
		return Scrapable.create(isScrapable, osScrapable);
	}

	protected V storageGet(@NonNull K key, InputStream inputStream) throws IOException {
		ThrowingFunction<List<ThrowingBiFunction<K, InputStream, InputStream, IOException>>, InputStream, IOException> reader = list -> {
			var result = inputStream;
			if (list.isEmpty())
				return result;
			for (var ic : list)
				result = ic.apply(key, result);
			return result;
		};
		var errorRef = new AtomicReference<Throwable>();
		var interceptedInputStream = inputStreamInterceptors.readAccess(reader.onError(errorRef::set));
		if (errorRef.get() != null)
			throw Utils.Exceptions.as(errorRef.get(), IOException.class);
		return byteStreamSerializerSupplier.get().deserialize(key, interceptedInputStream);
	}

	protected void storageUpdate(@NonNull K key, @Nullable V value, OutputStream outputStream) throws IOException {
		ThrowingFunction<List<ThrowingBiFunction<K, OutputStream, OutputStream, IOException>>, OutputStream, IOException> reader = list -> {
			var result = outputStream;
			if (list.isEmpty())
				return result;
			for (var ic : list)
				result = ic.apply(key, result);
			return result;
		};
		var errorRef = new AtomicReference<Throwable>();
		var interceptedOutputStream = outputStreamInterceptors.readAccess(reader.onError(errorRef::set));
		if (errorRef.get() != null)
			throw Utils.Exceptions.as(errorRef.get(), IOException.class);
		byteStreamSerializerSupplier.get().serialize(key, value, interceptedOutputStream);
	}

	private static <X> Scrapable accessorAdd(ReadWriteAccessor<List<X>> accessor, X element) {
		Validate.notNull(element);
		return accessor.writeAccess(list -> {
			for (var check : list)
				Validate.isTrue(check != element, "element instance exists");
			list.add(element);
			return Scrapable.create(() -> {
				accessor.writeAccess(list2 -> {
					list2.removeIf(check -> check == element);
				});
			});
		});
	}

	protected abstract ByteStreamSerializer<K, V> createByteStreamSerializer();

	protected abstract <X> X accessInputStream(@NonNull K key, ThrowingFunction<InputStream, X, IOException> accessor)
			throws IOException;

	protected abstract void accessOutputStream(@NonNull K key, @Nullable V value,
			ThrowingConsumer<OutputStream, IOException> accessor) throws IOException;

	public static interface ByteStreamSerializer<K, V> {

		V deserialize(@Nonnull K key, InputStream inputStream) throws IOException;

		void serialize(@Nonnull K key, @Nullable V value, OutputStream outputStream) throws IOException;
	}

}
