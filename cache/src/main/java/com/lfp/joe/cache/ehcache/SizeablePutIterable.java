package com.lfp.joe.cache.ehcache;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.URI;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

public class SizeablePutIterable<X> implements Iterable<X>, AutoCloseable, Serializable {

	private List<CompletableFuture<Optional<X>>> data = new ArrayList<>(List.of(new CompletableFuture<>()));

	@Override
	public Iterator<X> iterator() {
		var valueOpStream = IntStream.range(0, Integer.MAX_VALUE).mapToObj(i -> {
			if (i >= data.size())
				return null;
			var valueOp = data.get(i).join();
			if (valueOp == null)
				return null;
			return valueOp;
		});
		valueOpStream = valueOpStream.takeWhile(Objects::nonNull);
		return valueOpStream.map(v -> v.orElse(null)).iterator();
	}

	public void put(X value) {
		synchronized (data) {
			var currentFuture = data.get(data.size() - 1);
			if (currentFuture.isDone())
				throw new IllegalStateException();
			data.add(new CompletableFuture<Optional<X>>());
			currentFuture.complete(Optional.ofNullable(value));
		}
	}

	@Override
	public void close() {
		synchronized (data) {
			var currentFuture = data.get(data.size() - 1);
			if (currentFuture.complete(null) || currentFuture.join() == null)
				return;
			data.add(CompletableFuture.completedFuture(null));
		}
	}

	// serialize

	@SuppressWarnings({ "unchecked" })
	private void readObject(ObjectInputStream objectInputStream) throws ClassNotFoundException, IOException {
		List<SimpleEntry<Boolean, X>> entries = (List<SimpleEntry<Boolean, X>>) objectInputStream.readObject();
		this.data = entries.stream().map(ent -> {
			if (ent == null)
				return new CompletableFuture<Optional<X>>();
			else
				return CompletableFuture.completedFuture(ent.getKey() ? null : Optional.ofNullable(ent.getValue()));
		}).collect(Collectors.toCollection(ArrayList::new));
	}

	private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
		var list = data.stream().map(v -> {
			if (!v.isDone())
				return null;
			var op = v.join();
			if (op == null)
				return new SimpleEntry<Boolean, X>(true, null);
			return new SimpleEntry<Boolean, X>(false, op.orElse(null));
		}).collect(Collectors.toList());
		objectOutputStream.writeObject(list);
	}

	public static void main(String[] args) {
		var ible = new SizeablePutIterable<URI>();
		for (int i = 0; i < 10; i++)
			ible.put(URI.create("http://" + i + ".com"));
		for (int i = 0; i < 10; i++)
			System.out.println(Streams.of(ible).skip(i).findFirst().get());
		var bits = Utils.Bits.serialize(ible);
		ible = Utils.Bits.deserialize(bits);
		for (int i = 0; i < 10; i++)
			ible.put(URI.create("http://" + i + ".net"));
		for (int i = 0; i < 20; i++)
			System.out.println(Streams.of(ible).skip(i).findFirst().get());
	}

}
