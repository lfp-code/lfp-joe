package com.lfp.joe.cache.ehcache;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;

import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;
import org.ehcache.config.units.MemoryUnit;
import org.ehcache.expiry.ExpiryPolicy;
import org.ehcache.spi.serialization.Serializer;
import org.immutables.value.Value;

import com.github.jknack.handlebars.internal.lang3.Validate;
import com.google.common.reflect.TypeToken;
import com.lfp.joe.cache.Expiry;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.KeyGenerator;

@ValueLFP.Style
@Value.Immutable
public abstract class AbstractEhcacheOptions<K, V> {

	private static final AtomicLong ID_COUNTER = new AtomicLong(Utils.Crypto.getSecureRandom().nextLong());

	@Value.Derived
	public String id() {
		Validate.isTrue(idParts().stream().anyMatch(Objects::nonNull), "invalid id:%s", idParts());
		return KeyGenerator.apply(idParts().toArray());
	}

	@Value.Default
	public List<Object> idParts() {
		var id = ID_COUNTER.updateAndGet(v -> {
			if (v == Long.MAX_VALUE)
				return Long.MIN_VALUE;
			return v + 1;
		});
		return List.of(EhcacheOptions.class, id);
	}

	public abstract TypeToken<K> keyTypeToken();

	public abstract TypeToken<V> valueTypeToken();

	@Value.Default
	public ExpiryPolicy<? super K, ? super V> expiryPolicy() {
		return Expiry.builder().build();
	}

	@Value.Default
	public Function<ResourcePoolsBuilder, ResourcePoolsBuilder> resourcePoolsBuilderConfiguration() {
		return rpb -> rpb.heap(Long.MAX_VALUE, EntryUnit.ENTRIES);
	}

	@Value.Default
	public long sizeOfMaxObjectGraph() {
		return Long.MAX_VALUE;
	}

	@Value.Default
	public long sizeOfMaxObjectSize() {
		return Long.MAX_VALUE;
	}

	@Value.Default
	public MemoryUnit sizeOfMaxObjectSizeMemoryUnit() {
		return MemoryUnit.B;
	}

	@Value.Default
	public boolean clearOnClose() {
		return false;
	}

	public abstract Optional<Serializer<K>> keySerializer();

	public abstract Optional<Serializer<V>> valueSerializer();

	public static <K, V> EhcacheOptions<K, V> of(Class<K> keyType, Class<V> valueType, Object... idParts) {
		return of(TypeToken.of(keyType), TypeToken.of(valueType), idParts);
	}

	public static <K, V> EhcacheOptions<K, V> of(Class<K> keyType, TypeToken<V> valueTypeToken, Object... idParts) {
		return of(TypeToken.of(keyType), valueTypeToken, idParts);
	}

	public static <K, V> EhcacheOptions<K, V> of(TypeToken<K> keyTypeToken, TypeToken<V> valueTypeToken,
			Object... idParts) {
		return builder(keyTypeToken, valueTypeToken, idParts).build();
	}

	public static <K, V> EhcacheOptions.Builder<K, V> builder(TypeToken<K> keyTypeToken, TypeToken<V> valueTypeToken,
			Object... idParts) {
		var builder = EhcacheOptions.<K, V>builder().keyTypeToken(keyTypeToken).valueTypeToken(valueTypeToken);
		var idIter = Streams.of(idParts).nonNull().iterator();
		if (idIter.hasNext())
			builder.addAllIdParts(() -> idIter);
		return builder;
	}
}
