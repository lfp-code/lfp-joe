package com.lfp.joe.cache.caffeine;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.slf4j.Logger;

import com.github.benmanes.caffeine.cache.CacheLoader;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.utils.Utils;

public class CacheLoaderLogging<K, V> implements CacheLoader<K, V> {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger DEFAULT_LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static <K, V> CacheLoaderLogging<K, V> create(CacheLoader<K, V> delegate) {
		return create(delegate, false);
	}

	public static <K, V> CacheLoaderLogging<K, V> create(CacheLoader<K, V> delegate, boolean suppressErrors) {
		return new CacheLoaderLogging<>(delegate, suppressErrors,
				org.slf4j.LoggerFactory.getLogger(CoreReflections.getCallingClass(THIS_CLASS)));
	}


	private final CacheLoader<K, V> delegate;
	private boolean suppressErrors;
	private final Logger logger;

	public CacheLoaderLogging(CacheLoader<K, V> delegate, boolean suppressErrors, org.slf4j.Logger logger) {
		this.delegate = Objects.requireNonNull(delegate);
		this.suppressErrors = suppressErrors;
		this.logger = logger;
	}

	@Override
	public @Nullable V load(@NonNull K key) throws Exception {
		return logError(() -> delegate.load(key));
	}

	@Override
	public @NonNull Map<@NonNull K, @NonNull V> loadAll(@NonNull Iterable<? extends @NonNull K> keys) throws Exception {
		return logError(() -> delegate.loadAll(keys));
	}

	@Override
	public @NonNull CompletableFuture<V> asyncLoad(@NonNull K key, @NonNull Executor executor) {
		return logErrorUnchecked(() -> delegate.asyncLoad(key, executor));
	}

	@Override
	public @NonNull CompletableFuture<Map<@NonNull K, @NonNull V>> asyncLoadAll(@NonNull Iterable<? extends K> keys,
			@NonNull Executor executor) {
		return logErrorUnchecked(() -> delegate.asyncLoadAll(keys, executor));
	}

	@Override
	public @Nullable V reload(@NonNull K key, @NonNull V oldValue) throws Exception {
		return logError(() -> delegate.reload(key, oldValue));
	}

	@Override
	public @NonNull CompletableFuture<V> asyncReload(@NonNull K key, @NonNull V oldValue, @NonNull Executor executor) {
		return logErrorUnchecked(() -> delegate.asyncReload(key, oldValue, executor));
	}

	protected <T extends Throwable, X> X logErrorUnchecked(Callable<X> delegateGetter) {
		return Utils.Functions.unchecked(() -> logError(delegateGetter));
	}

	protected <T extends Throwable, X> X logError(Callable<X> delegateGetter) throws Exception {
		try {
			return delegateGetter.call();
		} catch (Exception e) {
			var loggerForError = logger != null ? logger : DEFAULT_LOGGER;
			loggerForError.warn("error during cache load", e);
			if (suppressErrors)
				return null;
			throw e;
		}
	}

}
