package com.lfp.joe.string.generators.config;

import java.net.URI;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.converter.URIConverter;

public interface WordGeneratorConfig extends Config{

	@ConverterClass(URIConverter.class)
	@DefaultValue("https://raw.githubusercontent.com/first20hours/google-10000-english/master/google-10000-english-usa-no-swears.txt")
	URI uri();
}
