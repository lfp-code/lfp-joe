package com.lfp.joe.string.generators;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.github.benmanes.caffeine.cache.LoadingCache;
import org.apache.commons.lang3.Validate;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.cache.StatValue;
import com.lfp.joe.cache.caffeine.writerloader.ByteStreamSerializers;
import com.lfp.joe.cache.caffeine.writerloader.FileWriterLoader;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

public interface StringGenerator extends Supplier<StreamEx<String>> {

	int size();

	default String random() {
		return random(-1).get();
	}

	default Optional<String> random(int minLength) {
		Validate.isTrue(minLength == -1 || minLength > 0, "invalid minLength:%s", minLength);
		var stream = get();
		if (minLength == -1 || minLength == 1)
			return stream.skip(Utils.Crypto.getRandomInclusiveSecure(0, size() - 1)).findFirst();
		var list = stream.filter(v -> Utils.Strings.length(v) >= minLength).toList();
		if (list.isEmpty())
			return Optional.empty();
		if (list.size() == 1)
			return Optional.of(list.get(0));
		var result = list.get(Utils.Crypto.getRandomInclusiveSecure(0, list.size() - 1));
		return Optional.of(result);
	}

	public static abstract class Abs implements StringGenerator {
		private static final Class<?> THIS_CLASS = new Object() {
		}.getClass().getEnclosingClass();
		private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
		private static final int VERSION = 0;
		private final LoadingCache<Nada, StatValue<List<String>>> cache;

		public Abs(Duration expireAfterWriteDisk, Duration expireAfterAccessMemory, Object cacheNamePart,
				Object... cacheNameParts) {
			var cacheNamePartStream = Utils.Lots.stream(cacheNameParts).prepend(THIS_CLASS, VERSION, cacheNamePart);
			var file = Utils.Files.tempFile(cacheNamePartStream.toArray(Object.class));
			this.cache = new FileWriterLoader<Nada, StatValue<List<String>>>() {

				private static final int VERSION = 1;

				@Override
				protected File getFile(@NonNull Nada key) throws IOException {
					return file;
				}

				@Override
				protected ByteStreamSerializer<Nada, StatValue<List<String>>> createByteStreamSerializer() {
					return ByteStreamSerializers.stringList();
				}

				@Override
				protected StatValue<List<String>> loadFresh(@NonNull Nada key) throws Exception {
					var lineStream = Utils.Lots.stream(Abs.this.loadFresh());
					lineStream = lineStream.map(Utils.Strings::trimToNull);
					lineStream = lineStream.nonNull().distinct();
					return StatValue.build(lineStream.toImmutableList());
				}

				@Override
				protected @Nullable Duration getStorageTTL(@NonNull Nada key, StatValue<List<String>> result) {
					return expireAfterWriteDisk;
				}
			}.buildCache(Caches.newCaffeineBuilder(-1, null, expireAfterAccessMemory));
		}

		public int size() {
			return Optional.ofNullable(this.cache.get(Nada.get())).map(v -> v.getValue()).map(v -> v.size()).orElse(0);
		}

		public StreamEx<String> get() {
			return Optional.ofNullable(this.cache.get(Nada.get())).map(v -> v.getValue()).map(Utils.Lots::stream)
					.orElse(StreamEx.empty());
		}

		protected abstract Iterable<String> loadFresh() throws IOException;
	}
}
