package com.lfp.joe.string.generators;

import java.io.IOException;
import java.time.Duration;

import com.lfp.joe.core.config.DevLogger;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.string.generators.config.WordGeneratorConfig;
import com.lfp.joe.utils.Utils;

public class WordGenerator extends StringGenerator.Abs {

	public static final WordGenerator INSTANCE = new WordGenerator();

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	private WordGenerator() {
		super(Duration.ofDays(7), Duration.ofSeconds(1), THIS_CLASS);
	}

	@Override
	protected Iterable<String> loadFresh() throws IOException {
		var cfg = Configs.get(WordGeneratorConfig.class);
		var request = HttpClients.getDefault().get(cfg.uri().toString());
		try (var is = request.ensureSuccess().asStream().getBody();) {
			return Utils.Strings.streamLines(is).toList();
		}
	}

	public static void main(String[] args) {
		var dv = DevLogger.create().enable();
		var total = WordGenerator.INSTANCE.get().count();
		dv.log("load complete. total:{}", total);
		for (int i = 0; i < 25; i++) {
			var word = WordGenerator.INSTANCE.random(4).get();
			System.out.println(word);
		}
	}

}
