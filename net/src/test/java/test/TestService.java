package test;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import com.lfp.joe.core.function.Durations;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.converter.MultimapConverter;
import com.lfp.joe.net.http.ServiceConfig;

public interface TestService extends ServiceConfig {

	@Override
	@ConverterClass(MultimapConverter.class)
	@DefaultValue("key1,kv11,key1,kv12,key1,kv13,key2,kv21")
	Map<String, List<String>> headersDefault();

	public static void main(String[] args) throws InterruptedException {
		var cfg = Configs.get(TestService.class);
		var passes = 10_000;
		Double avg = IntStream.range(0, passes).mapToLong(index -> {
			var startedAt = System.nanoTime();
			cfg.hash();
			return System.nanoTime() - startedAt;
		}).skip(passes - 1).average().getAsDouble();
		System.out.println(Durations.toMillis(Duration.ofNanos(avg.longValue())));
	}
}
