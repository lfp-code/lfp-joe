package test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import com.github.mizosoft.methanol.Methanol;
import com.github.mizosoft.methanol.Methanol.Interceptor;
import com.github.mizosoft.methanol.MutableRequest;
import com.lfp.joe.net.auth.AuthenticatorLFP;
import com.lfp.joe.net.config.NetConfig;
import com.lfp.joe.net.http.headers.UserAgents;

public class ProxyMenthanolTest {

	public static void main(String[] args) throws IOException, InterruptedException {
		NetConfig.init();
		AuthenticatorLFP.getDefault().add(ac -> {
			return null;
		});
		var proxyURI = URI.create("http://user:MJ7XgJIb89s68AVeN6rwDLDd3F4i@traefik.iplasso.com:3128");
		var builder = Methanol.newBuilder().userAgent(UserAgents.FIREFOX_LATEST.get()) // Custom User-Agent
				.requestTimeout(Duration.ofSeconds(20)) // Default request timeout
				.readTimeout(Duration.ofSeconds(5)) // Timeout for single reads
				.autoAcceptEncoding(true).proxy(new ProxySelector() {

					@Override
					public List<java.net.Proxy> select(URI uri) {
						var proxy = new java.net.Proxy(java.net.Proxy.Type.HTTP,
								new InetSocketAddress(proxyURI.getHost(), proxyURI.getPort()));
						return List.of(proxy);
					}

					@Override
					public void connectFailed(URI uri, SocketAddress sa, IOException ioe) {
						ioe.printStackTrace();
					}
				});
		builder = builder.interceptor(new Interceptor() {

			@Override
			public <T> HttpResponse<T> intercept(HttpRequest request, Chain<T> chain)
					throws IOException, InterruptedException {
				return chain.forward(intercept(request));
			}

			@Override
			public <T> CompletableFuture<HttpResponse<T>> interceptAsync(HttpRequest request, Chain<T> chain) {
				return chain.forwardAsync(intercept(request));
			}

			private HttpRequest intercept(HttpRequest request) {
				var userInfo = proxyURI.getUserInfo();
				var headerValue = "Basic "
						+ Base64.getEncoder().encodeToString(userInfo.getBytes(StandardCharsets.ISO_8859_1));
				request = MutableRequest.copyOf(request).setHeader("Proxy-Authorization", headerValue);
				return request;
			}
		});
		var client = builder.build();
		var response = client.send(MutableRequest.GET("https://api.ipify.org"), BodyHandlers.ofString());
		System.out.println(response.body());
	}
}
