package test;

import java.net.URI;
import java.util.List;
import java.util.Map;

import com.lfp.joe.net.http.oauth.OauthServiceConfig;
import com.lfp.joe.utils.crypto.Hashable;

public class TestServiceImpl implements OauthServiceConfig {

	@Override
	public int hashCode() {
		return Hashable.hashCode(this);
	}

	@Override
	public boolean equals(Object other) {
		return Hashable.equals(this, other);
	}

	@Override
	public URI uri() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public URI proxyURI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, List<String>> headersDefault() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String audience() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String clientId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String clientSecret() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String grantType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String scope() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String username() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String password() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public URI oauthURI() {
		return URI.create("http://cool.net");
	}

	public static class TestServiceImpl2 extends TestServiceImpl {

	}

	public static void main(String[] args) {
		TestServiceImpl2 test = new TestServiceImpl2();
		System.out.println(test.uuid());
	}
}
