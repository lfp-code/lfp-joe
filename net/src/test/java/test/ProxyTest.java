package test;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpResponse.BodyHandlers;

import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.net.http.request.HttpRequests;
import com.lfp.joe.net.proxy.Proxy;

public class ProxyTest {

	public static void main(String[] args) throws IOException, InterruptedException {
		System.out.println(IPs.getIPAddress());
		var proxyURI = URI.create("http://user:MJ7XgJIb89s68AVeN6rwDLDd3F4i@traefik.iplasso.com:3128");
		var proxy = Proxy.builder(proxyURI).build();
		var client = HttpClients.newClient(proxy);
		var response = client.send(HttpRequests.request().uri(URI.create("https://api.ipify.org")),
				BodyHandlers.ofString());
		System.out.println(response.body());
	}
}
