package com.lfp.joe.net.dns;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.utils.Utils;

public class DNSToIPAddressFunction implements Function<String, Optional<String>> {

	private LoadingCache<String, Optional<String>> cache;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public DNSToIPAddressFunction(
			Function<Caffeine<String, Optional<String>>, Caffeine<String, Optional<String>>> cacheBuilderConfig) {
		Caffeine<String, Optional<String>> cb = (Caffeine) Caffeine.newBuilder();
		if (cacheBuilderConfig != null)
			cb = Objects.requireNonNull(cacheBuilderConfig.apply(cb));
		this.cache = cb.build(k -> {
			try {
				var ip = DNSs.getIPAdress(k);
				return Optional.ofNullable(ip);
			} catch (Exception e) {
				// suppress
			}
			return Optional.empty();
		});
	}

	@Override
	public Optional<String> apply(String hostnameOrIp) {
		if (Utils.Strings.isBlank(hostnameOrIp))
			return Optional.empty();
		if (IPs.isValidIpAddress(hostnameOrIp))
			return Optional.of(hostnameOrIp);
		return cache.get(hostnameOrIp);
	}

}
