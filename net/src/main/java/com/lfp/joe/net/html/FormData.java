package com.lfp.joe.net.html;

import java.net.URI;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.lang3.Validate;
import org.jsoup.Connection.Method;

import com.lfp.joe.utils.Utils;

import one.util.streamex.EntryStream;

public class FormData {
	private final Map<String, LinkedHashSet<String>> data = new LinkedHashMap<>();
	private final Method method;
	private final URI actionURI;

	public FormData(Method method, URI actionURI) {
		this.method = Objects.requireNonNull(method);
		this.actionURI = Objects.requireNonNull(actionURI);
	}

	public Method getMethod() {
		return method;
	}

	public URI getActionURI() {
		return actionURI;
	}

	public boolean add(String name, String value) {
		validate(name, value);
		return this.data.computeIfAbsent(name, nil -> new LinkedHashSet<>()).add(value);
	}

	public boolean set(String name, String value) {
		remove(name);
		return add(name, value);
	}

	public boolean clear() {
		boolean result = !this.data.isEmpty();
		this.data.clear();
		return result;
	}

	public boolean remove(String name) {
		validate(name);
		AtomicBoolean result = new AtomicBoolean();
		this.data.compute(name, (k, v) -> {
			if (v == null)
				return null;
			if (v.size() > 0)
				result.set(true);
			v.clear();
			return null;
		});
		return result.get();
	}

	public boolean remove(String name, String value) {
		validate(name, value);
		AtomicBoolean result = new AtomicBoolean();
		this.data.compute(name, (k, v) -> {
			if (v == null)
				return null;
			result.set(v.remove(value));
			if (v.isEmpty())
				return null;
			return v;
		});
		return result.get();
	}

	@Override
	public String toString() {
		return "FormData [data=" + data + ", method=" + method + ", actionURI=" + actionURI + "]";
	}

	public EntryStream<String, String> stream() {
		return Utils.Lots.streamMultimap(this.data);
	}

	private static void validate(String name, String value) {
		validate(name);
		Objects.requireNonNull(value);
	}

	private static void validate(String name) {
		Validate.isTrue(Utils.Strings.isNotBlank(name), "non blank name required");

	}

}
