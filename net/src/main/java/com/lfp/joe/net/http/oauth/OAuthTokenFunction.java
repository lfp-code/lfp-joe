package com.lfp.joe.net.http.oauth;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.commons.lang3.Validate;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.cache.StatValue;
import com.lfp.joe.cache.caffeine.writerloader.ByteStreamSerializers;
import com.lfp.joe.cache.caffeine.writerloader.FileWriterLoader;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.encryption.Encryptions;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.headers.UserAgents;
import com.lfp.joe.net.http.request.HttpRequests;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;

public class OAuthTokenFunction implements ThrowingFunction<OauthRequest, OAuthTokenResponse, IOException> {
	protected static final float MAX_LEASE_USAGE = .95f;

	@Override
	public OAuthTokenResponse apply(OauthRequest oauthRequest) throws IOException {
		var oauthRequestImpl = OauthRequestImpl.build(oauthRequest);
		var request = HttpRequests.request();
		{
			Bytes requestBody;
			{
				var oauthRequestBody = oauthRequestImpl.toBuilder().oauthURI(null).build();
				if (MachineConfig.isDeveloper()) {
					var json = Serials.Gsons.get().toJson(oauthRequestBody);
					requestBody = Utils.Bits.from(json);
				} else
					requestBody = Serials.Gsons.toBytes(oauthRequestBody);
			}
			request.uri(oauthRequestImpl.oauthURI());
			request.method("POST", BodyPublishers.ofByteArray(requestBody.array()));
			request.header(Headers.CONTENT_TYPE, "application/json");
			request.header(Headers.USER_AGENT, UserAgents.CURL.get());
		}
		HttpResponse<InputStream> response = null;
		InputStream inputStream = null;
		try {
			response = HttpClients.send(request);
			inputStream = response.body();
			StatusCodes.validate(response.statusCode(), 2);
			return Serials.Gsons.fromStream(inputStream, OAuthTokenResponse.class);
		} catch (Exception e) {
			Map<String, Object> msgs = new LinkedHashMap<>();
			msgs.put("username", oauthRequestImpl.username());
			msgs.put("authEndpointURI", oauthRequestImpl.oauthURI());
			if (response != null) {
				msgs.put("statusCode", response.statusCode());
				var headerMap = HeaderMap.of(response.headers());
				var location = headerMap.firstValue(Headers.LOCATION, true).orElse(null);
				if (location != null)
					msgs.put("locationHeader", location);
			}
			if (MachineConfig.isDeveloper() && inputStream != null) {
				String responseBody = Utils.Functions.catching(inputStream, v -> {
					return Utils.Bits.from(v).encodeCharset(Utils.Bits.getDefaultCharset());
				}, t -> null);
				if (Utils.Strings.isNotBlank(responseBody))
					msgs.put("responseBody", responseBody);

			}
			msgs.put("error", e.getMessage());
			String append = Utils.Lots.stream(msgs)
					.nonNullValues()
					.mapValues(Object::toString)
					.map(ent -> String.format("%s:%s", ent.getKey(), ent.getValue()))
					.joining(" ");
			String msg = String.format("error during outh flow. " + append);
			throw new IOException(msg, e);
		} finally {
			if (inputStream != null)
				inputStream.close();
		}
	}

	public static class Cached extends OAuthTokenFunction {

		private static final long MEMORY_CACHE_MAXIMUM_SIZE_DEFAULT = 50_000;
		private static final Duration MEMORY_CACHE_EXPIRE_AFTER_ACCESS_DEFAULT = Duration.ofSeconds(30);

		private final MemoizedSupplier<LoadingCache<OauthRequestImpl, StatValue<OAuthTokenResponse>>> cacheSupplier;

		public Cached() {
			this(MEMORY_CACHE_MAXIMUM_SIZE_DEFAULT, MEMORY_CACHE_EXPIRE_AFTER_ACCESS_DEFAULT);
		}

		public Cached(long memoryMaximumSize, Duration memoryExpireAfterAccess) {
			Validate.isTrue(memoryMaximumSize >= -1, "invalid memoryMaximumSize:%s", memoryMaximumSize);
			Caffeine<OauthRequestImpl, StatValue<OAuthTokenResponse>> cacheBuilder = Caches.newCaffeineBuilder();
			if (memoryMaximumSize != -1)
				cacheBuilder.maximumSize(memoryMaximumSize);
			cacheBuilder.expireAfter(new OAuthTokenExpiry<>(MAX_LEASE_USAGE, memoryExpireAfterAccess));
			this.cacheSupplier = Utils.Functions.memoize(() -> buildCache(cacheBuilder));

		}

		protected LoadingCache<OauthRequestImpl, StatValue<OAuthTokenResponse>> buildCache(
				Caffeine<OauthRequestImpl, StatValue<OAuthTokenResponse>> cacheBuilder) {
			return cacheBuilder.build(v -> StatValue.build(loadFresh(v)));
		}

		@Override
		public OAuthTokenResponse apply(OauthRequest oauthRequest) throws IOException {
			var oauthRequestImpl = OauthRequestImpl.build(oauthRequest);
			StatValue<OAuthTokenResponse> statValue;
			try {
				statValue = this.cacheSupplier.get().get(oauthRequestImpl);
			} catch (Exception e) {
				Throwable t = Utils.Exceptions.streamCauses(e)
						.filter(v -> v instanceof IOException)
						.findFirst()
						.orElse(e);
				throw Utils.Exceptions.as(t, IOException.class);
			}
			if (statValue == null || statValue.getValue() == null || statValue.getCreatedAt() == null)
				return null;
			Duration ttlDuration = statValue.getValue().timeToLive(statValue.getCreatedAt(), MAX_LEASE_USAGE);
			return statValue.getValue().toBuilder().expiresIn(ttlDuration.getSeconds()).build();
		}

		protected final OAuthTokenResponse loadFresh(OauthRequestImpl oauthRequest) throws IOException {
			var oauthRequestImpl = OauthRequestImpl.build(oauthRequest);
			return super.apply(oauthRequestImpl);
		}

		public void invalidate(OauthRequest oauthRequest) {
			if (oauthRequest == null)
				return;
			var oauthRequestImpl = OauthRequestImpl.build(oauthRequest);
			this.cacheSupplier.get().invalidate(oauthRequestImpl);
		}
	}

	public static class FileCached extends Cached {
		private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
		private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
		private static final long MEMORY_CACHE_MAXIMUM_SIZE_DEFAULT = 25_000;
		private static final Duration MEMORY_CACHE_EXPIRE_AFTER_ACCESS_DEFAULT = Duration.ofSeconds(15);
		private static final int VERSION = 7;
		private final Class<?> callingClass;

		public FileCached() {
			this(MEMORY_CACHE_MAXIMUM_SIZE_DEFAULT, MEMORY_CACHE_EXPIRE_AFTER_ACCESS_DEFAULT);
		}

		public FileCached(long memoryMaximumSize, Duration memoryExpireAfterAccess) {
			super(memoryMaximumSize, memoryExpireAfterAccess);
			this.callingClass = CoreReflections.getCallingClass((i, sf) -> {
				var declarincClass = sf.getDeclaringClass();
				return !OAuthTokenFunction.class.isAssignableFrom(declarincClass);
			});
		}

		@Override
		protected LoadingCache<OauthRequestImpl, StatValue<OAuthTokenResponse>> buildCache(
				Caffeine<OauthRequestImpl, StatValue<OAuthTokenResponse>> cacheBuilder) {
			var writerLoader = new FileWriterLoader<OauthRequestImpl, StatValue<OAuthTokenResponse>>() {

				private final OAuthTokenExpiry<OauthRequestImpl> fileExpiry = new OAuthTokenExpiry<>(MAX_LEASE_USAGE,
						null);

				@Override
				protected File getFile(@NonNull OauthRequestImpl key) throws IOException {
					List<Object> parts = new ArrayList<>();
					parts.add(Configs.get(MachineConfig.class).environmentLevel());
					parts.add(key.oauthURI().toString());
					parts.add(key.hash());
					String hash = Utils.Crypto.hashSHA512(parts.toArray()).encodeHex();
					File file = Utils.Files.tempFile(THIS_CLASS.getName(), callingClass.getName(), "oauth-token-cache",
							VERSION, hash + ".aes");
					return file;
				}

				@Override
				protected ByteStreamSerializer<OauthRequestImpl, StatValue<OAuthTokenResponse>> createByteStreamSerializer() {
					return ByteStreamSerializers.gson();
				}

				@Override
				protected @Nullable StatValue<OAuthTokenResponse> loadFresh(@NonNull OauthRequestImpl key)
						throws IOException {
					OAuthTokenResponse response = FileCached.this.loadFresh(key);
					return response == null ? null : StatValue.build(response);
				}

				@Override
				protected Duration getStorageTTL(@NonNull OauthRequestImpl key,
						@NonNull StatValue<OAuthTokenResponse> result) {
					return fileExpiry.timeToLive(result);
				}

			};
			writerLoader.addEncryption(key -> Encryptions.newAESEncryptor(key.hash().encodeBase64()));
			return writerLoader.buildCache(cacheBuilder);
		}

	}

	public static void main(String[] args) throws IOException, InterruptedException {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter URL:");
		URI uri = URI.create(scanner.nextLine());
		System.out.println("Enter Client Secret:");
		var clientSecret = scanner.nextLine();
		System.out.println("Enter Username:");
		var username = scanner.nextLine();
		System.out.println("Enter Password:");
		var password = scanner.nextLine();
		OauthRequest request = OauthRequestImpl.builder()
				.oauthURI(uri)
				.username(username)
				.password(password)
				.clientSecret(clientSecret)
				.build();
		OAuthTokenFunction.Cached func = new OAuthTokenFunction.FileCached();
		for (int i = 0; i < 100; i++) {
			if (false && i % 7 == 0)
				func.invalidate(request);
			OAuthTokenResponse resp = func.apply(request);
			// System.out.println(Serials.Gsons.get().toJson(resp));
			System.out.println(i);
		}
	}

}
