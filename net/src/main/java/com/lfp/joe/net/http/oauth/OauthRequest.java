package com.lfp.joe.net.http.oauth;

import java.net.URI;
import java.nio.charset.StandardCharsets;

import org.aeonbits.owner.Config.ConverterClass;
import org.aeonbits.owner.Config.DefaultValue;
import org.apache.commons.lang3.Validate;
import org.joda.beans.MetaProperty;

import com.lfp.joe.core.properties.converter.URIConverter;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Hashable;

import at.favre.lib.bytes.Bytes;

public interface OauthRequest extends Hashable {

	public static String DEFAULT_GRANT_TYPE = "password";
	public static String DEFAULT_SCOPE = "openid";

	String audience();

	String clientId();

	String clientSecret();

	@DefaultValue(DEFAULT_GRANT_TYPE)
	String grantType();

	@DefaultValue(DEFAULT_SCOPE)
	String scope();

	String username();

	String password();

	@ConverterClass(URIConverter.class)
	URI oauthURI();

	@Override
	default Bytes hash() {
		var oauthRequestImpl = OauthRequestImpl.build(this);
		var md = Utils.Crypto.getMessageDigestSHA512();
		for (MetaProperty<?> mp : Streams.of(OauthRequestImpl.meta().metaPropertyIterable())
				.sortedBy(MetaProperty::name)) {
			md.update(mp.name().getBytes(StandardCharsets.UTF_8));
			var value = mp.get(oauthRequestImpl);
			if (value != null && !(value instanceof String))
				value = value.toString();
			var bytesOp = Utils.Bits.tryFrom(value);
			Validate.isTrue(bytesOp.isPresent(), "unable to read property:" + mp.name());
			md.update(bytesOp.get().array());
		}
		return Utils.Bits.from(md.digest());
	}

}
