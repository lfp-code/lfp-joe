package com.lfp.joe.net.http.headers;

import java.util.Optional;

import nl.basjes.parse.useragent.UserAgent;
import one.util.streamex.StreamEx;

public interface UserAgentService {

	default Optional<UserAgent> get(String browserName) {
		return stream(browserName).findFirst();
	}

	StreamEx<UserAgent> stream(String browserName);

}
