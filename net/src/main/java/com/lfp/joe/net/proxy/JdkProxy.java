package com.lfp.joe.net.proxy;

import java.util.Objects;

public class JdkProxy extends java.net.Proxy implements ProxyEnabled {

	private final Proxy parent;

	public JdkProxy(Proxy parent) {
		super(Objects.requireNonNull(parent).getType().asJdkType(), parent.getAddress());
		this.parent = parent;
	}

	public Proxy getProxy() {
		return parent;
	}

}
