package com.lfp.joe.net.http.client;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.DeflaterInputStream;
import java.util.zip.GZIPInputStream;
import java.util.Objects;

import org.apache.commons.lang3.Validate;
import org.brotli.dec.BrotliInputStream;

import com.goebl.david.Request.Method;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.headers.UserAgents;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.EntryStream;

public class HttpURLConnections {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static HttpURLConnectionResponse execute(URI requestURI) throws IOException {
		return execute(requestURI, Method.GET);
	}

	public static HttpURLConnectionResponse execute(URI requestURI, Method method) throws IOException {
		Map<String, List<String>> requestHeaders = new LinkedHashMap<>();
		requestHeaders.put(Headers.USER_AGENT, Arrays.asList(UserAgents.CURL.get()));
		return execute(requestURI, method, requestHeaders);
	}

	public static HttpURLConnectionResponse execute(URI requestURI, Method method,
			Map<String, List<String>> requestHeaders) throws IOException {
		return execute(requestURI, method, requestHeaders, null);
	}

	public static HttpURLConnectionResponse execute(URI requestURI, Method method,
			Map<String, List<String>> requestHeaders, InputStream body) throws IOException {
		return execute(requestURI, method, requestHeaders, body, false);
	}

	public static HttpURLConnectionResponse execute(URI requestURI, Method method,
			Map<String, List<String>> requestHeaders, InputStream body, boolean enableCaching) throws IOException {
		Closeable bodyCloser = body == null ? () -> {
		} : new Closeable() {

			private boolean complete = false;

			@Override
			public synchronized void close() throws IOException {
				if (!complete)
					body.close();
				complete = true;
			}
		};
		Objects.requireNonNull(method);
		Objects.requireNonNull(requestURI);
		try {
			HttpURLConnection urlConnection = (HttpURLConnection) requestURI.toURL().openConnection();
			urlConnection.setRequestMethod(method.name());
			EntryStream<String, String> headers = Utils.Lots.stream(
					requestHeaders == null ? null : requestHeaders.keySet(),
					k -> Utils.Lots.stream(requestHeaders.get(k)));
			for (Entry<String, String> ent : headers) {
				String key = ent.getKey();
				String value = ent.getValue();
				urlConnection.addRequestProperty(key, value);
			}
			if (Method.GET.equals(method))
				Validate.isTrue(body == null, "body can't be sent with method:%s", method);
			else if (body != null) {
				urlConnection.setDoOutput(true);
				try (OutputStream os = urlConnection.getOutputStream()) {
					Utils.Bits.copy(body, os);
				} finally {
					bodyCloser.close();
				}
			}
			HttpURLConnectionResponse.Builder respb = HttpURLConnectionResponse.builder();
			respb.statusCode(urlConnection.getResponseCode());
			Map<String, List<String>> responseHeaders = Utils.Lots.stream(urlConnection.getHeaderFields())
					.flatMapValues(v -> Utils.Lots.stream(v)).nonNullKeys().mapValues(v -> v == null ? "" : v)
					.grouping();
			if (HttpURLConnection.HTTP_NO_CONTENT != urlConnection.getResponseCode()) {
				InputStream responseBody = getResponseBody(urlConnection, responseHeaders);
				respb.body(responseBody);
			}
			respb.headers(responseHeaders);
			return respb.build();
		} finally {
			bodyCloser.close();
		}
	}

	public static InputStream getResponseBody(HttpURLConnection httpURLConnection,
			Map<String, List<String>> responseHeaders) throws IOException {
		Objects.requireNonNull(httpURLConnection);
		InputStream responseStream = httpURLConnection.getErrorStream();
		if (responseStream == null && httpURLConnection.getResponseCode() < 400)
			responseStream = httpURLConnection.getInputStream();
		if (responseStream == null)
			responseStream = Utils.Bits.emptyInputStream();
		Entry<String, String> encodingEntry = Utils.Lots
				.stream(responseHeaders == null ? null : responseHeaders.keySet(),
						k -> Utils.Lots.stream(responseHeaders.get(k)))
				.filterKeys(k -> Headers.CONTENT_ENCODING.toString().equalsIgnoreCase(k))
				.filterValues(Utils.Strings::isNotBlank).findFirst().orElse(null);
		if (encodingEntry == null)
			return responseStream;
		String encoding = encodingEntry.getValue();
		final boolean encodingMod;
		if (Utils.Strings.equalsIgnoreCase(encoding, "gzip")) {
			responseStream = new GZIPInputStream(responseStream);
			encodingMod = true;
		} else if (Utils.Strings.equalsIgnoreCase(encoding, "deflate")) {
			responseStream = new DeflaterInputStream(responseStream);
			encodingMod = true;
		} else if (Utils.Strings.equalsIgnoreCase(encoding, "br")) {
			responseStream = new BrotliInputStream(responseStream);
			encodingMod = true;
		} else
			encodingMod = false;
		if (encodingMod)
			responseHeaders.remove(encodingEntry.getKey());
		return responseStream;
	}

}
