package com.lfp.joe.net.email;

public interface InternetAddress {

	String getAddress();

	String getPersonal();

}
