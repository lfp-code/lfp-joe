package com.lfp.joe.net.http.servlet;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiFunction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.Validate;
import org.immutables.value.Value;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.Muto;
import com.lfp.joe.core.function.NumeralFunctions.TriConsumer;
import com.lfp.joe.net.http.servlet.ImmutableSessionAttribute.SessionAttributeAccessor;
import com.lfp.joe.utils.Utils;

@ValueLFP.Style
@Value.Enclosing
public class SessionAttribute<X> {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private final boolean atomic;
	private final String name;

	public SessionAttribute(boolean atomic) {
		this(atomic, THIS_CLASS.getName() + "#" + Utils.Crypto.getSecureRandomString());
	}

	public SessionAttribute(boolean atomic, String name) {
		super();
		this.atomic = atomic;
		this.name = Validate.notBlank(name);
	}

	public String getName() {
		return name;
	}

	public Optional<Muto<X>> getIfSessionExists(HttpServletRequest request) {
		return Optional.ofNullable(request).map(v -> v.getSession(false)).map(this::get);
	}

	public Muto<X> get(HttpServletRequest request) {
		return get(request == null ? null : request.getSession());
	}

	public Muto<X> get(HttpSession session) {
		var blder = SessionAttributeAccessor.<HttpSession>builder();
		blder.readFunction((s, name) -> s.getAttribute(name));
		blder.setFunction((s, name, value) -> s.setAttribute(name, value));
		return getInternal(session, blder.build(), false);
	}

	public Muto<X> get(Map<String, Object> attributes) {
		var blder = SessionAttributeAccessor.<Map<String, Object>>builder();
		blder.readFunction((s, name) -> s.get(name));
		blder.setFunction((s, name, value) -> s.put(name, value));
		return getInternal(attributes, blder.build(), false);
	}

	public <S> Muto<X> get(S session, SessionAttributeAccessor<S> sessionAttributeAccessor) {
		return getInternal(session, sessionAttributeAccessor, false);
	}

	@SuppressWarnings("unchecked")
	protected <S> Muto<X> getInternal(S session, SessionAttributeAccessor<S> sessionAttributeAccessor, boolean locked) {
		Objects.requireNonNull(session);
		Objects.requireNonNull(sessionAttributeAccessor);
		Muto<X> reference = CoreReflections
				.tryCast(sessionAttributeAccessor.readFunction().apply(session, name), Muto.class)
				.orElse(null);
		if (reference != null)
			return reference;
		if (this.atomic && !locked)
			synchronized (session) {
				return getInternal(session, sessionAttributeAccessor, true);
			}
		if (!this.atomic)
			reference = Muto.create();
		else
			reference = new ReferenceAtomic<>();
		sessionAttributeAccessor.setFunction().accept(session, name, reference);
		return reference;
	}

	@Value.Immutable
	public static abstract class SessionAttributeAccessorDef<S> {

		public abstract BiFunction<? super S, String, Object> readFunction();

		public abstract TriConsumer<? super S, String, Object> setFunction();

	}

	private static class ReferenceAtomic<X> extends AtomicReference<X> implements Muto<X> {}

}
