package com.lfp.joe.net.email;

import java.lang.reflect.Method;
import java.util.Objects;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.reflections.maven.MavenOrgSearchService;
import com.lfp.joe.reflections.reflection.LazyJar;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

public class JavaMails {

	private static final String INTERNET_ADDRESS_CLASS_NAME = "javax.mail.internet.InternetAddress";
	private static final MemoizedSupplier<Class<?>> INTERNET_ADDRESS_CLASS_S = Utils.Functions.memoize(() -> {
		var lazyJar = LazyJar.create(INTERNET_ADDRESS_CLASS_NAME,
				() -> MavenOrgSearchService.get().getURI("com.sun.mail", "javax.mail").get());
		return lazyJar.setup();
	}, null);
	private static final MemoizedSupplier<Method> INTERNET_ADDRESS_PARSE_METHOD_S = Utils.Functions.memoize(() -> {
		var method = INTERNET_ADDRESS_CLASS_S.get().getDeclaredMethod("parse", String.class);
		return Objects.requireNonNull(method);
	}, null);
	private static final MemoizedSupplier<Method> INTERNET_ADDRESS_GET_PERSONAL_METHOD_S = Utils.Functions
			.memoize(() -> {
				var method = INTERNET_ADDRESS_CLASS_S.get().getMethod("getPersonal");
				return Objects.requireNonNull(method);
			}, null);
	private static final MemoizedSupplier<Method> INTERNET_ADDRESS_GET_ADDRESS_METHOD_S = Utils.Functions
			.memoize(() -> {
				var method = INTERNET_ADDRESS_CLASS_S.get().getMethod("getAddress");
				return Objects.requireNonNull(method);
			}, null);

	public static Nada init() {
		INTERNET_ADDRESS_CLASS_S.get();
		return Nada.get();
	}

	public static StreamEx<InternetAddress> streamAddresses(String input) {
		init();
		return streamAddressesInternal(input);
	}

	private static StreamEx<InternetAddress> streamAddressesInternal(String input) {
		input = Utils.Strings.trimToNull(input);
		if (input == null)
			return StreamEx.empty();
		Object[] parseResults = (Object[]) Utils.Functions.catching(input,
				v -> INTERNET_ADDRESS_PARSE_METHOD_S.get().invoke(null, v), t -> null);
		if (parseResults == null || parseResults.length == 0)
			return StreamEx.empty();
		StreamEx<InternetAddress> internetAddressStream = Utils.Lots.stream(parseResults).nonNull().map(parseResult -> {
			return new InternetAddress() {

				@Override
				public String getAddress() {
					return (String) Utils.Functions
							.unchecked(() -> INTERNET_ADDRESS_GET_ADDRESS_METHOD_S.get().invoke(parseResult));
				}

				@Override
				public String getPersonal() {
					return (String) Utils.Functions
							.unchecked(() -> INTERNET_ADDRESS_GET_PERSONAL_METHOD_S.get().invoke(parseResult));
				}
			};
		});
		internetAddressStream = internetAddressStream.filter(ia -> {
			var indexesOf = Utils.Strings.indexesOf(ia.getAddress(), "@");
			return indexesOf.length == 1;
		});
		return internetAddressStream;
	}

}
