package com.lfp.joe.net.http.headers;

import javax.annotation.Generated;

public class UserAgentRecord {

	private final String userAgent;
	private final String version;
	private final String deviceType;
	private final String operatingSystem;

	@Generated("SparkTools")
	private UserAgentRecord(Builder builder) {
		this.userAgent = builder.userAgent;
		this.version = builder.version;
		this.deviceType = builder.deviceType;
		this.operatingSystem = builder.operatingSystem;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public String getVersion() {
		return version;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public String getOperatingSystem() {
		return operatingSystem;
	}

	/**
	 * Creates builder to build {@link UserAgentRecord}.
	 * 
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Creates a builder to build {@link UserAgentRecord} and initialize it with the
	 * given object.
	 * 
	 * @param userAgentRecord to initialize the builder with
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builderFrom(UserAgentRecord userAgentRecord) {
		return new Builder(userAgentRecord);
	}

	/**
	 * Builder to build {@link UserAgentRecord}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private String userAgent;
		private String version;
		private String deviceType;
		private String operatingSystem;

		private Builder() {
		}

		private Builder(UserAgentRecord userAgentRecord) {
			this.userAgent = userAgentRecord.userAgent;
			this.version = userAgentRecord.version;
			this.deviceType = userAgentRecord.deviceType;
			this.operatingSystem = userAgentRecord.operatingSystem;
		}

		public Builder withUserAgent(String userAgent) {
			this.userAgent = userAgent;
			return this;
		}

		public Builder withVersion(String version) {
			this.version = version;
			return this;
		}

		public Builder withDeviceType(String deviceType) {
			this.deviceType = deviceType;
			return this;
		}

		public Builder withOperatingSystem(String operatingSystem) {
			this.operatingSystem = operatingSystem;
			return this;
		}

		public UserAgentRecord build() {
			return new UserAgentRecord(this);
		}
	}

}
