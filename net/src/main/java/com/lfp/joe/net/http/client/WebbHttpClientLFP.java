package com.lfp.joe.net.http.client;

import java.util.Map.Entry;

import com.goebl.david.Request;
import com.goebl.david.Request.Method;

public interface WebbHttpClientLFP {

	public static WebbHttpClientLFP create() {
		return create(null);
	}

	public static WebbHttpClientLFP create(WebbHttpClientLFPConfig configuration) {
		final WebbHttpClientLFPConfig clientConfigFinal = configuration != null ? configuration
				: WebbHttpClientLFPConfig.builder().build();
		WebbLFP webb = new WebbLFP();
		webb.setFollowRedirects(clientConfigFinal.isFollowRedirects());
		if (clientConfigFinal.getDefaultHeaders() != null) {
			for (Entry<String, String> ent : clientConfigFinal.getDefaultHeaders().entrySet()) {
				String name = ent.getKey();
				if (name == null)
					continue;
				String val = ent.getValue();
				val = val != null ? val : "";
				webb.setDefaultHeader(name, val);
			}
		}
		if (clientConfigFinal.getProxy() != null)
			webb.setProxy(clientConfigFinal.getProxy());
		if (clientConfigFinal.getSslSocketFactory() != null)
			webb.setSSLSocketFactory(clientConfigFinal.getSslSocketFactory());
		if (clientConfigFinal.getHostnameVerifier() != null)
			webb.setHostnameVerifier(clientConfigFinal.getHostnameVerifier());
		if (clientConfigFinal.getRetryManager() != null)
			webb.setRetryManager(clientConfigFinal.getRetryManager());
		return new WebbHttpClientLFP() {

			@Override
			public Request put(String url) {
				return webb.put(url);
			}

			@Override
			public Request post(String url) {
				return webb.post(url);
			}

			@Override
			public Request get(String url) {
				return webb.get(url);
			}

			@Override
			public Request delete(String url) {
				return webb.delete(url);
			}

			@Override
			public WebbHttpClientLFPConfig getConfig() {
				return clientConfigFinal;
			}

			@Override
			public Request request(Method method, String url) {
				return webb.request(method, url);
			}
		};
	}



	Request get(String url);

	Request post(String url);

	Request put(String url);

	Request delete(String url);

	Request request(Method method, String url);

	WebbHttpClientLFPConfig getConfig();

}