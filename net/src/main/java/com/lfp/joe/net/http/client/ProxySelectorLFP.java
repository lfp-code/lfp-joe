package com.lfp.joe.net.http.client;

import java.io.IOException;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.util.List;
import java.util.Optional;

public class ProxySelectorLFP extends ProxySelector {

	private com.lfp.joe.net.proxy.Proxy proxy;

	public ProxySelectorLFP() {
		this(null);
	}

	public ProxySelectorLFP(com.lfp.joe.net.proxy.Proxy proxy) {
		this.proxy = proxy;
	}

	public void setProxy(com.lfp.joe.net.proxy.Proxy proxy) {
		this.proxy = proxy;
	}

	public Optional<com.lfp.joe.net.proxy.Proxy> getProxy() {
		return Optional.ofNullable(proxy);
	}

	@Override
	public List<Proxy> select(URI uri) {
		var jdkProxy = Optional.ofNullable(proxy).map(v -> v.asJdkProxy()).orElse(null);
		if (jdkProxy == null)
			return ProxySelector.getDefault().select(uri);
		return List.of(jdkProxy);
	}

	@Override
	public void connectFailed(URI uri, SocketAddress sa, IOException ioe) {
	}

}
