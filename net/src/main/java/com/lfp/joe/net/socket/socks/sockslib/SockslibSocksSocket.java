package com.lfp.joe.net.socket.socks.sockslib;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Optional;

import com.lfp.joe.reflections.JavaCode;

import javassist.Modifier;
import one.util.streamex.StreamEx;
import sockslib.client.SocksProxy;
import sockslib.client.SocksSocket;
import sockslib.common.SocksException;

public class SockslibSocksSocket extends SocksSocket {

	@SuppressWarnings("unchecked")
	private static final Field PROXY_FIELD = JavaCode.Reflections.getFieldUnchecked(SocksSocket.class, false,
			m -> m.getName().equals("proxy"), m -> SocksProxy.class.isAssignableFrom(m.getType()));

	public SockslibSocksSocket(SocksProxy proxy, InetAddress address, int port) throws SocksException, IOException {
		super(proxy, address, port);
	}

	public SockslibSocksSocket(SocksProxy proxy, Socket proxySocket) {
		super(proxy, proxySocket);
	}

	public SockslibSocksSocket(SocksProxy proxy, SocketAddress socketAddress) throws SocksException, IOException {
		super(proxy, socketAddress);
	}

	public SockslibSocksSocket(SocksProxy proxy, String remoteServerHost, int remoteServerPort)
			throws SocksException, IOException {
		super(proxy, remoteServerHost, remoteServerPort);
	}

	public SockslibSocksSocket(SocksProxy proxy) throws IOException {
		super(proxy);
	}

	public SocksProxy getSocksProxy() {
		try {
			return (SocksProxy) PROXY_FIELD.get(this);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
					? java.lang.RuntimeException.class.cast(e)
					: new java.lang.RuntimeException(e);
		}
	}

	protected Optional<Socket> tryGetProxySocket() {
		return Optional.ofNullable(getProxySocket());
	}

	@Override
	public boolean isConnected() {
		return tryGetProxySocket().map(Socket::isConnected).orElse(false);
	}

	@Override
	public boolean isBound() {
		return tryGetProxySocket().map(Socket::isBound).orElse(false);
	}

	@Override
	public boolean isClosed() {
		return tryGetProxySocket().map(Socket::isClosed).orElse(false);
	}

	@Override
	public boolean isInputShutdown() {
		return tryGetProxySocket().map(Socket::isInputShutdown).orElse(false);
	}

	@Override
	public void shutdownInput() throws IOException {
		Optional<Socket> socketOp = tryGetProxySocket();
		if (!socketOp.isPresent())
			return;
		socketOp.get().shutdownInput();
	}

	@Override
	public boolean isOutputShutdown() {
		return tryGetProxySocket().map(Socket::isOutputShutdown).orElse(false);
	}

	@Override
	public void shutdownOutput() throws IOException {
		Optional<Socket> socketOp = tryGetProxySocket();
		if (!socketOp.isPresent())
			return;
		socketOp.get().shutdownOutput();
	}

	@Override
	public synchronized void close() throws IOException {
		Optional<Socket> socketOp = tryGetProxySocket();
		if (socketOp.isPresent())
			socketOp.get().close();
		getSocksProxy().setProxySocket(null);
	}

	@Override
	public String toString() {
		try {
			if (this.isConnected())
				return super.toString();
		} catch (Exception e) {
		}
		return "Socket[unconnected]";
	}

	public static void main(String[] args) {
		StreamEx<Method> stream = JavaCode.Reflections.streamMethods(Socket.class, false,
				m -> m.getParameterCount() == 0, m -> !m.getName().startsWith("get"),
				m -> !m.getName().startsWith("set"), m -> m.getParameterCount() == 0,
				m -> Modifier.isPublic(m.getModifiers()));
		stream.forEach(m -> {
			System.out.println(m.getName());
		});
	}
}
