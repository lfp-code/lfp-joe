package com.lfp.joe.net.socket.socks.sockslib;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.config.NetConfig;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.net.proxy.ProxyEnabled;
import com.lfp.joe.utils.Utils;

import sockslib.client.Socks5;
import sockslib.common.SocksException;
import sockslib.common.UsernamePasswordCredentials;
import sockslib.common.methods.SocksMethod;

public class SockslibSocks5Proxy extends Socks5 implements ProxyEnabled {

	private static final MemoizedSupplier<ExecutorService> RUN_EXECUTOR_S = MemoizedSupplier.create(() -> {
		ExecutorService es = Executors.newCachedThreadPool();
		Runtime.getRuntime().addShutdownHook(new Thread(() -> es.shutdownNow()));
		return es;
	});
	private final Proxy proxy;

	public SockslibSocks5Proxy(Proxy proxy) {
		super(Objects.requireNonNull(proxy).getAddress());
		this.proxy = proxy;
		super.setAlwaysResolveAddressLocally(Configs.get(NetConfig.class).socks5AlwaysResolveAddressLocally());
		if (proxy.isAuthenticationEnabled())
			this.setCredentials(
					new UsernamePasswordCredentials(proxy.getUsername().orElse(""), proxy.getPassword().orElse("")));
	}

	@Override
	public SockslibSocks5Proxy copy() {
		SockslibSocks5Proxy socks5 = new SockslibSocks5Proxy(this.proxy);
		socks5.setAcceptableMethods(getAcceptableMethods())
				.setAlwaysResolveAddressLocally(this.isAlwaysResolveAddressLocally())
				.setCredentials(this.getCredentials()).setSocksMethodRequester(this.getSocksMethodRequester())
				.setChainProxy(this.getChainProxy());
		return socks5;
	}

	@Override
	public void buildConnection() throws SocksException, IOException {
		if (getInetAddress() == null)
			throw new IllegalArgumentException("Please set inetAddress before calling buildConnection.");
		Exception toThrow = null;
		NetConfig netConfig = Configs.get(NetConfig.class);
		boolean success = false;
		for (int i = 0; !success && (i == 0 || i < netConfig.socks5SocketRetryCount()); i++) {
			if (i > 0)
				Utils.Functions.unchecked(() -> Thread.sleep(netConfig.socks5SocketRetryDelayMillis()));
			Future<Void> connectFuture = RUN_EXECUTOR_S.get().submit(() -> {
				if (getProxySocket() == null)
					setProxySocket(createProxySocket(getInetAddress(), getPort()));
				else if (!getProxySocket().isConnected())
					getProxySocket().connect(new InetSocketAddress(getInetAddress(), getPort()));
				SocksMethod method = getSocksMethodRequester().doRequest(getAcceptableMethods(), getProxySocket(),
						SOCKS_VERSION);
				method.doMethod(this);
				return null;
			});
			try {
				connectFuture.get(netConfig.socks5SocketConnectTimeoutMillis(), TimeUnit.MILLISECONDS);
				success = true;
				continue;
			} catch (Exception e) {
				toThrow = e;
			}
			connectFuture.cancel(true);
			String msg = Utils.Exceptions.streamCauses(toThrow).map(Throwable::getMessage)
					.filter(Utils.Strings::isNotBlank).findFirst().orElse(null);
			String errAppend = "errType:" + toThrow.getClass().getName();
			if (msg != null)
				errAppend = String.format("errMsg:%s ", msg) + errAppend;
			logger.warn("proxy connection failed. attempt:{}/{} {} proxy:{} ", i + 1,
					netConfig.socks5SocketRetryCount(), errAppend, this.proxy);
			Socket proxySocket = this.getProxySocket();
			closeQuietly(proxySocket);
			closeQuietly(() -> this.getInputStream().close());
			closeQuietly(() -> this.getOutputStream().close());
			this.setProxySocket(null);
		}
		if (success)
			return;
		if (toThrow instanceof SocketException)
			throw (SocketException) toThrow;
		throw Utils.Exceptions.as(toThrow, IOException.class);
	}

	@Override
	public Proxy getProxy() {
		return this.proxy;
	}

	private static void closeQuietly(Closeable closeable) {
		if (closeable == null)
			return;
		try {
			closeable.close();
		} catch (Exception e) {
			// suppress
		}

	}

}
