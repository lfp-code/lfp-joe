package com.lfp.joe.net.http;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpClient.Version;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.LinkedHashMap;
import java.util.Objects;
import java.util.Optional;

import org.immutables.value.Value;

import com.github.mizosoft.methanol.internal.extensions.ResponseBuilder;
import com.google.common.net.MediaType;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.bytes.LazyInputStream;

import at.favre.lib.bytes.Bytes;

@ValueLFP.Style
@Value.Enclosing
public class HttpResponses {

	private static final int STATUS_CODE_DEFAULT = StatusCodes.OK;
	private static final Version VERSION_DEFAULT = Version.HTTP_1_1;

	protected HttpResponses() {};

	public static <U> ResponseBuilder<U> builder() {
		var builder = new ResponseBuilder<U>();
		builder.statusCode(STATUS_CODE_DEFAULT);
		builder.version(VERSION_DEFAULT);
		return builder;
	}

	public static <U> ResponseBuilder<U> builder(HttpResponse<U> httpResponse) {
		if (httpResponse == null)
			return builder();
		return ResponseBuilder.newBuilder(httpResponse);
	}

	public static ResponseBuilder<InputStream> builder(ThrowingSupplier<InputStream, IOException> inputStreamSupplier) {
		InputStream inputStream = new LazyInputStream(inputStreamSupplier);
		return HttpResponses.<InputStream>builder().body(inputStream);
	}

	public static HttpResponse<InputStream> build(URI uri, Integer statusCode) {
		var reason = StatusCodes.getReason(statusCode);
		var map = new LinkedHashMap<String, Object>(2);
		map.put("statusCode", statusCode);
		map.put("reason", reason);
		var jsonBytes = Serials.Gsons.toBytes(map);
		return build(uri, statusCode, jsonBytes, HeaderMap.of(Headers.CONTENT_TYPE, MediaType.JSON_UTF_8.toString()));
	}

	public static HttpResponse<InputStream> build(URI uri, Integer statusCode, Bytes bytes, HeaderMap headerMap) {
		if (bytes == null)
			bytes = Utils.Bits.empty();
		headerMap = HeaderMap.of(headerMap);
		headerMap = headerMap.withReplace(Headers.CONTENT_LENGTH, Objects.toString(bytes.length()));
		return build(uri, statusCode, bytes::inputStream, headerMap);
	}

	public static HttpResponse<InputStream> build(URI uri, Integer statusCode,
			ThrowingSupplier<InputStream, IOException> inputStreamSupplier, HeaderMap headerMap) {
		var request = HttpRequest.newBuilder(uri).build();
		headerMap = HeaderMap.of(headerMap);
		var builder = builder(inputStreamSupplier);
		builder.request(request);
		builder.uri(uri);
		Optional.ofNullable(statusCode).ifPresent(builder::statusCode);
		Optional.ofNullable(headerMap).map(HeaderMap::httpHeaders).ifPresent(builder::headers);
		request.version().ifPresent(builder::version);
		return builder.build();
	}

	public static void main(String[] args) throws IOException {
		var resp = HttpResponses.build(URI.create("https://example.com"), 404);
		System.out.println(resp);
		try (var is = resp.body()) {
			System.out.println(Utils.Bits.from(is).encodeUtf8());
		}
		System.out.println(resp);
	}

}
