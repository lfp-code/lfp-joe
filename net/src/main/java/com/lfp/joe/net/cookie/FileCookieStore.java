package com.lfp.joe.net.cookie;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpCookie;
import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.Validate;
import org.joda.beans.ser.JodaBeanSer;

import com.google.common.reflect.TypeToken;
import com.lfp.joe.core.function.Muto.MutoBoolean;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.lock.KeyLock;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.bytes.PeekableInputStream;
import com.lfp.joe.utils.crypto.Base58;

import one.util.streamex.StreamEx;

public class FileCookieStore extends AbstractCookieStore {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 6;
	private static final KeyLock<String> FILE_LOCK = KeyLock.create();
	@SuppressWarnings("serial")
	private static final TypeToken<List<Cookie>> COOKIE_LIST_TT = new TypeToken<List<Cookie>>() {};
	private File folder;

	public FileCookieStore(String uuid) {
		this(Utils.Files.tempFile(THIS_CLASS, "cookies", VERSION, uuid));
	}

	public FileCookieStore(File folder) {
		Objects.requireNonNull(folder);
		folder.mkdirs();
		Validate.isTrue(folder.isDirectory(), "invalid directory:%s", folder.getAbsolutePath());
		this.folder = folder;
	}

	@Override
	public boolean removeAll() {
		return Utils.Files.deleteDirectoryContents(this.folder, v -> true);
	}

	@Override
	protected Iterable<CookieIdentifier> getCookieIdentifiers() {
		var fileStream = Utils.Lots.stream(this.folder.listFiles());
		return fileStream.map(v -> decodeCookieIdentifier(v.getName()).orElse(null)).nonNull();
	}

	@Override
	protected Iterable<HttpCookie> get(CookieIdentifier cookieIdentifier) {
		return Utils.Functions.unchecked(() -> getInternal(cookieIdentifier));
	}

	protected Iterable<HttpCookie> getInternal(CookieIdentifier cookieIdentifier) throws IOException {
		File file = new File(this.folder, encodeCookieIdentifier(cookieIdentifier));
		List<Cookie> cookieList;
		var lock = FILE_LOCK.readLock(file.getAbsolutePath());
		lock.lock();
		try {
			cookieList = readFromFile(file);
		} finally {
			lock.unlock();
		}
		return Utils.Lots.stream(cookieList).map(Cookie::toHttpCookie);
	}

	@Override
	protected void addOrReplace(CookieIdentifier cookieIdentifier, StreamEx<HttpCookie> cookieStream) {
		Utils.Functions.unchecked(() -> modify(cookieIdentifier, cookieStream, false));

	}

	@Override
	protected boolean remove(CookieIdentifier cookieIdentifier, StreamEx<HttpCookie> cookieStream) {
		return Utils.Functions.unchecked(() -> modify(cookieIdentifier, cookieStream, true));
	}

	protected boolean modify(CookieIdentifier cookieIdentifier, StreamEx<HttpCookie> cookieStream, boolean remove)
			throws IOException {
		File file = new File(this.folder, encodeCookieIdentifier(cookieIdentifier));
		var mod = MutoBoolean.create();
		var lock = FILE_LOCK.writeLock(file.getAbsolutePath());
		lock.lock();
		try {
			var httpCookieSet = Utils.Lots.stream(readFromFile(file)).map(Cookie::toHttpCookie).toSet();
			httpCookieSet = modifyCookieSet(httpCookieSet, cookieStream, remove, () -> mod.set(true));
			if (mod.get())
				writeToFile(file, Utils.Lots.stream(httpCookieSet).map(Cookie::build).toList());
		} finally {
			lock.unlock();
		}
		return mod.get();

	}

	private static final String encodeCookieIdentifier(CookieIdentifier cookieIdentifier) {
		var barr = JodaBeanSer.COMPACT.binWriter().write(cookieIdentifier);
		return Base58.encode(barr);
	}

	// optional jic a random file ends up in the directory
	private static final Optional<CookieIdentifier> decodeCookieIdentifier(String fileName) {
		CookieIdentifier result;
		try {
			var barr = Base58.decode(fileName);
			result = (CookieIdentifier) JodaBeanSer.COMPACT.binReader().read(barr);
		} catch (Exception e) {
			result = null;
		}
		return Optional.ofNullable(result);
	}

	private static List<Cookie> readFromFile(File file) throws IOException {
		if (!file.exists())
			return null;
		try (var fis = new FileInputStream(file); var pis = new PeekableInputStream(fis);) {
			if (Objects.equals(pis.peek(), -1))
				return null;
			return Serials.Gsons.fromStream(pis, COOKIE_LIST_TT);
		}
	}

	private static void writeToFile(File file, List<Cookie> cookies) throws IOException {
		if (cookies == null || cookies.isEmpty()) {
			file.delete();
			return;
		}
		try (var fos = new FileOutputStream(file);) {
			Serials.Gsons.toStream(cookies, fos);
		}
	}

	public static void main(String[] args) {
		var cookieStore = new FileCookieStore("test-123");
		System.out.println(cookieStore.streamURIs().toList());
		var cookie = new HttpCookie("key", "nutz");
		cookie.setDomain(".sub2.www.yahoo.com");
		cookieStore.add(URI.create("https://sub2.www.yahoo.com"), cookie, true);
		cookieStore.stream(URI.create("https://sub3.sub2.www.yahoo.com")).map(Cookie::build).toListAndThen(list -> {
			System.out.println(Serials.Gsons.getPretty().toJson(list));
			return Nada.get();
		});
		cookieStore.stream(URI.create("https://sub2.www.yahoo.com")).map(Cookie::build).toListAndThen(list -> {
			System.out.println(Serials.Gsons.getPretty().toJson(list));
			return Nada.get();
		});

	}

}
