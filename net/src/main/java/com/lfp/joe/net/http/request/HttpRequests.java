package com.lfp.joe.net.http.request;

import java.io.IOException;
import java.io.InputStream;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse.BodyHandler;
import java.net.http.HttpResponse.BodySubscriber;
import java.net.http.HttpResponse.BodySubscribers;
import java.net.http.HttpResponse.ResponseInfo;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.function.Supplier;

import com.github.mizosoft.methanol.MutableRequest;
import com.google.common.hash.Hashing;
import com.lfp.joe.core.function.Throws.ThrowingBiFunction;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;

public class HttpRequests {

	protected HttpRequests() {}

	public static MutableRequest request() {
		return request(null);
	}

	public static MutableRequest request(HttpRequest httpRequest) {
		return Optional.ofNullable(httpRequest).map(MutableRequest::copyOf).orElseGet(MutableRequest::create);
	}

	public static <X> BodyHandler<Supplier<X>> bodyHandler(
			ThrowingBiFunction<ResponseInfo, InputStream, X, IOException> mapper) {
		return bodyHandler(null, mapper);
	}

	public static <X> BodyHandler<Supplier<X>> bodyHandler(Predicate<ResponseInfo> responseInfoFilter,
			ThrowingBiFunction<ResponseInfo, InputStream, X, IOException> mapper) {
		BodyHandler<Supplier<X>> bodyHandler = responseInfo -> {
			BodySubscriber<InputStream> upstream;
			if (responseInfoFilter == null || responseInfoFilter.test(responseInfo))
				upstream = BodySubscribers.mapping(BodySubscribers.ofInputStream(),
						is -> is != null ? is : Utils.Bits.emptyInputStream());
			else
				upstream = BodySubscribers.replacing(null);
			return BodySubscribers.mapping(upstream, is -> {
				Supplier<X> supplier = () -> {
					try {
						return mapper.apply(responseInfo, is);
					} catch (IOException e) {
						throw Utils.Exceptions.asRuntimeException(e);
					}
				};
				return supplier;
			});
		};
		return bodyHandler;
	}

	public static Bytes hashMetadata(HttpRequest request) {
		Objects.requireNonNull(request);
		var hasher = Hashing.farmHashFingerprint64().newHasher();
		int putIndex = 0;
		var objStream = Streams.<Object>of(request.method(), request.uri(),
				request.version().map(v -> v.name()).orElse(""));
		for (var obj : objStream) {
			hasher.putInt(putIndex++);
			if (obj == null)
				hasher.putInt(-1);
			else
				hasher.putString(obj.toString(), StandardCharsets.UTF_8);
		}
		hasher.putInt(putIndex++);
		hasher.putBoolean(request.expectContinue());
		hasher.putInt(putIndex++);
		hasher.putBytes(HeaderMap.of(request.headers()).hash().array());
		return Utils.Bits.from(hasher.hash().asBytes());
	}

	public static void main(String[] args) {
		var index = -1;
		System.out.println(index++);
		System.out.println(index++);
		System.out.println(index++);
	}
}
