package com.lfp.joe.net.media;

import java.lang.reflect.Type;

import com.google.common.net.MediaType;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.lfp.joe.serial.serializer.Serializer;

@Serializer(MediaType.class)
public class MediaTypeSerializer implements JsonSerializer<MediaType>, JsonDeserializer<MediaType> {

	@Override
	public MediaType deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		if (json == null || !json.isJsonPrimitive())
			return null;
		var str = json.getAsString();
		return MediaTypes.tryParse(str).orElse(null);
	}

	@Override
	public JsonElement serialize(MediaType src, Type typeOfSrc, JsonSerializationContext context) {
		if (src == null)
			return JsonNull.INSTANCE;
		return new JsonPrimitive(src.toString());
	}

}
