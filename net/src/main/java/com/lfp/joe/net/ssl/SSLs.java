package com.lfp.joe.net.ssl;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Objects;
import java.util.Optional;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import javax.security.auth.x500.X500Principal;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.utils.Utils;

public class SSLs {

	public static PrivateKey readPrivateKey(File file) throws IOException, GeneralSecurityException {
		return new PrivateKeyReader(file).getPrivateKey();
	}

	public static Certificate[] readCertificates(File file) throws CertificateException, IOException {
		return new CertificateReader(file).getCertificates();
	}

	public static SSLContext createSSLContext(File privateKey, File certificate)
			throws IOException, GeneralSecurityException {
		char[] passphrase = Utils.Crypto.getSecureRandomString().toCharArray();
		// create a key store
		KeyStore ts = KeyStore.getInstance("JKS");
		KeyStore ks = KeyStore.getInstance("JKS");
		ts.load(null, null);
		ks.load(null, null);
		Certificate[] trustedCerts = readCertificates(certificate);
		{ // read the trused certs
			int index = -1;
			for (Certificate cert : trustedCerts) {
				index++;
				ts.setCertificateEntry("rsa-trusted-2048-" + index, cert);
			}
		}
		// read the private key.
		PrivateKey priKey = readPrivateKey(privateKey);
		// import the key entry.
		ks.setKeyEntry("rsa-key-2048", priKey, passphrase, trustedCerts);
		// create SSL context
		KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
		kmf.init(ks, passphrase);
		TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
		tmf.init(ts);
		SSLContext sslCtx = SSLContext.getInstance("TLS");
		sslCtx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
		return sslCtx;
	}

	private static final MemoizedSupplier<TrustManager[]> TRUST_MANAGERS_DEFAULT_S = Utils.Functions.memoize(() -> {
		try {
			TrustManagerFactory trustManagerFactory = TrustManagerFactory
					.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			trustManagerFactory.init((KeyStore) null);
			TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
			Objects.requireNonNull(trustManagers);
			Validate.notEmpty(trustManagers);
			return trustManagers;
		} catch (Exception e) {
			throw new IllegalStateException("default trust manager lookup failed", e); // The system has no TLS. Just
																						// give up.
		}
	});

	public static TrustManager[] trustManagersDefault() {
		return TRUST_MANAGERS_DEFAULT_S.get();
	}

	private static final MemoizedSupplier<X509TrustManager> TRUST_ALL_MANAGER_S = Utils.Functions.memoize(() -> {
		// Create a trust manager that does not validate certificate chains
		return new X509TrustManager() {
			@Override
			public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType)
					throws CertificateException {
			}

			@Override
			public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType)
					throws CertificateException {
			}

			@Override
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[0];
			}
		};
	});

	public static final X509TrustManager trustAllManager() {
		return TRUST_ALL_MANAGER_S.get();
	}

	private static final MemoizedSupplier<SSLContext> TRUST_ALL_CONTEXT_S = Utils.Functions.memoize(() -> {
		// Install the all-trusting trust manager
		final SSLContext sslContext;
		try {
			sslContext = SSLContext.getInstance("SSL");
			sslContext.init(null, new TrustManager[] { trustAllManager() }, new java.security.SecureRandom());
		} catch (KeyManagementException | NoSuchAlgorithmException e) {
			throw Utils.Exceptions.asRuntimeException(e);
		}
		return sslContext;
	});

	public static final SSLContext trustAllContext() {
		return TRUST_ALL_CONTEXT_S.get();
	}

	public static Optional<String> parseSubjectCN(X509Certificate certificate) {
		if (certificate == null)
			return Optional.empty();
		var principal = certificate.getSubjectX500Principal();
		return parseCN(principal);
	}

	public static Optional<String> parseIssuerCN(X509Certificate certificate) {
		if (certificate == null)
			return Optional.empty();
		var principal = certificate.getIssuerX500Principal();
		return parseCN(principal);
	}

	private static Optional<String> parseCN(X500Principal principal) {
		if (principal == null)
			return Optional.empty();
		var result = principal.getName();
		var token = "CN=";
		var splitAt = Utils.Strings.indexOfIgnoreCase(result, token);
		result = result.substring(splitAt + token.length());
		result = Utils.Strings.substringBefore(result, ",");
		result = Utils.Strings.trim(result);
		if (Utils.Strings.isBlank(result))
			return Optional.empty();
		return Optional.of(result);
	}

	public static SSLSocketFactory createSSLSocketFactory(TrustManager... trustManagers) {
		try {
			SSLContext sslContext = SSLContext.getInstance("TLS");
			sslContext.init(null, trustManagers, Utils.Crypto.getSecureRandom());
			return sslContext.getSocketFactory();
		} catch (KeyManagementException | NoSuchAlgorithmException e) {
			throw Utils.Exceptions.asRuntimeException(e);
		}
	}

	public static X509TrustManager concat(X509TrustManager... trustManagers) {
		var tms = Utils.Lots.stream(trustManagers).nonNull().limit(2).toList();
		if (tms.size() == 1)
			return tms.get(0);
		return new MultiX509TrustManager(trustManagers);
	}

}
