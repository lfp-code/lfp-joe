package com.lfp.joe.net.socket.socks.sockslib;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Objects;

import javax.net.SocketFactory;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.net.proxy.ProxyEnabled;

public class SockslibSocks5SocketFactory extends SocketFactory implements ProxyEnabled {

	private Proxy proxy;

	public SockslibSocks5SocketFactory(Proxy proxy) {
		Objects.requireNonNull(proxy);
		Validate.isTrue(Proxy.Type.SOCKS_5.equals(proxy.getType()), "only socks5 sockets are supported");
		this.proxy = proxy;
	}

	@Override
	public Proxy getProxy() {
		return proxy;
	}

	@Override
	public Socket createSocket() throws IOException {
		return new SockslibSocksSocket(new SockslibSocks5Proxy(proxy));
	}

	@Override
	public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
		return new SockslibSocksSocket(new SockslibSocks5Proxy(proxy), host, port);
	}

	@Override
	public Socket createSocket(InetAddress host, int port) throws IOException {
		return new SockslibSocksSocket(new SockslibSocks5Proxy(proxy), host, port);
	}

	@Override
	public Socket createSocket(String host, int port, InetAddress localHost, int localPort)
			throws IOException, UnknownHostException {
		throw new UnsupportedOperationException(
				"not supported:createSocket(String host, int port, InetAddress localHost, int localPort)");
	}

	@Override
	public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort)
			throws IOException {
		throw new UnsupportedOperationException(
				"not supported:createSocket(InetAddress address, int port, InetAddress localAddress, int localPort)");
	}

}
