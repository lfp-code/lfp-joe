package com.lfp.joe.net.cookie;

import java.net.HttpCookie;
import java.net.URI;

import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.utils.Utils;

public class CookieRules {

	public static boolean hostMatch(URI requestURI, String cookieDomain, boolean matchSubDomain) {
		if (requestURI == null || Utils.Strings.isEmpty(cookieDomain))
			return false;
		String requestHost = requestURI.getHost();
		if (Utils.Strings.isEmpty(requestHost))
			return false;
		if (requestHost.equals(cookieDomain))
			return true;
		if (matchSubDomain && requestHost.endsWith("." + cookieDomain))
			return true;
		return false;
	}

	public static boolean pathMatch(URI requestURI, String cookiePath) {
		if (requestURI == null)
			return false;
		var requestPath = URIs.normalizePath(requestURI.getPath());
		cookiePath = URIs.normalizePath(cookiePath);
		if (URIs.pathPrefixMatch(requestPath, cookiePath))
			return true;
		return false;
	}

	public static boolean secureMatch(URI requestURI, HttpCookie httpCookie) {
		if (requestURI == null || httpCookie == null)
			return false;
		if (!Boolean.TRUE.equals(httpCookie.getSecure()))
			return true;
		if (URIs.isSecure(requestURI))
			return true;
		return false;
	}

}
