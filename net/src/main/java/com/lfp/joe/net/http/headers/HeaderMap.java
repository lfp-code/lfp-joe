package com.lfp.joe.net.http.headers;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.http.HttpHeaders;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.function.BiPredicate;
import java.util.function.Function;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.mizosoft.methanol.internal.extensions.HeadersBuilder;
import com.google.common.hash.Hashing;
import com.google.re2j.Pattern;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Hashable;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.EntryStream;

public class HeaderMap implements Iterable<Entry<String, String>>, Serializable, Hashable {

	private static final Pattern NAME_SPLIT_PATTERN = Pattern.compile("\\b");
	private static final BiPredicate<String, String> ACCEPT_ALL_FILTER = (k, v) -> true;
	private static final HttpHeaders EMPTY_HTTP_HEADERS = HttpHeaders.of(Map.of(), ACCEPT_ALL_FILTER);
	private static final HeaderMap EMPTY_HEADER_MAP = new HeaderMap(EMPTY_HTTP_HEADERS);
	private static final Cache<String, Optional<String>> HEADER_NAME_NORMALIZATION_CACHE = Caffeine.newBuilder()
			.expireAfterAccess(Duration.ofSeconds(1))
			.build();

	// only not final for serialization
	private HttpHeaders httpHeaders;

	protected HeaderMap() {
		this.httpHeaders = EMPTY_HTTP_HEADERS;
	}

	public HeaderMap(HttpHeaders httpHeaders) {
		this.httpHeaders = Optional.ofNullable(httpHeaders).orElse(EMPTY_HTTP_HEADERS);
	}

	@SafeVarargs
	public HeaderMap(Map<String, ? extends Iterable<String>> map, BiPredicate<String, String>... filters) {
		if (map == null)
			this.httpHeaders = EMPTY_HTTP_HEADERS;
		else {
			Map<String, List<String>> normMap = normalizeMap(map);
			BiPredicate<String, String> allFilters = ACCEPT_ALL_FILTER;
			if (filters != null)
				for (var filter : filters)
					if (filter != null)
						allFilters = allFilters.and(filter);
			this.httpHeaders = HttpHeaders.of(normMap, allFilters);
		}
	}

	@Override
	public Iterator<Entry<String, String>> iterator() {
		return stream().iterator();
	}

	public EntryStream<String, String> stream(String... nameFilters) {
		return Headers.stream(map(), nameFilters);
	}

	public HttpHeaders httpHeaders() {
		return this.httpHeaders;
	}

	public HeadersBuilder builder() {
		var builder = new HeadersBuilder();
		builder.addAll(this.httpHeaders);
		return builder;
	}

	public boolean isEmpty() {
		return this.map().isEmpty();
	}

	public long count(String... nameFilters) {
		return stream(nameFilters).count();
	}

	public boolean contains(String name) {
		return contains(name, false);
	}

	public boolean contains(String name, boolean requireNonBlankValue) {
		if (Utils.Strings.isBlank(name))
			return false;
		for (var key : map().keySet()) {
			var valueOp = firstValue(key);
			if (valueOp.isEmpty())
				continue;
			if (requireNonBlankValue && Utils.Strings.isBlank(valueOp.get()))
				continue;
			if (Utils.Strings.equalsIgnoreCase(key, name))
				return true;
		}
		return false;

	}

	public HeaderMap with(String name, String... values) {
		if (Utils.Strings.isBlank(name))
			return this;
		if (values == null || values.length == 0)
			return this;
		var estream = stream();
		for (var value : values)
			estream = estream.append(name, value);
		return of(estream);
	}

	public HeaderMap withReplace(String name, String... values) {
		if (Utils.Strings.isBlank(name))
			return this;
		if (values == null || values.length == 0)
			return this;
		var estream = stream();
		estream = estream.filterKeys(v -> !Utils.Strings.equalsIgnoreCase(name, v));
		for (var value : values)
			estream = estream.append(name, value);
		return of(estream);
	}

	public HeaderMap withRemoved(String name, String... values) {
		if (Utils.Strings.isBlank(name))
			return this;
		var estream = stream();
		estream = estream.filter(ent -> {
			var eName = ent.getKey();
			if (!Utils.Strings.equalsIgnoreCase(eName, name))
				return true;
			var eValue = ent.getValue();
			if (values == null || values.length == 0)
				return false;
			for (var value : values) {
				if (value != null && Utils.Strings.equals(value, eValue))
					return false;
			}
			return true;
		});
		return of(estream);
	}

	public Optional<String> firstValue(String name, boolean requireNonBlankValue) {
		if (!requireNonBlankValue)
			return this.firstValue(name);
		return stream(name).values().filter(Utils.Strings::isNotBlank).findFirst();
	}

	private static Map<String, List<String>> normalizeMap(Map<String, ? extends Iterable<String>> map) {
		if (map == null || map.isEmpty())
			return Map.of();
		return Headers.stream(map).mapKeys(k -> normalizeName(k)).nonNullKeys().grouping();
	}

	private static String normalizeName(String name) {
		if (name == null || name.isEmpty())
			return null;
		var nameOp = HEADER_NAME_NORMALIZATION_CACHE.get(name.toLowerCase(), nameLowerCase -> {
			try (var checkNameStream = Headers.streamNames()) {
				for (var checkName : checkNameStream)
					if (checkName.equalsIgnoreCase(name))
						return Optional.of(checkName);
			}
			var nameSplit = NAME_SPLIT_PATTERN.split(nameLowerCase);
			var nameNormalized = Streams.of(nameSplit).map(nameWord -> {
				if (nameWord.length() > 0 && Character.isAlphabetic(nameWord.charAt(0)))
					nameWord = Utils.Strings.capitalize(nameWord);
				return nameWord;
			}).joining();
			if (nameNormalized.length() != nameLowerCase.length())
				return Optional.empty();
			return Optional.of(nameNormalized);
		});
		return nameOp.orElse(name);
	}

	// hash

	@Override
	public Bytes hash() {
		return hash(false);
	}

	public Bytes hash(boolean disableSort) {
		var estream = stream();
		if (!disableSort) {
			var mm = estream.grouping();
			estream = Streams.of(mm.keySet()).sorted().flatMap(name -> {
				return Streams.of(mm.get(name)).sorted().mapToEntry(value -> name).invert();
			}).chain(EntryStreams::of);
		}
		var hasher = Hashing.farmHashFingerprint64().newHasher();
		var index = -1;
		for (var ent : estream) {
			index++;
			hasher.putInt(index);
			hasher.putString(ent.getKey(), StandardCharsets.UTF_8);
			index++;
			hasher.putInt(index);
			hasher.putString(ent.getValue(), StandardCharsets.UTF_8);
		}
		return Utils.Bits.from(hasher.hash().asBytes());
	}

	// serialize

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void readObject(ObjectInputStream objectInputStream) throws ClassNotFoundException, IOException {
		var mapObj = objectInputStream.readObject();
		if (mapObj instanceof Map) {
			Map map = (Map) mapObj;
			this.httpHeaders = HttpHeaders.of(map, ACCEPT_ALL_FILTER);
		} else
			this.httpHeaders = EMPTY_HTTP_HEADERS;
	}

	private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
		objectOutputStream.writeObject(this.map());
	}

	// ofs

	public static HeaderMap of() {
		return EMPTY_HEADER_MAP;
	}

	public static HeaderMap of(HttpHeaders httpHeaders) {
		if (httpHeaders == null)
			return HeaderMap.of();
		var hm = new HeaderMap(httpHeaders);
		return hm;
	}

	public static HeaderMap of(String... nameValues) {
		if (nameValues == null || nameValues.length == 0)
			return HeaderMap.of();
		EntryStream<String, String> estream = EntryStream.empty();
		for (int i = 0; i < nameValues.length; i = i + 2) {
			var name = nameValues[i];
			if (name == null)
				continue;
			var value = (i + 1) < nameValues.length ? nameValues[i + 1] : null;
			estream = estream.append(name, value);
		}
		var hm = of(estream);
		return hm;
	}

	@SafeVarargs
	public static HeaderMap of(Map<String, ?> headerMap, BiPredicate<String, String>... filters) {
		if (headerMap == null)
			return HeaderMap.of();
		var estream = Headers.stream(headerMap);
		var hm = of(estream, filters);
		return hm;
	}

	@SafeVarargs
	public static HeaderMap of(Iterable<? extends Entry<String, String>> headerEntries,
			BiPredicate<String, String>... filters) {
		if (headerEntries == null)
			return HeaderMap.of();
		if (headerEntries instanceof HeaderMap)
			return (HeaderMap) headerEntries;
		var hm = new HeaderMap(Utils.Lots.streamEntries(headerEntries).grouping(), filters);
		return hm;
	}

	public static HeaderMap of(Iterable<String> names,
			Function<String, ? extends Iterable<? extends CharSequence>> valueFunction) {
		if (names == null || valueFunction == null)
			return HeaderMap.of();
		var estream = Streams.of(names).nonNull().map(name -> {
			var values = valueFunction.apply(name);
			return Streams.of(values).nonNull().map(Object::toString).mapToEntry(v -> name).invert();
		}).chain(Streams.flatMap());
		var hm = of(estream);
		return hm;
	}

	// delegates

	public Optional<String> firstValue(String name) {
		return httpHeaders.firstValue(name);
	}

	public OptionalLong firstValueAsLong(String name) {
		return httpHeaders.firstValueAsLong(name);
	}

	public List<String> allValues(String name) {
		return httpHeaders.allValues(name);
	}

	public Map<String, List<String>> map() {
		return httpHeaders.map();
	}

	@Override
	public final boolean equals(Object obj) {
		return httpHeaders.equals(obj);
	}

	@Override
	public final int hashCode() {
		return httpHeaders.hashCode();
	}

	@Override
	public String toString() {
		return "HeaderMap [map=" + map() + "]";
	}

	public static void main(String[] args) {
		var name = "  --f-forwarded-for-------x. -- . cool";
		System.out.println(Arrays.asList(name.split("\\b")));
		System.out.println(Arrays.asList(NAME_SPLIT_PATTERN.split(name)));
		System.out.println(normalizeName(name));
		var map = new HashMap<String, List<String>>();
		map.put("hi", Arrays.asList("", ""));
		var headerMap = HeaderMap.of(map);
		System.out.println(headerMap.count("ok", null, ""));
		System.out.println(headerMap.allValues("hi"));
		System.out.println(headerMap.contains("hi"));
		System.out.println(headerMap.contains("hi", true));
		headerMap = headerMap.withReplace("hi", "argh");
		System.out.println(headerMap.map());
		System.out.println(headerMap.with("neat", "cool").withRemoved("hi", "").map());
		System.out.println(headerMap.with("neat", "cool").withRemoved("hi").map());
		System.out.println(headerMap.with("neat", "cool").withRemoved("hi", "argh").map());
		var serialized = Utils.Bits.serialize(headerMap);
		System.out.println(serialized.encodeHex());
		HeaderMap deser = Utils.Bits.deserialize(serialized);
		System.out.println(deser);
	}

}
