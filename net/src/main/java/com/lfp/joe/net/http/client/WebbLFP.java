package com.lfp.joe.net.http.client;

import java.lang.reflect.Constructor;
import java.util.Set;

import org.apache.commons.lang3.Validate;

import com.goebl.david.Request;
import com.goebl.david.Request.Method;
import com.goebl.david.Response;
import com.goebl.david.Webb;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.utils.Utils;

public class WebbLFP extends Webb {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static MemoizedSupplier<Constructor<Request>> REQUEST_CONSTRUCTOR = Utils.Functions.memoize(() -> {
		Set<Constructor> constructors = JavaCode.Reflections.getConstructors(Request.class,
				c -> c.getParameterCount() == 3, c -> {
					if (!Webb.class.isAssignableFrom(c.getParameterTypes()[0]))
						return false;
					if (!Method.class.isAssignableFrom(c.getParameterTypes()[1]))
						return false;
					if (!String.class.isAssignableFrom(c.getParameterTypes()[2]))
						return false;
					return true;
				});
		Validate.isTrue(constructors.size() == 1, "could not find constructor");
		Constructor ctor = constructors.iterator().next();
		ctor.setAccessible(true);
		return ctor;
	});

	public WebbLFP() {
		super();
		super.setBaseUri(null);
	}

	@Override
	public void setBaseUri(String baseUri) {
		logger.warn("set base uri is not supported. ignoring input:{}", baseUri);
	}

	public Request request(Method method, String uri) {
		return Utils.Functions.unchecked(() -> REQUEST_CONSTRUCTOR.get().newInstance(this, method, uri));
	}

}
