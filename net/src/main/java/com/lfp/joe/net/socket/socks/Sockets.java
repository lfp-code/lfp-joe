package com.lfp.joe.net.socket.socks;

import com.lfp.joe.threads.Threads;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

import org.threadly.concurrent.future.ListenableFuture;

import com.lfp.joe.net.http.ip.IPs;

import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;

public class Sockets {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static int allocatePort() throws IOException {
		var port = PortAllocator.INSTANCE.get();
		return port;
	}

	public static boolean isServerPortOpen(InetSocketAddress address) {
		return isServerPortOpen(address == null ? null : address.getHostString(),
				address == null ? -1 : address.getPort());
	}

	public static boolean isServerPortOpen(String host, int port) {
		return isServerPortOpen(host, port, null);
	}

	public static boolean isServerPortOpen(String host, int port, Function<Throwable, Boolean> onError) {
		if (Utils.Strings.isBlank(host))
			return false;
		if (port < 0)
			return false;
		try (var socket = new Socket(host, port)) {
			return true;
		} catch (Throwable t) {
			if (onError != null)
				return Boolean.TRUE.equals(onError.apply(t));
			return false;
		}
	}

	public static boolean isPortAvailable(int port) {
		try (var ss = new ServerSocket(port); var ds = new DatagramSocket(port)) {
			return true;
		} catch (IOException e) {
			return false;
		}
	}

	public static ListenableFuture<Boolean> isPubliclyReachable(int port, Duration timeout) {
		return isPubliclyReachable(IPs.getIPAddress(), port, timeout);
	}

	public static ListenableFuture<Boolean> isPubliclyReachable(String host, int port, Duration timeout) {
		Objects.requireNonNull(timeout);
		var future = testPublicReachability(host, port);
		var timeoutFuture = Threads.Pools.centralPool().submitScheduled(() -> {
			return future.cancel(true);
		}, timeout.toMillis());
		return future.mapFailure(CancellationException.class, t -> {
			return false;
		}).listener(() -> {
			timeoutFuture.cancel(true);
		});
	}

	public static ListenableFuture<Boolean> testPublicReachability(int port) {
		return testPublicReachability(IPs.getIPAddress(), port);
	}

	public static ListenableFuture<Boolean> testPublicReachability(String host, int port) {
		Objects.requireNonNull(host);
		String key = Utils.Crypto.getSecureRandomString();
		var future = Threads.Pools.centralPool().submit(() -> {
			ListenableFuture<Long> sendFuture = null;
			try (var server = new ServerSocket(port, 10, InetAddress.getByName("0.0.0.0"))) {
				sendFuture = Threads.Pools.centralPool().submit(() -> {
					while (!Thread.currentThread().isInterrupted()) {
						try (var socketSend = new Socket(host, port);
								var osSend = socketSend.getOutputStream();
								var isSend = Bytes.from(key.getBytes(StandardCharsets.UTF_8)).inputStream()) {
							return isSend.transferTo(osSend);
						}
					}
					return 0l;
				});
				server.setSoTimeout((int) Duration.ofSeconds(1).toMillis());
				while (!Thread.currentThread().isInterrupted()) {
					try (Socket socketListen = server.accept();
							var readerListen = new InputStreamReader(socketListen.getInputStream(),
									StandardCharsets.UTF_8)) {
						var message = new StringBuilder();
						while (true) {
							var nextChar = readerListen.read();
							if (nextChar == -1)
								break;
							message.append((char) nextChar);
							if (message.length() >= key.length())
								break;
						}
						if (key.equals(message.toString()))
							return true;
					} catch (Exception e) {
						// suppress
					}
				}
			} finally {
				if (sendFuture != null)
					sendFuture.cancel(true);
			}
			return false;
		});
		future = future.mapFailure(Throwable.class, t -> false);
		return future;
	}

	public static void main(String[] args) throws InterruptedException, ExecutionException, TimeoutException {
		System.out.println(Sockets.testPublicReachability(6969).get(5, TimeUnit.SECONDS));
	}

}
