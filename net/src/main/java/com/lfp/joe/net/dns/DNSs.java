package com.lfp.joe.net.dns;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Optional;
import java.util.regex.Pattern;

import org.jsoup.helper.Validate;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.config.NetConfig;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.utils.Utils;

public class DNSs {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static String toWildcardDNSHostname(String hostname) {
		return toWildcardDNSHostname(hostname, null);
	}

	public static String toWildcardDNSHostname(String hostname, String prefix) {
		String ipAddress = Utils.Functions.unchecked(() -> getIPAdress(hostname));
		String result = ipAddress + "." + Configs.get(NetConfig.class).wildcardDNSDomain().toLowerCase();
		if (Utils.Strings.isNotBlank(prefix))
			result = prefix + "." + result;
		return result;
	}

	public static String getIPAdress(final String hostname) throws UnknownHostException {
		Validate.isTrue(Utils.Strings.isNotBlank(hostname), "hostname is required");
		if (IPs.isValidIpAddress(hostname))
			return hostname;
		Optional<String> ipOptional = parseIPAddressFromWildcardDNSHostname(hostname);
		if (ipOptional.isPresent())
			return ipOptional.get();
		return InetAddress.getByName(hostname).getHostAddress();
	}

	public static Optional<String> parseIPAddressFromWildcardDNSHostname(String hostname) {
		if (Utils.Strings.isBlank(hostname))
			return Optional.empty();
		String wildcardDNSDomain = Configs.get(NetConfig.class).wildcardDNSDomain();
		if (Utils.Strings.isBlank(wildcardDNSDomain))
			return Optional.empty();
		if (!Utils.Strings.endsWithIgnoreCase(hostname, wildcardDNSDomain))
			return Optional.empty();
		String parseHostname = hostname.substring(0, hostname.length() - wildcardDNSDomain.length());
		parseHostname = Utils.Strings.stripEnd(parseHostname, ".");
		if (IPs.isValidIpAddress(parseHostname))
			return Optional.of(parseHostname);
		String[] parts = parseHostname.split(Pattern.quote("."));
		for (int i = 1; i < parts.length; i++) {
			String test = Utils.Lots.stream(parts).skip(i).joining(".");
			if (IPs.isValidIpAddress(test))
				return Optional.of(test);
		}
		return Optional.empty();
	}

	public static void main(String[] args) {
		String wildcardDNSHostname = toWildcardDNSHostname("google.com", "coooool");
		System.out.println(wildcardDNSHostname);
	}
}
