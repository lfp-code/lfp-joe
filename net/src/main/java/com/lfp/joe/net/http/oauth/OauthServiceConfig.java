package com.lfp.joe.net.http.oauth;

import java.util.Arrays;
import java.util.List;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.net.http.ServiceConfig;

import at.favre.lib.bytes.Bytes;

public interface OauthServiceConfig extends ServiceConfig, OauthRequest {

	@Override
	default List<HeaderGenerator> headerGenerators() {
		Class<? extends OauthHeaderGenerator> classType;
		if (MachineConfig.isDeveloper())
			classType = OauthHeaderGenerator.FileCached.class;
		else
			classType = OauthHeaderGenerator.Cached.class;
		var instance = HeaderGeneratorConverter.convert(classType);
		return Arrays.asList(instance);
	}

	@Override
	default Bytes hash() {
		return OauthRequest.super.hash();
	}

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.builder().withPrependClassNames(true).build());
	}
}
