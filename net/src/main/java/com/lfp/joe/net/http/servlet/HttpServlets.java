package com.lfp.joe.net.http.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.net.MediaType;
import com.google.gson.Gson;
import com.lfp.joe.core.function.Throws.ThrowingConsumer;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

public class HttpServlets {

	protected HttpServlets() {}

	public static URI getUri(HttpServletRequest request) {
		Objects.requireNonNull(request);
		var headerMap = getHeaders(request);
		String scheme = headerMap.firstValue(Headers.X_FORWARDED_PROTO, true).orElse(request.getScheme());
		String host = headerMap.firstValue(Headers.X_FORWARDED_HOST, true).orElse(request.getServerName());
		int port = headerMap.firstValue(Headers.X_FORWARDED_PORT, true).flatMap(Utils.Strings::parseNumber)
				.map(Number::intValue).orElse(request.getServerPort());
		if (port == 80 && !URIs.isSecure(scheme))
			port = -1;
		else if (port == 443 && URIs.isSecure(scheme))
			port = -1;
		String path = Optional.ofNullable(request.getRequestURI()).filter(Utils.Strings::isNotBlank).orElse("");
		if (!Utils.Strings.startsWith(path, "/"))
			path = "/" + path;
		String query = Optional.ofNullable(request.getQueryString()).map(v -> {
			if (Utils.Strings.startsWithIgnoreCase(v, "?"))
				v = v.substring(1);
			return v;
		}).filter(Utils.Strings::isNotBlank).orElse(null);
		String url = String.format("%s://%s%s%s%s", scheme, host, port < 0 ? "" : ":" + port, path,
				query == null ? "" : "?" + query);
		ThrowingSupplier<URI, ?> loader = () -> URI.create(url);
		return loader.onError(t -> null).get();
	}

	public static HeaderMap getHeaders(HttpServletRequest request) {
		if (request == null)
			return HeaderMap.of();
		var names = request.getHeaderNames();
		if (names == null)
			return HeaderMap.of();
		return HeaderMap.of(Streams.of(names), v -> {
			var values = request.getHeaders(v);
			return values == null ? null : Streams.of(values);
		});
	}

	public static HeaderMap getHeaders(HttpServletResponse response) {
		if (response == null)
			return HeaderMap.of();
		return HeaderMap.of(response.getHeaderNames(), v -> response.getHeaders(v));
	}

	public static void writeStatusCode(Integer statusCode, HttpServletResponse response) throws IOException {
		if (statusCode == null)
			statusCode = StatusCodes.OK;
		var reason = StatusCodes.getReason(statusCode);
		writeJson(statusCode, Map.of("message", reason), response);
	}

	public static void writeJson(Object data, HttpServletResponse response) throws IOException {
		writeJson(Serials.Gsons.get(), null, data, response);
	}

	public static void writeJson(Integer statusCode, Object data, HttpServletResponse response) throws IOException {
		writeJson(Serials.Gsons.get(), statusCode, data, response);
	}

	public static void writeJson(Gson gson, Integer statusCode, Object data, HttpServletResponse response)
			throws IOException {
		Objects.requireNonNull(gson);
		response.addHeader(Headers.CONTENT_TYPE, MediaType.JSON_UTF_8.toString());
		write(statusCode, os -> {
			Serials.Gsons.toStream(gson, data, os);
		}, response);
	}

	public static void write(Integer statusCode, InputStream inputStream, HttpServletResponse response)
			throws IOException {
		Objects.requireNonNull(inputStream);
		write(statusCode, os -> {
			try (inputStream) {
				inputStream.transferTo(os);
			}
		}, response);
	}

	public static void write(Integer statusCode, ThrowingConsumer<OutputStream, IOException> writer,
			HttpServletResponse response) throws IOException {
		Objects.requireNonNull(writer);
		Objects.requireNonNull(response);
		if (statusCode == null)
			statusCode = StatusCodes.OK;
		try (var os = response.getOutputStream()) {
			writer.accept(os);
		} finally {
			try {
				if (writer instanceof AutoCloseable)
					((AutoCloseable) writer).close();
			} catch (Exception e) {
				throw Utils.Exceptions.as(e, IOException.class);
			} finally {
				response.flushBuffer();
			}
		}
	}

}
