package com.lfp.joe.net.http.ip;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;

public interface IPAddressConfig extends Config {

	@DefaultValue("")
	String ipApiToken();
	
	public static void main(String[] args) {
		Configs.printProperties();
	}

}
