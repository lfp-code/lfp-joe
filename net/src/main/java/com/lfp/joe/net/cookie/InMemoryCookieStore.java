package com.lfp.joe.net.cookie;

import java.net.HttpCookie;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import one.util.streamex.StreamEx;

public class InMemoryCookieStore extends AbstractCookieStore {

	private final Map<CookieIdentifier, Set<HttpCookie>> cookies = new ConcurrentHashMap<>();

	@Override
	public boolean removeAll() {
		boolean empty = cookies.isEmpty();
		cookies.clear();
		return !empty;
	}

	@Override
	protected Iterable<CookieIdentifier> getCookieIdentifiers() {
		return cookies.keySet();
	}

	@Override
	protected Iterable<HttpCookie> get(CookieIdentifier cookieIdentifier) {
		var ible = cookies.get(cookieIdentifier);
		return ible;
	}

	@Override
	protected void addOrReplace(CookieIdentifier cookieIdentifier, StreamEx<HttpCookie> cookieStream) {
		cookies.compute(cookieIdentifier, (k, cookieSet) -> {
			return modifyCookieSet(cookieSet, cookieStream, false, null);
		});
	}

	@Override
	protected boolean remove(CookieIdentifier cookieIdentifier, StreamEx<HttpCookie> cookieStream) {
		var mod = new AtomicBoolean();
		cookies.compute(cookieIdentifier, (k, cookieSet) -> {
			return modifyCookieSet(cookieSet, cookieStream, true, () -> mod.set(true));
		});
		return mod.get();
	}

}