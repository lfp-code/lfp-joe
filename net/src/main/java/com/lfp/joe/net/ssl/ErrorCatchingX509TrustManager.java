package com.lfp.joe.net.ssl;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Objects;

import javax.net.ssl.X509TrustManager;

import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingConsumer;

public class ErrorCatchingX509TrustManager implements X509TrustManager {

	private final X509TrustManager delegate;
	private final ThrowingConsumer<Exception, CertificateException> onError;

	public ErrorCatchingX509TrustManager(X509TrustManager trustManager,
			ThrowingConsumer<Exception, CertificateException> onError) {
		this.delegate = Objects.requireNonNull(trustManager);
		this.onError = onError != null ? onError : e -> {
			if (e instanceof CertificateException)
				throw (CertificateException) e;
			throw Utils.Exceptions.asRuntimeException(e);
		};
	}

	@Override
	public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
		try {
			delegate.checkClientTrusted(chain, authType);
		} catch (Exception e) {
			this.onError.accept(e);
		}
	}

	@Override
	public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
		try {
			delegate.checkServerTrusted(chain, authType);
		} catch (Exception e) {
			this.onError.accept(e);
		}
	}

	@Override
	public X509Certificate[] getAcceptedIssuers() {
		return delegate.getAcceptedIssuers();
	}
}
