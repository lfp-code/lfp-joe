package com.lfp.joe.net.socket.socks.jsocks;

import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import org.jsoup.helper.Validate;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.utils.Utils;

import net.sourceforge.jsocks.socks.Proxy;
import net.sourceforge.jsocks.socks.ProxyMessage;
import net.sourceforge.jsocks.socks.SocksException;
import net.sourceforge.jsocks.socks.SocksSocket;

public class JsocksSocks5Socket extends SocksSocket {

	@SuppressWarnings("unchecked")
	private static final Field oldImpl_FIELD = JavaCode.Reflections.getFieldUnchecked(Socket.class, false,
			f -> boolean.class.isAssignableFrom(f.getType()), f -> f.getName().equals("oldImpl"));
	@SuppressWarnings("unchecked")
	private static final Field connected_FIELD = JavaCode.Reflections.getFieldUnchecked(Socket.class, false,
			f -> boolean.class.isAssignableFrom(f.getType()), f -> f.getName().equals("connected"));
	@SuppressWarnings("unchecked")
	private static final Field bound_FIELD = JavaCode.Reflections.getFieldUnchecked(Socket.class, false,
			f -> boolean.class.isAssignableFrom(f.getType()), f -> f.getName().equals("bound"));
	@SuppressWarnings("unchecked")
	private static final Field proxySocket_FIELD = JavaCode.Reflections.getFieldUnchecked(Proxy.class, false,
			f -> f.getName().equals("proxySocket"), f -> Socket.class.isAssignableFrom(f.getType()));
	@SuppressWarnings("unchecked")
	private static final MemoizedSupplier<Method> processReply_METHOD_S = MemoizedSupplier.create(() -> {
		Set<Method> methods = JavaCode.Reflections.streamMethods(SocksSocket.class, false,
				m -> m.getName().equals("processReply"), m -> m.getParameterCount() == 1,
				m -> ProxyMessage.class.isAssignableFrom(m.getParameterTypes()[0])).toSet();
		Validate.isTrue(!methods.isEmpty(), "could not find method");
		Validate.isTrue(methods.size() == 1, "matched multiple methods");
		Method method = methods.iterator().next();
		method.setAccessible(true);
		return method;
	});
	private static final MemoizedSupplier<ExecutorService> TIMEOUT_EXECUTOR_SERVICE = MemoizedSupplier.create(() -> {
		ExecutorService es = Executors.newCachedThreadPool();
		Runtime.getRuntime().addShutdownHook(new Thread(() -> es.shutdownNow()));
		return es;
	});

	private Supplier<String> localHostSupplier;

	public JsocksSocks5Socket(JsocksSocks5Proxy proxy) {
		super("localhost", 6969, addDummySocket(proxy.copy()));
		removeDummySocket(proxy);
		this.remotePort = Utils.Types.getPrimitiveDefault(int.class).get();
		this.localIP = null;
		this.localPort = Utils.Types.getPrimitiveDefault(int.class).get();
		this.remoteHost = null;
	}

	@Override
	public void connect(SocketAddress endpoint) throws IOException {
		connect(endpoint, 0);
	}

	@Override
	public void connect(SocketAddress endpoint, int timeout) throws IOException {
		Objects.requireNonNull(endpoint);
		InetSocketAddress inetSocketAddress = Utils.Types.tryCast(endpoint, InetSocketAddress.class).orElse(null);
		Objects.requireNonNull(inetSocketAddress, "endpoint must be InetSocketAddress");
		Callable<Void> loader = () -> {
			this.remoteHost = inetSocketAddress.getHostString();
			this.remotePort = inetSocketAddress.getPort();
			ProxyMessage reply = getJsocksSocks5Proxy().connect(this.remoteHost, this.remotePort);
			processReply(reply);
			this.setConnected(true);
			this.setBound(true);
			return null;
		};
		if (timeout == 0) {
			try {
				loader.call();
				return;
			} catch (Exception e) {
				throw Utils.Exceptions.as(e, IOException.class);
			}
		}
		Future<Void> future = TIMEOUT_EXECUTOR_SERVICE.get().submit(loader);
		try {
			future.get(timeout, TimeUnit.MILLISECONDS);
		} catch (Throwable t) {
			future.cancel(true);
			if (t instanceof ExecutionException && t.getCause() != null)
				t = t.getCause();
			JsocksSocks5Proxy socks5Proxy = this.getJsocksSocks5Proxy();
			Socket proxySocket = socks5Proxy == null ? null : socks5Proxy.getProxySocket();
			if (proxySocket != null) {
				closeQuietly(proxySocket.getOutputStream());
				closeQuietly(proxySocket.getInputStream());
				closeQuietly(proxySocket);
			}
			if (socks5Proxy != null)
				closeQuietly(() -> socks5Proxy.endSession());
			closeQuietly(this);
			throw Utils.Exceptions.as(t, IOException.class);
		}

	}

	protected JsocksSocks5Proxy getJsocksSocks5Proxy() {
		return (JsocksSocks5Proxy) this.proxy;
	}

	public void setOldImpl(boolean value) {
		Utils.Functions.unchecked(() -> oldImpl_FIELD.set(this, value));
	}

	public void setConnected(boolean value) {
		Utils.Functions.unchecked(() -> connected_FIELD.set(this, value));
	}

	public void setBound(boolean value) {
		Utils.Functions.unchecked(() -> bound_FIELD.set(this, value));
	}

	/**
	 * FIX SLOW LOCALHOST
	 */

	protected void processReply(ProxyMessage reply) throws SocksException {
		localPort = reply.port;
		JsocksSocks5Proxy proxy = this.getJsocksSocks5Proxy();
		/*
		 * If the server have assigned same host as it was contacted on it might return
		 * an address of all zeros
		 */
		if (reply.host.equals("0.0.0.0")) {
			localIP = proxy.getProxyIP();
			this.localHostSupplier = () -> localIP == null ? null : localIP.getHostName();
		} else {
			this.localHostSupplier = () -> reply == null ? null : reply.host;
			localIP = reply.ip;
		}
	}

	@Override
	public InetAddress getLocalAddress() {
		if (localIP == null) {
			try {
				localIP = InetAddress.getByName(getLocalHost());
			} catch (UnknownHostException e) {
				return null;
			}
		}
		return localIP;
	}

	@Override
	public String getLocalHost() {
		if (localHostSupplier != null)
			return localHostSupplier.get();
		return null;
	}

	/**
	 * UTILS
	 */

	private static JsocksSocks5Proxy addDummySocket(JsocksSocks5Proxy proxy) {
		Utils.Functions.unchecked(() -> proxySocket_FIELD.set(proxy, new Socket()));
		return proxy;
	}

	private static void removeDummySocket(JsocksSocks5Proxy proxy) {
		Utils.Functions.unchecked(() -> proxySocket_FIELD.set(proxy, null));
	}

	private static void closeQuietly(Closeable closeable) {
		if (closeable == null)
			return;
		try {
			closeable.close();
		} catch (Exception e) {
		}
	}

}
