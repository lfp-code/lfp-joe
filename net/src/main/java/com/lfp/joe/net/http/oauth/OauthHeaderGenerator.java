package com.lfp.joe.net.http.oauth;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.net.http.ServiceConfig;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.utils.Utils;

public class OauthHeaderGenerator implements ServiceConfig.HeaderGenerator {

	private final OAuthTokenFunction oAuthTokenFunction;

	public OauthHeaderGenerator() {
		this(new OAuthTokenFunction());
	}

	public OauthHeaderGenerator(OAuthTokenFunction oAuthTokenFunction) {
		this.oAuthTokenFunction = Objects.requireNonNull(oAuthTokenFunction);
	}

	@Override
	public Map<String, List<String>> generateHeaders(ServiceConfig service) {
		Validate.isTrue(service instanceof OauthRequest, "service must implement oauth request");
		OauthRequest oauthRequest = (OauthRequest) service;
		if (oauthRequest.oauthURI() == null)
			return Collections.emptyMap();
		var resp = Utils.Functions.unchecked(() -> this.oAuthTokenFunction.apply(oauthRequest));
		Map<String, List<String>> map = new LinkedHashMap<>();
		map.put(Headers.AUTHORIZATION, Arrays.asList(resp.getAccessToken()));
		return map;
	}

	public static class Cached extends OauthHeaderGenerator {

		public Cached() {
			super(new OAuthTokenFunction.Cached());
		}
	}

	public static class FileCached extends OauthHeaderGenerator {

		public FileCached() {
			super(new OAuthTokenFunction.FileCached());
		}
	}

}
