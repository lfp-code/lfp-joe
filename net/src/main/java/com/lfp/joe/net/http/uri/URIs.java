package com.lfp.joe.net.http.uri;

import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.PrimitiveIterator.OfInt;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.nibor.autolink.LinkType;

import com.google.common.collect.Maps;
import com.lfp.joe.net.html.Htmls;
import com.lfp.joe.utils.Utils;

import io.mikael.urlbuilder.UrlBuilder;
import one.util.streamex.EntryStream;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;

public class URIs {

	private static final String REDACTED_TOKEN = Utils.Crypto.getSecureRandomString();
	private static final List<String> SECURE_SCHEMES = List.of("https", "wss", "tcps");

	public static Optional<URI> parse(String url) {
		if (Utils.Strings.isBlank(url))
			return Optional.empty();
		if (!Utils.Strings.containsIgnoreCase(url, "://"))
			return Optional.empty();
		URI uri = Utils.Functions.catching(() -> URI.create(url), t -> null);
		if (uri == null)
			return Optional.empty();
		return Optional.of(uri);
	}

	public static URI normalize(URI uri) {
		if (uri == null)
			return null;
		boolean mod = false;
		String path;
		{// path
			path = normalizePath(uri.getPath());
			if (!mod && !Objects.equals(path, uri.getPath()))
				mod = true;
		}
		int port;
		{// port
			port = normalizePort(uri.getScheme(), uri.getPort());
			if (!mod && !Objects.equals(port, uri.getPort()))
				mod = true;
		}
		String fragment;
		{// fragment
			fragment = normalizeFragment(uri.getFragment());
			if (!mod && !Objects.equals(fragment, uri.getFragment()))
				mod = true;
		}
		String query;
		{// query
			query = normalizeQuery(uri.getQuery());
			if (!mod && !Objects.equals(query, uri.getQuery()))
				mod = true;
		}
		if (!mod)
			return uri;
		try {
			return new URI(uri.getScheme(), uri.getUserInfo(), uri.getHost(), port, path, query, fragment);
		} catch (URISyntaxException e) {
			throw Utils.Exceptions.asRuntimeException(e);
		}
	}

	public static int getPort(URI uri) {
		if (uri == null)
			return -1;
		if (uri.getPort() >= 0)
			return uri.getPort();
		var scheme = uri.getScheme();
		if ("https".equalsIgnoreCase(scheme) || "wss".equalsIgnoreCase(scheme))
			return 443;
		if ("http".equalsIgnoreCase(scheme) || "ws".equalsIgnoreCase(scheme))
			return 80;
		return -1;
	}

	public static int normalizePort(URI uri) {
		if (uri == null)
			return -1;
		return normalizePort(uri.getScheme(), uri.getPort());
	}

	public static int normalizePort(String scheme, int port) {
		if (port == -1)
			return port;
		if (port == 443) {
			if ("https".equalsIgnoreCase(scheme))
				return -1;
			if ("wss".equalsIgnoreCase(scheme))
				return -1;
		} else if (port == 80) {
			if ("http".equalsIgnoreCase(scheme))
				return -1;
			if ("ws".equalsIgnoreCase(scheme))
				return -1;
		}
		return port;
	}

	public static String normalizePath(URI uri) {
		return normalizePath(uri == null ? null : uri.getPath());
	}

	public static String normalizePath(String path) {
		path = normalize(path, null, '/', '?', '#');
		if (path == null)
			// everything stripped away
			return "/";
		else
			// ensure beginning slash
			return "/" + path;

	}

	public static String normalizeFragment(String value) {
		if (Utils.Strings.isBlank(value))
			return null;
		return value;
	}

	public static String normalizeQuery(String value) {
		if (Utils.Strings.isBlank(value))
			return null;
		return value;
	}

	public static boolean pathPrefixMatch(String path, String prefix) {
		path = normalizePath(path);
		prefix = normalizePath(prefix);
		if (Utils.Strings.equals(path, prefix))
			return true;
		if ("/".equals(prefix) && path.startsWith("/"))
			return true;
		if (Utils.Strings.startsWith(path, prefix + "/"))
			return true;
		return false;
	}

	public static StreamEx<String> streamPathSegments(URI uri) {
		return streamPathSegments(uri == null ? null : uri.getPath());
	}

	public static StreamEx<String> streamPathSegments(String path) {
		if (path == null || path.isEmpty())
			return StreamEx.empty();
		path = URIs.normalizePath(path);
		if (Utils.Strings.startsWithIgnoreCase(path, "/"))
			path = path.substring(1);
		if (Utils.Strings.isBlank(path))
			return StreamEx.empty();
		return Utils.Lots.stream(path.split(Pattern.quote("/")));
	}

	public static EntryStream<String, String> streamQueryParameters(String queryString) {
		return streamQueryParameters(queryString, StandardCharsets.UTF_8);
	}

	public static EntryStream<String, String> streamQueryParameters(String queryString, Charset charsetDecode) {
		return streamQueryParameters(queryString, charsetDecode, false);
	}

	private static EntryStream<String, String> streamQueryParameters(String queryString, Charset charsetDecode,
			boolean disableURLCheck) {
		queryString = normalize(queryString, false, '?');
		queryString = normalize(queryString, true, '#');
		if (queryString == null)
			return EntryStream.empty();
		{// after question mark
			boolean querySearch = false;
			if (!disableURLCheck)
				try {
					new URL(queryString);
					querySearch = true;
				} catch (Exception e) {
					// suppress
				}
			if (querySearch) {
				int splitAt = Utils.Strings.indexOf(queryString, '?');
				if (splitAt >= 0) {
					queryString = queryString.substring(splitAt + 1);
				}
			}
		}
		queryString = normalize(queryString, false, '?');
		queryString = normalize(queryString, true, '#');
		if (queryString == null)
			return EntryStream.empty();
		Function<String, String> decoder = str -> {
			return Optional.ofNullable(str).map(v -> {
				if (charsetDecode == null)
					return v;
				try {
					return URLDecoder.decode(str, charsetDecode);
				} catch (IllegalArgumentException e) {
					// suppress
				}
				return null;
			}).orElse("");
		};
		StreamEx<Entry<String, String>> stream = Utils.Lots.stream(Utils.Strings.split(queryString, "&"))
				.filter(chunk -> Utils.Strings.isNotBlank(chunk))
				.map(chunk -> {
					int splitAt = chunk.indexOf("=");
					String key = splitAt < 0 ? chunk : Utils.Strings.substring(chunk, 0, splitAt);
					String val = splitAt < 0 ? "" : Utils.Strings.substring(chunk, splitAt + 1);
					if (Utils.Strings.isBlank(key) && Utils.Strings.isBlank(val)) {
						return null;
					}
					key = decoder.apply(key);
					if (Utils.Strings.isBlank(key))
						return null;
					String value = decoder.apply(val);
					return Maps.immutableEntry(key, value);
				})
				.nonNull();
		return EntryStream.of(stream);
	}

	public static Entry<String, Optional<String>> parseCredentials(URI uri) {
		if (uri == null)
			return null;
		var userInfo = uri.getUserInfo();
		if (userInfo == null || userInfo.isEmpty())
			return null;
		int splitAt = Utils.Strings.indexOf(userInfo, ":");
		String username;
		if (splitAt == -1)
			username = userInfo;
		else
			username = userInfo.substring(0, splitAt);
		String password;
		if (splitAt == -1)
			password = null;
		else
			password = userInfo.substring(splitAt + 1);
		if (password == null || password.isEmpty())
			password = null;
		return Utils.Lots.entry(username, Optional.ofNullable(password));
	}

	public static boolean isSecure(URI uri) {
		return isSecure(uri == null ? null : uri.getScheme());
	}

	public static boolean isSecure(String scheme) {
		if (scheme == null)
			return false;
		for (var check : SECURE_SCHEMES) {
			if (check.equalsIgnoreCase(scheme))
				return true;
		}
		return false;
	}

	public static boolean isLocalhost(String host) {
		if (Utils.Strings.equalsIgnoreCase(host, "localhost"))
			return true;
		if (Utils.Strings.equalsIgnoreCase(host, "127.0.0.1"))
			return true;
		if (Utils.Strings.equalsIgnoreCase(host, "0:0:0:0:0:0:0:0"))
			return true;
		return false;
	}

	public static StreamEx<URI> extract(String text) {
		return Htmls.extractLinks(text, LinkType.WWW, LinkType.URL).map(s -> parse(s).orElse(null)).nonNull();
	}

	public static String toString(URI uri, boolean hideScheme, boolean hideUserInfo, boolean hideHost, boolean hidePort,
			boolean hidePath, boolean hideQuery, boolean hideFragment) {
		if (uri == null)
			return null;
		var urlb = UrlBuilder.fromUri(uri);
		if (hideScheme && Utils.Strings.isNotBlank(uri.getScheme()))
			urlb = urlb.withScheme(REDACTED_TOKEN);
		if (hideUserInfo && Utils.Strings.isNotBlank(uri.getUserInfo()))
			urlb = urlb.withUserInfo(REDACTED_TOKEN);
		if (hideHost && Utils.Strings.isNotBlank(uri.getHost()))
			urlb = urlb.withHost(REDACTED_TOKEN);
		if (hidePort && uri.getPort() != -1)
			urlb = urlb.withPort(0);
		if (hidePath && Utils.Strings.isNotBlank(uri.getPath()))
			urlb = urlb.withPath(REDACTED_TOKEN);
		if (hideQuery && Utils.Strings.isNotBlank(uri.getQuery()))
			urlb = urlb.withQuery(REDACTED_TOKEN);
		if (hideFragment && Utils.Strings.isNotBlank(uri.getFragment()))
			urlb = urlb.withFragment(REDACTED_TOKEN);
		var str = urlb.toString();
		str = str.replaceAll(Pattern.quote(REDACTED_TOKEN), "[REDACTED]");
		return str;
	}

	public static InetSocketAddress toAddress(URI uri) {
		if (uri == null)
			return null;
		var host = uri.getHost();
		if (Utils.Strings.isBlank(host))
			return null;
		var port = uri.getPort();
		if (port == -1)
			port = isSecure(uri) ? 443 : 80;
		return InetSocketAddress.createUnresolved(host, port);
	}

	private static String normalize(String input, Boolean end, Character... stripChars) {
		if (input == null || input.isEmpty())
			return null;
		if (stripChars == null || stripChars.length == 0)
			return input;
		if (end == null) {
			for (var endVariation : List.of(false, true))
				input = normalize(input, endVariation, stripChars);
			return input;
		}
		for (var stripChar : stripChars) {
			if (stripChar == null)
				continue;
			OfInt indexIter;
			if (!end)
				indexIter = IntStreamEx.range(0, input.length()).iterator();
			else
				indexIter = StreamEx.iterate(input.length() - 1, v -> v > -1, v -> v - 1).mapToInt(v -> v).iterator();
			int stripCharIndex = -1;
			boolean whitespace = true;
			while (indexIter.hasNext()) {
				var index = indexIter.next();
				var charAt = input.charAt(index);
				if (whitespace && Character.isWhitespace(charAt))
					continue;
				if (Objects.equals(stripChar, charAt))
					stripCharIndex = index;
				break;
			}
			if (stripCharIndex < 0)
				continue;
			var beginIndex = !end ? stripCharIndex + 1 : 0;
			var endIndex = !end ? input.length() : stripCharIndex;
			input = input.substring(beginIndex, endIndex);
			return normalize(input, end, stripChars);
		}
		return input;
	}

	public static void main(String[] args) {
		var istream = Stream.iterate(10, v -> v > -1, v -> v - 1).mapToInt(v -> v);

		System.out.println(Arrays.toString(istream.toArray()));

		var uri = URI.create("http://cool.com:80////?a=b#");
		System.out.println(uri);
		// System.out.println(URIs.normalize(uri));
		System.out.println(normalizePath("/aaa/    "));
		System.out.println(normalizePath("?   /aaa/    "));
		System.out.println(streamQueryParameters("?hat=cool   ").toMap());
		System.out.println(streamQueryParameters("?hat=cool   ").toMap());
		System.out.println(streamQueryParameters("?hat=cool   ").toMap());
	}

}
