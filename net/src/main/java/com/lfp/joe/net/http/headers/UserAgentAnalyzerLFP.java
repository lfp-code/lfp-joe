package com.lfp.joe.net.http.headers;

import java.util.Optional;

import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.utils.Utils;

import nl.basjes.parse.useragent.AbstractUserAgentAnalyzer;
import nl.basjes.parse.useragent.UserAgent;

public class UserAgentAnalyzerLFP extends AbstractUserAgentAnalyzer {

	public static UserAgentAnalyzerLFP get() {
		return Instances.get(UserAgentAnalyzerLFP.class, () -> {
			var blder = newBuilder();
			blder = blder.hideMatcherLoadStats();
			blder = blder.withCache(15_000);
			return blder.build();
		});
	}

	protected UserAgentAnalyzerLFP() {
		super();
	}

	public Optional<UserAgent> tryParse(String userAgentString) {
		userAgentString = Utils.Strings.trimToNull(userAgentString);
		if (userAgentString == null)
			return Optional.empty();
		try {
			var ua = this.parse(userAgentString);
			return Optional.ofNullable(ua);
		} catch (Exception e) {
			return Optional.empty();
		}
	}

	public static Builder newBuilder() {
		return new Builder(new UserAgentAnalyzerLFP());
	}

	public static final class Builder extends AbstractUserAgentAnalyzerBuilder<UserAgentAnalyzerLFP, Builder> {

		private Builder(UserAgentAnalyzerLFP uaa) {
			super(uaa);
			this.hideMatcherLoadStats();
		}
	}
}
