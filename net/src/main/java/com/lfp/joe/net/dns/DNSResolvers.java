package com.lfp.joe.net.dns;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.InetAddress;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.reflections8.ReflectionUtils;
import org.xbill.DNS.ExtendedResolver;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.Record;
import org.xbill.DNS.Type;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.config.NetConfig;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

public class DNSResolvers {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final MemoizedSupplier<ExecutorService> DNS_LOOKUP_EXECUTOR_SERVICE_S = MemoizedSupplier
			.create(() -> {
				ExecutorService es = Executors.newCachedThreadPool();
				Runtime.getRuntime().addShutdownHook(new Thread(() -> es.shutdownNow()));
				return es;
			});;

	public static Cache<String, ExtendedResolver> RESOLVER_CACHE = Caffeine.newBuilder()
			.expireAfterAccess(Duration.ofSeconds(30)).build();
	@SuppressWarnings("unchecked")
	private static LoadingCache<Class<? extends Record>, Optional<Method>> GET_ADDRESS_METHOD_CACHE = Caffeine
			.newBuilder().expireAfterAccess(Duration.ofSeconds(10)).build(ct -> {
				Set<Method> methods = ReflectionUtils.getAllMethods(ct,
						ReflectionUtils.withReturnTypeAssignableTo(InetAddress.class),
						ReflectionUtils.withName("getAddress"), ReflectionUtils.withModifier(Modifier.PUBLIC),
						ReflectionUtils.withParametersCount(0));
				if (methods.isEmpty())
					return Optional.empty();
				Method meth = methods.iterator().next();
				return Optional.of(meth);
			});

	private static final MemoizedSupplier<List<List<String>>> DEFAULT_DNS_SERVER_SETS_S = MemoizedSupplier
			.create(() -> {
				List<List<String>> servers = new ArrayList<>();
				/** Google **/
				servers.add(Arrays.asList("8.8.8.8", "8.8.4.4"));
				/** Quad9 **/
				// servers.add(Lists.newArrayList("9.9.9.9", "149.112.112.112"));
				/** OpenDNS Home **/
				servers.add(Arrays.asList("208.67.222.222", "208.67.220.220"));
				/** Cloudflare **/
				// servers.add(Lists.newArrayList("1.1.1.1", "1.0.0.1"));
				/** CleanBrowsing **/
				// servers.add(Lists.newArrayList("185.228.168.9", "185.228.169.9"));
				/** Verisign **/
				// servers.add(Lists.newArrayList("64.6.64.6", "64.6.65.6"));
				/** Alternate DNS **/
				// servers.add(Lists.newArrayList("198.101.242.72", "23.253.163.53"));
				/** AdGuard DNS **/
				// servers.add(Lists.newArrayList("176.103.130.130", "176.103.130.131"));
				return Utils.Lots.stream(servers).map(v -> Collections.unmodifiableList(v)).toImmutableList();
			});

	public static List<List<String>> getDefaultDNSServerSets() {
		return DEFAULT_DNS_SERVER_SETS_S.get();
	}

	public static List<String> getRandomDNSServerSet() {
		List<List<String>> lists = DEFAULT_DNS_SERVER_SETS_S.get();
		List<String> dnsServers = lists.get(Utils.Crypto.getRandomInclusive(0, lists.size() - 1));
		dnsServers = new ArrayList<>(dnsServers);
		Collections.shuffle(dnsServers, Utils.Crypto.getRandom());
		return Collections.unmodifiableList(dnsServers);
	}

	public static StreamEx<Record> streamRecords(String hostname, Iterable<String> resolveServers, Runnable rateLimiter,
			Integer... types) {
		return streamRecords(hostname, resolveServers, rateLimiter, false, types);
	}

	@SuppressWarnings("deprecation")
	public static StreamEx<Record> streamRecords(String hostname, Iterable<String> resolveServers, Runnable rateLimiter,
			boolean disableTimeoutWatch, Integer... types) {
		if (Utils.Strings.isBlank(hostname))
			return StreamEx.of();
		List<String> resolveServerList = Utils.Lots.stream(resolveServers).filter(Utils.Strings::isNotBlank)
				.map(Utils.Strings::trim).distinct().toList();
		String resolverCacheKey = Utils.Crypto.hashMD5(resolveServerList.toArray(new Object[resolveServerList.size()]))
				.encodeUtf8();
		ExtendedResolver extendedResolver = RESOLVER_CACHE.get(resolverCacheKey, nil -> {
			ExtendedResolver newExtendedResolver;
			if (resolveServerList.isEmpty())
				newExtendedResolver = new ExtendedResolver();
			else {
				String[] resolveServerArr = resolveServerList.toArray(new String[resolveServerList.size()]);
				newExtendedResolver = Utils.Functions.unchecked(() -> new ExtendedResolver(resolveServerArr));
			}
			Duration timeout = Duration.ofMillis(Configs.get(NetConfig.class).dnsLookupTimeoutMillis());
			newExtendedResolver.setTimeout((int) timeout.getSeconds());
			return newExtendedResolver;
		});
		LinkedHashSet<Integer> typeSet = Utils.Lots.stream(types).nonNull()
				.map(i -> Type.value(Objects.toString(i), true)).filter(i -> i >= 0)
				.toCollection(() -> new LinkedHashSet<>());
		if (typeSet.isEmpty()) {
			typeSet.add(Type.A);
			typeSet.add(Type.AAAA);
		}
		StreamEx<Record[]> recordListStream = Utils.Lots.stream(typeSet).map(type -> {
			Future<Record[]> future = DNS_LOOKUP_EXECUTOR_SERVICE_S.get().submit(() -> {
				Lookup lookup = new Lookup(Utils.Strings.trim(hostname), type);
				lookup.setResolver(extendedResolver);
				if (rateLimiter != null)
					rateLimiter.run();
				return lookup.run();
			});
			Duration timeout = Duration.ofMillis(Configs.get(NetConfig.class).dnsLookupTimeoutMillis());
			try {
				return future.get(timeout.toMillis(), TimeUnit.MILLISECONDS);
			} catch (TimeoutException e) {
				logger.warn("dns lookup timeout after:{} hostname:{} servers:{}", timeout.toMillis(), hostname,
						resolveServerList);
				future.cancel(true);
				return null;
			} catch (InterruptedException | ExecutionException e) {
				throw Utils.Exceptions.asRuntimeException(e);
			}
		});
		return Utils.Lots.flatMap(recordListStream.map(Utils.Lots::stream)).nonNull();

	}

	public static Optional<InetAddress> getInetAddress(Record record) {
		if (record == null)
			return Optional.empty();
		Method method = GET_ADDRESS_METHOD_CACHE.get(record.getClass()).orElse(null);
		if (method == null)
			return Optional.empty();
		InetAddress addr = Utils.Functions.unchecked(() -> (InetAddress) method.invoke(record));
		return Optional.ofNullable(addr);
	}

	public static StreamEx<InetAddress> streamInetAddresses(String hostname, Iterable<String> resolveServers,
			Runnable rateLimiter, Integer... types) {
		StreamEx<Record> stream = streamRecords(hostname, resolveServers, rateLimiter, types);
		return stream.map(r -> getInetAddress(r).orElse(null)).nonNull();
	}

	public static StreamEx<String> streamIPAddresses(String hostname, Iterable<String> resolveServers,
			Runnable rateLimiter) {
		return streamInetAddresses(hostname, resolveServers, rateLimiter, Type.A, Type.AAAA)
				.map(ia -> ia.getHostAddress()).filter(Utils.Strings::isNotBlank).distinct();
	}

	public static StreamEx<String> streamIPV4Addresses(String hostname, Iterable<String> resolveServers,
			Runnable rateLimiter) {
		return streamInetAddresses(hostname, resolveServers, rateLimiter, Type.A).map(ia -> ia.getHostAddress())
				.filter(Utils.Strings::isNotBlank).filter(s -> IPs.isValidIpAddress(s)).distinct();
	}

	public static void main(String[] args) {
		StreamEx<String> stream = DNSResolvers.streamIPAddresses("google.com", DNSResolvers.getRandomDNSServerSet(),
				null);
		stream.forEach(r -> System.out.println(r));
		System.exit(0);
	}
}