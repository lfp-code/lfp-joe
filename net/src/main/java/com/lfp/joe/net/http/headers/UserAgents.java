package com.lfp.joe.net.http.headers;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.net.http.headers.uaservice.CurlUserAgent;
import com.lfp.joe.net.http.headers.uaservice.TopUserAgentsService;
import com.lfp.joe.utils.Utils;

import nl.basjes.parse.useragent.UserAgent;
import one.util.streamex.StreamEx;

public class UserAgents {

	private static final List<Supplier<UserAgentService>> USER_AGENT_SERVICES = List.of(TopUserAgentsService::get);

	public static Optional<UserAgent> getLatest(String browserName) {
		return stream(browserName).findFirst();
	}

	public static Optional<String> getLatestUserAgentString(String browserName) {
		return stream(browserName).map(UserAgent::getUserAgentString).findFirst();
	}

	public static StreamEx<UserAgent> stream(String browserName) {
		var streams = StreamEx.of(USER_AGENT_SERVICES).map(Supplier::get).nonNull().map(v -> v.stream(browserName));
		return streams.chain(Utils.Lots::flatMap).nonNull().distinct();
	}

	public static UserAgentAnalyzerLFP getAnalyzer() {
		return UserAgentAnalyzerLFP.get();
	}

	public static final Supplier<String> DEFAULT = () -> "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36";
	public static final Supplier<String> CURL = () -> CurlUserAgent.get();
	public static final Supplier<String> CHROME_LATEST = () -> getLatestUserAgentString("chrome").orElse(
			"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36");
	public static final Supplier<String> FIREFOX_LATEST = () -> getLatestUserAgentString("firefox")
			.orElse("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:77.0) Gecko/20100101 Firefox/77.0");
	public static final Supplier<String> SAFARI_LATEST = () -> getLatestUserAgentString("safari").orElse(
			"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36");
	public static final Supplier<String> EDGE_LATEST = () -> getLatestUserAgentString("edge").orElse(
			"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36 Edg/83.0.478.45");
	public static final Supplier<String> OPERA_LATEST = () -> getLatestUserAgentString("opera").orElse(
			"Mozilla/5.0 (Macintosh; Intel Mac OS X 12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.63 Safari/537.36 OPR/87.0.4390.36");
	public static final Supplier<String> INTERNET_EXPLORER_LATEST = () -> getLatestUserAgentString("ie")
			.orElse("Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko");

	public static void main(String[] args) throws IOException {
		System.out.println(UserAgents.FIREFOX_LATEST.get());
		System.out.println(UserAgents.SAFARI_LATEST.get());
		System.out.println(UserAgents.INTERNET_EXPLORER_LATEST.get());
		System.out.println(UserAgents.EDGE_LATEST.get());
		System.out.println(UserAgents.CHROME_LATEST.get());
		System.out.println(UserAgents.CURL.get());
		System.out.println(UserAgents.OPERA_LATEST.get());
		System.out.println(getLatestUserAgentString("opera").orElse(null));
		System.out.println(getLatestUserAgentString("123").orElse(null));
		var ct = CoreReflections.tryForName("one.util.streamex.Java9Specific").get();
		System.out.println(ct);
	}

}
