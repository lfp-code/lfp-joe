package com.lfp.joe.net.proxy.test;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

import javax.annotation.Generated;

import com.lfp.joe.net.auth.AuthenticatorLFP;
import com.lfp.joe.net.http.client.WebbHttpClientLFP;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.net.proxy.ProxyAuthenticator;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

public interface ProxyTester {

	static ProxyTester getDefault() {
		return new ProxyTesterImpl();
	}

	boolean isValid(Proxy proxy, BiConsumer<Proxy, IOException> errorConsumer);

	boolean isValid(Proxy proxy, Options options, BiConsumer<Proxy, IOException> errorConsumer);

	void validate(Proxy proxy) throws IOException;

	void validate(Proxy proxy, Options options) throws IOException;

	public static class Options {

		public final URI uri;
		public final Map<String, List<String>> headers;
		public final int passes;

		@Generated("SparkTools")
		private Options(Builder builder) {
			this.uri = builder.uri;
			this.headers = builder.headers;
			this.passes = builder.passes;
		}

		/**
		 * Creates builder to build {@link Options}.
		 * 
		 * @return created builder
		 */
		@Generated("SparkTools")
		public static Builder builder() {
			return new Builder();
		}

		/**
		 * Creates a builder to build {@link Options} and initialize it with the given
		 * object.
		 * 
		 * @param options to initialize the builder with
		 * @return created builder
		 */
		@Generated("SparkTools")
		public static Builder builderFrom(Options options) {
			return new Builder(options);
		}

		/**
		 * Builder to build {@link Options}.
		 */
		@Generated("SparkTools")
		public static final class Builder {
			private URI uri;
			private Map<String, List<String>> headers = Collections.emptyMap();
			private int passes;

			private Builder() {
			}

			private Builder(Options options) {
				this.uri = options.uri;
				this.headers = options.headers;
				this.passes = options.passes;
			}

			public Builder withUri(URI uri) {
				this.uri = uri;
				return this;
			}

			public Builder withHeaders(Map<String, List<String>> headers) {
				this.headers = headers;
				return this;
			}

			public Builder withPasses(int passes) {
				this.passes = passes;
				return this;
			}

			public Options build() {
				return new Options(this);
			}
		}

	}

	public static class Request {

		public static Request build(Proxy proxy, Options options) {
			return Request.builder().withProxy(proxy).withUri(options.uri).withHeaders(options.headers).build();
		}

		public final Proxy proxy;
		public final URI uri;
		public final int passIndex;
		public final Map<String, List<String>> headers;

		@Generated("SparkTools")
		private Request(Builder builder) {
			this.proxy = builder.proxy;
			this.uri = builder.uri;
			this.passIndex = builder.passIndex;
			this.headers = builder.headers;
		}

		/**
		 * Creates builder to build {@link Request}.
		 * 
		 * @return created builder
		 */
		@Generated("SparkTools")
		public static Builder builder() {
			return new Builder();
		}

		/**
		 * Creates a builder to build {@link Request} and initialize it with the given
		 * object.
		 * 
		 * @param request to initialize the builder with
		 * @return created builder
		 */
		@Generated("SparkTools")
		public static Builder builderFrom(Request request) {
			return new Builder(request);
		}

		/**
		 * Builder to build {@link Request}.
		 */
		@Generated("SparkTools")
		public static final class Builder {
			private Proxy proxy;
			private URI uri;
			private int passIndex;
			private Map<String, List<String>> headers = Collections.emptyMap();

			private Builder() {
			}

			private Builder(Request request) {
				this.proxy = request.proxy;
				this.uri = request.uri;
				this.passIndex = request.passIndex;
				this.headers = request.headers;
			}

			public Builder withProxy(Proxy proxy) {
				this.proxy = proxy;
				return this;
			}

			public Builder withUri(URI uri) {
				this.uri = uri;
				return this;
			}

			public Builder withPassIndex(int passIndex) {
				this.passIndex = passIndex;
				return this;
			}

			public Builder withHeaders(Map<String, List<String>> headers) {
				this.headers = headers;
				return this;
			}

			public Request build() {
				return new Request(this);
			}
		}

	}

	public static class Response {

		public final int statusCode;
		public final ThrowingSupplier<InputStream, IOException> body;
		public final Closeable closeable;

		@Generated("SparkTools")
		private Response(Builder builder) {
			this.statusCode = builder.statusCode;
			this.body = builder.body;
			this.closeable = builder.closeable;
		}

		/**
		 * Creates builder to build {@link Response}.
		 * 
		 * @return created builder
		 */
		@Generated("SparkTools")
		public static Builder builder() {
			return new Builder();
		}

		/**
		 * Creates a builder to build {@link Response} and initialize it with the given
		 * object.
		 * 
		 * @param response to initialize the builder with
		 * @return created builder
		 */
		@Generated("SparkTools")
		public static Builder builderFrom(Response response) {
			return new Builder(response);
		}

		/**
		 * Builder to build {@link Response}.
		 */
		@Generated("SparkTools")
		public static final class Builder {
			private int statusCode;
			private ThrowingSupplier<InputStream, IOException> body;
			private Closeable closeable;

			private Builder() {
			}

			private Builder(Response response) {
				this.statusCode = response.statusCode;
				this.body = response.body;
				this.closeable = response.closeable;
			}

			public Builder withStatusCode(int statusCode) {
				this.statusCode = statusCode;
				return this;
			}

			public Builder withBody(ThrowingSupplier<InputStream, IOException> body) {
				this.body = body;
				return this;
			}

			public Builder withCloseable(Closeable closeable) {
				this.closeable = closeable;
				return this;
			}

			public Response build() {
				return new Response(this);
			}
		}
	}

	public static void main(String[] args) throws IOException {
		Proxy proxy = Serials.Gsons.get().fromJson(
				"{\"type\":\"SOCKS_5\",\"hostname\":\"192.161.53.145\",\"port\":61336,\"username\":\"reggiepierce\",\"password\":\"Cl3aDfVc3DWeEUNLRf3CTydm5arz\"}\r\n"
						+ "",
				Proxy.class);
		ProxyTester.getDefault().validate(proxy);
		System.out.println("GOOD");
	}

}