package com.lfp.joe.net.http.headers.uaservice;

import java.io.File;
import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Redirect;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.common.io.Files;
import com.google.re2j.Pattern;
import com.lfp.joe.core.function.Asserts;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Muto.MutoBoolean;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.log.Logging;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.net.http.headers.UserAgentAnalyzerLFP;
import com.lfp.joe.net.http.headers.UserAgentService;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import nl.basjes.parse.useragent.UserAgent;
import one.util.streamex.StreamEx;

public abstract class AbstractUserAgentService implements UserAgentService {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 4;
	private static final MemoizedSupplier<HttpClient> HTTP_CLIENT_S = new MemoizedSupplier.Impl<HttpClient>(
			() -> HttpClient.newBuilder().followRedirects(Redirect.NORMAL).executor(CoreTasks.executor()).build());
	private static final List<String> PREFERRED_DEVICE_TYPE = Arrays.asList("desktop", "tablet", "mobile", "phone");
	private static final List<String> PREFERRED_BROWSERS = Arrays.asList("chrome", "safari", "ie", "opera");
	private static final List<String> PREFERRED_OS = Arrays.asList("macos", "mac os", "window", "linux", "ios",
			"android");
	private final LoadingCache<Nada, Boolean> availabilityCache;
	protected final LoadingCache<Optional<String>, List<UserAgent>> memoryCache;
	private org.slf4j.Logger logger = Logging.logger();

	protected AbstractUserAgentService(Duration availabilityRefreshInterval, Duration valueRefreshInterval) {
		super();
		Objects.requireNonNull(availabilityRefreshInterval);
		Objects.requireNonNull(valueRefreshInterval);
		this.availabilityCache = Caffeine.newBuilder().maximumSize(1).executor(CoreTasks.executor())
				.refreshAfterWrite(availabilityRefreshInterval).build(nil -> {
					Exception error;
					boolean available;
					try {
						available = loadAvailability();
						error = null;
					} catch (Exception e) {
						available = false;
						error = e;
					}
					if (!available)
						logger.warn("service is unavailable", error);
					return available;
				});
		this.memoryCache = Caffeine.newBuilder().expireAfterWrite(valueRefreshInterval)
				.expireAfterAccess(Duration.ofSeconds(5)).build(browserNameOp -> {
					var uaIterable = loadUserAgents(valueRefreshInterval, browserNameOp);
					// null when service is unavailable
					if (uaIterable == null)
						return null;
					uaIterable = filterUserAgents(uaIterable, browserNameOp);
					uaIterable = sortUserAgents(uaIterable);
					return streamOf(uaIterable).toImmutableList();
				});
	}

	@Override
	public StreamEx<UserAgent> stream(String browserName) {
		var browserNameOp = Utils.Strings.trimToNullOptional(browserName).map(Utils.Strings::lowerCase);
		var list = this.memoryCache.get(browserNameOp);
		return Utils.Lots.stream(list);
	}

	protected boolean isAvailable() {
		return availabilityCache.get(Nada.get());
	}

	protected Iterable<UserAgent> loadUserAgents(Duration valueRefreshInterval, Optional<String> browserNameOp)
			throws IOException {
		File buildDirectory;
		File storeFile;
		{
			var directory = Utils.Files.tempFileRoot(this.getClass(), VERSION, browserNameOp.orElse("#"));
			buildDirectory = new File(directory, "build");
			storeFile = new File(directory, "store.b64");
		}
		var notAvailableMuto = MutoBoolean.create();
		Utils.Files.configureDirectoryLocked(buildDirectory, (f, elapsed) -> {
			if (!isAvailable())
				// re-use cache
				return true;
			if (elapsed.toMillis() <= valueRefreshInterval.toMillis())
				return true;
			return false;
		}, f -> {
			if (!isAvailable()) {
				notAvailableMuto.setTrue();
				return;
			}
			Iterable<String> valueIterable;
			try {
				valueIterable = loadValues(browserNameOp);
			} catch (InterruptedException e) {
				throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e) : new RuntimeException(e);
			}
			var uaStream = streamOf(valueIterable).mapPartial(UserAgentAnalyzerLFP.get()::tryParse);
			uaStream = uaStream.distinct();
			try (var storeTemp = Utils.Files.tempFile(THIS_CLASS, Utils.Crypto.getRandomString() + ".temp")
					.deleteAllOnScrap(true)) {
				try (var valueEncodedStream = uaStream.map(Utils.Bits::serialize).map(Bytes::encodeBase64);
						var bfw = storeTemp.outputStream().writer()) {
					bfw.lines(valueEncodedStream);
				}
				Files.move(storeTemp, storeFile);
			}
		});
		if (notAvailableMuto.isTrue())
			return null;
		return Utils.Files.streamLines(storeFile).filter(Utils.Strings::isNotBlank).map(Utils.Bits::parseBase64)
				.map(v -> (UserAgent) Utils.Bits.deserialize(v));
	}

	protected Iterable<UserAgent> filterUserAgents(Iterable<UserAgent> uaIterable, Optional<String> browserNameOp) {
		StreamEx<UserAgent> uaStream = streamOf(uaIterable);
		if (browserNameOp.isPresent()) {
			var uaList = uaStream.toList();
			if (uaList.isEmpty())
				uaStream = StreamEx.of(uaList);
			else {
				List<Predicate<UserAgent>> predicateList = new ArrayList<>();
				predicateList.add(v -> {
					return browserNameOp.get().equalsIgnoreCase(v.getValue(UserAgent.AGENT_NAME));
				});
				predicateList.add(v -> {
					var agentName = v.getValue(UserAgent.AGENT_NAME);
					return agentName != null && agentName.toLowerCase().contains(browserNameOp.get());
				});
				boolean matched = false;
				for (var predicate : predicateList) {
					var matchList = StreamEx.of(uaList).filter(predicate).toList();
					if (!matchList.isEmpty()) {
						matched = true;
						uaList = matchList;
					}
				}
				if (!matched)
					uaList = List.of();
				uaStream = StreamEx.of(uaList);
			}
		}
		return uaStream;
	}

	protected Iterable<UserAgent> sortUserAgents(Iterable<UserAgent> uaIterable) {
		Comparator<UserAgent> comparator = (v1, v2) -> 0;
		comparator = comparator.thenComparing(v -> {
			String value = v.getValue(UserAgent.DEVICE_CLASS);
			return StreamEx.of(PREFERRED_DEVICE_TYPE).indexOf(vv -> Utils.Strings.containsIgnoreCase(value, vv))
					.orElse(Integer.MAX_VALUE);
		});
		comparator = comparator.thenComparing(v -> {
			String value = v.getValue(UserAgent.OPERATING_SYSTEM_NAME);
			return StreamEx.of(PREFERRED_OS).indexOf(vv -> Utils.Strings.containsIgnoreCase(value, vv))
					.orElse(Integer.MAX_VALUE);
		});
		comparator = comparator.thenComparing(v -> {
			String value = v.getValue(UserAgent.OPERATING_SYSTEM_VERSION_MAJOR);
			return Utils.Strings.parseNumber(value).map(Number::doubleValue).map(vv -> -1 * vv)
					.orElse(Double.MAX_VALUE);
		});
		comparator = comparator.thenComparing(v -> {
			String value = v.getValue(UserAgent.AGENT_NAME);
			return StreamEx.of(PREFERRED_BROWSERS).indexOf(vv -> Utils.Strings.containsIgnoreCase(value, vv))
					.orElse(Integer.MAX_VALUE);
		});
		comparator = comparator.thenComparing(v -> {
			String value = v.getValue(UserAgent.AGENT_VERSION_MAJOR);
			return Utils.Strings.parseNumber(value).map(Number::doubleValue).map(vv -> -1 * vv)
					.orElse(Double.MAX_VALUE);
		});
		comparator = comparator.thenComparing((v1, v2) -> {
			Function<String, List<Double>> parser = version -> {
				if (version == null)
					return new ArrayList<>();
				return Utils.Lots.stream(version.split(Pattern.quote("."))).mapPartial(Utils.Strings::parseNumber)
						.map(Number::doubleValue).toList();
			};
			var versions1 = parser.apply(v1.getValue(UserAgent.AGENT_VERSION));
			var versions2 = parser.apply(v2.getValue(UserAgent.AGENT_VERSION));
			for (var i = 0; i < Math.max(versions1.size(), versions2.size()); i++) {
				var version1 = Utils.Lots.get(versions1, i, 0d);
				var version2 = Utils.Lots.get(versions2, i, 0d);
				var comparison = version1.compareTo(version2);
				if (comparison != 0)
					return comparison;
			}
			return 0;
		});
		return streamOf(uaIterable).sorted(comparator);
	}

	protected abstract Iterable<String> loadValues(Optional<String> browserNameOp)
			throws IOException, InterruptedException;

	protected abstract boolean loadAvailability() throws IOException, InterruptedException;

	@SuppressWarnings("unchecked")
	protected static <X> StreamEx<X> streamOf(Iterable<? extends X> ible) {
		StreamEx<X> stream;
		if (ible == null)
			stream = StreamEx.empty();
		else if (ible instanceof Stream)
			stream = StreamEx.of((Stream<X>) ible);
		else {
			var spliterator = ible.spliterator();
			if (spliterator == null)
				stream = StreamEx.empty();
			else
				stream = StreamEx.of(spliterator);
		}
		stream = stream.nonNull();
		stream = stream.filter(v -> {
			if (v instanceof CharSequence && v.toString().isBlank())
				return false;
			return true;
		});
		return stream;
	}

	protected static HttpClient getHttpClient() {
		return HTTP_CLIENT_S.get();
	}

	protected static HttpRequest.Builder newHttpRequestBuilder() {
		return HttpRequest.newBuilder().header("User-Agent", CurlUserAgent.get());
	}

	protected static <X> HttpResponse<X> validate(HttpResponse<X> response) throws IOException {
		Asserts.nonNull(response);
		if (isValid(response))
			return response;
		var e = Asserts.createException("error. statusCode:%s uri:%s", response.statusCode(), response.request().uri());
		var body = response.body();
		if (body instanceof AutoCloseable)
			try {
				((AutoCloseable) body).close();
			} catch (Exception e1) {
				// suppress
			}
		throw new IOException(e);
	}

	protected static boolean isValid(HttpResponse<?> response) {
		if (response == null)
			return false;
		return response.statusCode() / 100 == 2;
	}

}
