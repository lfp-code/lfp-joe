package com.lfp.joe.net.http.ip;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URI;
import java.nio.file.Files;
import java.time.Duration;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.function.Supplier;

import org.apache.commons.lang3.Validate;
import org.threadly.concurrent.future.FutureUtils;

import com.github.benmanes.caffeine.cache.Cache;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.core.io.FileExt;
import com.lfp.joe.core.lock.KeyLock;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.request.HttpRequests;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.StreamEx;

public class IPAddressLookups {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 1;
	private static final String NULL_REPRESENTATION = "#";
	public static final Duration CACHE_TTL = Duration.ofMinutes(5);
	private static final Duration MEMORY_CACHE_EAA = Duration.ofSeconds(1);
	private static final int LOOKUP_RETRY = 3;
	private static final Bytes DEFAULT_SELF_CACHE_KEY = MachineConfig.isDeveloper()
			? Utils.Bits.from(NULL_REPRESENTATION)
			: Utils.Bits.from(Utils.Crypto.getSecureRandomString());
	private static final KeyLock<String> FILE_LOCK = KeyLock.create();
	private static final Cache<Bytes, Optional<String>> MEMORY_CACHE = Caches
			.newCaffeineBuilder(-1, MEMORY_CACHE_EAA, CACHE_TTL)
			.build();
	private static final ThrowingFunction<URI, InputStream, IOException> DEFAULT_HTTP_GETTER = uri -> {
		var request = HttpRequests.request().uri(uri);
		var response = HttpClients.send(request);
		var body = response.body();
		try {
			StatusCodes.validate(response.statusCode(), 2);
		} catch (Throwable t) {
			body.close();
			throw t;
		}
		return body;
	};
	private static final MemoizedSupplier<File> DIRECTORY_SUPPLIER = Utils.Functions.memoize(() -> {
		var dir = Utils.Files.tempFile(THIS_CLASS, VERSION, "disk-cache");
		dir.mkdirs();
		Callable<Long> reaperTask = () -> {
			long modCount = 0;
			for (var file : StreamEx.of(dir.listFiles()).map(FileExt::from)) {
				var lock = FILE_LOCK.writeLock(file.getAbsolutePath());
				if (lock.tryLock())
					try {
						if (file.lastAccessTimeElapsed(CACHE_TTL))
							modCount += file.deleteAll();
					} finally {
						lock.unlock();
					}
			}
			return modCount;
		};
		FutureUtils.executeWhile(() -> {
			var reaperFuture = Threads.Pools.centralPool()
					.submitScheduled(reaperTask, CACHE_TTL.dividedBy(2).toMillis());
			reaperFuture = reaperFuture.flatMapFailure(Throwable.class, t -> {
				if (Throws.isHalt(t))
					return FutureUtils.immediateFailureFuture(t);
				logger.error("error during reap task", t);
				return FutureUtils.immediateResultFuture(0l);
			});
			return reaperFuture;
		}, nil -> true);
		return dir;
	});

	public static Optional<String> lookup(String input) {
		return lookup(input, null);
	}

	public static Optional<String> lookup(String input, Duration refreshDuration) {
		input = Utils.Strings.lowerCase(input);
		URI uri = URIs.parse(input).orElse(null);
		if (uri != null)
			input = uri.getHost();
		input = Utils.Strings.trimToNull(input);
		if (input == null)
			return Optional.empty();
		if (IPs.isValidIpAddress(input))
			return Optional.of(input);
		var cacheKey = Utils.Bits.concat(Utils.Bits.from("#input#"), Utils.Bits.from(input));
		ThrowingSupplier<String, IOException> ipLoader = ipLoaderHost(input);
		return lookup(cacheKey, refreshDuration, ipLoader);
	}

	public static Optional<String> lookupSelfFresh() throws IOException {
		return lookupSelf(Duration.ZERO);
	}

	public static Optional<String> lookupSelfFresh(ThrowingFunction<URI, InputStream, IOException> httpGetter)
			throws IOException {
		return load(ipLoaderSelf(httpGetter));
	}

	public static Optional<String> lookupSelf() {
		return lookupSelf(null);
	}

	public static Optional<String> lookupSelf(Duration refreshDuraton) {
		var cacheKey = DEFAULT_SELF_CACHE_KEY;
		return lookupSelf(refreshDuraton, cacheKey, DEFAULT_HTTP_GETTER);
	}

	public static Optional<String> lookupSelf(Duration refreshDuraton, Bytes cacheKey,
			ThrowingFunction<URI, InputStream, IOException> httpGetter) {
		Objects.requireNonNull(cacheKey);
		cacheKey = Utils.Bits.concat(Utils.Bits.from("#self#"), cacheKey);
		ThrowingSupplier<String, IOException> ipLoader = ipLoaderSelf(httpGetter);
		return lookup(cacheKey, refreshDuraton, ipLoader);
	}

	private static Optional<String> lookup(Bytes cacheKey, Duration refreshDuraton,
			ThrowingSupplier<String, IOException> ipLoader) {
		if (refreshDuraton != null && refreshDuraton.toMillis() <= 0)
			MEMORY_CACHE.invalidate(cacheKey);
		return MEMORY_CACHE.get(cacheKey, ck -> {
			return Utils.Functions.unchecked(() -> lookupFile(ck, refreshDuraton, ipLoader));
		});
	}

	private static Optional<String> lookupFile(Bytes cacheKey, Duration refreshDuraton,
			ThrowingSupplier<String, IOException> ipLoader) throws IOException {
		var file = getFile(cacheKey);
		for (int i = 0;; i++) {
			var write = i > 0;
			var lock = FILE_LOCK.lock(file.getAbsolutePath(), write);
			lock.lock();
			try {
				var ipOp = readFromFile(getFile(cacheKey), refreshDuraton);
				if (ipOp != null)
					return ipOp;
				if (!write)
					continue;
				file.delete();
				ipOp = load(ipLoader);
				Files.write(file.toPath(), ipOp.orElse(NULL_REPRESENTATION).getBytes(Utils.Bits.getDefaultCharset()));
				return ipOp;
			} finally {
				lock.unlock();
			}
		}
	}

	private static Optional<String> load(ThrowingSupplier<String, IOException> ipLoader) throws IOException {
		if (ipLoader == null)
			return Optional.empty();
		return Utils.Strings.trimToNullOptional(ipLoader.get()).filter(IPs::isValidIpAddress);
	}

	private static FileExt getFile(Bytes cacheKey) {
		return new FileExt(DIRECTORY_SUPPLIER.get(), Utils.Crypto.hashMD5(cacheKey).encodeHex());
	}

	private static Optional<String> readFromFile(FileExt file, Duration refreshDuraton) {
		if (!file.exists())
			return null;
		if (file.lastAccessTimeElapsed(CACHE_TTL))
			return null;
		var content = Utils.Functions
				.catching(() -> Utils.Bits.from(file).encodeCharset(Utils.Bits.getDefaultCharset()), t -> null);
		if (NULL_REPRESENTATION.equals(content))
			return Optional.empty();
		if (!IPs.isValidIpAddress(content))
			return null;
		return Optional.of(content);
	}

	private static ThrowingSupplier<String, IOException> ipLoaderSelf(
			ThrowingFunction<URI, InputStream, IOException> httpGetter) {
		return () -> {
			Supplier<URI> ipLookupServiceURISupplier = getIPLookupServiceURISupplier();
			IOException toThrow = null;
			for (int i = 0; i < LOOKUP_RETRY; i++) {
				if (Thread.currentThread().isInterrupted())
					return null;
				var uri = ipLookupServiceURISupplier.get();
				String body = null;
				try {
					try (var is = Optional.ofNullable(httpGetter).orElse(DEFAULT_HTTP_GETTER).apply(uri)) {
						body = Utils.Bits.from(is).encodeCharset(Utils.Bits.getDefaultCharset());
					}
					body = Utils.Strings.trimToNull(body);
					Validate.isTrue(IPs.isValidIpAddress(body), "ip parse failed. body:%s", body);
					return body;
				} catch (Exception e) {
					var message = String.format("ip lookup failed. uri:%s body:%s", uri, body);
					toThrow = new IOException(message, e);
				}
			}
			throw toThrow;
		};
	}

	private static Supplier<URI> getIPLookupServiceURISupplier() {
		return new Supplier<URI>() {

			private Iterator<URI> serviceURIIter = null;

			@Override
			public synchronized URI get() {
				if (serviceURIIter == null || !serviceURIIter.hasNext())
					serviceURIIter = IPs.streamIPLookupServiceURIs().iterator();
				if (serviceURIIter == null || !serviceURIIter.hasNext())
					throw new NoSuchElementException();
				return serviceURIIter.next();
			}
		};
	}

	private static ThrowingSupplier<String, IOException> ipLoaderHost(String host) {
		return () -> {
			InetAddress inetAddress = Utils.Functions.catching(() -> InetAddress.getByName(host), t -> null);
			if (inetAddress == null)
				return null;
			return inetAddress.getHostAddress();
		};
	}

	public static void main(String[] args) throws IOException {
		System.out.println(IPAddressLookups.lookupSelf().get());
		System.out.println(IPAddressLookups.lookup("https://google.com").get());
		System.out.println(IPAddressLookups.lookup("google.com").get());
		// System.out.println(IPAddressLookups.lookupSelfFresh().get());
	}

}
