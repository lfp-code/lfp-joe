package com.lfp.joe.net.http.servlet;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lfp.joe.core.classpath.CoreReflections;

public interface HttpFilter extends Filter {

	@Override
	default void init(FilterConfig filterConfig) throws ServletException {}

	@Override
	default void destroy() {}

	@Override
	default void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		var hrequest = CoreReflections.tryCast(request, HttpServletRequest.class).orElse(null);
		if (hrequest == null) {
			chain.doFilter(request, response);
			return;
		}
		var hresponse = CoreReflections.tryCast(response, HttpServletResponse.class).orElse(null);
		if (hresponse == null) {
			chain.doFilter(request, response);
			return;
		}
		doFilter(hrequest, hresponse, chain);
	}

	void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException;

	public static abstract class Abs implements HttpFilter {

		@Override
		public void init(FilterConfig filterConfig) throws ServletException {}

		@Override
		public void destroy() {}
	}

}
