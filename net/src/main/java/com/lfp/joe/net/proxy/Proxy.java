package com.lfp.joe.net.proxy;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.joda.beans.Bean;
import org.joda.beans.BeanDefinition;
import org.joda.beans.ImmutableBean;
import org.joda.beans.ImmutableDefaults;
import org.joda.beans.ImmutablePreBuild;
import org.joda.beans.JodaBeanUtils;
import org.joda.beans.MetaProperty;
import org.joda.beans.Property;
import org.joda.beans.PropertyDefinition;
import org.joda.beans.impl.direct.DirectFieldsBeanBuilder;
import org.joda.beans.impl.direct.DirectMetaBean;
import org.joda.beans.impl.direct.DirectMetaProperty;
import org.joda.beans.impl.direct.DirectMetaPropertyMap;

import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.net.dns.DNSs;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Hashable;

import at.favre.lib.bytes.Bytes;

@BeanDefinition
public class Proxy implements ImmutableBean, Hashable, Comparable<Proxy>, ProxyEnabled {

	public static enum Type {
		HTTP {
			@Override
			public java.net.Proxy.Type asJdkType() {
				return java.net.Proxy.Type.HTTP;
			}

			@Override
			public String asScheme() {
				return "http";
			}
		},
		SOCKS_5 {
			@Override
			public java.net.Proxy.Type asJdkType() {
				return java.net.Proxy.Type.SOCKS;
			}

			@Override
			public String asScheme() {
				return "socks5";
			}
		};

		public static Type fromJdkType(java.net.Proxy.Type type) {
			Objects.requireNonNull(type);
			switch (type) {
			case HTTP:
				return Type.HTTP;
			case SOCKS:
				return Type.SOCKS_5;
			default:
				break;
			}
			throw new IllegalArgumentException("could not convert:" + type);
		}

		public static Optional<Type> fromScheme(String scheme) {
			if (Utils.Strings.isBlank(scheme))
				return Optional.empty();
			for (var value : values()) {
				if (scheme.equalsIgnoreCase(value.asScheme()))
					return Optional.of(value);
			}
			return Optional.empty();
		}

		public abstract java.net.Proxy.Type asJdkType();

		public abstract String asScheme();
	}

	@PropertyDefinition(validate = "notNull")
	private final Type type;

	@PropertyDefinition(validate = "notEmpty")
	private final String hostname;

	@PropertyDefinition(validate = "validatePort")
	private final int port;

	@PropertyDefinition(get = "optional")
	private final String username;

	@PropertyDefinition(get = "optional")
	private final String password;

	public boolean isAuthenticationEnabled() {
		return this.getUsername().isPresent() || this.getPassword().isPresent();
	}

	private transient InetSocketAddress _inetSocketAddress;

	public InetSocketAddress getAddress() {
		if (_inetSocketAddress == null)
			synchronized (this) {
				if (_inetSocketAddress == null) {
					// trying to save an ip lookup here
					Optional<String> ipOp = DNSs.parseIPAddressFromWildcardDNSHostname(getHostname());
					InetAddress inetAddress = Utils.Functions
							.unchecked(() -> InetAddress.getByName(ipOp.orElse(getHostname())));
					_inetSocketAddress = new InetSocketAddress(inetAddress, getPort());
				}
			}
		return _inetSocketAddress;
	}

	private transient JdkProxy _jdkProxy;

	public JdkProxy asJdkProxy() {
		if (_jdkProxy == null)
			synchronized (this) {
				if (_jdkProxy == null)
					_jdkProxy = new JdkProxy(this);
			}
		return _jdkProxy;
	}

	private transient Bytes _hash;

	@Override
	public Bytes hash() {
		if (_hash == null)
			synchronized (this) {
				if (_hash == null) {
					Proxy.Meta meta = Proxy.meta();
					_hash = JodaBeans.hash(Utils.Crypto.getMessageDigestMD5(), this, mp -> {
						return meta.metaPropertyExists(mp.name());
					}, (mp, v) -> {
						if (v instanceof Optional) {
							Optional<?> op = (Optional<?>) v;
							if (op.isEmpty())
								v = "";
							else
								v = op.get();
						}
						if (meta.hostname.equals(mp) && v instanceof String)
							v = Utils.Strings.lowerCase((String) v);
						return v;
					});
				}
			}
		return _hash;
	}

	@Override
	public Proxy getProxy() {
		return this;
	}

	@Override
	public int compareTo(Proxy o) {
		if (o == null)
			return Boolean.TRUE.compareTo(Boolean.FALSE);
		Function<Object, Comparable<?>> toComp = v -> {
			if (v == null || !(v instanceof Comparable))
				return null;
			return (Comparable<?>) v;
		};
		CompareToBuilder ctb = new CompareToBuilder();
		for (MetaProperty<?> mp : Proxy.meta().metaPropertyIterable())
			ctb = ctb.append(toComp.apply(mp.get(this)), toComp.apply(mp.get(o)));
		return ctb.toComparison();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Proxy other = (Proxy) obj;
		return Objects.equals(this.hash(), other.hash());
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.hash());
	}

	@Override
	public String toString() {
		return String.format("%s{url=%s}", Proxy.class.getSimpleName(), toUrl(false));
	}

	public URI toURI() {
		return URI.create(toUrl(true));
	}

	protected String toUrl(boolean includeUserInfo) {
		var sb = new StringBuilder();
		sb.append(this.getType().asScheme());
		sb.append("://");
		if (this.isAuthenticationEnabled()) {
			String userInfo;
			if (includeUserInfo)
				userInfo = String.format("%s:%s", this.getUsername().orElse(""), this.getPassword().orElse(""));
			else
				userInfo = "[REDACTED]";
			sb.append(userInfo + "@");
		}
		sb.append(this.getHostname());
		if (this.getPort() >= 0)
			sb.append(":" + this.getPort());
		var url = sb.toString();
		return url;
	}

	private static void validatePort(Integer port, String propertyName) {
		Objects.requireNonNull(port, "port is required");
		if (port < 0 || port > 0xFFFF)
			throw new IllegalArgumentException("port out of range:" + port);
	}

	@ImmutableDefaults
	private static void applyDefaults(Builder builder) {
		applyDefaultsProtected(builder);
	}

	protected static void applyDefaultsProtected(Builder builder) {
		builder.type(Proxy.Type.HTTP);
	}

	@ImmutablePreBuild
	private static void preBuild(Builder builder) {
		preBuildProtected(builder);
	}

	protected static void preBuildProtected(Builder builder) {

	}

	public static Proxy.Builder builder(URI proxyURI) {
		Objects.requireNonNull(proxyURI);
		var builder = Proxy.builder();
		Proxy.Type type = Proxy.Type.fromScheme(proxyURI.getScheme()).orElse(null);
		builder.type(type);
		builder.hostname(proxyURI.getHost());
		int port = proxyURI.getPort();
		if (port == -1) {
			if ("http".equalsIgnoreCase(proxyURI.getScheme()))
				port = 80;
			else if ("https".equalsIgnoreCase(proxyURI.getScheme()))
				port = 443;
		}
		builder.port(port);
		var userInfo = proxyURI.getUserInfo();
		if (Utils.Strings.isNotBlank(userInfo)) {
			int index = Utils.Strings.indexOf(userInfo, ":");
			if (index < 0) {
				builder.username("");
				builder.password(URLDecoder.decode(userInfo, StandardCharsets.UTF_8));
			} else {
				builder.username(URLDecoder.decode(userInfo.substring(0, index), StandardCharsets.UTF_8));
				builder.password(URLDecoder.decode(userInfo.substring(index + 1), StandardCharsets.UTF_8));
			}
		}
		return builder;
	}

	public static Proxy.Builder builder(java.net.Proxy jdkProxy) {
		Objects.requireNonNull(jdkProxy);
		if (jdkProxy instanceof ProxyEnabled)
			return ((ProxyEnabled) jdkProxy).getProxy().toBuilder();
		Type type = Type.fromJdkType(jdkProxy.type());
		InetSocketAddress address = Utils.Types.tryCast(jdkProxy.address(), InetSocketAddress.class).orElse(null);
		Objects.requireNonNull(address, "address must be InetSocketAddress");
		return Proxy.builder().type(type).hostname(address.getHostName()).port(address.getPort());
	}

	public static void main(String[] args) {
		System.out.println(
				Proxy.builder().type(Proxy.Type.SOCKS_5).port(69).hostname("cool.192.168.1.1.niP.iO").build().uuid(10));
		System.out.println(
				Proxy.builder().type(Proxy.Type.SOCKS_5).port(69).hostname("cool.192.168.1.1.nip.IO").build().uuid(11));
		JodaBeans.updateCode();

	}

	// ------------------------- AUTOGENERATED START -------------------------
	/// CLOVER:OFF
	/**
	 * The meta-bean for {@code Proxy}.
	 * 
	 * @return the meta-bean, not null
	 */
	public static Proxy.Meta meta() {
		return Proxy.Meta.INSTANCE;
	}

	static {
		JodaBeanUtils.registerMetaBean(Proxy.Meta.INSTANCE);
	}

	/**
	 * Returns a builder used to create an instance of the bean.
	 * 
	 * @return the builder, not null
	 */
	public static Proxy.Builder builder() {
		return new Proxy.Builder();
	}

	/**
	 * Restricted constructor.
	 * 
	 * @param builder the builder to copy from, not null
	 */
	protected Proxy(Proxy.Builder builder) {
		JodaBeanUtils.notNull(builder.type, "type");
		JodaBeanUtils.notEmpty(builder.hostname, "hostname");
		validatePort(builder.port, "port");
		this.type = builder.type;
		this.hostname = builder.hostname;
		this.port = builder.port;
		this.username = builder.username;
		this.password = builder.password;
	}

	@Override
	public Proxy.Meta metaBean() {
		return Proxy.Meta.INSTANCE;
	}

	@Override
	public <R> Property<R> property(String propertyName) {
		return metaBean().<R>metaProperty(propertyName).createProperty(this);
	}

	@Override
	public Set<String> propertyNames() {
		return metaBean().metaPropertyMap().keySet();
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the type.
	 * 
	 * @return the value of the property, not null
	 */
	public Type getType() {
		return type;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hostname.
	 * 
	 * @return the value of the property, not empty
	 */
	public String getHostname() {
		return hostname;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the port.
	 * 
	 * @return the value of the property
	 */
	public int getPort() {
		return port;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the username.
	 * 
	 * @return the optional value of the property, not null
	 */
	public Optional<String> getUsername() {
		return Optional.ofNullable(username);
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the password.
	 * 
	 * @return the optional value of the property, not null
	 */
	public Optional<String> getPassword() {
		return Optional.ofNullable(password);
	}

	// -----------------------------------------------------------------------
	/**
	 * Returns a builder that allows this bean to be mutated.
	 * 
	 * @return the mutable builder, not null
	 */
	public Builder toBuilder() {
		return new Builder(this);
	}

	// -----------------------------------------------------------------------
	/**
	 * The meta-bean for {@code Proxy}.
	 */
	public static class Meta extends DirectMetaBean {
		/**
		 * The singleton instance of the meta-bean.
		 */
		static final Meta INSTANCE = new Meta();

		/**
		 * The meta-property for the {@code type} property.
		 */
		private final MetaProperty<Type> type = DirectMetaProperty.ofImmutable(this, "type", Proxy.class, Type.class);
		/**
		 * The meta-property for the {@code hostname} property.
		 */
		private final MetaProperty<String> hostname = DirectMetaProperty.ofImmutable(this, "hostname", Proxy.class,
				String.class);
		/**
		 * The meta-property for the {@code port} property.
		 */
		private final MetaProperty<Integer> port = DirectMetaProperty.ofImmutable(this, "port", Proxy.class,
				Integer.TYPE);
		/**
		 * The meta-property for the {@code username} property.
		 */
		private final MetaProperty<String> username = DirectMetaProperty.ofImmutable(this, "username", Proxy.class,
				String.class);
		/**
		 * The meta-property for the {@code password} property.
		 */
		private final MetaProperty<String> password = DirectMetaProperty.ofImmutable(this, "password", Proxy.class,
				String.class);
		/**
		 * The meta-properties.
		 */
		private final Map<String, MetaProperty<?>> metaPropertyMap$ = new DirectMetaPropertyMap(this, null, "type",
				"hostname", "port", "username", "password");

		/**
		 * Restricted constructor.
		 */
		protected Meta() {
		}

		@Override
		protected MetaProperty<?> metaPropertyGet(String propertyName) {
			switch (propertyName.hashCode()) {
			case 3575610: // type
				return type;
			case -299803597: // hostname
				return hostname;
			case 3446913: // port
				return port;
			case -265713450: // username
				return username;
			case 1216985755: // password
				return password;
			}
			return super.metaPropertyGet(propertyName);
		}

		@Override
		public Proxy.Builder builder() {
			return new Proxy.Builder();
		}

		@Override
		public Class<? extends Proxy> beanType() {
			return Proxy.class;
		}

		@Override
		public Map<String, MetaProperty<?>> metaPropertyMap() {
			return metaPropertyMap$;
		}

		// -----------------------------------------------------------------------
		/**
		 * The meta-property for the {@code type} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Type> type() {
			return type;
		}

		/**
		 * The meta-property for the {@code hostname} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hostname() {
			return hostname;
		}

		/**
		 * The meta-property for the {@code port} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Integer> port() {
			return port;
		}

		/**
		 * The meta-property for the {@code username} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> username() {
			return username;
		}

		/**
		 * The meta-property for the {@code password} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> password() {
			return password;
		}

		// -----------------------------------------------------------------------
		@Override
		protected Object propertyGet(Bean bean, String propertyName, boolean quiet) {
			switch (propertyName.hashCode()) {
			case 3575610: // type
				return ((Proxy) bean).getType();
			case -299803597: // hostname
				return ((Proxy) bean).getHostname();
			case 3446913: // port
				return ((Proxy) bean).getPort();
			case -265713450: // username
				return ((Proxy) bean).username;
			case 1216985755: // password
				return ((Proxy) bean).password;
			}
			return super.propertyGet(bean, propertyName, quiet);
		}

		@Override
		protected void propertySet(Bean bean, String propertyName, Object newValue, boolean quiet) {
			metaProperty(propertyName);
			if (quiet) {
				return;
			}
			throw new UnsupportedOperationException("Property cannot be written: " + propertyName);
		}

	}

	// -----------------------------------------------------------------------
	/**
	 * The bean-builder for {@code Proxy}.
	 */
	public static class Builder extends DirectFieldsBeanBuilder<Proxy> {

		private Type type;
		private String hostname;
		private int port;
		private String username;
		private String password;

		/**
		 * Restricted constructor.
		 */
		protected Builder() {
			applyDefaults(this);
		}

		/**
		 * Restricted copy constructor.
		 * 
		 * @param beanToCopy the bean to copy from, not null
		 */
		protected Builder(Proxy beanToCopy) {
			this.type = beanToCopy.getType();
			this.hostname = beanToCopy.getHostname();
			this.port = beanToCopy.getPort();
			this.username = beanToCopy.username;
			this.password = beanToCopy.password;
		}

		// -----------------------------------------------------------------------
		@Override
		public Object get(String propertyName) {
			switch (propertyName.hashCode()) {
			case 3575610: // type
				return type;
			case -299803597: // hostname
				return hostname;
			case 3446913: // port
				return port;
			case -265713450: // username
				return username;
			case 1216985755: // password
				return password;
			default:
				throw new NoSuchElementException("Unknown property: " + propertyName);
			}
		}

		@Override
		public Builder set(String propertyName, Object newValue) {
			switch (propertyName.hashCode()) {
			case 3575610: // type
				this.type = (Type) newValue;
				break;
			case -299803597: // hostname
				this.hostname = (String) newValue;
				break;
			case 3446913: // port
				this.port = (Integer) newValue;
				break;
			case -265713450: // username
				this.username = (String) newValue;
				break;
			case 1216985755: // password
				this.password = (String) newValue;
				break;
			default:
				throw new NoSuchElementException("Unknown property: " + propertyName);
			}
			return this;
		}

		@Override
		public Builder set(MetaProperty<?> property, Object value) {
			super.set(property, value);
			return this;
		}

		/**
		 * @deprecated Use Joda-Convert in application code
		 */
		@Override
		@Deprecated
		public Builder setString(String propertyName, String value) {
			setString(meta().metaProperty(propertyName), value);
			return this;
		}

		/**
		 * @deprecated Use Joda-Convert in application code
		 */
		@Override
		@Deprecated
		public Builder setString(MetaProperty<?> property, String value) {
			super.setString(property, value);
			return this;
		}

		/**
		 * @deprecated Loop in application code
		 */
		@Override
		@Deprecated
		public Builder setAll(Map<String, ? extends Object> propertyValueMap) {
			super.setAll(propertyValueMap);
			return this;
		}

		@Override
		public Proxy build() {
			preBuild(this);
			return new Proxy(this);
		}

		// -----------------------------------------------------------------------
		/**
		 * Sets the type.
		 * 
		 * @param type the new value, not null
		 * @return this, for chaining, not null
		 */
		public Builder type(Type type) {
			JodaBeanUtils.notNull(type, "type");
			this.type = type;
			return this;
		}

		/**
		 * Sets the hostname.
		 * 
		 * @param hostname the new value, not empty
		 * @return this, for chaining, not null
		 */
		public Builder hostname(String hostname) {
			JodaBeanUtils.notEmpty(hostname, "hostname");
			this.hostname = hostname;
			return this;
		}

		/**
		 * Sets the port.
		 * 
		 * @param port the new value
		 * @return this, for chaining, not null
		 */
		public Builder port(int port) {
			validatePort(port, "port");
			this.port = port;
			return this;
		}

		/**
		 * Sets the username.
		 * 
		 * @param username the new value
		 * @return this, for chaining, not null
		 */
		public Builder username(String username) {
			this.username = username;
			return this;
		}

		/**
		 * Sets the password.
		 * 
		 * @param password the new value
		 * @return this, for chaining, not null
		 */
		public Builder password(String password) {
			this.password = password;
			return this;
		}

		// -----------------------------------------------------------------------
		@Override
		public String toString() {
			StringBuilder buf = new StringBuilder(192);
			buf.append("Proxy.Builder{");
			int len = buf.length();
			toString(buf);
			if (buf.length() > len) {
				buf.setLength(buf.length() - 2);
			}
			buf.append('}');
			return buf.toString();
		}

		protected void toString(StringBuilder buf) {
			buf.append("type").append('=').append(JodaBeanUtils.toString(type)).append(',').append(' ');
			buf.append("hostname").append('=').append(JodaBeanUtils.toString(hostname)).append(',').append(' ');
			buf.append("port").append('=').append(JodaBeanUtils.toString(port)).append(',').append(' ');
			buf.append("username").append('=').append(JodaBeanUtils.toString(username)).append(',').append(' ');
			buf.append("password").append('=').append(JodaBeanUtils.toString(password)).append(',').append(' ');
		}

	}

	/// CLOVER:ON
	// -------------------------- AUTOGENERATED END --------------------------
}
