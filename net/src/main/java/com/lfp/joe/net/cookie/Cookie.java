package com.lfp.joe.net.cookie;

import java.net.HttpCookie;
import java.time.Duration;
import java.util.Date;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.Validate;
import org.joda.beans.Bean;
import org.joda.beans.BeanDefinition;
import org.joda.beans.ImmutableBean;
import org.joda.beans.ImmutableDefaults;
import org.joda.beans.ImmutablePreBuild;
import org.joda.beans.JodaBeanUtils;
import org.joda.beans.MetaProperty;
import org.joda.beans.Property;
import org.joda.beans.PropertyDefinition;
import org.joda.beans.impl.direct.DirectFieldsBeanBuilder;
import org.joda.beans.impl.direct.DirectMetaBean;
import org.joda.beans.impl.direct.DirectMetaProperty;
import org.joda.beans.impl.direct.DirectMetaPropertyMap;

import com.github.throwable.beanref.BeanPath;
import com.github.throwable.beanref.BeanRef;
import com.github.throwable.beanref.BeanRoot;
import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Hashable;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.IntStreamEx;

@BeanDefinition
public class Cookie implements Hashable, ImmutableBean {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	@PropertyDefinition(validate = "notEmpty")
	private final String name;

	@PropertyDefinition
	private final String value;
	@PropertyDefinition
	private final String comment;

	@PropertyDefinition
	private final String commentURL;

	@PropertyDefinition
	private final boolean discard;

	@PropertyDefinition
	private final String domain;

	@PropertyDefinition
	private final boolean httpOnly;

	@PropertyDefinition(validate = "validate")
	private final long maxAge;

	@PropertyDefinition
	private final String path;

	@PropertyDefinition
	private final String portlist;

	@PropertyDefinition
	private final boolean secure;

	@PropertyDefinition
	private final int version;

	@PropertyDefinition(validate = "notNull", get = "manual")
	private final Date whenCreated;

	// -----------------------------------------------------------------------
	/**
	 * Gets the whenCreated.
	 * 
	 * @return the value of the property
	 */
	public Date getWhenCreated() {
		if (whenCreated == null)
			synchronized (this) {
				if (whenCreated == null) {
					var field = JodaBeans.getField(meta().whenCreated()).get();
					Utils.Functions.unchecked(() -> field.set(this, new Date()));
				}
			}
		return whenCreated;
	}

	public boolean isExpired() {
		var expiresAt = getExpiresAt().orElse(null);
		if (expiresAt == null)
			return false;
		return expiresAt.getTime() <= System.currentTimeMillis();
	}

	public Optional<Date> getExpiresAt() {
		Date whenCreated = this.getWhenCreated();
		long maxAgeSeconds = this.getMaxAge();
		if (maxAgeSeconds == -1)
			return Optional.empty();
		Date expiresAt = new Date(whenCreated.getTime() + Duration.ofSeconds(maxAgeSeconds).toMillis());
		return Optional.of(expiresAt);
	}

	public IntStreamEx streamPorts() {
		String portList = getPortlist();
		if (Utils.Strings.isBlank(portList))
			return IntStreamEx.empty();
		var portStrStream = Utils.Lots.stream(portList.split(","));
		portStrStream = portStrStream.map(Utils.Strings::trim);
		var portIntegerStream = portStrStream.map(v -> {
			return Utils.Strings.parseNumber(v).map(Number::intValue).orElse(null);
		});
		portIntegerStream = portIntegerStream.nonNull().distinct();
		portIntegerStream = portIntegerStream.filter(v -> v >= 0);
		portIntegerStream = portIntegerStream.sorted();
		return portIntegerStream.mapToInt(v -> v);
	}

	private transient Bytes _hash;

	@Override
	public Bytes hash() {
		if (_hash == null)
			synchronized (this) {
				if (_hash == null)
					_hash = JodaBeans.hash(Utils.Crypto.getMessageDigestMD5(), this, (mp, v) -> {
						if (meta().whenCreated().equals(mp))
							v = this.getExpiresAt().orElse(null);
						if (v instanceof Date)
							v = ((Date) v).getTime();
						return v;
					});
			}
		return _hash;
	}

	private transient HttpCookie _httpCookie;

	public HttpCookie toHttpCookie() {
		if (_httpCookie == null)
			synchronized (this) {
				if (_httpCookie == null) {
					_httpCookie = toHttpCookie(this);
				}
			}
		return _httpCookie;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj != null && obj.getClass() == this.getClass()) {
			Cookie other = (Cookie) obj;
			return Objects.equals(this.toHttpCookie(), other.toHttpCookie());
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = getClass().hashCode();
		hash = hash * 31 + Objects.hash(this.toHttpCookie());
		return hash;
	}

	public static Cookie.Builder builder(HttpCookie cookie) {
		Objects.requireNonNull(cookie);
		Cookie.Builder blder = Cookie.builder();
		Cookie.Meta meta = Cookie.meta();
		BeanRoot<HttpCookie> beanRoot = BeanRef.$(HttpCookie.class);
		for (BeanPath<HttpCookie, ?> beanPath : beanRoot.all()) {
			String path = beanPath.getPath();
			if ("hasExpired".equals(path))
				continue;
			MetaProperty<?> mp = meta.metaPropertyGet(beanPath.getPath());
			Objects.requireNonNull(mp, () -> "unable to find meta property for bean read:" + beanPath);
			Object value = beanPath.get(cookie);
			blder.set(mp, value);
		}
		long whenCreated = Cookies.getWhenCreated(cookie);
		if (whenCreated >= 0)
			blder.whenCreated(new Date(whenCreated));
		else
			blder.whenCreated(null);
		return blder;
	}

	public static Cookie build(HttpCookie cookie) {
		return builder(cookie).build();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static HttpCookie toHttpCookie(Cookie cookie) {
		if (cookie == null)
			return null;
		Cookie.Meta meta = Cookie.meta();
		BeanRoot<HttpCookie> beanRoot = BeanRef.$(HttpCookie.class);
		HttpCookie httpCookie = new HttpCookie(cookie.getName(), cookie.getValue());
		for (BeanPath<HttpCookie, ?> beanPath : beanRoot.all()) {
			if (beanPath.isReadOnly())
				continue;
			String path = beanPath.getPath();
			if ("hasExpired".equals(path))
				continue;
			MetaProperty<?> mp = meta.metaPropertyGet(beanPath.getPath());
			Objects.requireNonNull(mp, () -> "unable to find meta property for bean write:" + beanPath);
			Object value = mp.get(cookie);
			((BeanPath) beanPath).set(httpCookie, value);
		}
		Date whenCreated = cookie.getWhenCreated();
		if (whenCreated != null)
			Cookies.setWhenCreated(httpCookie, whenCreated.getTime());
		return httpCookie;
	}

	private static void validate(Object value, String mpName) {
		var mp = meta().metaPropertyGet(mpName);
		if (meta().maxAge().equals(mp)) {
			var valueLong = Utils.Types.tryCast(value, Long.class).orElse(null);
			Validate.isTrue(valueLong != null && valueLong >= -1, "%s invalid:%s", mp.name(), value);
		}
	}

	@ImmutablePreBuild
	private static void preBuild(Builder builder) {
		builder.path(URIs.normalizePath(builder.path));
	}

	@ImmutableDefaults
	private static void applyDefaults(Builder builder) {
		builder.whenCreated(new Date());
		builder.maxAge(-1);
	}

	public static void main(String[] args) {
		JodaBeans.updateCode();

	}

	// ------------------------- AUTOGENERATED START -------------------------
	/// CLOVER:OFF
	/**
	 * The meta-bean for {@code Cookie}.
	 * 
	 * @return the meta-bean, not null
	 */
	public static Cookie.Meta meta() {
		return Cookie.Meta.INSTANCE;
	}

	static {
		JodaBeanUtils.registerMetaBean(Cookie.Meta.INSTANCE);
	}

	/**
	 * Returns a builder used to create an instance of the bean.
	 * 
	 * @return the builder, not null
	 */
	public static Cookie.Builder builder() {
		return new Cookie.Builder();
	}

	/**
	 * Restricted constructor.
	 * 
	 * @param builder the builder to copy from, not null
	 */
	protected Cookie(Cookie.Builder builder) {
		JodaBeanUtils.notEmpty(builder.name, "name");
		validate(builder.maxAge, "maxAge");
		JodaBeanUtils.notNull(builder.whenCreated, "whenCreated");
		this.name = builder.name;
		this.value = builder.value;
		this.comment = builder.comment;
		this.commentURL = builder.commentURL;
		this.discard = builder.discard;
		this.domain = builder.domain;
		this.httpOnly = builder.httpOnly;
		this.maxAge = builder.maxAge;
		this.path = builder.path;
		this.portlist = builder.portlist;
		this.secure = builder.secure;
		this.version = builder.version;
		this.whenCreated = (Date) builder.whenCreated.clone();
	}

	@Override
	public Cookie.Meta metaBean() {
		return Cookie.Meta.INSTANCE;
	}

	@Override
	public <R> Property<R> property(String propertyName) {
		return metaBean().<R>metaProperty(propertyName).createProperty(this);
	}

	@Override
	public Set<String> propertyNames() {
		return metaBean().metaPropertyMap().keySet();
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the name.
	 * 
	 * @return the value of the property, not empty
	 */
	public String getName() {
		return name;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the value.
	 * 
	 * @return the value of the property
	 */
	public String getValue() {
		return value;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the comment.
	 * 
	 * @return the value of the property
	 */
	public String getComment() {
		return comment;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the commentURL.
	 * 
	 * @return the value of the property
	 */
	public String getCommentURL() {
		return commentURL;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the discard.
	 * 
	 * @return the value of the property
	 */
	public boolean isDiscard() {
		return discard;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the domain.
	 * 
	 * @return the value of the property
	 */
	public String getDomain() {
		return domain;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the httpOnly.
	 * 
	 * @return the value of the property
	 */
	public boolean isHttpOnly() {
		return httpOnly;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the maxAge.
	 * 
	 * @return the value of the property
	 */
	public long getMaxAge() {
		return maxAge;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the path.
	 * 
	 * @return the value of the property
	 */
	public String getPath() {
		return path;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the portlist.
	 * 
	 * @return the value of the property
	 */
	public String getPortlist() {
		return portlist;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the secure.
	 * 
	 * @return the value of the property
	 */
	public boolean isSecure() {
		return secure;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the version.
	 * 
	 * @return the value of the property
	 */
	public int getVersion() {
		return version;
	}

	// -----------------------------------------------------------------------
	/**
	 * Returns a builder that allows this bean to be mutated.
	 * 
	 * @return the mutable builder, not null
	 */
	public Builder toBuilder() {
		return new Builder(this);
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder(448);
		buf.append("Cookie{");
		int len = buf.length();
		toString(buf);
		if (buf.length() > len) {
			buf.setLength(buf.length() - 2);
		}
		buf.append('}');
		return buf.toString();
	}

	protected void toString(StringBuilder buf) {
		buf.append("name").append('=').append(JodaBeanUtils.toString(name)).append(',').append(' ');
		buf.append("value").append('=').append(JodaBeanUtils.toString(value)).append(',').append(' ');
		buf.append("comment").append('=').append(JodaBeanUtils.toString(comment)).append(',').append(' ');
		buf.append("commentURL").append('=').append(JodaBeanUtils.toString(commentURL)).append(',').append(' ');
		buf.append("discard").append('=').append(JodaBeanUtils.toString(discard)).append(',').append(' ');
		buf.append("domain").append('=').append(JodaBeanUtils.toString(domain)).append(',').append(' ');
		buf.append("httpOnly").append('=').append(JodaBeanUtils.toString(httpOnly)).append(',').append(' ');
		buf.append("maxAge").append('=').append(JodaBeanUtils.toString(maxAge)).append(',').append(' ');
		buf.append("path").append('=').append(JodaBeanUtils.toString(path)).append(',').append(' ');
		buf.append("portlist").append('=').append(JodaBeanUtils.toString(portlist)).append(',').append(' ');
		buf.append("secure").append('=').append(JodaBeanUtils.toString(secure)).append(',').append(' ');
		buf.append("version").append('=').append(JodaBeanUtils.toString(version)).append(',').append(' ');
		buf.append("whenCreated").append('=').append(JodaBeanUtils.toString(whenCreated)).append(',').append(' ');
	}

	// -----------------------------------------------------------------------
	/**
	 * The meta-bean for {@code Cookie}.
	 */
	public static class Meta extends DirectMetaBean {
		/**
		 * The singleton instance of the meta-bean.
		 */
		static final Meta INSTANCE = new Meta();

		/**
		 * The meta-property for the {@code name} property.
		 */
		private final MetaProperty<String> name = DirectMetaProperty.ofImmutable(this, "name", Cookie.class,
				String.class);
		/**
		 * The meta-property for the {@code value} property.
		 */
		private final MetaProperty<String> value = DirectMetaProperty.ofImmutable(this, "value", Cookie.class,
				String.class);
		/**
		 * The meta-property for the {@code comment} property.
		 */
		private final MetaProperty<String> comment = DirectMetaProperty.ofImmutable(this, "comment", Cookie.class,
				String.class);
		/**
		 * The meta-property for the {@code commentURL} property.
		 */
		private final MetaProperty<String> commentURL = DirectMetaProperty.ofImmutable(this, "commentURL", Cookie.class,
				String.class);
		/**
		 * The meta-property for the {@code discard} property.
		 */
		private final MetaProperty<Boolean> discard = DirectMetaProperty.ofImmutable(this, "discard", Cookie.class,
				Boolean.TYPE);
		/**
		 * The meta-property for the {@code domain} property.
		 */
		private final MetaProperty<String> domain = DirectMetaProperty.ofImmutable(this, "domain", Cookie.class,
				String.class);
		/**
		 * The meta-property for the {@code httpOnly} property.
		 */
		private final MetaProperty<Boolean> httpOnly = DirectMetaProperty.ofImmutable(this, "httpOnly", Cookie.class,
				Boolean.TYPE);
		/**
		 * The meta-property for the {@code maxAge} property.
		 */
		private final MetaProperty<Long> maxAge = DirectMetaProperty.ofImmutable(this, "maxAge", Cookie.class,
				Long.TYPE);
		/**
		 * The meta-property for the {@code path} property.
		 */
		private final MetaProperty<String> path = DirectMetaProperty.ofImmutable(this, "path", Cookie.class,
				String.class);
		/**
		 * The meta-property for the {@code portlist} property.
		 */
		private final MetaProperty<String> portlist = DirectMetaProperty.ofImmutable(this, "portlist", Cookie.class,
				String.class);
		/**
		 * The meta-property for the {@code secure} property.
		 */
		private final MetaProperty<Boolean> secure = DirectMetaProperty.ofImmutable(this, "secure", Cookie.class,
				Boolean.TYPE);
		/**
		 * The meta-property for the {@code version} property.
		 */
		private final MetaProperty<Integer> version = DirectMetaProperty.ofImmutable(this, "version", Cookie.class,
				Integer.TYPE);
		/**
		 * The meta-property for the {@code whenCreated} property.
		 */
		private final MetaProperty<Date> whenCreated = DirectMetaProperty.ofImmutable(this, "whenCreated", Cookie.class,
				Date.class);
		/**
		 * The meta-properties.
		 */
		private final Map<String, MetaProperty<?>> metaPropertyMap$ = new DirectMetaPropertyMap(this, null, "name",
				"value", "comment", "commentURL", "discard", "domain", "httpOnly", "maxAge", "path", "portlist",
				"secure", "version", "whenCreated");

		/**
		 * Restricted constructor.
		 */
		protected Meta() {
		}

		@Override
		protected MetaProperty<?> metaPropertyGet(String propertyName) {
			switch (propertyName.hashCode()) {
			case 3373707: // name
				return name;
			case 111972721: // value
				return value;
			case 950398559: // comment
				return comment;
			case 899140240: // commentURL
				return commentURL;
			case 1671366814: // discard
				return discard;
			case -1326197564: // domain
				return domain;
			case -133228460: // httpOnly
				return httpOnly;
			case -1081167621: // maxAge
				return maxAge;
			case 3433509: // path
				return path;
			case 729096351: // portlist
				return portlist;
			case -906273929: // secure
				return secure;
			case 351608024: // version
				return version;
			case 913126990: // whenCreated
				return whenCreated;
			}
			return super.metaPropertyGet(propertyName);
		}

		@Override
		public Cookie.Builder builder() {
			return new Cookie.Builder();
		}

		@Override
		public Class<? extends Cookie> beanType() {
			return Cookie.class;
		}

		@Override
		public Map<String, MetaProperty<?>> metaPropertyMap() {
			return metaPropertyMap$;
		}

		// -----------------------------------------------------------------------
		/**
		 * The meta-property for the {@code name} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> name() {
			return name;
		}

		/**
		 * The meta-property for the {@code value} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> value() {
			return value;
		}

		/**
		 * The meta-property for the {@code comment} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> comment() {
			return comment;
		}

		/**
		 * The meta-property for the {@code commentURL} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> commentURL() {
			return commentURL;
		}

		/**
		 * The meta-property for the {@code discard} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Boolean> discard() {
			return discard;
		}

		/**
		 * The meta-property for the {@code domain} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> domain() {
			return domain;
		}

		/**
		 * The meta-property for the {@code httpOnly} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Boolean> httpOnly() {
			return httpOnly;
		}

		/**
		 * The meta-property for the {@code maxAge} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Long> maxAge() {
			return maxAge;
		}

		/**
		 * The meta-property for the {@code path} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> path() {
			return path;
		}

		/**
		 * The meta-property for the {@code portlist} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> portlist() {
			return portlist;
		}

		/**
		 * The meta-property for the {@code secure} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Boolean> secure() {
			return secure;
		}

		/**
		 * The meta-property for the {@code version} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Integer> version() {
			return version;
		}

		/**
		 * The meta-property for the {@code whenCreated} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Date> whenCreated() {
			return whenCreated;
		}

		// -----------------------------------------------------------------------
		@Override
		protected Object propertyGet(Bean bean, String propertyName, boolean quiet) {
			switch (propertyName.hashCode()) {
			case 3373707: // name
				return ((Cookie) bean).getName();
			case 111972721: // value
				return ((Cookie) bean).getValue();
			case 950398559: // comment
				return ((Cookie) bean).getComment();
			case 899140240: // commentURL
				return ((Cookie) bean).getCommentURL();
			case 1671366814: // discard
				return ((Cookie) bean).isDiscard();
			case -1326197564: // domain
				return ((Cookie) bean).getDomain();
			case -133228460: // httpOnly
				return ((Cookie) bean).isHttpOnly();
			case -1081167621: // maxAge
				return ((Cookie) bean).getMaxAge();
			case 3433509: // path
				return ((Cookie) bean).getPath();
			case 729096351: // portlist
				return ((Cookie) bean).getPortlist();
			case -906273929: // secure
				return ((Cookie) bean).isSecure();
			case 351608024: // version
				return ((Cookie) bean).getVersion();
			case 913126990: // whenCreated
				return ((Cookie) bean).getWhenCreated();
			}
			return super.propertyGet(bean, propertyName, quiet);
		}

		@Override
		protected void propertySet(Bean bean, String propertyName, Object newValue, boolean quiet) {
			metaProperty(propertyName);
			if (quiet) {
				return;
			}
			throw new UnsupportedOperationException("Property cannot be written: " + propertyName);
		}

	}

	// -----------------------------------------------------------------------
	/**
	 * The bean-builder for {@code Cookie}.
	 */
	public static class Builder extends DirectFieldsBeanBuilder<Cookie> {

		private String name;
		private String value;
		private String comment;
		private String commentURL;
		private boolean discard;
		private String domain;
		private boolean httpOnly;
		private long maxAge;
		private String path;
		private String portlist;
		private boolean secure;
		private int version;
		private Date whenCreated;

		/**
		 * Restricted constructor.
		 */
		protected Builder() {
			applyDefaults(this);
		}

		/**
		 * Restricted copy constructor.
		 * 
		 * @param beanToCopy the bean to copy from, not null
		 */
		protected Builder(Cookie beanToCopy) {
			this.name = beanToCopy.getName();
			this.value = beanToCopy.getValue();
			this.comment = beanToCopy.getComment();
			this.commentURL = beanToCopy.getCommentURL();
			this.discard = beanToCopy.isDiscard();
			this.domain = beanToCopy.getDomain();
			this.httpOnly = beanToCopy.isHttpOnly();
			this.maxAge = beanToCopy.getMaxAge();
			this.path = beanToCopy.getPath();
			this.portlist = beanToCopy.getPortlist();
			this.secure = beanToCopy.isSecure();
			this.version = beanToCopy.getVersion();
			this.whenCreated = (Date) beanToCopy.getWhenCreated().clone();
		}

		// -----------------------------------------------------------------------
		@Override
		public Object get(String propertyName) {
			switch (propertyName.hashCode()) {
			case 3373707: // name
				return name;
			case 111972721: // value
				return value;
			case 950398559: // comment
				return comment;
			case 899140240: // commentURL
				return commentURL;
			case 1671366814: // discard
				return discard;
			case -1326197564: // domain
				return domain;
			case -133228460: // httpOnly
				return httpOnly;
			case -1081167621: // maxAge
				return maxAge;
			case 3433509: // path
				return path;
			case 729096351: // portlist
				return portlist;
			case -906273929: // secure
				return secure;
			case 351608024: // version
				return version;
			case 913126990: // whenCreated
				return whenCreated;
			default:
				throw new NoSuchElementException("Unknown property: " + propertyName);
			}
		}

		@Override
		public Builder set(String propertyName, Object newValue) {
			switch (propertyName.hashCode()) {
			case 3373707: // name
				this.name = (String) newValue;
				break;
			case 111972721: // value
				this.value = (String) newValue;
				break;
			case 950398559: // comment
				this.comment = (String) newValue;
				break;
			case 899140240: // commentURL
				this.commentURL = (String) newValue;
				break;
			case 1671366814: // discard
				this.discard = (Boolean) newValue;
				break;
			case -1326197564: // domain
				this.domain = (String) newValue;
				break;
			case -133228460: // httpOnly
				this.httpOnly = (Boolean) newValue;
				break;
			case -1081167621: // maxAge
				this.maxAge = (Long) newValue;
				break;
			case 3433509: // path
				this.path = (String) newValue;
				break;
			case 729096351: // portlist
				this.portlist = (String) newValue;
				break;
			case -906273929: // secure
				this.secure = (Boolean) newValue;
				break;
			case 351608024: // version
				this.version = (Integer) newValue;
				break;
			case 913126990: // whenCreated
				this.whenCreated = (Date) newValue;
				break;
			default:
				throw new NoSuchElementException("Unknown property: " + propertyName);
			}
			return this;
		}

		@Override
		public Builder set(MetaProperty<?> property, Object value) {
			super.set(property, value);
			return this;
		}

		/**
		 * @deprecated Use Joda-Convert in application code
		 */
		@Override
		@Deprecated
		public Builder setString(String propertyName, String value) {
			setString(meta().metaProperty(propertyName), value);
			return this;
		}

		/**
		 * @deprecated Use Joda-Convert in application code
		 */
		@Override
		@Deprecated
		public Builder setString(MetaProperty<?> property, String value) {
			super.setString(property, value);
			return this;
		}

		/**
		 * @deprecated Loop in application code
		 */
		@Override
		@Deprecated
		public Builder setAll(Map<String, ? extends Object> propertyValueMap) {
			super.setAll(propertyValueMap);
			return this;
		}

		@Override
		public Cookie build() {
			preBuild(this);
			return new Cookie(this);
		}

		// -----------------------------------------------------------------------
		/**
		 * Sets the name.
		 * 
		 * @param name the new value, not empty
		 * @return this, for chaining, not null
		 */
		public Builder name(String name) {
			JodaBeanUtils.notEmpty(name, "name");
			this.name = name;
			return this;
		}

		/**
		 * Sets the value.
		 * 
		 * @param value the new value
		 * @return this, for chaining, not null
		 */
		public Builder value(String value) {
			this.value = value;
			return this;
		}

		/**
		 * Sets the comment.
		 * 
		 * @param comment the new value
		 * @return this, for chaining, not null
		 */
		public Builder comment(String comment) {
			this.comment = comment;
			return this;
		}

		/**
		 * Sets the commentURL.
		 * 
		 * @param commentURL the new value
		 * @return this, for chaining, not null
		 */
		public Builder commentURL(String commentURL) {
			this.commentURL = commentURL;
			return this;
		}

		/**
		 * Sets the discard.
		 * 
		 * @param discard the new value
		 * @return this, for chaining, not null
		 */
		public Builder discard(boolean discard) {
			this.discard = discard;
			return this;
		}

		/**
		 * Sets the domain.
		 * 
		 * @param domain the new value
		 * @return this, for chaining, not null
		 */
		public Builder domain(String domain) {
			this.domain = domain;
			return this;
		}

		/**
		 * Sets the httpOnly.
		 * 
		 * @param httpOnly the new value
		 * @return this, for chaining, not null
		 */
		public Builder httpOnly(boolean httpOnly) {
			this.httpOnly = httpOnly;
			return this;
		}

		/**
		 * Sets the maxAge.
		 * 
		 * @param maxAge the new value
		 * @return this, for chaining, not null
		 */
		public Builder maxAge(long maxAge) {
			validate(maxAge, "maxAge");
			this.maxAge = maxAge;
			return this;
		}

		/**
		 * Sets the path.
		 * 
		 * @param path the new value
		 * @return this, for chaining, not null
		 */
		public Builder path(String path) {
			this.path = path;
			return this;
		}

		/**
		 * Sets the portlist.
		 * 
		 * @param portlist the new value
		 * @return this, for chaining, not null
		 */
		public Builder portlist(String portlist) {
			this.portlist = portlist;
			return this;
		}

		/**
		 * Sets the secure.
		 * 
		 * @param secure the new value
		 * @return this, for chaining, not null
		 */
		public Builder secure(boolean secure) {
			this.secure = secure;
			return this;
		}

		/**
		 * Sets the version.
		 * 
		 * @param version the new value
		 * @return this, for chaining, not null
		 */
		public Builder version(int version) {
			this.version = version;
			return this;
		}

		/**
		 * Sets the whenCreated.
		 * 
		 * @param whenCreated the new value, not null
		 * @return this, for chaining, not null
		 */
		public Builder whenCreated(Date whenCreated) {
			JodaBeanUtils.notNull(whenCreated, "whenCreated");
			this.whenCreated = whenCreated;
			return this;
		}

		// -----------------------------------------------------------------------
		@Override
		public String toString() {
			StringBuilder buf = new StringBuilder(448);
			buf.append("Cookie.Builder{");
			int len = buf.length();
			toString(buf);
			if (buf.length() > len) {
				buf.setLength(buf.length() - 2);
			}
			buf.append('}');
			return buf.toString();
		}

		protected void toString(StringBuilder buf) {
			buf.append("name").append('=').append(JodaBeanUtils.toString(name)).append(',').append(' ');
			buf.append("value").append('=').append(JodaBeanUtils.toString(value)).append(',').append(' ');
			buf.append("comment").append('=').append(JodaBeanUtils.toString(comment)).append(',').append(' ');
			buf.append("commentURL").append('=').append(JodaBeanUtils.toString(commentURL)).append(',').append(' ');
			buf.append("discard").append('=').append(JodaBeanUtils.toString(discard)).append(',').append(' ');
			buf.append("domain").append('=').append(JodaBeanUtils.toString(domain)).append(',').append(' ');
			buf.append("httpOnly").append('=').append(JodaBeanUtils.toString(httpOnly)).append(',').append(' ');
			buf.append("maxAge").append('=').append(JodaBeanUtils.toString(maxAge)).append(',').append(' ');
			buf.append("path").append('=').append(JodaBeanUtils.toString(path)).append(',').append(' ');
			buf.append("portlist").append('=').append(JodaBeanUtils.toString(portlist)).append(',').append(' ');
			buf.append("secure").append('=').append(JodaBeanUtils.toString(secure)).append(',').append(' ');
			buf.append("version").append('=').append(JodaBeanUtils.toString(version)).append(',').append(' ');
			buf.append("whenCreated").append('=').append(JodaBeanUtils.toString(whenCreated)).append(',').append(' ');
		}

	}

	/// CLOVER:ON
	// -------------------------- AUTOGENERATED END --------------------------
}
