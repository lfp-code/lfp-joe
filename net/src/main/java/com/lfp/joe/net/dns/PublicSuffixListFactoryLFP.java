package com.lfp.joe.net.dns;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Properties;

import org.reflections8.ReflectionUtils;

import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingFunction;

import de.malkusch.whoisServerList.publicSuffixList.PublicSuffixList;
import de.malkusch.whoisServerList.publicSuffixList.PublicSuffixListFactory;
import de.malkusch.whoisServerList.publicSuffixList.index.Index;
import de.malkusch.whoisServerList.publicSuffixList.index.IndexFactory;
import de.malkusch.whoisServerList.publicSuffixList.parser.Parser;
import de.malkusch.whoisServerList.publicSuffixList.rule.Rule;

@SuppressWarnings("unchecked")
public enum PublicSuffixListFactoryLFP implements ThrowingFunction<InputStream, PublicSuffixList, IOException> {
	INSTANCE;

	private static final Method PublicSuffixListFactory_loadIndexFactory_METHOD;
	static {
		var meths = JavaCode.Reflections.streamMethods(PublicSuffixListFactory.class, true,
				v -> "loadIndexFactory".equals(v.getName()), v -> v.getParameterCount() == 1,
				v -> String.class.isAssignableFrom(v.getParameterTypes()[0]));
		PublicSuffixListFactory_loadIndexFactory_METHOD = Utils.Lots.one(meths);
	}
	private static final Constructor<PublicSuffixList> PublicSuffixList_CONSTRUCTOR;
	static {
		var ctorList = JavaCode.Reflections.getAllConstructors(PublicSuffixList.class,
				ReflectionUtils.withParametersAssignableFrom(Index.class, URL.class, Charset.class));
		var ctors = Utils.Lots.stream(ctorList).map(v -> {
			v.setAccessible(true);
			return v;
		});
		PublicSuffixList_CONSTRUCTOR = Utils.Lots.one(ctors);
	}

	@Override
	public PublicSuffixList apply(InputStream input) throws IOException {
		try {
			return applyInternal(input);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| InstantiationException e) {
			throw Utils.Exceptions.as(e, IOException.class);
		}
	}

	protected PublicSuffixList applyInternal(InputStream input) throws IOException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, InstantiationException {
		PublicSuffixListFactory factory = new PublicSuffixListFactory();
		Properties properties = factory.getDefaults();
		URL url = new URL(properties.getProperty(PublicSuffixListFactory.PROPERTY_URL));
		Charset charset = Charset.forName(properties.getProperty(PublicSuffixListFactory.PROPERTY_CHARSET));
		IndexFactory indexFactory = (IndexFactory) PublicSuffixListFactory_loadIndexFactory_METHOD.invoke(factory,
				properties.getProperty(PublicSuffixListFactory.PROPERTY_INDEX_FACTORY));
		Parser parser = new Parser();
		List<Rule> rules = parser.parse(input, charset);
		Index index = indexFactory.build(rules);
		return PublicSuffixList_CONSTRUCTOR.newInstance(index, url, charset);
	}

}
