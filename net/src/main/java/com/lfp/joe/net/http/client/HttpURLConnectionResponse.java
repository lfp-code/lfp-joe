package com.lfp.joe.net.http.client;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

import org.joda.beans.Bean;
import org.joda.beans.BeanDefinition;
import org.joda.beans.ImmutableBean;
import org.joda.beans.ImmutableDefaults;
import org.joda.beans.JodaBeanUtils;
import org.joda.beans.MetaProperty;
import org.joda.beans.Property;
import org.joda.beans.PropertyDefinition;
import org.joda.beans.impl.direct.DirectFieldsBeanBuilder;
import org.joda.beans.impl.direct.DirectMetaBean;
import org.joda.beans.impl.direct.DirectMetaProperty;
import org.joda.beans.impl.direct.DirectMetaPropertyMap;

import com.google.common.collect.ImmutableMap;
import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.utils.Utils;

import one.util.streamex.EntryStream;

@BeanDefinition
public class HttpURLConnectionResponse implements ImmutableBean, Closeable {

	@PropertyDefinition
	private final int statusCode;

	@PropertyDefinition(get = "manual")
	private final Map<String, List<String>> headers;

	public EntryStream<String, String> streamHeaders() {
		if (headers == null)
			return EntryStream.empty();
		return Utils.Lots.stream(headers.keySet(), k -> Utils.Lots.stream(headers.get(k))).nonNullKeys()
				.mapValues(v -> Optional.ofNullable(v).orElse(""));
	}

	/**
	 * Gets the headers.
	 * 
	 * @return the value of the property, not null
	 */
	public Map<String, List<String>> getHeaders() {
		return streamHeaders().grouping();
	}

	@PropertyDefinition(validate = "notNull")
	private final InputStream body;

	@Override
	public void close() throws IOException {
		if (body != null)
			body.close();
	}

	@ImmutableDefaults
	private static void applyDefaults(Builder builder) {
		builder.headers(Collections.emptyMap());
	}

	public static void main(String[] args) {
		JodaBeans.updateCode();
	}

	// ------------------------- AUTOGENERATED START -------------------------
	/// CLOVER:OFF
	/**
	 * The meta-bean for {@code HttpURLConnectionResponse}.
	 * 
	 * @return the meta-bean, not null
	 */
	public static HttpURLConnectionResponse.Meta meta() {
		return HttpURLConnectionResponse.Meta.INSTANCE;
	}

	static {
		JodaBeanUtils.registerMetaBean(HttpURLConnectionResponse.Meta.INSTANCE);
	}

	/**
	 * Returns a builder used to create an instance of the bean.
	 * 
	 * @return the builder, not null
	 */
	public static HttpURLConnectionResponse.Builder builder() {
		return new HttpURLConnectionResponse.Builder();
	}

	/**
	 * Restricted constructor.
	 * 
	 * @param builder the builder to copy from, not null
	 */
	protected HttpURLConnectionResponse(HttpURLConnectionResponse.Builder builder) {
		JodaBeanUtils.notNull(builder.body, "body");
		this.statusCode = builder.statusCode;
		this.headers = (builder.headers != null ? ImmutableMap.copyOf(builder.headers) : null);
		this.body = builder.body;
	}

	@Override
	public HttpURLConnectionResponse.Meta metaBean() {
		return HttpURLConnectionResponse.Meta.INSTANCE;
	}

	@Override
	public <R> Property<R> property(String propertyName) {
		return metaBean().<R>metaProperty(propertyName).createProperty(this);
	}

	@Override
	public Set<String> propertyNames() {
		return metaBean().metaPropertyMap().keySet();
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the statusCode.
	 * 
	 * @return the value of the property
	 */
	public int getStatusCode() {
		return statusCode;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the body.
	 * 
	 * @return the value of the property, not null
	 */
	public InputStream getBody() {
		return body;
	}

	// -----------------------------------------------------------------------
	/**
	 * Returns a builder that allows this bean to be mutated.
	 * 
	 * @return the mutable builder, not null
	 */
	public Builder toBuilder() {
		return new Builder(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj != null && obj.getClass() == this.getClass()) {
			HttpURLConnectionResponse other = (HttpURLConnectionResponse) obj;
			return (statusCode == other.statusCode) && JodaBeanUtils.equal(headers, other.headers)
					&& JodaBeanUtils.equal(body, other.body);
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = getClass().hashCode();
		hash = hash * 31 + JodaBeanUtils.hashCode(statusCode);
		hash = hash * 31 + JodaBeanUtils.hashCode(headers);
		hash = hash * 31 + JodaBeanUtils.hashCode(body);
		return hash;
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder(128);
		buf.append("HttpURLConnectionResponse{");
		int len = buf.length();
		toString(buf);
		if (buf.length() > len) {
			buf.setLength(buf.length() - 2);
		}
		buf.append('}');
		return buf.toString();
	}

	protected void toString(StringBuilder buf) {
		buf.append("statusCode").append('=').append(JodaBeanUtils.toString(statusCode)).append(',').append(' ');
		buf.append("headers").append('=').append(JodaBeanUtils.toString(headers)).append(',').append(' ');
		buf.append("body").append('=').append(JodaBeanUtils.toString(body)).append(',').append(' ');
	}

	// -----------------------------------------------------------------------
	/**
	 * The meta-bean for {@code HttpURLConnectionResponse}.
	 */
	public static class Meta extends DirectMetaBean {
		/**
		 * The singleton instance of the meta-bean.
		 */
		static final Meta INSTANCE = new Meta();

		/**
		 * The meta-property for the {@code statusCode} property.
		 */
		private final MetaProperty<Integer> statusCode = DirectMetaProperty.ofImmutable(this, "statusCode",
				HttpURLConnectionResponse.class, Integer.TYPE);
		/**
		 * The meta-property for the {@code headers} property.
		 */
		@SuppressWarnings({ "unchecked", "rawtypes" })
		private final MetaProperty<Map<String, List<String>>> headers = DirectMetaProperty.ofImmutable(this, "headers",
				HttpURLConnectionResponse.class, (Class) Map.class);
		/**
		 * The meta-property for the {@code body} property.
		 */
		private final MetaProperty<InputStream> body = DirectMetaProperty.ofImmutable(this, "body",
				HttpURLConnectionResponse.class, InputStream.class);
		/**
		 * The meta-properties.
		 */
		private final Map<String, MetaProperty<?>> metaPropertyMap$ = new DirectMetaPropertyMap(this, null,
				"statusCode", "headers", "body");

		/**
		 * Restricted constructor.
		 */
		protected Meta() {
		}

		@Override
		protected MetaProperty<?> metaPropertyGet(String propertyName) {
			switch (propertyName.hashCode()) {
			case 247507199: // statusCode
				return statusCode;
			case 795307910: // headers
				return headers;
			case 3029410: // body
				return body;
			}
			return super.metaPropertyGet(propertyName);
		}

		@Override
		public HttpURLConnectionResponse.Builder builder() {
			return new HttpURLConnectionResponse.Builder();
		}

		@Override
		public Class<? extends HttpURLConnectionResponse> beanType() {
			return HttpURLConnectionResponse.class;
		}

		@Override
		public Map<String, MetaProperty<?>> metaPropertyMap() {
			return metaPropertyMap$;
		}

		// -----------------------------------------------------------------------
		/**
		 * The meta-property for the {@code statusCode} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Integer> statusCode() {
			return statusCode;
		}

		/**
		 * The meta-property for the {@code headers} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Map<String, List<String>>> headers() {
			return headers;
		}

		/**
		 * The meta-property for the {@code body} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<InputStream> body() {
			return body;
		}

		// -----------------------------------------------------------------------
		@Override
		protected Object propertyGet(Bean bean, String propertyName, boolean quiet) {
			switch (propertyName.hashCode()) {
			case 247507199: // statusCode
				return ((HttpURLConnectionResponse) bean).getStatusCode();
			case 795307910: // headers
				return ((HttpURLConnectionResponse) bean).getHeaders();
			case 3029410: // body
				return ((HttpURLConnectionResponse) bean).getBody();
			}
			return super.propertyGet(bean, propertyName, quiet);
		}

		@Override
		protected void propertySet(Bean bean, String propertyName, Object newValue, boolean quiet) {
			metaProperty(propertyName);
			if (quiet) {
				return;
			}
			throw new UnsupportedOperationException("Property cannot be written: " + propertyName);
		}

	}

	// -----------------------------------------------------------------------
	/**
	 * The bean-builder for {@code HttpURLConnectionResponse}.
	 */
	public static class Builder extends DirectFieldsBeanBuilder<HttpURLConnectionResponse> {

		private int statusCode;
		private Map<String, List<String>> headers;
		private InputStream body;

		/**
		 * Restricted constructor.
		 */
		protected Builder() {
			applyDefaults(this);
		}

		/**
		 * Restricted copy constructor.
		 * 
		 * @param beanToCopy the bean to copy from, not null
		 */
		protected Builder(HttpURLConnectionResponse beanToCopy) {
			this.statusCode = beanToCopy.getStatusCode();
			this.headers = (beanToCopy.getHeaders() != null ? ImmutableMap.copyOf(beanToCopy.getHeaders()) : null);
			this.body = beanToCopy.getBody();
		}

		// -----------------------------------------------------------------------
		@Override
		public Object get(String propertyName) {
			switch (propertyName.hashCode()) {
			case 247507199: // statusCode
				return statusCode;
			case 795307910: // headers
				return headers;
			case 3029410: // body
				return body;
			default:
				throw new NoSuchElementException("Unknown property: " + propertyName);
			}
		}

		@SuppressWarnings("unchecked")
		@Override
		public Builder set(String propertyName, Object newValue) {
			switch (propertyName.hashCode()) {
			case 247507199: // statusCode
				this.statusCode = (Integer) newValue;
				break;
			case 795307910: // headers
				this.headers = (Map<String, List<String>>) newValue;
				break;
			case 3029410: // body
				this.body = (InputStream) newValue;
				break;
			default:
				throw new NoSuchElementException("Unknown property: " + propertyName);
			}
			return this;
		}

		@Override
		public Builder set(MetaProperty<?> property, Object value) {
			super.set(property, value);
			return this;
		}

		/**
		 * @deprecated Use Joda-Convert in application code
		 */
		@Override
		@Deprecated
		public Builder setString(String propertyName, String value) {
			setString(meta().metaProperty(propertyName), value);
			return this;
		}

		/**
		 * @deprecated Use Joda-Convert in application code
		 */
		@Override
		@Deprecated
		public Builder setString(MetaProperty<?> property, String value) {
			super.setString(property, value);
			return this;
		}

		/**
		 * @deprecated Loop in application code
		 */
		@Override
		@Deprecated
		public Builder setAll(Map<String, ? extends Object> propertyValueMap) {
			super.setAll(propertyValueMap);
			return this;
		}

		@Override
		public HttpURLConnectionResponse build() {
			return new HttpURLConnectionResponse(this);
		}

		// -----------------------------------------------------------------------
		/**
		 * Sets the statusCode.
		 * 
		 * @param statusCode the new value
		 * @return this, for chaining, not null
		 */
		public Builder statusCode(int statusCode) {
			this.statusCode = statusCode;
			return this;
		}

		/**
		 * Sets the headers.
		 * 
		 * @param headers the new value
		 * @return this, for chaining, not null
		 */
		public Builder headers(Map<String, List<String>> headers) {
			this.headers = headers;
			return this;
		}

		/**
		 * Sets the body.
		 * 
		 * @param body the new value, not null
		 * @return this, for chaining, not null
		 */
		public Builder body(InputStream body) {
			JodaBeanUtils.notNull(body, "body");
			this.body = body;
			return this;
		}

		// -----------------------------------------------------------------------
		@Override
		public String toString() {
			StringBuilder buf = new StringBuilder(128);
			buf.append("HttpURLConnectionResponse.Builder{");
			int len = buf.length();
			toString(buf);
			if (buf.length() > len) {
				buf.setLength(buf.length() - 2);
			}
			buf.append('}');
			return buf.toString();
		}

		protected void toString(StringBuilder buf) {
			buf.append("statusCode").append('=').append(JodaBeanUtils.toString(statusCode)).append(',').append(' ');
			buf.append("headers").append('=').append(JodaBeanUtils.toString(headers)).append(',').append(' ');
			buf.append("body").append('=').append(JodaBeanUtils.toString(body)).append(',').append(' ');
		}

	}

	/// CLOVER:ON
	// -------------------------- AUTOGENERATED END --------------------------
}
