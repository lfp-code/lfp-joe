package com.lfp.joe.net.proxy;

import java.net.PasswordAuthentication;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import com.github.throwable.beanref.BeanProperty;
import com.github.throwable.beanref.BeanRef;
import com.lfp.joe.net.auth.ImmutableAuthenticatorLFP.AuthenticatorContext;
import com.lfp.joe.net.dns.DNSs;
import com.lfp.joe.utils.Utils;

public class ProxyAuthenticator implements Function<AuthenticatorContext, PasswordAuthentication> {
	private static final List<BeanProperty<Proxy, ?>> COMPARE_PROPERTY_LIST = Arrays.asList(BeanRef.$(Proxy::getType),
			BeanRef.$(Proxy::getPort), BeanRef.$(Proxy::getUsername), BeanRef.$(Proxy::getPassword));
	private final Proxy proxy;

	public ProxyAuthenticator(Proxy proxy) {
		super();
		this.proxy = Objects.requireNonNull(proxy);
	}

	@Override
	public PasswordAuthentication apply(AuthenticatorContext authenticatorContext) {
		String requestingHost = authenticatorContext.host().orElse(null);
		if (Utils.Strings.isBlank(requestingHost))
			return null;
		requestingHost = normalizeHostname(requestingHost);
		if (!proxy.getUsername().isPresent() && !proxy.getPassword().isPresent())
			return null;
		if (!Objects.equals(proxy.getPort(), authenticatorContext.port()))
			return null;
		String proxyHostname = normalizeHostname(proxy.getHostname());
		if (!Utils.Strings.equalsIgnoreCase(requestingHost, proxyHostname))
			return null;
		return new PasswordAuthentication(proxy.getUsername().orElse(""), proxy.getPassword().orElse("").toCharArray());
	}

	public Proxy getProxy() {
		return proxy;
	}

	protected String normalizeHostname(final String hostname) {
		Objects.requireNonNull(hostname);
		Optional<String> ipOp = DNSs.parseIPAddressFromWildcardDNSHostname(hostname);
		String result;
		if (ipOp.isPresent())
			result = ipOp.get();
		else
			result = hostname;
		result = Utils.Strings.lowerCase(result);
		return result;
	}

	@Override
	public int hashCode() {
		Stream<Object> stream = COMPARE_PROPERTY_LIST.stream().map(bp -> bp.get(proxy));
		Object[] arr = Utils.Lots.stream(stream).append(normalizeHostname(this.proxy.getHostname()))
				.toArray(Object[]::new);
		return Objects.hash(arr);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProxyAuthenticator other = (ProxyAuthenticator) obj;
		if (proxy == null) {
			if (other.proxy != null)
				return false;
		}
		for (BeanProperty<Proxy, ?> bp : COMPARE_PROPERTY_LIST) {
			Object tValue = bp.get(this.proxy);
			Object oValue = bp.get(other.proxy);
			if (!Objects.equals(tValue, oValue))
				return false;
		}
		if (!Objects.equals(normalizeHostname(this.proxy.getHostname()),
				other.normalizeHostname(other.proxy.getHostname())))
			return false;
		return true;
	}

}
