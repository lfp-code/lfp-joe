package com.lfp.joe.net.socket.socks;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.time.StopWatch;

import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.process.Procs;
import com.lfp.joe.process.Which;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

public enum PortAllocator implements ThrowingSupplier<Integer, IOException> {
	INSTANCE;

	private Optional<File> pythonOp;

	@Override
	public Integer get() throws IOException {
		Integer port = getPython();
		if (port != null)
			return port;
		try (ServerSocket socket = new ServerSocket(0);) {
			return socket.getLocalPort();
		}
	}

	private Integer getPython() {
		if (this.pythonOp == null)
			synchronized (this) {
				if (this.pythonOp == null) {
					var setPythonOp = Which.get("python");
					if (setPythonOp.isEmpty())
						this.pythonOp = setPythonOp;
					else {
						var result = getPython(setPythonOp);
						if (result == null)
							this.pythonOp = Optional.empty();
						else
							this.pythonOp = setPythonOp;
						return result;
					}
				}
			}
		return getPython(this.pythonOp);
	}

	private static Integer getPython(Optional<File> pythonOp) {
		if (pythonOp.isEmpty())
			return null;
		var commands = List.of(pythonOp.get().getAbsolutePath(), "-c",
				"import socket; s=socket.socket(); s.bind(('', 0)); print(s.getsockname()[1]); s.close()");
		var pr = Utils.Functions.catching(() -> {
			var pe = Procs.processExecutor();
			pe.command(commands).disableOutputLog().readOutput(true).exitValueNormal();
			return pe.start().join();
		}, t -> null);
		if (pr == null || pr.getExitValue() != 0)
			return null;
		var lines = StreamEx.of(pr.getOutput().getLinesAsUTF8());
		lines = lines.map(Utils.Strings::trimToNull);
		lines = lines.nonNull();
		var portStream = lines.map(v -> Utils.Strings.parseNumber(v).map(Number::intValue).orElse(null));
		portStream = portStream.nonNull();
		portStream = portStream.filter(v -> v > 0);
		return portStream.findFirst().orElse(null);
	}

	public static void main(String[] args) throws IOException {
		for (int i = 0; i < 10 + 1; i++) {
			var sw = i == 0 ? null : StopWatch.createStarted();
			var port = PortAllocator.INSTANCE.get();
			System.out.println("pass1:" + port + " " + (sw == null ? null : sw.getTime()));
		}
		for (int i = 0; i < 10 + 1; i++) {
			var sw = i == 0 ? null : StopWatch.createStarted();
			int port;
			try (ServerSocket socket = new ServerSocket(0);) {
				port = socket.getLocalPort();
			}
			System.out.println("pass2:" + port + " " + (sw == null ? null : sw.getTime()));
		}
	}
}
