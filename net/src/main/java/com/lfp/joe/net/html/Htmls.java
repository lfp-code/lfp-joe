package com.lfp.joe.net.html;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.nibor.autolink.LinkExtractor;
import org.nibor.autolink.LinkType;

import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.KeyGenerator;

import one.util.streamex.StreamEx;

public class Htmls {

	private static final Pattern HTML_PATTERN = Pattern
			.compile("</?\\w+((\\s+\\w+(\\s*=\\s*(?:\".*?\"|'.*?'|[^'\">\\s]+))?)+\\s*|\\s*)/?>");

	public static boolean isHtml(String input) {
		if (Utils.Strings.isBlank(input))
			return false;
		return HTML_PATTERN.matcher(input).find();
	}

	public static StreamEx<String> extractLinks(String text, LinkType... linkTypes) {
		if (StringUtils.isBlank(text))
			return StreamEx.of();
		StreamEx<String> result = StreamEx.empty();
		{// use library
			var spanStream = Utils.Lots.defer(() -> {
				var linkTypesSet = Utils.Lots.stream(linkTypes).nonNull().toCollection(LinkedHashSet::new);
				if (linkTypesSet.isEmpty())
					linkTypesSet.addAll(Arrays.asList(LinkType.values()));
				LinkExtractor linkExtractor = LinkExtractor.builder().linkTypes(linkTypesSet).build();
				var iter = linkExtractor.extractLinks(text).iterator();
				return Utils.Lots.stream(iter);
			});
			var stream = spanStream.map(linkSpan -> {
				String str = text.substring(linkSpan.getBeginIndex(), linkSpan.getEndIndex());
				if (Utils.Strings.isBlank(str))
					return null;
				str = str.replaceAll("\\u00A0", " ");
				str = str.replaceAll("(\\r|\\n)", "");
				return str;
			});
			result = result.append(stream);
		}
		{// parse html doc
			var uriStream = Utils.Lots.defer(() -> {
				var doc = Jsoups.tryParse(text, null, null).orElse(null);
				if (doc == null)
					return StreamEx.empty();
				var uriStreams = Utils.Lots.stream("src", "href").map(attr -> {
					var elStream = Jsoups.select(doc, String.format("[%s]", attr));
					return elStream.map(v -> URIs.parse(v.absUrl(attr)).orElse(null));
				});
				return Utils.Lots.flatMap(uriStreams);
			});
			uriStream = uriStream.nonNull();
			result = result.append(uriStream.map(Objects::toString));
		}
		return result.filter(Utils.Strings::isNotBlank).map(Utils.Strings::trim);
	}

	public static String stripHtml(String input) {
		return Jsoups.tryParse(input, null, null).map(v -> v.text()).map(Utils.Strings::trim).orElse(input);
	}

	public static String generateRandomKey(Object... prepend) {
		return generateRandomKey(false, prepend);
	}

	public static String generateSecureRandomKey(Object... prepend) {
		return generateRandomKey(true, prepend);
	}

	private static String generateRandomKey(boolean secure, Object[] prepend) {
		String str;
		if (secure)
			str = Utils.Crypto.getSecureRandomString();
		else
			str = Utils.Crypto.getRandomString();
		return generateKey(Utils.Lots.stream(prepend).append(str).nonNull().toArray(Object.class));
	}

	// generate safe keys for attributes for style names
	public static String generateKey(Object... parts) {
		return KeyGenerator.apply(parts);
	}

	public static void main(String[] args) {
		System.out.println(generateKey("SemaphoreAccessor123213", "((((cool's neat wow fun*", 12321321, "  ok  "));
	}
}
