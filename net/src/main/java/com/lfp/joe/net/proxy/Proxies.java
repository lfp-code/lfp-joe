package com.lfp.joe.net.proxy;

import com.lfp.joe.serial.Serials;

public class Proxies {

	public static Proxy createHttp(String hostname, int port) {
		return createHttp(hostname, port, null, null);
	}

	public static Proxy createHttp(String hostname, int port, String username, String password) {
		return createBuilder(hostname, port, username, password).type(Proxy.Type.HTTP).build();
	}

	public static Proxy createSocks5(String hostname, int port) {
		return createSocks5(hostname, port, null, null);
	}

	public static Proxy createSocks5(String hostname, int port, String username, String password) {
		return createBuilder(hostname, port, username, password).type(Proxy.Type.SOCKS_5).build();
	}

	private static final Proxy.Builder createBuilder(String hostname, int port, String username, String password) {
		return Proxy.builder().hostname(hostname).port(port).username(username).password(password);
	}

	public static void main(String[] args) {
		Proxy p = Proxies.createHttp("localhost", 80, "hi", null);
		System.out.println(p.getAddress());
		System.out.println(p.getUsername().orElse(null));
		System.out.println(p.getPassword().orElse(null));
		String json = Serials.Gsons.get().toJson(p);
		System.out.println(json);
		Proxy p2 = Serials.Gsons.get().fromJson(json, Proxy.class);
		System.out.println(p2.equals(p));
	}

}
