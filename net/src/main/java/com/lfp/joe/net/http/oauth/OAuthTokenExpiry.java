package com.lfp.joe.net.http.oauth;

import java.time.Duration;
import java.util.function.Supplier;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.lfp.joe.cache.StatValue;
import com.lfp.joe.cache.caffeine.Caffeines;
import com.lfp.joe.cache.caffeine.TTLExpiry;

import one.util.streamex.StreamEx;

public class OAuthTokenExpiry<K> implements TTLExpiry<K, StatValue<OAuthTokenResponse>> {

	private final float maxLeaseUsage;
	private final Duration expireAfterAccess;

	public OAuthTokenExpiry(float maxLeaseUsage, Duration expireAfterAccess) {
		this.maxLeaseUsage = maxLeaseUsage;
		this.expireAfterAccess = expireAfterAccess;
	}

	@Override
	public Duration timeToLive(boolean write, @NonNull K key, @NonNull StatValue<OAuthTokenResponse> statValue,
			@NonNull Supplier<Duration> noChangeSupplier) {
		return timeToLive(statValue, noChangeSupplier);
	}

	protected Duration timeToLive(StatValue<OAuthTokenResponse> statValue) {
		return timeToLive(statValue, () -> Caffeines.MAX_TTL);
	}

	protected Duration timeToLive(StatValue<OAuthTokenResponse> statValue,
			@NonNull Supplier<Duration> noChangeSupplier) {
		if (statValue == null)
			return Duration.ZERO;
		var value = statValue.getValue();
		if (value == null)
			return Duration.ZERO;
		var createdAt = statValue.getCreatedAt();
		if (createdAt == null)
			return Duration.ZERO;
		var tokenTTL = value.timeToLive(createdAt, this.maxLeaseUsage);
		var accessTTL = expireAfterAccess != null ? expireAfterAccess : noChangeSupplier.get();
		return StreamEx.of(tokenTTL, accessTTL).nonNull().sortedBy(Duration::toMillis).findFirst()
				.orElse(Duration.ZERO);
	}

}
