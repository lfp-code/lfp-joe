package com.lfp.joe.net.http.headers;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import com.lfp.joe.serial.Serials;
import com.lfp.joe.serial.serializer.Serializer;

import com.google.common.reflect.TypeToken;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

@Serializer(HeaderMap.class)
public class HeaderMapSerializer implements JsonSerializer<HeaderMap>, JsonDeserializer<HeaderMap> {

	@SuppressWarnings("serial")
	private static final TypeToken<Map<String, List<String>>> TYPE_TOKEN = new TypeToken<Map<String, List<String>>>() {
	};

	@Override
	public HeaderMap deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		if (json == null || json.isJsonNull())
			return HeaderMap.of();
		if (json.isJsonObject())
			json.getAsJsonObject().remove(Serials.Gsons.CLASS_TYPE_FIELD_NAME);
		Map<String, List<String>> map = context.deserialize(json, TYPE_TOKEN.getType());
		return HeaderMap.of(map);
	}

	@Override
	public JsonElement serialize(HeaderMap src, Type typeOfSrc, JsonSerializationContext context) {
		if (src == null || src.isEmpty())
			return JsonNull.INSTANCE;
		var map = src.map();
		return context.serialize(map, TYPE_TOKEN.getType());
	}

}
