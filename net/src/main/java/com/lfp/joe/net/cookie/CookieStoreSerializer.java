package com.lfp.joe.net.cookie;

import java.lang.reflect.Type;
import java.net.CookieStore;
import java.net.URI;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.lfp.joe.serial.serializer.Serializer;
import com.lfp.joe.utils.Utils;

import com.google.common.reflect.TypeToken;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

@Serializer(value = CookieStore.class, hierarchy = true)
public enum CookieStoreSerializer implements JsonSerializer<CookieStore>, JsonDeserializer<CookieStore> {
	INSTANCE;

	@SuppressWarnings("serial")
	private static final TypeToken<Map<Optional<URI>, List<Cookie>>> COOKIE_MAP_TT = new TypeToken<Map<Optional<URI>, List<Cookie>>>() {
	};

	@Override
	public CookieStore deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		var cookieStore = Cookies.createCookieStore();
		if (json == null || json.isJsonNull())
			return cookieStore;
		Map<Optional<URI>, List<Cookie>> map = context.deserialize(json, COOKIE_MAP_TT.getType());
		if (map == null)
			return cookieStore;
		Utils.Lots.streamMultimap(map).forEach(ent -> {
			cookieStore.add(ent.getKey().orElse(null), ent.getValue().toHttpCookie());
		});
		return cookieStore;
	}

	@Override
	public JsonElement serialize(CookieStore src, Type typeOfSrc, JsonSerializationContext context) {
		if (src == null)
			return JsonNull.INSTANCE;
		Map<Optional<URI>, List<Cookie>> map = new LinkedHashMap<>();
		for (var uri : src.getURIs()) {
			var httpCookies = src.get(uri);
			var cookies = Utils.Lots.stream(httpCookies).map(Cookie::build).toList();
			if (cookies.isEmpty())
				continue;
			map.put(Optional.ofNullable(uri), cookies);
		}
		var result = context.serialize(map, COOKIE_MAP_TT.getType());
		return result;
	}

}
