package com.lfp.joe.net.http.ip;

import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.URI;
import java.net.UnknownHostException;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import com.github.mizosoft.methanol.MutableRequest;
import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.location.Location;
import com.lfp.joe.location.Locations;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.ip.attr.Asn;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.bytes.LazyInputStream;

import io.mikael.urlbuilder.UrlBuilder;
import one.util.streamex.StreamEx;

public class IPs {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String IP_API_HOST = "ip-api.com";
	private static final String IP_API_PRO_HOST = "pro.ip-api.com";
	private static final List<URI> IP_ECHO_URI_LIST = List.of(URI.create("https://api.ipify.org/"),
			URI.create("https://icanhazip.com/"));
	private static final String LOCAL_IP_ADDRESS = "127.0.0.1";
	private static final Pattern IPADDRESS_PATTERN = Pattern.compile(
			"((^\\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\\s*$)|(^\\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:)))(%.+)?\\s*$))");

	private static final MemoizedSupplier<Void> WARN_NOT_SECURE = Utils.Functions.memoize(() -> {
		logger.warn("ip info lookups are using a non-secure connection to:" + IP_API_HOST);
		return null;
	});

	private static MemoizedSupplier<String> IPAddress_NETWORK = Utils.Functions.memoize(() -> {
		try (final DatagramSocket socket = new DatagramSocket()) {
			socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
			return socket.getLocalAddress().getHostAddress();
		} catch (SocketException | UnknownHostException e) {
			throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
					? java.lang.RuntimeException.class.cast(e)
					: new java.lang.RuntimeException(e);
		}
	});

	public static String getNetworkIPAddress() {
		return IPAddress_NETWORK.get();
	}

	public static String getLocalIPAddress() {
		return LOCAL_IP_ADDRESS;
	}

	public static String getIPAddress() {
		return IPAddressLookups.lookupSelf().orElse(null);
	}

	public static String getIPAddress(Duration cacheExpiration) {
		return IPAddressLookups.lookupSelf(cacheExpiration).orElse(null);
	}

	public static String getIPAddress(Proxy proxy) {
		if (proxy == null)
			return getIPAddress();
		var client = HttpClients.newClient(proxy);
		ThrowingFunction<URI, InputStream, IOException> httpGetter = uri -> {
			HttpResponse<InputStream> response;
			try {
				response = client.send(MutableRequest.GET(uri), BodyHandlers.ofInputStream());
			} catch (InterruptedException e) {
				throw Utils.Exceptions.as(e, IOException.class);
			}
			return new LazyInputStream(() -> {
				var body = response.body();
				try {
					StatusCodes.validate(response.statusCode(), 2);
				} catch (Throwable t) {
					body.close();
					throw t;
				}
				return body;
			});
		};
		return getIPAddress(httpGetter);
	}

	public static String getIPAddress(ThrowingFunction<URI, InputStream, IOException> httpGetter) {
		var ipOp = Utils.Functions.unchecked(() -> IPAddressLookups.lookupSelfFresh(httpGetter));
		return ipOp.orElse(null);
	}

	public static IPAddressInfo getIPAddressInfo() {
		return IPAddressInfo_SELF.get(IPAddressLookups.CACHE_TTL);
	}

	public static IPAddressInfo getIPAddressInfo(Duration cacheExpiration) {
		return IPAddressInfo_SELF.get(cacheExpiration);
	}

	public static Optional<IPAddressInfo> getIPAddressInfo(String lookup) {
		lookup = Utils.Strings.trimToNull(lookup);
		if (lookup == null)
			return Optional.empty();
		var result = lookupIPAddressInfoFresh(Optional.of(lookup), true);
		return Optional.ofNullable(result);
	}

	private static final MemoizedSupplier<IPAddressInfo> IPAddressInfo_SELF = Utils.Functions.memoize(() -> {
		return lookupIPAddressInfoFresh(Optional.empty(), false);
	});

	private static IPAddressInfo lookupIPAddressInfoFresh(Optional<String> address, boolean suppressError) {
		Objects.requireNonNull(address);
		String lookup;
		if (address.isEmpty())
			lookup = "";
		else {
			var ipOp = parse(address.get());
			if (ipOp.isEmpty() && suppressError)
				return null;
			lookup = ipOp.orElseThrow();
		}
		IPAddressConfig ipAddressConfig = Configs.get(IPAddressConfig.class);
		String token = ipAddressConfig.ipApiToken();
		var urlb = UrlBuilder.empty().withPath("/json/" + lookup);
		if (StringUtils.isBlank(token)) {
			WARN_NOT_SECURE.get(Duration.ofMinutes(5));
			urlb = urlb.withScheme("http");
			urlb = urlb.withHost(IP_API_HOST);
		} else {
			urlb = urlb.withScheme("https");
			urlb = urlb.withHost(IP_API_PRO_HOST);
			urlb = urlb.addParameter("key", token);
		}
		var uri = urlb.toUri();
		String json = HttpClients.getDefault().get(uri.toString()).asString().getBody();
		Validate.isTrue(StringUtils.isNotBlank(json), "invalid response: " + uri);
		@SuppressWarnings("unchecked")
		Map<String, Object> map = Serials.Gsons.get().fromJson(json, Map.class);
		Validate.isTrue(map != null && !map.isEmpty(), "invalid parsed response: " + uri);
		return toIPAddressInfo(map, suppressError);
	}

	private static IPAddressInfo toIPAddressInfo(Map<String, Object> map, boolean suppressError) {
		Function<String, String> stringFunc = s -> {
			String val = Utils.Types.tryCast(map.get(s), String.class).orElse(null);
			val = StringUtils.trim(val);
			if (StringUtils.isBlank(val))
				return null;
			return val;
		};
		boolean success = StringUtils.equalsIgnoreCase(stringFunc.apply("status"), "success");
		if (!success) {
			if (suppressError)
				return null;
			Validate.isTrue(false, "invalid response from ip api server: " + map);
		}
		Location location;
		{
			var locationB = Location.builder();
			locationB.locality(stringFunc.apply("city"));
			locationB.region(stringFunc.apply("regionName"));
			locationB.countryCode(Locations.parseCountryCode(stringFunc.apply("countryCode")).orElse(null));
			locationB.postalCode(stringFunc.apply("zip"));
			locationB
					.latitude(Utils.Types.tryCast(map.get("lat"), Number.class).map(n -> n.doubleValue()).orElse(null));
			locationB.longitude(
					Utils.Types.tryCast(map.get("lon"), Number.class).map(n -> n.doubleValue()).orElse(null));
			boolean nonNull = false;
			for (var mp : Location.meta().metaPropertyIterable()) {
				var value = JodaBeans.get(locationB, mp);
				if (value instanceof String)
					value = Utils.Strings.trimToNull((String) value);
				if (value != null) {
					nonNull = true;
					break;
				}
			}
			location = nonNull ? locationB.build() : null;
		}
		IPAddressInfo.Builder b = IPAddressInfo.builder();
		b.location(location);
		b.ip(stringFunc.apply("query"));
		b.timezone(stringFunc.apply("timezone"));
		b.organization(stringFunc.apply("org"));
		String as = stringFunc.apply("as");
		if (StringUtils.isNotBlank(as)) {
			b = b.asn(Asn.builder().name(as).build());
		}
		return b.build();
	}

	public static Optional<String> parse(String lookup) {
		return IPAddressLookups.lookup(lookup);
	}

	public static boolean isValidIpAddress(String ip) {
		if (StringUtils.isBlank(ip))
			return false;
		return IPADDRESS_PATTERN.matcher(ip).matches();
	}

	public static URI getIPLookupServiceURI() {
		return IP_ECHO_URI_LIST.stream().findFirst().get();
	}

	public static StreamEx<URI> streamIPLookupServiceURIs() {
		return StreamEx.of(IP_ECHO_URI_LIST);
	}

	public static void main(String[] args) {
		System.out.println(IPs.getIPAddress());
		System.out.println(IPs.getIPAddressInfo());
		IPAddressConfig ipAddressConfig = Configs.get(IPAddressConfig.class);
		System.out.println("token: " + ipAddressConfig.ipApiToken());
		System.out.println(isValidIpAddress("FE80:0000:0000:0000:0202:B3FF:FE1E:8329"));
		System.out.println(parse("https://xxx.com/asdf"));
		System.out.println(getIPAddressInfo("2001:4860:4860::8888"));
		for (int i = 0; i < 10; i++) {
			System.out.println(getIPAddressInfo());
			System.out.println(getIPAddress());
		}
	}

}
