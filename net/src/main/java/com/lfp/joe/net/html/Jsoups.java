package com.lfp.joe.net.html;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

import org.jsoup.Connection.Method;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.lfp.joe.core.lot.AbstractLot;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public class Jsoups {

	protected Jsoups() {}

	public static Optional<Document> tryParse(String html, Map<String, ? extends Iterable<String>> responseHeaders,
			URI requestURI) {
		if (!Htmls.isHtml(html))
			return Optional.empty();
		var charset = Headers.tryGetCharset(Utils.Lots.streamMultimap(responseHeaders))
				.orElse(Utils.Bits.getDefaultCharset());
		var doc = Utils.Functions.catching(
				() -> parse(Bytes.from(html.getBytes(charset)).inputStream(), charset, requestURI), t -> null);
		return Optional.ofNullable(doc);
	}

	public static Document parse(InputStream inputStream, Map<String, ? extends Iterable<String>> responseHeaders,
			URI requestURI) throws IOException {
		var charset = Headers.tryGetCharset(Utils.Lots.streamMultimap(responseHeaders)).orElse(null);
		return parse(inputStream, charset, requestURI);
	}

	public static Document parse(InputStream inputStream, Charset charset, URI requestURI) throws IOException {
		if (inputStream == null)
			inputStream = Utils.Bits.emptyInputStream();
		try {
			if (charset == null)
				charset = Utils.Bits.getDefaultCharset();
			return Jsoup.parse(inputStream, charset.name(), requestURI == null ? "" : requestURI.toString());
		} finally {
			inputStream.close();
		}
	}

	public static StreamEx<Element> select(Element element, String... selects) {
		if (element == null)
			return StreamEx.empty();
		var streams = Utils.Lots.stream(selects)
				.filter(Utils.Strings::isNotBlank)
				.map(v -> Utils.Lots.stream(element.select(v)));
		return Utils.Lots.flatMap(streams);
	}

	public static FormData parseFormData(Element element) {
		Objects.requireNonNull(element);
		var actionURI = URIs.parse(element.absUrl("action")).orElse(null);
		if (actionURI == null)
			actionURI = URIs.parse(element.ownerDocument().baseUri()).orElse(null);
		Objects.requireNonNull(actionURI, "action uri not found");
		String methodStr = element.attr("method");
		Method method = null;
		if (Utils.Strings.isNotBlank(methodStr)) {
			method = Utils.Functions.catching(() -> Method.valueOf(methodStr.toLowerCase()), t -> null);
			if (method == null)
				method = Utils.Lots.stream(Method.values())
						.filter(v -> Utils.Strings.equalsIgnoreCase(methodStr, v.toString()))
						.findFirst()
						.orElse(null);
		}
		if (method == null)
			method = Method.GET;
		var formData = new FormData(method, actionURI);
		Jsoups.select(element, "[name]").forEach(el -> {
			String name = el.attr("name");
			if (Utils.Strings.isBlank(name))
				return;
			String value = el.attr("value");
			formData.add(name, value);
		});
		return formData;
	}

	public static StreamEx<Element> streamParents(Element element) {
		if (element == null)
			return StreamEx.empty();
		var iter = new AbstractLot.Indexed<Element>() {

			private Element currentElement = element.parent();

			@Override
			protected Element computeNext(long index) {
				if (currentElement == null)
					return this.end();
				var result = currentElement;
				currentElement = currentElement.parent();
				return result;
			}
		};
		return Utils.Lots.stream(iter);
	}

	public static EntryStream<Element, String> scripts(Element initialElement) {
		if (initialElement == null)
			return EntryStream.empty();
		StreamEx<Element> elements = StreamEx.empty();
		if (Utils.Strings.equalsIgnoreCase(initialElement.tagName(), "script"))
			elements = elements.append(initialElement);
		elements = elements.append(select(initialElement, "script"));
		elements = elements.distinct();
		var estreams = elements.map(el -> {
			var dataStream = Utils.Lots.stream(el.dataNodes()).map(v -> v.getWholeData());
			return dataStream.mapToEntry(v -> el, v -> v);
		});
		var estream = Utils.Lots.flatMap(estreams).mapToEntry(Entry::getKey, Entry::getValue);
		return estream;
	}

	public static EntryStream<String, String> meta(Element element) {
		if (element == null)
			return EntryStream.empty();
		Predicate<Attribute> baseFilter = attr -> {
			if (attr == null || attr.getKey() == null || attr.getValue() == null)
				return false;
			return true;
		};
		Predicate<Attribute> nameFilter = baseFilter.and(attr -> {
			for (var filter : List.of("name", "property")) {
				if (Utils.Strings.equalsIgnoreCase(attr.getKey(), filter))
					return true;
			}
			return false;
		});
		Predicate<Attribute> valueFilter = baseFilter.and(attr -> {
			for (var filter : List.of("content", "value")) {
				if (Utils.Strings.equalsIgnoreCase(attr.getKey(), filter))
					return true;
			}
			return false;
		});
		StreamEx<Element> elementStream = StreamEx.empty();
		if ("meta".equals(element.tagName()))
			elementStream = elementStream.append(element);
		elementStream = elementStream.append(Utils.Lots.stream(element.select("meta")));
		elementStream = elementStream.nonNull();
		var streams = elementStream.map(meta -> {
			StreamEx<String> metaNameStream = Utils.Lots.stream(meta.attributes())
					.filter(nameFilter)
					.map(Attribute::getValue);
			Iterable<String> metaValueIterable = () -> {
				StreamEx<String> metaValueStream = Utils.Lots.stream(meta.attributes())
						.filter(valueFilter)
						.map(Attribute::getValue);
				var text = meta.text();
				if (Utils.Strings.isNotBlank(text))
					metaValueStream = metaValueStream.append(text);
				metaValueStream = metaValueStream.nonNull();
				return metaValueStream.iterator();
			};
			return metaNameStream.flatMap(metaName -> {
				return Utils.Lots.stream(metaValueIterable).map(metaValue -> Utils.Lots.entry(metaName, metaValue));
			});
		});
		var estream = Utils.Lots.flatMap(streams).mapToEntry(Entry::getKey, Entry::getValue);
		return estream.distinct();
	}

	public static void relativeToAbsoluteURLs(Document document, String... attributes) {
		var attrs = Utils.Lots.stream(attributes).filter(Utils.Strings::isNotBlank).distinct().toList();
		if (attrs.isEmpty())
			attrs = Arrays.asList("src", "href");
		for (var attr : attrs) {
			for (var el : Jsoups.select(document, String.format("[%s]", attr))) {
				var value = el.attr(attr);
				var url = Optional.ofNullable(uri(el, attr)).map(Object::toString).orElse(null);
				if (url == null)
					continue;
				if (Utils.Strings.equals(url, value))
					continue;
				el.attr(attr, url);
			}
		}
	}

	public static URI uri(Element element) {
		return uri(element, (String[]) null);
	}

	public static URI uri(Element element, String... attributes) {
		if (element == null)
			return null;
		boolean validAttribute = false;
		for (int i = 0; !validAttribute && i < 2; i++) {
			if (i > 0)
				attributes = new String[] { "href", "src" };
			if (attributes == null)
				continue;
			for (var attribute : attributes) {
				if (Utils.Strings.isBlank(attribute))
					continue;
				validAttribute = true;
				if (!element.hasAttr(attribute))
					continue;
				var uriOp = URIs.parse(element.absUrl(attribute));
				if (uriOp.isPresent())
					return uriOp.get();
			}
		}
		return null;
	}

	public static void main(String[] args) {
		var doc = Jsoups.tryParse("<body><a id=\"cool\" href=\"\"></a></body>", null, URI.create("https://localhost"));
		var url = doc.get().getElementById("cool").absUrl("src");
		System.out.println(url);
	}
}