package com.lfp.joe.net.proxy;

public interface ProxyEnabled {

	Proxy getProxy();

}
