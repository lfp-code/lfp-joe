package com.lfp.joe.net.config;

import java.net.URI;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.aeonbits.owner.Config;

import com.google.re2j.Pattern;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.converter.URIConverter;

public interface NetConfig extends Config {

	static <X> X init() {
		Tools.init();
		return null;
	}

	@DefaultValue("100")
	int devAuthenticatorTimeWindow();

	@DefaultValue("true")
	boolean devAuthenticatorTimeLogging();

	@DefaultValue("nip.io")
	String wildcardDNSDomain();

	@DefaultValue("3")
	int socks5SocketRetryCount();

	@DefaultValue("10000")
	long socks5SocketConnectTimeoutMillis();

	@DefaultValue("750")
	long socks5SocketRetryDelayMillis();

	@DefaultValue("10000")
	long dnsLookupTimeoutMillis();

	@DefaultValue("true")
	boolean socks5AlwaysResolveAddressLocally();

	@DefaultValue("https://repo1.maven.org/maven2/javax/mail/javax.mail-api/1.6.2/javax.mail-api-1.6.2.jar")
	@ConverterClass(URIConverter.class)
	URI javaxMailJarURI();

	@DefaultValue("true")
	boolean preferIPv4Stack();

	@DefaultValue("TLSv1,TLSv1.1,TLSv1.2")
	List<String> httpsProtocols();

	@DefaultValue("Basic")
	List<String> enableHttpAuthTunnelingSchemes();

	@DefaultValue("30")
	int networkaddressCacheTTL();

	static enum Tools {
		;

		private static final Object MUTEX = new Object();
		private static Optional<Void> _INIT;

		private static void init() {
			if (_INIT == null)
				synchronized (MUTEX) {
					if (_INIT == null)
						initSynchronized(Configs.get(NetConfig.class));
				}
			_INIT = Optional.empty();
		}

		private static void initSynchronized(NetConfig netConfig) {
			Map<String, String> props = new HashMap<>();
			props.put("java.net.preferIPv4Stack", Objects.toString(netConfig.preferIPv4Stack()));
			props.put("networkaddress.cache.ttl", Objects.toString(netConfig.networkaddressCacheTTL()));
			props.put("https.protocols", streamNonBlank(netConfig.httpsProtocols()).collect(Collectors.joining(",")));
			props.compute("jdk.http.auth.tunneling.disabledSchemes", (name, nil) -> {
				return getHttpAuthTunnelingDisabledValue(netConfig, name);
			});
			if (MachineConfig.isDeveloper() && MachineConfig.isWindows())
				props.put("javax.net.ssl.trustStoreType", "Windows-ROOT");
			String summary = props.entrySet()
					.stream()
					.sorted(Comparator.comparing(v -> v.getKey().toLowerCase()))
					.map(ent -> {
						var name = ent.getKey();
						var value = ent.getValue();
						System.setProperty(name, value);
						var printValue = Optional.ofNullable(value)
								.filter(Predicate.not(String::isBlank))
								.orElseGet(() -> "[unset]");
						return String.format("%s:%s", name, printValue);
					})
					.collect(Collectors.joining(", "));
			org.slf4j.LoggerFactory.getLogger(NetConfig.class).info(summary);

		}

		private static String getHttpAuthTunnelingDisabledValue(NetConfig netConfig, String propertyName) {
			var propertyValue = System.getProperty(propertyName);
			if (propertyValue == null)
				return "";
			var enableSchemes = streamNonBlank(netConfig.enableHttpAuthTunnelingSchemes()).collect(Collectors.toList());
			return streamNonBlank(() -> {
				return Stream.of(propertyValue.split(Pattern.quote(","))).iterator();
			}).filter(v -> {
				return enableSchemes.stream().noneMatch(v::equalsIgnoreCase);
			}).collect(Collectors.joining(","));
		}

		private static Stream<String> streamNonBlank(Iterable<String> values) {
			if (values == null)
				return Stream.empty();
			Stream<String> stream;
			if (values instanceof Collection)
				stream = ((Collection<String>) values).stream();
			else
				stream = StreamSupport.stream(values.spliterator(), false);
			return stream.filter(Predicate.not(String::isBlank));
		}

	}
}
