package com.lfp.joe.net.socket.socks.jsocks;

import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Hashtable;
import java.util.Objects;

import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.net.proxy.ProxyEnabled;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.utils.Utils;

import net.sourceforge.jsocks.socks.ProxyMessage;
import net.sourceforge.jsocks.socks.Socks5Proxy;
import net.sourceforge.jsocks.socks.SocksException;

public class JsocksSocks5Proxy extends Socks5Proxy implements ProxyEnabled {

	@SuppressWarnings("unchecked")
	private static Field authMethods_FIELD = JavaCode.Reflections.getFieldUnchecked(Socks5Proxy.class, false,
			f -> f.getName().equals("authMethods"), f -> Hashtable.class.isAssignableFrom(f.getType()));
	@SuppressWarnings("unchecked")
	private static Field resolveAddrLocally_FIELD = JavaCode.Reflections.getFieldUnchecked(Socks5Proxy.class, false,
			f -> f.getName().equals("resolveAddrLocally"), f -> boolean.class.isAssignableFrom(f.getType()));

	private Proxy proxy;

	public JsocksSocks5Proxy(Proxy proxy) {
		super(Objects.requireNonNull(proxy, "proxy is required").getAddress().getAddress(), proxy.getPort());
		this.proxy = proxy;
	}

	protected JsocksSocks5Proxy(InetAddress proxyIp, int proxyPort, Proxy proxy) {
		super(proxyIp, proxyPort);
		this.proxy = proxy;
	}

	@Override
	public Proxy getProxy() {
		return proxy;
	}

	@Override
	protected ProxyMessage connect(String host, int port) throws UnknownHostException, SocksException {
		return super.connect(host, port);
	}

	public Socket getProxySocket() {
		return proxySocket;
	}

	public InetAddress getProxyIP() {
		return proxyIP;
	}

	@Override
	public void endSession() {
		super.endSession();
	}

	@Override
	public JsocksSocks5Proxy copy() {
		return Utils.Functions.unchecked(() -> copyInternal());
	}

	protected JsocksSocks5Proxy copyInternal() throws IllegalArgumentException, IllegalAccessException {
		final JsocksSocks5Proxy copy = new JsocksSocks5Proxy(proxyIP, proxyPort, proxy);
		authMethods_FIELD.set(copy, authMethods_FIELD.get(this));
		// copy.authMethods = this.authMethods; // same Hash, no copy
		copy.directHosts = this.directHosts;
		copy.chainProxy = this.chainProxy;
		resolveAddrLocally_FIELD.set(copy, resolveAddrLocally_FIELD.get(this));
		// copy.resolveAddrLocally = this.resolveAddrLocally;
		return copy;
	}

}
