package com.lfp.joe.net.cookie;

import java.io.File;
import java.net.HttpCookie;
import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

import com.github.throwable.beanref.BeanPath;
import com.github.throwable.beanref.BeanRef;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Maps;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.reflections.reflection.FieldAccessor;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public class Cookies {

	private static final Duration MAX_DATE_SHIFT = Duration.ofSeconds(2);
	@SuppressWarnings("unchecked")
	private static final FieldAccessor<HttpCookie, Long> HttpCookie_whenCreated_FIELD = JavaCode.Reflections
			.getFieldAccessorUnchecked(HttpCookie.class, false, f -> "whenCreated".equals(f.getName()),
					f -> long.class.isAssignableFrom(f.getType()));
	private static final BiMap<BeanPath<javax.servlet.http.Cookie, ?>, BeanPath<HttpCookie, ?>> SERVLET_COOKIE_TO_HTTP_COOKIE_MAPPING;
	static {
		BiMap<BeanPath<javax.servlet.http.Cookie, ?>, BeanPath<HttpCookie, ?>> biMap = HashBiMap.create();
		var cookieBPs = Streams.of(BeanRef.$(javax.servlet.http.Cookie.class).all()).mapToEntry(BeanPath::getPath)
				.invert().distinctKeys().toMap();
		var httpCookieBPs = Streams.of(BeanRef.$(HttpCookie.class).all()).mapToEntry(BeanPath::getPath).invert()
				.distinctKeys().toMap();
		for (var ent : cookieBPs.entrySet()) {
			var key = ent.getKey();
			var cookieBP = ent.getValue();
			var httpCookieBP = httpCookieBPs.get(key);
			if (httpCookieBP == null)
				continue;
			biMap.put(cookieBP, httpCookieBP);
		}
		SERVLET_COOKIE_TO_HTTP_COOKIE_MAPPING = Maps.unmodifiableBiMap(biMap);
	}

	public static StreamEx<Cookie> parseResponseCookies(EntryStream<String, String> headerStream) {
		if (headerStream == null)
			return StreamEx.empty();
		StreamEx<String> valueStream = headerStream.filterKeys(k -> {
			if (Utils.Strings.startsWithIgnoreCase(k, Headers.SET_COOKIE))
				return true;
			if (Utils.Strings.startsWithIgnoreCase(k, Headers.SET_COOKIE2))
				return true;
			return false;
		}).values();
		valueStream = Utils.Lots.flatMap(valueStream.map(v -> Utils.Strings.streamLines(v)));
		valueStream = valueStream.map(Utils.Strings::trim);
		valueStream = valueStream.filter(Utils.Strings::isNotBlank);
		StreamEx<List<HttpCookie>> cookieLists = valueStream
				.map(v -> Utils.Functions.catching(() -> HttpCookie.parse(v), t -> null));
		return Utils.Lots.flatMapIterables(cookieLists).nonNull().map(Cookie::build);
	}

	public static InMemoryCookieStore createCookieStore() {
		return new InMemoryCookieStore();
	}

	public static FileCookieStore createCookieStore(String cookieFolderUUID) {
		return new FileCookieStore(cookieFolderUUID);
	}

	public static FileCookieStore createCookieStore(File folder) {
		return new FileCookieStore(folder);
	}

	public static boolean equalsExact(HttpCookie httpCookie, HttpCookie... httpCookies) {
		var cookies = Utils.Lots.stream(httpCookies).map(v -> {
			return v == null ? null : Cookie.build(v);
		}).toArray(Cookie.class);
		return equalsExact(httpCookie == null ? null : Cookie.build(httpCookie), cookies);
	}

	public static boolean equalsExact(Cookie cookie, Cookie... cookies) {
		var cookieIter = Utils.Lots.stream(cookies).prepend(cookie).iterator();
		var toCompare = Utils.Lots.stream(cookieIter).limit(2).toList();
		if (toCompare.size() <= 1)
			return true;
		var cookie1 = toCompare.get(0);
		var cookie2 = toCompare.get(1);
		StreamEx<Function<Cookie, Object>> getters = StreamEx.empty();
		{// metaproperties ignoring expiration
			var mpStream = Utils.Lots.stream(Cookie.meta().metaPropertyIterable());
			mpStream = mpStream.filter(Predicate.not(Cookie.meta().whenCreated()::equals));
			mpStream = mpStream.filter(Predicate.not(Cookie.meta().maxAge()::equals));
			getters = getters.append(mpStream.map(v -> {
				return c -> v.get(c);
			}));
		}
		{// expiration
			getters = getters.append(c -> {
				return c.getExpiresAt().orElse(null);
			});
		}
		for (var getter : getters) {
			var v1 = getter.apply(cookie1);
			var v2 = getter.apply(cookie2);
			if (Objects.equals(v1, v2))
				continue;
			if (v1 instanceof Date && v2 instanceof Date) {
				var diffMillis = ((Date) v1).getTime() - ((Date) v2).getTime();
				diffMillis = Math.abs(diffMillis);
				if (diffMillis <= MAX_DATE_SHIFT.toMillis())
					continue;
			}
			return false;
		}
		if (!cookieIter.hasNext())
			return true;
		return equalsExact(cookieIter.next(), Utils.Lots.stream(cookieIter).toArray(Cookie.class));
	}

	public static long getWhenCreated(HttpCookie httpCookie) {
		return HttpCookie_whenCreated_FIELD.get(httpCookie);
	}

	public static void setWhenCreated(HttpCookie httpCookie, long whenCreated) {
		HttpCookie_whenCreated_FIELD.set(httpCookie, whenCreated);
	}

	@SuppressWarnings({ "unchecked" })
	public static javax.servlet.http.Cookie toServletCookie(HttpCookie httpCookie) {
		if (httpCookie == null)
			return null;
		var cookie = new javax.servlet.http.Cookie(httpCookie.getName(), httpCookie.getValue());
		SERVLET_COOKIE_TO_HTTP_COOKIE_MAPPING.inverse().forEach((readBP, writeBP) -> {
			if (writeBP.isReadOnly())
				return;
			var value = readBP.get(httpCookie);
			((BeanPath<javax.servlet.http.Cookie, Object>) writeBP).set(cookie, value);
		});
		return cookie;
	}

	@SuppressWarnings({ "unchecked" })
	public static HttpCookie toHttpCookie(javax.servlet.http.Cookie servletCookie) {
		if (servletCookie == null)
			return null;
		var cookie = new HttpCookie(servletCookie.getName(), servletCookie.getValue());
		SERVLET_COOKIE_TO_HTTP_COOKIE_MAPPING.forEach((readBP, writeBP) -> {
			if (writeBP.isReadOnly())
				return;
			var value = readBP.get(servletCookie);
			((BeanPath<HttpCookie, Object>) writeBP).set(cookie, value);
		});
		return cookie;
	}

	public static void main(String[] args) {
		System.out.println(Serials.Gsons.getPretty().toJson(SERVLET_COOKIE_TO_HTTP_COOKIE_MAPPING));
		int test1 = 1;
		System.out.println(Utils.Types.tryCast(test1, int.class).orElse(null));
		Integer test2 = 1;
		System.out.println(Utils.Types.tryCast(test2, int.class).orElse(null));

		String blob = " autorf=deleted; expires=Thu, 01-Jan-1970 00:00:01 GMT; Max-Age=0; path=/; domain=www.yahoo.com\r\n"
				+ " A1=d=AQABBIgM_V4CENqU6rDyLpCEoBiyjQk2FW0FEgEBAQFe_l4GXwSemaphoreAccessorSemaphoreAccessor_SMSemaphoreAccessor&S=AQSemaphoreAccessoroOzoN9HafXGqdn9XMgRMwM; Max-Age=31557600; Domain=.yahoo.com; Path=/; SameSite=Lax; Secure; HttpOnly\r\n"
				+ " A3=d=AQABBIgM_V4CENqU6rDyLpCEoBiyjQk2FW0FEgEBAQFe_l4GXwSemaphoreAccessorSemaphoreAccessor_SMSemaphoreAccessor&S=AQSemaphoreAccessoroOzoN9HafXGqdn9XMgRMwM; Max-Age=31557600; Domain=.yahoo.com; Path=/; SameSite=None; Secure; HttpOnly\r\n"
				+ " A1S=d=AQABBIgM_V4CENqU6rDyLpCEoBiyjQk2FW0FEgEBAQFe_l4GXwSemaphoreAccessorSemaphoreAccessor_SMSemaphoreAccessor&S=AQSemaphoreAccessoroOzoN9HafXGqdn9XMgRMwM&j=US; Domain=.yahoo.com; Path=/; SameSite=Lax; Secure\r\n"
				+ " B=6q59m15ffq348&b=3&s=vb; Max-Age=31557600; Domain=.yahoo.com; Path=/\r\n"
				+ " GUC=AQEBAQFe_l5fBkIaxwPF; Max-Age=31557600; Domain=.yahoo.com; Path=/; Secure\r\n" + "";
		StreamEx<Cookie> stream = parseResponseCookies(EntryStream.of(Headers.SET_COOKIE2, blob));
		stream.forEach(ent -> {
			System.out.println(ent);
		});
	}
}
