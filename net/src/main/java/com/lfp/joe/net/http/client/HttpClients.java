package com.lfp.joe.net.http.client;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Redirect;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandler;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

import org.apache.commons.lang3.Validate;
import org.json.JSONArray;
import org.json.JSONObject;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.github.mizosoft.methanol.Methanol;
import com.goebl.david.Request;
import com.goebl.david.Request.Method;
import com.goebl.david.Response;
import com.goebl.david.Webb;
import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.net.auth.AuthenticatorLFP;
import com.lfp.joe.net.config.NetConfig;
import com.lfp.joe.net.cookie.CookieStoreLFP;
import com.lfp.joe.net.cookie.Cookies;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.headers.UserAgents;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;

@SuppressWarnings("unchecked")
public class HttpClients {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String DEFAULT_CLIENT_INSTANCE_KEY = THIS_CLASS.getName() + "-default-client";
	static {
		NetConfig.init();
	}

	public static HttpClient get() {
		return Instances.get(DEFAULT_CLIENT_INSTANCE_KEY, () -> builder().build());
	}

	public static Methanol.Builder builder() {
		return builder(null);
	}

	public static Methanol.Builder builder(Proxy proxy) {
		var builder = Methanol.newBuilder();
		builder.userAgent(UserAgents.FIREFOX_LATEST.get());
		builder.followRedirects(Redirect.NORMAL);
		builder.executor(CoreTasks.executor());
		builder.autoAcceptEncoding(true);
		builder.proxy(new ProxySelectorLFP(proxy));
		builder.authenticator(AuthenticatorLFP.getDefault());
		if (proxy != null && proxy.isAuthenticationEnabled())
			builder.defaultHeader(Headers.PROXY_AUTHORIZATION,
					Headers.basicAuthorization(proxy.getUsername().orElse(""), proxy.getPassword().orElse("")));
		return builder;
	}

	public static HttpClient newClient() {
		return newClient(null);
	}

	public static HttpClient newClient(Proxy proxy) {
		return builder(proxy).build();
	}

	public static HttpResponse<InputStream> send(HttpRequest request) throws IOException {
		return send(request, BodyHandlers.ofInputStream());
	}

	public static <T> HttpResponse<T> send(HttpRequest request, BodyHandler<T> bodyHandler) throws IOException {
		try {
			return get().send(request, bodyHandler);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	// old webb client

	@Deprecated
	private static final java.lang.reflect.Method Response_setBody_Method;
	static {
		var meths = JavaCode.Reflections.streamMethods(Response.class, true, v -> "setBody".equals(v.getName()),
				v -> v.getParameterCount() == 1);
		var meth = Utils.Lots.one(meths);
		Response_setBody_Method = meth;
	}
	@Deprecated
	private static MemoizedSupplier<Constructor<Request>> REQUEST_CONSTRUCTOR_S = Utils.Functions.memoize(() -> {
		var ctors = JavaCode.Reflections.getConstructors(Request.class, c -> {
			var parameterTypes = c.getParameterTypes();
			List<Class<?>> requiredParameterTypes = List.of(Webb.class, Method.class, String.class);
			for (int i = 0; i < requiredParameterTypes.size(); i++) {
				if (parameterTypes.length <= i)
					return false;
				if (!requiredParameterTypes.get(i).isAssignableFrom(parameterTypes[i]))
					return false;
			}
			return true;
		});
		Validate.isTrue(ctors.size() == 1, "could not find constructor");
		var ctor = ctors.iterator().next();
		ctor.setAccessible(true);
		return ctor;
	});
	@Deprecated
	private static LoadingCache<Optional<java.net.Proxy>, WebbHttpClientLFP> INSTANCE_CACHE = Caffeine.newBuilder()
			.expireAfterAccess(Duration.ofMinutes(5)).build(pop -> {
				Map<String, String> headerMap = new LinkedHashMap<>();
				// make sure the user agent is hard coded,
				// a lookup could cause circular config lookups
				headerMap.put(Headers.USER_AGENT, UserAgents.CHROME_LATEST.get());
				var config = WebbHttpClientLFPConfig.builder().defaultHeaders(headerMap).proxy(pop.orElse(null))
						.build();
				return WebbHttpClientLFP.create(config);
			});

	@Deprecated
	public static WebbHttpClientLFP getDefault() {
		return INSTANCE_CACHE.get(Optional.empty());
	}

	@Deprecated
	public static WebbHttpClientLFP getDefault(java.net.Proxy proxy) {
		return INSTANCE_CACHE.get(Optional.of(proxy));
	}

	@Deprecated
	public static InputStream getResponseBody(Response<?> response) throws IOException {
		Objects.requireNonNull(response);
		Object body = response.getBody();
		if (body == null)
			body = response.getErrorBody();
		if (body == null)
			return Utils.Bits.emptyInputStream();
		else if (body instanceof String || body instanceof JSONObject || body instanceof JSONArray)
			return new ByteArrayInputStream(Objects.toString(body).getBytes(StandardCharsets.UTF_8));
		else if (body instanceof byte[])
			return new ByteArrayInputStream((byte[]) body);
		else if (body instanceof InputStream)
			return (InputStream) body;
		else
			throw new IOException("unexpected body type:" + body.getClass());
	}

	@Deprecated
	public static Bytes getResponseBodyBytes(Response<?> response) throws IOException {
		try (var is = getResponseBody(response);) {
			return Utils.Bits.from(is);
		}
	}

	@Deprecated
	public static Request copyRequest(Request request, BiConsumer<Field, Consumer<Object>> modifier) {
		Objects.requireNonNull(request);
		var fields = JavaCode.Reflections.streamFields(Request.class, false).toList();
		Function<String, Object> fieldNameToValue = name -> {
			var field = Utils.Lots.stream(fields).filter(f -> f.getName().equals(name)).findFirst().get();
			return Utils.Functions.unchecked(() -> field.get(request));
		};
		var ctor = REQUEST_CONSTRUCTOR_S.get();
		Request newRequest;
		try {
			newRequest = ctor.newInstance(fieldNameToValue.apply("webb"), fieldNameToValue.apply("method"),
					fieldNameToValue.apply("uri"));
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw Utils.Exceptions.asRuntimeException(e);
		}
		for (var field : fields) {
			var value = Utils.Functions.unchecked(() -> field.get(request));
			if (modifier != null) {
				AtomicReference<Optional<Object>> modRef = new AtomicReference<>();
				modifier.accept(field, modValue -> modRef.set(Optional.ofNullable(modValue)));
				if (modRef.get() != null)
					value = modRef.get().orElse(null);
			}
			try {
				field.set(newRequest, value);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				throw Utils.Exceptions.asRuntimeException(e);
			}
		}
		return newRequest;
	}

	@Deprecated
	public static Request withCookies(Request request, CookieStoreLFP cookieStore) {
		Objects.requireNonNull(request);
		Objects.requireNonNull(cookieStore);
		var cookies = cookieStore.get(URI.create(request.getUri()));
		StringBuilder cookieHeader = new StringBuilder();
		for (var cookie : cookies) {
			if (cookieHeader.length() > 0)
				cookieHeader.append(" ");
			cookieHeader.append(String.format("%s=%s;", cookie.getName(), cookie.getValue()));
		}
		if (cookieHeader.length() > 0)
			request.header(Headers.COOKIE, cookieHeader.toString());
		return request;
	}

	@Deprecated
	public static <X> Response<X> saveCookies(Response<X> response, CookieStoreLFP cookieStore) {
		Objects.requireNonNull(response);
		Objects.requireNonNull(cookieStore);
		var headers = response.getConnection().getHeaderFields();
		if (headers == null)
			headers = Collections.emptyMap();
		URI cookieURI = URI.create(response.getRequest().getUri());
		Cookies.parseResponseCookies(Utils.Lots.streamMultimap(headers)).forEach(c -> {
			cookieStore.add(cookieURI, c.toHttpCookie());
		});
		return response;
	}

	@Deprecated
	public static void setBody(Response<?> response, Object body) {
		Utils.Functions.unchecked(() -> Response_setBody_Method.invoke(response, body));
	}

}
