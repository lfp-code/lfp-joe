package com.lfp.joe.net.cookie;

import java.net.HttpCookie;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.regex.Pattern;

import com.lfp.joe.core.config.DevLogger;
import com.lfp.joe.net.dns.TLDs;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.utils.Utils;

import one.util.streamex.EntryStream;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;

public abstract class AbstractCookieStore implements CookieStoreLFP {

	@Override
	public StreamEx<CookieIdentifier> streamCookieIdentifiers() {
		var ciStream = Utils.Lots.stream(getCookieIdentifiers());
		ciStream = ciStream.nonNull();
		ciStream = ciStream.distinct();
		return ciStream;
	}

	@Override
	public StreamEx<HttpCookie> stream(URI uri) {
		var devLogger = DevLogger.create("stream");
		devLogger.disable();
		if (uri == null)
			return StreamEx.empty();
		var ciStream = streamCookieIdentifierVariations(uri);
		ciStream = ciStream.filter(ci -> {
			if (!ci.test(uri))
				return false;
			return true;
		});
		var result = Utils.Lots.flatMapIterables(ciStream.map(this::stream));
		if (!URIs.isSecure(uri))
			result = result.filter(v -> {
				return v.getSecure() ? false : true;
			});
		if (devLogger.isEnabled()) {
			var indexTracker = new AtomicLong();
			result = result.map(v -> {
				indexTracker.incrementAndGet();
				return v;
			});
			result = Utils.Lots.onComplete(result, () -> {
				devLogger.log("uri:{} count:{}", uri, indexTracker.get() + 1);
			});
		}
		return result;
	}

	@Override
	public StreamEx<HttpCookie> stream(CookieIdentifier cookieIdentifier) {
		if (cookieIdentifier == null)
			return StreamEx.empty();
		var ible = this.get(cookieIdentifier);
		var stream = Utils.Lots.stream(ible).nonNull();
		stream = stream.sortedBy(v -> Cookies.getWhenCreated(v) * -1);
		return stream;
	}

	@Override
	public void add(URI uri, Iterable<HttpCookie> cookies, boolean cookiesFromServer) {
		var cookieIdentifierMM = stream(uri, cookies, cookiesFromServer).grouping();
		for (var ent : cookieIdentifierMM.entrySet())
			add(ent.getKey(), ent.getValue());
	}

	protected void add(CookieIdentifier cookieIdentifier, Iterable<HttpCookie> addCookies) {
		var cookieStream = normalizeCookies(cookieIdentifier, addCookies);
		var hasNext = Utils.Lots.hasNext(cookieStream);
		if (!hasNext.getKey())
			return;
		cookieStream = hasNext.getValue();
		addOrReplace(cookieIdentifier, cookieStream);
	}

	public boolean remove(URI uri, Iterable<HttpCookie> cookies, boolean cookiesFromServer) {
		var cookieIdentifierMM = stream(uri, cookies, cookiesFromServer).grouping();
		boolean mod = false;
		for (var ent : cookieIdentifierMM.entrySet()) {
			if (remove(ent.getKey(), ent.getValue()))
				mod = true;
		}
		return mod;
	}

	protected boolean remove(CookieIdentifier cookieIdentifier, Iterable<HttpCookie> removeCookies) {
		var cookieStream = normalizeCookies(cookieIdentifier, removeCookies);
		var hasNext = Utils.Lots.hasNext(cookieStream);
		if (!hasNext.getKey())
			return false;
		cookieStream = hasNext.getValue();
		return remove(cookieIdentifier, cookieStream);
	}

	protected abstract Iterable<CookieIdentifier> getCookieIdentifiers();

	protected abstract Iterable<HttpCookie> get(CookieIdentifier cookieIdentifier);

	protected abstract void addOrReplace(CookieIdentifier cookieIdentifier, StreamEx<HttpCookie> cookieStream);

	protected abstract boolean remove(CookieIdentifier cookieIdentifier, StreamEx<HttpCookie> cookieStream);

	// utils

	private static EntryStream<CookieIdentifier, HttpCookie> stream(URI uri, Iterable<HttpCookie> cookies,
			boolean cookiesFromServer) {
		var estream = Utils.Lots.stream(cookies).nonNull().distinct().mapToEntry(v -> {
			return CookieIdentifier.tryBuild(uri, v, cookiesFromServer).orElse(null);
		}, v -> v);
		estream = estream.nonNullKeys();
		return estream;
	}

	private static StreamEx<CookieIdentifier> streamCookieIdentifierVariations(URI uri) {
		if (uri == null)
			return StreamEx.empty();
		var ciStreams = streamPathChain(uri).map(path -> {
			var ciHostStreams = streamHostChain(uri).map(host -> {
				var ciHostStream = StreamEx.of(true, false).map(matchSubHosts -> {
					return CookieIdentifier.builder().host(host).path(path).matchSubHosts(matchSubHosts).build();
				});
				return ciHostStream;
			});
			return Utils.Lots.flatMap(ciHostStreams);
		});
		var ciStream = Utils.Lots.flatMap(ciStreams);
		return ciStream;
	}

	private static StreamEx<String> streamPathChain(URI uri) {
		if (uri == null)
			return StreamEx.empty();
		String path = URIs.normalizePath(uri.getPath());
		StringBuilder sb = new StringBuilder();
		List<String> paths = new ArrayList<>();
		var indexIter = IntStreamEx.range(path.length()).iterator();
		while (indexIter.hasNext()) {
			var charAt = path.charAt(indexIter.nextInt());
			sb.append(charAt);
			if (Objects.equals('/', charAt) || !indexIter.hasNext())
				paths.add(URIs.normalizePath(sb.toString()));
		}
		paths = Utils.Lots.reverseView(paths);
		return Utils.Lots.stream(paths).distinct();
	}

	private static StreamEx<String> streamHostChain(URI uri) {
		if (uri == null)
			return StreamEx.empty();
		var host = uri.getHost();
		if (Utils.Strings.isEmpty(host))
			return StreamEx.empty();
		if (IPs.isValidIpAddress(host))
			return StreamEx.of(host);
		var domain = TLDs.parseDomainName(host);
		if (domain != null) {
			host = Utils.Strings.substringBeforeLast(host, domain);
			if (Utils.Strings.isEmpty(host) || ".".equals(host))
				return StreamEx.of(domain);
		}
		List<String> hostSplit;
		{
			var hostSplitStream = Utils.Lots.stream(host.split(Pattern.quote(".")));
			hostSplitStream = hostSplitStream.append(domain);
			hostSplitStream = hostSplitStream.filter(Predicate.not(Utils.Strings::isEmpty));
			hostSplit = hostSplitStream.toList();
		}
		StreamEx<String> hostStream = IntStreamEx.range(hostSplit.size()).mapToObj(i -> {
			var subHost = Utils.Lots.stream(hostSplit).skip(i).joining(".");
			return subHost;
		});
		return hostStream;
	}

	private static StreamEx<HttpCookie> normalizeCookies(CookieIdentifier cookieIdentifier,
			Iterable<HttpCookie> cookies) {
		StreamEx<HttpCookie> cookieStream = Utils.Lots.stream(cookies).nonNull().distinct();
		{// normalize domains
			cookieStream = cookieStream.map(v -> {
				var domain = cookieIdentifier.toDomain();
				if (Utils.Strings.equals(domain, v.getDomain()))
					return v;
				return Cookie.builder(v).domain(domain).build().toHttpCookie();
			});
		}
		{// normalize paths
			cookieStream = cookieStream.map(v -> {
				var path = cookieIdentifier.getPath();
				if (Utils.Strings.equals(path, v.getPath()))
					return v;
				return Cookie.builder(v).path(path).build().toHttpCookie();
			});
		}
		cookieStream = cookieStream.nonNull().distinct();
		return cookieStream;

	}

	protected static Set<HttpCookie> modifyCookieSet(Collection<HttpCookie> cookieCollection,
			Iterable<HttpCookie> input, boolean remove, Runnable onModify) {
		Runnable onModifyF = onModify != null ? onModify : Utils.Functions.doNothingRunnable();
		Supplier<Predicate<HttpCookie>> validCookiePredicateSupplier = () -> {
			Set<HttpCookie> uniqueTracker = new HashSet<>();
			return cookie -> {
				if (cookie == null || cookie.hasExpired() || !uniqueTracker.add(cookie)) {
					onModifyF.run();
					return false;
				}
				return true;
			};

		};
		Set<HttpCookie> cookieSet;
		{
			StreamEx<HttpCookie> stream = Utils.Lots.stream(cookieCollection);
			stream = stream.filter(validCookiePredicateSupplier.get());
			var hasNext = Utils.Lots.hasNext(stream);
			if (!hasNext.getKey() && remove)
				return null;
			stream = hasNext.getValue();
			cookieSet = stream.toCollection(LinkedHashSet::new);
		}
		input = Utils.Lots.stream(input).nonNull().distinct();
		for (var cookie : input) {
			if (remove) {
				if (cookieSet.remove(cookie))
					onModifyF.run();
			} else {
				cookieSet.removeIf(v -> Objects.equals(cookie, v) && !Cookies.equalsExact(cookie, v));
				if (cookieSet.add(cookie))
					onModifyF.run();
			}
		}
		cookieSet.removeIf(Predicate.not(validCookiePredicateSupplier.get()));
		if (cookieSet.isEmpty())
			return null;
		return cookieSet;
	}

}
