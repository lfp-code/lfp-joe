package com.lfp.joe.net.socket.socks.jsocks;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Objects;

import javax.net.SocketFactory;

import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.net.proxy.ProxyEnabled;

import net.sourceforge.jsocks.socks.SocksSocket;
import net.sourceforge.jsocks.socks.UserPasswordAuthentication;

public class JsocksSocks5SocketFactory extends SocketFactory implements ProxyEnabled {
	private Proxy proxy;
	private JsocksSocks5Proxy socks5Proxy;

	public JsocksSocks5SocketFactory(Proxy proxy) {
		this.proxy = Objects.requireNonNull(proxy);
		this.socks5Proxy = new JsocksSocks5Proxy(proxy);
		this.socks5Proxy.resolveAddrLocally(true);
		if (proxy.isAuthenticationEnabled())
			this.socks5Proxy.setAuthenticationMethod(UserPasswordAuthentication.METHOD_ID,
					new UserPasswordAuthentication(proxy.getUsername().orElse(""), proxy.getPassword().orElse("")));
	}

	@Override
	public Proxy getProxy() {
		return proxy;
	}

	@Override
	public Socket createSocket() throws IOException {
		JsocksSocks5Socket socket = new JsocksSocks5Socket(socks5Proxy);
		return socket;
	}

	@Override
	public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
		return new SocksSocket(this.socks5Proxy, host, port);
	}

	@Override
	public Socket createSocket(InetAddress host, int port) throws IOException {
		return new SocksSocket(this.socks5Proxy, host.getHostName(), port);
	}

	@Override
	public Socket createSocket(String host, int port, InetAddress localHost, int localPort)
			throws IOException, UnknownHostException {
		throw new UnsupportedOperationException(
				"not supported:createSocket(String host, int port, InetAddress localHost, int localPort)");
	}

	@Override
	public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort)
			throws IOException {
		throw new UnsupportedOperationException(
				"not supported:createSocket(InetAddress address, int port, InetAddress localAddress, int localPort)");
	}

}
