package com.lfp.joe.net.ssl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;

public class CertificateReader {

	private static String BEGIN_CERT_DELIM = "-----BEGIN CERTIFICATE-----";
	private static String END_CERT_DELIM = "-----END CERTIFICATE-----";

	private File file;

	public CertificateReader(File file) {
		this.file = Objects.requireNonNull(file);
	}

	public Certificate[] getCertificates() throws CertificateException, IOException {
		String pem = Bytes.from(Files.readAllBytes(file.toPath())).encodeUtf8();
		List<Bytes> ders = parseDERs(pem);
		List<Certificate> result = new ArrayList<>(ders.size());
		CertificateFactory factory = CertificateFactory.getInstance("X.509");
		for (Bytes der : ders) {
			try (InputStream is = der.inputStream()) {
				Certificate cert = factory.generateCertificate(is);
				result.add(cert);
			}
		}
		return result.stream().toArray(Certificate[]::new);
	}

	protected static List<Bytes> parseDERs(String pem) {
		if (Utils.Strings.isBlank(pem))
			return Collections.emptyList();
		List<Bytes> bytesList = new ArrayList<>();
		String[] parts = pem.split(String.format("(?i)%s", BEGIN_CERT_DELIM));
		for (String part : parts) {
			int index = Utils.Strings.indexOfIgnoreCase(part, END_CERT_DELIM);
			if (index >= 0)
				part = part.substring(0, index);
			part = Utils.Strings.trim(part);
			if (!Utils.Strings.isBlank(part))
				bytesList.add(Bytes.parseBase64(part));
		}
		return bytesList;
	}

}
