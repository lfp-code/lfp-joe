package com.lfp.joe.net.auth;

import java.lang.reflect.Modifier;
import java.net.Authenticator;
import java.net.InetAddress;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.time.Duration;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BooleanSupplier;
import java.util.function.Function;
import java.util.stream.Stream;

import org.checkerframework.checker.index.qual.NonNegative;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.immutables.value.Value;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.Expiry;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.Durations;
import com.lfp.joe.net.auth.ImmutableAuthenticatorLFP.AuthenticatorCacheOptions;
import com.lfp.joe.net.auth.ImmutableAuthenticatorLFP.AuthenticatorCacheValue;
import com.lfp.joe.net.auth.ImmutableAuthenticatorLFP.AuthenticatorContext;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

@ValueLFP.Style
@Value.Enclosing
public class AuthenticatorLFP extends Authenticator {

	private static final AuthenticatorLFP INSTANCE;
	static {
		var instance = new AuthenticatorLFP();
		Authenticator.setDefault(instance);
		INSTANCE = instance;
	}

	public static AuthenticatorLFP getDefault() {
		return INSTANCE;
	}

	private final Cache<AuthenticatorCacheOptions, AuthenticatorCacheValue> authenticatorFunctionCache;

	private AuthenticatorLFP() {
		this.authenticatorFunctionCache = Caffeine.newBuilder()
				.expireAfter(new Expiry<AuthenticatorCacheOptions, AuthenticatorCacheValue>() {

					@Override
					public long expireAfterCreate(@NonNull AuthenticatorCacheOptions key,
							@NonNull AuthenticatorCacheValue value, long currentTime) {
						return ttlNanos(key, value, true);
					}

					@Override
					public long expireAfterUpdate(@NonNull AuthenticatorCacheOptions key,
							@NonNull AuthenticatorCacheValue value, long currentTime,
							@NonNegative long currentDuration) {
						return ttlNanos(key, value, true);
					}

					@Override
					public long expireAfterRead(@NonNull AuthenticatorCacheOptions key,
							@NonNull AuthenticatorCacheValue value, long currentTime,
							@NonNegative long currentDuration) {
						return ttlNanos(key, value, false);
					}

					private long ttlNanos(AuthenticatorCacheOptions key, AuthenticatorCacheValue value, boolean write) {
						var sb = Stream.<Duration>builder();
						if (write)
							nonMax(key.expireAfterWrite()).ifPresent(sb);
						nonMax(key.expireAfterLastSuccess()).map(ttl -> {
							var expiresAt = ttl.plusMillis(value.lastSuccessAtReference().get());
							return expiresAt.minusMillis(System.currentTimeMillis());
						}).ifPresent(sb::add);
						var ttlNanos = Streams.of(sb.build()).nonNull().map(Duration::toNanos).sorted().findFirst()
								.orElse(Long.MAX_VALUE);
						return Math.max(0, ttlNanos);
					}

					private Optional<Duration> nonMax(Duration duration) {
						return Optional.ofNullable(duration).filter(v -> !Durations.max().equals(v));
					}

				}).build();

	};

	@Override
	public PasswordAuthentication getPasswordAuthentication() {
		var authenticatorContext = getAuthenticatorContext();
		for (var key : authenticatorFunctionCache.asMap().keySet()) {
			var cacheValue = authenticatorFunctionCache.getIfPresent(key);
			if (cacheValue == null)
				continue;
			var passwordAuthentication = cacheValue.function().apply(authenticatorContext);
			if (passwordAuthentication == null)
				continue;
			cacheValue.markSuccess();
			// update expiry
			authenticatorFunctionCache.getIfPresent(key);
			return passwordAuthentication;
		}
		return super.getPasswordAuthentication();
	}

	public BooleanSupplier add(Function<AuthenticatorContext, PasswordAuthentication> authenticatorFunction) {
		return add(authenticatorFunction, AuthenticatorCacheOptions.builder().build());
	}

	public BooleanSupplier add(Function<AuthenticatorContext, PasswordAuthentication> authenticatorFunction,
			Duration expireAfterLastSuccess) {
		var optionsb = AuthenticatorCacheOptions.builder();
		if (expireAfterLastSuccess != null)
			optionsb.expireAfterLastSuccess(expireAfterLastSuccess);
		return add(authenticatorFunction, optionsb.build());
	}

	public BooleanSupplier add(Function<AuthenticatorContext, PasswordAuthentication> authenticatorFunction,
			AuthenticatorCacheOptions options) {
		Objects.requireNonNull(authenticatorFunction);
		if (options == null)
			return add(authenticatorFunction, AuthenticatorCacheOptions.builder().build());
		if (options.identifier().isEmpty())
			return add(authenticatorFunction,
					AuthenticatorCacheOptions.copyOf(options).withIdentifier(authenticatorFunction));
		authenticatorFunctionCache.get(options,
				nil -> AuthenticatorCacheValue.builder().function(authenticatorFunction).build());
		return () -> authenticatorFunctionCache.asMap().remove(options) != null;
	}

	protected AuthenticatorContext getAuthenticatorContext() {
		var builder = AuthenticatorContext.builder();
		builder.authenticator(this);
		builder.host(Optional.ofNullable(getRequestingHost()));
		builder.port(getRequestingPort());
		builder.prompt(getRequestingPrompt());
		builder.protocol(getRequestingProtocol());
		builder.requestorType(getRequestorType());
		builder.scheme(getRequestingScheme());
		builder.site(Optional.ofNullable(getRequestingSite()));
		builder.url(getRequestingURL());
		return builder.build();
	}

	@Value.Immutable
	public static abstract class AbstractAuthenticatorContext {

		private static final long serialVersionUID = -6685508453602963048L;

		public abstract AuthenticatorLFP authenticator();

		public abstract Optional<String> host();

		public abstract int port();

		public abstract String prompt();

		public abstract String protocol();

		public abstract RequestorType requestorType();

		public abstract String scheme();

		public abstract Optional<InetAddress> site();

		public abstract URL url();

	}

	@Value.Immutable
	public static abstract class AbstractAuthenticatorCacheOptions {

		public abstract Optional<Object> identifier();

		@Value.Default
		public Duration expireAfterWrite() {
			return Durations.max();
		}

		@Value.Default
		public Duration expireAfterLastSuccess() {
			return Durations.max();
		}

	}

	@Value.Immutable
	static abstract class AbstractAuthenticatorCacheValue {

		public abstract Function<AuthenticatorContext, PasswordAuthentication> function();

		public void markSuccess() {
			lastSuccessAtReference().set(System.currentTimeMillis());
		}

		@Value.Derived
		public AtomicLong lastSuccessAtReference() {
			return new AtomicLong(System.currentTimeMillis());
		}

	}

	public static void main(String[] args) {
		System.out.println(AuthenticatorCacheOptions.builder().expireAfterLastSuccess(Durations.max())
				.expireAfterWrite(Duration.ofMillis(Long.MAX_VALUE)).build());

		var mstream = CoreReflections.streamMethods(Authenticator.class, false);
		mstream = mstream.filter(v -> !Modifier.isStatic(v.getModifiers()));
		mstream = mstream.filter(v -> Modifier.isPublic(v.getModifiers()) || Modifier.isProtected(v.getModifiers()));
		mstream = mstream.filter(v -> v.getParameterCount() == 0);
		mstream = mstream.filter(v -> Authenticator.class.equals(v.getDeclaringClass()));
		var nameMethodStream = Streams.of(mstream).mapToEntry(v -> {
			var name = v.getName();
			name = Utils.Strings.removeStartIgnoreCase(name, "get");
			name = Utils.Strings.removeStartIgnoreCase(name, "requesting");
			name = Utils.Strings.uncapitalize(name);
			return name;
		}).invert();
		nameMethodStream = nameMethodStream.sortedBy(ent -> ent.getKey().toLowerCase());
		var codeStream = nameMethodStream.map(ent -> {
			var name = ent.getKey();
			var method = ent.getValue();
			return String.format("public abstract %s %s();\n", method.getReturnType().getSimpleName(), name);
		});
		codeStream.forEach(System.out::println);
	}
}
