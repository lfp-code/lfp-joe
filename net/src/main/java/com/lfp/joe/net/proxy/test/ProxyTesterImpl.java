package com.lfp.joe.net.proxy.test;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.time.Duration;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Objects;
import java.util.function.BiConsumer;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.net.auth.AuthenticatorLFP;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.client.WebbHttpClientLFP;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.headers.UserAgents;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.net.proxy.ProxyAuthenticator;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;

public class ProxyTesterImpl implements ProxyTester {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final URI DEFAULT_URI = URI.create("http://www.msftncsi.com/ncsi.txt");
	private static final String DEFAULT_USER_AGENT = "Microsoft NCSI";
	private static final int DEFAULT_PASSES = 2;
	static final ThrowingFunction<Request, Response, IOException> DEFAULT_ENGINE = request -> {
		WebbHttpClientLFP client = HttpClients.getDefault(request.proxy.asJdkProxy());
		com.goebl.david.Request req = client.get(request.uri.toString());
		for (String name : request.headers.keySet())
			for (String value : Utils.Lots.stream(request.headers.get(name)))
				req = req.header(name, value);
		if (request.proxy.isAuthenticationEnabled()) {
			if (Proxy.Type.SOCKS_5.equals(request.proxy.getType()))
				AuthenticatorLFP.getDefault().add(new ProxyAuthenticator(request.proxy), Duration.ofMinutes(5));
			else
				req = req.header(Headers.AUTHORIZATION, Headers.basicAuthorization(
						request.proxy.getUsername().orElse(null), request.proxy.getPassword().orElse(null)));
		}
		com.goebl.david.Response<InputStream> response = req.asStream();
		Closeable onClose = () -> {
			HttpURLConnection connection = response.getConnection();
			if (connection != null)
				Utils.Functions.catching(() -> {
					connection.disconnect();
					return null;
				}, t -> null);
		};
		return Response.builder().withBody(() -> response.getBody()).withStatusCode(response.getStatusCode())
				.withCloseable(onClose).build();
	};

	private final ThrowingFunction<Request, Response, IOException> engine;

	public ProxyTesterImpl() {
		this(DEFAULT_ENGINE);
	}

	public ProxyTesterImpl(ThrowingFunction<Request, Response, IOException> engine) {
		this.engine = Objects.requireNonNull(engine);
	}

	@Override
	public boolean isValid(Proxy proxy, BiConsumer<Proxy, IOException> errorConsumer) {
		return isValid(proxy, null, errorConsumer);
	}

	@Override
	public boolean isValid(Proxy proxy, Options options, BiConsumer<Proxy, IOException> errorConsumer) {
		try {
			validate(proxy, options);
			return true;
		} catch (IOException e) {
			if (errorConsumer != null)
				errorConsumer.accept(proxy, e);
		}
		return false;
	}

	@Override
	public void validate(Proxy proxy) throws IOException {
		validate(proxy, null);
	}

	@Override
	public void validate(Proxy proxy, Options options) throws IOException {
		Objects.requireNonNull(proxy);
		options = normalizeOptions(options);
		boolean valid = true;
		Request request = Request.build(proxy, options);
		Response response = null;
		String responseBody = null;
		Throwable failure = null;
		for (int i = 0; valid && i < options.passes; i++) {
			request = Request.builderFrom(request).withPassIndex(i).build();
			try {
				valid = false;
				response = this.engine.apply(request);
				Validate.isTrue(response.statusCode / 100 == 2, "request is unsuccessful. code:%s",
						response.statusCode);
				responseBody = Bytes.from(response.body.get()).encodeUtf8();
				valid = !Utils.Strings.isBlank(responseBody);
			} catch (Throwable t) {
				failure = t;
			} finally {
				Closeable closeable = response == null ? null : response.closeable;
				if (closeable != null)
					closeable.close();
			}
		}
		if (!valid) {
			String errorMessage = String.format("proxy failed validation. responseBody:%s proxy:%s requestURI:%s",
					responseBody, proxy, request.uri);
			if (failure == null)
				throw new IOException(errorMessage);
			else
				throw new IOException(errorMessage, failure);
		}

	}

	protected Options normalizeOptions(Options options) {
		options = options != null ? options : Options.builder().build();
		if (options.uri == null)
			options = Options.builderFrom(options).withUri(DEFAULT_URI).build();
		if (options.passes <= 0)
			options = Options.builderFrom(options).withPasses(DEFAULT_PASSES).build();
		HeaderMap httpHeaders = HeaderMap.of(options.headers);
		if (!httpHeaders.contains(Headers.CONNECTION, true))
			httpHeaders = httpHeaders.withReplace(Headers.CONNECTION, "close");
		if (!httpHeaders.contains(Headers.USER_AGENT, true)) {
			String value;
			if (DEFAULT_URI.equals(options.uri))
				value = DEFAULT_USER_AGENT;
			else
				value = UserAgents.CHROME_LATEST.get();
			var estream = Utils.Lots.stream(options.headers);
			estream = estream.append(Headers.USER_AGENT, Arrays.asList(value));
			options = Options.builderFrom(options).withHeaders(estream.toCustomMap(LinkedHashMap::new)).build();
		}
		return options;
	}

}
