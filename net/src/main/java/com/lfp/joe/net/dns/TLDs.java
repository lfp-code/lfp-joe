package com.lfp.joe.net.dns;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.utils.Utils;

import de.malkusch.whoisServerList.publicSuffixList.PublicSuffixList;
import de.malkusch.whoisServerList.publicSuffixList.rule.Rule;

public class TLDs {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String SCHEME_SEP = "://";
	private static final String SLASH = "/";
	private static final String QUERY_PREFIX = "?";
	private static final String HASH_PREFIX = "#";
	private static final String PORT = ":";
	private static final String PUBLIC_SUFFIX_ORG = "https://publicsuffix.org/list/public_suffix_list.dat";
	private static final Duration EXPIRE_DURATION = Duration.ofHours(48);

	private static LoadingCache<Optional<Void>, PublicSuffixList> PublicSuffixList_CACHE = Caffeine.newBuilder()
			.expireAfterWrite(EXPIRE_DURATION).build(nil -> {
				PublicSuffixList publicSuffixList;
				try (InputStream is = new FileInputStream(readOrDownloadToFile());) {
					publicSuffixList = PublicSuffixListFactoryLFP.INSTANCE.apply(is);
				}
				return publicSuffixList;
			});

	public static PublicSuffixList getPublicSuffixList() {
		return PublicSuffixList_CACHE.get(Optional.empty());
	}

	private static File readOrDownloadToFile() throws IOException {
		File cacheFile = Utils.Files.tempFile(THIS_CLASS, "public-suffix-v1.dat");
		boolean cacheValid = false;
		if (cacheFile.exists() && cacheFile.isFile()) {
			Duration elapsed = Duration.ofMillis(new Date().getTime() - cacheFile.lastModified());
			if (elapsed.toMillis() < EXPIRE_DURATION.toMillis())
				cacheValid = true;
		}
		if (cacheValid)
			return cacheFile;
		if (cacheFile.exists())
			cacheFile.delete();
		logger.info("updating tld suffix information from: " + PUBLIC_SUFFIX_ORG);
		String publicSuffixData = HttpClients.getDefault().get(PUBLIC_SUFFIX_ORG).ensureSuccess().asString().getBody();
		publicSuffixData = removePrivateDomains(publicSuffixData);
		FileUtils.writeStringToFile(cacheFile, publicSuffixData, StandardCharsets.UTF_8);
		return cacheFile;
	}

	private static String removePrivateDomains(String publicSuffixData) {
		String[] chunks = publicSuffixData.split("\\/\\/.*BEGIN PRIVATE DOMAINS.*");
		if (chunks.length != 2)
			return publicSuffixData;
		String[] postChunks = chunks[1].split("\\/\\/.*END PRIVATE DOMAINS.*");
		if (postChunks.length != 2)
			return publicSuffixData;
		return chunks[0] + postChunks[1];
	}

	public static boolean domainEquals(URI uri, String domainName) {
		if (uri == null)
			return false;
		if (domainName == null || domainName.isEmpty() || Utils.Strings.containsWhitespace(domainName))
			return false;
		var host = uri.getHost();
		if (host == null)
			return false;
		var indexOfDomainName = Utils.Strings.indexOfIgnoreCase(host, domainName);
		if (indexOfDomainName < 0)
			return false;
		else if (indexOfDomainName == 0)
			return true;
		else if (Objects.equals('.', host.charAt(indexOfDomainName - 1)))
			return true;
		return domainEquals(host, domainName);
	}

	public static boolean domainEquals(String url, String domainName) {
		if (Utils.Strings.isBlank(url))
			return false;
		if (domainName == null || domainName.isEmpty() || Utils.Strings.containsWhitespace(domainName))
			return false;
		if (!Utils.Strings.containsIgnoreCase(url, domainName))
			return false;
		var urlDomainName = parseDomainName(url);
		return Utils.Strings.equalsIgnoreCase(urlDomainName, domainName);
	}

	public static String parseDomainName(URI uri) {
		return parseDomainName(uri == null ? null : uri.getHost());
	}

	public static String parseDomainName(String url) {
		if (StringUtils.isBlank(url))
			return null;
		{// scheme
			int index = StringUtils.indexOfIgnoreCase(url, SCHEME_SEP);
			if (index >= 0)
				url = url.substring(index + SCHEME_SEP.length(), url.length());
		}
		{// slash
			int index = StringUtils.indexOfIgnoreCase(url, SLASH);
			if (index >= 0)
				url = url.substring(0, index);
		}
		{// port
			int index = StringUtils.indexOfIgnoreCase(url, PORT);
			if (index >= 0)
				url = url.substring(0, index);
		}
		{// query
			int index = StringUtils.indexOfIgnoreCase(url, HASH_PREFIX);
			if (index >= 0)
				url = url.substring(0, index);
		}
		{// hash
			int index = StringUtils.indexOfIgnoreCase(url, QUERY_PREFIX);
			if (index >= 0)
				url = url.substring(0, index);
		}
		if (StringUtils.isBlank(url))
			return null;
		if (IPs.isValidIpAddress(url))
			return null;
		var result = StringUtils.lowerCase(getPublicSuffixList().getRegistrableDomain(url));
		return result;
	}

	public static String parseTLD(String url) {
		return getPublicSuffixList().getPublicSuffix(parseDomainName(url));
	}

	public static ImmutableSet<String> getAllTLDs() {
		Set<String> res = Sets.newLinkedHashSet();
		for (Rule rule : getPublicSuffixList().getRules()) {
			String patt = rule.getPattern();
			while (StringUtils.startsWith(patt, "*") || StringUtils.startsWith(patt, ".")) {
				patt = patt.substring(1);
			}
			if (StringUtils.isBlank(patt)) {
				continue;
			}
			res.add(patt);
		}
		return ImmutableSet.copyOf(res);
	}

	public static boolean secondLevelDomainMatches(URI uri, String secondLevelDomain) {
		if (uri == null)
			return false;
		secondLevelDomain = Utils.Strings.stripStart(secondLevelDomain, ".");
		secondLevelDomain = Utils.Strings.stripEnd(secondLevelDomain, ".");
		if (Utils.Strings.isBlank(secondLevelDomain))
			return false;
		var host = uri.getHost();
		int indexOfSLD = Utils.Strings.indexOfIgnoreCase(host, secondLevelDomain);
		if (indexOfSLD < 0)
			return false;
		if (Utils.Strings.equalsIgnoreCase(host, secondLevelDomain))
			return false;
		if (indexOfSLD == 0) {
			var hostSplit = host.split(Pattern.quote("."));
			if (hostSplit.length == 2 && Utils.Strings.equalsIgnoreCase(hostSplit[0], secondLevelDomain)
					&& !Utils.Strings.isBlank(hostSplit[1]))
				return true;
		}
		var domainName = parseDomainName(uri);
		if (Utils.Strings.startsWithIgnoreCase(domainName, secondLevelDomain + "."))
			return true;
		return false;
	}

	public static void main(String[] args) throws InterruptedException {
		System.out.println(TLDs.domainEquals(URI.create("http://auth.cool.com"), "coOl.com"));
		System.out.println(TLDs.domainEquals(URI.create("http://cool.com"), "coOl.com"));
		getAllTLDs();
		for (int i = 0; i < 100; i++) {
			System.out.println(parseDomainName("http://192.178.1.128/neat/cool"));
			System.out.println(parseDomainName("google.co.Uk/neat/cool"));
			System.out.println(parseTLD("google.co.Uk/neat/cool"));
			Thread.sleep(2 * 1000);
		}

	}

}
