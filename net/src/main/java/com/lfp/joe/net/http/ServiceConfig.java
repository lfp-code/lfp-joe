package com.lfp.joe.net.http;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.function.Function;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Converter;
import org.apache.commons.lang3.Validate;
import org.reflections8.ReflectionUtils;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.properties.converter.ClassConverter;
import com.lfp.joe.core.properties.converter.MultimapConverter;
import com.lfp.joe.core.properties.converter.URIConverter;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.reflections.reflection.ReflectionPredicates;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Hashable;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public interface ServiceConfig extends Config, Hashable {

	@ConverterClass(URIConverter.class)
	URI uri();

	@ConverterClass(URIConverter.class)
	URI proxyURI();

	@ConverterClass(MultimapConverter.class)
	Map<String, List<String>> headersDefault();

	@ConverterClass(HeaderGeneratorConverter.class)
	List<HeaderGenerator> headerGenerators();

	@SuppressWarnings("unchecked")
	@Override
	default Bytes hash() {
		var hash = Hashable.basicHash(Utils.Crypto.getMessageDigestSHA512(), this, (pd, valueAccessor) -> {
			if ("headerGenerators".equals(pd.getName())) {
				var headerGenerators = (Iterable<HeaderGenerator>) valueAccessor.get();
				var value = Streams.<Object>of(headerGenerators)
						.nonNull()
						.map(Object::getClass)
						.map(CoreReflections::getNamedClassType)
						.map(Class::getName)
						.distinct()
						.sorted()
						.joining("#");
				valueAccessor.accept(value);
			} else if ("headersDefault".equals(pd.getName())) {
				var map = (Map<String, List<String>>) valueAccessor.get();
				var value = EntryStreams.ofMultimap(map).map(ent -> {
					return Streams.of(ent.getKey(), ent.getValue()).joining(":");
				}).distinct().sorted().joining("#");
				valueAccessor.accept(value);
			}
			return true;
		});
		return hash;
	}

	@Hashable.Disabled
	default Map<String, List<String>> headers() {
		Map<String, List<String>> result = new LinkedHashMap<>();
		Consumer<Map<String, ? extends Collection<String>>> adder = m -> {
			if (m == null)
				return;
			var estream = EntryStream.of(m).nonNullKeys().nonNullValues();
			estream = estream.filterKeys(k -> !result.containsKey(k));
			estream.forEach(ent -> {
				var valueStream = Utils.Lots.stream(ent.getValue()).map(v -> v == null ? "" : v);
				for (String value : valueStream) {
					var list = result.computeIfAbsent(ent.getKey(), nil -> new ArrayList<>());
					if (!list.contains(value))
						list.add(value);
				}
			});
		};
		var headerGenerators = Utils.Lots.stream(this.headerGenerators());
		for (var headerGenerator : headerGenerators) {
			if (headerGenerator == null)
				continue;
			// generated headers will be preferred over static
			adder.accept(headerGenerator.generateHeaders(this));
		}
		adder.accept(headersDefault());
		return result;
	}

	@Hashable.Disabled
	default HeaderMap headerMap() {
		var headers = this.headers();
		return HeaderMap.of(headers);
	}

	@Hashable.Disabled
	default StreamEx<Bytes> authorizationTokens() {
		var headerMap = this.headerMap();
		var stream = headerMap.stream(Headers.AUTHORIZATION).values();
		stream = stream.map(v -> Utils.Strings.removeStartIgnoreCase(v, "Bearer "));
		stream = stream.filter(Utils.Strings::isNotBlank).distinct();
		var bytesStream = stream.map(v -> {
			Bytes bytes = null;
			if (Utils.Bits.isBase64(v))
				bytes = Utils.Functions.catching(() -> Utils.Bits.parseBase64(v), t -> null);
			if (bytes == null)
				bytes = Utils.Bits.from(v);
			return bytes;
		});
		bytesStream = bytesStream.distinct();
		return bytesStream;
	}

	@SuppressWarnings("unchecked")
	public static StreamEx<ServiceConfig> discover(Object object) {
		if (object == null)
			return StreamEx.empty();
		Class<?> classType;
		Object invokeOn;
		if (object instanceof Class) {
			classType = (Class<?>) object;
			invokeOn = null;
		} else {
			classType = object.getClass();
			invokeOn = object;
		}
		Function<Object, ServiceConfig> serviceConfigConverter = obj -> {
			if (obj == null)
				return null;
			if (obj instanceof ServiceConfig)
				return (ServiceConfig) obj;
			if (obj instanceof ServiceConfigEnabled)
				return ((ServiceConfigEnabled) obj).getServiceConfig();
			return null;
		};
		StreamEx<ServiceConfig> result = StreamEx.empty();
		{// methods
			StreamEx<Method> methods = JavaCode.Reflections.streamMethods(classType, true,
					ReflectionPredicates.Methods.PUBLIC, ReflectionPredicates.Methods.NO_ARGUMENTS, m -> {
						if (ReflectionUtils.withReturnTypeAssignableTo(ServiceConfig.class).test(m))
							return true;
						if (ReflectionUtils.withReturnTypeAssignableTo(ServiceConfigEnabled.class).test(m))
							return true;
						return false;
					});
			StreamEx<ServiceConfig> stream = methods.map(m -> {
				Object obj;
				if (Modifier.isStatic(m.getModifiers()))
					obj = Utils.Functions.unchecked(() -> m.invoke(null));
				else if (invokeOn == null)
					obj = null;
				else
					obj = Utils.Functions.unchecked(() -> m.invoke(invokeOn));
				return obj;
			}).map(serviceConfigConverter);
			result = result.append(stream);
		}
		{// fields
			StreamEx<Field> fields = JavaCode.Reflections.streamFields(classType, true,
					ReflectionPredicates.Fields.PUBLIC, f -> {
						if (ReflectionUtils.withTypeAssignableTo(ServiceConfig.class).test(f))
							return true;
						if (ReflectionUtils.withTypeAssignableTo(ServiceConfigEnabled.class).test(f))
							return true;
						return false;
					});
			StreamEx<ServiceConfig> stream = fields.map(f -> {
				Object obj;
				if (Modifier.isStatic(f.getModifiers()))
					obj = Utils.Functions.unchecked(() -> f.get(null));
				else if (invokeOn == null)
					obj = null;
				else
					obj = Utils.Functions.unchecked(() -> f.get(invokeOn));
				return obj;
			}).map(serviceConfigConverter);
			result = result.append(stream);
		}
		result = result.nonNull().distinct();
		return result;
	}

	public static interface ServiceConfigEnabled {

		ServiceConfig getServiceConfig();

	}

	public static interface HeaderGenerator {

		Map<String, List<String>> generateHeaders(ServiceConfig service);

	}

	public static class HeaderGeneratorConverter implements Converter<HeaderGenerator> {

		private static Map<Class<? extends HeaderGenerator>, HeaderGenerator> INSTANCE_CACHE = new ConcurrentHashMap<>();
		private static final ClassConverter CLASS_CONVERTER = new ClassConverter();

		@SuppressWarnings("unchecked")
		@Override
		public HeaderGenerator convert(Method method, String input) {
			var classType = CLASS_CONVERTER.convert(method, input);
			if (classType == null)
				return null;
			Validate.isTrue(HeaderGenerator.class.isAssignableFrom(classType),
					"class type can't be assigned to HeaderGenerator:%s", classType);
			return convert((Class<? extends HeaderGenerator>) classType);
		}

		public static HeaderGenerator convert(Class<? extends HeaderGenerator> classType) {
			if (classType == null)
				return null;
			HeaderGenerator headerGenerator = INSTANCE_CACHE.computeIfAbsent(classType,
					nil -> Utils.Types.newInstanceUnchecked(classType));
			return headerGenerator;
		}
	}

}
