package com.lfp.joe.net.ssl;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.function.Supplier;

import javax.net.ssl.X509TrustManager;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingConsumer;

import one.util.streamex.StreamEx;

public class MultiX509TrustManager implements X509TrustManager {

	private final boolean requireAllValid;
	private final Supplier<StreamEx<X509TrustManager>> delegates;

	public MultiX509TrustManager(X509TrustManager... trustManagers) {
		this(true, trustManagers);
	}

	public MultiX509TrustManager(boolean requireAllValid, X509TrustManager... trustManagers) {
		this.requireAllValid = requireAllValid;
		this.delegates = () -> Utils.Lots.stream(trustManagers).nonNull().distinct();
		Validate.isTrue(this.delegates.get().iterator().hasNext(), "%s is required", X509TrustManager.class.getName());
	}

	private void invoke(ThrowingConsumer<X509TrustManager, CertificateException> task) throws CertificateException {
		CertificateException exception = null;
		try (var stream = this.delegates.get();) {
			for (var tm : stream) {
				try {
					task.accept(tm);
					if (!requireAllValid)
						return;
				} catch (CertificateException e) {
					if (requireAllValid)
						throw e;
					exception = e;
				}
			}
		}
		throw exception;
	}

	@Override
	public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
		invoke(tm -> tm.checkClientTrusted(chain, authType));
	}

	@Override
	public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
		invoke(tm -> tm.checkServerTrusted(chain, authType));

	}

	@Override
	public X509Certificate[] getAcceptedIssuers() {
		return this.delegates.get().flatMap(v -> Utils.Lots.stream(v.getAcceptedIssuers())).nonNull().distinct()
				.toArray(X509Certificate.class);
	}
}
