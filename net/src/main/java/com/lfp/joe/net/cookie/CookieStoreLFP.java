package com.lfp.joe.net.cookie;

import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Hashable;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.StreamEx;

public interface CookieStoreLFP extends CookieStore {

	default long size() {
		return streamCookies().count();
	}

	default boolean isEmpty() {
		return Objects.equals(0l, size());
	}

	StreamEx<CookieIdentifier> streamCookieIdentifiers();

	@Override
	default List<URI> getURIs() {
		return streamURIs().toList();
	}

	default StreamEx<URI> streamURIs() {
		return streamCookieIdentifiers().map(CookieIdentifier::toURI);
	}

	@Override
	default List<HttpCookie> getCookies() {
		return streamCookies().toList();
	}

	default StreamEx<HttpCookie> streamCookies() {
		var streams = streamCookieIdentifiers().map(this::stream);
		return Utils.Lots.flatMap(streams).nonNull();
	}

	@Override
	default List<HttpCookie> get(URI uri) {
		var result = stream(uri).toList();
		return result;
	}

	StreamEx<HttpCookie> stream(URI uri);

	StreamEx<HttpCookie> stream(CookieIdentifier cookieIdentifier);

	@Override
	default void add(URI uri, HttpCookie cookie) {
		add(uri, Arrays.asList(cookie));
	}

	default void add(URI uri, Iterable<HttpCookie> cookies) {
		add(uri, cookies, true);
	}

	default void add(URI uri, HttpCookie cookie, boolean cookiesFromServer) {
		add(uri, Arrays.asList(cookie), cookiesFromServer);
	}

	void add(URI uri, Iterable<HttpCookie> cookies, boolean cookiesFromServer);

	@Override
	default boolean remove(URI uri, HttpCookie cookie) {
		return remove(uri, Arrays.asList(cookie));
	}

	default boolean remove(URI uri, Iterable<HttpCookie> cookies) {
		return remove(uri, cookies, true);
	}

	default boolean remove(URI uri, HttpCookie cookie, boolean cookiesFromServer) {
		return remove(uri, Arrays.asList(cookie), cookiesFromServer);
	}

	boolean remove(URI uri, Iterable<HttpCookie> cookies, boolean cookiesFromServer);

	default Bytes hashCookies() {
		var md = Utils.Crypto.getMessageDigestMD5();
		var uriStream = Utils.Lots.stream(this.getURIs());
		uriStream = uriStream.sorted();
		for (var uri : uriStream) {
			Utils.Crypto.update(md, "uri");
			Utils.Crypto.update(md, uri);
			var hashIter = stream(uri).map(Cookie::build).map(Hashable::hash).sorted().iterator();
			for (int i = 0; hashIter.hasNext(); i++) {
				Utils.Crypto.update(md, i);
				Utils.Crypto.update(md, hashIter.next());
			}
		}
		return Bytes.from(md.digest());
	}

	default Optional<String> getCookieHeaderValue(URI requestURI) {
		return getCookieHeaderValue(requestURI, null);
	}

	default Optional<String> getCookieHeaderValue(URI requestURI, Predicate<HttpCookie> filter) {
		if (requestURI == null)
			return Optional.empty();
		var cookies = get(requestURI);
		if (cookies == null)
			return Optional.empty();
		if (filter != null)
			cookies.removeIf(filter);
		if (cookies.isEmpty())
			return Optional.empty();
		StringBuilder cookieHeader = new StringBuilder();
		for (int i = 0, size = cookies.size(); i < size; i++) {
			if (i > 0)
				cookieHeader.append("; ");
			HttpCookie cookie = cookies.get(i);
			cookieHeader.append(cookie.getName()).append('=').append(cookie.getValue());
		}
		return Utils.Strings.trimToNullOptional(cookieHeader.toString());

	}

}