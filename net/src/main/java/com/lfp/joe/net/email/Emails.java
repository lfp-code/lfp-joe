package com.lfp.joe.net.email;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;

import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import com.google.re2j.Matcher;
import com.google.re2j.Pattern;

import one.util.streamex.StreamEx;

public class Emails {

	public static final String EMAIL_REGEX = "(([\\.\\w\\+\\-])+@([\\w\\-]+\\.)+[A-Za-z]{2,})";

	public static StreamEx<String> parseAddresses(String input) {
		if (Utils.Strings.isBlank(input))
			return StreamEx.empty();
		Pattern p = Pattern.compile(EMAIL_REGEX);
		Matcher m = p.matcher(input);
		Iterator<String> iter = Utils.Lots.newIterator(il -> {
			if (m.find())
				return m.group();
			return il.end();
		});
		if (!iter.hasNext())
			iter = JavaMails.streamAddresses(input).map(InternetAddress::getAddress).iterator();
		// technically you can mix cases on emails, but.... no
		return Utils.Lots.stream(iter).filter(Utils.Strings::isNotBlank).map(Utils.Strings::lowerCase).distinct();
	}

	public static Optional<String> parseAddress(String input) {
		return parseAddress(input, true);
	}

	public static Optional<String> parseAddress(String input, boolean requireSingleAddress) {
		var list = parseAddresses(input).limit(2).toList();
		if (list.isEmpty())
			return Optional.empty();
		if (requireSingleAddress && list.size() != 1)
			return Optional.empty();
		return Optional.of(list.iterator().next());
	}

	public static Optional<String> parseName(String input) {
		return JavaMails.streamAddresses(input).map(InternetAddress::getPersonal).filter(Utils.Strings::isNotBlank)
				.findFirst();
	}

	public static Optional<EmailAddress> parse(String input) {
		return EmailAddress.tryBuild(input);
	}

	public static void main(String[] args) {
		var inputs = new ArrayList<String>();
		inputs.add("reggie.pierce@gmail.com");
		inputs.add("reggie pierce <reggie.pierce@gmail.com>");
		inputs.add("reg <jack.pierce@gmail.com>");
		inputs.add("jack man <jack.pierce@gmail.com>");
		inputs.add("jack maa <jack.pierce@gmail.com>");
		inputs.add(" <yep.pierce@gmail.com>");
		inputs.add("reggie.pierce");
		var list = Utils.Lots.stream(inputs).map(v -> Emails.parse(v).orElse(null)).nonNull().sorted().distinct()
				.toList();
		System.out.println(Serials.Gsons.getPretty().toJson(list));
		System.out.println(list.get(0).compareTo(null));
	}
}