package com.lfp.joe.net.http.headers.uaservice;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.List;
import java.util.Optional;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.lfp.joe.core.classpath.Instances;

import nl.basjes.parse.useragent.UserAgent;

public class TopUserAgentsService extends AbstractUserAgentService {

	public static TopUserAgentsService get() {
		return Instances.get(TopUserAgentsService.class,
				() -> new TopUserAgentsService(Duration.ofMinutes(15), Duration.ofDays(1)));
	}

	private static final URI SERVICE_URI = URI
			.create("https://raw.githubusercontent.com/Kikobeats/top-user-agents/master/index.json");
	private static final Gson GSON = new Gson();
	@SuppressWarnings("serial")
	private static final TypeToken<List<String>> TYPE_TOKEN = new TypeToken<List<String>>() {
	};

	protected TopUserAgentsService(Duration availabilityRefreshInterval, Duration valueRefreshInterval) {
		super(availabilityRefreshInterval, valueRefreshInterval);
	}

	@Override
	protected Iterable<UserAgent> loadUserAgents(Duration valueRefreshInterval, Optional<String> browserNameOp)
			throws IOException {
		// filtering will handle
		return super.loadUserAgents(valueRefreshInterval, Optional.empty());
	}

	@Override
	protected Iterable<String> loadValues(Optional<String> browserNameOp) throws IOException, InterruptedException {
		return load();
	}

	@Override
	protected boolean loadAvailability() throws IOException, InterruptedException {
		return !load().isEmpty();
	}

	private List<String> load() throws IOException, InterruptedException {
		var req = newHttpRequestBuilder().uri(SERVICE_URI).build();
		var resp = getHttpClient().send(req, BodyHandlers.ofInputStream());
		validate(resp);
		try (var is = resp.body(); var reader = new InputStreamReader(is, StandardCharsets.UTF_8)) {
			return GSON.fromJson(reader, TYPE_TOKEN.getType());
		}
	}

	public static void main(String[] args) {
		var stream = TopUserAgentsService.get().stream("chroMe");
		for (var ua : stream)
			System.out.println(ua);
		System.exit(0);
	}
}
