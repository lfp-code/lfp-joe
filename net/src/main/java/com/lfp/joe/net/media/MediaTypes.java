package com.lfp.joe.net.media;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.bytes.InputStreamSource;

import org.apache.tika.config.TikaConfig;
import org.apache.tika.detect.DefaultDetector;
import org.apache.tika.detect.Detector;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MimeType;

import com.google.common.net.MediaType;

public class MediaTypes {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final TikaConfig TIKA_CONFIG = TikaConfig.getDefaultConfig();
	private static final Detector TIKA_DETECTOR = new DefaultDetector();

	public static Optional<MediaType> tryParse(Object value) {
		if (value == null)
			return Optional.empty();
		if (value instanceof MediaType)
			return Optional.of((MediaType) value);
		if (value instanceof File)
			return tryParse((File) value);
		if (value instanceof InputStreamSource)
			return tryParse((InputStreamSource) value);
		if (value instanceof HeaderMap)
			return tryParse((HeaderMap) value);
		if (!(value instanceof String)) {
			var classTypeName = value.getClass().getName();
			Predicate<List<String>> matchPredicate = vals -> {
				if (vals == null || vals.isEmpty())
					return false;
				for (var val : vals) {
					if (!Utils.Strings.containsIgnoreCase(classTypeName, val))
						return false;
				}
				return true;
			};
			if (matchPredicate.test(List.of("media", "type")))
				value = value.toString();
			else if (matchPredicate.test(List.of("mime", "type")))
				value = value.toString();
		}
		if (!(value instanceof String))
			return Optional.empty();
		var str = ((String) value);
		if (Utils.Strings.isBlank(str))
			return Optional.empty();
		try {
			return Optional.of(MediaType.parse(str));
		} catch (IllegalArgumentException e) {
			logger.trace("could not use media type to parse: " + value, e);
		}
		return Optional.empty();
	}

	@SuppressWarnings("resource")
	public static Optional<MediaType> tryParse(HeaderMap headerMap) {
		if (headerMap == null || headerMap.isEmpty())
			return Optional.empty();
		for (var value : headerMap.stream(Headers.CONTENT_TYPE).values()) {
			var op = tryParse(value);
			if (op.isPresent())
				return op;
		}
		var fileNameOp = Headers.tryGetFileName(headerMap);
		if (fileNameOp.isEmpty())
			return Optional.empty();
		Metadata metadata = new Metadata();
		metadata.add(Metadata.RESOURCE_NAME_KEY, fileNameOp.get());
		return tryParse(null, metadata);
	}

	public static Optional<MediaType> tryParse(File file) {
		if (file == null)
			return Optional.empty();
		Metadata metadata = new Metadata();
		metadata.add(Metadata.RESOURCE_NAME_KEY, file.getName());
		return tryParse(InputStreamSource.create(() -> new FileInputStream(file)), metadata);
	}

	public static Optional<MediaType> tryParse(InputStreamSource inputStreamSource) {
		return tryParse(inputStreamSource, null);
	}

	private static Optional<MediaType> tryParse(InputStreamSource inputStreamSource, Metadata metadata) {
		if (inputStreamSource == null)
			inputStreamSource = InputStreamSource.empty();
		org.apache.tika.mime.MediaType tikaMediaType = null;
		if (metadata == null)
			metadata = new Metadata();
		try (var is = inputStreamSource.openStream(); var tis = TikaInputStream.get(is)) {
			tikaMediaType = TIKA_DETECTOR.detect(tis, metadata);
		} catch (IOException e) {
			return Optional.empty();
		}
		return tryParse(tikaMediaType);
	}

	public static Optional<String> getExtension(MediaType mediaType) {
		if (mediaType == null)
			return Optional.empty();
		List<String> candidates = List.of(mediaType.toString(), mediaType.type() + "/" + mediaType.subtype());
		for (var candidate : candidates) {
			MimeType mimeType = Utils.Functions.catching(() -> TIKA_CONFIG.getMimeRepository().forName(candidate),
					t -> null);
			if (mimeType == null)
				continue;
			var extension = mimeType.getExtension();
			extension = Utils.Strings.stripStart(extension, ".");
			extension = Utils.Strings.trim(extension);
			if (Utils.Strings.isNotBlank(extension))
				return Optional.of(extension);
		}
		return Optional.empty();
	}

	public static boolean matches(MediaType mediaType, String type, String subtype, Charset charset) {
		if (mediaType == null)
			return false;
		boolean allEmpty = true;
		if (allEmpty && Utils.Strings.isNotBlank(type))
			allEmpty = false;
		if (allEmpty && Utils.Strings.isNotBlank(subtype))
			allEmpty = false;
		if (allEmpty && charset != null)
			allEmpty = false;
		if (allEmpty)
			return true;
		BiPredicate<String, String> isMatch = (requirement, value) -> {
			if (Utils.Strings.isBlank(requirement))
				return true;
			return Utils.Strings.equalsIgnoreCase(requirement, value);
		};
		if (!isMatch.test(type, mediaType.type()))
			return false;
		if (!isMatch.test(subtype, mediaType.subtype()))
			return false;
		if (!isMatch.test(charset == null ? null : charset.name(),
				Optional.ofNullable(mediaType.charset().orNull()).map(v -> v.name()).orElse(null)))
			return false;
		return true;
	}

	public static void main(String[] args) {
		var hm = HeaderMap.of(Headers.CONTENT_DISPOSITION, "attachment; filename=\"filename.jpg\"");
		System.out.println(tryParse(hm).orElse(null));
	}
}
