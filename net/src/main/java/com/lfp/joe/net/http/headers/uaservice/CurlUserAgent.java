package com.lfp.joe.net.http.headers.uaservice;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import org.slf4j.Logger;

import com.lfp.joe.core.log.Logging;

public class CurlUserAgent {

	protected CurlUserAgent() {
	}

	private static final Logger logger = Logging.logger();
	private static final String US_PREFIX = "curl/";
	private static final String VERSION_FALLBACK = "7.79.1";
	private static final Object VERSION_LOCK = new Object();
	private static String _VERSION;

	public static String get() {
		if (_VERSION == null)
			synchronized (VERSION_LOCK) {
				if (_VERSION == null)
					try {
						_VERSION = loadVersion();
					} catch (IOException e) {
						throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e)
								: new RuntimeException(e);
					}
			}
		return US_PREFIX + _VERSION;
	}

	private static String loadVersion() throws IOException {
		var commands = new String[] { "curl", "--version" };
		Process process;
		try {
			process = Runtime.getRuntime().exec(commands);
		} catch (IOException e) {
			logger.warn("curl exec failed:{}", Arrays.toString(commands), e);
			return VERSION_FALLBACK;
		}
		var out = new ByteArrayOutputStream();
		try (var is = process.getInputStream()) {
			is.transferTo(out);
		}
		var err = new ByteArrayOutputStream();
		try (var is = process.getErrorStream()) {
			is.transferTo(err);
		}
		process.onExit().join();
		String version;
		if (process.exitValue() != 0) {
			logger.warn("curl version failed:\n{}", new String(err.toByteArray(), StandardCharsets.UTF_8));
			version = null;
		} else
			version = loadVersion(new String(out.toByteArray(), StandardCharsets.UTF_8));
		if (version == null || version.isBlank())
			version = VERSION_FALLBACK;
		return version;
	}

	private static String loadVersion(String output) {
		var parts = output.split("\\s+");
		var curlFound = false;
		for (var part : parts) {
			if (!curlFound && part.equalsIgnoreCase("curl")) {
				curlFound = true;
				continue;
			}
			if (!curlFound)
				continue;
			Boolean partValid = null;
			for (int i = 0; i < part.length(); i++) {
				var charAt = part.charAt(i);
				if (Character.isDigit(charAt) || '.' == charAt) {
					partValid = true;
				} else {
					partValid = false;
					break;
				}
			}
			if (Boolean.TRUE.equals(partValid))
				return part;
		}
		return null;
	}

	public static void main(String[] args) {
		var ua = CurlUserAgent.get();
		System.out.println(ua);
	}
}
