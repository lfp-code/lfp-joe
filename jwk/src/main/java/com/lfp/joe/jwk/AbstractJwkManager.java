package com.lfp.joe.jwk;

import java.io.IOException;
import java.security.Key;
import java.time.Duration;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.function.Supplier;

import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.jwt.builder.JwtBuilderLFP;
import com.lfp.joe.jwt.resolver.AbstractSigningKeyResolver;
import com.lfp.joe.jwt.resolver.ImmutableAbstractSigningKeyResolver.SigningKeyRequest;
import com.lfp.joe.jwt.resolver.SigningKeyRequestFilters;
import com.lfp.joe.jwt.token.JwkLFP;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.utils.Utils;
import com.nimbusds.jose.jwk.JWK;

import one.util.streamex.StreamEx;

public abstract class AbstractJwkManager extends AbstractSigningKeyResolver implements JwkManager {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	private final String issuer;
	private final Duration keyUpdateWindow;
	private final Duration keyValidWindow;
	private final Supplier<JWK> jwkGenerator;
	private final Predicate<SigningKeyRequest> requestFilter;

	public AbstractJwkManager(String issuer, Duration keyUpdateWindow, Duration keyValidWindow) {
		this(issuer, keyUpdateWindow, keyValidWindow, () -> Jwks.createECKey());
	}

	public AbstractJwkManager(String issuer, Duration keyUpdateWindow, Duration keyValidWindow,
			Supplier<JWK> jwkGenerator) {
		issuer = Utils.Strings.trimToNull(issuer);
		if (issuer == null) {
			this.issuer = null;
			this.requestFilter = null;
		} else {
			var issuerURI = URIs.parse(issuer).map(URIs::normalize).orElse(null);
			if (issuerURI != null) {
				this.issuer = issuerURI.toString();
				this.requestFilter = SigningKeyRequestFilters.issuerURIMatches(issuerURI);
			} else {
				this.issuer = issuer;
				this.requestFilter = SigningKeyRequestFilters.issuerHostsEquals(this.issuer);
			}
		}
		this.keyUpdateWindow = Objects.requireNonNull(keyUpdateWindow);
		this.keyValidWindow = Objects.requireNonNull(keyValidWindow);
		this.jwkGenerator = Objects.requireNonNull(jwkGenerator);
	}

	@Override
	public Predicate<? super SigningKeyRequest> requestFilter() {
		return requestFilter;
	}

	@Override
	protected Iterable<? extends Key> resolveSigningKeys(SigningKeyRequest request) {
		var kid = request.header().getKeyId();
		var records = Throws.unchecked(() -> streamRecords());
		return records.filter(v -> {
			return Objects.equals(kid, v.getId());
		}).map(JwkRecord::getValue).map(Throws.functionUnchecked(JwkLFP::getPublicKey));
	}

	@Override
	public JwtBuilderLFP newJwtBuilder() throws IOException {
		JwkRecord record = null;
		var iter = streamRecords().iterator();
		while (iter.hasNext()) {
			var next = iter.next();
			if (record == null || next.getCreatedAt().after(record.getCreatedAt()))
				record = next;
		}
		var jwtBuilder = record.createJwtBuilder();
		if (issuer != null)
			jwtBuilder.setIssuer(issuer);
		return jwtBuilder;
	}

	@Override
	public StreamEx<JwkLFP> streamJwks(boolean includePrivate) throws IOException {
		return streamRecords().map(v -> {
			if (includePrivate)
				return v.getValue();
			return v.getPublicJwk();
		});
	}

	protected StreamEx<JwkRecord> streamRecords() throws IOException {
		Iterable<JwkRecord> ible;
		try {
			ible = readOrUpdate(false);
		} catch (Exception e) {
			throw Utils.Exceptions.as(e, IOException.class);
		}
		return Utils.Lots.stream(ible);
	}

	protected Collection<JwkRecord> readOrUpdate(boolean locked) throws Exception {
		Collection<JwkRecord> jwkRecColl = Jwks.mergeJwkRecords(read())
				.filter(v -> isValid(v, null))
				.chain(Utils.Lots::asCollection);
		// read existing
		var validRecord = jwkRecColl.stream().anyMatch(v -> isValid(v, this.keyUpdateWindow));
		var privateRecord = jwkRecColl.stream().anyMatch(v -> v.isPrivate());
		if (validRecord && privateRecord)
			return jwkRecColl;
		if (!locked)
			return lockAccess(() -> readOrUpdate(true));
		{// make public, prepend one with new private
			var jwkRecStream = Utils.Lots.stream(jwkRecColl);
			jwkRecStream = jwkRecStream.map(JwkRecord::toPublicRecord);
			jwkRecStream = jwkRecStream.distinct().sorted();
			jwkRecStream = jwkRecStream.prepend(createJwkRecord());
			jwkRecColl = jwkRecStream.chain(Utils.Lots::asCollection);
		}
		write(Utils.Lots.stream(jwkRecColl));
		return jwkRecColl;
	}

	protected Collection<JwkRecord> update(Iterable<JwkRecord> jwkRecIterable) throws IOException {
		Collection<JwkRecord> jwkRecColl;
		{// make public, prepend one with private
			var jwkRecStream = Utils.Lots.stream(jwkRecIterable);
			jwkRecStream = jwkRecStream.map(JwkRecord::toPublicRecord);
			jwkRecStream = jwkRecStream.distinct().sorted();
			jwkRecStream = jwkRecStream.prepend(createJwkRecord());
			jwkRecColl = jwkRecStream.chain(Utils.Lots::asCollection);
		}
		write(Utils.Lots.stream(jwkRecColl));
		return jwkRecColl;

	}

	protected JwkRecord createJwkRecord() {
		var nimbusJwk = this.jwkGenerator.get();
		var jwk = Jwks.toJwk(nimbusJwk, true);
		var blder = JwkRecord.builder(jwk);
		blder.expiresAt(new Date(System.currentTimeMillis() + this.keyValidWindow.toMillis()));
		return blder.build();
	}

	protected boolean isValid(JwkRecord record, Duration maxTTL) {
		if (record == null)
			return false;
		if (record.isExpired(this.keyValidWindow))
			return false;
		if (record.isExpired(maxTTL))
			return false;
		return true;
	}

	protected abstract Iterable<JwkRecord> read() throws IOException;

	protected abstract void write(StreamEx<JwkRecord> jwks) throws IOException;

	protected abstract <X> X lockAccess(ThrowingSupplier<X, Exception> accessor) throws Exception, InterruptedException;
}
