package com.lfp.joe.jwk;

import java.time.Duration;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;

import org.joda.beans.Bean;
import org.joda.beans.BeanDefinition;
import org.joda.beans.ImmutableDefaults;
import org.joda.beans.JodaBeanUtils;
import org.joda.beans.MetaProperty;
import org.joda.beans.PropertyDefinition;
import org.joda.beans.impl.direct.DirectMetaProperty;
import org.joda.beans.impl.direct.DirectMetaPropertyMap;

import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.cache.StatValue;
import com.lfp.joe.jwt.builder.JwtBuilderLFP;
import com.lfp.joe.jwt.token.JwkLFP;
import com.lfp.joe.utils.Utils;
import com.nimbusds.jose.jwk.JWK;

import one.util.streamex.StreamEx;

@BeanDefinition(hierarchy = "immutable")
public class JwkRecord extends StatValue<JwkLFP> implements Comparable<JwkRecord> {

	@PropertyDefinition(get = "optional")
	private final Date expiresAt;

	private transient Map<Boolean, JWK> _nimbusJWKConversionCache;

	protected JWK getNimbusJWK(boolean includePrivate) {
		if (_nimbusJWKConversionCache == null)
			synchronized (this) {
				if (_nimbusJWKConversionCache == null)
					_nimbusJWKConversionCache = new ConcurrentHashMap<>(2);
			}
		return _nimbusJWKConversionCache.computeIfAbsent(includePrivate, nil -> {
			var nimbusJWK = Jwks.toNimbusJWK(getValue());
			if (includePrivate)
				return nimbusJWK;
			return nimbusJWK.toPublicJWK();
		});
	}

	private transient JwkLFP _publicJwk;

	public JwkLFP getPublicJwk() {
		if (_publicJwk == null)
			synchronized (this) {
				if (_publicJwk == null) {
					var nimbusJWK = this.getNimbusJWK(false);
					_publicJwk = Jwks.toJwk(nimbusJWK, false);
				}
			}
		return _publicJwk;
	}

	public String getId() {
		return this.getValue().getId();
	}

	public JwkRecord toPublicRecord() {
		if (!this.isPrivate())
			return this;
		var blder = this.toBuilder();
		blder.value(getPublicJwk());
		return blder.build();
	}

	public boolean isPrivate() {
		var nimbusJWK = this.getNimbusJWK(true);
		return nimbusJWK.isPrivate();
	}

	public JwtBuilderLFP createJwtBuilder() {
		return Jwks.createJwtBuilder(getNimbusJWK(true));
	}

	@SuppressWarnings("unchecked")
	public Optional<JwkRecord> tryMerge(JwkRecord other) {
		if (other == null)
			return Optional.empty();
		if (other == this)
			return Optional.of(this);
		if (!Objects.equals(this.getId(), other.getId()))
			return Optional.empty();
		var jo1 = this.getNimbusJWK(true).toJSONObject();
		var jo2 = other.getNimbusJWK(true).toJSONObject();
		StreamEx<String> keys;
		{
			keys = Utils.Lots.stream(jo1, jo2).nonNull().flatMap(v -> Utils.Lots.stream(v.keySet()));
			keys = keys.filter(Utils.Strings::isNotBlank);
			keys = keys.distinct();
		}
		Map<String, Object> merged = new LinkedHashMap<>();
		BiFunction<Map<String, Object>, String, Object> valueGetter = (jo, k) -> {
			if (jo == null)
				return null;
			if (Utils.Strings.isBlank(k))
				return null;
			var value = jo.get(k);
			if (value instanceof CharSequence && value.toString().isBlank())
				return null;
			return value;
		};
		for (var key : keys) {
			var v1 = valueGetter.apply(jo1, key);
			var v2 = valueGetter.apply(jo2, key);
			if (v1 == null && v2 == null)
				continue;
			if (Objects.deepEquals(v1, v2))
				merged.put(key, v1);
			else if (v1 == null)
				merged.put(key, v2);
			else if (v2 == null)
				merged.put(key, v1);
			else
				return Optional.empty();
		}
		var jwkRecOldest = Utils.Lots.stream(this, other).sortedBy(v -> v.getCreatedAt().getTime()).findFirst().get();
		var jwkRecB = jwkRecOldest.toBuilder();
		jwkRecB.value(Jwks.toJwk(merged));
		// earliest expiration wins
		Utils.Lots.stream(this.getExpiresAt(), other.getExpiresAt()).nonNull().map(v -> v.orElse(null)).nonNull()
				.sortedBy(Date::getTime).findFirst().ifPresent(v -> jwkRecB.expiresAt(v));
		return Optional.of(jwkRecB.build());
	}

	@Override
	public int compareTo(JwkRecord other) {
		Comparator<JwkRecord> compartor = Comparator.comparing(v -> {
			return v.isPrivate() ? 0 : 1;
		});
		compartor = compartor.thenComparing(v -> {
			return v.getCreatedAt().getTime() * -1;
		});
		compartor = compartor.thenComparing(v -> {
			return v.getExpiresAt().map(Date::getTime).map(vv -> -1 * vv).orElse(Long.MAX_VALUE);
		});
		compartor = compartor.thenComparing(v -> {
			return v.getValue().getId();
		});
		compartor = Comparator.nullsLast(compartor);
		return compartor.compare(this, other);
	}

	@Override
	public boolean isExpired(Duration timeToLive) {
		var expiresAt = this.getExpiresAt().orElse(null);
		if (expiresAt != null && expiresAt.getTime() <= System.currentTimeMillis())
			return true;
		return super.isExpired(timeToLive);
	}

	@ImmutableDefaults
	private static void applyDefaults(Builder builder) {
		StatValue.applyDefaultsProtected(builder);
	}

	public static JwkRecord.Builder builder(JwkLFP value) {
		return (Builder) builder().value(value);
	}

	public static void main(String[] args) {
		JodaBeans.updateCode();
	}

	// ------------------------- AUTOGENERATED START -------------------------
	/// CLOVER:OFF
	/**
	 * The meta-bean for {@code JwkRecord}.
	 * 
	 * @return the meta-bean, not null
	 */
	public static JwkRecord.Meta meta() {
		return JwkRecord.Meta.INSTANCE;
	}

	static {
		JodaBeanUtils.registerMetaBean(JwkRecord.Meta.INSTANCE);
	}

	/**
	 * Returns a builder used to create an instance of the bean.
	 * 
	 * @return the builder, not null
	 */
	public static JwkRecord.Builder builder() {
		return new JwkRecord.Builder();
	}

	/**
	 * Restricted constructor.
	 * 
	 * @param builder the builder to copy from, not null
	 */
	protected JwkRecord(JwkRecord.Builder builder) {
		super(builder);
		this.expiresAt = (builder.expiresAt != null ? (Date) builder.expiresAt.clone() : null);
	}

	@Override
	public JwkRecord.Meta metaBean() {
		return JwkRecord.Meta.INSTANCE;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the expiresAt.
	 * 
	 * @return the optional value of the property, not null
	 */
	public Optional<Date> getExpiresAt() {
		return Optional.ofNullable(expiresAt);
	}

	// -----------------------------------------------------------------------
	/**
	 * Returns a builder that allows this bean to be mutated.
	 * 
	 * @return the mutable builder, not null
	 */
	@Override
	public Builder toBuilder() {
		return new Builder(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj != null && obj.getClass() == this.getClass()) {
			JwkRecord other = (JwkRecord) obj;
			return JodaBeanUtils.equal(expiresAt, other.expiresAt) && super.equals(obj);
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = hash * 31 + JodaBeanUtils.hashCode(expiresAt);
		return hash ^ super.hashCode();
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder(64);
		buf.append("JwkRecord{");
		int len = buf.length();
		toString(buf);
		if (buf.length() > len) {
			buf.setLength(buf.length() - 2);
		}
		buf.append('}');
		return buf.toString();
	}

	@Override
	protected void toString(StringBuilder buf) {
		super.toString(buf);
		buf.append("expiresAt").append('=').append(JodaBeanUtils.toString(expiresAt)).append(',').append(' ');
	}

	// -----------------------------------------------------------------------
	/**
	 * The meta-bean for {@code JwkRecord}.
	 */
	public static class Meta extends StatValue.Meta<JwkLFP> {
		/**
		 * The singleton instance of the meta-bean.
		 */
		static final Meta INSTANCE = new Meta();

		/**
		 * The meta-property for the {@code expiresAt} property.
		 */
		private final MetaProperty<Date> expiresAt = DirectMetaProperty.ofImmutable(this, "expiresAt", JwkRecord.class,
				Date.class);
		/**
		 * The meta-properties.
		 */
		private final Map<String, MetaProperty<?>> metaPropertyMap$ = new DirectMetaPropertyMap(this,
				(DirectMetaPropertyMap) super.metaPropertyMap(), "expiresAt");

		/**
		 * Restricted constructor.
		 */
		protected Meta() {
		}

		@Override
		protected MetaProperty<?> metaPropertyGet(String propertyName) {
			switch (propertyName.hashCode()) {
			case 250196615: // expiresAt
				return expiresAt;
			}
			return super.metaPropertyGet(propertyName);
		}

		@Override
		public JwkRecord.Builder builder() {
			return new JwkRecord.Builder();
		}

		@Override
		public Class<? extends JwkRecord> beanType() {
			return JwkRecord.class;
		}

		@Override
		public Map<String, MetaProperty<?>> metaPropertyMap() {
			return metaPropertyMap$;
		}

		// -----------------------------------------------------------------------
		/**
		 * The meta-property for the {@code expiresAt} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Date> expiresAt() {
			return expiresAt;
		}

		// -----------------------------------------------------------------------
		@Override
		protected Object propertyGet(Bean bean, String propertyName, boolean quiet) {
			switch (propertyName.hashCode()) {
			case 250196615: // expiresAt
				return ((JwkRecord) bean).expiresAt;
			}
			return super.propertyGet(bean, propertyName, quiet);
		}

		@Override
		protected void propertySet(Bean bean, String propertyName, Object newValue, boolean quiet) {
			metaProperty(propertyName);
			if (quiet) {
				return;
			}
			throw new UnsupportedOperationException("Property cannot be written: " + propertyName);
		}

	}

	// -----------------------------------------------------------------------
	/**
	 * The bean-builder for {@code JwkRecord}.
	 */
	public static class Builder extends StatValue.Builder<JwkLFP> {

		private Date expiresAt;

		/**
		 * Restricted constructor.
		 */
		protected Builder() {
			applyDefaults(this);
		}

		/**
		 * Restricted copy constructor.
		 * 
		 * @param beanToCopy the bean to copy from, not null
		 */
		protected Builder(JwkRecord beanToCopy) {
			super(beanToCopy);
			this.expiresAt = (beanToCopy.expiresAt != null ? (Date) beanToCopy.expiresAt.clone() : null);
		}

		// -----------------------------------------------------------------------
		@Override
		public Object get(String propertyName) {
			switch (propertyName.hashCode()) {
			case 250196615: // expiresAt
				return expiresAt;
			default:
				return super.get(propertyName);
			}
		}

		@Override
		public Builder set(String propertyName, Object newValue) {
			switch (propertyName.hashCode()) {
			case 250196615: // expiresAt
				this.expiresAt = (Date) newValue;
				break;
			default:
				super.set(propertyName, newValue);
				break;
			}
			return this;
		}

		@Override
		public Builder set(MetaProperty<?> property, Object value) {
			super.set(property, value);
			return this;
		}

		/**
		 * @deprecated Use Joda-Convert in application code
		 */
		@Override
		@Deprecated
		public Builder setString(String propertyName, String value) {
			setString(meta().metaProperty(propertyName), value);
			return this;
		}

		/**
		 * @deprecated Use Joda-Convert in application code
		 */
		@Override
		@Deprecated
		public Builder setString(MetaProperty<?> property, String value) {
			super.setString(property, value);
			return this;
		}

		/**
		 * @deprecated Loop in application code
		 */
		@Override
		@Deprecated
		public Builder setAll(Map<String, ? extends Object> propertyValueMap) {
			super.setAll(propertyValueMap);
			return this;
		}

		@Override
		public JwkRecord build() {
			return new JwkRecord(this);
		}

		// -----------------------------------------------------------------------
		/**
		 * Sets the expiresAt.
		 * 
		 * @param expiresAt the new value
		 * @return this, for chaining, not null
		 */
		public Builder expiresAt(Date expiresAt) {
			this.expiresAt = expiresAt;
			return this;
		}

		// -----------------------------------------------------------------------
		@Override
		public String toString() {
			StringBuilder buf = new StringBuilder(64);
			buf.append("JwkRecord.Builder{");
			int len = buf.length();
			toString(buf);
			if (buf.length() > len) {
				buf.setLength(buf.length() - 2);
			}
			buf.append('}');
			return buf.toString();
		}

		@Override
		protected void toString(StringBuilder buf) {
			super.toString(buf);
			buf.append("expiresAt").append('=').append(JodaBeanUtils.toString(expiresAt)).append(',').append(' ');
		}

	}

	/// CLOVER:ON
	// -------------------------- AUTOGENERATED END --------------------------
}
