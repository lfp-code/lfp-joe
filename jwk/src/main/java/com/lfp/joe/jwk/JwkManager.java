package com.lfp.joe.jwk;

import java.io.IOException;
import java.security.Key;
import java.util.List;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.lfp.joe.jwt.builder.JwtBuilderLFP;
import com.lfp.joe.jwt.resolver.SigningKeyResolverLFP;
import com.lfp.joe.jwt.token.JwkLFP;
import com.lfp.joe.utils.Utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwsHeader;
import one.util.streamex.StreamEx;

public interface JwkManager extends SigningKeyResolverLFP {

	JwtBuilderLFP newJwtBuilder() throws IOException;

	default StreamEx<JwkLFP> streamJwks() throws IOException {
		return streamJwks(false);
	}

	StreamEx<JwkLFP> streamJwks(boolean includePrivate) throws IOException;

	default JwkManager withCache(Caffeine<Object, Object> cacheBuilder) {
		if (cacheBuilder == null)
			return this;
		LoadingCache<Boolean, List<JwkLFP>> cache = cacheBuilder.build(includePrivate -> {
			return this.streamJwks(includePrivate).toImmutableList();
		});
		var self = this;
		return new JwkManager() {

			@SuppressWarnings("rawtypes")
			@Override
			public Key resolveSigningKey(JwsHeader header, Claims claims) {
				return self.resolveSigningKey(header, claims);
			}

			@Override
			public JwtBuilderLFP newJwtBuilder() throws IOException {
				return self.newJwtBuilder();
			}

			@Override
			public StreamEx<JwkLFP> streamJwks(boolean includePrivate) throws IOException {
				var list = cache.get(includePrivate);
				return Utils.Lots.stream(list);
			}
		};
	}

}