package com.lfp.joe.jwk;

import java.security.InvalidKeyException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import com.google.common.reflect.TypeToken;
import com.lfp.joe.jwt.Jwts;
import com.lfp.joe.jwt.builder.JwtBuilderLFP;
import com.lfp.joe.jwt.token.JwkLFP;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.RSAKeyPair;
import com.nimbusds.jose.Algorithm;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.Curve;
import com.nimbusds.jose.jwk.ECKey;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.gen.ECKeyGenerator;

import io.jsonwebtoken.SignatureAlgorithm;
import one.util.streamex.StreamEx;

@SuppressWarnings({ "serial" })
public class Jwks {

	private static final TypeToken<Map<String, Object>> NIMBUS_JSON_OBJECT_TT = new TypeToken<Map<String, Object>>() {};

	public static RSAKey createRSAKey() {
		return createRSAKey(null);
	}

	public static RSAKey createRSAKey(String keyId) {
		RSAKeyPair keyPair = Utils.Crypto.generateRSAKeyPair();
		return createRSAKey(keyPair, keyId);
	}

	public static RSAKey createRSAKey(RSAKeyPair rsaKeyPair, String keyId) {
		var jwsAlgorithm = JWSAlgorithm.RS512;
		var publicKey = rsaKeyPair == null ? null : rsaKeyPair.getPublicKey();
		var privateKey = rsaKeyPair == null ? null : rsaKeyPair.getPrivateKey();
		var builder = new RSAKey.Builder(rsaKeyPair == null ? null : publicKey)
				.privateKey(rsaKeyPair == null ? null : privateKey)
				.keyID(normalizeKeyId(keyId))
				.algorithm(jwsAlgorithm);
		return builder.build();
	}

	public static ECKey createECKey() {
		return createECKey(null);
	}

	public static ECKey createECKey(String keyId) {
		var jwsAlgorithm = JWSAlgorithm.ES512;
		try {
			var jwk = new ECKeyGenerator(Curve.P_521).algorithm(jwsAlgorithm).keyID(normalizeKeyId(keyId)).generate();
			return jwk;
		} catch (JOSEException e) {
			throw Utils.Exceptions.asRuntimeException(e);
		}
	}

	public static JwtBuilderLFP createJwtBuilder(JWK jwk) {
		var blder = new JwtBuilderLFP();
		if (jwk != null) {
			blder.signWith(getPrivateKey(jwk), toSignatureAlgorithm(jwk.getAlgorithm()));
			var keyId = jwk.getKeyID();
			blder.setKeyId(keyId);
		}
		return blder;
	}

	public static PublicKey getPublicKey(JWK jwk) {
		Objects.requireNonNull(jwk);
		try {
			if (jwk instanceof RSAKey)
				return ((RSAKey) jwk).toPublicKey();
			if (jwk instanceof ECKey)
				return ((ECKey) jwk).toPublicKey();
		} catch (JOSEException e) {
			throw Utils.Exceptions.asRuntimeException(e);
		}
		throw new IllegalArgumentException("unable to read public key:" + jwk);
	}

	public static PrivateKey getPrivateKey(JWK jwk) {
		Objects.requireNonNull(jwk);
		try {
			if (jwk instanceof RSAKey)
				return ((RSAKey) jwk).toPrivateKey();
			if (jwk instanceof ECKey)
				return ((ECKey) jwk).toPrivateKey();
		} catch (JOSEException e) {
			throw Utils.Exceptions.asRuntimeException(e);
		}
		throw new IllegalArgumentException("unable to read private key:" + jwk);
	}

	public static SignatureAlgorithm toSignatureAlgorithm(Algorithm algorithm) {
		var name = Optional.ofNullable(algorithm).map(v -> v.getName()).orElse(null);
		if (Utils.Strings.isBlank(name))
			return null;
		return SignatureAlgorithm.forName(name);
	}

	public static JwkLFP toJwk(JWK jwk) {
		return toJwk(jwk, false);
	}

	public static JwkLFP toJwk(JWK jwk, boolean includePrivateKey) {
		Objects.requireNonNull(jwk);
		Map<String, Object> jsonMap;
		if (!includePrivateKey)
			jsonMap = jwk.toPublicJWK().toJSONObject();
		else
			jsonMap = jwk.toJSONObject();
		return toJwk(jsonMap);
	}

	public static JwkLFP toJwk(Map<String, Object> jsonMap) {
		Objects.requireNonNull(jsonMap);
		var jsonElement = Serials.Gsons.get().toJsonTree(jsonMap);
		return Serials.Gsons.get().fromJson(jsonElement, JwkLFP.class);
	}

	public static JWK toNimbusJWK(JwkLFP jwk) {
		Objects.requireNonNull(jwk);
		var jsonElement = Serials.Gsons.get().toJsonTree(jwk);
		Map<String, Object> nimbusJsonObject = Serials.Gsons.get()
				.fromJson(jsonElement, NIMBUS_JSON_OBJECT_TT.getType());
		return Utils.Functions.unchecked(() -> JWK.parse(nimbusJsonObject));
	}

	public static StreamEx<JwkRecord> mergeJwkRecords(Iterable<? extends JwkRecord> jwkRecords) {
		if (jwkRecords == null)
			return StreamEx.empty();
		var jwkRecList = new ArrayList<JwkRecord>();
		for (var jwkRec : Utils.Lots.stream(jwkRecords).nonNull()) {
			var merged = false;
			for (int i = 0; !merged && i < jwkRecList.size(); i++) {
				var jwkRecMergedOp = jwkRecList.get(i).tryMerge(jwkRec);
				if (jwkRecMergedOp.isEmpty())
					continue;
				jwkRecList.set(i, jwkRecMergedOp.get());
				merged = true;
			}
			if (merged)
				continue;
			jwkRecList.add(jwkRec);
		}
		return StreamEx.of(jwkRecList).sorted();
	}

	private static String normalizeKeyId(String keyId) {
		keyId = Utils.Strings.trimToNull(keyId);
		if (keyId == null)
			keyId = "jwk" + Utils.Crypto.getSecureRandomString();
		return keyId;
	}

	public static void main(String[] args) throws JOSEException, InvalidKeyException {
		var ecKey = Jwks.createECKey();
		System.out.println(Serials.Gsons.getPretty().toJson(Jwks.toJwk(ecKey, true)));
		System.out.println(Serials.Gsons.getPretty().toJson(Jwks.toJwk(ecKey, false)));
		Jwks.toJwk(ecKey, true).getPublicKey();
		var key = ecKey;
		{
			var blder = Jwks.createJwtBuilder(key)
					.setIssuer("iplasso.com")
					.setExpiration(new Date(System.currentTimeMillis() + Duration.ofDays(1).toMillis()));
			blder.claim("neat2", new Date());
			var jwt = blder.compact();
			System.out.println(jwt);
			var jwtOp = Jwts.tryParse(jwt, key.toPublicKey());
			System.out.println(jwtOp.orElse(null));
		}
		System.out.println(Serials.Gsons.getPretty().toJson(toJwk(key, false)));
		var json = Serials.Gsons.getPretty().toJson(toJwk(key, true));
		System.out.println(json);
		var jwk = Serials.Gsons.get().fromJson(json, JwkLFP.class);
		System.out.println(jwk);
		{
			var nimbusJwk = toNimbusJWK(jwk);
			System.out.println(Utils.Bits.from(getPublicKey(nimbusJwk).getEncoded()).encodeBase64());
			System.out.println(Utils.Bits.from(getPrivateKey(nimbusJwk).getEncoded()).encodeBase64());
			var blder = Jwks.createJwtBuilder(nimbusJwk).setIssuer("iplasso.com");
			// blder.claim("neat", new Date());
			var jwt = blder.compact();
			System.out.println(jwt);
		}

	}
}
