package com.lfp.joe.jwk;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;
import java.util.Objects;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.gson.JsonObject;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.jwt.config.JwtConfig;
import com.lfp.joe.jwt.token.JwkSetLFP;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.threads.ImmutableFutures.ScheduleWhileRequest;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;

public class JwkManagerEndpoint extends Scrapable.Impl {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final Duration CACHE_EXPIRE_AFTER_WRITE = Duration.ofSeconds(10);
	private static final Duration CACHE_REFRESH_AFTER_WRITE = Duration.ofSeconds(1);
	private static final Duration CACHE_POLL_INTERVAL_DEFAULT = CACHE_REFRESH_AFTER_WRITE;
	private final JwkManager jwkManager;

	private final LoadingCache<Nada, Bytes> wellKnownJwkSetCache;

	public JwkManagerEndpoint(JwkManager jwkManager) {
		this(jwkManager, CACHE_POLL_INTERVAL_DEFAULT);
	}

	public JwkManagerEndpoint(JwkManager jwkManager, Duration cachePollInterval) {
		this.jwkManager = Objects.requireNonNull(jwkManager);
		this.wellKnownJwkSetCache = Caffeine.newBuilder().expireAfterWrite(CACHE_EXPIRE_AFTER_WRITE)
				.refreshAfterWrite(CACHE_POLL_INTERVAL_DEFAULT).executor(Threads.Pools.centralPool().limit())
				.build(nil -> getWellKnownJwkSetResponseFresh());
		if (cachePollInterval != null) {
			var pollFuture = ScheduleWhileRequest.builder(cachePollInterval, () -> {
				this.wellKnownJwkSetCache.get(Nada.get());
			}, () -> {
				if (this.isScrapped())
					return false;
				return true;
			}).errorHandler(t -> {
				if (!Utils.Exceptions.isCancelException(t))
					logger.warn("error during cache poll", t);
				return Nada.get();
			}).build().schedule();
			this.onScrap(() -> pollFuture.cancel(true));
		}
	}

	public Bytes getWellKnownOpenIDConfigurationResponse(URI requestURI) throws IOException {
		JwtConfig jwtConfig = Configs.get(JwtConfig.class);
		int port;
		if (URIs.isSecure(requestURI) && MachineConfig.isDeveloper())
			port = -1;
		else
			port = requestURI.getPort();
		URI wellKnownJwkSetURI;
		try {
			wellKnownJwkSetURI = new URI(requestURI.getScheme(), requestURI.getUserInfo(), requestURI.getHost(), port,
					jwtConfig.wellKnownJwkSetPath(), null, null);
		} catch (URISyntaxException e) {
			throw Utils.Exceptions.as(e, IOException.class);
		}
		JsonObject jo = new JsonObject();
		jo.addProperty(jwtConfig.openIDConfigurationKey_jwks_uri(), wellKnownJwkSetURI.toString());
		return Serials.Gsons.toBytes(jo);
	}

	public Bytes getWellKnownJwkSetResponse() throws IOException {
		return wellKnownJwkSetCache.get(Nada.get());
	}

	protected Bytes getWellKnownJwkSetResponseFresh() throws IOException {
		var keys = jwkManager.streamJwks().toList();
		var jwkSet = JwkSetLFP.builder().keys(keys).build();
		return Serials.Gsons.toBytes(jwkSet);
	}

	public JwkManager getJwkManager() {
		return jwkManager;
	}

}
