package com.lfp.joe.jwk.serialize;

import java.lang.reflect.Type;
import java.security.interfaces.RSAKey;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import java.util.Map;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.lfp.joe.serial.serializer.Serializer;
import com.lfp.joe.utils.Utils;
import com.nimbusds.jose.JOSEException;

@Serializer(value = RSAKey.class, hierarchy = true)
public enum RSAKeySerializer implements JsonSerializer<RSAKey>, JsonDeserializer<RSAKey> {
	INSTANCE;

	@Override
	public RSAKey deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		if (json == null || json.isJsonNull())
			return null;
		var classType = Utils.Types.toClass(typeOfT);
		if (classType == null)
			return null;
		Map<String, Object> jsonMap = context.deserialize(json, Map.class);
		if (jsonMap == null)
			return null;
		try {
			var nimbusRSAKey = com.nimbusds.jose.jwk.RSAKey.parse(jsonMap);
			if (RSAPrivateKey.class.isAssignableFrom(classType))
				return nimbusRSAKey.toRSAPrivateKey();
			else if (RSAPublicKey.class.isAssignableFrom(classType))
				return nimbusRSAKey.toRSAPublicKey();
		} catch (JOSEException | ParseException e) {
			throw Utils.Exceptions.asRuntimeException(e);
		}
		return null;
	}

	@Override
	public JsonElement serialize(RSAKey src, Type typeOfSrc, JsonSerializationContext context) {
		if (src == null)
			return JsonNull.INSTANCE;
		RSAPublicKey rsaPublic;
		RSAPrivateCrtKey rsaPrivate;
		if (src instanceof RSAPublicKey) {
			rsaPrivate = null;
			rsaPublic = (RSAPublicKey) src;
		} else if (src instanceof RSAPrivateKey) {
			rsaPrivate = (RSAPrivateCrtKey) src;
			rsaPublic = Utils.Crypto.generateRSAPublicKey((RSAPrivateKey) src);
		} else
			throw new IllegalArgumentException("unknown key type:" + src.getClass().getName());
		var rsaKeyB = new com.nimbusds.jose.jwk.RSAKey.Builder(rsaPublic);
		if (rsaPrivate != null)
			rsaKeyB.privateKey(rsaPrivate);
		return context.serialize(rsaKeyB.build().toJSONObject());
	}

}
