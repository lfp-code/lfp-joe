package test;

import java.text.ParseException;
import java.time.Duration;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Base58;
import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWEHeader;
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.RSADecrypter;
import com.nimbusds.jose.crypto.RSAEncrypter;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

public class Test {

	public static void main(String[] args) throws JOSEException, ParseException {
		RSAKey senderJWK = new RSAKeyGenerator(2048).keyUse(KeyUse.SIGNATURE).generate();
		System.out.println("server private:\n" + Serials.Gsons.get().toJson(senderJWK.toRSAPrivateKey()));
		System.out.println("server public:\n" + Serials.Gsons.get().toJson(senderJWK.toRSAPublicKey()));
		RSAKey recipientJWK = new RSAKeyGenerator(2048).keyUse(KeyUse.ENCRYPTION).generate();
		System.out.println("tunnel private:\n" + Serials.Gsons.get().toJson(recipientJWK.toRSAPrivateKey()));
		System.out.println("tunnel public:\n" + Serials.Gsons.get().toJson(recipientJWK.toRSAPublicKey()));

		Map<String, Object> data = Map.of("username", "reggiepierce", "password", Utils.Crypto.getSecureRandomString());
		System.out.println("input:" + Serials.Gsons.getPretty().toJson(data));
		RSAKey recipientPublicJWK = recipientJWK.toPublicJWK();
		String encoded = encode(senderJWK, recipientPublicJWK, data);
		System.out.println("token:" + encoded);
		var json = Serials.Gsons.get().toJson(senderJWK);
		System.out.println(json);
		senderJWK = Serials.Gsons.get().fromJson(json, RSAKey.class);
		RSAKey senderPublicJWK = senderJWK.toPublicJWK();
		var decoded = decode(senderPublicJWK, recipientJWK, encoded);
		System.out.println("output:" + Serials.Gsons.getPretty().toJson(decoded));
	}

	private static String encode(RSAKey senderJWK, RSAKey recipientPublicJWK, Map<String, Object> data)
			throws JOSEException {
		var expiresAt = new Date(System.currentTimeMillis() + Duration.ofDays(1).toMillis());
		// expiresAt = new Date();
		var claimBuilder = new JWTClaimsSet.Builder().expirationTime(expiresAt);
		for (var ent : data.entrySet())
			claimBuilder.claim(ent.getKey(), ent.getValue());
		// Create JWT
		SignedJWT signedJWT = new SignedJWT(
				new JWSHeader.Builder(JWSAlgorithm.RS256).keyID(senderJWK.getKeyID()).build(), claimBuilder.build());
		// Sign the JWT
		signedJWT.sign(new RSASSASigner(senderJWK));
		// Create JWE object with signed JWT as payload
		JWEObject jweObject = new JWEObject(
				new JWEHeader.Builder(JWEAlgorithm.RSA_OAEP_256, EncryptionMethod.A256GCM).contentType("JWT").build(),
				new Payload(signedJWT));
		// Encrypt with the recipient's public key
		jweObject.encrypt(new RSAEncrypter(recipientPublicJWK));
		// Serialise to JWE compact form
		String jweString = jweObject.serialize();
		System.out.println("jweString encrypt:" + jweString);
		var partsIter = Utils.Lots.stream(Utils.Strings.split(jweString, '.')).map(Utils.Bits::parseBase64Url)
				.iterator();
		var result = Utils.Lots.stream(partsIter).map(Base58::encodeBytes).joining("-");
		return result;
	}

	private static Map<String, Object> decode(RSAKey senderPublicJWK, RSAKey recipientJWK, String encoded)
			throws JOSEException, ParseException {
		var jweString = Utils.Lots.stream(Utils.Strings.split(encoded, '-')).map(Base58::decodeToBytes)
				.map(v -> v.encodeBase64(true, false)).joining(".");
		System.out.println("jweString decrypt:" + jweString);
		JWEObject jweObject = JWEObject.parse(jweString);
		// Decrypt with private key
		jweObject.decrypt(new RSADecrypter(recipientJWK));
		// Extract payload
		SignedJWT signedJWT = jweObject.getPayload().toSignedJWT();
		Objects.requireNonNull(signedJWT, "payload not a signed JWT");
		// Check the signature
		Validate.isTrue(signedJWT.verify(new RSASSAVerifier(senderPublicJWK)));
		var expirationTime = signedJWT.getJWTClaimsSet().getExpirationTime();
		Validate.isTrue(expirationTime == null || expirationTime.after(new Date()), "expired JWT");
		return signedJWT.getJWTClaimsSet().getClaims();
	}
}
