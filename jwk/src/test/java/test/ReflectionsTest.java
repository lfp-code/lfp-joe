package test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.Future;

import org.apache.commons.lang3.time.StopWatch;

import com.lfp.joe.reflections.JavaCode.Reflections;

public class ReflectionsTest {

	public static void main(String[] args) throws FileNotFoundException, IOException, InterruptedException {
		for (int i = 0; i < 10; i++) {
			var sw = StopWatch.createStarted();
			Reflections.streamSubTypesOf(Future.class).forEach(v -> {
				System.out.println(v.getName());
			});
			System.out.println(i + " - " + sw.getTime());
		}
		Thread.sleep(30_000);
	}
}
