package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.StopWatch;
import org.threadly.concurrent.future.ListenableFuture;

import com.google.common.reflect.TypeToken;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.jwk.AbstractJwkManager;
import com.lfp.joe.jwk.JwkRecord;
import com.lfp.joe.jwk.Jwks;
import com.lfp.joe.jwt.Jwts;
import com.lfp.joe.jwt.token.JwkSetLFP;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.Semaphore;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

import one.util.streamex.StreamEx;

public class JwkManagerTest {

	private static final TypeToken<List<JwkRecord>> TT = new TypeToken<List<JwkRecord>>() {
	};

	public static void main(String[] args) throws InterruptedException, IOException {
		var semaphore = Semaphore.create(1);
		File file = new File("temp/jwk-manager-test.json");
		file.getParentFile().mkdirs();
		File webFile = new File(file.getParentFile(), "jwk-manager-test-web.json");
		var manager = new AbstractJwkManager("reggie-pierce-dev.iplasso.com/" + webFile.getName(),
				Duration.ofSeconds(5), Duration.ofSeconds(30), () -> Jwks.createRSAKey()) {

			private ListenableFuture<Nada> future;

			@Override
			protected Iterable<JwkRecord> read() throws IOException {
				if (!file.exists())
					return null;
				try (var fis = new FileInputStream(file)) {
					return Serials.Gsons.fromStream(fis, TT);
				}
			}

			@Override
			protected void write(StreamEx<JwkRecord> jwks) throws IOException {
				if (future != null && !future.isDone())
					return;
				System.out.println("updating keys");
				var pool = Threads.Pools.centralPool();
				var future = pool.submit(() -> {
					var list = jwks.toList();
					try (var fos = new FileOutputStream(file)) {
						Serials.Gsons.toStream(list, fos);
					}
					var jwkSet = JwkSetLFP.builder().keys(Utils.Lots.stream(list).map(v -> v.getPublicJwk()).toList())
							.build();
					try (var fos = new FileOutputStream(webFile)) {
						Serials.Gsons.toStream(jwkSet, fos);
					}
					return Nada.get();
				});
				Threads.Futures.logFailureError(future, true, "error");
				this.future = future;

			}

			@Override
			protected <X> X lockAccess(ThrowingSupplier<X, Exception> accessor) throws Exception {
				semaphore.acquire();
				try {
					return accessor.get();
				} finally {
					semaphore.release();
				}
			}
		};
		var blder = manager.newJwtBuilder();
		blder.claim("this is cool", new Date());
		blder.claim("wow", "fun");
		var jwt = blder.compact();
		System.out.println(jwt);
		for (int i = 0;; i++) {
			if (i > 0)
				Threads.sleep(Duration.ofSeconds(2));
			var sw = StopWatch.createStarted();
			var kids = manager.streamJwks().map(v -> v.getId()).toList();
			var jws = Jwts.tryParse(jwt, manager).orElse(null);
			var parsed = jws != null;
			if (jws != null) {
				var claims = jws.getBody();
				claims.put(Utils.Crypto.getSecureRandomString(), "hey");
				var jwt2 = manager.newJwtBuilder().addClaims(claims).compact();
				System.out.println(jwt2);
			}
			System.out.println(String.format("%s %s - %s", parsed, sw.getTime(), kids));
		}
	}

}
