package test;

import java.util.ArrayList;
import java.util.List;

import com.lfp.joe.core.lot.AbstractLot;

import one.util.streamex.LongStreamEx;

public class TestTemp {

	public static void main(String[] args) {
		List<Long> times = new ArrayList<>();
		for (int i = 0; i < 25; i++)
			times.add(run());
		var avg = LongStreamEx.of(times).average().getAsDouble();
		System.err.println(avg);
	}

	private static long run() {
		var millis = System.currentTimeMillis();
		var iter = new AbstractLot<String>() {
			long index = -1;

			@Override
			protected String computeNext() {
				index++;
				if (index > 100_000)
					return this.end();
				return index + "_cool";
			}
		};
		iter.forEachRemaining(v -> {
			// System.out.println(v);
		});
		var elapsed = System.currentTimeMillis() - millis;
		System.out.println(elapsed);
		return elapsed;
	}

}