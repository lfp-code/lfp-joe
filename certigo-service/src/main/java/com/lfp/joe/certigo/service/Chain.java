
package com.lfp.joe.certigo.service;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class Chain {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("is_self_signed")
    @Expose
    private Boolean isSelfSigned;
    @SerializedName("pem")
    @Expose
    private String pem;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsSelfSigned() {
        return isSelfSigned;
    }

    public void setIsSelfSigned(Boolean isSelfSigned) {
        this.isSelfSigned = isSelfSigned;
    }

    public String getPem() {
        return pem;
    }

    public void setPem(String pem) {
        this.pem = pem;
    }

}
