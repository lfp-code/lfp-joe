
package com.lfp.joe.certigo.service;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class Certificate {

    @SerializedName("serial")
    @Expose
    private String serial;
    @SerializedName("not_before")
    @Expose
    private String notBefore;
    @SerializedName("not_after")
    @Expose
    private String notAfter;
    @SerializedName("signature_algorithm")
    @Expose
    private String signatureAlgorithm;
    @SerializedName("is_self_signed")
    @Expose
    private Boolean isSelfSigned;
    @SerializedName("subject")
    @Expose
    private Subject subject;
    @SerializedName("issuer")
    @Expose
    private Issuer issuer;
    @SerializedName("basic_constraints")
    @Expose
    private BasicConstraints basicConstraints;
    @SerializedName("ocsp_server")
    @Expose
    private List<String> ocspServer = null;
    @SerializedName("issuing_certificate")
    @Expose
    private List<String> issuingCertificate = null;
    @SerializedName("key_usage")
    @Expose
    private List<String> keyUsage = null;
    @SerializedName("extended_key_usage")
    @Expose
    private List<String> extendedKeyUsage = null;
    @SerializedName("dns_names")
    @Expose
    private List<String> dnsNames = null;
    @SerializedName("pem")
    @Expose
    private String pem;

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getNotBefore() {
        return notBefore;
    }

    public void setNotBefore(String notBefore) {
        this.notBefore = notBefore;
    }

    public String getNotAfter() {
        return notAfter;
    }

    public void setNotAfter(String notAfter) {
        this.notAfter = notAfter;
    }

    public String getSignatureAlgorithm() {
        return signatureAlgorithm;
    }

    public void setSignatureAlgorithm(String signatureAlgorithm) {
        this.signatureAlgorithm = signatureAlgorithm;
    }

    public Boolean getIsSelfSigned() {
        return isSelfSigned;
    }

    public void setIsSelfSigned(Boolean isSelfSigned) {
        this.isSelfSigned = isSelfSigned;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Issuer getIssuer() {
        return issuer;
    }

    public void setIssuer(Issuer issuer) {
        this.issuer = issuer;
    }

    public BasicConstraints getBasicConstraints() {
        return basicConstraints;
    }

    public void setBasicConstraints(BasicConstraints basicConstraints) {
        this.basicConstraints = basicConstraints;
    }

    public List<String> getOcspServer() {
        return ocspServer;
    }

    public void setOcspServer(List<String> ocspServer) {
        this.ocspServer = ocspServer;
    }

    public List<String> getIssuingCertificate() {
        return issuingCertificate;
    }

    public void setIssuingCertificate(List<String> issuingCertificate) {
        this.issuingCertificate = issuingCertificate;
    }

    public List<String> getKeyUsage() {
        return keyUsage;
    }

    public void setKeyUsage(List<String> keyUsage) {
        this.keyUsage = keyUsage;
    }

    public List<String> getExtendedKeyUsage() {
        return extendedKeyUsage;
    }

    public void setExtendedKeyUsage(List<String> extendedKeyUsage) {
        this.extendedKeyUsage = extendedKeyUsage;
    }

    public List<String> getDnsNames() {
        return dnsNames;
    }

    public void setDnsNames(List<String> dnsNames) {
        this.dnsNames = dnsNames;
    }

    public String getPem() {
        return pem;
    }

    public void setPem(String pem) {
        this.pem = pem;
    }

}
