
package com.lfp.joe.certigo.service;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class VerifyResult {

    @SerializedName("ocsp_response")
    @Expose
    private OcspResponse ocspResponse;
    @SerializedName("ocsp_was_stapled")
    @Expose
    private Boolean ocspWasStapled;
    @SerializedName("chains")
    @Expose
    private List<List<Chain>> chains = null;

    public OcspResponse getOcspResponse() {
        return ocspResponse;
    }

    public void setOcspResponse(OcspResponse ocspResponse) {
        this.ocspResponse = ocspResponse;
    }

    public Boolean getOcspWasStapled() {
        return ocspWasStapled;
    }

    public void setOcspWasStapled(Boolean ocspWasStapled) {
        this.ocspWasStapled = ocspWasStapled;
    }

    public List<List<Chain>> getChains() {
        return chains;
    }

    public void setChains(List<List<Chain>> chains) {
        this.chains = chains;
    }

}
