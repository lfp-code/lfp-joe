
package com.lfp.joe.certigo.service;

import java.util.List;
import java.util.Optional;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

@Generated("jsonschema2pojo")
public class CertificateInfo {

	@SerializedName("certificates")
	@Expose
	private List<Certificate> certificates = null;
	@SerializedName("tls_connection")
	@Expose
	private TlsConnection tlsConnection;
	@SerializedName("verify_result")
	@Expose
	private VerifyResult verifyResult;

	private transient String _pem;

	public String getPem() {
		if (_pem == null)
			synchronized (this) {
				if (_pem == null)
					_pem = toPem(Utils.Lots.stream(getCertificates()).map(Certificate::getPem)).orElse("");
			}
		return _pem;
	}

	private transient List<String> _bundlePems;

	public List<String> getBundlePems() {
		if (_bundlePems == null)
			synchronized (this) {
				if (_bundlePems == null) {
					var chainsStream = Optional.ofNullable(getVerifyResult()).map(VerifyResult::getChains)
							.map(Utils.Lots::stream).orElseGet(StreamEx::empty);
					chainsStream = chainsStream.nonNull().filter(v -> !v.isEmpty());
					var bundlePemStream = chainsStream.mapPartial(v -> {
						Optional<String> pemOp = Utils.Lots.stream(v).map(Chain::getPem).chain(s -> toPem(s));
						return pemOp;
					});
					_bundlePems = bundlePemStream.toImmutableList();
				}
			}
		return _bundlePems;
	}

	public List<Certificate> getCertificates() {
		return certificates;
	}

	public void setCertificates(List<Certificate> certificates) {
		this.certificates = certificates;
	}

	public TlsConnection getTlsConnection() {
		return tlsConnection;
	}

	public void setTlsConnection(TlsConnection tlsConnection) {
		this.tlsConnection = tlsConnection;
	}

	public VerifyResult getVerifyResult() {
		return verifyResult;
	}

	public void setVerifyResult(VerifyResult verifyResult) {
		this.verifyResult = verifyResult;
	}

	private static Optional<String> toPem(Iterable<? extends String> iterable) {
		var pem = Utils.Lots.stream(iterable).mapPartial(Utils.Strings::trimToNullOptional)
				.chain(Utils.Strings::joinNewLine);
		return Utils.Strings.trimToNullOptional(pem);
	}
}
