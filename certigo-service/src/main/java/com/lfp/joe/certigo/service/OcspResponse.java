
package com.lfp.joe.certigo.service;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class OcspResponse {

    @SerializedName("Status")
    @Expose
    private Long status;
    @SerializedName("SerialNumber")
    @Expose
    private Long serialNumber;
    @SerializedName("ProducedAt")
    @Expose
    private String producedAt;
    @SerializedName("ThisUpdate")
    @Expose
    private String thisUpdate;
    @SerializedName("NextUpdate")
    @Expose
    private String nextUpdate;
    @SerializedName("RevokedAt")
    @Expose
    private String revokedAt;
    @SerializedName("RevocationReason")
    @Expose
    private Long revocationReason;
    @SerializedName("Certificate")
    @Expose
    private Object certificate;
    @SerializedName("TBSResponseData")
    @Expose
    private String tBSResponseData;
    @SerializedName("Signature")
    @Expose
    private String signature;
    @SerializedName("SignatureAlgorithm")
    @Expose
    private Long signatureAlgorithm;
    @SerializedName("IssuerHash")
    @Expose
    private Long issuerHash;
    @SerializedName("RawResponderName")
    @Expose
    private Object rawResponderName;
    @SerializedName("ResponderKeyHash")
    @Expose
    private String responderKeyHash;
    @SerializedName("Extensions")
    @Expose
    private Object extensions;
    @SerializedName("ExtraExtensions")
    @Expose
    private Object extraExtensions;

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(Long serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getProducedAt() {
        return producedAt;
    }

    public void setProducedAt(String producedAt) {
        this.producedAt = producedAt;
    }

    public String getThisUpdate() {
        return thisUpdate;
    }

    public void setThisUpdate(String thisUpdate) {
        this.thisUpdate = thisUpdate;
    }

    public String getNextUpdate() {
        return nextUpdate;
    }

    public void setNextUpdate(String nextUpdate) {
        this.nextUpdate = nextUpdate;
    }

    public String getRevokedAt() {
        return revokedAt;
    }

    public void setRevokedAt(String revokedAt) {
        this.revokedAt = revokedAt;
    }

    public Long getRevocationReason() {
        return revocationReason;
    }

    public void setRevocationReason(Long revocationReason) {
        this.revocationReason = revocationReason;
    }

    public Object getCertificate() {
        return certificate;
    }

    public void setCertificate(Object certificate) {
        this.certificate = certificate;
    }

    public String getTBSResponseData() {
        return tBSResponseData;
    }

    public void setTBSResponseData(String tBSResponseData) {
        this.tBSResponseData = tBSResponseData;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Long getSignatureAlgorithm() {
        return signatureAlgorithm;
    }

    public void setSignatureAlgorithm(Long signatureAlgorithm) {
        this.signatureAlgorithm = signatureAlgorithm;
    }

    public Long getIssuerHash() {
        return issuerHash;
    }

    public void setIssuerHash(Long issuerHash) {
        this.issuerHash = issuerHash;
    }

    public Object getRawResponderName() {
        return rawResponderName;
    }

    public void setRawResponderName(Object rawResponderName) {
        this.rawResponderName = rawResponderName;
    }

    public String getResponderKeyHash() {
        return responderKeyHash;
    }

    public void setResponderKeyHash(String responderKeyHash) {
        this.responderKeyHash = responderKeyHash;
    }

    public Object getExtensions() {
        return extensions;
    }

    public void setExtensions(Object extensions) {
        this.extensions = extensions;
    }

    public Object getExtraExtensions() {
        return extraExtensions;
    }

    public void setExtraExtensions(Object extraExtensions) {
        this.extraExtensions = extraExtensions;
    }

}
