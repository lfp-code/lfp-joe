
package com.lfp.joe.certigo.service;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class Issuer {

    @SerializedName("common_name")
    @Expose
    private String commonName;
    @SerializedName("country")
    @Expose
    private List<String> country = null;
    @SerializedName("key_id")
    @Expose
    private String keyId;
    @SerializedName("organization")
    @Expose
    private List<String> organization = null;
    @SerializedName("locality")
    @Expose
    private List<String> locality = null;
    @SerializedName("province")
    @Expose
    private List<String> province = null;

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public List<String> getCountry() {
        return country;
    }

    public void setCountry(List<String> country) {
        this.country = country;
    }

    public String getKeyId() {
        return keyId;
    }

    public void setKeyId(String keyId) {
        this.keyId = keyId;
    }

    public List<String> getOrganization() {
        return organization;
    }

    public void setOrganization(List<String> organization) {
        this.organization = organization;
    }

    public List<String> getLocality() {
        return locality;
    }

    public void setLocality(List<String> locality) {
        this.locality = locality;
    }

    public List<String> getProvince() {
        return province;
    }

    public void setProvince(List<String> province) {
        this.province = province;
    }

}
