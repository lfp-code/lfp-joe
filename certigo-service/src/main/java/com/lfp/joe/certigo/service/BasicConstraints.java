
package com.lfp.joe.certigo.service;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class BasicConstraints {

    @SerializedName("is_ca")
    @Expose
    private Boolean isCa;
    @SerializedName("pathlen")
    @Expose
    private Long pathlen;

    public Boolean getIsCa() {
        return isCa;
    }

    public void setIsCa(Boolean isCa) {
        this.isCa = isCa;
    }

    public Long getPathlen() {
        return pathlen;
    }

    public void setPathlen(Long pathlen) {
        this.pathlen = pathlen;
    }

}
