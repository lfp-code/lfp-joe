
package com.lfp.joe.certigo.service;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class TlsConnection {

    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("cipher")
    @Expose
    private String cipher;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCipher() {
        return cipher;
    }

    public void setCipher(String cipher) {
        this.cipher = cipher;
    }

}
