package com.lfp.joe.certigo.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;

import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.Requires;

import at.favre.lib.bytes.Bytes;

public interface CertigoService {

	CertificateInfo connect(InetSocketAddress address) throws IOException;

	default CertificateInfo dump(String pem) throws IOException {
		return dump(pem == null ? null : Utils.Bits.from(pem));
	}

	default CertificateInfo dump(Bytes pem) throws IOException {
		Requires.isTrue(pem != null && pem.length() > 0, "pem contents required");
		var tempFile = Utils.Files.tempFile(this.getClass(),
				String.format("cert-%s.pem", Utils.Crypto.getSecureRandomString()));
		try {
			try (var is = pem.inputStream(); var fos = new FileOutputStream(tempFile)) {
				is.transferTo(fos);
			}
			return dump(tempFile);
		} finally {
			tempFile.delete();
		}
	}

	CertificateInfo dump(File pem) throws IOException;
}
