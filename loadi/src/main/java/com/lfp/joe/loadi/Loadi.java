package com.lfp.joe.loadi;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.threadly.concurrent.SubmitterExecutor;
import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.SettableListenableFuture;

import com.lfp.joe.cache.StatValue;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.core.log.Logging;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.loadi.config.LoadiConfig;
import com.lfp.joe.reactor.Fluxi;
import com.lfp.joe.reactor.Monos;
import com.lfp.joe.reactor.Subscribers;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.KeyGenerator;

import ch.qos.logback.classic.Level;
import one.util.streamex.StreamEx;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;
import reactor.core.publisher.Sinks.Many;

public abstract class Loadi<I, X> implements Function<I, Flux<X>> {

	private final org.slf4j.Logger logger = Logging.logger();
	private static final int VERSION = 1;
	protected static final String MACHINE_ID = Utils.Crypto.getSecureRandomString();
	protected static final Many<EventValue<?>> EVENT_SINK = Sinks.many().multicast().onBackpressureBuffer();

	private final Set<Mono<Long>> asyncTasks = Utils.Lots.newConcurrentHashSet();
	private final LoadiOptions<I> options;
	private final Object[] additionalKeyParts;

	public Loadi(LoadiOptions<I> options, Object... additionalKeyParts) {
		this.options = Objects.requireNonNull(options);
		this.additionalKeyParts = additionalKeyParts;
	}

	public Loadi(Consumer<LoadiOptions.Builder<I>> optionsModifier, Object... additionalKeyParts) {
		LoadiOptions.Builder<I> builder = LoadiOptions.builder();
		Objects.requireNonNull(optionsModifier).accept(builder);
		this.options = builder.build();
		this.additionalKeyParts = additionalKeyParts;
	}

	public Flux<Mono<Long>> getAsyncTasks() {
		return Fluxi.from(this.asyncTasks);
	}

	public Mono<Long> getAsyncTasksFlat() {
		return Fluxi.from(this.asyncTasks).flatMap(Function.identity()).collectList().map(list -> {
			return Utils.Lots.stream(list).nonNull().mapToLong(v -> v > 0 ? v : 0).sum();
		});
	}

	public LoadiOptions<I> getOptions() {
		return options;
	}

	@Override
	public Flux<X> apply(I input) {
		if (input == null)
			return Flux.empty();
		Flux<X> result = Flux.create(sink -> {
			var fluxFuture = this.options.getSyncExecutor().submit(() -> {
				return process(input, sink::next);
			});
			var flux = Monos.fromFuture(fluxFuture).flatMapMany(Function.identity());
			Fluxi.link(flux, sink, () -> fluxFuture.cancel(true));
		});
		result = result.distinct();
		return result;
	}

	private Flux<X> process(I input, Consumer<X> sink) {
		return process(input, sink, false, false, false);
	}

	private Flux<X> process(I input, Consumer<X> sink, boolean listenerConfigured, boolean syncLock,
			boolean asyncLock) {
		{// check if ready
			var result = load(input, this.options.getSyncTTLFunction().apply(input));
			if (shouldForceLoad(input, result, false))
				result = null;
			if (result != null) {
				var asyncTask = processAsync(input, asyncLock);
				asyncTask = logAsyncTask(asyncTask);
				asyncTask = trackAsyncTask(asyncTask);
				asyncTask.subscribeOn(this.options.getAsyncScheduler()).subscribe();
				return result;
			}
		}
		if (!listenerConfigured)
			return processListener(input, sink, () -> process(input, sink, true, false, false));
		if (!syncLock)
			return processLocked(input, false, () -> process(input, sink, true, true, false));
		if (!asyncLock)
			return processLocked(input, true, () -> process(input, sink, true, true, true));
		var publisherMono = createPublisher(input);
		return publisherMono.flatMapMany(publisher -> {
			return Objects.requireNonNull(loadFresh(input, publisher));
		});
	}

	private Mono<Long> processAsync(I input, boolean asyncLock) {
		if (!asyncLock) {
			var lockFunction = createTryLock(input, true);
			var monoFuture = new SettableListenableFuture<Mono<Long>>(false);
			var lockFuture = lockFunction.apply(() -> {
				var mono = processAsync(input, true);
				var doneFuture = new SettableListenableFuture<Nada>(false);
				mono = Monos.doOnDone(mono, () -> doneFuture.setResult(Nada.get()));
				monoFuture.setResult(mono);
				return doneFuture;
			}, () -> {
				monoFuture.setResult(Mono.empty());
				return Nada.get();
			});
			lockFuture.listener(() -> monoFuture.cancel(true));
			try {
				return monoFuture.get();
			} catch (Throwable t) {
				lockFuture.cancel(true);
				throw Throws.unchecked(t);
			}
		}
		{
			var asyncResult = load(input, this.options.getAsyncTTLFunction().apply(input));
			if (shouldForceLoad(input, asyncResult, true))
				asyncResult = null;
			if (asyncResult != null) {
				asyncResult.subscribe(Subscribers.cancel());
				return Mono.empty();
			}
		}
		var publisherMono = createPublisher(input);
		var asyncResultFlux = publisherMono.flatMapMany(publisher -> {
			return Objects.requireNonNull(loadFresh(input, publisher));
		});
		asyncResultFlux = asyncResultFlux.distinct();
		return asyncResultFlux.count();
	}

	private Mono<Long> logAsyncTask(Mono<Long> asyncTask) {
		asyncTask = asyncTask.doOnError(t -> {
			if (!Utils.Exceptions.isCancelException(t))
				logger.warn("async processing failed", t);
		});
		asyncTask = asyncTask.doOnCancel(() -> {
			if (MachineConfig.isDeveloper())
				logger.info("async procesing complete");
		});
		return asyncTask;
	}

	private Mono<Long> trackAsyncTask(Mono<Long> asyncTask) {
		this.asyncTasks.add(asyncTask);
		return Monos.doOnDone(asyncTask, () -> this.asyncTasks.remove(asyncTask));
	}

	private Flux<X> processListener(I input, Consumer<X> sink, Supplier<Flux<X>> loader) {
		var futureFlux = Fluxi.fromFutures(createListeners(input, sink));
		return futureFlux.collectList().flatMapMany(list -> {
			var flux = loader.get();
			flux = Fluxi.doOnDone(flux, () -> Utils.Exceptions.closeQuietly(Level.WARN, list));
			return flux;
		});
	}

	@SuppressWarnings("unchecked")
	protected Iterable<ListenableFuture<AutoCloseable>> createListeners(I input, Consumer<X> sink) {
		var eventBusKey = generateKeyEventBus(input);
		var disposable = EVENT_SINK.asFlux().subscribe(eventValue -> {
			if (!eventBusKey.equals(eventValue.eventBusKey))
				return;
			((EventValue<X>) eventValue).streamValues().map(StatValue::getValue).forEach(sink::accept);
		});
		return Arrays.asList(FutureUtils.immediateResultFuture(disposable::dispose));
	}

	private Mono<Consumer<X>> createPublisher(I input) {
		var futureFlux = Fluxi.fromFutures(createPublishers(input));
		return futureFlux.collectList().map(list -> {
			return value -> {
				for (var consumer : list)
					consumer.accept(value);
			};
		});

	}

	protected Iterable<ListenableFuture<Consumer<X>>> createPublishers(I input) {
		var eventBusKey = generateKeyEventBus(input);
		Consumer<X> publisher = value -> {
			var eventValue = new EventValue<X>(eventBusKey, Arrays.asList(value));
			EVENT_SINK.tryEmitNext(eventValue);
		};
		return Arrays.asList(FutureUtils.immediateResultFuture(publisher));
	}

	private Flux<X> processLocked(I input, boolean async, Supplier<Flux<X>> loader) {
		var lockFunction = createLock(input, async);
		var fluxFuture = new SettableListenableFuture<Flux<X>>(false);
		var lockFuture = lockFunction.apply(() -> {
			var flux = loader.get();
			var doneFuture = new SettableListenableFuture<Nada>(false);
			flux = Fluxi.doOnDone(flux, () -> doneFuture.setResult(Nada.get()));
			fluxFuture.setResult(flux);
			return doneFuture;
		});
		lockFuture.listener(() -> fluxFuture.cancel(true));
		try {
			return fluxFuture.get();
		} catch (Throwable t) {
			lockFuture.cancel(true);
			throw Throws.unchecked(t);
		}
	}

	protected String generateKeyEventBus(I input) {
		return generateKey(input, "event");
	}

	private <V> Function<ThrowingSupplier<? extends Future<? extends V>, ?>, ListenableFuture<V>> createLock(I input,
			boolean async) {
		var lockKey = generateKey(input, "lock", async);
		var executor = async ? this.options.getAsyncExecutor() : this.options.getSyncExecutor();
		return createLock(lockKey, executor, this.options.getLockDuration());
	}

	private <V> BiFunction<ThrowingSupplier<? extends Future<? extends V>, ?>, ThrowingSupplier<? extends V, ?>, ListenableFuture<V>> createTryLock(
			I input, boolean async) {
		var lockKey = generateKey(input, "lock", async);
		var executor = async ? this.options.getAsyncExecutor() : this.options.getSyncExecutor();
		return createTryLock(lockKey, executor, this.options.getLockDuration());
	}

	@SuppressWarnings("rawtypes")
	private boolean shouldForceLoad(I input, Flux<X> result, boolean async) {
		if (result == null || !MachineConfig.isDeveloper())
			return false;
		List<Class> classTypes;
		if (!async)
			classTypes = Configs.get(LoadiConfig.class).forceSyncLoadClassTypesDev();
		else
			classTypes = Configs.get(LoadiConfig.class).forceAsyncLoadClassTypesDev();
		if (classTypes == null || !classTypes.contains(this.getClass()))
			return false;
		logger.info("forcing reload in developer mode. loadiType:{} async:{} input:{}", this.getClass(), async, input);
		return true;
	}

	protected StreamEx<Object> generateKeyParts(I input, Object additionalPart, Object... additionalParts) {
		var stream = Utils.Lots.stream(additionalPart).append(Utils.Lots.stream(additionalParts));
		stream = stream.prepend(VERSION, this.getClass(),
				input == null ? null : this.options.getKeyFunction().apply(input));
		stream = stream.append(Utils.Lots.stream(this.additionalKeyParts));
		stream = stream.nonNull();
		return stream;
	}

	protected String generateKey(I input, Object additionalPart, Object... additionalParts) {
		return KeyGenerator.apply(generateKeyParts(input, additionalPart, additionalParts).toArray());
	}

	protected abstract @Nullable Flux<X> load(@Nonnull I input, @Nonnull Duration ttl);

	protected abstract @Nonnull Flux<X> loadFresh(@Nonnull I input, @Nonnull Consumer<X> loadListener);

	protected abstract <V> Function<ThrowingSupplier<? extends Future<? extends V>, ?>, ListenableFuture<V>> createLock(
			String lockKey, SubmitterExecutor executor, Duration lockDuration);

	protected abstract <V> BiFunction<ThrowingSupplier<? extends Future<? extends V>, ?>, ThrowingSupplier<? extends V, ?>, ListenableFuture<V>> createTryLock(
			String lockKey, SubmitterExecutor executor, Duration lockDuration);

	protected static class EventValue<X> {

		public final String machineId = MACHINE_ID;
		public final String eventBusKey;
		private final List<StatValue<X>> values;

		public EventValue(String eventBusKey, Iterable<X> values) {
			super();
			this.eventBusKey = eventBusKey;
			this.values = Utils.Lots.stream(values).map(StatValue::build).toList();
		}

		public StreamEx<StatValue<X>> streamValues() {
			return Utils.Lots.stream(values).nonNull();
		}
	}
}
