package com.lfp.joe.loadi.config;

import java.util.List;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.core.properties.converter.ClassConverter;

@SuppressWarnings("rawtypes")
public interface LoadiConfig extends Config {

	@ConverterClass(ClassConverter.class)
	List<Class> forceSyncLoadClassTypesDev();

	@ConverterClass(ClassConverter.class)
	List<Class> forceAsyncLoadClassTypesDev();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.properties());
		Configs.printProperties(PrintOptions.json());
	}

}
