package com.lfp.joe.loadi;

import java.time.Duration;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import org.joda.beans.Bean;
import org.joda.beans.BeanDefinition;
import org.joda.beans.ImmutableBean;
import org.joda.beans.ImmutableDefaults;
import org.joda.beans.JodaBeanUtils;
import org.joda.beans.MetaProperty;
import org.joda.beans.Property;
import org.joda.beans.PropertyDefinition;
import org.joda.beans.impl.direct.DirectFieldsBeanBuilder;
import org.joda.beans.impl.direct.DirectMetaBean;
import org.joda.beans.impl.direct.DirectMetaProperty;
import org.joda.beans.impl.direct.DirectMetaPropertyMap;
import org.threadly.concurrent.SubmitterExecutor;

import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.reactor.Scheduli;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import reactor.core.scheduler.Scheduler;

@BeanDefinition
public class LoadiOptions<I> implements ImmutableBean {

	@PropertyDefinition(validate = "notNull")
	private final Function<I, Object> keyFunction;

	@PropertyDefinition(validate = "notNull")
	private final Duration lockDuration;

	@PropertyDefinition(validate = "notNull")
	private final SubmitterExecutor syncExecutor;

	@PropertyDefinition(get = "manual")
	private final Function<I, Duration> syncTTLFunction;

	@PropertyDefinition(validate = "notNull")
	private final SubmitterExecutor asyncExecutor;

	@PropertyDefinition(validate = "notNull", get = "manual")
	private final Function<I, Duration> asyncTTLFunction;

	@PropertyDefinition
	private final int bufferSize;

	@PropertyDefinition
	private final Duration bufferTimeout;

	// -----------------------------------------------------------------------
	/**
	 * Gets the syncTTLFunction.
	 * 
	 * @return the value of the property, not null
	 */
	public Function<I, Duration> getSyncTTLFunction() {
		return getTTLFunction(syncTTLFunction);
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the asyncTTLFunction.
	 * 
	 * @return the value of the property, not null
	 */
	public Function<I, Duration> getAsyncTTLFunction() {
		return getTTLFunction(asyncTTLFunction);
	}

	private Function<I, Duration> getTTLFunction(Function<I, Duration> ttlFunction) {
		return input -> {
			var ttl = ttlFunction == null ? null : ttlFunction.apply(input);
			return Optional.ofNullable(ttl).orElse(Duration.ofMillis(Long.MAX_VALUE));
		};
	}

	private transient Scheduler syncScheduler;

	public Scheduler getSyncScheduler() {
		if (syncScheduler == null)
			synchronized (this) {
				if (syncScheduler == null)
					syncScheduler = Scheduli.createScheduler(getSyncExecutor());
			}
		return syncScheduler;
	}

	private transient Scheduler asyncScheduler;

	public Scheduler getAsyncScheduler() {
		if (asyncScheduler == null)
			synchronized (this) {
				if (asyncScheduler == null)
					asyncScheduler = Scheduli.createScheduler(getAsyncExecutor());
			}
		return asyncScheduler;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@ImmutableDefaults
	private static void applyDefaults(Builder builder) {
		builder.lockDuration(Duration.ofSeconds(15));
		builder.syncExecutor(Threads.Pools.centralPool());
		builder.asyncExecutor(Threads.Pools.centralPool());
		builder.keyFunction(Utils.Crypto::hashMD5);
		builder.bufferSize(1);
		builder.bufferTimeout(null);
	}

	public static void main(String[] args) {
		JodaBeans.updateCode();
	}

	// ------------------------- AUTOGENERATED START -------------------------
	/// CLOVER:OFF
	/**
	 * The meta-bean for {@code LoadiOptions}.
	 * 
	 * @return the meta-bean, not null
	 */
	@SuppressWarnings("rawtypes")
	public static LoadiOptions.Meta meta() {
		return LoadiOptions.Meta.INSTANCE;
	}

	/**
	 * The meta-bean for {@code LoadiOptions}.
	 * 
	 * @param <R> the bean's generic type
	 * @param cls the bean's generic type
	 * @return the meta-bean, not null
	 */
	@SuppressWarnings("unchecked")
	public static <R> LoadiOptions.Meta<R> metaLoadiOptions(Class<R> cls) {
		return LoadiOptions.Meta.INSTANCE;
	}

	static {
		JodaBeanUtils.registerMetaBean(LoadiOptions.Meta.INSTANCE);
	}

	/**
	 * Returns a builder used to create an instance of the bean.
	 * 
	 * @param <I> the type
	 * @return the builder, not null
	 */
	public static <I> LoadiOptions.Builder<I> builder() {
		return new LoadiOptions.Builder<I>();
	}

	/**
	 * Restricted constructor.
	 * 
	 * @param builder the builder to copy from, not null
	 */
	protected LoadiOptions(LoadiOptions.Builder<I> builder) {
		JodaBeanUtils.notNull(builder.keyFunction, "keyFunction");
		JodaBeanUtils.notNull(builder.lockDuration, "lockDuration");
		JodaBeanUtils.notNull(builder.syncExecutor, "syncExecutor");
		JodaBeanUtils.notNull(builder.asyncExecutor, "asyncExecutor");
		JodaBeanUtils.notNull(builder.asyncTTLFunction, "asyncTTLFunction");
		this.keyFunction = builder.keyFunction;
		this.lockDuration = builder.lockDuration;
		this.syncExecutor = builder.syncExecutor;
		this.syncTTLFunction = builder.syncTTLFunction;
		this.asyncExecutor = builder.asyncExecutor;
		this.asyncTTLFunction = builder.asyncTTLFunction;
		this.bufferSize = builder.bufferSize;
		this.bufferTimeout = builder.bufferTimeout;
	}

	@SuppressWarnings("unchecked")
	@Override
	public LoadiOptions.Meta<I> metaBean() {
		return LoadiOptions.Meta.INSTANCE;
	}

	@Override
	public <R> Property<R> property(String propertyName) {
		return metaBean().<R>metaProperty(propertyName).createProperty(this);
	}

	@Override
	public Set<String> propertyNames() {
		return metaBean().metaPropertyMap().keySet();
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the keyFunction.
	 * 
	 * @return the value of the property, not null
	 */
	public Function<I, Object> getKeyFunction() {
		return keyFunction;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the lockDuration.
	 * 
	 * @return the value of the property, not null
	 */
	public Duration getLockDuration() {
		return lockDuration;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the syncExecutor.
	 * 
	 * @return the value of the property, not null
	 */
	public SubmitterExecutor getSyncExecutor() {
		return syncExecutor;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the asyncExecutor.
	 * 
	 * @return the value of the property, not null
	 */
	public SubmitterExecutor getAsyncExecutor() {
		return asyncExecutor;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the bufferSize.
	 * 
	 * @return the value of the property
	 */
	public int getBufferSize() {
		return bufferSize;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the bufferTimeout.
	 * 
	 * @return the value of the property
	 */
	public Duration getBufferTimeout() {
		return bufferTimeout;
	}

	// -----------------------------------------------------------------------
	/**
	 * Returns a builder that allows this bean to be mutated.
	 * 
	 * @return the mutable builder, not null
	 */
	public Builder<I> toBuilder() {
		return new Builder<I>(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj != null && obj.getClass() == this.getClass()) {
			LoadiOptions<?> other = (LoadiOptions<?>) obj;
			return JodaBeanUtils.equal(keyFunction, other.keyFunction)
					&& JodaBeanUtils.equal(lockDuration, other.lockDuration)
					&& JodaBeanUtils.equal(syncExecutor, other.syncExecutor)
					&& JodaBeanUtils.equal(syncTTLFunction, other.syncTTLFunction)
					&& JodaBeanUtils.equal(asyncExecutor, other.asyncExecutor)
					&& JodaBeanUtils.equal(asyncTTLFunction, other.asyncTTLFunction) && (bufferSize == other.bufferSize)
					&& JodaBeanUtils.equal(bufferTimeout, other.bufferTimeout);
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = getClass().hashCode();
		hash = hash * 31 + JodaBeanUtils.hashCode(keyFunction);
		hash = hash * 31 + JodaBeanUtils.hashCode(lockDuration);
		hash = hash * 31 + JodaBeanUtils.hashCode(syncExecutor);
		hash = hash * 31 + JodaBeanUtils.hashCode(syncTTLFunction);
		hash = hash * 31 + JodaBeanUtils.hashCode(asyncExecutor);
		hash = hash * 31 + JodaBeanUtils.hashCode(asyncTTLFunction);
		hash = hash * 31 + JodaBeanUtils.hashCode(bufferSize);
		hash = hash * 31 + JodaBeanUtils.hashCode(bufferTimeout);
		return hash;
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder(288);
		buf.append("LoadiOptions{");
		int len = buf.length();
		toString(buf);
		if (buf.length() > len) {
			buf.setLength(buf.length() - 2);
		}
		buf.append('}');
		return buf.toString();
	}

	protected void toString(StringBuilder buf) {
		buf.append("keyFunction").append('=').append(JodaBeanUtils.toString(keyFunction)).append(',').append(' ');
		buf.append("lockDuration").append('=').append(JodaBeanUtils.toString(lockDuration)).append(',').append(' ');
		buf.append("syncExecutor").append('=').append(JodaBeanUtils.toString(syncExecutor)).append(',').append(' ');
		buf.append("syncTTLFunction").append('=').append(JodaBeanUtils.toString(syncTTLFunction)).append(',')
				.append(' ');
		buf.append("asyncExecutor").append('=').append(JodaBeanUtils.toString(asyncExecutor)).append(',').append(' ');
		buf.append("asyncTTLFunction").append('=').append(JodaBeanUtils.toString(asyncTTLFunction)).append(',')
				.append(' ');
		buf.append("bufferSize").append('=').append(JodaBeanUtils.toString(bufferSize)).append(',').append(' ');
		buf.append("bufferTimeout").append('=').append(JodaBeanUtils.toString(bufferTimeout)).append(',').append(' ');
	}

	// -----------------------------------------------------------------------
	/**
	 * The meta-bean for {@code LoadiOptions}.
	 * 
	 * @param <I> the type
	 */
	public static class Meta<I> extends DirectMetaBean {
		/**
		 * The singleton instance of the meta-bean.
		 */
		@SuppressWarnings("rawtypes")
		static final Meta INSTANCE = new Meta();

		/**
		 * The meta-property for the {@code keyFunction} property.
		 */
		@SuppressWarnings({ "unchecked", "rawtypes" })
		private final MetaProperty<Function<I, Object>> keyFunction = DirectMetaProperty.ofImmutable(this,
				"keyFunction", LoadiOptions.class, (Class) Function.class);
		/**
		 * The meta-property for the {@code lockDuration} property.
		 */
		private final MetaProperty<Duration> lockDuration = DirectMetaProperty.ofImmutable(this, "lockDuration",
				LoadiOptions.class, Duration.class);
		/**
		 * The meta-property for the {@code syncExecutor} property.
		 */
		private final MetaProperty<SubmitterExecutor> syncExecutor = DirectMetaProperty.ofImmutable(this,
				"syncExecutor", LoadiOptions.class, SubmitterExecutor.class);
		/**
		 * The meta-property for the {@code syncTTLFunction} property.
		 */
		@SuppressWarnings({ "unchecked", "rawtypes" })
		private final MetaProperty<Function<I, Duration>> syncTTLFunction = DirectMetaProperty.ofImmutable(this,
				"syncTTLFunction", LoadiOptions.class, (Class) Function.class);
		/**
		 * The meta-property for the {@code asyncExecutor} property.
		 */
		private final MetaProperty<SubmitterExecutor> asyncExecutor = DirectMetaProperty.ofImmutable(this,
				"asyncExecutor", LoadiOptions.class, SubmitterExecutor.class);
		/**
		 * The meta-property for the {@code asyncTTLFunction} property.
		 */
		@SuppressWarnings({ "unchecked", "rawtypes" })
		private final MetaProperty<Function<I, Duration>> asyncTTLFunction = DirectMetaProperty.ofImmutable(this,
				"asyncTTLFunction", LoadiOptions.class, (Class) Function.class);
		/**
		 * The meta-property for the {@code bufferSize} property.
		 */
		private final MetaProperty<Integer> bufferSize = DirectMetaProperty.ofImmutable(this, "bufferSize",
				LoadiOptions.class, Integer.TYPE);
		/**
		 * The meta-property for the {@code bufferTimeout} property.
		 */
		private final MetaProperty<Duration> bufferTimeout = DirectMetaProperty.ofImmutable(this, "bufferTimeout",
				LoadiOptions.class, Duration.class);
		/**
		 * The meta-properties.
		 */
		private final Map<String, MetaProperty<?>> metaPropertyMap$ = new DirectMetaPropertyMap(this, null,
				"keyFunction", "lockDuration", "syncExecutor", "syncTTLFunction", "asyncExecutor", "asyncTTLFunction",
				"bufferSize", "bufferTimeout");

		/**
		 * Restricted constructor.
		 */
		protected Meta() {
		}

		@Override
		protected MetaProperty<?> metaPropertyGet(String propertyName) {
			switch (propertyName.hashCode()) {
			case 118662999: // keyFunction
				return keyFunction;
			case -868541473: // lockDuration
				return lockDuration;
			case -1601459058: // syncExecutor
				return syncExecutor;
			case -217884599: // syncTTLFunction
				return syncTTLFunction;
			case 3220335: // asyncExecutor
				return asyncExecutor;
			case 1799907784: // asyncTTLFunction
				return asyncTTLFunction;
			case 1906231393: // bufferSize
				return bufferSize;
			case 1357449473: // bufferTimeout
				return bufferTimeout;
			}
			return super.metaPropertyGet(propertyName);
		}

		@Override
		public LoadiOptions.Builder<I> builder() {
			return new LoadiOptions.Builder<I>();
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		@Override
		public Class<? extends LoadiOptions<I>> beanType() {
			return (Class) LoadiOptions.class;
		}

		@Override
		public Map<String, MetaProperty<?>> metaPropertyMap() {
			return metaPropertyMap$;
		}

		// -----------------------------------------------------------------------
		/**
		 * The meta-property for the {@code keyFunction} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Function<I, Object>> keyFunction() {
			return keyFunction;
		}

		/**
		 * The meta-property for the {@code lockDuration} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Duration> lockDuration() {
			return lockDuration;
		}

		/**
		 * The meta-property for the {@code syncExecutor} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<SubmitterExecutor> syncExecutor() {
			return syncExecutor;
		}

		/**
		 * The meta-property for the {@code syncTTLFunction} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Function<I, Duration>> syncTTLFunction() {
			return syncTTLFunction;
		}

		/**
		 * The meta-property for the {@code asyncExecutor} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<SubmitterExecutor> asyncExecutor() {
			return asyncExecutor;
		}

		/**
		 * The meta-property for the {@code asyncTTLFunction} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Function<I, Duration>> asyncTTLFunction() {
			return asyncTTLFunction;
		}

		/**
		 * The meta-property for the {@code bufferSize} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Integer> bufferSize() {
			return bufferSize;
		}

		/**
		 * The meta-property for the {@code bufferTimeout} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Duration> bufferTimeout() {
			return bufferTimeout;
		}

		// -----------------------------------------------------------------------
		@Override
		protected Object propertyGet(Bean bean, String propertyName, boolean quiet) {
			switch (propertyName.hashCode()) {
			case 118662999: // keyFunction
				return ((LoadiOptions<?>) bean).getKeyFunction();
			case -868541473: // lockDuration
				return ((LoadiOptions<?>) bean).getLockDuration();
			case -1601459058: // syncExecutor
				return ((LoadiOptions<?>) bean).getSyncExecutor();
			case -217884599: // syncTTLFunction
				return ((LoadiOptions<?>) bean).getSyncTTLFunction();
			case 3220335: // asyncExecutor
				return ((LoadiOptions<?>) bean).getAsyncExecutor();
			case 1799907784: // asyncTTLFunction
				return ((LoadiOptions<?>) bean).getAsyncTTLFunction();
			case 1906231393: // bufferSize
				return ((LoadiOptions<?>) bean).getBufferSize();
			case 1357449473: // bufferTimeout
				return ((LoadiOptions<?>) bean).getBufferTimeout();
			}
			return super.propertyGet(bean, propertyName, quiet);
		}

		@Override
		protected void propertySet(Bean bean, String propertyName, Object newValue, boolean quiet) {
			metaProperty(propertyName);
			if (quiet) {
				return;
			}
			throw new UnsupportedOperationException("Property cannot be written: " + propertyName);
		}

	}

	// -----------------------------------------------------------------------
	/**
	 * The bean-builder for {@code LoadiOptions}.
	 * 
	 * @param <I> the type
	 */
	public static class Builder<I> extends DirectFieldsBeanBuilder<LoadiOptions<I>> {

		private Function<I, Object> keyFunction;
		private Duration lockDuration;
		private SubmitterExecutor syncExecutor;
		private Function<I, Duration> syncTTLFunction;
		private SubmitterExecutor asyncExecutor;
		private Function<I, Duration> asyncTTLFunction;
		private int bufferSize;
		private Duration bufferTimeout;

		/**
		 * Restricted constructor.
		 */
		protected Builder() {
			applyDefaults(this);
		}

		/**
		 * Restricted copy constructor.
		 * 
		 * @param beanToCopy the bean to copy from, not null
		 */
		protected Builder(LoadiOptions<I> beanToCopy) {
			this.keyFunction = beanToCopy.getKeyFunction();
			this.lockDuration = beanToCopy.getLockDuration();
			this.syncExecutor = beanToCopy.getSyncExecutor();
			this.syncTTLFunction = beanToCopy.getSyncTTLFunction();
			this.asyncExecutor = beanToCopy.getAsyncExecutor();
			this.asyncTTLFunction = beanToCopy.getAsyncTTLFunction();
			this.bufferSize = beanToCopy.getBufferSize();
			this.bufferTimeout = beanToCopy.getBufferTimeout();
		}

		// -----------------------------------------------------------------------
		@Override
		public Object get(String propertyName) {
			switch (propertyName.hashCode()) {
			case 118662999: // keyFunction
				return keyFunction;
			case -868541473: // lockDuration
				return lockDuration;
			case -1601459058: // syncExecutor
				return syncExecutor;
			case -217884599: // syncTTLFunction
				return syncTTLFunction;
			case 3220335: // asyncExecutor
				return asyncExecutor;
			case 1799907784: // asyncTTLFunction
				return asyncTTLFunction;
			case 1906231393: // bufferSize
				return bufferSize;
			case 1357449473: // bufferTimeout
				return bufferTimeout;
			default:
				throw new NoSuchElementException("Unknown property: " + propertyName);
			}
		}

		@SuppressWarnings("unchecked")
		@Override
		public Builder<I> set(String propertyName, Object newValue) {
			switch (propertyName.hashCode()) {
			case 118662999: // keyFunction
				this.keyFunction = (Function<I, Object>) newValue;
				break;
			case -868541473: // lockDuration
				this.lockDuration = (Duration) newValue;
				break;
			case -1601459058: // syncExecutor
				this.syncExecutor = (SubmitterExecutor) newValue;
				break;
			case -217884599: // syncTTLFunction
				this.syncTTLFunction = (Function<I, Duration>) newValue;
				break;
			case 3220335: // asyncExecutor
				this.asyncExecutor = (SubmitterExecutor) newValue;
				break;
			case 1799907784: // asyncTTLFunction
				this.asyncTTLFunction = (Function<I, Duration>) newValue;
				break;
			case 1906231393: // bufferSize
				this.bufferSize = (Integer) newValue;
				break;
			case 1357449473: // bufferTimeout
				this.bufferTimeout = (Duration) newValue;
				break;
			default:
				throw new NoSuchElementException("Unknown property: " + propertyName);
			}
			return this;
		}

		@Override
		public Builder<I> set(MetaProperty<?> property, Object value) {
			super.set(property, value);
			return this;
		}

		/**
		 * @deprecated Use Joda-Convert in application code
		 */
		@Override
		@Deprecated
		public Builder<I> setString(String propertyName, String value) {
			setString(meta().metaProperty(propertyName), value);
			return this;
		}

		/**
		 * @deprecated Use Joda-Convert in application code
		 */
		@Override
		@Deprecated
		public Builder<I> setString(MetaProperty<?> property, String value) {
			super.setString(property, value);
			return this;
		}

		/**
		 * @deprecated Loop in application code
		 */
		@Override
		@Deprecated
		public Builder<I> setAll(Map<String, ? extends Object> propertyValueMap) {
			super.setAll(propertyValueMap);
			return this;
		}

		@Override
		public LoadiOptions<I> build() {
			return new LoadiOptions<I>(this);
		}

		// -----------------------------------------------------------------------
		/**
		 * Sets the keyFunction.
		 * 
		 * @param keyFunction the new value, not null
		 * @return this, for chaining, not null
		 */
		public Builder<I> keyFunction(Function<I, Object> keyFunction) {
			JodaBeanUtils.notNull(keyFunction, "keyFunction");
			this.keyFunction = keyFunction;
			return this;
		}

		/**
		 * Sets the lockDuration.
		 * 
		 * @param lockDuration the new value, not null
		 * @return this, for chaining, not null
		 */
		public Builder<I> lockDuration(Duration lockDuration) {
			JodaBeanUtils.notNull(lockDuration, "lockDuration");
			this.lockDuration = lockDuration;
			return this;
		}

		/**
		 * Sets the syncExecutor.
		 * 
		 * @param syncExecutor the new value, not null
		 * @return this, for chaining, not null
		 */
		public Builder<I> syncExecutor(SubmitterExecutor syncExecutor) {
			JodaBeanUtils.notNull(syncExecutor, "syncExecutor");
			this.syncExecutor = syncExecutor;
			return this;
		}

		/**
		 * Sets the syncTTLFunction.
		 * 
		 * @param syncTTLFunction the new value
		 * @return this, for chaining, not null
		 */
		public Builder<I> syncTTLFunction(Function<I, Duration> syncTTLFunction) {
			this.syncTTLFunction = syncTTLFunction;
			return this;
		}

		/**
		 * Sets the asyncExecutor.
		 * 
		 * @param asyncExecutor the new value, not null
		 * @return this, for chaining, not null
		 */
		public Builder<I> asyncExecutor(SubmitterExecutor asyncExecutor) {
			JodaBeanUtils.notNull(asyncExecutor, "asyncExecutor");
			this.asyncExecutor = asyncExecutor;
			return this;
		}

		/**
		 * Sets the asyncTTLFunction.
		 * 
		 * @param asyncTTLFunction the new value, not null
		 * @return this, for chaining, not null
		 */
		public Builder<I> asyncTTLFunction(Function<I, Duration> asyncTTLFunction) {
			JodaBeanUtils.notNull(asyncTTLFunction, "asyncTTLFunction");
			this.asyncTTLFunction = asyncTTLFunction;
			return this;
		}

		/**
		 * Sets the bufferSize.
		 * 
		 * @param bufferSize the new value
		 * @return this, for chaining, not null
		 */
		public Builder<I> bufferSize(int bufferSize) {
			this.bufferSize = bufferSize;
			return this;
		}

		/**
		 * Sets the bufferTimeout.
		 * 
		 * @param bufferTimeout the new value
		 * @return this, for chaining, not null
		 */
		public Builder<I> bufferTimeout(Duration bufferTimeout) {
			this.bufferTimeout = bufferTimeout;
			return this;
		}

		// -----------------------------------------------------------------------
		@Override
		public String toString() {
			StringBuilder buf = new StringBuilder(288);
			buf.append("LoadiOptions.Builder{");
			int len = buf.length();
			toString(buf);
			if (buf.length() > len) {
				buf.setLength(buf.length() - 2);
			}
			buf.append('}');
			return buf.toString();
		}

		protected void toString(StringBuilder buf) {
			buf.append("keyFunction").append('=').append(JodaBeanUtils.toString(keyFunction)).append(',').append(' ');
			buf.append("lockDuration").append('=').append(JodaBeanUtils.toString(lockDuration)).append(',').append(' ');
			buf.append("syncExecutor").append('=').append(JodaBeanUtils.toString(syncExecutor)).append(',').append(' ');
			buf.append("syncTTLFunction").append('=').append(JodaBeanUtils.toString(syncTTLFunction)).append(',')
					.append(' ');
			buf.append("asyncExecutor").append('=').append(JodaBeanUtils.toString(asyncExecutor)).append(',')
					.append(' ');
			buf.append("asyncTTLFunction").append('=').append(JodaBeanUtils.toString(asyncTTLFunction)).append(',')
					.append(' ');
			buf.append("bufferSize").append('=').append(JodaBeanUtils.toString(bufferSize)).append(',').append(' ');
			buf.append("bufferTimeout").append('=').append(JodaBeanUtils.toString(bufferTimeout)).append(',')
					.append(' ');
		}

	}

	/// CLOVER:ON
	// -------------------------- AUTOGENERATED END --------------------------
}
