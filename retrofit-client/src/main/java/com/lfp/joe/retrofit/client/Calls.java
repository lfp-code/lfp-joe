package com.lfp.joe.retrofit.client;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import org.apache.commons.lang3.Validate;

import com.github.throwable.beanref.BeanPath;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.net.media.MediaTypes;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import de.xn__ho_hia.storage_unit.StorageUnit;
import de.xn__ho_hia.storage_unit.StorageUnits;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

public class Calls {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static <S, X, C extends Call<X>> X consume(BeanPath<S, C> beanPath) {
		Objects.requireNonNull(beanPath);
		return consume(RFit.Clients.get(beanPath.getBeanClass()), beanPath::get);
	}

	public static <S, X, C extends Call<X>> X consume(S service, Function<S, C> callFunction) {
		Objects.requireNonNull(service);
		Objects.requireNonNull(callFunction);
		var call = callFunction.apply(service);
		return consume(call);
	}

	public static <X> X consume(Call<X> call) {
		return Utils.Functions.unchecked(() -> consumeThrowing(call));
	}

	public static <X> X consumeThrowing(Call<X> call) throws IOException {
		Objects.requireNonNull(call);
		Response<X> response = null;
		try {
			response = call.execute();
			validateSuccess(response);
			return response.body();
		} catch (Throwable t) {
			var errorId = logErrorDebug(call, response, t);
			var errorMessage = String.format("errorId=%s", errorId);
			if (Utils.Strings.isNotBlank(t.getMessage()))
				errorMessage += " - " + t.getMessage();
			Utils.Exceptions.setMessage(t, errorMessage);
			throw t;
		}

	}

	public static Optional<String> tryReadBodyAsString(Response<?> response, StorageUnit<?> limit) {
		Objects.requireNonNull(response);
		{// standard body
			var body = response.body();
			if (body != null) {
				try {
					return Optional.of(Serials.Gsons.get().toJson(body));
				} catch (Exception e) {
					return Optional.of(body.toString());
				}
			}
		}
		var charset = com.lfp.joe.okhttp.Ok.Calls.tryGetCharset(response.headers()).orElse(null);
		return com.lfp.joe.okhttp.Ok.Calls.tryReadBodyAsString(response.errorBody(), charset, limit);
	}

	public static void validateSuccess(Response<?> response) throws IOException {
		Objects.requireNonNull(response);
		if (response.isSuccessful())
			return;
		if (response.code() == StatusCodes.NOT_FOUND)
			return;
		boolean includeHeaders;
		StorageUnit<?> bodyReadAmount;
		if (MachineConfig.isDeveloper()) {
			includeHeaders = true;
			bodyReadAmount = StorageUnits.kilobyte(256);
		} else {
			includeHeaders = false;
			bodyReadAmount = null;
		}
		var summary = getSummary(response, includeHeaders, bodyReadAmount);
		var summaryStr = Utils.Lots.stream(summary).nonNullValues().mapValues(Objects::toString)
				.filterValues(Utils.Strings::isNotBlank)
				.map(ent -> String.format("%s:%s", ent.getKey(), ent.getValue())).joining(" ");
		throw new IOException("call error. " + summaryStr);
	}

	public static Request getRequest(Response<?> response) {
		Objects.requireNonNull(response);
		var rawResponse = response.raw();
		return rawResponse.request();
	}

	public static Map<String, Object> getSummary(Response<?> response) {
		return getSummary(response, false, null);
	}

	public static Map<String, Object> getSummary(Response<?> response, boolean includeHeaders,
			StorageUnit<?> bodyReadAmount) {
		Objects.requireNonNull(response);
		// do not read body here, at it yourself
		var summary = com.lfp.joe.okhttp.Ok.Calls.getSummary(response.raw(), includeHeaders, null);
		if (bodyReadAmount != null)
			tryReadBodyAsString(response, bodyReadAmount).ifPresent(v -> summary.put("resBody", v));
		return summary;
	}

	public static MultipartBody.Part createFilePart(String partName, File file) {
		return createFilePart(partName, file, null);
	}

	public static MultipartBody.Part createFilePart(String partName, File file, String fileName) {
		Validate.isTrue(file.exists(), "file does not exist:%s", fileName);
		if (Utils.Strings.isBlank(fileName))
			fileName = file.getName();
		if (Utils.Strings.isBlank(partName))
			partName = "files";
		var mediaType = MediaTypes.tryParse(file).orElse(com.google.common.net.MediaType.APPLICATION_BINARY);
		RequestBody requestFile = RequestBody.create(MediaType.parse(mediaType.toString()), file);
		return MultipartBody.Part.createFormData(partName, fileName, requestFile);
	}

	private static <X> String logErrorDebug(Call<X> call, Response<X> response, Throwable t) {
		String errorId = Utils.Crypto.getRandomString();
		var summary = Serials.Gsons.get().toJson(getSummary(call, response));
		LOGGER.debug("{} - {}", errorId, summary);
		return errorId;
	}

	private static Map<String, Object> getSummary(Call<?> call, Response<?> response) {
		var data = new LinkedHashMap<String, Object>();
		data.put("call", getSummary(call));
		data.put("response", getSummary(Optional.ofNullable(response).map(Response::raw).orElse(null)));
		return data;
	}

	private static Map<String, Object> getSummary(Call<?> call) {
		if (call == null)
			return Map.of();
		var data = new LinkedHashMap<String, Object>();
		data.put(Call.class.getName(), call.toString());
		data.put("canceled", call.isCanceled());
		data.put("executed", call.isExecuted());
		data.put("request", getSummary(call.request()));
		return data;
	}

	private static Map<String, Object> getSummary(Request request) {
		if (request == null)
			return Map.of();
		var data = new LinkedHashMap<String, Object>();
		data.put(Request.class.getName(), request.toString());
		data.put("url", request.url());
		data.put("headers", Ok.Calls.headerMap(request.headers()));
		return data;
	}

	private static Map<String, Object> getSummary(okhttp3.Response response) {
		if (response == null)
			return Map.of();
		var data = new LinkedHashMap<String, Object>();
		data.put(okhttp3.Response.class.getName(), response.toString());
		data.put("code", response.code());
		data.put("message", response.message());
		data.put("headers", Ok.Calls.headerMap(response.headers()));
		data.put("request", getSummary(response.request()));
		data.put("prior-response", getSummary(response.priorResponse()));
		return data;
	}

}
