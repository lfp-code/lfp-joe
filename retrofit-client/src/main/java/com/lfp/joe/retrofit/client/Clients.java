package com.lfp.joe.retrofit.client;

import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.google.common.base.Preconditions;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.net.http.ServiceConfig;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.okhttp.cookie.CookieJars;
import com.lfp.joe.okhttp.interceptor.ImmutableRetryInterceptor.RetryInterceptorFilter;
import com.lfp.joe.okhttp.interceptor.RetryInterceptor;
import com.lfp.joe.retrofit.Services;
import com.lfp.joe.utils.Utils;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import one.util.streamex.StreamEx;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Clients {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String DEFAULT_INSTANCE_CACHE_PREFIX = Utils.Crypto.getSecureRandomString();
	private static final Duration EXPIRE_AFTER_ACCESS = Duration.ofMinutes(5);
	private static final int RETRY_FAILURE = 3;
	private static final int RETRY_UNAUTHORIZED = 3;
	private static final Duration RETRY_DELAY = Duration.ofSeconds(1);
	private static final Cache<String, Object> INSTANCE_CACHE = Caffeine.newBuilder()
			.expireAfterAccess(EXPIRE_AFTER_ACCESS).build();

	public static Retrofit.Builder builder() throws IOException {
		return builder(Services.INSTANCE.client(), null, null, null);
	}

	public static Retrofit.Builder builder(OkHttpClient client, ServiceConfig serviceConfig,
			Function<OkHttpClient.Builder, OkHttpClient.Builder> clientModifier,
			Function<Retrofit.Builder, Retrofit.Builder> frameworkModifier) {
		Preconditions.checkNotNull(client, "client required");
		CookieJar cookieJar;
		{
			CookieJar newCookieJar = CookieJars.create();
			CookieJar existingCookieJar = client.cookieJar();
			cookieJar = existingCookieJar == null ? newCookieJar : new CookieJar() {

				@Override
				public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
					existingCookieJar.saveFromResponse(url, cookies);
					newCookieJar.saveFromResponse(url, cookies);
				}

				@Override
				public List<Cookie> loadForRequest(HttpUrl url) {
					StreamEx<Cookie> stream = Utils.Lots.stream(newCookieJar.loadForRequest(url))
							.append(Utils.Lots.stream(existingCookieJar.loadForRequest(url))).nonNull().distinct();
					return stream.toList();
				}
			};
		}
		OkHttpClient.Builder clientBuilder = client.newBuilder();
		clientBuilder.cookieJar(cookieJar);
		clientBuilder.interceptors().add(0, chain -> {
			var request = chain.request();
			var headers = Optional.ofNullable(serviceConfig).map(ServiceConfig::headers)
					.filter(Predicate.not(Map::isEmpty)).orElse(null);
			if (headers == null)
				return chain.proceed(request);
			{
				var requestBuilder = request.newBuilder();
				Utils.Lots.streamMultimap(headers).forKeyValue(requestBuilder::header);
				request = requestBuilder.build();
			}
			return chain.proceed(request);
		});
		var retryInterceptor = new RetryInterceptor();
		clientBuilder.interceptors().add(0, retryInterceptor);
		retryInterceptor.add(RetryInterceptorFilter.builder().name("failure").maxRetries(RETRY_FAILURE)
				.delay(RETRY_DELAY).retryFunction((request, response, failure) -> {
					return failure != null;
				}).build());
		retryInterceptor.add(RetryInterceptorFilter.builder().name(StatusCodes.UNAUTHORIZED_STRING)
				.maxRetries(RETRY_UNAUTHORIZED).delay(RETRY_DELAY).retryFunction((request, response, failure) -> {
					if (response != null && response.code() == StatusCodes.UNAUTHORIZED)
						return true;
					return false;
				}).build());
		if (clientModifier != null)
			clientBuilder = clientModifier.apply(clientBuilder);
		Retrofit.Builder builder = new Retrofit.Builder()
				.addConverterFactory(GsonConverterFactory.create(Services.INSTANCE.gson()))
				.callFactory(clientBuilder.build()).callbackExecutor(CoreTasks.executor());
		if (serviceConfig != null && serviceConfig.uri() != null)
			builder = builder.baseUrl(serviceConfig.uri().toString());
		if (frameworkModifier != null)
			builder = frameworkModifier.apply(builder);
		return builder;
	}

	public static <X> X build(Class<X> classType) throws IOException {
		Objects.requireNonNull(classType);
		var serviceConfig = ServiceConfig.discover(classType).findFirst().orElse(null);
		return builder(Services.INSTANCE.client(), serviceConfig, null, null).build().create(classType);
	}

	public static <X> X get(Class<X> classType) {
		Objects.requireNonNull(classType);
		var serviceConfig = ServiceConfig.discover(classType).findFirst().orElse(null);
		return get(classType, serviceConfig, DEFAULT_INSTANCE_CACHE_PREFIX + classType.getName());
	}

	public static <X> X get(Class<X> classType, ServiceConfig serviceConfig) {
		return get(classType, serviceConfig, null);
	}

	@SuppressWarnings("unchecked")
	public static <X> X get(Class<X> classType, ServiceConfig serviceConfig, String instanceId) {
		Objects.requireNonNull(classType);
		String cacheKey;
		if (Utils.Strings.isNotBlank(instanceId))
			cacheKey = instanceId;
		else
			cacheKey = Utils.Crypto.hashMD5(classType.getName(), serviceConfig == null ? null : serviceConfig.hash())
					.encodeHex();
		return (X) INSTANCE_CACHE.get(cacheKey, nil -> {
			var builder = builder(Services.INSTANCE.client(serviceConfig.proxyURI()), serviceConfig, null, null);
			return builder.build().create(classType);
		});
	}

	public static void main(String[] args) {
		var failureCount = 0;
		while (true) {
			if (failureCount++ < RETRY_FAILURE)
				System.out.println("running");
			else
				break;

		}
	}

}
