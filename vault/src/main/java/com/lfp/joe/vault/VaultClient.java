package com.lfp.joe.vault;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.reflections8.ReflectionUtils;

import com.bettercloud.vault.Vault;
import com.bettercloud.vault.VaultException;
import com.bettercloud.vault.response.AuthResponse;
import com.bettercloud.vault.response.LogicalResponse;
import com.github.benmanes.caffeine.cache.CacheLoader;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.lfp.joe.cache.caffeine.TTLExpiry;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

import at.favre.lib.bytes.Bytes;

//avoid standard any autowired gsons here
public class VaultClient implements ThrowingSupplier<Vault, VaultException> {

	private static final MemoizedSupplier<Optional<VaultClient>> DEFAULT_INSTANCE_S = Utils.Functions.memoize(() -> {
		VaultConfig config = Configs.get(VaultConfig.class);
		if (!isVaultConfigValid(config))
			return Optional.empty();
		VaultClient client;
		try {
			client = new VaultClient(config.toClientConfig(), config.username(), config.password());
		} catch (VaultException e) {
			throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
					? java.lang.RuntimeException.class.cast(e)
					: new java.lang.RuntimeException(e);
		}
		return Optional.of(client);
	});

	public static VaultClient getDefault() {
		return DEFAULT_INSTANCE_S.get().get();
	}

	public static Optional<VaultClient> tryGetDefault() {
		return DEFAULT_INSTANCE_S.get();
	}

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final Duration GET_AUTH_RESPONSE_FRESH_RATE_LIMIT = Duration.ofSeconds(3);
	@SuppressWarnings("unchecked")
	private static final MemoizedSupplier<Field> vaultConfig_FIELD = Utils.Functions.memoize(() -> {
		Set<Field> fields = ReflectionUtils.getFields(Vault.class, ReflectionUtils.withName("vaultConfig"),
				ReflectionUtils.withTypeAssignableTo(com.bettercloud.vault.VaultConfig.class));
		Validate.isTrue(fields.size() == 1, "can't get vault config field");
		Field field = fields.iterator().next();
		field.setAccessible(true);
		return field;
	});

	private static final LoadingCache<VaultTokenCacheKey, Runnable> GET_AUTH_RESPONSE_FRESH_RATE_LIMIT_CACHE = Caffeine
			.newBuilder().expireAfterAccess(GET_AUTH_RESPONSE_FRESH_RATE_LIMIT.multipliedBy(2)).build(vtck -> {
				AtomicReference<Date> lastAccessRef = new AtomicReference<>();
				return () -> {
					synchronized (lastAccessRef) {
						Date lastAccess = lastAccessRef.getAndSet(new Date());
						if (lastAccess == null)
							return;
						long elapsed = System.currentTimeMillis() - lastAccess.getTime();
						long remaining = GET_AUTH_RESPONSE_FRESH_RATE_LIMIT.toMillis() - elapsed;
						if (remaining <= 0)
							return;
						org.slf4j.LoggerFactory.getLogger(THIS_CLASS).warn(
								"throttling vault connection. millis:{} username:{} address:{}", remaining,
								vtck.getUsername(), vtck.getClientConfig().getAddress());
						try {
							Thread.sleep(remaining);
						} catch (InterruptedException e) {
							// suppress
						}
					}
				};
			});

	private static final LoadingCache<VaultTokenCacheKey, AuthResponseContext> AUTH_RESPONSE_CACHE = Caffeine
			.newBuilder().expireAfter(new TTLExpiry<VaultTokenCacheKey, AuthResponseContext>() {

				@Override
				public Duration timeToLive(boolean write, @NonNull VaultTokenCacheKey key,
						@NonNull AuthResponseContext value, @NonNull Supplier<Duration> noChangeSupplier) {
					if (key == null || value == null)
						return Duration.ZERO;
					if (!value.isValid())
						return Duration.ZERO;
					return value.getTTL();
				}
			}).build(new CacheLoader<VaultTokenCacheKey, AuthResponseContext>() {

				@Override
				public @Nullable AuthResponseContext load(@NonNull VaultTokenCacheKey vtck) throws Exception {
					AuthResponseContext authResponseContext = getAuthResponseFromDisk(vtck);
					if (authResponseContext != null)
						return authResponseContext;
					GET_AUTH_RESPONSE_FRESH_RATE_LIMIT_CACHE.get(vtck).run();
					authResponseContext = new AuthResponseContext(getAuthResponseFresh(vtck));
					try (OutputStream os = vtck.outputStream()) {
						writeJsonToOutputStream(authResponseContext, os);
					} catch (Exception e) {
						throw Utils.Exceptions.asRuntimeException(e);
					}
					return authResponseContext;
				}
			});

	private final com.bettercloud.vault.VaultConfig clientConfig;
	private final String username;
	private final String password;
	private Vault _vault;

	public VaultClient(com.bettercloud.vault.VaultConfig clientConfig, String username, String password) {
		this.clientConfig = Objects.requireNonNull(clientConfig);
		this.username = username;
		this.password = password;
	}

	public AuthResponse getAuthResponse() {
		VaultTokenCacheKey vaultTokenCacheKey = VaultTokenCacheKey.builder().withClientConfig(clientConfig)
				.withUsername(username).withPassword(password).build();
		return AUTH_RESPONSE_CACHE.get(vaultTokenCacheKey).getAuthResponse();
	}

	@Override
	public Vault get() throws VaultException {
		if (!isTokenValid(_vault, getAuthResponse()))
			synchronized (THIS_CLASS) {
				if (!isTokenValid(_vault, getAuthResponse()))
					_vault = new Vault(clientConfig.token(getAuthResponse().getAuthClientToken()).build());
			}
		return _vault;
	}

	public <X> X read(String path, TypeToken<X> typeToken) throws VaultException {
		Objects.requireNonNull(path);
		Objects.requireNonNull(typeToken);
		LogicalResponse response = Objects.requireNonNull(get()).logical().read(path);
		Map<String, String> data;
		if (response.getRestResponse().getStatus() == 404)
			data = null;
		else if (response.getRestResponse().getStatus() / 100 == 2)
			data = response.getData();
		else
			throw new VaultException(Bytes.from(response.getRestResponse().getBody()).encodeUtf8());
		if (data == null)
			return null;
		JsonObject jo = new JsonObject();
		for (String key : data.keySet()) {
			String value = data.get(key);
			JsonElement valueJe = parseJsonTree(value);
			jo.add(key, valueJe);
		}
		return getAdvancedGson().fromJson(jo, typeToken.getType());
	}

	public <X> void write(String path, X data) throws VaultException {
		Objects.requireNonNull(path);
		JsonElement dataJe = data == null ? null : getAdvancedGson().toJsonTree(data);
		Map<String, Object> dataMap = new LinkedHashMap<>();
		if (dataJe != null) {
			Validate.isTrue(dataJe.isJsonObject(), "data did not serialize as jsonObject:{}", dataJe);
			JsonObject jo = dataJe.getAsJsonObject();
			for (String key : jo.keySet()) {
				JsonElement valueJe = jo.get(key);
				if (valueJe == null || valueJe.isJsonNull())
					dataMap.put(key, null);
				else
					dataMap.put(key, valueJe.toString());
			}
		}
		LogicalResponse writeResponse = get().logical().write(path, dataMap);
		if (writeResponse.getRestResponse().getStatus() / 100 == 2)
			return;
		throw new VaultException(Bytes.from(writeResponse.getRestResponse().getBody()).encodeUtf8());

	}

	public static boolean isVaultConfigValid(VaultConfig vaultConfig) {
		if (vaultConfig == null)
			return false;
		if (vaultConfig.address() == null)
			return false;
		if (StringUtils.isBlank(vaultConfig.username()))
			return false;
		if (StringUtils.isBlank(vaultConfig.password()))
			return false;
		return true;
	}

	private static boolean isTokenValid(Vault vault, AuthResponse authResponse) {
		if (vault == null)
			return false;
		com.bettercloud.vault.VaultConfig vc = getVaultConfig(vault);
		if (Utils.Strings.equals(vc.getToken(), authResponse.getAuthClientToken()))
			return true;
		return false;
	}

	private static com.bettercloud.vault.VaultConfig getVaultConfig(Vault vault) {
		try {
			return (com.bettercloud.vault.VaultConfig) vaultConfig_FIELD.get().get(vault);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
					? java.lang.RuntimeException.class.cast(e)
					: new java.lang.RuntimeException(e);
		}
	}

	private static AuthResponse getAuthResponseFresh(VaultTokenCacheKey vaultTokenCacheKey) throws IOException {
		AuthResponse response;
		try {
			Vault vault = new Vault(vaultTokenCacheKey.getClientConfig());
			response = vault.auth().loginByUserPass(vaultTokenCacheKey.getUsername(), vaultTokenCacheKey.getPassword());
		} catch (VaultException e) {
			throw Utils.Exceptions.as(e, IOException.class);
		}
		Validate.isTrue(Utils.Strings.isNotBlank(response.getAuthClientToken()),
				"auth token not found. address:%s username:%s", vaultTokenCacheKey.getClientConfig().getAddress(),
				vaultTokenCacheKey.getUsername());
		return response;
	}

	private static AuthResponseContext getAuthResponseFromDisk(VaultTokenCacheKey vaultTokenCacheKey) {
		var file = vaultTokenCacheKey.getFile();
		if (!file.exists() || file.length() <= 0)
			return null;
		if (file.length() <= 0) {
			file.delete();
			return null;
		}
		AuthResponseContext authResponseContext = null;
		try (InputStream is = vaultTokenCacheKey.inputStream();) {
			AuthResponseContext fromDisk = readJsonFromInputStream(is, AuthResponseContext.class);
			fromDisk.isValid();
			authResponseContext = fromDisk;
		} catch (Exception e) {
			org.slf4j.LoggerFactory.getLogger(THIS_CLASS).warn(
					"unexpected error while reading token from disk. username:{} address:{}",
					vaultTokenCacheKey.getUsername(), vaultTokenCacheKey.getClientConfig().getAddress(), e);
		}
		if (authResponseContext == null || !authResponseContext.isValid()) {
			file.delete();
			return null;
		}
		return authResponseContext;
	}

	// for use with serializing key values
	private static Gson getAdvancedGson() {
		return Serials.Gsons.get();
	}

	// for use with serializing internal data
	private static Gson getStandardGson() {
		return new Gson();
	}

	private static <X> X readJsonFromInputStream(InputStream is, Class<X> classType) throws IOException {
		try (InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);
				JsonReader jsr = new JsonReader(isr);) {
			return getStandardGson().fromJson(jsr, classType);
		}
	}

	private static <X> void writeJsonToOutputStream(X object, OutputStream os) throws IOException {
		try (OutputStreamWriter osw = new OutputStreamWriter(os, StandardCharsets.UTF_8);) {
			// avoid standard any autowired gsons here
			getStandardGson().toJson(object, osw);
			osw.flush();
		}
	}

	@SuppressWarnings("deprecation")
	private static JsonElement parseJsonTree(String value) {
		if (value == null)
			return JsonNull.INSTANCE;
		return new JsonParser().parse(value);
	}

}
