package com.lfp.joe.vault;

import java.net.URI;
import java.util.Objects;

import org.aeonbits.owner.Config;

import com.bettercloud.vault.VaultException;
import com.lfp.joe.core.config.EnvironmentLevel;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.core.properties.annotation.LocalOnly;
import com.lfp.joe.core.properties.converter.URIConverter;
import com.lfp.joe.utils.Utils;

@LocalOnly
public interface VaultConfig extends Config {

	@DefaultValue("5000")
	long pathCacheTTLMillis();

	@DefaultValue("default")
	String propertiesPathDefault();

	@DefaultValue("properties-{{{environmentLevel}}}")
	String propertiesPathTemplate_environmentLevel();

	@ConverterClass(URIConverter.class)
	URI address();

	String username();

	String password();

	String token();

	default String getPropertiesPath(EnvironmentLevel environmentLevel) {
		String str = environmentLevel != null ? environmentLevel.name() : propertiesPathDefault();
		str = str.toLowerCase();
		String path = Utils.Strings.templateApply(propertiesPathTemplate_environmentLevel(), "environmentLevel", str);
		path = path.toLowerCase();
		return path;
	}

	default com.bettercloud.vault.VaultConfig toClientConfig() throws VaultException {
		return new com.bettercloud.vault.VaultConfig().address(Objects.toString(address())).build();
	}

	default VaultClient toClient() throws VaultException {
		com.bettercloud.vault.VaultConfig clientConfig = toClientConfig();
		return new VaultClient(clientConfig, username(), password());
	}

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.builder().withIncludeValues(true).build());
	}

}
