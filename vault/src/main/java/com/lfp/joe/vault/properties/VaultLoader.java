package com.lfp.joe.vault.properties;

import java.lang.reflect.Method;
import java.time.Duration;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.aeonbits.owner.Config;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import com.bettercloud.vault.VaultException;
import com.bettercloud.vault.api.Logical;
import com.bettercloud.vault.response.LogicalResponse;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.github.throwable.beanref.BeanRef;
import com.lfp.joe.core.classpath.Instances.Autowired;
import com.lfp.joe.core.config.EnvironmentLevel;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.annotation.LocalOnly;
import com.lfp.joe.core.properties.loader.Loaders;
import com.lfp.joe.core.properties.loader.MultiLoader;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.vault.VaultClient;
import com.lfp.joe.vault.VaultConfig;

@Autowired
public enum VaultLoader implements MultiLoader.ClassTypeLoader {
	INSTANCE;

	private static final AtomicReference<org.slf4j.Logger> LOGGER_REF = new AtomicReference<>();
	private static final int HTTP_RETRY = 3;
	private static final Duration HTTP_RETRY_DELAY = Duration.ofSeconds(2);
	private static final Semaphore HTTP_THROTTLE = new Semaphore(2);
	private static final Duration ENVIRONMENT_LEVEL_REFRESH_DURATION = Duration.ofSeconds(15);

	private static org.slf4j.Logger getLogger() {
		if (LOGGER_REF.get() == null)
			synchronized (LOGGER_REF) {
				if (LOGGER_REF.get() == null)
					LOGGER_REF.set(org.slf4j.LoggerFactory.getLogger(new Object() {
					}.getClass().getEnclosingClass()));
			}
		return LOGGER_REF.get();
	}

	private LoadingCache<String, Map<String, String>> _pathLookupCache;

	protected LoadingCache<String, Map<String, String>> getPathLookupCache() {
		if (_pathLookupCache == null)
			synchronized (this) {
				if (_pathLookupCache == null) {
					_pathLookupCache = com.github.benmanes.caffeine.cache.Caffeine.newBuilder()
							.expireAfterWrite(Duration.ofMillis(Configs.get(VaultConfig.class).pathCacheTTLMillis()))
							.build(path -> {
								LogicalResponse data = logicalRead(VaultClient.getDefault(), path, (l, p) -> l.read(p));
								Map<String, String> map = data.getData();
								return Collections.unmodifiableMap(map);
							});
				}
			}
		return _pathLookupCache;
	}

	private LoadingCache<Optional<Void>, Optional<EnvironmentLevel>> _environmentLevelCache;

	protected LoadingCache<Optional<Void>, Optional<EnvironmentLevel>> getEnvironmentLevelCache() {
		if (_environmentLevelCache == null)
			synchronized (this) {
				if (_environmentLevelCache == null) {
					_environmentLevelCache = com.github.benmanes.caffeine.cache.Caffeine.newBuilder()
							.expireAfterWrite(ENVIRONMENT_LEVEL_REFRESH_DURATION)
							.build(nil -> Optional.ofNullable(lookupEnvironmentLevelFresh()));
				}
			}
		return _environmentLevelCache;
	}

	private EnvironmentLevel lookupEnvironmentLevelFresh() throws VaultException {
		VaultClient client = VaultClient.tryGetDefault().orElse(null);
		if (client == null)
			return null;
		VaultConfig vaultConfig = Configs.get(VaultConfig.class);
		Iterable<EnvironmentLevel> envIble = () -> EnvironmentLevel.streamValuesDecreasing().iterator();
		List<CompletableFuture<SimpleImmutableEntry<EnvironmentLevel, Integer>>> futures = new ArrayList<>();
		for (EnvironmentLevel envLevel : envIble) {
			Supplier<SimpleImmutableEntry<EnvironmentLevel, Integer>> lookupSupplier = () -> {
				String path = vaultConfig.getPropertiesPath(envLevel);
				LogicalResponse lr;
				try {
					lr = logicalRead(client, path, (l, p) -> l.list(p));
				} catch (VaultException e) {
					throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
							? java.lang.RuntimeException.class.cast(e)
							: new java.lang.RuntimeException(e);
				}
				return new SimpleImmutableEntry<>(envLevel, lr.getRestResponse().getStatus());
			};
			CompletableFuture<SimpleImmutableEntry<EnvironmentLevel, Integer>> future = CompletableFuture
					.supplyAsync(lookupSupplier);
			futures.add(future);
		}
		Stream<EnvironmentLevel> envLevelStream = futures.stream().map(f -> {
			SimpleImmutableEntry<EnvironmentLevel, Integer> ent = unchecked(() -> f.get());
			return ent;
		}).filter(Objects::nonNull).filter(e -> {
			if (e.getValue() / 100 == 2)
				return true; // access with properties
			if (e.getValue() == 404)
				return true; // access with no properties
			return false; // no access
		}).map(e -> e.getKey()).sorted(Comparator.comparing(el -> el.getLevel()));
		EnvironmentLevel highestEnvironmentLevel = envLevelStream.findFirst().orElse(null);
		return highestEnvironmentLevel;
	}

	private String getName(Class<? extends Config> classType) {
		Objects.requireNonNull(classType);
		String normalizedName = StringUtils.substringBeforeLast(classType.getName(),
				JavaCode.Proxies.GENERATED_CLASS_TOKEN);
		return normalizedName;
	}

	@Override
	public void load(String instanceId, Properties result, Class<Config> classType,
			Iterable<Entry<Method, Set<String>>> publicNoArgMethodEntries) {
		if (MachineConfig.class.equals(classType)) {
			for (Entry<Method, Set<String>> ent : publicNoArgMethodEntries) {
				Set<String> variations = ent.getValue();
				if (variations.contains(BeanRef.$(MachineConfig::environmentLevel).getName())) {
					EnvironmentLevel environmentLevel = getEnvironmentLevelCache().get(Optional.empty()).orElse(null);
					if (environmentLevel == null)
						continue;
					boolean loaderPut = Loaders.put(result, variations, environmentLevel.name());
					Validate.isTrue(loaderPut, "could not add property:%s", variations);
					continue;
				}
			}
		}
		if (classType.getAnnotation(LocalOnly.class) != null)
			return;
		VaultConfig vaultConfig = Configs.get(VaultConfig.class);
		if (!VaultClient.isVaultConfigValid(vaultConfig))
			return;
		final Set<String> pathPrefixes;
		{
			Set<String> setPathPrefixes = new LinkedHashSet<>();
			setPathPrefixes.add(vaultConfig.getPropertiesPath(Configs.get(MachineConfig.class).environmentLevel()));
			setPathPrefixes.add(vaultConfig.getPropertiesPath(null));
			setPathPrefixes = setPathPrefixes.stream().map(StringUtils::lowerCase)
					.collect(Collectors.toCollection(() -> new LinkedHashSet<>()));
			pathPrefixes = setPathPrefixes;
		}
		publicNoArgMethodEntries.forEach(ent -> {
			for (String pathPrefix : pathPrefixes) {
				String path = pathPrefix + "/" + getName(classType);
				Map<String, String> map = getPathLookupCache().get(path);
				Loaders.putAll(result, publicNoArgMethodEntries, map);
			}
		});

	}

	private static void unchecked(ThrowingRunnable throwingRunnable) {
		unchecked(() -> {
			throwingRunnable.run();
			return null;
		});

	}

	private static <X> X unchecked(Callable<X> callable) {
		try {
			return callable.call();
		} catch (Exception e) {
			throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
					? java.lang.RuntimeException.class.cast(e)
					: new java.lang.RuntimeException(e);
		}
	}

	private static <X> X logicalRead(VaultClient client, String path, LogicalProcessor<X> logicalProcessor)
			throws VaultException {
		Objects.requireNonNull(logicalProcessor);
		Exception toThrow = null;
		for (int i = 0; i < HTTP_RETRY; i++) {
			if (i > 0) {
				getLogger().warn("error during logical read, retrying. path:{}", path, toThrow);
				unchecked(() -> Thread.sleep(HTTP_RETRY_DELAY.toMillis()));
			}
			unchecked(() -> HTTP_THROTTLE.acquire());
			try {
				return logicalProcessor.apply(client.get().logical(), path);
			} catch (Exception e) {
				toThrow = e;
			} finally {
				HTTP_THROTTLE.release();
			}
		}
		if (toThrow instanceof VaultException)
			throw (VaultException) toThrow;
		throw new VaultException(toThrow);
	}

	private static interface ThrowingRunnable {

		void run() throws Exception;
	}

	private static interface LogicalProcessor<X> {

		X apply(Logical logical, String path) throws VaultException;
	}
}
