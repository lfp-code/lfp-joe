package com.lfp.joe.vault;

import java.time.Duration;
import java.util.Date;
import java.util.Objects;

import com.bettercloud.vault.response.AuthResponse;
import com.google.gson.annotations.JsonAdapter;

public class AuthResponseContext {

	@JsonAdapter(VaultDateSerializer.class)
	private final Date createdAt = new Date();
	private final AuthResponse authResponse;

	public AuthResponseContext(AuthResponse authResponse) {
		this.authResponse = Objects.requireNonNull(authResponse);
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public AuthResponse getAuthResponse() {
		return authResponse;
	}

	public Date getExpiresAt() {
		var authLeaseDuration = Duration.ofSeconds(this.authResponse.getAuthLeaseDuration());
		authLeaseDuration = Duration.ofMillis((long) (authLeaseDuration.toMillis() * .95));
		return new Date(authLeaseDuration.toMillis() + this.getCreatedAt().getTime());
	}

	public Duration getTTL() {
		var ttlMillis = getExpiresAt().getTime() - System.currentTimeMillis();
		ttlMillis = Math.max(ttlMillis, 0);
		return Duration.ofMillis(ttlMillis);
	}

	public boolean isValid() {
		var authClientToken = authResponse.getAuthClientToken();
		if (authClientToken == null || authClientToken.isBlank())
			return false;
		var ttl = getTTL();
		if (ttl.isZero() || ttl.isNegative())
			return false;
		return true;
	}

	public static void main(String[] args) {
		var seconds = 86400;
		var authLeaseDuration = Duration.ofSeconds(seconds);
		authLeaseDuration = Duration.ofMillis((long) (authLeaseDuration.toMillis() * .95));
		System.out.println(authLeaseDuration.toMinutes());
	}
}
