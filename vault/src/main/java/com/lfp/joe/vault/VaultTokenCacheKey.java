package com.lfp.joe.vault;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.AbstractMap.SimpleEntry;
import java.util.Arrays;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Stream;

import javax.annotation.Generated;

import org.encryptor4j.Encryptor;

import com.bettercloud.vault.VaultConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.encryption.Encryptions;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;

public class VaultTokenCacheKey {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	private final MemoizedSupplier<Entry<Encryptor, File>> fileAccessSupplier = MemoizedSupplier.create(() -> {
		final Bytes passwordBytes;
		{// generate unique password
			Bytes bytes = Bytes.empty();
			for (String part : Arrays.asList(this.getClientConfig().getAddress(), this.getUsername(),
					this.getPassword())) {
				if (part == null)
					part = "";
				Bytes append = Utils.Bits.from(part.getBytes(StandardCharsets.UTF_8));
				bytes = Utils.Bits.concat(bytes, append);
			}
			passwordBytes = bytes;
		}
		Encryptor encryptor = Encryptions.newAESEncryptor(passwordBytes.encodeBase64());
		Bytes passwordBytesHash = Utils.Crypto.hashSHA512(THIS_CLASS.getName(), "$", passwordBytes);
		File file = Utils.Files.tempFile(THIS_CLASS.getName(), "vault-token-cache", "v6",
				passwordBytesHash.encodeHex() + ".aes");
		return new SimpleEntry<>(encryptor, file);
	});

	private final VaultConfig clientConfig;

	private final String username;

	private final String password;

	@Generated("SparkTools")
	private VaultTokenCacheKey(Builder builder) {
		this.clientConfig = builder.clientConfig;
		this.username = builder.username;
		this.password = builder.password;
	}

	public String uuid(Object... qualifiers) {
		Stream<Object> stream = qualifiers == null ? Stream.empty() : Stream.of(qualifiers);
		stream = Stream.concat(stream, Stream.of(clientConfig.getAddress(), getUsername(), getPassword()));
		Bytes hashBytes = Utils.Crypto.hashSHA512(stream.toArray(Object[]::new));
		String encodedHash = hashBytes.encodeBase64();
		encodedHash = Utils.Strings.stripEnd(encodedHash, "=");
		return encodedHash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj != null && obj.getClass() == this.getClass()) {
			VaultTokenCacheKey other = (VaultTokenCacheKey) obj;
			return Objects.equals(uuid(), other.uuid());
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = getClass().hashCode();
		hash = hash * 31 + Objects.hashCode(uuid());
		return hash;
	}

	public File getFile() {
		return fileAccessSupplier.get().getValue();
	}

	public InputStream inputStream() throws FileNotFoundException, GeneralSecurityException, IOException {
		return fileAccessSupplier.get().getKey().wrapInputStream(new FileInputStream(this.getFile()));
	}

	public OutputStream outputStream() throws FileNotFoundException, GeneralSecurityException, IOException {
		return fileAccessSupplier.get().getKey().wrapOutputStream(new FileOutputStream(this.getFile()));
	}

	public VaultConfig getClientConfig() {
		return clientConfig;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	/**
	 * Creates builder to build {@link VaultTokenCacheKey}.
	 * 
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Creates a builder to build {@link VaultTokenCacheKey} and initialize it with
	 * the given object.
	 * 
	 * @param vaultTokenCacheKey to initialize the builder with
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builderFrom(VaultTokenCacheKey vaultTokenCacheKey) {
		return new Builder(vaultTokenCacheKey);
	}

	/**
	 * Builder to build {@link VaultTokenCacheKey}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private VaultConfig clientConfig;
		private String username;
		private String password;

		private Builder() {
		}

		private Builder(VaultTokenCacheKey vaultTokenCacheKey) {
			this.clientConfig = vaultTokenCacheKey.clientConfig;
			this.username = vaultTokenCacheKey.username;
			this.password = vaultTokenCacheKey.password;
		}

		public Builder withClientConfig(VaultConfig clientConfig) {
			this.clientConfig = clientConfig;
			return this;
		}

		public Builder withUsername(String username) {
			this.username = username;
			return this;
		}

		public Builder withPassword(String password) {
			this.password = password;
			return this;
		}

		public VaultTokenCacheKey build() {
			return new VaultTokenCacheKey(this);
		}
	}

}
