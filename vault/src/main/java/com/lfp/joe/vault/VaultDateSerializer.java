package com.lfp.joe.vault;

import java.lang.reflect.Type;
import java.util.Date;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.lfp.joe.serial.gson.DateTimeJsonSerializer;
import com.lfp.joe.utils.time.TimeParser;

public class VaultDateSerializer implements JsonSerializer<Date>, JsonDeserializer<Date> {

	@Override
	public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		if (json == null || json.isJsonNull())
			return null;
		if (json.isJsonPrimitive()) {
			var jsonPrimitive = json.getAsJsonPrimitive();
			if (jsonPrimitive.isString())
				return parseDate(jsonPrimitive.getAsString());
			return new Date(jsonPrimitive.getAsLong());
		}
		return DateTimeJsonSerializer.INSTANCE.deserialize(json, typeOfT, context);
	}

	@Override
	public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
		if (src == null)
			return JsonNull.INSTANCE;
		return new JsonPrimitive(src.getTime());
	}

	private Date parseDate(String text) {
		var result = TimeParser.instance().tryParseDate(text).orElse(null);
		return result;
	}

}
