package test;

import java.time.Duration;
import java.util.stream.Collectors;

import com.lfp.joe.core.function.Durations;
import com.lfp.joe.core.properties.loader.Loaders;

public class ConfigTest {

	public static void main(String[] args) {
		System.out.println(Duration.ofSeconds(2).compareTo(Duration.ofSeconds(3)));
		System.out.println(Duration.ofSeconds(3).compareTo(Duration.ofSeconds(2)));
		System.out.println(Duration.ofSeconds(2).compareTo(Durations.max()));
		System.out.println(Durations.max().compareTo(Durations.max()));

		for (int i = 0; i < 10; i++) {
			var currentTimeMillis = System.currentTimeMillis();
			System.out.println(Loaders.streamAutowiredLoaders().collect(Collectors.toList()).size());
			System.out
					.println(Loaders.streamAutowiredMultiLoaderClassTypeLoaders().collect(Collectors.toList()).size());
			System.out.println(System.currentTimeMillis() - currentTimeMillis);
		}
	}
}
