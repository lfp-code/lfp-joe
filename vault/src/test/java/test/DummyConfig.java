package test;

import org.aeonbits.owner.Config;

public interface DummyConfig extends Config {

	@DefaultValue("true")
	boolean enabled();
}
