package com.lfp.joe.stream;

import java.util.Collections;
import java.util.Iterator;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.BaseStream;

import org.immutables.value.Value;

import com.lfp.joe.core.config.ValueLFP;

import one.util.streamex.StreamEx;

@ValueLFP.Style
@Value.Immutable
abstract class AbstractFlatMapFunction<X, Y> implements Function<BaseStream<? extends X, ?>, StreamEx<Y>> {

	public abstract Function<? super X, ? extends BaseStream<? extends Y, ?>> mapper();

	@Value.Default
	public boolean disableSubStreamCloseOnComplete() {
		return false;
	}

	@Override
	public StreamEx<Y> apply(BaseStream<? extends X, ?> baseStream) {
		if (baseStream == null)
			return Streams.empty();
		var sc = StreamContextAccessor.of(baseStream);
		var producer = new Predicate<Consumer<? super Y>>() {

			private Iterator<? extends X> iterator;
			private BaseStream<? extends Y, ?> subStream;
			private Iterator<? extends Y> subIterator;

			@Override
			public boolean test(Consumer<? super Y> action) {
				if (iterator == null) {
					iterator = baseStream.iterator();
					if (iterator == null)
						return false;
				}
				var nextOp = tryAdvance(subIterator);
				if (nextOp != null) {
					action.accept(nextOp.orElse(null));
					return true;
				}
				if (!disableSubStreamCloseOnComplete())
					Streams.close(subStream);
				var subStreamOp = tryAdvance(iterator);
				if (subStreamOp != null) {
					subStream = subStreamOp.map(mapper()).orElse(null);
					subIterator = Optional.ofNullable(subStream).map(BaseStream::iterator).orElse(null);
					return test(action);
				} else {
					discard(false);
					return false;
				}
			}

			public void discard(boolean forceSubStreamClose) {
				iterator = Collections.emptyIterator();
				if (forceSubStreamClose || !disableSubStreamCloseOnComplete())
					Streams.close(subStream);
				subStream = null;
				subIterator = null;
			}

		};
		var stream = StreamEx.produce(producer);
		stream = sc.applyToStream(stream);
		stream = stream.onClose(() -> {
			producer.discard(true);
		});
		return stream;
	}

	private static <U> Optional<U> tryAdvance(Iterator<? extends U> iterator) {
		if (iterator == null || !iterator.hasNext())
			return null;
		return Optional.ofNullable(iterator.next());
	}

}
