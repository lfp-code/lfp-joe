package com.lfp.joe.stream;

import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.OptionalLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.BaseStream;

import javax.annotation.Nullable;

import org.immutables.value.Value;

import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.Durations;
import com.lfp.joe.stream.BufferFunction.State;

import one.util.streamex.StreamEx;

@ValueLFP.Style
@Value.Immutable
@Value.Enclosing
abstract class AbstractBufferFunction<X> implements Function<BaseStream<? extends X, ?>, StreamEx<List<X>>> {

	@Value.Parameter(order = 0)
	@Nullable
	public abstract Number bufferSize();

	@Value.Parameter(order = 1)
	@Nullable
	public abstract Duration bufferTimeout();

	@Override
	public StreamEx<List<X>> apply(BaseStream<? extends X, ?> baseStream) {
		if (baseStream == null)
			return StreamEx.empty();
		StreamEx<X> stream = Streams.of(baseStream);
		if (stream.isParallel())
			stream = stream.chain(Streams.parallelSequential());
		var stateReference = new AtomicReference<State>(State.build(BufferFunction.copyOf(this)));
		return stream.groupRuns((nil1, nil2) -> {
			var state = stateReference.updateAndGet(v -> v.increment(BufferFunction.copyOf(this)));
			if (state.reset())
				return false;
			return true;
		});
	}

	protected OptionalInt tryGetBufferSize() {
		if (bufferSize() == null)
			return OptionalInt.empty();
		return OptionalInt.of(bufferSize().intValue());
	}

	protected Optional<Duration> tryGetBufferTimeout() {
		return Optional.ofNullable(bufferTimeout());
	}

	@Value.Check
	protected AbstractBufferFunction<X> check() {
		final Integer bufferSize;
		if (bufferSize() == null)
			bufferSize = null;
		else if (bufferSize() instanceof Integer)
			bufferSize = bufferSize().intValue();
		else
			return BufferFunction.copyOf(this).withBufferSize(bufferSize().intValue());
		if (bufferSize != null) {
			if (bufferSize == -1)
				return BufferFunction.copyOf(this).withBufferSize(null);
			if (bufferSize < -1)
				throw new IllegalArgumentException("invalid buffer size:" + bufferSize());
		}
		final var bufferTimeout = bufferTimeout();
		if (bufferTimeout != null) {
			if (Durations.isMax(bufferTimeout))
				return BufferFunction.copyOf(this).withBufferTimeout(null);
			if (bufferTimeout.isZero())
				return BufferFunction.of(1, null);
			if (bufferTimeout.isNegative())
				throw new IllegalArgumentException("invalid buffer timeout:" + bufferTimeout());
		}
		return this;
	}

	@Value.Immutable
	static abstract class AbstractState {

		@Value.Default
		boolean reset() {
			return false;
		}

		@Value.Default
		int size() {
			return 0;
		}

		abstract OptionalLong resetAt();

		State increment(BufferFunction<?> bufferPredicate) {
			var state = State.copyOf(this);
			if (state.reset())
				state = state.withReset(false);
			var bufferSize = bufferPredicate.tryGetBufferSize();
			if (bufferSize.isPresent()) {
				state = state.withSize(state.size() + 1);
				if (state.size() >= bufferSize.getAsInt())
					state = state.withReset(true);
			}
			var bufferTimeout = bufferPredicate.tryGetBufferTimeout();
			if (bufferTimeout.isPresent()) {
				if (resetAt().isEmpty())
					state = state.withReset(true);
				else {
					var elapsed = System.currentTimeMillis() - resetAt().getAsLong();
					if (elapsed >= bufferTimeout.get().toMillis())
						state = state.withReset(true);
				}
			}
			if (state.reset()) {
				state = state.withSize(0);
				if (bufferPredicate.tryGetBufferTimeout().isPresent())
					state = state.withResetAt(System.currentTimeMillis());
			}
			return state;
		}

		public static State build(BufferFunction<?> bufferPredicate) {
			var resetAt = Optional.ofNullable(bufferPredicate.bufferTimeout()).map(v -> {
				return OptionalLong.of(System.currentTimeMillis());
			}).orElse(OptionalLong.empty());
			var state = State.builder().resetAt(resetAt).build();
			return state;
		}

	}

}
