package com.lfp.joe.stream;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Supplier;
import java.util.stream.Stream;

import one.util.streamex.EntryStream;

/**
 * Auto generated static delegate class of {@link EntryStream}
 * 
 * @see EntryStream
 */
public class EntryStreamAccess {

	protected EntryStreamAccess() {}

	/**
	 * ++++++++++++++++++++ BEGIN STATIC DELEGATE METHODS ++++++++++++++++++++
	 **/
	/**
	 * Delegate method of {@link EntryStream#empty()}
	 * 
	 * @see EntryStream#empty()
	 */
	public static <K, V> EntryStream<K, V> empty() {
		return EntryStream.empty();
	}

	/**
	 * Delegate method of {@link EntryStream#of(V[])}
	 * 
	 * @see EntryStream#of(V[])
	 */
	public static <V> EntryStream<Integer, V> of(V[] array) {
		return EntryStream.of(array);
	}

	/**
	 * Delegate method of {@link EntryStream#of(K, V)}
	 * 
	 * @see EntryStream#of(K, V)
	 */
	public static <K, V> EntryStream<K, V> of(K key, V value) {
		return EntryStream.of(key, value);
	}

	/**
	 * Delegate method of {@link EntryStream#of(K, V, K, V)}
	 * 
	 * @see EntryStream#of(K, V, K, V)
	 */
	public static <K, V> EntryStream<K, V> of(K k1, V v1, K k2, V v2) {
		return EntryStream.of(k1, v1, k2, v2);
	}

	/**
	 * Delegate method of {@link EntryStream#of(K, V, K, V, K, V)}
	 * 
	 * @see EntryStream#of(K, V, K, V, K, V)
	 */
	public static <K, V> EntryStream<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3) {
		return EntryStream.of(k1, v1, k2, v2, k3, v3);
	}

	/**
	 * Delegate method of {@link EntryStream#of(K, V, K, V, K, V, K, V)}
	 * 
	 * @see EntryStream#of(K, V, K, V, K, V, K, V)
	 */
	public static <K, V> EntryStream<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4) {
		return EntryStream.of(k1, v1, k2, v2, k3, v3, k4, v4);
	}

	/**
	 * Delegate method of {@link EntryStream#of(K, V, K, V, K, V, K, V, K, V)}
	 * 
	 * @see EntryStream#of(K, V, K, V, K, V, K, V, K, V)
	 */
	public static <K, V> EntryStream<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
		return EntryStream.of(k1, v1, k2, v2, k3, v3, k4, v4, k5, v5);
	}

	/**
	 * Delegate method of {@link EntryStream#of(K, V, K, V, K, V, K, V, K, V, K, V)}
	 * 
	 * @see EntryStream#of(K, V, K, V, K, V, K, V, K, V, K, V)
	 */
	public static <K, V> EntryStream<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5, K k6, V v6) {
		return EntryStream.of(k1, v1, k2, v2, k3, v3, k4, v4, k5, v5, k6, v6);
	}

	/**
	 * Delegate method of
	 * {@link EntryStream#of(K, V, K, V, K, V, K, V, K, V, K, V, K, V)}
	 * 
	 * @see EntryStream#of(K, V, K, V, K, V, K, V, K, V, K, V, K, V)
	 */
	public static <K, V> EntryStream<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5, K k6, V v6,
			K k7, V v7) {
		return EntryStream.of(k1, v1, k2, v2, k3, v3, k4, v4, k5, v5, k6, v6, k7, v7);
	}

	/**
	 * Delegate method of
	 * {@link EntryStream#of(K, V, K, V, K, V, K, V, K, V, K, V, K, V, K, V)}
	 * 
	 * @see EntryStream#of(K, V, K, V, K, V, K, V, K, V, K, V, K, V, K, V)
	 */
	public static <K, V> EntryStream<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5, K k6, V v6,
			K k7, V v7, K k8, V v8) {
		return EntryStream.of(k1, v1, k2, v2, k3, v3, k4, v4, k5, v5, k6, v6, k7, v7, k8, v8);
	}

	/**
	 * Delegate method of
	 * {@link EntryStream#of(K, V, K, V, K, V, K, V, K, V, K, V, K, V, K, V, K, V)}
	 * 
	 * @see EntryStream#of(K, V, K, V, K, V, K, V, K, V, K, V, K, V, K, V, K, V)
	 */
	public static <K, V> EntryStream<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5, K k6, V v6,
			K k7, V v7, K k8, V v8, K k9, V v9) {
		return EntryStream.of(k1, v1, k2, v2, k3, v3, k4, v4, k5, v5, k6, v6, k7, v7, k8, v8, k9, v9);
	}

	/**
	 * Delegate method of
	 * {@link EntryStream#of(K, V, K, V, K, V, K, V, K, V, K, V, K, V, K, V, K, V, K, V)}
	 * 
	 * @see EntryStream#of(K, V, K, V, K, V, K, V, K, V, K, V, K, V, K, V, K, V, K,
	 *      V)
	 */
	public static <K, V> EntryStream<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5, K k6, V v6,
			K k7, V v7, K k8, V v8, K k9, V v9, K k10, V v10) {
		return EntryStream.of(k1, v1, k2, v2, k3, v3, k4, v4, k5, v5, k6, v6, k7, v7, k8, v8, k9, v9, k10, v10);
	}

	/**
	 * Delegate method of {@link EntryStream#zip(List<K>, List<V>)}
	 * 
	 * @see EntryStream#zip(List<K>, List<V>)
	 */
	public static <K, V> EntryStream<K, V> zip(List<K> keys, List<V> values) {
		return EntryStream.zip(keys, values);
	}

	/**
	 * Delegate method of {@link EntryStream#zip(K[], V[])}
	 * 
	 * @see EntryStream#zip(K[], V[])
	 */
	public static <K, V> EntryStream<K, V> zip(K[] keys, V[] values) {
		return EntryStream.zip(keys, values);
	}

	/**
	 * Delegate method of {@link EntryStream#ofPairs(List<T>)}
	 * 
	 * @see EntryStream#ofPairs(List<T>)
	 */
	public static <T> EntryStream<T, T> ofPairs(List<T> list) {
		return EntryStream.ofPairs(list);
	}

	/**
	 * Delegate method of {@link EntryStream#ofPairs(T[])}
	 * 
	 * @see EntryStream#ofPairs(T[])
	 */
	public static <T> EntryStream<T, T> ofPairs(T[] array) {
		return EntryStream.ofPairs(array);
	}

	/**
	 * Delegate method of {@link EntryStream#ofTree(T, BiFunction<Integer, T,
	 * Stream<T>>)}
	 * 
	 * @see EntryStream#ofTree(T, BiFunction<Integer, T, Stream<T>>)
	 */
	public static <T> EntryStream<Integer, T> ofTree(T root, BiFunction<Integer, T, Stream<T>> mapper) {
		return EntryStream.ofTree(root, mapper);
	}

	/**
	 * Delegate method of {@link EntryStream#ofTree(T, Class<TT>,
	 * BiFunction<Integer, TT, Stream<T>>)}
	 * 
	 * @see EntryStream#ofTree(T, Class<TT>, BiFunction<Integer, TT, Stream<T>>)
	 */
	public static <T, TT extends T> EntryStream<Integer, T> ofTree(T root, Class<TT> collectionClass,
			BiFunction<Integer, TT, Stream<T>> mapper) {
		return EntryStream.ofTree(root, collectionClass, mapper);
	}

	/**
	 * Delegate method of {@link EntryStream#generate(Supplier<? extends K>,
	 * Supplier<? extends V>)}
	 * 
	 * @see EntryStream#generate(Supplier<? extends K>, Supplier<? extends V>)
	 */
	public static <K, V> EntryStream<K, V> generate(Supplier<? extends K> keySupplier,
			Supplier<? extends V> valueSupplier) {
		return EntryStream.generate(keySupplier, valueSupplier);
	}
}
