package com.lfp.joe.stream.support;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

public interface CloseableCollection<T> extends Collection<T>, AutoCloseable {

	@SuppressWarnings("unchecked")
	public static <U> CloseableCollection<U> empty() {
		return Const.EMPTY;
	}

	public static abstract class Abs<T> extends AbstractCollection<T> implements CloseableCollection<T> {

	}

	static enum Const {
		;

		@SuppressWarnings("rawtypes")
		private static final CloseableCollection EMPTY = new CloseableCollection.Abs() {

			@Override
			public Iterator iterator() {
				return Collections.emptyIterator();
			}

			@Override
			public int size() {
				return 0;
			}

			@Override
			public void close() {}
		};
	}
}
