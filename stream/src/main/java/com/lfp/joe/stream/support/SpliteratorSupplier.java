package com.lfp.joe.stream.support;

import java.util.Spliterator;
import java.util.Spliterators;

public interface SpliteratorSupplier<T> extends CloseableSupplier<Spliterator<T>> {

	@SuppressWarnings("unchecked")
	public static <U> SpliteratorSupplier<U> empty() {
		return Const.EMPTY;
	}

	static enum Const {
		;

		@SuppressWarnings("rawtypes")
		private static final SpliteratorSupplier EMPTY = new SpliteratorSupplier() {

			@Override
			public Spliterator get() {
				return Spliterators.emptySpliterator();
			}

			@Override
			public void close() {}
		};
	}
}
