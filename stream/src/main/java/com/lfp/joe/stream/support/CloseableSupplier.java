package com.lfp.joe.stream.support;

import java.util.function.Supplier;

interface CloseableSupplier<T> extends Supplier<T>, AutoCloseable {

	@Override
	void close();

}
