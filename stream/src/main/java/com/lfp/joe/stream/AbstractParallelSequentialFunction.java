package com.lfp.joe.stream;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.Flow;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.BaseStream;

import javax.annotation.Nullable;

import org.immutables.value.Value;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.EmptyFunctions;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.lock.PermitObtainer;
import com.lfp.joe.core.process.future.Completion;

import one.util.streamex.StreamEx;

@ValueLFP.Style
@Value.Immutable
abstract class AbstractParallelSequentialFunction<X> implements Function<BaseStream<? extends X, ?>, StreamEx<X>> {

	@Value.Default
	public Function<StreamContextAccessor, ForkJoinPoolContext> forkJoinPoolConextFunction() {
		return ForkJoinPoolContext::from;
	}

	@Nullable
	@Value.Default
	public Number maxBufferSize() {
		var defaultBufferSize = Flow.defaultBufferSize();
		return (int) (defaultBufferSize + ((MachineConfig.logicalCoreCount() - 1) * defaultBufferSize * .25));
	}

	@Value.Default
	public BiConsumer<? super Void, Throwable> whenComplete() {
		return EmptyFunctions.biConsumer();
	}

	@Override
	public StreamEx<X> apply(BaseStream<? extends X, ?> baseStream) {
		if (baseStream == null)
			return StreamEx.empty();
		var sc = StreamContextAccessor.of(baseStream);
		var jfpContext = Objects.requireNonNull(forkJoinPoolConextFunction().apply(sc));
		var stream = Streams.of(baseStream);
		var queue = new LinkedBlockingQueue<Optional<Completion<X>>>();
		var queueThrottle = maxBufferSize() == null ? PermitObtainer.unlimited()
				: PermitObtainer.create(maxBufferSize().intValue(), false, true);
		var future = CompletableFuture.runAsync(() -> {
			stream.parallel(jfpContext.forkJoinPool()).forEachOrdered(v -> {
				try (var r = Throws.unchecked(() -> queueThrottle.obtain())) {
					queue.add(Optional.of(Completion.of(v)));
				}
			});
		}, jfpContext.forkJoinPool());
		var whenComplete = whenComplete().andThen((v, t) -> {
			jfpContext.close();
			queueThrottle.close();
			if (t != null) {
				t = Optional.ofNullable(t.getCause()).orElse(t);
				queue.add(Optional.of(Completion.of(t)));
			} else
				queue.add(Optional.empty());
		});
		future.whenComplete(whenComplete);
		Supplier<Completion<X>> advanceSupplier = () -> {
			try {
				var completion = queue.take().orElse(null);
				return completion;
			} catch (InterruptedException e) {
				return Completion.of(e);
			}
		};
		var consumeStream = StreamEx.iterate(advanceSupplier.get(), Objects::nonNull, nil -> advanceSupplier.get())
				.map(completion -> {
					if (completion.isSuccess())
						return completion.result();
					var failure = completion.failure();
					failure = Throws.unwrap(failure, CompletionException.class);
					throw Throws.unchecked(failure);
				});
		consumeStream = sc.applyToStream(consumeStream).sequential();
		consumeStream = consumeStream.onClose(() -> future.cancel(true));
		return consumeStream;
	}

	@Value.Check
	protected AbstractParallelSequentialFunction<X> check() {
		if (maxBufferSize() != null) {
			var maxBufferSizeInt = maxBufferSize().intValue();
			if (maxBufferSizeInt == -1)
				return ParallelSequentialFunction.copyOf(this).withMaxBufferSize(null);
			if (maxBufferSizeInt <= 0)
				throw new IllegalArgumentException("invalid maxBufferSize:" + maxBufferSize());
		}
		return this;
	}

}
