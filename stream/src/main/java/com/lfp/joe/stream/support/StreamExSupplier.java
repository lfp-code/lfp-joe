package com.lfp.joe.stream.support;

import one.util.streamex.StreamEx;

public interface StreamExSupplier<T> extends CloseableSupplier<StreamEx<T>> {

	@SuppressWarnings("unchecked")
	public static <U> StreamExSupplier<U> empty() {
		return Const.EMPTY;
	}

	static enum Const {
		;

		@SuppressWarnings("rawtypes")
		private static final StreamExSupplier EMPTY = new StreamExSupplier() {

			@Override
			public StreamEx get() {
				return StreamEx.empty();
			}

			@Override
			public void close() {}
		};
	}
}
