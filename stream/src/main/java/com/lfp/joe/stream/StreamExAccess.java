package com.lfp.joe.stream;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Spliterator;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import one.util.streamex.StreamEx;

/**
 * Auto generated static delegate class of {@link StreamEx}
 * 
 * @see StreamEx
 */
public class StreamExAccess {

	protected StreamExAccess() {}

	/**
	 * ++++++++++++++++++++ BEGIN STATIC DELEGATE METHODS ++++++++++++++++++++
	 **/

	/**
	 * Delegate method of {@link StreamEx#empty()}
	 * 
	 * @see StreamEx#empty()
	 */
	public static <T> StreamEx<T> empty() {
		return StreamEx.empty();
	}

	/**
	 * Delegate method of {@link StreamEx#of(T)}
	 * 
	 * @see StreamEx#of(T)
	 */
	public static <T> StreamEx<T> of(T element) {
		return StreamEx.of(element);
	}

	/**
	 * Delegate method of {@link StreamEx#of(T[], int, int)}
	 * 
	 * @see StreamEx#of(T[], int, int)
	 */
	public static <T> StreamEx<T> of(T[] array, int startInclusive, int endExclusive) {
		return StreamEx.of(array, startInclusive, endExclusive);
	}

	/**
	 * Delegate method of {@link StreamEx#of(Collection<? extends T>)}
	 * 
	 * @see StreamEx#of(Collection<? extends T>)
	 * 
	 *      public static <T> StreamEx<T> of(Collection<? extends T> collection) {
	 *      return StreamEx.of(collection); }
	 */

	/**
	 * Delegate method of {@link StreamEx#ofReversed(List<? extends T>)}
	 * 
	 * @see StreamEx#ofReversed(List<? extends T>)
	 */
	public static <T> StreamEx<T> ofReversed(List<? extends T> list) {
		return StreamEx.ofReversed(list);
	}

	/**
	 * Delegate method of {@link StreamEx#ofReversed(T[])}
	 * 
	 * @see StreamEx#ofReversed(T[])
	 */
	public static <T> StreamEx<T> ofReversed(T[] array) {
		return StreamEx.ofReversed(array);
	}

	/**
	 * Delegate method of {@link StreamEx#of(Spliterator<? extends T>)}
	 * 
	 * @see StreamEx#of(Spliterator<? extends T>)
	 */
	public static <T> StreamEx<T> of(Spliterator<? extends T> spliterator) {
		return StreamEx.of(spliterator);
	}

	/**
	 * Delegate method of {@link StreamEx#of(Iterator<? extends T>)}
	 * 
	 * @see StreamEx#of(Iterator<? extends T>)
	 */
	public static <T> StreamEx<T> of(Iterator<? extends T> iterator) {
		return StreamEx.of(iterator);
	}

	/**
	 * Delegate method of {@link StreamEx#of(Enumeration<? extends T>)}
	 * 
	 * @see StreamEx#of(Enumeration<? extends T>)
	 */
	public static <T> StreamEx<T> of(Enumeration<? extends T> enumeration) {
		return StreamEx.of(enumeration);
	}

	/**
	 * Delegate method of {@link StreamEx#of(Optional<? extends T>)}
	 * 
	 * @see StreamEx#of(Optional<? extends T>)
	 */
	public static <T> StreamEx<T> of(Optional<? extends T> optional) {
		return StreamEx.of(optional);
	}

	/**
	 * Delegate method of {@link StreamEx#ofNullable(T)}
	 * 
	 * @see StreamEx#ofNullable(T)
	 */
	public static <T> StreamEx<T> ofNullable(T element) {
		return StreamEx.ofNullable(element);
	}

	/**
	 * Delegate method of {@link StreamEx#ofLines(BufferedReader)}
	 * 
	 * @see StreamEx#ofLines(BufferedReader)
	 */
	public static StreamEx<String> ofLines(BufferedReader reader) {
		return StreamEx.ofLines(reader);
	}

	/**
	 * Delegate method of {@link StreamEx#ofLines(Reader)}
	 * 
	 * @see StreamEx#ofLines(Reader)
	 */
	public static StreamEx<String> ofLines(Reader reader) {
		return StreamEx.ofLines(reader);
	}

	/**
	 * Delegate method of {@link StreamEx#ofLines(Path)}
	 * 
	 * @throws IOException
	 * 
	 * @see StreamEx#ofLines(Path)
	 */
	public static StreamEx<String> ofLines(Path path) throws IOException {
		return StreamEx.ofLines(path);
	}

	/**
	 * Delegate method of {@link StreamEx#ofLines(Path, Charset)}
	 * 
	 * @throws IOException
	 * 
	 * @see StreamEx#ofLines(Path, Charset)
	 */
	public static StreamEx<String> ofLines(Path path, Charset charset) throws IOException {
		return StreamEx.ofLines(path, charset);
	}

	/**
	 * Delegate method of {@link StreamEx#ofKeys(Map<T, ?>)}
	 * 
	 * @see StreamEx#ofKeys(Map<T, ?>)
	 */
	public static <T> StreamEx<T> ofKeys(Map<T, ?> map) {
		return StreamEx.ofKeys(map);
	}

	/**
	 * Delegate method of {@link StreamEx#ofKeys(Map<T, V>, Predicate<? super V>)}
	 * 
	 * @see StreamEx#ofKeys(Map<T, V>, Predicate<? super V>)
	 */
	public static <T, V> StreamEx<T> ofKeys(Map<T, V> map, Predicate<? super V> valueFilter) {
		return StreamEx.ofKeys(map, valueFilter);
	}

	/**
	 * Delegate method of {@link StreamEx#ofValues(Map<?, T>)}
	 * 
	 * @see StreamEx#ofValues(Map<?, T>)
	 */
	public static <T> StreamEx<T> ofValues(Map<?, T> map) {
		return StreamEx.ofValues(map);
	}

	/**
	 * Delegate method of {@link StreamEx#ofValues(Map<K, T>, Predicate<? super K>)}
	 * 
	 * @see StreamEx#ofValues(Map<K, T>, Predicate<? super K>)
	 */
	public static <K, T> StreamEx<T> ofValues(Map<K, T> map, Predicate<? super K> keyFilter) {
		return StreamEx.ofValues(map, keyFilter);
	}

	/**
	 * Delegate method of {@link StreamEx#ofPermutations(int)}
	 * 
	 * @see StreamEx#ofPermutations(int)
	 */
	public static StreamEx<int[]> ofPermutations(int length) {
		return StreamEx.ofPermutations(length);
	}

	/**
	 * Delegate method of {@link StreamEx#ofCombinations(int, int)}
	 * 
	 * @see StreamEx#ofCombinations(int, int)
	 */
	public static StreamEx<int[]> ofCombinations(int n, int k) {
		return StreamEx.ofCombinations(n, k);
	}

	/**
	 * Delegate method of {@link StreamEx#split(CharSequence, Pattern)}
	 * 
	 * @see StreamEx#split(CharSequence, Pattern)
	 */
	public static StreamEx<String> split(CharSequence str, Pattern pattern) {
		return StreamEx.split(str, pattern);
	}

	/**
	 * Delegate method of {@link StreamEx#split(CharSequence, String)}
	 * 
	 * @see StreamEx#split(CharSequence, String)
	 */
	public static StreamEx<String> split(CharSequence str, String regex) {
		return StreamEx.split(str, regex);
	}

	/**
	 * Delegate method of {@link StreamEx#split(CharSequence, char)}
	 * 
	 * @see StreamEx#split(CharSequence, char)
	 */
	public static StreamEx<String> split(CharSequence str, char delimiter) {
		return StreamEx.split(str, delimiter);
	}

	/**
	 * Delegate method of {@link StreamEx#split(CharSequence, char, boolean)}
	 * 
	 * @see StreamEx#split(CharSequence, char, boolean)
	 */
	public static StreamEx<String> split(CharSequence str, char delimiter, boolean trimEmpty) {
		return StreamEx.split(str, delimiter, trimEmpty);
	}

	/**
	 * Delegate method of {@link StreamEx#iterate(final T, final UnaryOperator<T>)}
	 * 
	 * @see StreamEx#iterate(final T, final UnaryOperator<T>)
	 */
	public static <T> StreamEx<T> iterate(final T seed, final UnaryOperator<T> f) {
		return StreamEx.iterate(seed, f);
	}

	/**
	 * Delegate method of {@link StreamEx#iterate(T, Predicate<? super T>,
	 * UnaryOperator<T>)}
	 * 
	 * @see StreamEx#iterate(T, Predicate<? super T>, UnaryOperator<T>)
	 */
	public static <T> StreamEx<T> iterate(T seed, Predicate<? super T> predicate, UnaryOperator<T> f) {
		return StreamEx.iterate(seed, predicate, f);
	}

	/**
	 * Delegate method of {@link StreamEx#generate(Supplier<T>)}
	 * 
	 * @see StreamEx#generate(Supplier<T>)
	 */
	public static <T> StreamEx<T> generate(Supplier<T> s) {
		return StreamEx.generate(s);
	}

	/**
	 * Delegate method of {@link StreamEx#produce(Predicate<Consumer<? super T>>)}
	 * 
	 * @see StreamEx#produce(Predicate<Consumer<? super T>>)
	 */
	public static <T> StreamEx<T> produce(Predicate<Consumer<? super T>> producer) {
		return StreamEx.produce(producer);
	}

	/**
	 * Delegate method of {@link StreamEx#constant(T, long)}
	 * 
	 * @see StreamEx#constant(T, long)
	 */
	public static <T> StreamEx<T> constant(T value, long length) {
		return StreamEx.constant(value, length);
	}

	/**
	 * Delegate method of {@link StreamEx#ofPairs(List<U>, BiFunction<? super U, ?
	 * super U, ? extends T>)}
	 * 
	 * @see StreamEx#ofPairs(List<U>, BiFunction<? super U, ? super U, ? extends T>)
	 */
	public static <U, T> StreamEx<T> ofPairs(List<U> list, BiFunction<? super U, ? super U, ? extends T> mapper) {
		return StreamEx.ofPairs(list, mapper);
	}

	/**
	 * Delegate method of {@link StreamEx#ofPairs(U[], BiFunction<? super U, ? super
	 * U, ? extends T>)}
	 * 
	 * @see StreamEx#ofPairs(U[], BiFunction<? super U, ? super U, ? extends T>)
	 */
	public static <U, T> StreamEx<T> ofPairs(U[] array, BiFunction<? super U, ? super U, ? extends T> mapper) {
		return StreamEx.ofPairs(array, mapper);
	}

	/**
	 * Delegate method of {@link StreamEx#zip(List<U>, List<V>, BiFunction<? super
	 * U, ? super V, ? extends T>)}
	 * 
	 * @see StreamEx#zip(List<U>, List<V>, BiFunction<? super U, ? super V, ?
	 *      extends T>)
	 */
	public static <U, V, T> StreamEx<T> zip(List<U> first, List<V> second,
			BiFunction<? super U, ? super V, ? extends T> mapper) {
		return StreamEx.zip(first, second, mapper);
	}

	/**
	 * Delegate method of {@link StreamEx#zip(U[], V[], BiFunction<? super U, ?
	 * super V, ? extends T>)}
	 * 
	 * @see StreamEx#zip(U[], V[], BiFunction<? super U, ? super V, ? extends T>)
	 */
	public static <U, V, T> StreamEx<T> zip(U[] first, V[] second,
			BiFunction<? super U, ? super V, ? extends T> mapper) {
		return StreamEx.zip(first, second, mapper);
	}

	/**
	 * Delegate method of {@link StreamEx#ofTree(T, Function<T, Stream<T>>)}
	 * 
	 * @see StreamEx#ofTree(T, Function<T, Stream<T>>)
	 */
	public static <T> StreamEx<T> ofTree(T root, Function<T, Stream<T>> mapper) {
		return StreamEx.ofTree(root, mapper);
	}

	/**
	 * Delegate method of {@link StreamEx#ofTree(T, Class<TT>, Function<TT,
	 * Stream<T>>)}
	 * 
	 * @see StreamEx#ofTree(T, Class<TT>, Function<TT, Stream<T>>)
	 */
	public static <T, TT extends T> StreamEx<T> ofTree(T root, Class<TT> collectionClass,
			Function<TT, Stream<T>> mapper) {
		return StreamEx.ofTree(root, collectionClass, mapper);
	}

	/**
	 * Delegate method of {@link StreamEx#ofSubLists(List<T>, int)}
	 * 
	 * @see StreamEx#ofSubLists(List<T>, int)
	 */
	public static <T> StreamEx<List<T>> ofSubLists(List<T> source, int length) {
		return StreamEx.ofSubLists(source, length);
	}

	/**
	 * Delegate method of {@link StreamEx#ofSubLists(List<T>, int, int)}
	 * 
	 * @see StreamEx#ofSubLists(List<T>, int, int)
	 */
	public static <T> StreamEx<List<T>> ofSubLists(List<T> source, int length, int shift) {
		return StreamEx.ofSubLists(source, length, shift);
	}

	/**
	 * Delegate method of {@link StreamEx#cartesianProduct(Collection<? extends
	 * Collection<T>>)}
	 * 
	 * @see StreamEx#cartesianProduct(Collection<? extends Collection<T>>)
	 */
	public static <T> StreamEx<List<T>> cartesianProduct(Collection<? extends Collection<T>> source) {
		return StreamEx.cartesianProduct(source);
	}

	/**
	 * Delegate method of {@link StreamEx#cartesianProduct(Collection<? extends
	 * Collection<T>>, U, BiFunction<U, ? super T, U>)}
	 * 
	 * @see StreamEx#cartesianProduct(Collection<? extends Collection<T>>, U,
	 *      BiFunction<U, ? super T, U>)
	 */
	public static <T, U> StreamEx<U> cartesianProduct(Collection<? extends Collection<T>> source, U identity,
			BiFunction<U, ? super T, U> accumulator) {
		return StreamEx.cartesianProduct(source, identity, accumulator);
	}

	/**
	 * Delegate method of {@link StreamEx#cartesianPower(int, Collection<T>)}
	 * 
	 * @see StreamEx#cartesianPower(int, Collection<T>)
	 */
	public static <T> StreamEx<List<T>> cartesianPower(int n, Collection<T> source) {
		return StreamEx.cartesianPower(n, source);
	}

	/**
	 * Delegate method of {@link StreamEx#cartesianPower(int, Collection<T>, U,
	 * BiFunction<U, ? super T, U>)}
	 * 
	 * @see StreamEx#cartesianPower(int, Collection<T>, U, BiFunction<U, ? super T,
	 *      U>)
	 */
	public static <T, U> StreamEx<U> cartesianPower(int n, Collection<T> source, U identity,
			BiFunction<U, ? super T, U> accumulator) {
		return StreamEx.cartesianPower(n, source, identity, accumulator);
	}

}
