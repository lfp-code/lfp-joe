package com.lfp.joe.stream;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Spliterator;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.BaseStream;
import java.util.stream.Stream;

import one.util.streamex.AbstractStreamEx;
import one.util.streamex.EntryStream;

@SuppressWarnings("unchecked")
public class EntryStreams extends EntryStreamAccess {

	public static <K, V> EntryStream<K, V> of(BaseStream<? extends Entry<K, V>, ?> baseStream) {
		if (baseStream == null)
			return EntryStream.empty();
		if (baseStream instanceof EntryStream)
			return (EntryStream<K, V>) baseStream;
		if (baseStream instanceof Stream)
			return EntryStream.of((Stream<? extends Entry<K, V>>) baseStream);
		return EntryStream.of(baseStream.spliterator());
	}

	public static <K, V> EntryStream<K, V> of(AbstractStreamEx<? extends Entry<K, V>, ?> stream) {
		return of((BaseStream<? extends Entry<K, V>, ?>) stream);
	}

	public static <K, V> EntryStream<K, V> of(Iterable<? extends Entry<K, V>> iterable) {
		return Optional.ofNullable(iterable).map(v -> {
			if (v instanceof BaseStream)
				return of((BaseStream<? extends Entry<K, V>, ?>) v);
			if (v instanceof Collection)
				return EntryStream.of(((Collection<? extends Entry<K, V>>) v).stream());
			return EntryStream.of(v.spliterator());
		}).orElseGet(EntryStream::empty);
	}

	public static <K, V> EntryStream<K, V> of(Spliterator<? extends Entry<K, V>> spliterator) {
		return Optional.ofNullable(spliterator).map(EntryStream::of).orElseGet(EntryStream::empty);
	}

	public static <K, V> EntryStream<K, V> of(Iterator<? extends Entry<K, V>> iterator) {
		return Optional.ofNullable(iterator).map(EntryStream::of).orElseGet(EntryStream::empty);
	}

	public static <K, V> EntryStream<K, V> of(Map<K, V> map) {
		return Optional.ofNullable(map).map(EntryStream::of).orElseGet(EntryStream::empty);
	}

	public static <K, V> EntryStream<K, V> ofMultimap(Map<K, ? extends Iterable<V>> map) {
		return EntryStreams.of(map).<V>flatMapValues(Streams::of);
	}

	public static <K, V> Function<BaseStream<? extends Entry<K, V>, ?>, Map<K, V>> immmutableOrderedMap() {
		return baseStream -> {
			if (baseStream == null)
				return Collections.emptyMap();
			return EntryStreams.<K, V>of(baseStream)
					.chain(s -> Collections.unmodifiableMap(s.toCustomMap(LinkedHashMap::new)));
		};
	}

	public static <T> Function<BaseStream<? extends T, ?>, EntryStream<Long, T>> indexed() {
		return baseStream -> {
			if (baseStream == null)
				return EntryStream.empty();
			var index = new AtomicLong(-1);
			return Streams.<T>of(baseStream).mapToEntry(v -> index.incrementAndGet()).invert();
		};
	}

}
