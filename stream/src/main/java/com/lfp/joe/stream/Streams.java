package com.lfp.joe.stream;

import java.lang.reflect.Array;
import java.time.Duration;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.Spliterators.AbstractSpliterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Flow;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.BaseStream;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.classpath.MemberCache.Accessor;
import com.lfp.joe.core.function.ArrayParalleSort;
import com.lfp.joe.core.function.Muto;
import com.lfp.joe.core.function.Once.RunnableOnce;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.stream.support.CloseableCollection;
import com.lfp.joe.stream.support.SpliteratorSupplier;
import com.lfp.joe.stream.support.StreamExSupplier;

import one.util.streamex.AbstractStreamEx;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;

@SuppressWarnings("unchecked")
public class Streams extends StreamExAccess {

	protected Streams() {}

	@SafeVarargs
	public static <T> StreamEx<T> of(T... elements) {
		if (elements == null)
			return Streams.empty();
		return StreamEx.of(elements);
	}

	public static <T> StreamEx<T> of(BaseStream<? extends T, ?> stream) {
		if (stream == null)
			return Streams.empty();
		if (stream instanceof StreamEx)
			return (StreamEx<T>) stream;
		if (stream instanceof java.util.stream.Stream)
			return StreamEx.of((java.util.stream.Stream<T>) stream);
		return create(stream.spliterator(), stream).map(v -> v);
	}

	public static <T> StreamEx<T> of(AbstractStreamEx<? extends T, ?> stream) {
		return of((BaseStream<T, ?>) stream);
	}

	public static <T> StreamEx<T> of(Iterable<? extends T> iterable) {
		if (iterable == null)
			return Streams.empty();
		if (iterable instanceof BaseStream)
			return of((BaseStream<T, ?>) iterable);
		if (iterable instanceof Collection)
			return StreamEx.of((Collection<T>) iterable);
		return Streams.of(iterable.spliterator());
	}

	public static <T> StreamEx<T> of(Supplier<? extends Iterable<? extends T>> iterableSupplier) {
		if (iterableSupplier == null)
			return Streams.empty();
		return StreamEx.of(iterableSupplier).map(Supplier::get).map(Streams::of).chain(Streams.flatMap());
	}

	public static StreamEx<Object> flatten(Object input) {
		return flatten(input, null);
	}

	@SuppressWarnings("rawtypes")
	public static StreamEx<Object> flatten(Object input,
			Function<? super Object, ? extends Iterable<? extends Object>> interceptor) {
		if (interceptor != null) {
			var ible = interceptor.apply(input);
			if (ible != null)
				return Streams.of(ible);
		}
		if (input == null)
			return Streams.of(input);
		StreamEx<Object> stream = CoreReflections.<StreamEx>tryCast(input, StreamEx.class)
				.or(() -> CoreReflections.tryCast(input, BaseStream.class).map(Streams::of))
				.or(() -> CoreReflections.tryCast(input, Spliterator.class).map(Streams::of))
				.or(() -> CoreReflections.tryCast(input, Iterable.class).map(Streams::of))
				.or(() -> CoreReflections.tryCast(input, Iterator.class).map(Streams::of))
				.or(() -> CoreReflections.tryCast(input, Enumeration.class).map(Streams::of))
				.or(() -> CoreReflections.tryCast(input, Map.class).map(m -> {
					return EntryStreams.of(m).<Object>map(Function.identity());
				}))
				.or(() -> {
					if (!input.getClass().isArray())
						return Optional.empty();
					var arrStream = IntStreamEx.range(0, Array.getLength(input)).mapToObj(i -> Array.get(input, i));
					return Optional.of(arrStream);
				})
				.orElse(null);
		if (stream == null)
			return Streams.of(input);
		return stream.flatMap(v -> flatten(v, interceptor));
	}

	public static <T> Function<BaseStream<? extends T, ?>, Set<T>> immmutableOrderedSet() {
		return baseStream -> {
			if (baseStream == null)
				return Collections.emptySet();
			var stream = Streams.<T>of(baseStream);
			var set = stream.toCollection(LinkedHashSet::new);
			return Collections.unmodifiableSet(set);
		};
	}

	public static <T> Function<BaseStream<? extends Optional<T>, ?>, StreamEx<T>> orElse(T value) {
		return orElseGet(() -> value);
	}

	public static <T> Function<BaseStream<? extends Optional<T>, ?>, StreamEx<T>> orElseGet(
			Supplier<? extends T> supplier) {
		Objects.requireNonNull(supplier);
		return stream -> Streams.of(stream).map(v -> {
			return v.orElseGet(supplier);
		});
	}

	public static <T> Function<BaseStream<? extends T, ?>, StreamEx<T>> ifEmpty(Runnable runnable) {
		Objects.requireNonNull(runnable);
		return baseStream -> {
			if (baseStream == null) {
				runnable.run();
				return Streams.empty();
			}
			var stream = Streams.<T>of(baseStream);
			stream = stream.ifEmpty(Streams.of(() -> {
				runnable.run();
				return Streams.empty();
			}));
			return stream;
		};
	}

	public static <T> Function<BaseStream<? extends T, ?>, StreamEx<T>> onLast(Runnable runnable) {
		Objects.requireNonNull(runnable);
		return baseStream -> {
			if (baseStream == null)
				return Streams.empty();
			var opStream = Streams.<T>of(baseStream).map(Optional::ofNullable);
			opStream = opStream.append(Streams.of((Supplier<Iterable<Optional<T>>>) () -> {
				runnable.run();
				return null;
			}));
			return opStream.nonNull().map(v -> v.orElse(null));
		};
	}

	public static <T> Function<BaseStream<? extends T, ?>, StreamEx<T>> onComplete(Runnable onComplete) {
		Objects.requireNonNull(onComplete);
		return baseStream -> {
			if (baseStream == null) {
				onComplete.run();
				return Streams.empty();
			}
			var onCompleteOnce = RunnableOnce.of(onComplete::run);
			return Streams.<T>of(baseStream).chain(v -> {
				var stream = onLast(onCompleteOnce::run).apply(v);
				stream = onLast(onCompleteOnce::run).apply(stream);
				stream = stream.onClose(onCompleteOnce::run);
				return (StreamEx<T>) stream;
			});
		};
	}

	public static <T> Function<BaseStream<? extends T, ?>, StreamEx<T>> parallelSort() {
		return parallelSort(null);
	}

	public static <T> Function<BaseStream<? extends T, ?>, StreamEx<T>> parallelSort(Comparator<? super T> comparator) {
		return parallelSort(comparator, null);
	}

	@SuppressWarnings("rawtypes")
	public static <T> Function<BaseStream<? extends T, ?>, StreamEx<T>> parallelSort(Comparator<? super T> comparator,
			Integer parallelism) {
		return baseStream -> {
			if (baseStream == null)
				return Streams.empty();
			var sc = StreamContextAccessor.of(baseStream);
			T[] arr;
			if (baseStream instanceof java.util.stream.Stream)
				arr = (T[]) ((java.util.stream.Stream) baseStream).toArray();
			else
				arr = (T[]) Streams.of(baseStream).toArray();
			ArrayParalleSort.accept(arr, comparator, parallelism);
			return (StreamEx) sc.applyToStream(Streams.of(arr));
		};
	}

	public static <T> Function<BaseStream<? extends T, ?>, StreamEx<T>> parallelSequential() {
		return parallelSequential(null);
	}

	public static <T> Function<BaseStream<? extends T, ?>, StreamEx<T>> parallelSequential(
			Supplier<ForkJoinPoolContext> forkJoinPoolContextSupplier) {
		var blder = ParallelSequentialFunction.<T>builder();
		if (forkJoinPoolContextSupplier != null)
			blder = blder.forkJoinPoolConextFunction(nil -> forkJoinPoolContextSupplier.get());
		return blder.build();
	}

	public static <T> Function<BaseStream<? extends BaseStream<? extends T, ?>, ?>, StreamEx<T>> flatMap() {
		return flatMap(Function.identity());
	}

	public static <T, R> Function<BaseStream<? extends T, ?>, StreamEx<R>> flatMap(
			Function<? super T, ? extends BaseStream<? extends R, ?>> mapper) {
		return flatMap(mapper, false);
	}

	protected static <T, R> Function<BaseStream<? extends T, ?>, StreamEx<R>> flatMap(
			Function<? super T, ? extends BaseStream<? extends R, ?>> mapper, boolean disableSubStreamCloseOnComplete) {
		Objects.requireNonNull(mapper);
		return FlatMapFunction.<T, R>builder()
				.mapper(mapper)
				.disableSubStreamCloseOnComplete(disableSubStreamCloseOnComplete)
				.build();
	}

	public static <T> Function<BaseStream<? extends T, ?>, StreamEx<List<T>>> buffer() {
		return buffer(Flow.defaultBufferSize(), null);
	}

	public static <T> Function<BaseStream<? extends T, ?>, StreamEx<List<T>>> buffer(Number bufferSize) {
		return buffer(bufferSize, null);
	}

	public static <T> Function<BaseStream<? extends T, ?>, StreamEx<List<T>>> buffer(Duration bufferTimeout) {
		return buffer(-1, bufferTimeout);
	}

	public static <T> Function<BaseStream<? extends T, ?>, StreamEx<List<T>>> buffer(Number bufferSize,
			Duration bufferTimeout) {
		return BufferFunction.<T>of(bufferSize, bufferTimeout);
	}

	public static <T> Function<BaseStream<? extends T, ?>, StreamExSupplier<T>> cached() {
		return baseStream -> {
			if (baseStream == null)
				return StreamExSupplier.empty();
			Function<BaseStream<? extends T, ?>, SpliteratorSupplier<T>> cachedSpliteratorFunction = cachedSpliterator();
			var spliteratorSupplier = cachedSpliteratorFunction.apply(baseStream);
			return new StreamExSupplier<T>() {

				@Override
				public void close() {
					spliteratorSupplier.close();
				}

				@Override
				public StreamEx<T> get() {
					return Streams.of(spliteratorSupplier.get());
				}

			};
		};
	}

	public static <T> Function<BaseStream<? extends T, ?>, SpliteratorSupplier<T>> cachedSpliterator() {
		return baseStream -> {
			if (baseStream == null)
				return SpliteratorSupplier.empty();
			return new SpliteratorSupplier<T>() {

				private final Muto<Spliterator<? extends T>> delegateRef = Muto.create();
				private final Map<Long, Optional<T>> cache = new ConcurrentHashMap<>();

				@Override
				public Spliterator<T> get() {
					return new AbstractSpliterator<T>(getDelegate().estimateSize(), getDelegate().characteristics()) {

						private final AtomicLong indexTracker = new AtomicLong(-1);

						@Override
						public boolean tryAdvance(Consumer<? super T> action) {
							var nextOp = cache.computeIfAbsent(indexTracker.incrementAndGet(), nil -> {
								if (isComplete())
									return null;
								var nextRef = Muto.<T>create();
								if (!getDelegate().tryAdvance(nextRef)) {
									complete();
									return null;
								}
								return Optional.ofNullable(nextRef.get());
							});
							if (nextOp == null)
								return false;
							action.accept(nextOp.orElse(null));
							return true;
						}

						@Override
						public long estimateSize() {
							return isComplete() ? cache.size() : super.estimateSize();
						}

						@Override
						public long getExactSizeIfKnown() {
							return isComplete() ? cache.size() : super.getExactSizeIfKnown();
						}

					};
				}

				@Override
				public void close() {
					complete();
					cache.clear();
				}

				private Spliterator<? extends T> getDelegate() {
					if (delegateRef.get() == null)
						synchronized (delegateRef) {
							if (delegateRef.get() == null) {
								Spliterator<? extends T> delegate = Optional.ofNullable(baseStream.spliterator())
										.orElseGet(Spliterators::emptySpliterator);
								delegateRef.set(delegate);
							}
						}
					return delegateRef.get();
				}

				private void complete() {
					delegateRef.set(Spliterators.emptySpliterator());
					Streams.close(baseStream);
				}

				private boolean isComplete() {
					return Spliterators.emptySpliterator().equals(delegateRef.get());
				}

			};
		};
	}

	public static <T> Function<BaseStream<? extends T, ?>, CloseableCollection<T>> cachedCollection() {
		return baseStream -> {
			if (baseStream == null)
				return CloseableCollection.empty();
			Function<BaseStream<? extends T, ?>, SpliteratorSupplier<T>> cachedSpliteratorFunction = cachedSpliterator();
			var spliteratorSupplier = cachedSpliteratorFunction.apply(baseStream);
			return new CloseableCollection.Abs<T>() {

				@Override
				public void close() {
					spliteratorSupplier.close();
				}

				@Override
				public Iterator<T> iterator() {
					return Spliterators.iterator(spliteratorSupplier.get());
				}

				@Override
				public int size() {
					var spliterator = spliteratorSupplier.get();
					var complete = false;
					while (true) {
						var size = spliterator.getExactSizeIfKnown();
						if (size >= 0)
							return (int) size;
						if (complete)
							break;
						if (!spliterator.tryAdvance(v -> {}))
							complete = true;
					}
					throw new IllegalStateException();
				}

			};
		};
	}

	public static boolean close(BaseStream<?, ?> stream) {
		return CoreReflections.tryCast(stream, AutoCloseable.class).map(v -> {
			Throws.unchecked(() -> v.close());
			return true;
		}).orElse(false);
	}

	public static boolean isStreamEx(BaseStream<?, ?> stream) {
		if (stream == null)
			return false;
		return AbstractStreamEx.class.getSuperclass().isInstance(stream);
	}

	public static long estimateSize(BaseStream<?, ?> stream) {
		long result = Long.MAX_VALUE;
		var iter = streamWrappedSpliterators(stream).iterator();
		while (iter.hasNext()) {
			var size = iter.next().estimateSize();
			if (size < 0 || size >= Long.MAX_VALUE)
				continue;
			if (result == Long.MAX_VALUE)
				result = size;
			else
				result += size;
		}
		return result;
	}

	public static long getExactSizeIfKnown(BaseStream<?, ?> stream) {
		long result = -1;
		var iter = streamWrappedSpliterators(stream).iterator();
		while (iter.hasNext()) {
			var size = iter.next().getExactSizeIfKnown();
			if (size < 0)
				continue;
			if (result == -1)
				result = size;
			else
				result += size;
			if (result >= Long.MAX_VALUE)
				break;
		}
		return result;
	}

	@SuppressWarnings("rawtypes")
	private static final Accessor<? super AbstractStreamEx, Spliterator> BaseStreamEx_spliterator_ACCESSOR = MemberCache
			.getFieldAccessor(
					FieldRequest.of(AbstractStreamEx.class.getSuperclass(), Spliterator.class, "spliterator"));
	private static final Class<?> AbstractPipeline_CLASS_TYPE = CoreReflections
			.tryForName(BaseStream.class.getPackageName() + ".AbstractPipeline")
			.get();
	@SuppressWarnings("rawtypes")
	private static final Accessor AbstractPipeline_nextStage_ACCESSOR = MemberCache
			.getFieldAccessor(FieldRequest.of(AbstractPipeline_CLASS_TYPE, AbstractPipeline_CLASS_TYPE, "nextStage"));
	@SuppressWarnings("rawtypes")
	private static final Accessor AbstractPipeline_sourceSpliterator_ACCESSOR = MemberCache
			.getFieldAccessor(FieldRequest.of(AbstractPipeline_CLASS_TYPE, Spliterator.class, "sourceSpliterator"));

	protected static <T> Optional<Spliterator<T>> tryGetWrappedSpliterator(BaseStream<T, ?> stream) {
		if (!isStreamEx(stream))
			return Optional.empty();
		var streamEx = (AbstractStreamEx<T, ?>) stream;
		var wrapped = BaseStreamEx_spliterator_ACCESSOR.apply(streamEx);
		return Optional.ofNullable(wrapped);
	}

	protected static StreamEx<Spliterator<?>> streamWrappedSpliterators(BaseStream<?, ?> stream) {
		if (stream == null)
			return StreamEx.empty();
		var op = tryGetWrappedSpliterator(stream);
		if (op.isPresent())
			return Streams.<Spliterator<?>>of(op.get());
		UnaryOperator<Object> nextStageOperator = v -> {
			if (!AbstractPipeline_CLASS_TYPE.isInstance(v))
				return null;
			return AbstractPipeline_nextStage_ACCESSOR.apply(v);
		};
		var stageStream = StreamEx.<Object>iterate(nextStageOperator.apply(stream), Objects::nonNull,
				nextStageOperator);
		Function<Object, Spliterator<?>> spliteratorFunction = v -> {
			if (!AbstractPipeline_CLASS_TYPE.isInstance(v))
				return null;
			return (Spliterator<?>) AbstractPipeline_sourceSpliterator_ACCESSOR.apply(v);
		};
		return stageStream.map(spliteratorFunction).nonNull();
	}

	protected static <T> StreamEx<T> create(Spliterator<T> spliterator, BaseStream<?, ?> streamContextBaseStream) {
		Objects.requireNonNull(spliterator);
		var sc = Optional.ofNullable(streamContextBaseStream)
				.map(StreamContextAccessor::of)
				.orElseGet(StreamContextAccessor::SEQUENTIAL);
		return sc.applyToSpliterator(spliterator);
	}

}