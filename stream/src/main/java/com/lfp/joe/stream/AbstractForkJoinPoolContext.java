package com.lfp.joe.stream;

import java.util.Objects;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.BaseStream;

import org.immutables.value.Value;

import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.process.executor.CoreTasks;

@ValueLFP.Style
@Value.Immutable
abstract class AbstractForkJoinPoolContext implements AutoCloseable {

	public static enum CloseAction {
		NONE, SHUTDOWN, SHUTDOWN_NOW;
	}

	@Value.Parameter(order = 0)
	public abstract ForkJoinPool forkJoinPool();

	@Value.Parameter(order = 1)
	public abstract CloseAction closeAction();

	@Override
	public void close() {
		switch (closeAction()) {
		case NONE:
			break;
		case SHUTDOWN:
			forkJoinPool().shutdown();
			break;
		case SHUTDOWN_NOW:
			forkJoinPool().shutdownNow();
			break;
		default:
			throw new IllegalStateException();
		}
	}

	public static ForkJoinPoolContext create() {
		return ForkJoinPoolContext.of(CoreTasks.forkJoinPool(), CloseAction.SHUTDOWN_NOW);
	}

	public static ForkJoinPoolContext create(int parallelism) {
		return ForkJoinPoolContext.of(CoreTasks.forkJoinPool(parallelism), CloseAction.SHUTDOWN_NOW);
	}

	public static ForkJoinPoolContext from(BaseStream<?, ?> baseStream) {
		return from(StreamContextAccessor.of(baseStream));
	}

	public static ForkJoinPoolContext from(StreamContextAccessor streamContext) {
		Objects.requireNonNull(streamContext);
		var fjp = streamContext.tryGetForkJoinPool().orElse(null);
		if (fjp == null) {
			if (streamContext.isParallel())
				return ForkJoinPoolContext.of(ForkJoinPool.commonPool(), CloseAction.NONE);
			else
				return ForkJoinPoolContext.of(CoreTasks.forkJoinPool(), CloseAction.SHUTDOWN_NOW);
		} else
			return ForkJoinPoolContext.of(fjp, CloseAction.NONE);
	}
}
