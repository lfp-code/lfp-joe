package com.lfp.joe.stream;

import java.util.Objects;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.BaseStream;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.ConstructorRequest;
import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.ImmutableMemberCache.MethodRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.classpath.MemberCache.Accessor;
import com.lfp.joe.core.classpath.MemberCache.Instantiator;
import com.lfp.joe.core.classpath.MemberCache.Invoker;

import one.util.streamex.StreamEx;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class StreamContextAccessor {

	private static final Class CLASS_TYPE = CoreReflections
			.tryForName(StreamEx.class.getPackageName() + ".StreamContext")
			.get();

	private static final Instantiator<StreamEx> StreamEx_INSTANTIATOR = MemberCache
			.getConstructorInstantiator(ConstructorRequest.of(StreamEx.class, Spliterator.class, CLASS_TYPE));

	private static final Accessor<Object, Object> SEQUENTIAL_ACCESSOR = MemberCache
			.getFieldAccessor(FieldRequest.of(CLASS_TYPE, CLASS_TYPE, "SEQUENTIAL"));
	private static final Accessor<Object, Object> PARALLEL_ACCESSOR = MemberCache
			.getFieldAccessor(FieldRequest.of(CLASS_TYPE, CLASS_TYPE, "PARALLEL"));

	private static final Accessor<Object, Boolean> parallel_ACCESSOR = MemberCache
			.getFieldAccessor(FieldRequest.of(CLASS_TYPE, boolean.class, "parallel"));
	private static final Accessor<Object, ForkJoinPool> fjp_ACCESSOR = MemberCache
			.getFieldAccessor(FieldRequest.of(CLASS_TYPE, ForkJoinPool.class, "fjp"));
	private static final Accessor<Object, Runnable> closeHandler_ACCESSOR = MemberCache
			.getFieldAccessor(FieldRequest.of(CLASS_TYPE, Runnable.class, "closeHandler"));

	private static final Invoker<Object, Object> parallel_INVOKER = MemberCache
			.getMethodInvoker(MethodRequest.of(CLASS_TYPE, CLASS_TYPE, "parallel"));
	private static final Invoker<Object, Object> parallel_ForkJoinPool_INVOKER = MemberCache
			.getMethodInvoker(MethodRequest.of(CLASS_TYPE, CLASS_TYPE, "parallel", ForkJoinPool.class));
	private static final Invoker<Object, Object> sequentialINVOKER = MemberCache
			.getMethodInvoker(MethodRequest.of(CLASS_TYPE, CLASS_TYPE, "sequential"));

	private final Object streamContext;

	private StreamContextAccessor(Object streamContext) {
		super();
		this.streamContext = Objects.requireNonNull(streamContext);
	}

	public StreamContextAccessor parallel() {
		var streamContext = parallel_INVOKER.invoke(this.streamContext);
		return new StreamContextAccessor(streamContext);
	}

	public StreamContextAccessor parallel(ForkJoinPool fjp) {
		var streamContext = parallel_ForkJoinPool_INVOKER.invoke(this.streamContext);
		return new StreamContextAccessor(streamContext);
	}

	public StreamContextAccessor sequential() {
		var streamContext = sequentialINVOKER.invoke(this.streamContext);
		return new StreamContextAccessor(streamContext);
	}

	public boolean isParallel() {
		return parallel_ACCESSOR.apply(this.streamContext);
	}

	public Optional<ForkJoinPool> tryGetForkJoinPool() {
		return Optional.ofNullable(fjp_ACCESSOR.apply(this.streamContext));
	}

	public Optional<Runnable> tryGetCloseHandler() {
		return Optional.ofNullable(closeHandler_ACCESSOR.apply(this.streamContext));
	}

	public <U> StreamEx<U> createStream() {
		return applyToSpliterator(null);
	}

	public <U> StreamEx<U> applyToSpliterator(Spliterator<U> spliterator) {
		spliterator = Optional.ofNullable(spliterator).orElseGet(Spliterators::<U>emptySpliterator);
		return StreamEx_INSTANTIATOR.newInstance(spliterator, this.streamContext);
	}

	public <U> StreamEx<U> applyToStream(BaseStream<U, ?> baseStream) {
		if (baseStream == null)
			return createStream();
		var stream = Streams.of(baseStream);
		if (isParallel()) {
			if (tryGetForkJoinPool().isPresent())
				stream = stream.parallel(tryGetForkJoinPool().get());
			else
				stream = stream.parallel();
		} else
			stream = stream.sequential();
		if (tryGetCloseHandler().isPresent())
			stream = stream.onClose(tryGetCloseHandler().get());
		return stream;
	}

	@Override
	public int hashCode() {
		return Objects.hash(streamContext);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StreamContextAccessor other = (StreamContextAccessor) obj;
		return Objects.equals(streamContext, other.streamContext);
	}

	public static StreamContextAccessor of(BaseStream<?, ?> baseStream) {
		if (baseStream == null)
			return SEQUENTIAL();
		var streamContext = MemberCache.invokeMethod(MethodRequest.of(CLASS_TYPE, CLASS_TYPE, "of", BaseStream.class),
				null, baseStream);
		return new StreamContextAccessor(streamContext);
	}

	public static StreamContextAccessor SEQUENTIAL() {
		return new StreamContextAccessor(SEQUENTIAL_ACCESSOR.get());
	}

	public static StreamContextAccessor PARALLEL() {
		return new StreamContextAccessor(PARALLEL_ACCESSOR.get());
	}
}
