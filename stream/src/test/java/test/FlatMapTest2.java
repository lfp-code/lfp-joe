package test;

import java.util.function.Function;
import java.util.stream.IntStream;

import one.util.streamex.IntStreamEx;

public class FlatMapTest2 {

	public static void main(String[] args) {
		var streams = IntStreamEx.range(0, 10).boxed().map(index -> {
			var subStream = IntStream.range(0, 5).mapToObj(v -> {
				System.out.println(String.format("producing %s - %s", index, v));
				return index;
			}).onClose(() -> {
				System.err.println("closed:" + index);
			});
			return subStream;
		});
		var stream = streams.flatMap(Function.identity());
		var spliter = stream.spliterator();
		while (spliter.tryAdvance(v -> System.out.println(v))) {}
	}
}
