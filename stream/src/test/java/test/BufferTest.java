package test;

import java.time.Duration;

import com.lfp.joe.core.function.Throws;
import com.lfp.joe.stream.Streams;

import one.util.streamex.IntStreamEx;

public class BufferTest {

	public static void main(String[] args) {

		IntStreamEx.range(10_000).boxed().chain(Streams.buffer()).forEach(v -> {
			System.out.println(v.size());
		});
		IntStreamEx.range(100).boxed().map(v -> {
			Throws.unchecked(() -> Thread.sleep(50));
			return v;
		}).chain(Streams.buffer(100, Duration.ofSeconds(1))).forEach(v -> {
			System.out.println(v);
		});
	}
}
