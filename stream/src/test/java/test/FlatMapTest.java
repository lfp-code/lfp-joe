package test;

import java.util.stream.IntStream;

import com.lfp.joe.stream.Streams;

import one.util.streamex.IntStreamEx;

public class FlatMapTest {

	public static void main(String[] args) {
		var streams = IntStreamEx.range(0, 10).boxed().parallel().map(index -> {
			var subStream = IntStream.range(0, 5).map(v -> {
				System.out.println(String.format("producing %s - %s", index, v));
				return index;
			}).onClose(() -> {
				System.err.println("closed:" + index);
			});
			return subStream;
		});
		var stream = streams.chain(Streams.flatMap()).parallel();
		var iter = stream.iterator();
		var index = -1;
		while (iter.hasNext()) {
			index++;
			if (index == 10) {
				// stream.close();
			}
			System.out.println(iter.next());
		}
	}
}
