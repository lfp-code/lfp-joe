package com.lfp.joe.jwt.resolver;

import java.security.Key;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Predicate;

import javax.validation.constraints.NotNull;

import org.immutables.value.Value;
import org.immutables.value.Value.Style.BuilderVisibility;
import org.immutables.value.Value.Style.ImplementationVisibility;

import com.lfp.joe.jwt.claim.ClaimsLFP;
import com.lfp.joe.jwt.resolver.ImmutableAbstractSigningKeyResolver.SigningKeyRequest;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.impl.DefaultJwsHeader;

@SuppressWarnings({ "unchecked", "rawtypes" })
@Value.Enclosing
@Value.Style(typeAbstract = "*Def", visibility = ImplementationVisibility.PUBLIC, builderVisibility = BuilderVisibility.PUBLIC)
public abstract class AbstractSigningKeyResolver implements SigningKeyResolverLFP {

	@Override
	public Key resolveSigningKey(JwsHeader header, Claims claims) {
		var request = SigningKeyRequest.of(Optional.ofNullable(header).orElseGet(DefaultJwsHeader::new),
				ClaimsLFP.of(claims));
		if (!Optional.ofNullable(requestFilter()).map(v -> v.test(request)).orElse(true))
			return null;
		var keys = resolveSigningKeys(request);
		return Streams.of(keys).nonNull().findFirst().orElse(null);
	}

	protected abstract Predicate<? super SigningKeyRequest> requestFilter();

	protected abstract Iterable<? extends Key> resolveSigningKeys(@NotNull SigningKeyRequest request);

	@Value.Immutable
	static interface SigningKeyRequestDef {

		@Value.Parameter
		JwsHeader header();

		@Value.Parameter
		ClaimsLFP claims();

		@Value.Lazy
		default Bytes hash() {
			var md = Utils.Crypto.getMessageDigestMD5();
			BiConsumer<String, Map<String, ?>> hasher = (type, map) -> {
				var estream = EntryStreams.of(map)
						.nonNullKeys()
						.<Object>mapValues(Function.identity())
						.sortedBy(Entry::getKey);
				estream = estream.prepend(type, type);
				var i = -1;
				for (var ent : estream) {
					i++;
					Utils.Crypto.update(md, i);
					Utils.Crypto.update(md, ent.getKey());
					Utils.Crypto.update(md, ent.getValue());
				}
			};
			hasher.accept("header", header());
			hasher.accept("claims", claims());
			return Utils.Bits.from(md.digest());
		}

	}

}
