package com.lfp.joe.jwt.builder;

import java.time.Duration;

import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.MemberCache;

import io.jsonwebtoken.impl.DefaultJwtParserBuilder;

public class JwtParserBuilderLFP extends DefaultJwtParserBuilder {

	private static final Duration MAX_CLOCK_SKEW;
	static {
		long maxClockSkewMillis = MemberCache.getFieldValue(
				FieldRequest.of(DefaultJwtParserBuilder.class, long.class, "MAX_CLOCK_SKEW_MILLIS"), null);
		MAX_CLOCK_SKEW = Duration.ofMillis(maxClockSkewMillis);
	}

	private boolean disableSignatureValidation;

	public JwtParserBuilderLFP setAllowedClockSkewSecondsMax() {
		return (JwtParserBuilderLFP) super.setAllowedClockSkewSeconds(MAX_CLOCK_SKEW.getSeconds());
	}

	public JwtParserBuilderLFP setDisableSignatureValidation(boolean disableSignatureValidation) {
		this.disableSignatureValidation = disableSignatureValidation;
		return this;
	}

	@Override
	public JwtParserLFP build() {
		var jwtParser = super.build();
		return new JwtParserLFP(jwtParser, disableSignatureValidation);

	}

}
