package com.lfp.joe.jwt;

import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.time.StopWatch;

import com.google.gson.JsonParseException;
import com.google.gson.stream.MalformedJsonException;
import com.google.re2j.Pattern;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.jwt.builder.JwtParserBuilderLFP;
import com.lfp.joe.jwt.builder.JwtParserLFP;
import com.lfp.joe.jwt.resolver.ImmutableSigningKeyResolver;
import com.lfp.joe.jwt.resolver.IssuerJwkSigningKeyResolver;
import com.lfp.joe.jwt.resolver.SigningKeyRequestFilters;
import com.lfp.joe.jwt.token.JwkLFP;
import com.lfp.joe.jwt.token.JwsLFP;
import com.lfp.joe.jwt.token.JwtLFP;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.JwtParserBuilder;
import io.jsonwebtoken.SigningKeyResolver;
import io.jsonwebtoken.UnsupportedJwtException;

@SuppressWarnings("rawtypes")
public class Jwts {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String BEARER_PREFIX = "Bearer ";
	private static final int JWT_PARTS_LENGTH = 3;
	private static final Pattern JWT_PARTS_LENGTH_SPLIT_PATTERN = Pattern.compile(Pattern.quote("."));
	private static final Map<Boolean, JwtParserLFP> INSECURE_JWT_PARSERS = new ConcurrentHashMap<>();

	// *** secure parse
	public static Optional<JwsLFP> tryParse(String jwt, Key signingKey) {
		return tryParse(jwt, ImmutableSigningKeyResolver.builder().addAllKeys(Streams.ofNullable(signingKey)).build());
	}

	public static Optional<JwsLFP> tryParse(String jwt, Bytes signingKey) {
		return tryParse(jwt, ImmutableSigningKeyResolver.builder().addAllBytes(Streams.ofNullable(signingKey)).build());
	}

	public static Optional<JwsLFP> tryParse(String jwt, JwkLFP signingKey) {
		return tryParse(jwt, ImmutableSigningKeyResolver.builder().addAllJwks(Streams.ofNullable(signingKey)).build());
	}

	public static Optional<JwsLFP> tryParse(String jwt, SigningKeyResolver signingKeyResolver) {
		var jwtParser = Optional.ofNullable(signingKeyResolver)
				.map(new JwtParserBuilderLFP()::setSigningKeyResolver)
				.map(JwtParserBuilder::build)
				.orElse(null);
		return tryParse(jwt, jwtParser);
	}

	public static Optional<JwsLFP> tryParse(String jwt, JwtParser jwtParser) {
		if (jwtParser == null)
			return Optional.empty();
		return tryParseInternal(jwt, jwtFormatted -> {
			var jwtParserLFP = CoreReflections.tryCast(jwtParser, JwtParserLFP.class).orElseGet(() -> {
				return new JwtParserLFP(jwtParser);
			});
			return jwtParserLFP.parseClaimsJwsLFP(jwtFormatted);
		});
	}

	// *** insecure parse

	public static Optional<JwtLFP<? extends Header>> tryParseInsecure(String jwt) {
		return tryParseInsecure(jwt, false);
	}

	public static Optional<JwtLFP<? extends Header>> tryParseInsecure(String jwt, boolean disableExpirationValidation) {
		return tryParseInternal(jwt, jwtFormatted -> {
			var jwtParser = INSECURE_JWT_PARSERS.computeIfAbsent(disableExpirationValidation, nil -> {
				var jwtParserBuilder = new JwtParserBuilderLFP();
				jwtParserBuilder.setDisableSignatureValidation(true);
				if (disableExpirationValidation)
					jwtParserBuilder.setAllowedClockSkewSecondsMax();
				return jwtParserBuilder.build();
			});
			return jwtParser.parseClaimsJwtLFP(jwtFormatted);
		});
	}

	// *** private parse

	protected static <J extends JwtLFP<?>> Optional<J> tryParseInternal(String jwt,
			ThrowingFunction<String, J, ?> parseFunction) {
		Objects.requireNonNull(parseFunction);
		jwt = normalize(jwt).orElse(null);
		if (jwt == null)
			return Optional.empty();
		try {
			return Optional.ofNullable(parseFunction.apply(jwt));
		} catch (Throwable t) {
			suppressException(t);
			if (MachineConfig.isDeveloper() && t instanceof UnsupportedJwtException)
				LOGGER.warn("key error", t);
			return Optional.empty();
		}
	}

	// *** public utils

	public static boolean isJwt(String jwt) {
		return tryParseInsecure(jwt).isPresent();
	}

	public static boolean isJwtFormat(String jwt) {
		return normalize(jwt).isPresent();
	}

	// *** private utils

	private static Optional<String> normalize(String jwt) {
		jwt = Optional.ofNullable(jwt)
				.map(Utils.Strings::trimToNull)
				.map(v -> Utils.Strings.removeStartIgnoreCase(v, BEARER_PREFIX))
				.map(Utils.Strings::trimToNull)
				.orElse(null);
		if (jwt == null)
			return Optional.empty();
		var parts = JWT_PARTS_LENGTH_SPLIT_PATTERN.split(jwt, JWT_PARTS_LENGTH + 1);
		if (parts.length != JWT_PARTS_LENGTH)
			return Optional.empty();
		for (int i = 0; i < parts.length; i++) {
			var part = Utils.Strings.trim(parts[i]);
			if (i == parts.length - 1)
				// signature
				parts[i] = part;
			else {
				// not signature
				if (part.isEmpty())
					return Optional.empty();
				try {
					var bytes = Bytes.parseBase64(part);
					try (var is = bytes.inputStream();
							var isr = new InputStreamReader(is, MachineConfig.getDefaultCharset())) {
						var je = Serials.Gsons.getJsonParser().parse(isr);
						if (je == null || !je.isJsonObject())
							return Optional.empty();
						parts[i] = bytes.encodeBase64(true, false);
					}
				} catch (Exception e) {
					suppressException(e);
					return Optional.empty();
				}
			}
		}
		return Optional.of(Stream.of(parts).collect(Collectors.joining(".")));
	}

	private static void suppressException(Throwable t) throws RuntimeException {
		if (t instanceof IllegalArgumentException)
			return;
		if (t instanceof JsonParseException)
			return;
		if (t instanceof MalformedJsonException)
			return;
		if (t instanceof GeneralSecurityException)
			return;
		if (t instanceof JwtException)
			return;
		if (t instanceof RuntimeException)
			throw (RuntimeException) t;
		throw new RuntimeException(t);
	}

	public static void main(String[] args) {
		var token = " bearer 7b22616c67223a225253323536222c22747970223a224a5754222c226b6964223a22324776354670487554704867794746707272504762227d.7b2261757468302f726f6c65223a5b2264617368626f617264225d2c226e69636b6e616d65223a227265676769652e7069657263652b32222c226e616d65223a227265676769652e7069657263652b3240676d61696c2e636f6d222c2270696374757265223a2268747470733a2f2f732e67726176617461722e636f6d2f6176617461722f32343731383464353633303131373966366432396132356563623633323531303f733d34383026723d706726643d687474707325334125324625324663646e2e61757468302e636f6d2532466176617461727325324672652e706e67222c22757064617465645f6174223a22323032322d30382d30395431353a33313a31312e3138355a222c22656d61696c223a227265676769652e7069657263652b3240676d61696c2e636f6d222c22656d61696c5f7665726966696564223a747275652c22697373223a2268747470733a2f2f617574682e73796e63666c75782e636f2f222c22737562223a2261757468307c363265396163393361636266626633656665373364383465222c22617564223a22363779626764453456705868485a51636b5373646f776b6f703359454e32736a222c22696174223a313636303035393233332c22657870223a313636303039353233332c226e6f6e6365223a22654574464e6b6f3564556c514f55524452485532596b5645616d6b31646e4e72633031705a576c4e4e556452546c5a51566935445a544a4b4c673d3d227d.";
		var token2 = " bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjJHdjVGcEh1VHBIZ3lHRnByclBHYiJ9===   .eyJhdXRoMC9yb2xlIjpbImRhc2hib2FyZCJdLCJuaWNrbmFtZSI6InJlZ2dpZS5waWVyY2UrMiIsIm5hbWUiOiJyZWdnaWUucGllcmNlKzJAZ21haWwuY29tIiwicGljdHVyZSI6Imh0dHBzOi8vcy5ncmF2YXRhci5jb20vYXZhdGFyLzI0NzE4NGQ1NjMwMTE3OWY2ZDI5YTI1ZWNiNjMyNTEwP3M9NDgwJnI9cGcmZD1odHRwcyUzQSUyRiUyRmNkbi5hdXRoMC5jb20lMkZhdmF0YXJzJTJGcmUucG5nIiwidXBkYXRlZF9hdCI6IjIwMjItMDgtMDlUMTU6MzE6MTEuMTg1WiIsImVtYWlsIjoicmVnZ2llLnBpZXJjZSsyQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJpc3MiOiJodHRwczovL2F1dGguc3luY2ZsdXguY28vIiwic3ViIjoiYXV0aDB8NjJlOWFjOTNhY2JmYmYzZWZlNzNkODRlIiwiYXVkIjoiNjd5YmdkRTRWcFhoSFpRY2tTc2Rvd2tvcDNZRU4yc2oiLCJpYXQiOjE2NjAwNTkyMzMsImV4cCI6MTY2MDA5NTIzMywibm9uY2UiOiJlRXRGTmtvNWRVbFFPVVJEUkhVMllrVkVhbWsxZG5OcmMwMXBaV2xOTlVkUlRsWlFWaTVEWlRKS0xnPT0ifQ.A3j_94Q2rK60hzKvKezl30MtYhgyYAJ1_chFLIExvf4l9BdjzmtA17hdyNirdI4vITfjLP252vSrUhqjfoFixKviSsouIaAxvy91l0o4FKYvTIRLl1hW4N-j9XrF9vGXZGAUOof7iYv9JA58SFlqXWWVZL3bobODLNK5YOQgAjvO4Bo-tvDSPixAgmU7fKj60eIA9iV4Y_EJywqbs494gaPYP75KQ_Pv4W2yHrYl1DkiuDfQ-xVx386-wkcl_f2xFuUmgO00SG_Gy_hsi-x8VI5F8B0MJFZXnSt10_hXzJbfILGCoo-_LgMA7xQvqdX2zOz2XnZXEi5GoMx149iPXA";
		var token3 = "eyI";
		var verifier = IssuerJwkSigningKeyResolver.of(SigningKeyRequestFilters.issuerDomainEquals("iplasso.com"));
		System.out.println(Jwts.tryParse(token3, verifier).map(Jwt::getBody).orElse(null));
		System.out.println(Jwts.normalize(token));
		var jwt0 = Jwts.tryParseInsecure(token2, true).get();
		System.out.println(Serials.Gsons.getPretty().toJson(jwt0));
		var je = Jwts.tryParseInsecure(token2, true).get().getBody().readClaimData(Map.class);
		System.out.println(je);
		System.out.println(Jwts
				.isJwt("yJraWQiOiJ5Y21leTQ2azVjbDh0NnNneXg3MHMwOTlvIyJraWQiOiJ5Y21leTQ.2azVjbDh0NnNneXg3MHMwOTlvI."));
		String jwt = token2;
		SigningKeyResolver signingKeyResolver = IssuerJwkSigningKeyResolver
				.of(SigningKeyRequestFilters.issuerDomainEquals("iplasso.com"));
		JwtParser jwtParser = null;
		for (var build : Arrays.asList(false, true)) {
			if (jwtParser == null || build) {
				var jwtParserBuilder = new JwtParserBuilderLFP();
				jwtParserBuilder.setSigningKeyResolver(signingKeyResolver);
				jwtParser = jwtParserBuilder.build();
			}
			for (int i = 0; i < 10; i++) {
				var sw = StopWatch.createStarted();
				isJwtFormat(jwt);
				var normalizeTime = sw.getTime();
				sw.reset();
				sw.start();
				var jwsOp = Jwts.tryParseInsecure(jwt, true);
				var time = sw.getTime();
				System.out.println(String.format("%s %s %s %s %s", build, i, jwsOp.isPresent(), normalizeTime, time));
			}
		}
		System.exit(0);
	}

}
