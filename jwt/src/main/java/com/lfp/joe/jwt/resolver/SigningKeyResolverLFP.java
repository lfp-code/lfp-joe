package com.lfp.joe.jwt.resolver;

import java.security.Key;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

import com.google.common.reflect.TypeToken;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.jwt.claim.ClaimsLFP;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.utils.Utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.SigningKeyResolver;

@SuppressWarnings("rawtypes")
public interface SigningKeyResolverLFP extends SigningKeyResolver {

	@Override
	default Key resolveSigningKey(JwsHeader header, String plaintext) {
		plaintext = Utils.Strings.trimToNull(plaintext);
		Map<String, ?> claims;
		if (Utils.Strings.startsWith(plaintext, "{") && Utils.Strings.endsWith(plaintext, "}")) {
			var claimsRaw = Throws.attempt(plaintext, v -> {
				return Serials.Gsons.get().<Map<Object, Object>>fromJson(v, Const.CLAIMS_MAP_TT.getType());
			}).filter(Predicate.not(Map::isEmpty)).orElse(null);
			if (claimsRaw != null)
				claims = EntryStreams.of(claimsRaw).selectKeys(String.class).toCustomMap(LinkedHashMap::new);
			else
				claims = null;
		} else
			claims = null;
		return this.resolveSigningKey(header, ClaimsLFP.of(claims));
	}

	default SigningKeyResolverLFP append(SigningKeyResolver signingKeyResolver) {
		if (signingKeyResolver == null || signingKeyResolver == this)
			return this;
		var self = this;
		return new SigningKeyResolverLFP() {

			@Override
			public Key resolveSigningKey(JwsHeader header, String plaintext) {
				return Optional.ofNullable(self.resolveSigningKey(header, plaintext)).orElseGet(() -> {
					return signingKeyResolver.resolveSigningKey(header, plaintext);
				});
			}

			@Override
			public Key resolveSigningKey(JwsHeader header, Claims claims) {
				return Optional.ofNullable(self.resolveSigningKey(header, claims)).orElseGet(() -> {
					return signingKeyResolver.resolveSigningKey(header, claims);
				});
			}

		};
	}

	static enum Const {
		;

		private static final TypeToken<Map<Object, Object>> CLAIMS_MAP_TT = new TypeToken<Map<Object, Object>>() {};
	}

}
