package com.lfp.joe.jwt.resolver;

import java.net.URI;
import java.util.function.Predicate;
import java.util.function.Supplier;

import org.apache.commons.lang3.time.StopWatch;
import org.immutables.value.Value;

import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.jwt.Jwts;
import com.lfp.joe.jwt.builder.JwtBuilderLFP;
import com.lfp.joe.jwt.resolver.ImmutableAbstractSigningKeyResolver.SigningKeyRequest;
import com.lfp.joe.stream.Streams;

import io.jsonwebtoken.impl.DefaultJwsHeader;
import io.mikael.urlbuilder.UrlBuilder;
import one.util.streamex.StreamEx;

@ValueLFP.Style
@Value.Immutable
public abstract class AbstractIssuerJwkSigningKeyResolver extends AbstractURIJwkSigningKeyResolver {

	private static final String WELL_KNOWN_PATH = "/.well-known";
	private static final String JWKS_PATH = WELL_KNOWN_PATH + "/jwks.json";
	private static final String OPEN_ID_CONFIGURATION_PATH = WELL_KNOWN_PATH + "/openid-configuration";

	@Value.Parameter
	@Override
	public abstract Predicate<? super SigningKeyRequest> requestFilter();

	@Override
	protected Iterable<URI> resolveURIs(SigningKeyRequest request) {
		URI uri = request.claims().tryGetIssuerURI().orElse(null);
		if (uri == null)
			return null;
		Supplier<Iterable<URI>> additionalURIs = () -> {
			var urlb = UrlBuilder.fromUri(uri);
			return StreamEx.of(urlb.withPath(JWKS_PATH), urlb.withPath(OPEN_ID_CONFIGURATION_PATH))
					.map(UrlBuilder::toUri);
		};
		var uris = Streams.of(uri);
		if ("/".equals(uri.getPath()))
			uris = uris.prepend(Streams.of(additionalURIs));
		else
			uris = uris.append(Streams.of(additionalURIs));
		return uris;
	}

	public static void main(String[] args) {
		var resolver = IssuerJwkSigningKeyResolver.of(SigningKeyRequestFilters.issuerHostsEquals("asasdfsd.com")
				.or(SigningKeyRequestFilters.issuerDomainEquals("syncflux.co")));
		var jwtBuilder = new JwtBuilderLFP().setKeyId("2Gv5FpHuTpHgyGFprrPGb")
				.setIssuer("auth.syncflux.co")
				.claim("sup", "dood");
		var token = jwtBuilder.compact();
		for (int i = 0; i < 10; i++) {
			var sw = StopWatch.createStarted();
			var jwt = Jwts.tryParseInsecure(token).get();
			var jwk = resolver.resolveSigningKey(new DefaultJwsHeader(jwt.getHeader()), jwt.getBody());
			System.out.println(String.format("found:%s time:%s", jwk != null, sw.getTime()));
		}
	}
}
