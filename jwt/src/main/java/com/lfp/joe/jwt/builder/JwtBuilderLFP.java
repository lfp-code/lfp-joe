package com.lfp.joe.jwt.builder;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.Validate;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.jwt.claim.ClaimsLFP;
import com.lfp.joe.jwt.config.JwtConfig;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.impl.DefaultJwtBuilder;

public class JwtBuilderLFP extends DefaultJwtBuilder {

	private static final Field DefaultJwtBuilder_claims_FIELD = Throws
			.unchecked(() -> MemberCache.getField(FieldRequest.of(DefaultJwtBuilder.class, Claims.class, "claims")));
	private final GsonSerializerLFP<Map<String, ?>> gsonSerializer;

	public JwtBuilderLFP() {
		this(GsonSerializerLFP.getDefault());
	}

	public JwtBuilderLFP(GsonSerializerLFP<Map<String, ?>> gsonSerializer) {
		this.gsonSerializer = Objects.requireNonNull(gsonSerializer);
	}

	@Override
	public String compact() {
		var claims = getClaims(true);
		if (claims == null)
			return super.compact();
		if (claims.getIssuedAt() == null)
			claims.setIssuedAt(new Date());
		return super.compact();

	}

	public JwtBuilderLFP setKeyId(String keyId) {
		var name = Configs.get(JwtConfig.class).keyIdHeaderName();
		return (JwtBuilderLFP) this.setHeaderParam(name, keyId);
	}

	@Override
	protected ClaimsLFP ensureClaims() {
		return getClaims();
	}

	public ClaimsLFP getClaims() {
		return getClaims(false);
	}

	public ClaimsLFP getClaims(boolean disableCreate) {
		var claims = (Claims) Throws.unchecked(() -> DefaultJwtBuilder_claims_FIELD.get(this));
		if (claims == null && disableCreate)
			return null;
		if (claims instanceof ClaimsLFP)
			return (ClaimsLFP) claims;
		var claimsLFP = ClaimsLFP.of(claims);
		setClaims(claimsLFP);
		return claimsLFP;
	}

	@Override
	public JwtBuilderLFP setClaims(Claims claims) {
		return setClaims((Map<String, ?>) claims);
	}

	@Override
	public JwtBuilderLFP setClaims(Map<String, ?> claims) {
		Throws.unchecked(() -> {
			DefaultJwtBuilder_claims_FIELD.set(this, Optional.ofNullable(claims).map(ClaimsLFP::of).orElse(null));
		});
		return this;
	}

	public <U> JwtBuilderLFP putClaimData(U data) {
		return putClaimData(data, null);
	}

	@SuppressWarnings("unchecked")
	public <U> JwtBuilderLFP putClaimData(U data, Class<? super U> classType) {
		Objects.requireNonNull(data);
		classType = (Class<? super U>) Optional.ofNullable(classType).map(Object::getClass).get();
		JsonElement je = gsonSerializer.getGson().toJsonTree(data);
		if (je == null || je.isJsonNull())
			return this;
		Validate.isTrue(je.isJsonObject(), "data did not serialize to json object:{}", je);
		JsonObject jo = je.getAsJsonObject();
		for (String key : jo.keySet())
			getClaims().put(key, jo.get(key));
		getClaims().put(Serials.Gsons.CLASS_TYPE_FIELD_NAME, classType.getName());
		return this;
	}

	public static void main(String[] args) {
		var blder = new JwtBuilderLFP();
		blder.setIssuer("hithere");
		blder.setSubject("yoo");
		var rsa = Utils.Crypto.generateRSAKeyPair();

		System.out.println(blder.compact());

	}

}
