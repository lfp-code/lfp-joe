package com.lfp.joe.jwt.builder;

import java.security.Key;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

import com.lfp.joe.jwt.claim.ClaimsLFP;
import com.lfp.joe.jwt.token.JwsLFP;
import com.lfp.joe.jwt.token.JwtLFP;
import com.lfp.joe.utils.Utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Clock;
import io.jsonwebtoken.CompressionCodecResolver;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.JwtHandler;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SigningKeyResolver;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.io.Decoder;
import io.jsonwebtoken.io.Deserializer;
import io.jsonwebtoken.security.SignatureException;

public class JwtParserLFP implements JwtParser {

	private final JwtParser delegate;
	private final boolean disableSignatureValidation;

	public JwtParserLFP(JwtParser delegate) {
		this(delegate, false);
	}

	public JwtParserLFP(JwtParser delegate, boolean disableSignatureValidation) {
		super();
		this.delegate = Objects.requireNonNull(delegate);
		this.disableSignatureValidation = disableSignatureValidation;
	}

	@SuppressWarnings("rawtypes")
	public JwtLFP<Header> parseClaimsJwtLFP(final String claimsJwt) throws ExpiredJwtException, UnsupportedJwtException,
			MalformedJwtException, SignatureException, IllegalArgumentException {
		var parseClaimsJwt = claimsJwt;
		if (disableSignatureValidation) {
			var splitAt = Utils.Strings.lastIndexOf(claimsJwt, '.');
			if (splitAt >= 0)
				parseClaimsJwt = parseClaimsJwt.substring(0, splitAt + 1);
		}
		var jwt = parseClaimsJwt(parseClaimsJwt);
		if (jwt == null)
			return null;
		return new JwtLFP.Impl(claimsJwt, jwt.getHeader(), ClaimsLFP.of(jwt.getBody()));
	}

	public JwsLFP parseClaimsJwsLFP(final String claimsJws) throws ExpiredJwtException, UnsupportedJwtException,
			MalformedJwtException, SignatureException, IllegalArgumentException {
		var jws = parseClaimsJws(claimsJws);
		if (jws == null)
			return null;
		return new JwsLFP.Impl(claimsJws, jws.getHeader(), ClaimsLFP.of(jws.getBody()), jws.getSignature());
	}

	// **** DELEGATES *****

	/**
	 * @param id
	 * @return
	 * @deprecated
	 * @see io.jsonwebtoken.JwtParser#requireId(java.lang.String)
	 */
	@Deprecated
	@Override
	public JwtParser requireId(String id) {
		return delegate.requireId(id);
	}

	/**
	 * @param subject
	 * @return
	 * @deprecated
	 * @see io.jsonwebtoken.JwtParser#requireSubject(java.lang.String)
	 */
	@Deprecated
	@Override
	public JwtParser requireSubject(String subject) {
		return delegate.requireSubject(subject);
	}

	/**
	 * @param audience
	 * @return
	 * @deprecated
	 * @see io.jsonwebtoken.JwtParser#requireAudience(java.lang.String)
	 */
	@Deprecated
	@Override
	public JwtParser requireAudience(String audience) {
		return delegate.requireAudience(audience);
	}

	/**
	 * @param issuer
	 * @return
	 * @deprecated
	 * @see io.jsonwebtoken.JwtParser#requireIssuer(java.lang.String)
	 */
	@Deprecated
	@Override
	public JwtParser requireIssuer(String issuer) {
		return delegate.requireIssuer(issuer);
	}

	/**
	 * @param issuedAt
	 * @return
	 * @deprecated
	 * @see io.jsonwebtoken.JwtParser#requireIssuedAt(java.util.Date)
	 */
	@Deprecated
	@Override
	public JwtParser requireIssuedAt(Date issuedAt) {
		return delegate.requireIssuedAt(issuedAt);
	}

	/**
	 * @param expiration
	 * @return
	 * @deprecated
	 * @see io.jsonwebtoken.JwtParser#requireExpiration(java.util.Date)
	 */
	@Deprecated
	@Override
	public JwtParser requireExpiration(Date expiration) {
		return delegate.requireExpiration(expiration);
	}

	/**
	 * @param notBefore
	 * @return
	 * @deprecated
	 * @see io.jsonwebtoken.JwtParser#requireNotBefore(java.util.Date)
	 */
	@Deprecated
	@Override
	public JwtParser requireNotBefore(Date notBefore) {
		return delegate.requireNotBefore(notBefore);
	}

	/**
	 * @param claimName
	 * @param value
	 * @return
	 * @deprecated
	 * @see io.jsonwebtoken.JwtParser#require(java.lang.String, java.lang.Object)
	 */
	@Deprecated
	@Override
	public JwtParser require(String claimName, Object value) {
		return delegate.require(claimName, value);
	}

	/**
	 * @param clock
	 * @return
	 * @deprecated
	 * @see io.jsonwebtoken.JwtParser#setClock(io.jsonwebtoken.Clock)
	 */
	@Deprecated
	@Override
	public JwtParser setClock(Clock clock) {
		return delegate.setClock(clock);
	}

	/**
	 * @param seconds
	 * @return
	 * @throws IllegalArgumentException
	 * @deprecated
	 * @see io.jsonwebtoken.JwtParser#setAllowedClockSkewSeconds(long)
	 */
	@Deprecated
	@Override
	public JwtParser setAllowedClockSkewSeconds(long seconds) throws IllegalArgumentException {
		return delegate.setAllowedClockSkewSeconds(seconds);
	}

	/**
	 * @param key
	 * @return
	 * @deprecated
	 * @see io.jsonwebtoken.JwtParser#setSigningKey(byte[])
	 */
	@Deprecated
	@Override
	public JwtParser setSigningKey(byte[] key) {
		return delegate.setSigningKey(key);
	}

	/**
	 * @param base64EncodedSecretKey
	 * @return
	 * @deprecated
	 * @see io.jsonwebtoken.JwtParser#setSigningKey(java.lang.String)
	 */
	@Deprecated
	@Override
	public JwtParser setSigningKey(String base64EncodedSecretKey) {
		return delegate.setSigningKey(base64EncodedSecretKey);
	}

	/**
	 * @param key
	 * @return
	 * @deprecated
	 * @see io.jsonwebtoken.JwtParser#setSigningKey(java.security.Key)
	 */
	@Deprecated
	@Override
	public JwtParser setSigningKey(Key key) {
		return delegate.setSigningKey(key);
	}

	/**
	 * @param signingKeyResolver
	 * @return
	 * @deprecated
	 * @see io.jsonwebtoken.JwtParser#setSigningKeyResolver(io.jsonwebtoken.SigningKeyResolver)
	 */
	@Deprecated
	@Override
	public JwtParser setSigningKeyResolver(SigningKeyResolver signingKeyResolver) {
		return delegate.setSigningKeyResolver(signingKeyResolver);
	}

	/**
	 * @param compressionCodecResolver
	 * @return
	 * @deprecated
	 * @see io.jsonwebtoken.JwtParser#setCompressionCodecResolver(io.jsonwebtoken.CompressionCodecResolver)
	 */
	@Deprecated
	@Override
	public JwtParser setCompressionCodecResolver(CompressionCodecResolver compressionCodecResolver) {
		return delegate.setCompressionCodecResolver(compressionCodecResolver);
	}

	/**
	 * @param base64UrlDecoder
	 * @return
	 * @deprecated
	 * @see io.jsonwebtoken.JwtParser#base64UrlDecodeWith(io.jsonwebtoken.io.Decoder)
	 */
	@Deprecated
	@Override
	public JwtParser base64UrlDecodeWith(Decoder<String, byte[]> base64UrlDecoder) {
		return delegate.base64UrlDecodeWith(base64UrlDecoder);
	}

	/**
	 * @param deserializer
	 * @return
	 * @deprecated
	 * @see io.jsonwebtoken.JwtParser#deserializeJsonWith(io.jsonwebtoken.io.Deserializer)
	 */
	@Deprecated
	@Override
	public JwtParser deserializeJsonWith(Deserializer<Map<String, ?>> deserializer) {
		return delegate.deserializeJsonWith(deserializer);
	}

	/**
	 * @param jwt
	 * @return
	 * @see io.jsonwebtoken.JwtParser#isSigned(java.lang.String)
	 */
	@Override
	public boolean isSigned(String jwt) {
		return delegate.isSigned(jwt);
	}

	/**
	 * @param jwt
	 * @return
	 * @throws ExpiredJwtException
	 * @throws MalformedJwtException
	 * @throws SignatureException
	 * @throws IllegalArgumentException
	 * @see io.jsonwebtoken.JwtParser#parse(java.lang.String)
	 */
	@Override
	public Jwt parse(String jwt)
			throws ExpiredJwtException, MalformedJwtException, SignatureException, IllegalArgumentException {
		return delegate.parse(jwt);
	}

	/**
	 * @param <T>
	 * @param jwt
	 * @param handler
	 * @return
	 * @throws ExpiredJwtException
	 * @throws UnsupportedJwtException
	 * @throws MalformedJwtException
	 * @throws SignatureException
	 * @throws IllegalArgumentException
	 * @see io.jsonwebtoken.JwtParser#parse(java.lang.String,
	 *      io.jsonwebtoken.JwtHandler)
	 */
	@Override
	public <T> T parse(String jwt, JwtHandler<T> handler) throws ExpiredJwtException, UnsupportedJwtException,
			MalformedJwtException, SignatureException, IllegalArgumentException {
		return delegate.parse(jwt, handler);
	}

	/**
	 * @param plaintextJwt
	 * @return
	 * @throws UnsupportedJwtException
	 * @throws MalformedJwtException
	 * @throws SignatureException
	 * @throws IllegalArgumentException
	 * @see io.jsonwebtoken.JwtParser#parsePlaintextJwt(java.lang.String)
	 */
	@Override
	public Jwt<Header, String> parsePlaintextJwt(String plaintextJwt)
			throws UnsupportedJwtException, MalformedJwtException, SignatureException, IllegalArgumentException {
		return delegate.parsePlaintextJwt(plaintextJwt);
	}

	/**
	 * @param claimsJwt
	 * @return
	 * @throws ExpiredJwtException
	 * @throws UnsupportedJwtException
	 * @throws MalformedJwtException
	 * @throws SignatureException
	 * @throws IllegalArgumentException
	 * @see io.jsonwebtoken.JwtParser#parseClaimsJwt(java.lang.String)
	 */
	@Override
	public Jwt<Header, Claims> parseClaimsJwt(String claimsJwt) throws ExpiredJwtException, UnsupportedJwtException,
			MalformedJwtException, SignatureException, IllegalArgumentException {
		return delegate.parseClaimsJwt(claimsJwt);
	}

	/**
	 * @param plaintextJws
	 * @return
	 * @throws UnsupportedJwtException
	 * @throws MalformedJwtException
	 * @throws SignatureException
	 * @throws IllegalArgumentException
	 * @see io.jsonwebtoken.JwtParser#parsePlaintextJws(java.lang.String)
	 */
	@Override
	public Jws<String> parsePlaintextJws(String plaintextJws)
			throws UnsupportedJwtException, MalformedJwtException, SignatureException, IllegalArgumentException {
		return delegate.parsePlaintextJws(plaintextJws);
	}

	/**
	 * @param claimsJws
	 * @return
	 * @throws ExpiredJwtException
	 * @throws UnsupportedJwtException
	 * @throws MalformedJwtException
	 * @throws SignatureException
	 * @throws IllegalArgumentException
	 * @see io.jsonwebtoken.JwtParser#parseClaimsJws(java.lang.String)
	 */
	@Override
	public Jws<Claims> parseClaimsJws(String claimsJws) throws ExpiredJwtException, UnsupportedJwtException,
			MalformedJwtException, SignatureException, IllegalArgumentException {
		return delegate.parseClaimsJws(claimsJws);
	}

}
