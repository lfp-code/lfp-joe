package com.lfp.joe.jwt.token;

import java.math.BigInteger;
import java.security.AlgorithmParameters;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.ECParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.ECPublicKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Nullable;

import org.immutables.gson.Gson;
import org.immutables.value.Value;
import org.immutables.value.Value.Style.BuilderVisibility;
import org.immutables.value.Value.Style.ImplementationVisibility;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.serial.Serials;

@Gson.TypeAdapters
@Value.Immutable
@Value.Style(typeAbstract = "Abstract*", typeImmutable = "*", visibility = ImplementationVisibility.PUBLIC, builderVisibility = BuilderVisibility.PUBLIC)
abstract class AbstractJwkLFP {

	private static final String ALGORITHM_RSA = "RSA";
	private static final String ALGORITHM_ELLIPTIC_CURVE = "EC";
	private static final String ELLIPTIC_CURVE_TYPE_P256 = "P-256";
	private static final String ELLIPTIC_CURVE_TYPE_P384 = "P-384";
	private static final String ELLIPTIC_CURVE_TYPE_P521 = "P-521";

	/**
	 * Creates a new Jwk
	 *
	 * @param id                    kid
	 * @param type                  kty
	 * @param algorithm             alg
	 * @param usage                 use
	 * @param operations            key_ops
	 * @param certificateUrl        x5u
	 * @param certificateChain      x5c
	 * @param certificateThumbprint x5t
	 * @param additionalAttributes  additional attributes not part of the standard
	 *                              ones
	 */

	@Nullable
	@SerializedName("kid")
	public abstract String getId();

	@Nullable
	@SerializedName("kty")
	public abstract String getType();

	@Nullable
	@SerializedName("alg")
	public abstract String getAlgorithm();

	@Nullable
	@SerializedName("use")
	public abstract String getUsage();

	@SerializedName("key_ops")
	public abstract List<String> getOperations();

	@Nullable
	@SerializedName("x5u")
	public abstract String getCertificateUrl();

	@SerializedName("x5c")
	public abstract List<String> getCertificateChain();

	@Nullable
	@SerializedName("x5t")
	public abstract String getCertificateThumbprint();

	public abstract Map<String, Object> getAdditionalAttributes();

	@Value.Lazy
	public PublicKey getPublicKey() throws InvalidKeyException {
		PublicKey publicKey = null;
		var type = getType();
		switch (type) {
		case ALGORITHM_RSA:
			try {
				KeyFactory kf = KeyFactory.getInstance(ALGORITHM_RSA);
				BigInteger modulus = new BigInteger(1,
						Base64.getUrlDecoder().decode(getAdditionalAttributeAsString("n")));
				BigInteger exponent = new BigInteger(1,
						Base64.getUrlDecoder().decode(getAdditionalAttributeAsString("e")));
				publicKey = kf.generatePublic(new RSAPublicKeySpec(modulus, exponent));
			} catch (InvalidKeySpecException e) {
				throw new InvalidKeyException("Invalid public key", e);
			} catch (NoSuchAlgorithmException e) {
				throw new InvalidKeyException("Invalid algorithm to generate key", e);
			}
			break;
		case ALGORITHM_ELLIPTIC_CURVE:
			try {
				KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM_ELLIPTIC_CURVE);
				ECPoint ecPoint = new ECPoint(
						new BigInteger(Base64.getUrlDecoder().decode(getAdditionalAttributeAsString("x"))),
						new BigInteger(Base64.getUrlDecoder().decode(getAdditionalAttributeAsString("y"))));
				AlgorithmParameters algorithmParameters = AlgorithmParameters.getInstance(ALGORITHM_ELLIPTIC_CURVE);

				String curve = getAdditionalAttributeAsString("crv");
				switch (curve) {
				case ELLIPTIC_CURVE_TYPE_P256:
					algorithmParameters.init(new ECGenParameterSpec("secp256r1"));
					break;
				case ELLIPTIC_CURVE_TYPE_P384:
					algorithmParameters.init(new ECGenParameterSpec("secp384r1"));
					break;
				case ELLIPTIC_CURVE_TYPE_P521:
					algorithmParameters.init(new ECGenParameterSpec("secp521r1"));
					break;
				default:
					throw new InvalidKeyException("Invalid or unsupported curve type " + curve);
				}
				ECParameterSpec ecParameterSpec = algorithmParameters.getParameterSpec(ECParameterSpec.class);
				ECPublicKeySpec ecPublicKeySpec = new ECPublicKeySpec(ecPoint, ecParameterSpec);
				publicKey = keyFactory.generatePublic(ecPublicKeySpec);
			} catch (NoSuchAlgorithmException e) {
				throw new InvalidKeyException("Invalid algorithm to generate key", e);
			} catch (InvalidKeySpecException | InvalidParameterSpecException e) {
				throw new InvalidKeyException("Invalid public key", e);
			}
			break;

		default:
			throw new InvalidKeyException("The key type of " + type + " is not supported");
		}
		return publicKey;
	}

	protected String getAdditionalAttributeAsString(String key) {
		return getAdditionalAttribute(key, String.class);
	}

	protected <X> X getAdditionalAttribute(String key, Class<X> classType) {
		return Optional.ofNullable(getAdditionalAttributes())
				.map(v -> v.get(key))
				.flatMap(v -> CoreReflections.tryCast(v, classType))
				.orElse(null);

	}

	public static void main(String[] args) throws InvalidKeyException {
		var jwk = JwkLFP.builder()
				.id("123")
				.type("cool")
				.algorithm("ec")
				.usage("ok")
				.certificateUrl("123")
				.certificateThumbprint("ok")
				.putAdditionalAttributes("wow", "neat")
				.addCertificateChain("cert chain")
				.build();
		var json = Serials.Gsons.getPretty().toJson(jwk);
		System.out.println(json);
		json = "{\n" + "\"alg\": \"RS256\",\n" + "\"kty\": \"RSA\",\n" + "\"use\": \"sig\",\n"
				+ "\"n\": \"uJc5JpyDWg9qWtRjhUrivF_hKQ_orMeDNa5EecE6zDNb8RLkMjo4mBHghP3yqdM53DOui6XdecGfXbo12mYMOEmJO1sU9i7wBkVdJJN8E2RJecImdiNMh7zRQ8qdladhHGASv4hmAZMRypajvliaSbMa3F1VI6uOqPhE8cZpDJvjcsHgGfAWw29cRkIWVkKVEOyu11jwHiqSbZLSCuqQiTMe2JpbNBhd2LDJmZ7SAq7o1WcB9igyQoU8-5Oj7y3aJoQOHhdJM4hs6SFHKvs563YzMWDiSXaCpI5hkRgAmXdt782GMwwNrL5g4VDh119PXVWtQB64TbHmwqlCKXHBAQ\",\n"
				+ "\"e\": \"AQAB\",\n" + "\"kid\": \"2Gv5FpHuTpHgyGFprrPGb\",\n"
				+ "\"x5t\": \"-cEwx09y3fwAeDboa5FKbiwajQM\",\n" + "\"x5c\": [\n"
				+ "\"MIIDDTCCAfWgAwIBAgIJCwLjDisDkKqLMA0GCSqGSIb3DQEBCwUAMCQxIjAgBgNVBAMTGWRldi1yZWRqZHBwYS51cy5hdXRoMC5jb20wHhcNMjIwODAxMTU0ODIzWhcNMzYwNDA5MTU0ODIzWjAkMSIwIAYDVQQDExlkZXYtcmVkamRwcGEudXMuYXV0aDAuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuJc5JpyDWg9qWtRjhUrivF/hKQ/orMeDNa5EecE6zDNb8RLkMjo4mBHghP3yqdM53DOui6XdecGfXbo12mYMOEmJO1sU9i7wBkVdJJN8E2RJecImdiNMh7zRQ8qdladhHGASv4hmAZMRypajvliaSbMa3F1VI6uOqPhE8cZpDJvjcsHgGfAWw29cRkIWVkKVEOyu11jwHiqSbZLSCuqQiTMe2JpbNBhd2LDJmZ7SAq7o1WcB9igyQoU8+5Oj7y3aJoQOHhdJM4hs6SFHKvs563YzMWDiSXaCpI5hkRgAmXdt782GMwwNrL5g4VDh119PXVWtQB64TbHmwqlCKXHBAQIDAQABo0IwQDAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBTZk7IwhyRoOTOuWWYqzKi7i7HGxjAOBgNVHQ8BAf8EBAMCAoQwDQYJKoZIhvcNAQELBQADggEBAByFLh3a4q9S5fzZlOUuv9j6NuVo6EjirefGfEfkXgf3qXqIOnP0p8VJFQ09YAjw95C6/3x5CBM5lVqH8BNXoONGsLjBbhUaCXXm7E84LRaOGMfqcgjrJinmv33ZoGEHVkyGijoKOv5I7FN0jRdfStQ2jv3bA1GtYG7QVutfhNVfi4NwTS8LzNl9e/GWybo8b1FXNCsgqlj5J2ZWwDPUt9Bs4xD0Qrks6QmFlLWDF0HKfN+uzQnDhgwSxnpozQd/2q7roO0PJk+zV+6hzfac86Hktibn7ktC4xMMWHSbY0n6vXZVJx5m1DFTRGWyimAVEXGNEejIMudOvoB6T+b1Zlo=\"\n"
				+ "]\n" + "}";
		var jsonElement = Serials.Gsons.get().fromJson(json, JsonElement.class);
		System.out.println(Serials.Gsons.getPretty().toJson(jsonElement));
		var jwk2 = Serials.Gsons.getPretty().fromJson(json, JwkLFP.class);
		System.out.println(jwk2);
		System.out.println(jwk2.getPublicKey());
	}

}