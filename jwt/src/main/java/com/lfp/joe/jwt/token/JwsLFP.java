package com.lfp.joe.jwt.token;

import com.github.jknack.handlebars.internal.lang3.Validate;
import com.lfp.joe.jwt.claim.ClaimsLFP;

import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.impl.DefaultJws;

@SuppressWarnings("rawtypes")
public interface JwsLFP extends JwtLFP<JwsHeader>, Jws<ClaimsLFP> {

	public static class Impl extends DefaultJws<ClaimsLFP> implements JwsLFP {
		private final String jwt;

		public Impl(String jwt, JwsHeader header, ClaimsLFP body, String signature) {
			super(header, body, signature);
			this.jwt = Validate.notBlank(jwt);
		}

		@Override
		public String getJwt() {
			return jwt;
		}

	}

}
