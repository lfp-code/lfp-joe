package com.lfp.joe.jwt.token;

import com.github.jknack.handlebars.internal.lang3.Validate;
import com.lfp.joe.jwt.claim.ClaimsLFP;

import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.impl.DefaultJwt;

@SuppressWarnings({ "rawtypes" })
public interface JwtLFP<H extends Header> extends Jwt<H, ClaimsLFP> {

	String getJwt();

	public static class Impl extends DefaultJwt<ClaimsLFP> implements JwtLFP<Header> {

		private final String jwt;

		public Impl(String jwt, Header header, ClaimsLFP body) {
			super(header, body);
			this.jwt = Validate.notBlank(jwt);
		}

		@Override
		public String getJwt() {
			return jwt;
		}

	}
}
