package com.lfp.joe.jwt.resolver;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Duration;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.gson.JsonElement;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.jwt.resolver.ImmutableAbstractSigningKeyResolver.SigningKeyRequest;
import com.lfp.joe.jwt.token.JwkLFP;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.request.HttpRequests;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.media.MediaTypes;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.Streams;

public abstract class AbstractURIJwkSigningKeyResolver extends AbstractJwkSigningKeyResolver {

	private static final String OPEN_ID_CONFIGURATION_JWKS_URI_KEY = "jwks_uri";
	private static final String JWK_SET_KEYS_KEY = "keys";
	private static final ThrowingFunction<URI, List<JwkLFP>, Exception> CACHE_LOADER = uri -> {
		return Streams.<JwkLFP>of(resolveJwks(uri, null)).nonNull().toImmutableList();
	};
	private static final LoadingCache<URI, List<JwkLFP>> CACHE_DEFAULT = Caffeine.newBuilder()
			.expireAfterWrite(Duration.ofMinutes(1))
			.refreshAfterWrite(Duration.ofSeconds(15))
			.executor(CoreTasks.executor())
			.build(CACHE_LOADER::apply);
	private Cache<URI, List<JwkLFP>> cache = CACHE_DEFAULT;

	public void setCache(Cache<URI, List<JwkLFP>> cache) {
		this.cache = cache;
	}

	@Override
	protected Iterable<? extends JwkLFP> resolveJwks(SigningKeyRequest request) {
		var uris = Streams.of(resolveURIs(request)).nonNull().distinct();
		return uris.map(uri -> resolveJwks(request, uri)).flatMap(Streams::of);

	}

	protected Iterable<? extends JwkLFP> resolveJwks(SigningKeyRequest request, URI uri) {
		uri = Optional.ofNullable(uri).map(URIs::normalize).orElse(null);
		if (uri == null)
			return null;
		var cache = this.cache;
		if (cache == null)
			return resolveJwks(uri, null);
		if (cache instanceof LoadingCache)
			return ((LoadingCache<URI, List<JwkLFP>>) cache).get(uri);
		return cache.get(uri, CACHE_LOADER.unchecked());
	}

	protected abstract Iterable<URI> resolveURIs(SigningKeyRequest request);

	protected static Iterable<? extends JwkLFP> resolveJwks(URI uri,
			ThrowingFunction<URI, HttpResponse<InputStream>, IOException> httpFunction) {
		if (httpFunction == null)
			return resolveJwks(uri, v -> {
				try {
					return HttpClients.get().send(HttpRequests.request().uri(v).build(), BodyHandlers.ofInputStream());
				} catch (InterruptedException e) {
					throw Throws.unchecked(e);
				}
			});
		return resolveJwks(uri, httpFunction, new HashSet<>());
	}

	protected static Iterable<? extends JwkLFP> resolveJwks(URI uri,
			ThrowingFunction<URI, HttpResponse<InputStream>, IOException> httpFunction, Set<URI> visitedURIs) {
		if (uri == null || !visitedURIs.add(uri))
			return null;
		JsonElement jsonResponse;
		try {
			HttpResponse<InputStream> response = httpFunction.apply(uri);
			try (var is = response.body()) {
				if (!StatusCodes.isSuccess(response.statusCode()))
					return null;
				var mediaType = MediaTypes.tryParse(HeaderMap.of(response.headers())).orElse(null);
				if (mediaType != null && (!"application".equalsIgnoreCase(mediaType.type())
						|| !"json".equalsIgnoreCase(mediaType.subtype())))
					return null;
				try (var isr = new InputStreamReader(is)) {
					jsonResponse = Throws.attempt(() -> Serials.Gsons.getJsonParser().parse(isr)).orElse(null);
				}
			}
		} catch (IOException e) {
			// suppress
			return null;
		}
		if (jsonResponse == null || !jsonResponse.isJsonObject())
			return null;
		var jwkIter = Streams.of(resolveJwks(jsonResponse)).nonNull().iterator();
		if (jwkIter.hasNext())
			return Streams.of(jwkIter);
		var jwksURI = Serials.Gsons.tryGetAsString(jsonResponse, OPEN_ID_CONFIGURATION_JWKS_URI_KEY)
				.flatMap(URIs::parse)
				.orElse(null);
		return resolveJwks(jwksURI, httpFunction, visitedURIs);
	}

	protected static Iterable<? extends JwkLFP> resolveJwks(JsonElement jsonResponse) {
		if (jsonResponse == null)
			return null;
		var jwkIter = Serials.Gsons.tryGetAsJsonArray(jsonResponse).stream().flatMap(Streams::of).map(v -> {
			return Throws.attempt(() -> Serials.Gsons.get().fromJson(v, JwkLFP.class)).orElse(null);
		}).filter(Objects::nonNull).iterator();
		if (jwkIter.hasNext())
			return Streams.of(jwkIter);
		return resolveJwks(Serials.Gsons.tryGet(jsonResponse, JWK_SET_KEYS_KEY).orElse(null));
	}

}
