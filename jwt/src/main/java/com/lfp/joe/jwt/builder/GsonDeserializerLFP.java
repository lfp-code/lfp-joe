package com.lfp.joe.jwt.builder;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.immutables.value.Value;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.jwt.builder.ImmutableGsonDeserializerLFP.GsonDeserializerLFPOptions;
import com.lfp.joe.serial.Serials;

import io.jsonwebtoken.io.DeserializationException;
import io.jsonwebtoken.io.Deserializer;

@ValueLFP.Style
@Value.Enclosing
@SuppressWarnings({ "serial" })
public class GsonDeserializerLFP<T> implements Deserializer<T> {

	private static final GsonDeserializerLFP<Map<String, ?>> DEFAULT_INSTANCE;
	static {
		var tt = new TypeToken<Map<String, ?>>() {};
		DEFAULT_INSTANCE = new GsonDeserializerLFP<>(
				GsonDeserializerLFPOptions.<Map<String, ?>>builder().typeToken(tt).build());
	}

	public static GsonDeserializerLFP<Map<String, ?>> getDefault() {
		return DEFAULT_INSTANCE;
	}

	private final GsonDeserializerLFPOptions<T> options;

	public GsonDeserializerLFP(GsonDeserializerLFPOptions<T> options) {
		this.options = Objects.requireNonNull(options);
	}

	@Override
	public T deserialize(byte[] bytes) throws DeserializationException {
		try (var is = new ByteArrayInputStream(bytes);
				var isr = new InputStreamReader(is, MachineConfig.getDefaultCharset());) {
			return options.gson().orElseGet(Serials.Gsons::get).fromJson(isr, options.type());
		} catch (Throwable t) {
			if (t instanceof DeserializationException)
				throw (DeserializationException) t;
			var value = new String(bytes, MachineConfig.getDefaultCharset());
			var message = String.format("value:%s", value);
			throw new DeserializationException(message, t);
		}

	}

	@Value.Immutable
	public static abstract class AbstractGsonDeserializerLFPOptions<T> {

		abstract Optional<Gson> gson();

		abstract TypeToken<T> typeToken();

		@Value.Lazy
		Type type() {
			return typeToken().getType();
		}

	}

}
