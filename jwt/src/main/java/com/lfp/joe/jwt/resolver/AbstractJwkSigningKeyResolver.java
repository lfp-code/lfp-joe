package com.lfp.joe.jwt.resolver;

import java.security.Key;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import com.lfp.joe.core.function.Throws;
import com.lfp.joe.jwt.resolver.ImmutableAbstractSigningKeyResolver.SigningKeyRequest;
import com.lfp.joe.jwt.token.JwkLFP;
import com.lfp.joe.stream.Streams;

import one.util.streamex.StreamEx;

public abstract class AbstractJwkSigningKeyResolver extends AbstractSigningKeyResolver {

	@Override
	protected Iterable<? extends Key> resolveSigningKeys(@NotNull SigningKeyRequest request) {
		return Streams.<JwkLFP>of(resolveJwks(request)).nonNull().chain(s -> {
			return Streams.of(filterJwks(request, s));
		}).map(Throws.functionUnchecked(JwkLFP::getPublicKey));
	}

	protected Iterable<? extends JwkLFP> filterJwks(@NotNull SigningKeyRequest request, StreamEx<JwkLFP> jwks) {
		return jwks.filter(v -> Objects.equals(v.getId(), request.header().getKeyId()));
	}

	protected abstract Iterable<? extends JwkLFP> resolveJwks(SigningKeyRequest request);

}
