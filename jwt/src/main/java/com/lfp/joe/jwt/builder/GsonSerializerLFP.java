package com.lfp.joe.jwt.builder;

import java.util.Map;

import com.google.gson.Gson;
import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.serial.Serials;

import io.jsonwebtoken.gson.io.GsonSerializer;

public class GsonSerializerLFP<T> extends GsonSerializer<T> {

	private static final GsonSerializerLFP<Map<String, ?>> DEFAULT_INSTANCE = new GsonSerializerLFP<>();

	public static GsonSerializerLFP<Map<String, ?>> getDefault() {
		return DEFAULT_INSTANCE;
	}

	public GsonSerializerLFP() {
		this(Serials.Gsons.get());
	}

	public GsonSerializerLFP(Gson gson) {
		super(gson);
	}

	public Gson getGson() {
		return MemberCache.getFieldValue(FieldRequest.of(GsonSerializer.class, Gson.class, "gson"), this);
	}
}
