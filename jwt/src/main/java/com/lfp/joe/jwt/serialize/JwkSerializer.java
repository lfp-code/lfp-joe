package com.lfp.joe.jwt.serialize;

import java.io.IOException;
import java.lang.reflect.AccessibleObject;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.lfp.joe.jwt.token.JwkLFP;
import com.lfp.joe.serial.serializer.Serializer;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

@Serializer
public enum JwkSerializer implements TypeAdapterFactory {
	INSTANCE;

	private static final TypeToken<Map<String, Object>> MAP_TT = new TypeToken<Map<String, Object>>() {};
	private static final List<String> ROOT_FIELD_NAMES = Streams.of(JwkLFP.class, JwkLFP.class.getSuperclass())
			.flatMap(v -> Streams.<AccessibleObject>of(v.getMethods()).append(v.getFields()))
			.map(v -> v.getAnnotation(SerializedName.class))
			.nonNull()
			.map(SerializedName::value)
			.filter(Utils.Strings::isNotBlank)
			.distinct()
			.toImmutableList();

	@SuppressWarnings("unchecked")
	@Override
	public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
		if (!Optional.ofNullable(type).map(TypeToken::getType).map(JwkLFP.class::equals).orElse(false))
			return null;
		TypeAdapter<JwkLFP> delegate = (TypeAdapter<JwkLFP>) gson.getDelegateAdapter(this, type);
		return (TypeAdapter<T>) new TypeAdapter<JwkLFP>() {

			@Override
			public void write(JsonWriter out, JwkLFP jwk) throws IOException {
				var je = delegate.toJsonTree(jwk);
				if (je != null && je.isJsonObject()) {
					var root = je.getAsJsonObject();
					var keyIter = root.keySet().iterator();
					while (keyIter.hasNext()) {
						var key = keyIter.next();
						var value = root.get(key);
						if (value instanceof JsonArray && ((JsonArray) value).size() == 0) {
							keyIter.remove();
							continue;
						}
						if (value instanceof JsonObject && !ROOT_FIELD_NAMES.contains(key)) {
							keyIter.remove();
							var sub = (JsonObject) value;
							for (var name : sub.keySet())
								if (!root.has(name))
									root.add(name, sub.get(name));
						}
					}
				}
				gson.getAdapter(JsonElement.class).write(out, je);
			}

			@Override
			public JwkLFP read(JsonReader in) throws IOException {
				var je = gson.getAdapter(JsonElement.class).read(in);
				var jwk = delegate.fromJsonTree(je);
				if (jwk != null && je != null && je.isJsonObject()) {
					var root = je.getAsJsonObject();
					ROOT_FIELD_NAMES.stream().filter(root::has).forEach(root::remove);
					Map<String, Object> map = gson.fromJson(root, MAP_TT.getType());
					jwk = jwk.withAdditionalAttributes(map);
				}
				return jwk;
			}
		};
	}

	public static void main(String[] args) {
		System.out.println(ROOT_FIELD_NAMES);
	}
}
