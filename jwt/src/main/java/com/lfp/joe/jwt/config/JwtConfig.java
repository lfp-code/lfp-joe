package com.lfp.joe.jwt.config;

import java.time.Duration;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.properties.converter.DurationConverter;

public interface JwtConfig extends Config {

	@ConverterClass(DurationConverter.class)
	@DefaultValue("15 seconds")
	Duration defaultIssuerClaimSigningKeyResolverCacheExpireAfterWrite();

	@ConverterClass(DurationConverter.class)
	@DefaultValue("10 seconds")
	Duration defaultIssuerClaimSigningKeyResolverCacheExpireAfterAccess();

	@ConverterClass(DurationConverter.class)
	@DefaultValue("5 seconds")
	Duration defaultIssuerClaimSigningKeyResolverCacheRefreshAfterWrite();

	@DefaultValue("/.well-known")
	String wellKnownPath();

	@DefaultValue("${wellKnownPath}/openid-configuration")
	String wellKnownOpenIDConfigurationPath();

	@DefaultValue("${wellKnownPath}/jwks.json")
	String wellKnownJwkSetPath();

	@DefaultValue("jwks_uri")
	String openIDConfigurationKey_jwks_uri();

	@DefaultValue("kid")
	String keyIdHeaderName();

	@DefaultValue("iss")
	String issuerClaimName();

	@DefaultValue("sub")
	String subjectClaimName();

	@DefaultValue("aud")
	String audienceClaimName();

	@DefaultValue("exp")
	String expirationClaimName();

	@DefaultValue("nbf")
	String notBeforeClaimName();

	@DefaultValue("iat")
	String issuedAtClaimName();

	@DefaultValue("jti")
	String jwtIdClaimName();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.builder().withPrependClassNames(false).withJsonOutput(true).build());
	}
}
