package com.lfp.joe.jwt.claim;

import java.net.URI;
import java.time.Duration;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import org.immutables.value.Value;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonNull;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.net.dns.TLDs;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import io.jsonwebtoken.impl.DefaultClaims;
import io.mikael.urlbuilder.UrlBuilder;

public class ClaimsLFP extends DefaultClaims {

	public static ClaimsLFP create() {
		return of(null);
	}

	public static ClaimsLFP of(Map<String, ?> map) {
		if (map instanceof ClaimsLFP)
			return (ClaimsLFP) map;
		return new ClaimsLFP(map);
	}

	protected ClaimsLFP(Map<String, ?> map) {
		super(Optional.ofNullable(map).orElseGet(Map::of));
	}

	public Optional<Date> tryGetExpiration() {
		return Optional.ofNullable(this.getExpiration());
	}

	public Optional<Duration> tryGetTimeToLive() {
		return Optional.ofNullable(this.getExpiration()).map(v -> {
			var ttlMillis = System.currentTimeMillis() - v.getTime();
			return Duration.ofMillis(Math.max(0, ttlMillis));
		});
	}

	public boolean isExpired() {
		return tryGetTimeToLive().filter(Duration::isZero).isPresent();
	}

	private transient Optional<URI> _issuerURI;

	@Value.Lazy
	public Optional<URI> tryGetIssuerURI() {
		if (_issuerURI == null)
			_issuerURI = parseURI(getIssuer(), MachineConfig.isDeveloper());
		return _issuerURI;
	}

	public boolean issuerHostEquals(String host) {
		return Utils.Strings.equalsIgnoreCase(tryGetIssuerURI().map(URI::getHost).orElse(null), host);
	}

	public boolean issuerDomainEquals(String domain) {
		return TLDs.domainEquals(tryGetIssuerURI().orElse(null), domain);
	}

	public boolean issuerURIMatches(String urlPrefix) {
		return issuerURIMatches(parseURI(urlPrefix).orElse(null));
	}

	public boolean issuerURIMatches(URI uriPrefix) {
		if (uriPrefix == null)
			return false;
		var issuerURI = tryGetIssuerURI().orElse(null);
		if (issuerURI == null)
			return false;
		uriPrefix = URIs.normalize(uriPrefix);
		Function<URI, String> schemeAndHostGetter = v -> {
			return UrlBuilder.fromUri(v).withPath("/").toString();
		};
		if (!schemeAndHostGetter.apply(issuerURI).equalsIgnoreCase(schemeAndHostGetter.apply(uriPrefix)))
			return false;
		return URIs.pathPrefixMatch(uriPrefix.getPath(), issuerURI.getPath());
	}

	public Optional<Class<?>> tryGetClassType() {
		return Optional.ofNullable(get(Serials.Gsons.CLASS_TYPE_FIELD_NAME))
				.flatMap(v -> CoreReflections.tryCast(v, String.class))
				.flatMap(JavaCode.Reflections::tryForName);
	}

	public <X> X readClaimData() {
		return readClaimData((Gson) null);
	}

	public <X> X readClaimData(Class<X> classType) {
		return readClaimData(null, classType);
	}

	public <X> X readClaimData(TypeToken<X> typeToken) {
		return readClaimData(null, typeToken);
	}

	@SuppressWarnings({ "unchecked" })
	public <X> X readClaimData(Gson gson) {
		return (X) readClaimData(gson, tryGetClassType().orElse(null));
	}

	public <X> X readClaimData(Gson gson, Class<X> classType) {
		return readClaimData(gson, Optional.ofNullable(classType).map(TypeToken::of).orElse(null));
	}

	public <X> X readClaimData(Gson gson, TypeToken<X> typeToken) {
		if (gson == null)
			return readClaimData(Serials.Gsons.get(), typeToken);
		var jsonElement = Optional.ofNullable(gson.toJsonTree(this, Map.class)).orElse(JsonNull.INSTANCE);
		return gson.fromJson(jsonElement, Optional.ofNullable(typeToken).map(TypeToken::getType).orElse(null));
	}

	private static Optional<URI> parseURI(String url) {
		return parseURI(url, false);
	}

	private static Optional<URI> parseURI(String url, boolean allowInsecure) {
		if (Utils.Strings.isBlank(url))
			return Optional.empty();
		return URIs.parse(url).or(() -> {
			return URIs.parse(String.format("https://%s", url));
		}).map(URIs::normalize).map(v -> {
			if (URIs.isSecure(v) || allowInsecure)
				return v;
			return UrlBuilder.fromUri(v).withScheme("https").toUri();
		});
	}

}
