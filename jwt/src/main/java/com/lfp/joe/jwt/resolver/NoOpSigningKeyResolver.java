package com.lfp.joe.jwt.resolver;

import java.security.Key;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.SigningKeyResolver;

@SuppressWarnings("rawtypes")
public enum NoOpSigningKeyResolver implements SigningKeyResolver {
	INSTANCE;

	@Override
	public Key resolveSigningKey(JwsHeader header, Claims claims) {
		return null;
	}

	@Override
	public Key resolveSigningKey(JwsHeader header, String plaintext) {
		return null;
	}

}
