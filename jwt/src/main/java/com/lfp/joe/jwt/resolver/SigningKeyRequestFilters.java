package com.lfp.joe.jwt.resolver;

import java.net.URI;
import java.util.function.Predicate;

import com.lfp.joe.jwt.claim.ClaimsLFP;
import com.lfp.joe.jwt.resolver.ImmutableAbstractSigningKeyResolver.SigningKeyRequest;
import com.lfp.joe.stream.Streams;

import io.jsonwebtoken.JwsHeader;

public class SigningKeyRequestFilters {

	protected SigningKeyRequestFilters() {}

	public static Predicate<SigningKeyRequest> hasIssuerURI() {
		return r -> r.claims().tryGetIssuerURI().isPresent();
	}

	public static Predicate<SigningKeyRequest> issuerHostsEquals(String... allowedHosts) {
		return issuerHostsEquals(Streams.of(allowedHosts));
	}

	public static Predicate<SigningKeyRequest> issuerHostsEquals(Iterable<? extends String> allowedHosts) {
		var ss = Streams.of(allowedHosts).chain(Streams.cached());
		return r -> ss.get().anyMatch(r.claims()::issuerHostEquals);
	}

	public static Predicate<SigningKeyRequest> issuerDomainEquals(String... allowedDomains) {
		return issuerDomainEquals(Streams.of(allowedDomains));
	}

	public static Predicate<SigningKeyRequest> issuerDomainEquals(Iterable<? extends String> allowedDomains) {
		var ss = Streams.of(allowedDomains).chain(Streams.cached());
		return r -> ss.get().anyMatch(r.claims()::issuerDomainEquals);
	}

	public static Predicate<SigningKeyRequest> issuerURIMatches(URI... allowedURIs) {
		return issuerURIMatches(Streams.of(allowedURIs));
	}

	public static Predicate<SigningKeyRequest> issuerURIMatches(Iterable<? extends URI> allowedURIs) {
		var ss = Streams.of(allowedURIs).chain(Streams.cached());
		return r -> ss.get().anyMatch(r.claims()::issuerURIMatches);
	}

	@SuppressWarnings("rawtypes")
	public static Predicate<SigningKeyRequest> headerFilter(Predicate<? super JwsHeader> headerFilter) {
		return r -> headerFilter != null && headerFilter.test(r.header());
	}

	public static Predicate<SigningKeyRequest> claimsFilter(Predicate<? super ClaimsLFP> claimsFilter) {
		return r -> claimsFilter != null && claimsFilter.test(r.claims());
	}

}
