package com.lfp.joe.jwt.resolver;

import java.security.Key;
import java.util.List;
import java.util.Objects;

import javax.crypto.spec.SecretKeySpec;
import javax.validation.constraints.NotNull;

import org.immutables.value.Value;
import org.immutables.value.Value.Style.BuilderVisibility;
import org.immutables.value.Value.Style.ImplementationVisibility;

import com.lfp.joe.core.function.Throws;
import com.lfp.joe.jwt.resolver.ImmutableAbstractSigningKeyResolver.SigningKeyRequest;
import com.lfp.joe.jwt.token.JwkLFP;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import io.jsonwebtoken.SignatureAlgorithm;

@Value.Immutable
@Value.Style(typeAbstract = "*Def", visibility = ImplementationVisibility.PUBLIC, builderVisibility = BuilderVisibility.PUBLIC)
abstract class SigningKeyResolverDef extends AbstractSigningKeyResolver {

	public abstract List<Key> keys();

	public abstract List<Bytes> bytes();

	public abstract List<JwkLFP> jwks();

	@Override
	protected Iterable<? extends Key> resolveSigningKeys(@NotNull SigningKeyRequest request) {
		var keys = Streams.of(keys())
				.append(Streams.of(() -> keysFromBytes(request)))
				.append(Streams.of(() -> keysFromJwks(request)));
		return keys;
	}

	protected Iterable<? extends Key> keysFromBytes(@NotNull SigningKeyRequest request) {
		if (bytes().isEmpty())
			return null;
		var algorithm = request.header().getAlgorithm();
		if (Utils.Strings.isBlank(algorithm))
			return null;
		var signatureAlgorithm = Streams.of(SignatureAlgorithm.values())
				.filter(SignatureAlgorithm::isHmac)
				.findFirst(v -> v.getValue().equalsIgnoreCase(algorithm))
				.orElse(null);
		if (signatureAlgorithm == null)
			return null;
		return Streams.of(bytes()).nonNull().map(v -> new SecretKeySpec(v.array(), signatureAlgorithm.getJcaName()));
	}

	protected Iterable<? extends Key> keysFromJwks(@NotNull SigningKeyRequest request) {
		return Streams.of(jwks()).nonNull().filter(jwk -> {
			return Objects.equals(request.header().getKeyId(), jwk.getId());
		}).map(Throws.functionUnchecked(JwkLFP::getPublicKey));
	}

	public static void main(String[] args) {

	}
}
