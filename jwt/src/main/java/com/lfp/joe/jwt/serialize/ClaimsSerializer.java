package com.lfp.joe.jwt.serialize;

import java.lang.reflect.Type;
import java.util.Map;

import com.google.common.reflect.TypeToken;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.lfp.joe.jwt.claim.ClaimsLFP;
import com.lfp.joe.serial.serializer.Serializer;

import io.jsonwebtoken.Claims;

@Serializer(value = Claims.class, hierarchy = true)
public enum ClaimsSerializer implements JsonSerializer<Claims>, JsonDeserializer<Claims> {
	INSTANCE;

	@SuppressWarnings("serial")
	private static final Type TYPE = (new TypeToken<Map<String, Object>>() {}).getType();

	@SuppressWarnings("unchecked")
	@Override
	public Claims deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		if (json == null || json.isJsonNull())
			return null;
		var map = (Map<String, Object>) context.deserialize(json, TYPE);
		if (map == null)
			return null;
		return ClaimsLFP.of(map);
	}

	@Override
	public JsonElement serialize(Claims src, Type typeOfSrc, JsonSerializationContext context) {
		if (src == null)
			return JsonNull.INSTANCE;
		return context.serialize(src, Map.class);
	}

}
