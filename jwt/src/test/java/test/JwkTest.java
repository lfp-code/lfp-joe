package test;

import com.github.throwable.beanref.BeanRef;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.jwt.token.JwkLFP;
import com.lfp.joe.serial.Serials;

public class JwkTest {

	public static void main(String[] args) {
		var jwk = JwkLFP.builder()
				.id("123")
				.type("cool")
				.algorithm("ec")
				.usage("ok")
				.certificateUrl("123")
				.certificateThumbprint("ok")
				.putAdditionalAttributes("wow", "neat")
				.build();

		var bps = BeanRef.$(JwkLFP.class).all();
		for (var bp : bps) {
			System.out.println(bp.getPath());
			System.out
					.println(Serials.Gsons.getMemberNames(CoreReflections.tryGetReadMethod(bp).orElse(null)).toList());
		}
		System.out.println(jwk);
		System.out.println(Serials.Gsons.getPretty().toJson(jwk));
	}

}
