package test;

import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.lfp.joe.phone.PhoneNumbers;
import com.lfp.joe.serial.Serials;

public class SerTest {

	public static void main(String[] args) {
		var pn = PhoneNumbers.tryParse("6108101822 ext 10").get();
		System.out.println(pn.getCountryCode());
		System.out.println(pn.getNationalNumber());
		var json = Serials.Gsons.getPretty().toJson(pn);
		System.out.println(json);
		var pn2 = Serials.Gsons.get().fromJson(json, PhoneNumber.class);
		System.out.println(pn2.getCountryCode());
		System.out.println(pn2.getNationalNumber());
	}
}
