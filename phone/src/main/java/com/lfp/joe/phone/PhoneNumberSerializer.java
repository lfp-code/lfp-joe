package com.lfp.joe.phone;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Objects;

import com.github.throwable.beanref.BeanProperty;
import com.github.throwable.beanref.BeanRef;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.lfp.joe.location.Locations;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.serial.serializer.Serializer;
import com.lfp.joe.utils.Utils;

@Serializer(PhoneNumber.class)
public class PhoneNumberSerializer implements JsonSerializer<PhoneNumber>, JsonDeserializer<PhoneNumber> {

	@Override
	public PhoneNumber deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		var countryCode = Serials.Gsons.tryGetAsNumber(json, BeanRef.$(PhoneNumber::getCountryCode).getPath())
				.orElse(null);
		var nationalNumber = Serials.Gsons.tryGetAsNumber(json, BeanRef.$(PhoneNumber::getNationalNumber).getPath())
				.orElse(null);
		if (countryCode == null && nationalNumber == null)
			return null;
		var regionCode = countryCode == null ? null
				: PhoneNumberUtil.getInstance().getRegionCodeForCountryCode(countryCode.intValue());
		var cc = Locations.parseCountryCode(regionCode).orElse(null);
		var pnOp = PhoneNumbers.tryParse(nationalNumber == null ? null : Objects.toString(nationalNumber), cc);
		if (pnOp.isEmpty())
			return null;
		var pn = pnOp.get();
		var extension = Serials.Gsons.tryGetAsString(json, BeanRef.$(PhoneNumber::getExtension).getPath()).orElse(null);
		if (Utils.Strings.isNotBlank(extension))
			pn.setExtension(extension);
		return pn;
	}

	@Override
	public JsonElement serialize(PhoneNumber src, Type typeOfSrc, JsonSerializationContext context) {
		if (src == null)
			return JsonNull.INSTANCE;
		JsonObject jo = new JsonObject();
		var bps = List.of(BeanRef.$(PhoneNumber::getCountryCode), BeanRef.$(PhoneNumber::getNationalNumber),
				BeanRef.$(PhoneNumber::getExtension));
		for (var bp : bps) {
			var je = getJsonElement(src, bp);
			if (je == null || je.isJsonNull())
				continue;
			jo.add(bp.getPath(), je);
		}
		if (jo.size() == 0)
			return JsonNull.INSTANCE;
		return jo;
	}

	private static JsonElement getJsonElement(PhoneNumber src,
			BeanProperty<PhoneNumber, ? extends Object> beanProperty) {
		var value = beanProperty.get(src);
		if (value == null)
			return null;
		if (value instanceof CharSequence && Utils.Strings.isBlank((CharSequence) value))
			return null;
		if (value instanceof Number && ((Number) value).longValue() == 0)
			return null;
		var jpOp = Serials.Gsons.tryCreateJsonPrimitive(value);
		return jpOp.orElse(null);
	}

}
