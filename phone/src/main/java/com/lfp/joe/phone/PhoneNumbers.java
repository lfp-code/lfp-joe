package com.lfp.joe.phone;

import java.util.Optional;

import com.lfp.joe.utils.Utils;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.neovisionaries.i18n.CountryCode;

public class PhoneNumbers {

	private static final CountryCode DEFAULT_REGION = CountryCode.US;

	public static Optional<PhoneNumber> tryParse(CharSequence numberToParse) {
		return tryParse(numberToParse, null);
	}

	public static Optional<PhoneNumber> tryParse(CharSequence numberToParse, CountryCode defaultRegion) {
		if (Utils.Strings.isBlank(numberToParse))
			return Optional.empty();
		if (defaultRegion == null)
			defaultRegion = DEFAULT_REGION;
		PhoneNumber phoneNumber;
		try {
			phoneNumber = PhoneNumberUtil.getInstance().parse(numberToParse, defaultRegion.getAlpha2());
		} catch (NumberParseException e) {
			return Optional.empty();
		}
		return Optional.of(phoneNumber);
	}
}
