package com.lfp.joe.retrofit.callback;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import com.lfp.joe.okhttp.callback.ListenableCallback;
import com.lfp.joe.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetrofitListenableCallback<X> extends
		ListenableCallback.Abs<X, Call<X>, Response<X>, RuntimeException, Throwable> implements Callback<X> {

	public static <X> RetrofitListenableCallback<X> create(Call<X> call) {
		Objects.requireNonNull(call);
		var callback = new RetrofitListenableCallback<X>();
		callback.listener(() -> call.cancel());
		call.enqueue(callback);
		return callback;
	}

	public RetrofitListenableCallback() {
		super((c, r) -> r.body());
	}

	@Override
	protected Throwable uncaughtExceptionResponse(Call<X> call, Response<X> response, Throwable t) {
		throw new RuntimeException(getSummary(call, response, UNCAUGHT_ERROR_MESSAGE), t);
	}

	@Override
	protected RuntimeException alreadyCompleteExceptionResponse(Call<X> call, Response<X> response) {
		throw new RuntimeException(getSummary(call, response, ALREADY_INVOKED_ERROR_MESSAGE));
	}

	@Override
	protected RuntimeException alreadyCompleteExceptionFailure(Call<X> call, Throwable t) {
		throw new RuntimeException(getSummary(call, null, ALREADY_INVOKED_ERROR_MESSAGE), t);
	}

	@Override
	protected void closeResponse(Call<X> call, Response<X> response) {
		// no - operation
	}

	private String getSummary(Call<X> call, Response<X> response, String... append) {
		Map<String, Object> data = new LinkedHashMap<>();
		var message = Utils.Lots.stream(append).filter(Utils.Strings::isNotBlank).joining(", ");
		data.put("", message);
		if (call != null)
			data.put("request", call.request());
		if (response != null) {
			data.put("response-code", response.code());
			data.put("response-message", response.message());
			var errorBody = response.errorBody();
			if (errorBody != null)
				data.put("response-error-body", Utils.Functions.catching(errorBody::toString, t -> null));
		}
		var estream = Utils.Lots.stream(data).nonNullValues().mapValues(Object::toString)
				.filterValues(Utils.Strings::isNotBlank);
		return estream.map(ent -> {
			if (Utils.Strings.isBlank(ent.getKey()))
				return ent.getValue();
			return String.format("%s:%s", ent.getKey(), ent.getValue());
		}).joining(" ");
	}
}
