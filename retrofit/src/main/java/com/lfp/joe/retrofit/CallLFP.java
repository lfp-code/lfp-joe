package com.lfp.joe.retrofit;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeoutException;
import java.util.function.BooleanSupplier;
import java.util.function.Supplier;

import org.immutables.value.Value;
import org.threadly.concurrent.SameThreadSubmitterExecutor;
import org.threadly.concurrent.SubmitterExecutor;
import org.threadly.concurrent.future.FutureCallback;
import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.ListenableFutureTask;

import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.ImmutableEntry;
import com.lfp.joe.core.function.Muto;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.process.future.Completion;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.retrofit.ImmutableCallLFP.CallContext;
import com.lfp.joe.retrofit.timeout.ObservableTimeout;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import okhttp3.MediaType;
import okhttp3.Request;
import okio.Timeout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@ValueLFP.Style
@Value.Enclosing
public class CallLFP<X> implements Call<X> {

	public static <X> CallLFP<X> success(X body) {
		return success(() -> body);
	}

	public static <X> CallLFP<X> success(Callable<X> responseLoader) {
		Callable<Entry<Integer, X>> responseEntryLoader = responseLoader == null ? null
				: () -> ImmutableEntry.of(StatusCodes.OK, responseLoader.call());
		return new CallLFP<X>(CallContext.builder(responseEntryLoader).build());
	}

	public static <X> CallLFP<X> error(Exception error) {
		Callable<Entry<Integer, X>> responseEntryLoader = () -> {
			throw error;
		};
		return new CallLFP<X>(CallContext.builder(responseEntryLoader).build());
	}

	public static <X> CallLFP<X> create(int code, X body) {
		Callable<Entry<Integer, X>> responseEntryLoader = () -> ImmutableEntry.of(code, body);
		return new CallLFP<X>(CallContext.builder(responseEntryLoader).build());
	}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String ALREADY_EXECUTED_MSG = "call already executed";
	private static final String CANCELED_MSG = "call canceled";
	private static final MediaType CONTENT_TYPE_JSON = MediaType
			.parse("application/json; charset=" + StandardCharsets.UTF_8.name());
	private final Muto<ListenableFuture<Response<X>>> responseFutureRef = Muto.create();
	private final Muto<ListenableFuture<?>> timeoutFutureRef = Muto.create();
	private final ObservableTimeout timeout = ObservableTimeout.create();
	private final CallContext<X> context;
	private Long submitTimeNanos;

	public CallLFP(CallContext<X> context) {
		this.context = Objects.requireNonNull(context);
		timeout.addListener(nil -> updateTimeout());
	}

	@Override
	public Response<X> execute() throws IOException {
		Throwable failure;
		Completion<Response<X>> completion;
		try {
			completion = Completion.get(submit(false));
			failure = completion.failure();
		} catch (Throwable t) {
			completion = null;
			failure = t;
		}
		if (failure != null)
			return createResponse(failure);
		return completion.result();
	}

	@Override
	public void enqueue(Callback<X> callback) {
		Objects.requireNonNull(callback);
		var future = submit(false);
		future.callback(new FutureCallback<Response<X>>() {

			@Override
			public void handleResult(Response<X> result) {
				callback.onResponse(CallLFP.this, result);
			}

			@Override
			public void handleFailure(Throwable t) {
				callback.onFailure(CallLFP.this, t);
			}
		});
	}

	protected ListenableFuture<Response<X>> submit(boolean locked) {
		if (responseFutureRef.get() != null)
			return FutureUtils.immediateFailureFuture(new IllegalStateException(ALREADY_EXECUTED_MSG));
		if (!locked) {
			synchronized (responseFutureRef) {
				return submit(true);
			}
		}
		this.submitTimeNanos = System.nanoTime();
		try {
			this.timeout.throwIfReached();
		} catch (Throwable t) {
			return responseFutureRef.getAndSet(FutureUtils.immediateFailureFuture(t));
		}
		var responseFuture = new ListenableFutureExt<>(() -> {
			this.timeout.throwIfReached();
			var responseEntry = this.context.responseEntryLoader().call();
			return createResponse(responseEntry.getKey(), responseEntry.getValue());
		}, this.context.executor()) {

			@Override
			protected boolean completeWithFailure(Throwable t) {
				var response = createResponse(t);
				var result = completeWithResult(response);
				if (result && (this.isTimeout() || !Throws.isHalt(t)))
					LOGGER.warn("call failed. timeout:{} request:{}", this.isTimeout(), request(), t);
				return result;
			}
		};
		responseFuture.listener(() -> updateTimeout());
		responseFutureRef.set(responseFuture);
		updateTimeout();
		this.context.executor().execute(responseFuture);
		return responseFuture;
	}

	protected Response<X> createResponse(Integer code, X result) {
		if (code == null)
			code = 200;
		var rawResponse = Ok.Calls.createResponse(this.context.request(), code, null);
		if (code >= 400) {
			var responseBody = Ok.Calls.createResponseBody(CONTENT_TYPE_JSON, -1l,
					() -> Serials.Gsons.toBytes(Services.INSTANCE.gson(), result).inputStream());
			return Response.error(responseBody, rawResponse);
		}
		return Response.success(result, rawResponse);
	}

	protected Response<X> createResponse(Throwable error) {
		int code = StatusCodes.INTERNAL_SERVER_ERROR;
		var rawResponse = Ok.Calls.createResponse(this.context.request(), code, null);
		String message = Utils.Strings.trimToNullOptional(error.getMessage()).orElseGet(() -> {
			return StatusCodes.getReason(code) + " - " + error.getClass().getSimpleName();
		});
		Map<String, String> result = Map.of("message", message);
		var responseBody = Ok.Calls.createResponseBody(CONTENT_TYPE_JSON, -1l,
				() -> Serials.Gsons.toBytes(Services.INSTANCE.gson(), result).inputStream());
		return Response.error(responseBody, rawResponse);
	}

	protected void updateTimeout() {
		BooleanSupplier shouldUpdate = () -> {
			if (timeoutFutureRef.get() != null)
				return true;
			if (isDone())
				return false;
			return timeout.tryGetDeadlineNanoTime(submitTimeNanos).isPresent();
		};
		if (!shouldUpdate.getAsBoolean())
			return;
		synchronized (timeoutFutureRef) {
			if (!shouldUpdate.getAsBoolean())
				return;
			timeoutFutureRef.updateAndGet(v -> {
				if (v != null)
					v.cancel(true);
				return null;
			});
			if (isDone())
				return;
			var deadlineNanoTimeOp = timeout.tryGetDeadlineNanoTime(submitTimeNanos);
			if (deadlineNanoTimeOp.isEmpty())
				return;
			var delayMillis = Duration.ofNanos(deadlineNanoTimeOp.getAsLong() - System.nanoTime()).toMillis();
			if (delayMillis <= 0) {
				cancel(TimeoutException::new);
				return;
			}
			var timeoutFuture = Threads.Pools.centralPool().submitScheduled(() -> {
				cancel(TimeoutException::new);
			}, delayMillis);
			timeoutFutureRef.set(timeoutFuture);
		}

	}

	@Override
	public void cancel() {
		cancel(() -> new CancellationException(CANCELED_MSG));
	}

	public void cancel(Supplier<Throwable> failureSupplier) {
		Objects.requireNonNull(failureSupplier);
		if (responseFutureRef.get() == null)
			synchronized (responseFutureRef) {
				if (responseFutureRef.get() == null) {
					responseFutureRef.set(FutureUtils.immediateFailureFuture(failureSupplier.get()));
					return;
				}
			}
		var responseFuture = responseFutureRef.get();
		if (responseFuture instanceof ListenableFutureExt)
			((ListenableFutureExt<?>) responseFuture).cancelTimeout();
		else
			responseFuture.cancel(true);
	}

	public boolean isDone() {
		return Optional.ofNullable(responseFutureRef.get()).map(v -> v.isDone()).orElse(false);
	}

	@Override
	public boolean isCanceled() {
		return Optional.ofNullable(responseFutureRef.get()).map(v -> v.isCancelled()).orElse(false);
	}

	@Override
	public boolean isExecuted() {
		return responseFutureRef.get() != null;
	}

	@Override
	public Request request() {
		return context.request();
	}

	@Override
	public Timeout timeout() {
		return this.timeout;
	}

	@Override
	public Call<X> clone() {
		return new CallLFP<X>(this.context);
	}

	@Value.Immutable
	public static abstract class CallContextDef<X> {

		@Value.Default
		public Request request() {
			return new Request.Builder().url("http://localhost/").build();
		}

		@Value.Default
		public SubmitterExecutor executor() {
			return SameThreadSubmitterExecutor.instance();
		}

		public abstract Callable<Entry<Integer, X>> responseEntryLoader();

		public static <U> CallContext.Builder<U> builder(Callable<Entry<Integer, U>> responseEntryLoader) {
			return CallContext.<U>builder().responseEntryLoader(responseEntryLoader);
		}
	}

	protected static class ListenableFutureExt<X> extends ListenableFutureTask<X> {

		private boolean timeout;

		public ListenableFutureExt(Callable<X> task, Executor executingExecutor) {
			super(task, executingExecutor);
		}

		public boolean cancelTimeout() {
			this.timeout = true;
			if (super.cancel(true))
				return true;
			this.timeout = false;
			return false;
		}

		public boolean isTimeout() {
			return timeout;
		}

	}

	public static void main(String[] args) throws IOException, InterruptedException {
		var call = CallLFP.success(() -> {
			System.out.println("loading");
			Thread.sleep(5_000);
			System.out.println("done loading");
			return new Date();
		});
		// call.timeout().timeout(4_000, TimeUnit.MILLISECONDS);
		System.out.println("ready");
		var response = call.execute();
		if (!response.isSuccessful())
			System.err.println(response.errorBody().string());
		else
			System.out.println(response.body());
		Thread.sleep(10_000);
	}

}
