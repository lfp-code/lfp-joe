package com.lfp.joe.retrofit.timeout;

import java.lang.reflect.Constructor;
import java.time.Duration;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.Stream;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.stream.Streams;

import javassist.util.proxy.MethodHandler;
import javassist.util.proxy.Proxy;
import javassist.util.proxy.ProxyFactory;
import okio.Timeout;

public class ObservableTimeout extends Timeout {

	private static final Constructor<?> PROXY_CLASS_TYPE_CONSTRUCTOR = Throws.unchecked(() -> {
		var proxyFactory = new ProxyFactory();
		proxyFactory.setUseCache(false);
		proxyFactory.setSuperclass(ObservableTimeout.class);
		var proxyClassType = proxyFactory.createClass();
		return proxyClassType.getConstructor();
	});
	private static final MethodHandler PROXY_METHOD_HANDLER = (self, thisMethod, proceed, args) -> {
		var method = Optional.ofNullable(proceed).orElse(thisMethod);
		if (!Timeout.class.isAssignableFrom(method.getReturnType()))
			return method.invoke(self, args);
		ObservableTimeout timeout = (ObservableTimeout) self;
		var deadlineNanoTimePre = timeout.tryGetDeadlineNanoTime();
		var timeoutNanosPre = timeout.tryGetTimeoutNanos();
		var result = method.invoke(self, args);
		if (timeout != result)
			return result;
		var deadlineNanoTimePost = timeout.tryGetDeadlineNanoTime();
		var timeoutNanosPost = timeout.tryGetTimeoutNanos();
		if (!Objects.equals(deadlineNanoTimePre, deadlineNanoTimePost)
				|| !Objects.equals(timeoutNanosPre, timeoutNanosPost))
			timeout.eventBus.post(timeout);
		return result;
	};

	public static ObservableTimeout create() {
		ObservableTimeout observableTimeout = (ObservableTimeout) Throws
				.unchecked(() -> PROXY_CLASS_TYPE_CONSTRUCTOR.newInstance());
		((Proxy) observableTimeout).setHandler(PROXY_METHOD_HANDLER);
		return observableTimeout;
	}

	protected final EventBus eventBus = new EventBus();

	protected ObservableTimeout() {
		super();
	}

	public Scrapable addListener(Consumer<ObservableTimeout> listener) {
		var listenerWrapper = new ObservableTimeoutListener(listener);
		eventBus.register(listenerWrapper);
		return Scrapable.create(() -> eventBus.unregister(listenerWrapper));
	}

	public boolean isReached() {
		var deadlineNanoTimeOp = tryGetDeadlineNanoTime();
		if (deadlineNanoTimeOp.isEmpty())
			return false;
		return deadlineNanoTimeOp.getAsLong() - System.nanoTime() <= 0;
	}

	public OptionalLong tryGetDeadlineNanoTime(Long startTimeNanos) {
		var sb = Stream.<Long>builder();
		if (startTimeNanos != null)
			tryGetTimeoutNanos().ifPresent(v -> sb.add(v + startTimeNanos));
		tryGetDeadlineNanoTime().ifPresent(sb::add);
		return Streams.of(sb.build()).mapToLong(v -> v).min();
	}

	public OptionalLong tryGetDeadlineNanoTime() {
		if (!hasDeadline())
			return OptionalLong.empty();
		try {
			return OptionalLong.of(deadlineNanoTime());
		} catch (IllegalStateException e) {
			return OptionalLong.empty();
		}
	}

	public OptionalLong tryGetTimeoutNanos() {
		var timeoutNanos = timeoutNanos();
		if (timeoutNanos == 0)
			return OptionalLong.empty();
		return OptionalLong.of(timeoutNanos);
	}

	protected static class ObservableTimeoutListener {

		private final Consumer<ObservableTimeout> listener;

		public ObservableTimeoutListener(Consumer<ObservableTimeout> listener) {
			super();
			this.listener = Objects.requireNonNull(listener);
		}

		@Subscribe
		public void update(ObservableTimeout timeout) {
			listener.accept(timeout);
		}

	}

	public static void main(String[] args) {
		ObservableTimeout timeout = ObservableTimeout.create();
		timeout.addListener(v -> {
			System.out.println("updated");
		});
		timeout.timeout(Duration.ofHours(1).toMillis(), TimeUnit.MILLISECONDS);
		timeout.clearTimeout();
	}

}
