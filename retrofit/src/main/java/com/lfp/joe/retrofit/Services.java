package com.lfp.joe.retrofit;

import java.net.URI;

import com.github.throwable.beanref.BeanRef;
import com.google.gson.Gson;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.retrofit.client.config.RetrofitConfig;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.serial.gson.IterableSerializer;
import com.lfp.joe.serial.gson.StreamTypeAdapterFactory;
import com.lfp.joe.utils.function.PropertyCache;

import okhttp3.OkHttpClient;

public enum Services {
	INSTANCE;

	private final PropertyCache propertyCache = PropertyCache.create();

	public Gson gson() {
		return propertyCache.get(BeanRef.$(Services::gson), () -> {
			var gsonb = Serials.Gsons.getBuilder();
			gsonb = gsonb.setLenient();
			gsonb = gsonb.serializeNulls();
			gsonb = gsonb.registerTypeAdapter(Iterable.class, IterableSerializer.INSTANCE);
			gsonb = gsonb.registerTypeAdapterFactory(new StreamTypeAdapterFactory(true, true));
			return gsonb.create();
		});
	}

	public OkHttpClient client() {
		return client((Proxy) null);
	}

	public OkHttpClient client(URI proxyURI) {
		return client(proxyURI == null ? null : Proxy.builder(proxyURI).build());
	}

	public OkHttpClient client(Proxy proxy) {
		var cfg = Configs.get(RetrofitConfig.class);
		if (proxy == null && cfg.proxyURI() != null)
			proxy = Proxy.builder(cfg.proxyURI()).build();
		OkHttpClient client = Ok.Clients.get(proxy);
		if (MachineConfig.isDeveloper() && cfg.developerDisableSSL())
			client = Ok.Clients.trustAllSSL(client);
		return client;
	}
}
