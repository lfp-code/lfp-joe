package com.lfp.joe.retrofit.client.config;

import java.net.URI;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.DefaultValue;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.core.properties.converter.URIConverter;

public interface RetrofitConfig extends Config {

	@ConverterClass(URIConverter.class)
	URI proxyURI();

	@DefaultValue("false")
	boolean developerDisableSSL();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.properties());
	}
}
