package com.lfp.joe.process;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

import org.zeroturnaround.exec.InvalidExitValueException;
import org.zeroturnaround.exec.ProcessResult;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.stream.Streams;

public class Procs {

	public static ProcessExecutorLFP processExecutor() {
		return processExecutor((Iterable<String>) null);
	}

	// processExecutor parsed commands

	public static ProcessExecutorLFP processExecutor(String... commands) {
		return processExecutor(nullOrIterable(commands));
	}

	public static ProcessExecutorLFP processExecutor(String command, File directory) {
		return processExecutor(nullOrIterable(command), directory);
	}

	public static ProcessExecutorLFP processExecutor(String command, File directory,
			Map<String, String> environmentVariables) {
		return processExecutor(nullOrIterable(command), directory, environmentVariables);
	}

	// processExecutor iterable commands

	public static ProcessExecutorLFP processExecutor(Iterable<String> commands) {
		return processExecutor(commands, null);
	}

	public static ProcessExecutorLFP processExecutor(Iterable<String> commands, File directory) {
		return processExecutor(commands, directory, null);
	}

	public static ProcessExecutorLFP processExecutor(Iterable<String> commands, File directory,
			Map<String, String> environmentVariables) {
		var processExecutor = new ProcessExecutorLFP();
		processExecutor.exitValueNormal();
		var commandIterator = Optional.ofNullable(commands)
				.map(Iterable::iterator)
				.orElseGet(Collections::emptyIterator);
		if (commandIterator.hasNext()) {
			var firstCommand = commandIterator.next();
			if (!commandIterator.hasNext())
				processExecutor.commandSplit(firstCommand);
			else
				processExecutor.command(Streams.of(commandIterator).prepend(firstCommand));
		}
		if (directory != null)
			processExecutor.directory(directory);
		if (environmentVariables == null)
			processExecutor.systemEnvironmentVariables();
		else
			processExecutor.environment(environmentVariables);
		return processExecutor;
	}

	// start parsed commands

	public static ProcessLFP start(String... commands) throws IOException {
		return start(nullOrIterable(commands));
	}

	public static ProcessLFP start(String command, File directory) throws IOException {
		return start(nullOrIterable(command), directory);
	}

	public static ProcessLFP start(String command, File directory, Map<String, String> environmentVariables)
			throws IOException {
		return start(nullOrIterable(command), directory, environmentVariables);
	}

	public static ProcessLFP start(String command, Consumer<ProcessExecutorLFP> processExecutorConfig)
			throws IOException {
		return start(nullOrIterable(command), processExecutorConfig);
	}

	// start iterable commands

	public static ProcessLFP start(Iterable<String> commands) throws IOException {
		return start(commands, (File) null);
	}

	public static ProcessLFP start(Iterable<String> commands, File directory) throws IOException {
		return start(commands, directory, null);
	}

	public static ProcessLFP start(Iterable<String> commands, File directory, Map<String, String> environmentVariables)
			throws IOException {
		return processExecutor(commands, directory, environmentVariables).start();
	}

	public static ProcessLFP start(Iterable<String> commands, Consumer<ProcessExecutorLFP> processExecutorConfig)
			throws IOException {
		var processExecutor = processExecutor(commands);
		if (processExecutorConfig != null)
			processExecutorConfig.accept(processExecutor);
		return processExecutor.start();
	}

	// execute parse commands

	public static ProcessResult execute(String... commands) throws IOException {
		return execute(nullOrIterable(commands));
	}

	public static ProcessResult execute(String command, File directory) throws IOException {
		return execute(nullOrIterable(command), directory);
	}

	public static ProcessResult execute(String command, File directory, Map<String, String> environmentVariables)
			throws IOException {
		return execute(nullOrIterable(command), directory, environmentVariables);
	}

	public static ProcessResult execute(String command, Consumer<ProcessExecutorLFP> processExecutorConfig)
			throws IOException {
		return execute(nullOrIterable(command), processExecutorConfig);
	}

	// execute iterable commands

	public static ProcessResult execute(Iterable<String> commands) throws IOException {
		return execute(commands, (File) null);
	}

	public static ProcessResult execute(Iterable<String> commands, File directory) throws IOException {
		return execute(commands, directory, null);
	}

	public static ProcessResult execute(Iterable<String> commands, File directory,
			Map<String, String> environmentVariables) throws IOException {
		return processExecutor(commands, directory, environmentVariables).readOutput(true).start().join();
	}

	public static ProcessResult execute(Iterable<String> command, Consumer<ProcessExecutorLFP> processExecutorConfig)
			throws IOException {
		return start(command, cfg -> {
			cfg.readOutput(true);
			if (processExecutorConfig != null)
				processExecutorConfig.accept(cfg);
		}).join();
	}

	public static boolean killAll(String name) throws IOException {
		if (name == null || name.isBlank())
			throw new IllegalArgumentException("invalid name:" + name);
		List<String> commands;
		if (MachineConfig.isWindows())
			commands = List.of("taskkill", "/f", "/t", "/im", name);
		else
			commands = List.of("pkill", "-9", name);
		var pr = Procs.execute(commands, cfg -> {
			if (MachineConfig.isDeveloper())
				cfg.logOutput();
			cfg.exitValueAny();
		});
		return pr.getExitValue() == 0;
	}

	private static Iterable<String> nullOrIterable(String... commands) {
		if (commands == null || commands.length == 0)
			return null;
		return Arrays.asList(commands);
	}

	public static void main(String[] args)
			throws InvalidExitValueException, IOException, InterruptedException, TimeoutException, ExecutionException {
		var pr = start("java --version", pe -> {
			pe.logOutput();
			pe.addListener(ProcessListenerLFP.builder().afterComplete(p -> {
				System.out.println(p.exitValue());
			}).build());
		}).join();
		Thread.sleep(Duration.ofSeconds(10).toMillis());
	}

}
