package com.lfp.joe.process;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

import org.immutables.value.Value;
import org.zeroturnaround.exec.ProcessExecutor;
import org.zeroturnaround.exec.ProcessResult;
import org.zeroturnaround.exec.listener.ProcessListener;

@Value.Immutable
public abstract class ProcessListenerLFP extends ProcessListener {

	private static final Consumer<Process> NO_OP_PROCESS_CONSUMER = nil -> {};

	@Value.Default
	public Consumer<ProcessExecutorLFP> beforeStart() {
		return nil -> {};
	}

	@Value.Default
	public BiConsumer<Process, ProcessExecutorLFP> afterStart() {
		return (nil1, nil2) -> {};
	}

	@Value.Default
	public BiConsumer<Process, ProcessResult> afterFinish() {
		return (nil1, nil2) -> {};
	}

	@Value.Default
	public Consumer<Process> afterStop() {
		return nil -> {};
	}

	@Value.Default
	public Consumer<Process> beforeComplete() {
		return NO_OP_PROCESS_CONSUMER;
	}

	@Value.Default
	public Consumer<Process> afterComplete() {
		return NO_OP_PROCESS_CONSUMER;
	}

	@Override
	public void beforeStart(ProcessExecutor executor) {
		beforeStart().accept((ProcessExecutorLFP) executor);
	}

	@Override
	public void afterStart(Process process, ProcessExecutor executor) {
		afterStart().accept(process, (ProcessExecutorLFP) executor);
	}

	@Override
	public void afterFinish(Process process, ProcessResult result) {
		afterFinish().accept(process, result);
	}

	@Override
	public void afterStop(Process process) {
		afterStop().accept(process);
	}

	@Value.Check
	protected ProcessListenerLFP check() {
		{
			var processListenerUpdated = withCompleteListener(this, ProcessListenerLFP::beforeComplete,
					(b, l) -> b.beforeComplete(l), true);
			if (processListenerUpdated.isPresent())
				return processListenerUpdated.get();
		}
		{
			var processListenerUpdated = withCompleteListener(this, ProcessListenerLFP::afterComplete,
					(b, l) -> b.afterComplete(l), false);
			if (processListenerUpdated.isPresent())
				return processListenerUpdated.get();
		}
		return this;
	}

	private static Optional<ProcessListenerLFP> withCompleteListener(ProcessListenerLFP processListener,
			Function<ProcessListenerLFP, Consumer<Process>> getter,
			BiConsumer<ImmutableProcessListenerLFP.Builder, Consumer<Process>> setter, boolean before) {
		var listener = getter.apply(processListener);
		if (NO_OP_PROCESS_CONSUMER.equals(listener))
			return Optional.empty();
		var listenerOnce = new Consumer<Process>() {

			private final AtomicBoolean complete = new AtomicBoolean();

			@Override
			public void accept(Process process) {
				if (!complete.compareAndSet(false, true))
					return;
				listener.accept(process);
			}
		};
		var blder = ImmutableProcessListenerLFP.builder().from(processListener);
		blder.afterFinish((p, pr) -> {
			try {
				try {
					if (before)
						listenerOnce.accept(p);
				} finally {
					processListener.afterFinish().accept(p, pr);
				}
			} finally {
				if (!before)
					listenerOnce.accept(p);
			}
		});
		blder.afterStop(p -> {
			try {
				try {
					if (before)
						listenerOnce.accept(p);
				} finally {
					processListener.afterStop().accept(p);
				}
			} finally {
				if (!before)
					listenerOnce.accept(p);
			}
		});
		setter.accept(blder, NO_OP_PROCESS_CONSUMER);
		return Optional.of(blder.build());
	}

	public static ImmutableProcessListenerLFP.Builder builder() {
		return ImmutableProcessListenerLFP.builder();
	}

}
