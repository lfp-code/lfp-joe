package com.lfp.joe.process;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.stream.Stream;

import org.zeroturnaround.exec.ProcessExecutor;
import org.zeroturnaround.exec.ProcessResult;
import org.zeroturnaround.exec.close.ProcessCloser;
import org.zeroturnaround.exec.close.StandardProcessCloser;
import org.zeroturnaround.exec.stream.ExecuteStreamHandler;
import org.zeroturnaround.exec.stream.PumpStreamHandler;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.ImmutableMemberCache.MethodRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.function.Throws;

@SuppressWarnings("unchecked")
public class WaitForProcessContext {

	private final Callable<ProcessResult> waitForProcess;

	public WaitForProcessContext(Callable<ProcessResult> waitForProcess) {
		Objects.requireNonNull(waitForProcess);
		if (!getClassType().isInstance(waitForProcess)) {
			var message = String.format("invalid waitForProcess. requiredType:%s requestType:%s", getClassType(),
					waitForProcess.getClass());
			throw new IllegalArgumentException(message);
		}
		this.waitForProcess = waitForProcess;
	}

	public Callable<ProcessResult> get() {
		return this.waitForProcess;
	}

	public Process getProcess() {
		var req = MethodRequest.of(getClassType(), null, "getProcess");
		return (Process) MemberCache.invokeMethod(req, get());
	}

	public ProcessAttributesContext getAttributes() {
		var req = FieldRequest.of(getClassType(), null, "attributes");
		var attributesObj = MemberCache.getFieldValue(req, get());
		return new ProcessAttributesContext(attributesObj);
	}

	public ProcessCloser getCloser() {
		var req = FieldRequest.of(getClassType(), ProcessCloser.class, "closer");
		return MemberCache.getFieldValue(req, get());
	}

	public Optional<PumpStreamHandler> tryGetPumpStreamHandler() {
		var standardProcessCloserOp = CoreReflections.tryCast(getCloser(), StandardProcessCloser.class);
		var streamsOp = standardProcessCloserOp.map(v -> {
			var req = FieldRequest.of(StandardProcessCloser.class, ExecuteStreamHandler.class, "streams");
			return MemberCache.getFieldValue(req, v);
		});
		return streamsOp.flatMap(v -> CoreReflections.tryCast(v, PumpStreamHandler.class));
	}

	public Optional<Thread> tryGetOutputThread() {
		return tryGetThread("outputThread");
	}

	public Optional<Thread> tryGetErrorThread() {
		return tryGetThread("errorThread");
	}

	public Optional<Thread> tryGetInputThread() {
		return tryGetThread("inputThread");
	}

	public Stream<Thread> streamThreads() {
		return Stream.of(tryGetOutputThread(), tryGetErrorThread(), tryGetInputThread())
				.filter(Optional::isPresent)
				.map(Optional::get);
	}

	private Optional<Thread> tryGetThread(String fieldName) {
		var psHandlerOp = tryGetPumpStreamHandler();
		var threadOp = psHandlerOp.map(v -> {
			var req = FieldRequest.of(PumpStreamHandler.class, Thread.class, fieldName);
			return MemberCache.getFieldValue(req, v);
		});
		return threadOp;
	}

	public static Class<? super Callable<ProcessResult>> getClassType() {
		var req = MethodRequest.of(ProcessExecutor.class, null, "startInternal");
		var method = Throws.unchecked(() -> MemberCache.getMethod(req));
		var classType = (Class<? super Callable<ProcessResult>>) method.getReturnType();
		return classType;
	}

}
