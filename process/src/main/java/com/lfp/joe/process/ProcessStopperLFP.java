package com.lfp.joe.process;

import java.util.Arrays;
import java.util.Objects;
import java.util.OptionalLong;

import org.slf4j.Logger;
import org.zeroturnaround.exec.stop.ProcessStopper;
import org.zeroturnaround.process.Processes;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Throws;

public interface ProcessStopperLFP extends ProcessStopper {

	public static final ProcessStopperLFP INSTANCE = new ProcessStopperLFP() {};

	@Override
	default void stop(Process process) {
		OptionalLong pidOp;
		try {
			pidOp = OptionalLong.of(process.pid());
		} catch (Throwable t) {
			pidOp = OptionalLong.empty();
		}
		if (pidOp.isPresent())
			kill(pidOp.getAsLong());
		var javaProcess = Processes.newJavaProcess(process);
		Throws.unchecked(javaProcess::destroyForcefully);
	}

	private static void kill(long pid) {
		String[] command;
		if (MachineConfig.isWindows())
			command = new String[] { "Taskkill", "/F", "/T", "/PID", Objects.toString(pid) };
		else
			command = new String[] { "kill", "-9", Objects.toString(pid) };
		try {
			Runtime.getRuntime().exec(command).waitFor();
		} catch (Throwable t) {
			logger().warn("kill failed. command:{} error:{}", Arrays.toString(command), t.getMessage());
		}
	}

	private static Logger logger() {
		return org.slf4j.LoggerFactory.getLogger(ProcessStopperLFP.class);
	}
}
