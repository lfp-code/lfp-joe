package com.lfp.joe.process;

import java.util.Objects;
import java.util.concurrent.Future;

import org.threadly.concurrent.future.ListenableFuture;
import org.zeroturnaround.exec.ProcessResult;
import org.zeroturnaround.exec.StartedProcess;

import com.lfp.joe.threads.Threads;
import com.lfp.joe.threads.future.ListenableFutureWrapper;

public class ProcessLFP extends StartedProcess implements ListenableFutureWrapper<ProcessResult> {

	private final ProcessAttributesContext processAttributesContext;

	public ProcessLFP(Process process, ListenableFuture<ProcessResult> future,
			ProcessAttributesContext processAttributesContext) {
		super(process, future);
		this.processAttributesContext = Objects.requireNonNull(processAttributesContext);
	}

	public ProcessAttributesContext getAttributes() {
		return this.processAttributesContext;
	}

	@Override
	public ListenableFuture<ProcessResult> getFuture() {
		return (ListenableFuture<ProcessResult>) super.getFuture();
	}

	public ProcessResult join() {
		return Threads.Futures.join(this);
	}

	@Override
	public Future<ProcessResult> unwrap() {
		return getFuture();
	}

	@Override
	public boolean cancel(boolean mayInterruptIfRunning) {
		return Threads.Futures.cancel(getFuture(), mayInterruptIfRunning);
	}

}
