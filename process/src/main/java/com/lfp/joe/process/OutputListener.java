package com.lfp.joe.process;

import java.util.Optional;

public interface OutputListener {

	void accept(Optional<Process> process, OutputType outputType, String line);
}
