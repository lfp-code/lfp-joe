package com.lfp.joe.process;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.threadly.concurrent.future.ListenableFutureTask;
import org.threadly.concurrent.wrapper.traceability.ThreadRenamingRunnable;
import org.zeroturnaround.exec.MessageLogger;
import org.zeroturnaround.exec.ProcessExecutor;
import org.zeroturnaround.exec.ProcessResult;
import org.zeroturnaround.exec.listener.ProcessDestroyer;
import org.zeroturnaround.exec.listener.ProcessListener;
import org.zeroturnaround.exec.stop.ProcessStopper;
import org.zeroturnaround.exec.stream.ExecuteStreamHandler;
import org.zeroturnaround.exec.stream.LogOutputStream;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.log.LogConsumer;
import com.lfp.joe.core.log.Logging;
import com.lfp.joe.core.process.executor.CoreTasks;

import ch.qos.logback.classic.Level;

@SuppressWarnings("deprecation")
public class ProcessExecutorLFP extends ProcessExecutor {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger("process-executor");
	private static final Map<Thread, Process> THREAD_TO_PROCESS_MAP = new ConcurrentHashMap<>();
	private final Set<OutputListener> ouputListeners = Collections.newSetFromMap(new ConcurrentHashMap<>());

	public ProcessExecutorLFP() {
		super();
		stopper(ProcessStopperLFP.INSTANCE);
		for (var outputType : OutputType.values()) {
			var logOutputStream = new LogOutputStream() {

				@Override
				protected void processLine(String line) {
					var processOp = Optional.ofNullable(THREAD_TO_PROCESS_MAP.get(Thread.currentThread()));
					ouputListeners.forEach(v -> v.accept(processOp, outputType, line));
				}
			};
			outputType.getOutputStreamConsumer().accept(this, logOutputStream);
		}
		if (MachineConfig.isDeveloper())
			this.addListener(ProcessListenerLFP.builder().afterStart((p, nil) -> {
				var commandStream = ShellCommands.stripFromCommand(getCommand()).stream();
				var executableName = tryGetExecutableName().orElse(null);
				if (executableName != null)
					commandStream = Stream.concat(Stream.of(executableName), commandStream.skip(1));
				LOGGER.info("[{}] {}", p.pid(), commandStream.collect(Collectors.joining(" ")));
			}).build());
		this.logOutput();
	}

	@Override
	public ProcessLFP start() throws IOException {
		var waitForProcess = startInternal();
		var waitForProcessContext = new WaitForProcessContext(waitForProcess);
		var threads = waitForProcessContext.streamThreads().collect(Collectors.toSet());
		Runnable cleanup = () -> {
			for (var thread : threads)
				THREAD_TO_PROCESS_MAP.remove(thread);
		};
		try {
			for (var thread : threads)
				THREAD_TO_PROCESS_MAP.put(thread, waitForProcessContext.getProcess());
			var result = start(waitForProcessContext);
			result.listener(cleanup);
			return result;
		} catch (Throwable t) {
			cleanup.run();
			throw t;
		}
	}

	@Override
	public ProcessExecutorLFP commandSplit(String commandWithArgs) {
		return command(CommandSplitter.get().apply(commandWithArgs));
	}

	@Override
	public ProcessExecutorLFP command(String... command) {
		return command(command == null ? null : Arrays.asList(command));
	}

	@Override
	public ProcessExecutorLFP command(Iterable<String> command) {
		return command(command == null ? null
				: StreamSupport.stream(command.spliterator(), false).collect(Collectors.toList()));
	}

	@Override
	public ProcessExecutorLFP command(List<String> command) {
		if (command != null && !command.isEmpty()) {
			var execFile = Which.get(command.get(0)).orElse(null);
			if (execFile != null)
				command = Stream.concat(Stream.of(execFile.getAbsolutePath()), command.stream().skip(1))
						.collect(Collectors.toUnmodifiableList());
		}
		return (ProcessExecutorLFP) super.command(command);
	}

	protected ProcessLFP start(WaitForProcessContext waitForProcessContext) {
		var process = waitForProcessContext.getProcess();
		ListenableFutureTask<ProcessResult> lfutureTask;
		{
			Executor executor = r -> {
				r = new ThreadRenamingRunnable(r, process.toString(), false);
				CoreTasks.executor().execute(r);
			};
			var wrappedTask = wrapTask(waitForProcessContext.get());
			lfutureTask = new ListenableFutureTask<ProcessResult>(wrappedTask, executor);
			executor.execute(lfutureTask);
		}
		lfutureTask.failureCallback(t -> {
			if (t == null)
				return;
			if (t instanceof CancellationException && !(t instanceof InterruptedException))
				return;
			ProcessStopperLFP.INSTANCE.stop(process);
		});
		return new ProcessLFP(process, lfutureTask, waitForProcessContext.getAttributes());
	}

	protected Optional<String> tryGetExecutableName() {
		var command = ShellCommands.stripFromCommand(getCommand());
		var executable = Which.get(command.stream().findFirst().orElse(null)).orElse(null);
		if (executable == null)
			return Optional.empty();
		else {
			var fileName = executable.getName();
			if (fileName == null || fileName.isBlank())
				return Optional.empty();
			else
				return Optional.of(fileName);
		}
	}

	public Scrapable onOutput(OutputType outputType, Consumer<String> listener) {
		Objects.requireNonNull(outputType);
		return onOutput(listener == null ? null : (o, line) -> {
			if (outputType.equals(o))
				listener.accept(line);
		});
	}

	public Scrapable onOutput(BiConsumer<OutputType, String> listener) {
		Objects.requireNonNull(listener);
		return onOutput(listener == null ? null : (procOp, ot, line) -> listener.accept(ot, line));
	}

	public Scrapable onOutput(OutputListener listener) {
		Objects.requireNonNull(listener);
		this.ouputListeners.add(listener);
		return Scrapable.create(() -> this.ouputListeners.remove(listener));
	}

	public ProcessExecutorLFP logOutput() {
		for (var outputType : OutputType.values())
			logOutput(outputType);
		return this;
	}

	public ProcessExecutorLFP logOutput(Logger logger) {
		for (var outputType : OutputType.values())
			logOutput(outputType, outputType.logLevel(), logger);
		return this;
	}

	public ProcessExecutorLFP logOutput(OutputType outputType) {
		this.ouputListeners.add(new LogOutputListener(this, outputType));
		return this;
	}

	public ProcessExecutorLFP logOutput(OutputType outputType, Level level) {
		this.ouputListeners.add(new LogOutputListener(this, outputType, level));
		return this;
	}

	public ProcessExecutorLFP logOutput(OutputType outputType, Level level, org.slf4j.Logger logger) {
		this.ouputListeners.add(new LogOutputListener(this, outputType, level, logger));
		return this;
	}

	public ProcessExecutorLFP disableOutputLog() {
		for (var outputType : OutputType.values())
			disableOutputLog(outputType);
		return this;
	}

	public ProcessExecutorLFP disableOutputLog(OutputType outputType) {
		this.ouputListeners.remove(new LogOutputListener(this, outputType));
		return this;
	}

	public ProcessExecutorLFP disableOutputLog(OutputType outputType, Level level) {
		this.ouputListeners.remove(new LogOutputListener(this, outputType, level));
		return this;
	}

	public ProcessExecutorLFP disableOutputLog(OutputType outputType, Level level, org.slf4j.Logger logger) {
		this.ouputListeners.remove(new LogOutputListener(this, outputType, level, logger));
		return this;
	}

	public ProcessExecutorLFP systemEnvironmentVariables() {
		return systemEnvironmentVariables(false);
	}

	public ProcessExecutorLFP systemEnvironmentVariables(boolean replace) {
		var envMap = System.getenv();
		if (envMap == null || envMap.isEmpty())
			return this;
		for (var ent : envMap.entrySet()) {
			var key = ent.getKey();
			if ("CLASSPATH".equalsIgnoreCase(key))
				continue;
			if ("CD".equalsIgnoreCase(key))
				continue;
			var value = this.getEnvironment().get(key);
			if (!replace && value != null)
				continue;
			this.environment(ent.getKey(), ent.getValue());
		}
		return this;
	}

	private static class LogOutputListener implements OutputListener {

		private final ProcessExecutorLFP processExecutor;
		private final OutputType outputType;
		private final Level level;
		private final Logger logger;
		private LogConsumer levelConsumer;

		public LogOutputListener(ProcessExecutorLFP processExecutor, OutputType outputType) {
			this(processExecutor, outputType, Optional.ofNullable(outputType).map(OutputType::logLevel).orElse(null));
		}

		public LogOutputListener(ProcessExecutorLFP processExecutor, OutputType outputType, Level level) {
			this(processExecutor, outputType, level, LOGGER);
		}

		public LogOutputListener(ProcessExecutorLFP processExecutor, OutputType outputType, Level level,
				org.slf4j.Logger logger) {
			this.processExecutor = Objects.requireNonNull(processExecutor);
			this.outputType = Objects.requireNonNull(outputType);
			this.level = Objects.requireNonNull(level);
			this.logger = Objects.requireNonNull(logger);
		}

		@Override
		public void accept(Optional<Process> process, OutputType outputType, String line) {
			if (!Objects.equals(this.outputType, outputType))
				return;
			if (levelConsumer == null)
				levelConsumer = Logging.levelConsumer(logger, level);
			var executableNameOp = processExecutor.tryGetExecutableName();
			var pidOp = Optional.ofNullable(THREAD_TO_PROCESS_MAP.get(Thread.currentThread())).map(Process::pid);
			var prependStream = Stream.of(executableNameOp, pidOp).filter(Optional::isPresent).map(Optional::get)
					.map(Objects::toString);
			prependStream = prependStream.filter(v -> !v.isBlank());
			var prepend = prependStream.collect(Collectors.joining("-"));
			if (!prepend.isEmpty())
				line = "[" + prepend + "] " + line;
			levelConsumer.log(line);

		}

		@Override
		public int hashCode() {
			return Objects.hash(level, logger, outputType, processExecutor);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			LogOutputListener other = (LogOutputListener) obj;
			return Objects.equals(level, other.level) && Objects.equals(logger, other.logger)
					&& outputType == other.outputType && Objects.equals(processExecutor, other.processExecutor);
		}
	}

	public static void main(String[] args) throws IOException {
		var pe = new ProcessExecutorLFP().commandSplit("npm list");
		pe.environment(System.getenv());
		pe.exitValueNormal();
		if (true)
			pe.onOutput((v, t) -> {
				System.out.println(v + " - " + t);
			});
		pe.onOutput(OutputType.out, v -> {
			System.out.println(v);
		});
		pe.logOutput();
		pe.start().join();
		pe.start().join();
		System.err.println("done");
	}

	/*
	 * delegates
	 */

	@Override
	public ProcessExecutorLFP directory(File directory) {
		return (ProcessExecutorLFP) super.directory(directory);
	}

	@Override
	public ProcessExecutorLFP environment(Map<String, String> env) {
		return (ProcessExecutorLFP) super.environment(env);
	}

	@Override
	public ProcessExecutorLFP environment(String name, String value) {
		return (ProcessExecutorLFP) super.environment(name, value);
	}

	@Override
	public ProcessExecutorLFP redirectErrorStream(boolean redirectErrorStream) {
		return (ProcessExecutorLFP) super.redirectErrorStream(redirectErrorStream);
	}

	@Override
	public ProcessExecutorLFP exitValueAny() {
		return (ProcessExecutorLFP) super.exitValueAny();
	}

	@Override
	public ProcessExecutorLFP exitValueNormal() {
		return (ProcessExecutorLFP) super.exitValueNormal();
	}

	@Override
	public ProcessExecutorLFP exitValue(Integer exitValue) {
		return (ProcessExecutorLFP) super.exitValue(exitValue);
	}

	@Override
	public ProcessExecutorLFP exitValues(Integer... exitValues) {
		return (ProcessExecutorLFP) super.exitValues(exitValues);
	}

	@Override
	public ProcessExecutorLFP exitValues(int[] exitValues) {
		return (ProcessExecutorLFP) super.exitValues(exitValues);
	}

	@Override
	public ProcessExecutorLFP timeout(long timeout, TimeUnit unit) {
		return (ProcessExecutorLFP) super.timeout(timeout, unit);
	}

	@Override
	public ProcessExecutorLFP stopper(ProcessStopper stopper) {
		return (ProcessExecutorLFP) super.stopper(stopper);
	}

	@Override
	public ProcessExecutorLFP streams(ExecuteStreamHandler streams) {
		return (ProcessExecutorLFP) super.streams(streams);
	}

	@Override
	public ProcessExecutorLFP closeTimeout(long timeout, TimeUnit unit) {
		return (ProcessExecutorLFP) super.closeTimeout(timeout, unit);
	}

	@Override
	public ProcessExecutorLFP redirectInput(InputStream input) {
		return (ProcessExecutorLFP) super.redirectInput(input);
	}

	@Override
	public ProcessExecutorLFP redirectOutput(OutputStream output) {
		return (ProcessExecutorLFP) super.redirectOutput(output);
	}

	@Override
	public ProcessExecutorLFP redirectError(OutputStream output) {
		return (ProcessExecutorLFP) super.redirectError(output);
	}

	@Override
	public ProcessExecutorLFP redirectOutputAlsoTo(OutputStream output) {
		return (ProcessExecutorLFP) super.redirectOutputAlsoTo(output);
	}

	@Override
	public ProcessExecutorLFP redirectErrorAlsoTo(OutputStream output) {
		return (ProcessExecutorLFP) super.redirectErrorAlsoTo(output);
	}

	@Override
	public ProcessExecutorLFP readOutput(boolean readOutput) {
		return (ProcessExecutorLFP) super.readOutput(readOutput);
	}

	@Override
	public ProcessExecutorLFP info(org.slf4j.Logger log) {
		return (ProcessExecutorLFP) super.info(log);
	}

	@Override
	public ProcessExecutorLFP debug(org.slf4j.Logger log) {
		return (ProcessExecutorLFP) super.debug(log);
	}

	@Override
	public ProcessExecutorLFP info(String name) {
		return (ProcessExecutorLFP) super.info(name);
	}

	@Override
	public ProcessExecutorLFP debug(String name) {
		return (ProcessExecutorLFP) super.debug(name);
	}

	@Override
	public ProcessExecutorLFP info() {
		return (ProcessExecutorLFP) super.info();
	}

	@Override
	public ProcessExecutorLFP debug() {
		return (ProcessExecutorLFP) super.debug();
	}

	@Override
	public ProcessExecutorLFP redirectOutputAsInfo(org.slf4j.Logger log) {
		return (ProcessExecutorLFP) super.redirectOutputAsInfo(log);
	}

	@Override
	public ProcessExecutorLFP redirectOutputAsDebug(org.slf4j.Logger log) {
		return (ProcessExecutorLFP) super.redirectOutputAsDebug(log);
	}

	@Override
	public ProcessExecutorLFP redirectOutputAsInfo(String name) {
		return (ProcessExecutorLFP) super.redirectOutputAsInfo(name);
	}

	@Override
	public ProcessExecutorLFP redirectOutputAsDebug(String name) {
		return (ProcessExecutorLFP) super.redirectOutputAsDebug(name);
	}

	@Override
	public ProcessExecutorLFP redirectOutputAsInfo() {
		return (ProcessExecutorLFP) super.redirectOutputAsInfo();
	}

	@Override
	public ProcessExecutorLFP redirectOutputAsDebug() {
		return (ProcessExecutorLFP) super.redirectOutputAsDebug();
	}

	@Override
	public ProcessExecutorLFP redirectErrorAsInfo(org.slf4j.Logger log) {
		return (ProcessExecutorLFP) super.redirectErrorAsInfo(log);
	}

	@Override
	public ProcessExecutorLFP redirectErrorAsDebug(org.slf4j.Logger log) {
		return (ProcessExecutorLFP) super.redirectErrorAsDebug(log);
	}

	@Override
	public ProcessExecutorLFP redirectErrorAsInfo(String name) {
		return (ProcessExecutorLFP) super.redirectErrorAsInfo(name);
	}

	@Override
	public ProcessExecutorLFP redirectErrorAsDebug(String name) {
		return (ProcessExecutorLFP) super.redirectErrorAsDebug(name);
	}

	@Override
	public ProcessExecutorLFP redirectErrorAsInfo() {
		return (ProcessExecutorLFP) super.redirectErrorAsInfo();
	}

	@Override
	public ProcessExecutorLFP redirectErrorAsDebug() {
		return (ProcessExecutorLFP) super.redirectErrorAsDebug();
	}

	@Override
	public ProcessExecutorLFP addDestroyer(ProcessDestroyer destroyer) {
		return (ProcessExecutorLFP) super.addDestroyer(destroyer);
	}

	@Override
	public ProcessExecutorLFP destroyer(ProcessDestroyer destroyer) {
		return (ProcessExecutorLFP) super.destroyer(destroyer);
	}

	@Override
	public ProcessExecutorLFP destroyOnExit() {
		return (ProcessExecutorLFP) super.destroyOnExit();
	}

	@Override
	public ProcessExecutorLFP listener(ProcessListener listener) {
		return (ProcessExecutorLFP) super.listener(listener);
	}

	@Override
	public ProcessExecutorLFP addListener(ProcessListener listener) {
		return (ProcessExecutorLFP) super.addListener(listener);
	}

	@Override
	public ProcessExecutorLFP removeListener(ProcessListener listener) {
		return (ProcessExecutorLFP) super.removeListener(listener);
	}

	@Override
	public ProcessExecutorLFP removeListeners(Class<? extends ProcessListener> listenerType) {
		return (ProcessExecutorLFP) super.removeListeners(listenerType);
	}

	@Override
	public ProcessExecutorLFP clearListeners() {
		return (ProcessExecutorLFP) super.clearListeners();
	}

	@Override
	public ProcessExecutorLFP setMessageLogger(MessageLogger messageLogger) {
		return (ProcessExecutorLFP) super.setMessageLogger(messageLogger);
	}

}
