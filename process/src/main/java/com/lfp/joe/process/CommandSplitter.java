package com.lfp.joe.process;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.lfp.joe.core.classpath.Instances;

public class CommandSplitter implements Function<String, List<String>> {
	private static final Pattern SPLIT_PATTERN = Pattern
			.compile("\"([^\"\\\\]*(?:\\\\.[^\"\\\\]*)*)\"|'([^'\\\\]*(?:\\\\.[^'\\\\]*)*)'|[^\\s]+");

	public static CommandSplitter get() {
		return Instances.get();
	}

	@Override
	public List<String> apply(String input) {
		if (input == null || input.isEmpty())
			return List.of();
		List<String> matchList = new ArrayList<String>();
		Matcher regexMatcher = SPLIT_PATTERN.matcher(input);
		while (regexMatcher.find()) {
			if (regexMatcher.group(1) != null) {
				// Add double-quoted string without the quotes
				matchList.add(regexMatcher.group(1));
			} else if (regexMatcher.group(2) != null) {
				// Add single-quoted string without the quotes
				matchList.add(regexMatcher.group(2));
			} else {
				// Add unquoted word
				matchList.add(regexMatcher.group());
			}
		}
		return Collections.unmodifiableList(matchList);
	}

	public static void main(String[] args) {
		System.out.println(CommandSplitter.get().apply("cmd /c args.bat 'wow\\no\\t    k' cool \"%ProgramFiles%\""));
	}

}
