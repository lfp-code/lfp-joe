package com.lfp.joe.process;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.function.Predicate;

import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;
import org.zeroturnaround.exec.ProcessExecutor;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.threads.Threads;

public class Which {

	protected Which() {

	}

	private static final Duration POLL_INTERVAL_DEFAULT = Duration.ofMillis(500);
	private static final MemoizedSupplier<List<Function<String, String[]>>> COMMAND_FUNCTIONS_SUPPLIER = MemoizedSupplier
			.create(() -> loadCommandsFunctions());
	private static final LoadingCache<String, Optional<File>> CACHE = Caffeine.newBuilder()
			.expireAfterAccess(Duration.ofSeconds(15)).build(lookup -> {
				return Optional.ofNullable(getFresh(lookup));
			});

	public static Optional<File> get(String input) {
		if (input == null)
			return Optional.empty();
		input = input.trim();
		if (input.isEmpty())
			return Optional.empty();
		return CACHE.get(input);
	}

	public static ListenableFuture<File> poll(String input) {
		return poll(input, null);
	}

	public static ListenableFuture<File> poll(String input, Duration interval) {
		if (interval == null)
			return poll(input, POLL_INTERVAL_DEFAULT);
		if (input != null)
			input = input.trim();
		return pollInternal(input, interval);
	}

	public static ListenableFuture<File> pollInternal(String input, Duration interval) {
		if (input == null || input.isEmpty())
			return FutureUtils.immediateFailureFuture(new IllegalArgumentException("input required"));
		{
			var fileOp = CACHE.getIfPresent(input);
			if (fileOp != null && fileOp.isPresent())
				return FutureUtils.immediateResultFuture(fileOp.get());
		}
		return FutureUtils.scheduleWhile(Threads.Pools.centralPool(), interval.toMillis(), false, () -> {
			var fileOp = Which.get(input);
			if (fileOp.isEmpty())
				CACHE.invalidate(input);
			return fileOp.orElse(null);
		}, file -> file == null);
	}

	private static File getFresh(String input) {
		var fileOp = asFile(input);
		if (fileOp.isPresent())
			return fileOp.get();
		List<File> results = new ArrayList<>();
		for (var commandsFunction : COMMAND_FUNCTIONS_SUPPLIER.get()) {
			var commands = commandsFunction.apply(input);
			var lines = execute(commands);
			for (var line : lines)
				asFile(line).ifPresent(results::add);
		}
		if (results.isEmpty())
			return null;
		Comparator<File> sorter = Comparator.comparing(f -> {
			return f.canExecute() ? 0 : 1;
		});
		sorter = sorter.thenComparing(f -> {
			return -1 * f.lastModified();
		});
		sorter = sorter.thenComparing(f -> {
			return isNativeExecutable(f) ? 0 : 1;
		});
		sorter = sorter.thenComparing(f -> {
			return getExtension(f).map(String::length).map(v -> -1 * v).orElse(0);
		});
		results.sort(sorter);
		return results.get(0);
	}

	private static List<Function<String, String[]>> loadCommandsFunctions() {
		List<Function<String, String[]>> commandsFunctions = new ArrayList<>();
		List<String> attempts = List.of("where", "which");
		Predicate<List<String>> outputPredicate = lines -> {
			if (lines == null || lines.isEmpty())
				return false;
			return lines.stream().anyMatch(v -> asFile(v).isPresent());
		};
		for (var attempt : attempts) {
			var lines = execute(attempt, "echo");
			if (outputPredicate.test(lines))
				commandsFunctions.add(v -> new String[] { attempt, v });
		}
		{// powershell
			Function<String, String[]> commandsFunction = v -> {
				var script = String.format("gcm %s | select Source -expandproperty Source", v);
				return new String[] { "powershell.exe", "/C", script };
			};
			var lines = execute(commandsFunction.apply("cmd"));
			if (outputPredicate.test(lines))
				commandsFunctions.add(commandsFunction);
		}
		return Collections.unmodifiableList(commandsFunctions);
	}

	private static List<String> execute(String... commands) {
		if (commands == null || commands.length == 0)
			return List.of();
		try {
			var lines = new ProcessExecutor(commands).readOutput(true).exitValueNormal().execute().getOutput()
					.getLinesAsUTF8();
			return Collections.unmodifiableList(lines);
		} catch (Throwable t) {
			// suppress
		}
		return List.of();
	}

	private static Optional<File> asFile(String input) {
		if (input == null)
			return Optional.empty();
		input = input.trim();
		if (input.isEmpty())
			return Optional.empty();
		try {
			var file = new File(input);
			if (file.exists())
				return Optional.of(file);
		} catch (Throwable t) {
			return Optional.empty();
			// suppress
		}
		return Optional.empty();
	}

	private static Optional<String> getExtension(File file) {
		if (file == null)
			return Optional.empty();
		var name = file.getName();
		var index = name.lastIndexOf(".");
		if (index == -1)
			return Optional.empty();
		var ext = name.substring(index + 1, name.length());
		if (ext.isEmpty())
			return Optional.empty();
		return Optional.of(ext);
	}

	private static boolean isNativeExecutable(File file) {
		var ext = getExtension(file).orElse(null);
		if (ext == null)
			return false;
		if (!MachineConfig.isUnix()) {
			if ("exe".equalsIgnoreCase(ext))
				return true;
			if ("cmd".equalsIgnoreCase(ext))
				return true;
			if ("bat".equalsIgnoreCase(ext))
				return true;
		} else {
			if ("sh".equalsIgnoreCase(ext))
				return true;
			if ("bin".equalsIgnoreCase(ext))
				return true;
		}
		return false;
	}

	public static void main(String[] args) throws IOException, InterruptedException, ExecutionException {
		System.out.println(Which.poll("java").get());

		System.out.println(Which.get("certinfo").orElse(null));
		System.out.println(Which.get("java").orElse(null));
	}

}
