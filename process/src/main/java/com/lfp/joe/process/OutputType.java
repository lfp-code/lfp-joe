package com.lfp.joe.process;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.function.BiConsumer;

import org.zeroturnaround.exec.ProcessExecutor;

import ch.qos.logback.classic.Level;

public enum OutputType {
	out {

		@Override
		InputStream getInputStream(Process process) {
			return process.getInputStream();
		}

		@Override
		public Level logLevel() {
			return Level.INFO;
		}

		@Override
		BiConsumer<ProcessExecutor, OutputStream> getOutputStreamConsumer() {
			return (pe, os) -> pe.redirectOutputAlsoTo(os);
		}

		@Override
		BiConsumer<ProcessExecutor, OutputStream> getOutputStreamConsumerReplace() {
			return (pe, os) -> pe.redirectOutput(os);
		}

	},
	err {

		@Override
		InputStream getInputStream(Process process) {
			return process.getErrorStream();
		}

		@Override
		public Level logLevel() {
			return Level.ERROR;
		}

		@Override
		BiConsumer<ProcessExecutor, OutputStream> getOutputStreamConsumer() {
			return (pe, os) -> pe.redirectErrorAlsoTo(os);
		}

		@Override
		BiConsumer<ProcessExecutor, OutputStream> getOutputStreamConsumerReplace() {
			return (pe, os) -> pe.redirectError(os);
		}

	};

	abstract InputStream getInputStream(Process process);

	abstract BiConsumer<ProcessExecutor, OutputStream> getOutputStreamConsumer();

	abstract BiConsumer<ProcessExecutor, OutputStream> getOutputStreamConsumerReplace();

	public abstract Level logLevel();

}
