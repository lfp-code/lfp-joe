package com.lfp.joe.process;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringEscapeUtils;
import org.zeroturnaround.exec.InvalidExitValueException;
import org.zeroturnaround.exec.ProcessExecutor;

import com.lfp.joe.core.function.Durations;
import com.lfp.joe.core.function.MemoizedSupplier;

public class ShellCommands {

	protected ShellCommands() {

	}

	private static final MemoizedSupplier<Optional<List<String>>> SUPPLIER = MemoizedSupplier.create(() -> lookup());

	private static Optional<List<String>> lookup() {
		var attempts = new ArrayList<List<String>>();
		{
			attempts.add(List.of("cmd.exe", "/c"));
			attempts.add(List.of("/bin/bash", "-c"));
			attempts.add(List.of("/bin/sh", "-c"));
		}
		var testToken = UUID.randomUUID().toString().replaceAll("\\W+", "");
		for (var attempt : attempts) {
			List<String> shellCommands;
			try {
				shellCommands = lookup(testToken, attempt);
			} catch (Throwable t) {
				continue;
			}
			if (shellCommands != null)
				return Optional.of(Collections.unmodifiableList(shellCommands));
		}
		return Optional.empty();
	}

	private static List<String> lookup(String testToken, List<String> commands)
			throws InvalidExitValueException, IOException, InterruptedException, TimeoutException, ExecutionException {
		var testTokenCommand = String.format("echo \"%s\"", testToken);
		var pe = new ProcessExecutor();
		pe.command(Stream.concat(commands.stream(), Stream.of(testTokenCommand)).collect(Collectors.toList()));
		pe.readOutput(true);
		pe.exitValueNormal();
		var startedProcess = pe.start();
		var info = startedProcess.getProcess().info();
		List<String> shellCommands = new ArrayList<>();
		info.command().ifPresent(v -> {
			shellCommands.add(v);
			info.arguments().ifPresent(args -> {
				for (var arg : args)
					shellCommands.add(arg);
			});
		});
		var processResult = startedProcess.getFuture().get();
		var output = processResult.outputUTF8();
		if (!output.contains(testToken))
			return null;
		var newCommands = IntStream.range(0, commands.size()).mapToObj(i -> {
			if (i < shellCommands.size())
				return shellCommands.get(i);
			return commands.get(i);
		}).collect(Collectors.toList());
		return newCommands;
	}

	@SuppressWarnings("deprecation")
	private static String normalizeArguments(String arguments) {
		if (arguments == null)
			return arguments;
		var split = arguments.split("\\s");
		if (split.length == 1)
			return arguments;
		var mod = false;
		var sb = new StringBuilder();
		for (var part : split) {
			try {
				File file = new File(part);
				if (file.exists()) {
					part = StringEscapeUtils.escapeJava(part);
					mod = true;
				}
			} catch (Throwable t) {
				// noop
			}
			if (sb.length() > 0)
				sb.append(" ");
			sb.append(part);
		}
		if (mod)
			return sb.toString();
		return arguments;
	}

	public static Optional<List<String>> get() {
		return SUPPLIER.get();
	}

	public static List<String> stripFromCommand(List<String> command) {
		if (command == null || command.isEmpty())
			return List.of();
		var shellCommands = ShellCommands.get().orElse(null);
		if (shellCommands == null)
			return Collections.unmodifiableList(command);
		int offset = 0;
		for (int i = 0; i < shellCommands.size(); i++) {
			var shellCommandat = shellCommands.get(i);
			var commandAt = command.size() > i ? command.get(i) : null;
			if (Objects.equals(shellCommandat, commandAt))
				offset++;
			else {
				offset = 0;
				break;
			}
		}
		if (offset > 0)
			command = command.subList(offset, command.size());
		if (command.size() == 1) {
			var arguments = command.get(0);
			arguments = normalizeArguments(arguments);
			var tokenizedCommand = CommandSplitter.get().apply(arguments);
			if (tokenizedCommand != null && !tokenizedCommand.isEmpty())
				return Collections.unmodifiableList(tokenizedCommand);
		}
		return Collections.unmodifiableList(command);
	}

	public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			var nanoTime = System.nanoTime();
			System.out.println(stripFromCommand(List.of("C:\\Windows\\System32\\cmd.exe", "/c", "echo")));
			var elapsed = Duration.ofNanos(System.nanoTime() - nanoTime);
			System.out.println(Durations.toMillis(elapsed).doubleValue());
		}
	}
}
