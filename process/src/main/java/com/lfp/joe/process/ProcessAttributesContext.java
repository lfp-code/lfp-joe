package com.lfp.joe.process;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.zeroturnaround.exec.ProcessExecutor;

import com.lfp.joe.core.function.Throws;

@SuppressWarnings("unchecked")
public class ProcessAttributesContext {

	private static final Method ProcessExecutor_getAttributes_METHOD = Throws.unchecked(() -> {
		var method = ProcessExecutor.class.getDeclaredMethod("getAttributes");
		method.setAccessible(true);
		return method;
	});

	private static final Class<?> ProcessAttributes_CLASS_TYPE = ProcessExecutor_getAttributes_METHOD.getReturnType();

	private static final Field ProcessExecutor_command_FIELD = Throws.unchecked(() -> {
		var method = ProcessAttributes_CLASS_TYPE.getDeclaredField("command");
		method.setAccessible(true);
		return method;
	});
	private static final Field ProcessExecutor_directory_FIELD = Throws.unchecked(() -> {
		var method = ProcessAttributes_CLASS_TYPE.getDeclaredField("directory");
		method.setAccessible(true);
		return method;
	});

	private static final Field ProcessExecutor_environment_FIELD = Throws.unchecked(() -> {
		var method = ProcessAttributes_CLASS_TYPE.getDeclaredField("environment");
		method.setAccessible(true);
		return method;
	});

	private static final Field ProcessExecutor_allowedExitValues_FIELD = Throws.unchecked(() -> {
		var method = ProcessAttributes_CLASS_TYPE.getDeclaredField("allowedExitValues");
		method.setAccessible(true);
		return method;
	});

	private final Object processAttributes;

	public ProcessAttributesContext(Object processAttributes) {
		Objects.requireNonNull(processAttributes);
		if (!ProcessAttributes_CLASS_TYPE.isAssignableFrom(processAttributes.getClass()))
			throw new IllegalArgumentException(
					String.format("invalid processAttributes. requiredType:%s paramaterType:%s",
							ProcessAttributes_CLASS_TYPE.getName(), processAttributes.getClass().getName()));
		this.processAttributes = processAttributes;
	}

	public List<String> getCommand() {
		var result = (List<String>) Throws.unchecked(() -> ProcessExecutor_command_FIELD.get(this.processAttributes));
		return Collections.unmodifiableList(result);
	}

	public List<String> getCommand(boolean removeShellCommands) {
		var command = getCommand();
		if (!removeShellCommands)
			return command;
		return ShellCommands.stripFromCommand(command);
	}

	public File getDirectory() {
		var result = (File) Throws.unchecked(() -> ProcessExecutor_directory_FIELD.get(this.processAttributes));
		return result;
	}

	public Map<String, String> getEnvironment() {
		var result = (Map<String, String>) Throws
				.unchecked(() -> ProcessExecutor_environment_FIELD.get(this.processAttributes));
		return Collections.unmodifiableMap(result);
	}

	public Set<Integer> getAllowedExitValues() {
		var result = (Set<Integer>) Throws
				.unchecked(() -> ProcessExecutor_allowedExitValues_FIELD.get(this.processAttributes));
		return Collections.unmodifiableSet(result);
	}

}
