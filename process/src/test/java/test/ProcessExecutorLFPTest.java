package test;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletionException;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.io.FileExt;
import com.lfp.joe.core.log.Logging;
import com.lfp.joe.process.ProcessExecutorLFP;
import com.lfp.joe.process.Procs;

public class ProcessExecutorLFPTest {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = Logging.logger();

	public static void main(String[] args)
			throws CancellationException, CompletionException, IOException, InterruptedException {
		LOGGER.info("test");
		{
			var pactProc = new ProcessExecutorLFP()
					.command(System.getenv().get("ProgramFiles") + "\\Git\\bin\\bash.exe", "-c", "mvn --version")
					.readOutput(true)
					.start();
			pactProc.listener(() -> {
				System.out.println("done");
			});
			var pr = pactProc.join();
			pactProc.cancel(true);
			System.out.println(pr.getOutput().getUTF8());
			System.out.println();
		}
		{
			var script = "trap printout SIGINT\n" + "printout() {\n" + "    echo \"\"\n"
					+ "    echo \"Finished with count=$count\"\n" + "    exit\n" + "}\n" + "while :\n" + "do\n"
					+ "    ((count++))\n" + "    echo \"hi $count\"\n" + "    sleep 1\n" + "done";
			try (var file = new FileExt(MachineConfig.getTempDirectory(), UUID.randomUUID().toString() + "_script.sh")
					.deleteAllOnScrap(true);) {
				var writer = file.outputStream().writer();
				writer.write(script);
				writer.close();
				var pactProc = new ProcessExecutorLFP()
						.command(System.getenv().get("ProgramFiles") + "\\Git\\bin\\bash.exe", file.getAbsolutePath())
						.readOutput(true)
						.logOutput()
						.start();
				Thread.sleep(5_000);
				pactProc.cancel(true);
				System.out.println();
			}
		}
		System.out.println(Procs.execute("java --version").outputUTF8());
		Thread.currentThread().join();
	}

}
