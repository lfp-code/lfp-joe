package com.lfp.joe.threads.future.transform;

import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.function.BiConsumer;

import org.threadly.concurrent.future.FutureCallback;
import org.threadly.concurrent.future.ListenableFuture;

import com.lfp.joe.core.classpath.Instances.Autowired;



@SuppressWarnings({ "unchecked", "rawtypes" })

@Autowired(priority = Integer.MAX_VALUE - 1)
public class CallbackConsumerThreadly extends CallbackConsumer.Abs<ListenableFuture> {

	@Override
	protected Class<? extends ListenableFuture> classType() {
		return ListenableFuture.class;
	}

	@Override
	protected Optional<Boolean> applyInternal(ListenableFuture future, BiConsumer<?, ? super Throwable> action,
			Executor executor) {
		var futureCallback = new FutureCallback() {

			@Override
			public void handleResult(Object result) {
				((BiConsumer<Object, ? super Throwable>) action).accept(result, null);
			}

			@Override
			public void handleFailure(Throwable t) {
				action.accept(null, t);
			}
		};
		if (executor == null)
			future.callback(futureCallback);
		else
			future.callback(futureCallback, executor);
		return Optional.of(true);
	}

}
