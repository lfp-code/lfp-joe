package com.lfp.joe.threads.cpu;

import java.time.Duration;
import java.util.Optional;

import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.core.config.MachineConfig;

public class CPUMonitorWindows extends AbstractCPUMonitor {

	public static Optional<CPUMonitorWindows> tryGet() {
		if (!MachineConfig.isWindows())
			return Optional.empty();
		var instance = Instances.get(CPUMonitorWindows.class, () -> new CPUMonitorWindows(null));
		return Optional.of(instance);
	}

	protected CPUMonitorWindows(Duration maxHistory) {
		super(Duration.ofMillis(250), maxHistory);
	}

	@Override
	protected Double poll() throws Exception {
		var command = "typeperf \"\\Processor Information(_Total)\\% Processor Utility\"";
		var process = Runtime.getRuntime().exec(command);
		poll(process);
		return null;
	}

	@Override
	protected Double parseUsage(String line) {
		var usage = super.parseUsage(line);
		return usage == null ? null : usage / 100;
	}

	public static void main(String[] args) throws InterruptedException {
		var monitor = new CPUMonitorWindows(Duration.ofHours(1));
		monitor.addUpdateListener(() -> {
			System.out.println(monitor.getAverage(Duration.ofSeconds(1)).orElse(-1));
		});
		Thread.sleep(30_000);
		monitor.scrap();
		Thread.currentThread().join();
	}

}
