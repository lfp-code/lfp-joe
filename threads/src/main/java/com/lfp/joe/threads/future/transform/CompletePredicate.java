package com.lfp.joe.threads.future.transform;

import java.lang.reflect.Method;
import java.util.AbstractMap.SimpleEntry;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.stream.Stream;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.MethodRequest;
import com.lfp.joe.core.classpath.Instances.Autowired;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.core.process.future.CSFutureTask;
import com.lfp.joe.core.process.future.Completion;

@SuppressWarnings({ "unchecked", "rawtypes" })

@Autowired
public class CompletePredicate extends FutureTransform.Abs<Entry<Object, Throwable>, Boolean, Future> {

	private static final List<String> COMPLETE_METHOD_NAMES = List.of("complete", "setResult", "set",
			"completeWithResult");
	private static final List<String> COMPLETE_EXCEPTIONALLY_METHOD_NAMES = List.of("completeExceptionally",
			"setFailure", "setException", "completeWithFailure");

	@Override
	protected Class<? extends Future> classType() {
		return Future.class;
	}

	@Override
	protected Optional<Boolean> applyInternal(Future future, Entry<Object, Throwable> input) {
		if (input == null)
			return Optional.empty();
		if (future instanceof CompletableFuture)
			return applyInternalCompletableFuture((CompletableFuture) future, input);
		if (future.isDone())
			return Optional.empty();
		var completion = Completion.of(input.getKey(), input.getValue());
		if (completion.isCancellation())
			return Optional.of(future.cancel(completion.isInterruption()));
		var method = streamMethodEntries(future).map(ent -> {
			return completion.isSuccess() ? ent.getKey() : ent.getValue();
		}).filter(v -> v.getDeclaringClass().isInstance(future)).findFirst().orElse(null);
		if (method == null)
			return Optional.empty();
		Boolean isDone;
		if (CoreReflections.isAssignableFrom(method.getReturnType(), boolean.class))
			isDone = null;
		else
			isDone = future.isDone();
		Boolean returnValue;
		{
			var argument = completion.isSuccess() ? completion.result() : completion.failure();
			var returnValueObj = Throws.unchecked(() -> method.invoke(future, argument));
			returnValue = Optional.ofNullable(returnValueObj)
					.flatMap(v -> CoreReflections.tryCast(v, Boolean.class))
					.orElse(null);
		}
		if (returnValue != null)
			return Optional.of(returnValue);
		return Optional.of(!Objects.equals(isDone, future.isDone()));
	}

	private Optional<Boolean> applyInternalCompletableFuture(CompletableFuture future, Entry<Object, Throwable> input) {
		// special case to use obtrude dude
		var completion = Completion.of(input.getKey(), input.getValue());
		if (completion.isSuccess()) {
			if (future.complete(completion.result()))
				return Optional.of(true);
		} else {
			if (future.completeExceptionally(completion.failure()))
				return Optional.of(true);
		}
		if (!completion.isInterruption() || !future.isCancelled())
			return Optional.of(false);
		if (!Completion.getNow(future).isInterruption())
			future.obtrudeException(completion.failure());
		return Optional.of(false);
	}

	private static Stream<Entry<Method, Method>> streamMethodEntries(Future future) {
		var completeMethod = getMethod(future, COMPLETE_METHOD_NAMES, Object.class);
		if (completeMethod == null)
			return Stream.empty();
		var completeExceptionallyMethod = getMethod(future, COMPLETE_EXCEPTIONALLY_METHOD_NAMES, Throwable.class);
		if (completeExceptionallyMethod == null)
			return Stream.empty();
		return Stream.of(Map.entry(completeMethod, completeExceptionallyMethod));
	}

	private static Method getMethod(Future future, List<String> methodNames, Class<?> parameterType) {
		for (var name : methodNames) {
			var method = MemberCache.tryGetMethod(MethodRequest.of(future.getClass(), null, name, parameterType), true)
					.orElse(null);
			if (method != null)
				return method;
		}
		return null;
	}

	public static void main(String[] args) {
		var pred = new CompletePredicate();
		var csFutureTask = new CSFutureTask<>(() -> {
			return new Date();
		});
		CoreTasks.executor().execute(csFutureTask);
		System.out.println(csFutureTask.isDone());
		var result = pred.apply(csFutureTask, new SimpleEntry<>(new Date(), null));
		System.out.println(result);
		System.out.println(csFutureTask.isDone());
	}

}
