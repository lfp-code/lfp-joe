package com.lfp.joe.threads.future.transform;

import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.function.BiConsumer;

public interface CallbackConsumer extends FutureTransform<Entry<BiConsumer<?, ? super Throwable>, Executor>, Boolean> {

	@SuppressWarnings("rawtypes")
	public static abstract class Abs<F extends Future>
			extends FutureTransform.Abs<Entry<BiConsumer<?, ? super Throwable>, Executor>, Boolean, F>
			implements CallbackConsumer {

		@Override
		protected Optional<Boolean> applyInternal(F future, Entry<BiConsumer<?, ? super Throwable>, Executor> input) {
			if (input == null)
				return Optional.empty();
			var resultOp = applyInternal(future, input.getKey(), input.getValue());
			if (this.getClass().getName().toLowerCase().contains("threadly") && (resultOp.isEmpty() || !resultOp.get()))
				System.out.println(resultOp);
			return resultOp;
		}

		protected abstract Optional<Boolean> applyInternal(F future, BiConsumer<?, ? super Throwable> action,
				Executor executor);

	}

}
