
package com.lfp.joe.threads;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.time.Duration;
import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadFactory;

import org.threadly.concurrent.CentralThreadlyPool;
import org.threadly.concurrent.ConfigurableThreadFactory;
import org.threadly.concurrent.PriorityScheduler;
import org.threadly.concurrent.PrioritySchedulerService;
import org.threadly.concurrent.ReschedulingOperation;
import org.threadly.concurrent.SchedulerService;
import org.threadly.concurrent.SubmitterScheduler;
import org.threadly.concurrent.TaskPriority;
import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.watchdog.MixedTimeWatchdog;
import org.threadly.concurrent.wrapper.limiter.SchedulerServiceLimiter;
import org.threadly.concurrent.wrapper.priority.DefaultPriorityWrapper;
import org.threadly.concurrent.wrapper.traceability.ThreadRenamingSchedulerService;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Muto;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Pools.ProtectedAccess.DynamicGenericThreadLimiterAccess;
import com.lfp.joe.threads.Pools.ProtectedAccess.PerTaskSizingSubmitterSchedulerAccess;
import com.lfp.joe.threads.Pools.ProtectedAccess.SinglePriorityThreadSubPoolAccess;
import com.lfp.joe.threads.executor.ExecutorServices.ShutdownUnsupported;
import com.lfp.joe.threads.executor.SubmitterSchedulerLFP;

import one.util.streamex.IntStreamEx;

public class Pools {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String THREAD_POOL_NAME = "PoolsLFP";
	private static final Muto<SchedulerService> CENTRAL_WATCHDOG_SCHEDULER_REF = Muto.create();
	private static final Map<Boolean, MixedTimeWatchdog> CENTRAL_WATCHDOG_CACHE = new ConcurrentHashMap<>(2);
	static {
		int cpuCount = MachineConfig.logicalCoreCount();
		int genericThreadCount = getGenericThreadCount();
		var masterScheduler = updateStaticField("MASTER_SCHEDULER", () -> {
			int poolSize = cpuCount + genericThreadCount + 1;
			ThreadFactory threadFactory = new ConfigurableThreadFactory(THREAD_POOL_NAME + "-Master", false, true,
					Thread.NORM_PRIORITY, null, null, null);
			return new PriorityScheduler(poolSize, TaskPriority.High, ProtectedAccess.LOW_PRIORITY_MAX_WAIT_IN_MS(),
					false, threadFactory);
		});
		var LOW_PRIORITY_MASTER_SCHEDULER = updateStaticField("LOW_PRIORITY_MASTER_SCHEDULER", () -> {
			return new DefaultPriorityWrapper(masterScheduler, TaskPriority.Low);
		});
		updateStaticField("STARVABLE_PRIORITY_MASTER_SCHEDULER", () -> {
			return new DefaultPriorityWrapper(masterScheduler, TaskPriority.Starvable);
		});
		updateStaticField("POOL_SIZE_UPDATER", () -> {
			return ProtectedAccess.poolResizeUpdater(LOW_PRIORITY_MASTER_SCHEDULER);
		});
		updateStaticField("COMPUTATION_POOL", () -> {
			SchedulerService computationPool = new ThreadRenamingSchedulerService(masterScheduler,
					THREAD_POOL_NAME + "-Computation", false);
			return new SchedulerServiceLimiter(computationPool, cpuCount);
		});
		updateStaticField("LOW_PRIORITY_POOL", () -> {
			var lowPriorityPool = new DynamicGenericThreadLimiterAccess(TaskPriority.Low, 0, -1,
					THREAD_POOL_NAME + "-LowPriority", true);
			return lowPriorityPool;
		});
		updateStaticField("SINGLE_THREADED_LOW_PRIORITY_POOL", () -> {
			return CoreReflections.newInstanceUnchecked(SinglePriorityThreadSubPoolAccess.class, TaskPriority.Low,
					false, THREAD_POOL_NAME + "-SingleThreadLowPriority", true, Thread.MIN_PRIORITY);
		});
		updateStaticField("PER_TASK_SIZING_POOL", () -> {
			return CoreReflections.newInstanceUnchecked(PerTaskSizingSubmitterSchedulerAccess.class);
		});
	}

	private static enum CentralPool implements SubmitterSchedulerLFP, ShutdownUnsupported {
		INSTANCE;

		@Override
		public Executor getContainedExecutor() {
			return CoreTasks.executor();
		}

		@Override
		public void schedule(Runnable task, long delayInMs) {
			CoreTasks.delayedExecutor(Duration.ofMillis(delayInMs)).execute(task);
		}
	}

	static {
		if (MachineConfig.isDeveloper()) {
			var fieldStream = Streams.of(CoreReflections.streamFields(THIS_CLASS, true));
			fieldStream = fieldStream.filter(v -> Modifier.isStatic(v.getModifiers()));
			fieldStream = fieldStream.filter(v -> Executor.class.isAssignableFrom(v.getType()));
			var fieldValueStream = fieldStream.mapToEntry(v -> Throws.unchecked(() -> v.get(null)))
					.selectValues(Executor.class);
			fieldValueStream = fieldValueStream.filterValues(v -> {
				if (DefaultPriorityWrapper.class.isInstance(v))
					return false;
				if (PerTaskSizingSubmitterSchedulerAccess.class.isInstance(v))
					return false;
				return true;
			});
			Comparator<Entry<Field, Executor>> sorter = Comparator.comparingInt(ent -> {
				if (ProtectedAccess.MASTER_SCHEDULER().equals(ent.getValue()))
					return Integer.MIN_VALUE;
				if (CentralPool.INSTANCE.equals(ent.getValue()))
					return Integer.MAX_VALUE;
				return 0;
			});
			sorter = sorter.thenComparing(ent -> ent.getKey().getName().toLowerCase());
			fieldValueStream.sorted(sorter).forEach(ent -> {
				LOGGER.info("initialized pool:{}", ent.getKey().getName());
			});
		}
	}

	public static SubmitterSchedulerLFP centralPool() {
		return CentralPool.INSTANCE;
	}

	protected static SchedulerService centralWatchdogScheduler() {
		if (CENTRAL_WATCHDOG_SCHEDULER_REF.get() == null)
			synchronized (CENTRAL_WATCHDOG_SCHEDULER_REF) {
				if (CENTRAL_WATCHDOG_SCHEDULER_REF.get() == null)
					CENTRAL_WATCHDOG_SCHEDULER_REF.set(Pools.threadPool(2, THREAD_POOL_NAME + "-Watchdog"));
			}
		return CENTRAL_WATCHDOG_SCHEDULER_REF.get();
	}

	public static MixedTimeWatchdog centralWatchdog(boolean sendInterruptOnFutureCancel) {
		return CENTRAL_WATCHDOG_CACHE.computeIfAbsent(sendInterruptOnFutureCancel, nil -> {
			return new MixedTimeWatchdog(centralWatchdogScheduler(), sendInterruptOnFutureCancel);
		});
	}

	private static <V> V updateStaticField(String fieldName, ThrowingSupplier<V, ?> loader) {
		V value = loader.unchecked().get();
		Objects.requireNonNull(value);
		var fieldRequest = FieldRequest.of(CentralThreadlyPool.class, null, fieldName);
		MemberCache.setFieldValue(fieldRequest, null, value);
		return value;
	}

	/**
	 * ++++++++++++++++++++ BEGIN STATIC DELEGATE METHODS ++++++++++++++++++++
	 **/

	protected static class ProtectedAccess extends CentralThreadlyPool {

		static PriorityScheduler MASTER_SCHEDULER() {
			return MASTER_SCHEDULER;
		}

		static int LOW_PRIORITY_MAX_WAIT_IN_MS() {
			return LOW_PRIORITY_MAX_WAIT_IN_MS;
		}

		static ReschedulingOperation poolResizeUpdater(SubmitterScheduler scheduler) {
			return CoreReflections.newInstanceUnchecked(PoolResizeUpdater.class, LOW_PRIORITY_MASTER_SCHEDULER);
		}

		static class PerTaskSizingSubmitterSchedulerAccess extends PerTaskSizingSubmitterScheduler {}

		static class DynamicGenericThreadLimiterAccess extends DynamicGenericThreadLimiter {

			public DynamicGenericThreadLimiterAccess(TaskPriority priority, int guaranteedThreads, int maxThreads,
					String threadName, boolean replaceName) {
				super(priority, guaranteedThreads, maxThreads, threadName, replaceName);
			}
		}

		static class SinglePriorityThreadSubPoolAccess extends SinglePriorityThreadSubPool {

			protected SinglePriorityThreadSubPoolAccess(TaskPriority tickPriority, boolean threadGuaranteed,
					String threadName, boolean replaceName, int threadPriority) {
				super(tickPriority, threadGuaranteed, threadName, replaceName, threadPriority);
			}
		}

	}

	/**
	 * Delegate method of {@link CentralThreadlyPool#increaseGenericThreads(int)}
	 * 
	 * @see CentralThreadlyPool#increaseGenericThreads(int)
	 */
	public static void increaseGenericThreads(int count) {
		CentralThreadlyPool.increaseGenericThreads(count);
	}

	/**
	 * Delegate method of {@link CentralThreadlyPool#getGenericThreadCount()}
	 * 
	 * @see CentralThreadlyPool#getGenericThreadCount()
	 */
	public static int getGenericThreadCount() {
		return CentralThreadlyPool.getGenericThreadCount();
	}

	/**
	 * Delegate method of {@link CentralThreadlyPool#computationPool()}
	 * 
	 * @see CentralThreadlyPool#computationPool()
	 */
	public static SchedulerService computationPool() {
		return CentralThreadlyPool.computationPool();
	}

	/**
	 * Delegate method of {@link CentralThreadlyPool#computationPool(String)}
	 * 
	 * @see CentralThreadlyPool#computationPool(String)
	 */
	public static SchedulerService computationPool(String threadName) {
		return CentralThreadlyPool.computationPool(threadName);
	}

	/**
	 * Delegate method of {@link CentralThreadlyPool#lowPrioritySingleThreadPool()}
	 * 
	 * @see CentralThreadlyPool#lowPrioritySingleThreadPool()
	 */
	public static PrioritySchedulerService lowPrioritySingleThreadPool() {
		return CentralThreadlyPool.lowPrioritySingleThreadPool();
	}

	/**
	 * Delegate method of
	 * {@link CentralThreadlyPool#lowPrioritySingleThreadPool(String)}
	 * 
	 * @see CentralThreadlyPool#lowPrioritySingleThreadPool(String)
	 */
	public static PrioritySchedulerService lowPrioritySingleThreadPool(String threadName) {
		return CentralThreadlyPool.lowPrioritySingleThreadPool(threadName);
	}

	/**
	 * Delegate method of {@link CentralThreadlyPool#lowPriorityPool()}
	 * 
	 * @see CentralThreadlyPool#lowPriorityPool()
	 */
	public static SchedulerService lowPriorityPool() {
		return CentralThreadlyPool.lowPriorityPool();
	}

	/**
	 * Delegate method of {@link CentralThreadlyPool#lowPriorityPool(String)}
	 * 
	 * @see CentralThreadlyPool#lowPriorityPool(String)
	 */
	public static SchedulerService lowPriorityPool(String threadName) {
		return CentralThreadlyPool.lowPriorityPool(threadName);
	}

	/**
	 * Delegate method of {@link CentralThreadlyPool#singleThreadPool()}
	 * 
	 * @see CentralThreadlyPool#singleThreadPool()
	 */
	public static PrioritySchedulerService singleThreadPool() {
		return CentralThreadlyPool.singleThreadPool();
	}

	/**
	 * Delegate method of {@link CentralThreadlyPool#singleThreadPool(String)}
	 * 
	 * @see CentralThreadlyPool#singleThreadPool(String)
	 */
	public static PrioritySchedulerService singleThreadPool(String threadName) {
		return CentralThreadlyPool.singleThreadPool(threadName);
	}

	/**
	 * Delegate method of {@link CentralThreadlyPool#singleThreadPool(boolean)}
	 * 
	 * @see CentralThreadlyPool#singleThreadPool(boolean)
	 */
	public static PrioritySchedulerService singleThreadPool(boolean threadGuaranteed) {
		return CentralThreadlyPool.singleThreadPool(threadGuaranteed);
	}

	/**
	 * Delegate method of
	 * {@link CentralThreadlyPool#singleThreadPool(boolean, String)}
	 * 
	 * @see CentralThreadlyPool#singleThreadPool(boolean, String)
	 */
	public static PrioritySchedulerService singleThreadPool(boolean threadGuaranteed, String threadName) {
		return CentralThreadlyPool.singleThreadPool(threadGuaranteed, threadName);
	}

	/**
	 * Delegate method of
	 * {@link CentralThreadlyPool#singleThreadPool(boolean, String, int)}
	 * 
	 * @see CentralThreadlyPool#singleThreadPool(boolean, String, int)
	 */
	public static PrioritySchedulerService singleThreadPool(boolean threadGuaranteed, String threadName,
			int threadPriority) {
		return CentralThreadlyPool.singleThreadPool(threadGuaranteed, threadName, threadPriority);
	}

	/**
	 * Delegate method of {@link CentralThreadlyPool#threadPool(int)}
	 * 
	 * @see CentralThreadlyPool#threadPool(int)
	 */
	public static SchedulerService threadPool(int threadCount) {
		return CentralThreadlyPool.threadPool(threadCount);
	}

	/**
	 * Delegate method of {@link CentralThreadlyPool#threadPool(int, String)}
	 * 
	 * @see CentralThreadlyPool#threadPool(int, String)
	 */
	public static SchedulerService threadPool(int threadCount, String threadName) {
		return CentralThreadlyPool.threadPool(threadCount, threadName);
	}

	/**
	 * Delegate method of {@link CentralThreadlyPool#threadPool(TaskPriority, int)}
	 * 
	 * @see CentralThreadlyPool#threadPool(TaskPriority, int)
	 */
	public static SchedulerService threadPool(TaskPriority priority, int threadCount) {
		return CentralThreadlyPool.threadPool(priority, threadCount);
	}

	/**
	 * Delegate method of
	 * {@link CentralThreadlyPool#threadPool(TaskPriority, int, String)}
	 * 
	 * @see CentralThreadlyPool#threadPool(TaskPriority, int, String)
	 */
	public static SchedulerService threadPool(TaskPriority priority, int threadCount, String threadName) {
		return CentralThreadlyPool.threadPool(priority, threadCount, threadName);
	}

	/**
	 * Delegate method of {@link CentralThreadlyPool#rangedThreadPool(int, int)}
	 * 
	 * @see CentralThreadlyPool#rangedThreadPool(int, int)
	 */
	public static SchedulerService rangedThreadPool(int guaranteedThreads, int maxThreads) {
		return CentralThreadlyPool.rangedThreadPool(guaranteedThreads, maxThreads);
	}

	/**
	 * Delegate method of
	 * {@link CentralThreadlyPool#rangedThreadPool(int, int, String)}
	 * 
	 * @see CentralThreadlyPool#rangedThreadPool(int, int, String)
	 */
	public static SchedulerService rangedThreadPool(int guaranteedThreads, int maxThreads, String threadName) {
		return CentralThreadlyPool.rangedThreadPool(guaranteedThreads, maxThreads, threadName);
	}

	/**
	 * Delegate method of
	 * {@link CentralThreadlyPool#rangedThreadPool(TaskPriority, int, int)}
	 * 
	 * @see CentralThreadlyPool#rangedThreadPool(TaskPriority, int, int)
	 */
	public static SchedulerService rangedThreadPool(TaskPriority priority, int guaranteedThreads, int maxThreads) {
		return CentralThreadlyPool.rangedThreadPool(priority, guaranteedThreads, maxThreads);
	}

	/**
	 * Delegate method of
	 * {@link CentralThreadlyPool#rangedThreadPool(TaskPriority, int, int, String)}
	 * 
	 * @see CentralThreadlyPool#rangedThreadPool(TaskPriority, int, int, String)
	 */
	public static SchedulerService rangedThreadPool(TaskPriority priority, int guaranteedThreads, int maxThreads,
			String threadName) {
		return CentralThreadlyPool.rangedThreadPool(priority, guaranteedThreads, maxThreads, threadName);
	}

	/**
	 * Delegate method of {@link CentralThreadlyPool#isolatedTaskPool()}
	 * 
	 * @see CentralThreadlyPool#isolatedTaskPool()
	 */
	public static SchedulerService isolatedTaskPool() {
		return CentralThreadlyPool.isolatedTaskPool();
	}

	/**
	 * Delegate method of {@link CentralThreadlyPool#isolatedTaskPool(String)}
	 * 
	 * @see CentralThreadlyPool#isolatedTaskPool(String)
	 */
	public static SchedulerService isolatedTaskPool(String threadName) {
		return CentralThreadlyPool.isolatedTaskPool(threadName);
	}

	public static void main(String[] args) throws InterruptedException {
		var exec = Pools.computationPool();
		var futures = IntStreamEx.range(50).mapToObj(index -> {
			var future = exec.submitScheduled(() -> {
				try {
					for (int j = 0;; j++) {
						var names = Thread.currentThread().getName();
						System.out.println("sleeping - " + names);
						Thread.sleep(10_000);

					}
				} catch (Throwable t) {
					System.err.println(t.getMessage());
					throw t;
				}
			}, Duration.ofSeconds(1).toMillis());
			Threads.Futures.callback(future, () -> {
				var names = Thread.currentThread().getName();
				System.out.println("finished:" + index + " names:" + names);
			});
			return future;
		}).toList();
		System.out.println("called");
		Thread.sleep(5_000);
		FutureUtils.cancelIncompleteFutures(futures, true);
		System.err.println("cancel complete");
		Thread.currentThread().join();
	}

}
