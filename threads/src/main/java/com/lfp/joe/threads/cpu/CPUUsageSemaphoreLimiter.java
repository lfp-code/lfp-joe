package com.lfp.joe.threads.cpu;

import java.time.Duration;
import java.util.Optional;

import org.slf4j.Logger;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.lock.PermitObtainer;

public class CPUUsageSemaphoreLimiter extends PermitObtainer.Impl implements CPUUsageLimiter {

	private AbstractCPUUsageLimiter cpuLImiter;

	public CPUUsageSemaphoreLimiter(double cpuTargetUsage, Duration updateInterval, Integer initialMaxConcurrency) {
		this(cpuTargetUsage, updateInterval, initialMaxConcurrency, null, false);
	}

	public CPUUsageSemaphoreLimiter(double cpuTargetUsage, Duration updateInterval, Integer initialMaxConcurrency,
			Double cpuTargetUsageRange, boolean fair) {
		super(Optional.ofNullable(initialMaxConcurrency).orElseGet(MachineConfig::logicalCoreCount), fair, false);
		this.cpuLImiter = new AbstractCPUUsageLimiter(cpuTargetUsage, updateInterval, cpuTargetUsageRange) {

			@Override
			protected void setMaxConcurrency(int maxConcurrency) {
				CPUUsageSemaphoreLimiter.this.setMaxPermits(maxConcurrency);

			}

			@Override
			protected boolean hasCapacity() {
				return CPUUsageSemaphoreLimiter.this.availablePermits() > 0;
			}

			@Override
			public int getMaxConcurrency() {
				return CPUUsageSemaphoreLimiter.this.getMaxPermits();
			}
		};
	}

	@Override
	public boolean isScrapped() {
		return this.cpuLImiter.isScrapped();
	}

	@Override
	public boolean scrap() {
		return this.cpuLImiter.scrap();
	}

	@Override
	public Scrapable onScrap(Runnable runnable) {
		return this.cpuLImiter.onScrap(runnable);
	}

	@Override
	public void enableLogging(Logger logger) {
		this.cpuLImiter.enableLogging(logger);

	}

	@Override
	public void disableLogging() {
		this.cpuLImiter.disableLogging();

	}

	@Override
	public int getMaxConcurrency() {
		return this.cpuLImiter.getMaxConcurrency();
	}

}
