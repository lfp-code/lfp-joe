package com.lfp.joe.threads.future;

import java.lang.reflect.Method;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.threadly.concurrent.future.ListenableFuture;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.DistinctInstancePredicate;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.threads.Futures;

public interface ListenableFutureWrapper<X> extends ListenableFutureLFP<X> {

	Future<X> unwrap();

	// intercept wrapped ListenableFutures

	@Override
	default ListenableFuture<X> listener(Runnable listener, Executor executor,
			ListenerOptimizationStrategy optimizeExecution) {
		var lfuture = tryUnwrapListenableFuture().orElse(null);
		if (lfuture != null) {
			lfuture.listener(listener, executor, optimizeExecution);
			return this;
		}
		if (this.isDone()) {
			getImmediateListenerExecutor(executor, optimizeExecution).ifPresentOrElse(listenerExecutor -> {
				listenerExecutor.execute(listener);
			}, listener);
			return this;
		}
		getListenerExecutor(executor, optimizeExecution).ifPresentOrElse(listenerExecutor -> {
			Futures.callback(unwrap(), listener, listenerExecutor);
		}, () -> {
			Futures.callback(unwrap(), listener);
		});
		return this;
	}

	@Override
	default StackTraceElement[] getRunningStackTrace() {
		return tryUnwrapListenableFuture().map(f -> {
			return f.getRunningStackTrace();
		}).orElseGet(() -> {
			return ListenableFutureLFP.super.getRunningStackTrace();
		});
	}

	@Override
	default boolean isCompletedExceptionally() {
		return tryUnwrapListenableFuture().map(f -> {
			return f.isCompletedExceptionally();
		}).orElseGet(() -> {
			return ListenableFutureLFP.super.isCompletedExceptionally();
		});
	}

	@Override
	default Throwable getFailure() throws InterruptedException {
		var lfuture = tryUnwrapListenableFuture().orElse(null);
		if (lfuture != null)
			return lfuture.getFailure();
		return ListenableFutureLFP.super.getFailure();
	}

	@Override
	default Throwable getFailure(long timeout, TimeUnit unit) throws InterruptedException, TimeoutException {
		var lfuture = tryUnwrapListenableFuture().orElse(null);
		if (lfuture != null)
			return lfuture.getFailure(timeout, unit);
		return ListenableFutureLFP.super.getFailure(timeout, unit);
	}

	// delegates future

	@Override
	default boolean isCancelled() {
		return unwrap().isCancelled();
	}

	@Override
	default boolean isDone() {
		return unwrap().isDone();
	}

	@Override
	default X get() throws InterruptedException, ExecutionException {
		return unwrap().get();
	}

	@Override
	default X get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
		return unwrap().get(timeout, unit);
	}

	private Optional<ListenableFuture<X>> tryUnwrapListenableFuture() {
		return Optional.ofNullable(unwrap())
				.filter(v -> v != this)
				.filter(ListenableFuture.class::isInstance)
				.map(v -> (ListenableFuture<X>) v);
	}

	public static <U> Optional<U> tryUnwrap(Object value, Class<U> wrappedType) {
		if (value == null || wrappedType == null)
			return Optional.empty();
		return CoreReflections.tryCast(value, ListenableFutureWrapper.class)
				.map(ListenableFutureWrapper::unwrap)
				.flatMap(v -> CoreReflections.tryCast(v, wrappedType));
	}

	@SuppressWarnings("unchecked")
	public static <U> Stream<U> stream(Object value, Class<U> wrappedType) {
		if (!CoreReflections.isInstance(wrappedType, value))
			return Stream.empty();
		if (!CoreReflections.isInstance(ListenableFutureWrapper.class, value))
			return Stream.of((U) value);
		Predicate<U> hasNext = Objects::nonNull;
		hasNext = hasNext.and(DistinctInstancePredicate.create());
		return Stream.<U>iterate((U) value, hasNext, prev -> {
			return tryUnwrap(prev, wrappedType).orElse(null);
		});
	}

	public static Method unwrapMethod() {
		return Const.UNWRAP_METHOD;
	}

	static enum Const {
		;

		private static final Method UNWRAP_METHOD = Throws
				.unchecked(() -> ListenableFutureWrapper.class.getMethod("unwrap"));
	}

}
