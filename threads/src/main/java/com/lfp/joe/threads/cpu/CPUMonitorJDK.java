package com.lfp.joe.threads.cpu;

import java.time.Duration;

import com.lfp.joe.core.classpath.Instances;

import oshi.SystemInfo;
import oshi.hardware.CentralProcessor;

public class CPUMonitorJDK extends AbstractCPUMonitor {

	public static CPUMonitorJDK get() {
		return Instances.get(CPUMonitorJDK.class, () -> new CPUMonitorJDK(null));
	}

	private static final CentralProcessor CENTRAL_PROCESSOR = new SystemInfo().getHardware().getProcessor();

	private long[] prevTicks = null;

	protected CPUMonitorJDK(Duration maxHistory) {
		super(Duration.ofMillis(500), maxHistory);
	}

	@Override
	protected Double poll() {
		Double cpuLoad;
		if (prevTicks == null)
			cpuLoad = null;
		else
			cpuLoad = CENTRAL_PROCESSOR.getSystemCpuLoadBetweenTicks(prevTicks);
		prevTicks = CENTRAL_PROCESSOR.getSystemCpuLoadTicks();
		return cpuLoad;
	}

	public static void main(String[] args) throws InterruptedException {
		var monitor = CPUMonitorJDK.get();
		monitor.addUpdateListener(() -> {
			System.out.println(monitor.getAverage(Duration.ofSeconds(1)).orElse(-1));
		});
		Thread.currentThread().join();
	}

}
