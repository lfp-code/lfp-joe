package com.lfp.joe.threads;

import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;

import com.lfp.joe.core.function.Throws;

public class Threads {

	public static class Futures extends com.lfp.joe.threads.Futures {}

	public static class Executors extends com.lfp.joe.threads.Executors {}

	public static class Pools extends com.lfp.joe.threads.Pools {}

	// duration sleep

	public static void sleep(Duration duration) throws InterruptedException {
		if (duration == null)
			return;
		Thread.sleep(duration.toMillis());
	}

	public static void sleepRandom(Duration duration1, Duration duration2) throws InterruptedException {
		if (duration1 == null)
			duration1 = duration2;
		if (duration2 == null)
			duration2 = duration1;
		if (Objects.equals(duration1, duration2)) {
			sleep(duration1);
			return;
		}
		long max = Math.max(duration1.toMillis(), duration2.toMillis());
		long min = Math.min(duration1.toMillis(), duration2.toMillis());
		var millis = ThreadLocalRandom.current().nextLong(min, max + 1);
		sleep(Duration.ofMillis(millis));
	}

	// duration sleep unchecked

	public static void sleepUnchecked(Duration duration) {
		Throws.unchecked(() -> sleep(duration));
	}

	public static void sleepRandomUnchecked(Duration duration1, Duration duration2) {
		Throws.unchecked(() -> sleepRandom(duration1, duration2));
	}

	// millis sleep

	public static void sleep(long millis) throws InterruptedException {
		sleep(Duration.ofMillis(millis));
	}

	public static void sleepRandom(long millis1, long millis2) throws InterruptedException {
		sleepRandom(Duration.ofMillis(millis1), Duration.ofMillis(millis2));
	}

	// millis sleep unchecked

	public static void sleepUnchecked(long millis) {
		sleepUnchecked(Duration.ofMillis(millis));
	}

	public static void sleepRandomUnchecked(long millis1, long millis2) {
		sleepRandomUnchecked(Duration.ofMillis(millis1), Duration.ofMillis(millis2));
	}
}
