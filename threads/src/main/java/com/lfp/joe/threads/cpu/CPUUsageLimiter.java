package com.lfp.joe.threads.cpu;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.log.Logging;

public interface CPUUsageLimiter extends Scrapable {

	default void enableLogging() {
		var callingClass = CoreReflections.getCallingClass(this.getClass());
		enableLogging(Logging.logger(callingClass));
	}

	void enableLogging(org.slf4j.Logger logger);

	void disableLogging();

	int getMaxConcurrency();

}