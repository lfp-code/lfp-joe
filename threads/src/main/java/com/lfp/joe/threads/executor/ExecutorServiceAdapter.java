package com.lfp.joe.threads.executor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.LongSupplier;

import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.ListenableFutureTask;
import org.threadly.concurrent.future.ListenableRunnableFuture;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.Durations;
import com.lfp.joe.core.process.future.Completion;
import com.lfp.joe.threads.Threads;

/** Adapts a plain {@code Executor} into an {@code ExecutorService}. */
public class ExecutorServiceAdapter extends AbstractExecutorService implements SubmitterSchedulerLFP {

	public static ExecutorService adapt(Executor executor) {
		if (executor instanceof ExecutorService)
			return (ExecutorService) executor;
		return new ExecutorServiceAdapter(executor);
	}

	private final Queue<Entry<ListenableRunnableFuture<Void>, TrackedContext>> taskTracker = new ConcurrentLinkedQueue<>();
	private final ReadWriteLock shutdownStateLock = new ReentrantReadWriteLock();
	private final CountDownLatch shutdownLatch = new CountDownLatch(1);
	private final Executor executor;

	public ExecutorServiceAdapter(Executor executor) {
		super();
		this.executor = Objects.requireNonNull(executor);
	}

	@Override
	public Executor getContainedExecutor() {
		return executor;
	}

	@Override
	public void execute(Runnable command) {
		submitInternal(command);
	}

	@Override
	public ListenableFuture<?> submit(Runnable task) {
		return submitInternal(task);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> ListenableFuture<T> submit(Runnable task, T result) {
		if (result == null)
			return (ListenableFuture<T>) submitInternal(task);
		return (ListenableFuture<T>) super.submit(task, result);
	}

	@Override
	public <T> ListenableFuture<T> submit(Callable<T> task) {
		return (ListenableFuture<T>) super.submit(task);
	}

	protected ListenableFuture<?> submitInternal(Runnable command) {
		Objects.requireNonNull(command);
		validateNotShutdown();
		shutdownStateLock.readLock().lock();
		try {
			validateNotShutdown();
			return tryGetDoneFuture(command).orElseGet(() -> {
				var trackedFuture = createTrackedFuture(command);
				getContainedExecutor().execute(trackedFuture);
				return trackedFuture;
			});
		} finally {
			shutdownStateLock.readLock().unlock();
		}
	}

	protected ListenableRunnableFuture<Void> createTrackedFuture(Runnable runnable) {
		var trackedContext = new TrackedContext();
		trackedContext.taskReference.set(runnable);
		var trackedFuture = new ListenableFutureTask<Void>(() -> {
			var command = Optional.ofNullable(trackedContext.taskReference.getAndSet(null))
					.filter(v -> !isDoneFuture(v))
					.orElse(null);
			if (command != null)
				command.run();
			return null;
		});
		var entry = Map.<ListenableRunnableFuture<Void>, TrackedContext>entry(trackedFuture, trackedContext);
		this.taskTracker.add(entry);
		trackedContext.listener = () -> taskTracker.remove(entry);
		trackedFuture.listener(() -> {
			Threads.Futures.cancellationDefault().ifPresent(interrupt -> {
				CoreReflections.tryCast(trackedContext.taskReference.getPlain(), Future.class)
						.ifPresent(v -> v.cancel(interrupt));
			});
			Optional.ofNullable(trackedContext.listener).ifPresent(Runnable::run);
		});
		return trackedFuture;
	}

	@Override
	public boolean isShutdown() {
		return shutdownLatch.getCount() <= 0;
	}

	@Override
	public void shutdown() {
		if (isShutdown())
			return;
		shutdownStateLock.writeLock().lock();
		try {
			shutdownLatch.countDown();
		} finally {
			shutdownStateLock.writeLock().unlock();
		}
	}

	@Override
	public List<Runnable> shutdownNow() {
		return shutdownNow(Boolean.TRUE.equals(Threads.Futures.cancellationDefault().orElse(null)));
	}

	public List<Runnable> shutdownNow(boolean interrupt) {
		if (isTerminated())
			return List.of();
		shutdownStateLock.writeLock().lock();
		try {
			if (isTerminated())
				return List.of();
			List<Runnable> commands = new ArrayList<>(taskTracker.size());
			while (true) {
				var entry = taskTracker.poll();
				if (entry == null)
					break;
				var trackedFuture = entry.getKey();
				var trackedContext = entry.getValue();
				trackedContext.listener = null;
				Optional.ofNullable(trackedContext.taskReference.getAndSet(null))
						.filter(v -> !isDoneFuture(v))
						.ifPresent(commands::add);
				trackedFuture.cancel(interrupt);
			}
			shutdownLatch.countDown();
			return Collections.unmodifiableList(commands);
		} finally {
			shutdownStateLock.writeLock().unlock();
		}
	}

	@Override
	public boolean isTerminated() {
		if (!isShutdown())
			return false;
		return taskTracker.isEmpty();
	}

	@Override
	public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
		if (isTerminated())
			return true;
		var timeoutAt = System.currentTimeMillis() + Durations.from(timeout, unit).toMillis();
		LongSupplier ttlSupplier = () -> Math.max(timeoutAt - System.currentTimeMillis(), 0);
		if (!isShutdown() && !shutdownLatch.await(ttlSupplier.getAsLong(), TimeUnit.MILLISECONDS))
			return false;
		while (true) {
			var trackedFuture = Optional.ofNullable(taskTracker.peek()).map(Entry::getKey).orElse(null);
			if (trackedFuture == null)
				return true;
			try {
				trackedFuture.get(ttlSupplier.getAsLong(), TimeUnit.MILLISECONDS);
			} catch (TimeoutException e) {
				return false;
			} catch (ExecutionException | CancellationException e) {
				// suppress
			}
		}
	}

	@Override
	protected <T> RunnableFuture<T> newTaskFor(Runnable runnable, T value) {
		return new ListenableFutureTask<>(runnable, value);
	}

	@Override
	protected <T> RunnableFuture<T> newTaskFor(Callable<T> callable) {
		return new ListenableFutureTask<>(callable);
	}

	protected void validateNotShutdown() {
		if (this.isShutdown())
			throw new RejectedExecutionException("ExecutorService has been shutdown");
	}

	protected static Optional<ListenableFuture<?>> tryGetDoneFuture(Runnable command) {
		if (!isDoneFuture(command))
			return Optional.empty();
		if (command instanceof ListenableFuture)
			return Optional.of((ListenableFuture<?>) command);
		var lfuture = Completion.join((Future<?>) command)
				.map(FutureUtils::immediateResultFuture, FutureUtils::immediateFailureFuture);
		return Optional.of(lfuture);
	}

	protected static boolean isDoneFuture(Runnable command) {
		if (command instanceof Future)
			return ((Future<?>) command).isDone();
		return false;
	}

	protected static class TrackedContext {

		public final AtomicReference<Runnable> taskReference = new AtomicReference<Runnable>();
		public Runnable listener;

	}

}
