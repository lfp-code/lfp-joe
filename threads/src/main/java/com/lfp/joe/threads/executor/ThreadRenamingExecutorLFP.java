package com.lfp.joe.threads.executor;

import java.util.concurrent.Executor;

import org.threadly.concurrent.wrapper.traceability.ThreadRenamingExecutor;

public class ThreadRenamingExecutorLFP extends ThreadRenamingExecutor implements ExecutorContainer {

	public ThreadRenamingExecutorLFP(Executor executor, String threadName, boolean replace) {
		super(executor, threadName, replace);
	}

	@Override
	public Executor getContainedExecutor() {
		return executor;
	}

}
