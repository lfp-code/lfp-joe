package com.lfp.joe.threads.cpu;

import java.text.DecimalFormat;
import java.time.Duration;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Durations;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.log.LogConsumer;
import com.lfp.joe.core.log.Logging;
import com.lfp.joe.threads.ImmutableFutures.ScheduleWhileRequest;
import com.lfp.joe.threads.Threads;

import ch.qos.logback.classic.Level;

public abstract class AbstractCPUUsageLimiter extends Scrapable.Impl implements CPUUsageLimiter {

	private static final double DEFAULT_CPU_TARGET_USAGE_RANGE = .075;
	private final double cpuTargetUsage;
	private final Duration updateInterval;
	private final Double cpuTargetUsageRange;
	private LogConsumer logConsumer = LogConsumer.NO_OP;

	public AbstractCPUUsageLimiter(double cpuTargetUsage, Duration updateInterval, Double cpuTargetUsageRange) {
		if (cpuTargetUsage < 0 || cpuTargetUsage > 1)
			throw new IllegalArgumentException("invalid cpuTargetUsage:" + cpuTargetUsage);
		this.cpuTargetUsage = cpuTargetUsage;
		Objects.requireNonNull(updateInterval);
		var maxHistory = CPUMonitor.getDefault().getMaxHistory();
		if (!Durations.isGreaterThanOrEqual(maxHistory, updateInterval)) {
			var message = String.format("updateInterval (%sms) > CPU monitor maxHistory (%sms)",
					updateInterval.toMillis(), maxHistory.toMillis());
			throw new IllegalArgumentException(message);
		}
		this.updateInterval = updateInterval;
		cpuTargetUsageRange = cpuTargetUsageRange != null ? cpuTargetUsageRange : DEFAULT_CPU_TARGET_USAGE_RANGE;
		if (cpuTargetUsageRange < 0 || cpuTargetUsageRange > 1)
			throw new IllegalArgumentException("invalid cpuTargetUsageRange:" + cpuTargetUsageRange);
		this.cpuTargetUsageRange = Optional.ofNullable(cpuTargetUsageRange).orElse(DEFAULT_CPU_TARGET_USAGE_RANGE);
		var swRequest = ScheduleWhileRequest.builder(this::poll)
				.delay(updateInterval)
				.loopTest(nil -> !this.isScrapped())
				.build();
		var future = Threads.Futures.scheduleWhile(swRequest);
		Threads.Futures.onScrapCancel(this, future, true);
	}

	private boolean poll() {
		var logicalProcessorCount = MachineConfig.logicalCoreCount();
		var intervalMillis = Double.valueOf(this.updateInterval.toMillis() * .75).longValue();
		var interval = Duration.ofMillis(Math.max(Duration.ofSeconds(1).toMillis(), intervalMillis));
		var cpuAverageUsage = CPUMonitor.getDefault().getAverage(interval).orElse(-1);
		if (cpuAverageUsage < 0)
			return false;
		var cpuTargetUsageMin = Math.max(0, cpuTargetUsage - this.cpuTargetUsageRange);
		var cpuTargetUsageMax = Math.min(1, cpuTargetUsage + this.cpuTargetUsageRange);

		int delta;
		if (cpuAverageUsage > cpuTargetUsageMax)
			delta = -1 * (logicalProcessorCount * 2);
		else if (cpuAverageUsage < cpuTargetUsageMin)
			delta = logicalProcessorCount;
		else
			delta = 0;
		var maxConcurrency = getMaxConcurrency();
		var maxConcurrencyUpdated = maxConcurrency + delta;
		Supplier<Map<String, Object>> logDataSupplier = () -> {
			var logData = new LinkedHashMap<String, Object>();
			var decimalFormat = new DecimalFormat("#,##0.##");
			logData.put("cpuAverageUsage", decimalFormat.format(100 * cpuAverageUsage) + "%");
			logData.put("cpuTargetUsage", String.format("%s%%-%s%%", decimalFormat.format(100 * cpuTargetUsageMin),
					decimalFormat.format(100 * cpuTargetUsageMax)));
			logData.put("maxConcurrency", maxConcurrency);
			logData.put("maxConcurrencyUpdated", maxConcurrencyUpdated);
			logData.put("totalThreads", Thread.getAllStackTraces().keySet().size());
			modifyLogData(logData);
			return logData;
		};
		final boolean updated;
		if (maxConcurrencyUpdated < logicalProcessorCount) {
			log("max permits unmodified, minimum permits reached", logDataSupplier);
			updated = false;
		} else if (maxConcurrency == maxConcurrencyUpdated) {
			log("max permits unmodified, within target range", logDataSupplier);
			updated = false;
		} else {
			var increase = maxConcurrencyUpdated > maxConcurrency;
			if (increase && this.hasCapacity()) {
				log("max permits unmodified, queue empty", logDataSupplier);
				updated = false;
			} else {
				setMaxConcurrency(maxConcurrencyUpdated);
				if (increase)
					log("max permits increased", logDataSupplier);
				else
					log("max permits decreased", logDataSupplier);
				updated = true;
			}
		}
		return updated;
	}

	@Override
	public void enableLogging() {
		var callingClass = CoreReflections.getCallingClass(this.getClass());
		enableLogging(org.slf4j.LoggerFactory.getLogger(callingClass));
	}

	@Override
	public void enableLogging(org.slf4j.Logger logger) {
		this.logConsumer = Logging.levelConsumer(Objects.requireNonNull(logger), Level.INFO);
	}

	@Override
	public void disableLogging() {
		this.logConsumer = LogConsumer.NO_OP;
	}

	protected void log(String message, Supplier<Map<String, Object>> logDataSupplier) {
		if (LogConsumer.NO_OP.equals(logConsumer))
			return;
		var summary = logDataSupplier.get().entrySet().stream().map(ent -> {
			return String.format("%s=%s", ent.getKey(), ent.getValue());
		}).collect(Collectors.joining(" "));
		logConsumer.log(message + " {}", summary);
	}

	protected void modifyLogData(Map<String, Object> logData) {}

	@Override
	public abstract int getMaxConcurrency();

	protected abstract void setMaxConcurrency(int maxConcurrency);

	protected abstract boolean hasCapacity();
}
