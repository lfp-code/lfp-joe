package com.lfp.joe.threads.future.transform;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.stream.Stream;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.future.ListenableFutureWrapper;

import one.util.streamex.StreamEx;

public interface FutureTransform<X, Y> {

	Optional<Y> apply(Future<?> future, X input);

	public static <Y, U extends FutureTransform<Void, Y>> Stream<Y> apply(Class<U> classType, Future<?> future) {
		return apply(classType, future, null);
	}

	public static <X, Y, U extends FutureTransform<X, Y>> StreamEx<Y> apply(Class<U> classType, Future<?> future,
			X input) {
		Objects.requireNonNull(classType);
		return Streams.<U>of(Instances.streamAutowired(classType)).chain(Streams.flatMap(futureTransform -> {
			var futureStream = ListenableFutureWrapper.stream(future, Future.class);
			return futureStream.<Optional<Y>>map(v -> futureTransform.apply(v, input));
		})).nonNull().mapPartial(Function.identity());

	}

	@SuppressWarnings("rawtypes")
	public static abstract class Abs<X, Y, F extends Future> implements FutureTransform<X, Y> {

		@SuppressWarnings("unchecked")
		@Override
		public Optional<Y> apply(Future<?> future, X input) {
			if (!CoreReflections.isInstance(classType(), future))
				return Optional.empty();
			var value = applyInternal((F) future, input);
			if (value == null)
				return Optional.empty();
			return value;
		}

		protected abstract Optional<Y> applyInternal(F future, X input);

		protected abstract Class<? extends F> classType();

	}

}
