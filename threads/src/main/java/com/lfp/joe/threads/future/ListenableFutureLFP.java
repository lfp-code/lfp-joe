package com.lfp.joe.threads.future;

import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.ListenableFuture.ListenerOptimizationStrategy;

import com.lfp.joe.core.function.Durations;
import com.lfp.joe.core.process.future.Completion;

@SuppressWarnings("unused")
public interface ListenableFutureLFP<T> extends ListenableFuture<T> {

	default Executor executingExecutor() {
		return null;
	}

	default Thread executingThread() {
		return null;
	}

	@Override
	default StackTraceElement[] getRunningStackTrace() {
		return Optional.ofNullable(executingThread()).map(Thread::getStackTrace).orElse(null);
	}

	@Override
	default boolean isCompletedExceptionally() {
		return Completion.tryGet(this).map(Completion::isFailure).orElse(false);
	}

	@Override
	default Throwable getFailure() throws InterruptedException {
		return Completion.get(this).failure();
	}

	@Override
	default Throwable getFailure(long timeout, TimeUnit unit) throws InterruptedException, TimeoutException {
		var completion = Completion.get(this, Durations.from(timeout, unit)).orElse(null);
		if (completion == null)
			throw new TimeoutException(String.format("timeout:%s unit:%s", timeout, unit));
		return completion.failure();

	}

	/**
	 * Logic from
	 * {@link org.threadly.concurrent.future.AbstractCompletableListenableFuture#listener(Runnable, Executor, ListenerOptimizationStrategy)}
	 */
	default Optional<Executor> getListenerExecutor(Executor executor, ListenerOptimizationStrategy optimize) {
		if (executor == null)
			return Optional.empty();
		if (executor == executingExecutor()
				&& (optimize == ListenerOptimizationStrategy.SingleThreadIfExecutorMatchOrDone
						| optimize == ListenerOptimizationStrategy.SingleThreadIfExecutorMatch))
			return Optional.empty();
		return Optional.of(executor);
	}

	/**
	 * Logic from
	 * {@link org.threadly.concurrent.future.AbstractCompletableListenableFuture#listener(Runnable, Executor, ListenerOptimizationStrategy)}
	 */
	default Optional<Executor> getImmediateListenerExecutor(Executor executor, ListenerOptimizationStrategy optimize) {
		if (executor == null)
			return Optional.empty();
		if (optimize == ListenerOptimizationStrategy.InvokingThreadIfDone
				| optimize == ListenerOptimizationStrategy.SingleThreadIfExecutorMatchOrDone)
			return Optional.empty();
		return Optional.of(executor);
	}

}
