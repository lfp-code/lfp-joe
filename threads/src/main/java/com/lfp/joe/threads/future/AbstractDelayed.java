package com.lfp.joe.threads.future;

import static java.util.concurrent.TimeUnit.NANOSECONDS;

import java.time.Duration;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

import org.immutables.value.Value;
import org.immutables.value.Value.Style.BuilderVisibility;
import org.immutables.value.Value.Style.ImplementationVisibility;

import com.lfp.joe.core.function.Sequencer;

@Value.Immutable
@Value.Style(stagedBuilder = true, strictBuilder = false, visibility = ImplementationVisibility.PUBLIC, builderVisibility = BuilderVisibility.PUBLIC)
abstract class AbstractDelayed implements Delayed {

	private static final Sequencer SEQUENCER = new Sequencer();

	@Value.Parameter(order = 0)
	abstract Duration initial();

	abstract Optional<Duration> delay();

	abstract Optional<Duration> period();

	@Value.Derived
	protected AtomicLong time() {
		return new AtomicLong(initial().toMillis() + System.currentTimeMillis());
	}

	@Value.Derived
	protected Sequencer.SequenceId sequenceId() {
		return SEQUENCER.next();
	}

	@Override
	public int compareTo(Delayed other) {
		if (other == this) // compare zero if same object
			return 0;
		if (other == null)
			return -1;
		long diff = getDelay(NANOSECONDS) - other.getDelay(NANOSECONDS);
		if (diff != 0)
			return diff < 0 ? -1 : 1;
		if (other instanceof AbstractDelayed)
			return sequenceId().compareTo(((AbstractDelayed) other).sequenceId());
		return 0;
	}

	@Override
	public long getDelay(TimeUnit unit) {
		var time = time().get();
		var delayMillis = time - System.currentTimeMillis();
		return unit.convert(delayMillis, TimeUnit.MILLISECONDS);
	}

	public void advance() {
		if (delay().isEmpty() && period().isEmpty())
			return;
		time().updateAndGet(time -> {
			var delay = delay().map(v -> v.toMillis() + System.currentTimeMillis()).orElse(null);
			var period = period().map(v -> v.toMillis() + time).orElse(null);
			return Stream.of(delay, period).filter(Objects::nonNull).sorted().findFirst().orElse(time);
		});
	}

	@Value.Check
	protected void validate() {
		BiConsumer<String, Duration> validator = (name, dur) -> {
			if (dur != null && !dur.isNegative())
				return;
			throw new IllegalArgumentException(String.format("%s must be >= 0", name));
		};
		validator.accept("initial", initial());
		delay().ifPresent(v -> validator.accept("delay", v));
		period().ifPresent(v -> validator.accept("period", v));
	}

	public static void main(String[] args) {
		var delayed = ImmutableDelayed.builder()
				.initial(Duration.ofSeconds(10))
				.period(Duration.ofSeconds(5))
				.delay(Duration.ofSeconds(1))
				.build();
		System.out.println(delayed.getDelay(TimeUnit.MILLISECONDS));
		delayed.advance();
		System.out.println(delayed.getDelay(TimeUnit.MILLISECONDS));
	}

}
