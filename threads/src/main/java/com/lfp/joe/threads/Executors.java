package com.lfp.joe.threads;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import java.util.function.BooleanSupplier;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import org.threadly.concurrent.SameThreadSubmitterExecutor;
import org.threadly.concurrent.SubmitterExecutor;
import org.threadly.concurrent.future.ListenableFutureTask;
import org.threadly.concurrent.wrapper.SubmitterExecutorAdapter;

import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.process.future.Completion;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.executor.ExecutorServiceAdapter;
import com.lfp.joe.threads.executor.ExecutorServices;

public class Executors {

	protected Executors() {}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static ExecutorService asExecutorService(Executor executor) {
		return asExecutorService(executor, Optional.empty());
	}

	public static ExecutorService asExecutorService(Executor executor, Optional<Boolean> shutdownSupport) {
		Objects.requireNonNull(executor);
		shutdownSupport = Optional.ofNullable(shutdownSupport).flatMap(Function.identity());
		if (executor instanceof ExecutorService) {
			ExecutorService executorService = (ExecutorService) executor;
			if (shutdownSupport.isEmpty())
				return executorService;
			var shutdownUnsupported = executorService instanceof ExecutorServices.ShutdownUnsupported;
			if (shutdownSupport.get() && !shutdownUnsupported)
				return executorService;
			if (!shutdownSupport.get() && shutdownUnsupported)
				return executorService;
		}
		if (shutdownSupport.isEmpty() || !shutdownSupport.get())
			return new ExecutorServices.Wrapper() {

				@Override
				public Executor getContainedExecutor() {
					return executor;
				}
			};
		return ExecutorServiceAdapter.adapt(executor);
	}

	@SafeVarargs
	public static SubmitterExecutor asSubmitterExecutor(Executor executor, Supplier<Executor>... fallbackExecutors) {
		var executors = Streams.of(fallbackExecutors).nonNull().map(Supplier::get).prepend(executor).nonNull();
		executor = executors.findFirst().orElseThrow();
		return SubmitterExecutorAdapter.adaptExecutor(executor);
	}

	@SafeVarargs
	public static SubmitterExecutor throttle(Executor executor, Integer threadLimit,
			Supplier<Executor>... fallbackExecutors) {
		return throttle(executor, threadLimit, r -> {
			var fallbackExecutor = Streams.of(fallbackExecutors)
					.nonNull()
					.map(Supplier::get)
					.nonNull()
					.findFirst()
					.orElse(null);
			if (fallbackExecutor == null)
				return true;
			fallbackExecutor.execute(r);
			return false;
		});
	}

	public static SubmitterExecutor throttle(Executor executor, Integer threadLimit,
			Predicate<Runnable> blockPredicate) {
		Objects.requireNonNull(executor);
		BooleanSupplier tryAcquire;
		Runnable acquire;
		Runnable release;
		{
			if (threadLimit == null || threadLimit == -1) {
				tryAcquire = () -> true;
				acquire = () -> {};
				release = () -> {};
			} else if (threadLimit <= 0)
				throw new IllegalArgumentException("invalid threadLimit:" + threadLimit);
			else {
				var throttle = new Semaphore(threadLimit);
				tryAcquire = throttle::tryAcquire;
				acquire = Throws.runnableUnchecked(throttle::acquire);
				release = throttle::release;
			}
		}
		Executor throttledExecutor = task -> {
			if (blockPredicate == null)
				acquire.run();
			else if (!tryAcquire.getAsBoolean()) {
				if (!blockPredicate.test(task))
					return;
				acquire.run();
			}
			Future<?> future;
			if (task instanceof Future)
				future = (Future<?>) task;
			else {
				var lfutureTask = new ListenableFutureTask<>(task);
				future = lfutureTask;
				task = lfutureTask;
			}
			Threads.Futures.callback(future, release);
			executor.execute(task);
		};
		return asSubmitterExecutor(throttledExecutor);
	}

	public static void main(String[] args) throws InterruptedException {
		var executor = Executors.throttle(Threads.Pools.centralPool(), 4, SameThreadSubmitterExecutor::instance);
		List<Future<?>> futures = new ArrayList<>();
		for (int i = 0; i < 100; i++) {
			var index = i;
			var future = executor.submit(() -> {
				LOGGER.info("starting:{} thead:{}", index, Thread.currentThread().getName());
				Thread.sleep(1_000);
				LOGGER.info("done:{} thead:{}", index, Thread.currentThread().getName());
				return null;
			});
			Threads.Futures.logFailureError(future, true, "error:{}", index);
			futures.add(future);
		}
		var complete = Threads.Futures.makeCompleteFuture(futures);
		Completion.join(complete);
	}

}
