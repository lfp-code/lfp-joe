package com.lfp.joe.threads.cpu;

import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

import org.threadly.concurrent.event.RunnableListenerHelper;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.core.io.InputStreamExt;
import com.lfp.joe.core.log.Logging;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.ImmutableFutures.ScheduleWhileRequest;
import com.lfp.joe.threads.Threads;

import ch.qos.logback.classic.Level;

public abstract class AbstractCPUMonitor extends Scrapable.Impl implements CPUMonitor {

	private static final Duration DEFAULT_MAX_HISTORY = Duration.ofMinutes(15);

	private final org.slf4j.Logger logger = Logging.logger(this);
	private final ReentrantReadWriteLock historyLock = new ReentrantReadWriteLock();
	private final List<Entry<Date, Double>> history = new ArrayList<>();
	private final RunnableListenerHelper listeners = new RunnableListenerHelper(false);
	private final Duration maxHistory;

	public AbstractCPUMonitor(Duration pollDuration, Duration maxHistory) {
		super();
		Objects.requireNonNull(pollDuration);
		this.maxHistory = Optional.ofNullable(maxHistory).orElse(DEFAULT_MAX_HISTORY);
		var swRequestb = ScheduleWhileRequest.builder(() -> {
			recordUsage(poll());
			return Nada.get();
		}).delay(pollDuration);
		if (MachineConfig.isDeveloper())
			swRequestb.errorLogFunction(v -> {
				return Logging.levelConsumer(Logging.logger(), Level.WARN);
			});
		var future = Threads.Futures.scheduleWhile(swRequestb.build());
		Threads.Futures.onScrapCancel(this, future, true);
	}

	protected void recordUsage(Double usage) {
		if (usage == null || usage == 0)
			return;
		historyLock.writeLock().lock();
		try {
			var intervalTest = createIntervalPredicate(this.maxHistory);
			history.removeIf(ent -> {
				return !intervalTest.test(ent);
			});
			history.add(new SimpleEntry<>(new Date(), usage));
		} finally {
			historyLock.writeLock().unlock();
		}
		listeners.callListeners();
	}

	@Override
	public Scrapable addUpdateListener(Runnable listener) {
		Objects.requireNonNull(listener);
		listeners.addListener(listener);
		return Scrapable.create(() -> listeners.removeListener(listener));
	}

	protected <X> X accessWindow(Duration interval, Function<DoubleStream, X> access) {
		Objects.requireNonNull(access);
		historyLock.readLock().lock();
		try {
			var intervalTest = createIntervalPredicate(interval);
			var usageStream = Streams.of(history).filter(intervalTest).mapToDouble(Entry::getValue);
			return access.apply(usageStream);
		} finally {
			historyLock.readLock().unlock();
		}
	}

	protected Predicate<Entry<Date, Double>> createIntervalPredicate(Duration interval) {
		var cutOff = interval == null ? null : new Date(System.currentTimeMillis() - interval.toMillis());
		return ent -> {
			if (cutOff != null && cutOff.after(ent.getKey()))
				return false;
			return true;
		};
	}

	@Override
	public Duration getMaxHistory() {
		return this.maxHistory;
	}

	@Override
	public OptionalDouble getMax(Duration interval) {
		return accessWindow(interval, v -> v.max());
	}

	@Override
	public OptionalDouble getMin(Duration interval) {
		return accessWindow(interval, v -> v.min());
	}

	@Override
	public OptionalDouble getAverage(Duration interval) {
		return accessWindow(interval, v -> v.average());
	}

	@Override
	public OptionalDouble getMedian(Duration interval) {
		return accessWindow(interval, stream -> {
			var list = stream.sorted().boxed().collect(Collectors.toList());
			var sortedStream = list.stream().mapToDouble(v -> v);
			OptionalDouble median = (list.size() % 2 == 0 ? sortedStream.skip((list.size() / 2) - 1).limit(2).average()
					: sortedStream.skip(list.size() / 2).findFirst());
			return median;
		});
	}

	protected Double parseUsage(String line) {
		if (line == null)
			return null;
		var parseStream = Stream.of(line.split(Pattern.quote("\"")));
		parseStream = parseStream.map(v -> {
			while (true) {
				v = v.trim();
				if (v.endsWith("%"))
					v = v.substring(0, v.length() - 1);
				else
					break;
			}
			return v;
		});
		parseStream = parseStream.filter(v -> !v.isBlank());
		var usageStream = parseStream.map(v -> {
			try {
				return Double.parseDouble(v);
			} catch (NumberFormatException e) {
				return null;
			}
		});
		usageStream = usageStream.filter(Objects::nonNull);
		usageStream = usageStream.filter(v -> v >= 0);
		var usage = usageStream.findFirst().orElse(null);
		return usage;
	}

	protected void validateExitValue(int exitValue) {
		if (exitValue != 0)
			throw new IllegalArgumentException("iinvalid exitValue:" + exitValue);
	}

	protected void poll(Process process) throws InterruptedException {
		Objects.requireNonNull(process);
		List<ThrowingSupplier<InputStream, IOException>> isSuppliers = new ArrayList<>(1);
		isSuppliers.add(process::getInputStream);
		for (var isSupplier : isSuppliers) {
			var isFuture = Threads.Pools.centralPool().submit(() -> {
				try (var reader = new InputStreamExt(isSupplier).reader()) {
					var lineIter = reader.lines().iterator();
					while (!Thread.currentThread().isInterrupted() && lineIter.hasNext()) {
						var line = lineIter.next();
						recordUsage(parseUsage(line));
					}
				}
				return null;
			});
			process.onExit().whenComplete((v, t) -> {
				isFuture.cancel(true);
			});
		}
		Runnable kill = () -> process.destroyForcibly();
		var registration = this.onScrap(kill);
		try {
			validateExitValue(process.waitFor());
		} finally {
			kill.run();
			registration.scrap();
		}
	}

	protected abstract Double poll() throws Exception;

}
