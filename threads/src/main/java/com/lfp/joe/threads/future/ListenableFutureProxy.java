package com.lfp.joe.threads.future;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RunnableFuture;

import org.threadly.concurrent.future.ListenableFuture;

import com.lfp.joe.bytecode.ImmutableMethodHandlerLFP.MethodHandlerRequest;
import com.lfp.joe.bytecode.MethodHandlerLFP;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.InterfaceSet;
import com.lfp.joe.core.function.EmptyArrays;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.stream.Streams;

import javassist.util.proxy.Proxy;
import javassist.util.proxy.ProxyFactory;

@SuppressWarnings("unchecked")

public interface ListenableFutureProxy<X> extends ListenableFutureWrapper<X> {

	@SafeVarargs
	public static <U> ListenableFuture<U> from(Future<U> future, Collection<Object> delegates, Class<?>... interfaces) {
		Objects.requireNonNull(future);
		var delegateList = Streams.of(delegates).nonNull().toImmutableList();
		var interfaceSet = new InterfaceSet(future, ListenableFuture.class);
		Streams.of(interfaces).nonNull().forEach(interfaceSet::add);
		if (delegateList.isEmpty() && interfaceSet.isInstanceAll(future))
			return (ListenableFuture<U>) future;
		interfaceSet.add(ListenableFutureProxy.class);
		future = ListenableFutureWrapper.stream(future, Future.class).reduce((f, s) -> s).get();
		return Throws.unchecked(future, v -> {
			return fromProxy(v, delegateList, interfaceSet);
		});
	}

	private static <U> ListenableFuture<U> fromProxy(Future<U> future, List<Object> delegates,
			InterfaceSet interfaceSet) throws NoSuchMethodException, IllegalArgumentException, InstantiationException,
			IllegalAccessException, InvocationTargetException {
		var proxyFactory = new ProxyFactory();
		proxyFactory.setUseCache(true);
		proxyFactory.setInterfaces(interfaceSet.toArray());
		var futureProxy = (ListenableFuture<U>) proxyFactory.create(EmptyArrays.of(Class.class),
				EmptyArrays.of(Object.class));
		var methodHandler = new MethodHandlerLFP() {

			@Override
			public Object invoke(MethodHandlerRequest request) throws Throwable {
				var method = request.method();
				if (CoreReflections.compatibleMethodInvoke(ListenableFutureWrapper.unwrapMethod(), method))
					return future;
				var declaringClass = method.getDeclaringClass();
				var delegateStream = Streams.of(delegates).prepend(future).nonNull();
				var delegate = delegateStream.filter(declaringClass::isInstance).findFirst().orElse(null);
				if (delegate != null)
					return method.invoke(delegate, request.args());
				if (request.proceedMethod().isPresent())
					return request.proceedInvoke();
				throw new UnsupportedOperationException(request.summary());
			}

		};
		((Proxy) futureProxy).setHandler(methodHandler);
		return futureProxy;
	}

	public static void main(String[] args) throws NoSuchMethodException, IllegalArgumentException,
			InstantiationException, IllegalAccessException, InvocationTargetException {
		var futureTask = new FutureTask<Date>(() -> {
			return new Date();
		});
		var future = ListenableFutureProxy.from(futureTask, List.of(), RunnableFuture.class);
		future.resultCallback(System.out::println);
		future.failureCallback(Throwable::printStackTrace);
		((Runnable) future).run();
		System.out.println(future.isDone());
	}

}
