package com.lfp.joe.threads.future;

import org.threadly.concurrent.event.RunnableListenerHelper;

public class Notifier extends RunnableListenerHelper {

	public Notifier() {
		this(true);
	}

	public Notifier(boolean callListenersOnce) {
		super(callListenersOnce);
	}

	public boolean isDone() {
		return super.done;
	}

	public boolean tryCallListeners() {
		if (!callOnce) {
			super.callListeners();
			return true;
		}
		if (!done)
			synchronized (listenersLock) {
				if (!done) {
					super.callListeners();
					return true;
				}
			}
		return false;
	}

}
