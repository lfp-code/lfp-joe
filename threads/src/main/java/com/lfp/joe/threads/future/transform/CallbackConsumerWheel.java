package com.lfp.joe.threads.future.transform;

import java.util.Date;
import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.function.BiConsumer;

import com.lfp.joe.core.classpath.Instances.Autowired;
import com.lfp.joe.core.process.future.Completion;
import com.lfp.joe.threads.Threads;

@SuppressWarnings({ "unchecked", "rawtypes" })

@Autowired(priority = Integer.MIN_VALUE)
public class CallbackConsumerWheel extends CallbackConsumerJdk {

	@Override
	protected Class<? extends Future> classType() {
		return Future.class;
	}

	@Override
	protected Optional<Boolean> applyInternal(Future future, BiConsumer<?, ? super Throwable> action,
			Executor executor) {
		if (executor == null)
			executor = defaultExecutor();
		var completionStage = FuturityWheelLFP.INSTANCE.asCompletionStage(future, executor);
		return applyInternal(completionStage, action, executor);
	}

	protected Executor defaultExecutor() {
		return Threads.Pools.centralPool();
	}

	public static void main(String[] args) throws InterruptedException {
		var exec = Executors.newCachedThreadPool();
		var ft = new FutureTask<Date>(() -> {
			for (int i = 0; i < 100; i++) {
				if (i > 0)
					Thread.sleep(1_000);
				System.out.println("running:" + i);
			}
			return new Date();
		});
		exec.execute(ft);
		var cf1 = FuturityWheelLFP.INSTANCE.shift(ft);
		cf1.whenComplete((v, t) -> System.out.println(Completion.of(v, t)));
		var cf2 = FuturityWheelLFP.INSTANCE.shift(ft);
		Threads.Futures.callback(ft, (v, t) -> System.out.println(Completion.of(v, t)));
		System.out.println(cf1 == cf2);
		Thread.sleep(2_000);
		cf2.cancel(true);
		exec.shutdown();
	}
}
