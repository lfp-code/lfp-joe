package com.lfp.joe.threads.cpu;

import java.time.Duration;
import java.util.Optional;
import java.util.OptionalDouble;

import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.log.Logging;

public interface CPUMonitor {

	public static CPUMonitor getDefault() {
		return Instances.get(CPUMonitor.class, () -> {
			Optional<CPUMonitor> cpuMonitorOp = Optional.empty();
			cpuMonitorOp = cpuMonitorOp.or(() -> {
				return CPUMonitorWindows.tryGet();
			});
			var cpuMonitor = cpuMonitorOp.orElseGet(CPUMonitorJDK::get);
			var thisClass = CPUMonitor.class;
			var logger = Logging.logger(thisClass);
			logger.info("{} default:{}", thisClass.getSimpleName(), cpuMonitor.getClass().getName());
			return cpuMonitor;
		});
	}

	Duration getMaxHistory();

	default OptionalDouble getMax() {
		return getMax(null);
	}

	OptionalDouble getMax(Duration interval);

	default OptionalDouble getMin() {
		return getMin(null);
	}

	OptionalDouble getMin(Duration interval);

	default OptionalDouble getAverage() {
		return getAverage(null);
	}

	OptionalDouble getAverage(Duration interval);

	default OptionalDouble getMedian() {
		return getMedian(null);
	}

	OptionalDouble getMedian(Duration interval);

	Scrapable addUpdateListener(Runnable listener);

	public static void main(String[] args) throws InterruptedException {
		var monitor = CPUMonitor.getDefault();
		monitor.addUpdateListener(() -> {
			System.out.println(monitor.getAverage(Duration.ofSeconds(1)).orElse(-1));
		});
		Thread.currentThread().join();
	}

}