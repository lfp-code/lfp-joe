package com.lfp.joe.threads.future.transform;

import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.function.BiConsumer;

import org.checkerframework.checker.nullness.qual.Nullable;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import com.lfp.joe.core.classpath.Instances.Autowired;

@SuppressWarnings({ "unchecked", "rawtypes" })
@Autowired(requiredClassNames = "com.google.common.util.concurrent.ListenableFuture")
public class CallbackConsumerGuava extends CallbackConsumer.Abs<ListenableFuture> {

	@Override
	protected Class<? extends ListenableFuture> classType() {
		return ListenableFuture.class;
	}

	@Override
	protected Optional<Boolean> applyInternal(ListenableFuture future, BiConsumer<?, ? super Throwable> action,
			Executor executor) {
		var futureCallback = new FutureCallback() {

			@Override
			public void onSuccess(@Nullable Object result) {
				((BiConsumer<Object, ? super Throwable>) action).accept(result, null);
			}

			@Override
			public void onFailure(Throwable t) {
				action.accept(null, t);
			}
		};
		if (executor == null)
			executor = MoreExecutors.directExecutor();
		Futures.addCallback(future, futureCallback, executor);
		return Optional.of(true);
	}

}
