package com.lfp.joe.threads;

import java.time.Duration;
import java.util.AbstractMap.SimpleEntry;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.BaseStream;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Nullable;

import org.immutables.value.Value;
import org.slf4j.Logger;
import org.threadly.concurrent.CallableContainer;
import org.threadly.concurrent.RunnableContainer;
import org.threadly.concurrent.SameThreadSubmitterExecutor;
import org.threadly.concurrent.SubmitterScheduler;
import org.threadly.concurrent.event.RunnableListenerHelper;
import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.SettableListenableFuture;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.DistinctInstancePredicate;
import com.lfp.joe.core.function.Durations;
import com.lfp.joe.core.function.Muto;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.core.log.LogConsumer;
import com.lfp.joe.core.log.Logging;
import com.lfp.joe.core.process.ShutdownNotifier;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.core.process.future.CSFutureTask;
import com.lfp.joe.core.process.future.Completion;
import com.lfp.joe.core.process.future.InterruptCancellationException;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.ImmutableFutures.ScheduleWhileRequest;
import com.lfp.joe.threads.future.FutureTracker;
import com.lfp.joe.threads.future.ListenableFutureProxy;
import com.lfp.joe.threads.future.transform.CallbackConsumer;
import com.lfp.joe.threads.future.transform.CompletePredicate;
import com.lfp.joe.threads.future.transform.FutureTransform;
import com.lfp.joe.threads.future.transform.InterruptedPredicate;
import com.lfp.joe.threads.future.transform.StateFunction;

import ch.qos.logback.classic.Level;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;

@ValueLFP.Style
@Value.Enclosing
@SuppressWarnings("unchecked")
public class Futures {

	protected Futures() {}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final List<Class<?>> LOGGER_SKIP_CLASS_TYPES = Stream
			.concat(Stream.of(THIS_CLASS), Stream.of(THIS_CLASS.getDeclaredClasses()))
			.distinct()
			.collect(Collectors.toUnmodifiableList());
	private static final Optional<Boolean> CANCELLATION_DEFAULT = Optional.of(true);

	public static Optional<Boolean> cancellationDefault() {
		return CANCELLATION_DEFAULT;
	}

	public static <X> ListenableFuture<X> asListenable(Future<X> future) {
		if (future instanceof ListenableFuture)
			return (ListenableFuture<X>) future;
		return ListenableFutureProxy.from(future, null);
	}

	public static <X> ListenableFuture<X> immediateFuture(ThrowingSupplier<? extends X, ?> loader) {
		Objects.requireNonNull(loader);
		try {
			return FutureUtils.immediateResultFuture(loader.get());
		} catch (Throwable t) {
			return FutureUtils.immediateFailureFuture(t);
		}
	}

	public static <X> ListenableFuture<X> immediateFuture(Completion<X> completion) {
		Objects.requireNonNull(completion);
		if (completion.isFailure())
			return FutureUtils.immediateFailureFuture(completion.failure());
		return FutureUtils.immediateResultFuture(completion.result());

	}

	public static <X> X join(Future<X> future) {
		if (future instanceof CompletableFuture)
			return ((CompletableFuture<X>) future).join();
		return Completion.join(future).resultOrThrow();
	}

	public static ListenableFuture<Nada> makeCompleteFuture(Future<?>... futures) {
		return makeCompleteFuture(futures == null ? null : Arrays.asList(futures));
	}

	public static ListenableFuture<Nada> makeCompleteFuture(Iterable<? extends Future<?>> futures) {
		var futureIter = Streams.of(futures).nonNull().iterator();
		if (!futureIter.hasNext())
			return FutureUtils.immediateResultFuture(Nada.get());
		var resultFuture = new SettableListenableFuture<Nada>(false);
		var remainingFutures = new AtomicLong(1);
		Runnable listener = () -> {
			var remaining = remainingFutures.decrementAndGet();
			if (remaining <= 0)
				resultFuture.setResult(Nada.get());
		};
		while (futureIter.hasNext()) {
			Future<?> future = futureIter.next();
			if (future.isDone())
				continue;
			remainingFutures.incrementAndGet();
			Futures.callback(future, listener);
		}
		listener.run();
		return resultFuture;
	}

	public static FutureTracker createFutureTracker(Future<?>... futures) {
		return createFutureTracker(futures == null ? null : Arrays.asList(futures));
	}

	public static FutureTracker createFutureTracker(Iterable<? extends Future<?>> futures) {
		var futureTracker = new FutureTracker();
		if (futures != null)
			for (var future : futures)
				if (future != null)
					futureTracker.apply(future);
		return futureTracker;
	}

	// callback Runnable

	public static <X, F extends Future<X>> F callback(F future, Runnable callback) {
		return callback(future, callback, null);
	}

	public static <X, F extends Future<X>> F callback(F future, Runnable callback, Executor executor) {
		return callback(future, callback == null ? null : (v, t) -> callback.run(), executor);
	}

	// callback result Consumer

	public static <X, F extends Future<X>> F callbackResult(F future, Consumer<? super X> callback) {
		return callbackResult(future, callback, null);
	}

	public static <X, F extends Future<X>> F callbackResult(F future, Consumer<? super X> callback, Executor executor) {
		return callback(future, callback == null ? null : (v, t) -> {
			if (t == null)
				callback.accept(v);
		}, executor);
	}

	// callback error Consumer

	public static <X, F extends Future<X>> F callbackFailure(F future, Consumer<? super Throwable> callback) {
		return callbackFailure(future, callback, null);
	}

	public static <X, F extends Future<X>> F callbackFailure(F future, Consumer<? super Throwable> callback,
			Executor executor) {
		return callback(future, callback == null ? null : (v, t) -> {
			if (t != null)
				callback.accept(t);
		}, executor);
	}

	// callback Completion

	public static <X, F extends Future<X>> F callback(F future, Consumer<? super Completion<X>> callback) {
		return callback(future, callback, null);
	}

	public static <X, F extends Future<X>> F callback(F future, Consumer<? super Completion<X>> callback,
			Executor executor) {
		return callback(future, callback == null ? null : (v, t) -> {
			callback.accept(Completion.tryGet(future).get());
		}, executor);
	}

	// callback BiConsumer

	public static <X, F extends Future<X>> F callback(F future, BiConsumer<? super X, ? super Throwable> callback) {
		return callback(future, callback, null);
	}

	public static <X, F extends Future<X>> F callback(F future, BiConsumer<? super X, ? super Throwable> callback,
			Executor executor) {
		Objects.requireNonNull(future);
		if (callback == null)
			return future;
		try (var resultStream = FutureTransform.apply(CallbackConsumer.class, future,
				new SimpleEntry<>(callback, executor));) {
			for (var result : resultStream)
				if (Boolean.TRUE.equals(result))
					return future;
		}
		throw new IllegalStateException("add callback failed. future type:" + future.getClass().getName());
	}

	// callback timeout

	public static <X, F extends Future<X>> F callbackTimeout(F future, Duration rotationTimeout) {
		return callbackTimeout(future, rotationTimeout, (Optional<Boolean>) null);
	};

	public static <X, F extends Future<X>> F callbackTimeout(F future, Duration rotationTimeout,
			Optional<Boolean> cancellation) {
		return callbackTimeout(future, rotationTimeout, v -> {
			Optional.ofNullable(cancellation)
					.or(() -> Optional.ofNullable(cancellationDefault()))
					.flatMap(Function.identity())
					.ifPresent(interrupt -> v.cancel(interrupt));
		}, null);
	}

	public static <X, F extends Future<X>> F callbackTimeout(F future, Duration rotationTimeout, Consumer<F> callback) {
		return callbackTimeout(future, rotationTimeout, callback, null);
	};

	public static <X, F extends Future<X>> F callbackTimeout(F future, Duration timeout, Consumer<F> callback,
			Executor executor) {
		Objects.requireNonNull(future);
		if (timeout == null || callback == null || future.isDone())
			return future;
		if (Durations.isNegativeOrZero(timeout)) {
			callback.accept(future);
			return future;
		}
		executor = Optional.ofNullable(executor).orElseGet(Threads.Pools::lowPriorityPool);
		var timeoutTask = new FutureTask<>(() -> callback.accept(future), null);
		CompletableFuture.delayedExecutor(Durations.getTimeAmount(timeout), Durations.getTimeUnit(timeout), executor)
				.execute(timeoutTask);
		callback(future, () -> timeoutTask.cancel(false));
		return future;
	};

	// mapping

	public static <X> Function<X, ListenableFuture<X>> filterFlatMap(Predicate<? super X> filter,
			Function<? super X, ? extends ListenableFuture<X>> mapper) {
		return value -> {
			if ((filter != null && !filter.test(value)) || mapper == null)
				return FutureUtils.immediateResultFuture(value);
			return Optional.<ListenableFuture<X>>ofNullable(mapper.apply(value))
					.orElseGet(() -> FutureUtils.immediateResultFuture(null));
		};
	}

	public static <X> Function<X, ListenableFuture<X>> flatMapIfNull(Supplier<Future<? extends X>> futureSupplier) {
		return value -> {
			if (value != null)
				return FutureUtils.immediateResultFuture(value);
			var future = Optional.ofNullable(futureSupplier).map(Supplier::get).map(Futures::asListenable).orElse(null);
			if (future == null)
				return FutureUtils.immediateResultFuture(null);
			return future.map(v -> v);
		};
	}

	public static <X> Function<X, CompletionStage<X>> thenComposeOnNull(
			Supplier<CompletionStage<? extends X>> completionStageSupplier) {
		return value -> {
			if (value != null)
				return CompletableFuture.completedStage(value);
			if (completionStageSupplier != null) {
				var stage = completionStageSupplier.get();
				if (stage != null)
					return stage.thenApply(v -> v);
			}
			return CompletableFuture.completedStage(null);
		};
	}

	@SafeVarargs
	public static <X> ListenableFuture<X> flatMapFinally(ListenableFuture<X> future,
			Supplier<? extends Future<?>>... futureSuppliers) {
		return flatMapFinally(future, Streams.of(futureSuppliers));
	}

	public static <X> ListenableFuture<X> flatMapFinally(ListenableFuture<X> future,
			Iterable<Supplier<? extends Future<?>>> futureSuppliers) {
		Objects.requireNonNull(future);
		var futureSupplierIter = Optional.ofNullable(futureSuppliers)
				.map(Iterable::iterator)
				.orElseGet(Collections::emptyIterator);
		var futureSupplier = StreamEx.of(futureSupplierIter).nonNull().findFirst().orElse(null);
		if (futureSupplier == null)
			return future;
		BiFunction<X, Throwable, ListenableFuture<X>> flatMapper = (v, t1) -> {
			var flatMapFuture = Optional.<Future<?>>ofNullable(futureSupplier.get())
					.map(Futures::asListenable)
					.orElseGet(() -> FutureUtils.immediateResultFuture(null));
			return flatMapFuture.flatMapFailure(Throwable.class, t2 -> {
				return FutureUtils.immediateFailureFuture(Throws.combine(t2, t1));
			}).flatMap(nil -> {
				return Futures.immediateFuture(Completion.of(v, t1));
			});
		};
		future = future.flatMapFailure(Throwable.class, t -> {
			return flatMapper.apply(null, t);
		}).flatMap(v -> {
			return flatMapper.apply(v, null);
		});
		return flatMapFinally(future, () -> futureSupplierIter);
	}

	// forward completion

	public static <X> ListenableFuture<Boolean> forwardCompletion(Future<? extends X> source,
			Future<? extends X> future) {
		if (source == null)
			return forwardCompletion(FutureUtils.immediateResultFuture(null), future);
		if (future == null)
			return FutureUtils.immediateResultFuture(false);
		Predicate<Completion<? extends X>> completionPredicate = completion -> {
			if (completion.isSuccess())
				return complete(future, completion.result(), null);
			Throwable failure;
			if (!completion.isInterruption() && isInterrupted(source))
				failure = new InterruptCancellationException();
			else
				failure = completion.failure();
			return complete(future, null, failure);
		};
		if (source.isDone())
			return FutureUtils.immediateResultFuture(completionPredicate.test(Completion.join(source)));
		return Futures.asListenable(source).map(v -> {
			return completionPredicate.test(Completion.of(v));
		}).mapFailure(Throwable.class, t -> {
			return completionPredicate.test(Completion.of(t));
		});
	}

	public static <X> ListenableFuture<Entry<Boolean, Boolean>> linkCompletion(Future<? extends X> future0,
			Future<? extends X> future1) {
		if ((future0 == null && future1 == null) || future0 == future1)
			return FutureUtils.immediateResultFuture(Map.entry(false, false));
		var forward0 = forwardCompletion(future0, future1);
		var forward1 = forwardCompletion(future1, future0);
		return FutureUtils.makeCompleteFuture(Arrays.asList(forward0, forward1)).flatMap(nil -> {
			var completion1 = Completion.join(forward0);
			var completion2 = Completion.join(forward1);
			Throwable failure = null;
			for (var c : Arrays.asList(completion1, completion2)) {
				if (c.isSuccess())
					continue;
				if (failure == null)
					failure = c.failure();
				else if (failure != c.failure())
					failure.addSuppressed(c.failure());
			}
			if (failure != null)
				return FutureUtils.immediateFailureFuture(failure);
			return FutureUtils.immediateResultFuture(Map.entry(completion1.result(), completion2.result()));
		});

	}

	// complete

	public static <X> boolean complete(Future<? extends X> future, X result, Throwable failure) {
		return complete(future, result, failure, null);
	}

	public static <X> boolean complete(Future<? extends X> future, X result, Throwable failure,
			Optional<Boolean> completionCancellationFallback) {
		if (completionCancellationFallback == null)
			return complete(future, result, failure, cancellationDefault());
		var resultIter = FutureTransform.apply(CompletePredicate.class, future, new SimpleEntry<>(result, failure))
				.iterator();
		while (resultIter.hasNext()) {
			if (resultIter.next())
				return true;
		}
		if (future == null || future.isDone() || completionCancellationFallback.isEmpty())
			return false;
		return future.cancel(completionCancellationFallback.get());
	}

	public static boolean cancel(Future<?> future, Boolean mayInterruptIfRunning) {
		if (mayInterruptIfRunning == null)
			return cancel(future, false);
		return complete(future, null,
				mayInterruptIfRunning ? new InterruptCancellationException() : new CancellationException());
	}

	/**
	 * complete stream
	 */

	public static <X> StreamEx<X> completeStream(Iterable<? extends Future<X>> futureIterable) {
		return completeStream(futureIterable, false);
	}

	public static <X> StreamEx<X> completeStream(Iterable<? extends Future<X>> futureIterable, boolean ignoreErrors) {
		return completeStream(futureIterable, ignoreErrors, false);
	}

	public static <X> StreamEx<X> completeStream(Iterable<? extends Future<X>> futureIterable, boolean ignoreErrors,
			boolean preserveOrder) {
		return completeStream(futureIterable, ignoreErrors, preserveOrder, null);
	}

	public static <X> StreamEx<X> completeStream(Iterable<? extends Future<X>> futureIterable, boolean ignoreErrors,
			boolean preserveOrder, Executor readExecutor) {
		return completeStream(futureIterable, ignoreErrors ? t -> Optional.empty() : null, preserveOrder);
	}

	/**
	 * former: public static <XX> StreamEx<XX> completeStream(Iterable<? extends
	 * Future<? extends XX>> input, Consumer<Throwable> onError, Executor
	 * readExecutor, boolean preserveOrder)
	 */
	public static <X> StreamEx<X> completeStream(Iterable<? extends Future<X>> futureIterable,
			Function<Throwable, Optional<X>> onError, boolean preserveOrder) {
		if (futureIterable == null)
			return Streams.empty();
		var producer = new Predicate<Consumer<? super X>>() {

			private final Map<Long, Optional<Future<X>>> submitMap = new ConcurrentHashMap<>();
			private AtomicLong submitIndex = new AtomicLong(Long.MIN_VALUE);
			private final LinkedBlockingQueue<Optional<Future<X>>> resultQueue = new LinkedBlockingQueue<>();
			private AtomicLong resultIndex = !preserveOrder ? null : new AtomicLong(Long.MIN_VALUE);

			private Spliterator<? extends Future<X>> spliterator;

			@Override
			public boolean test(Consumer<? super X> action) {
				try {
					return testInternal(action);
				} catch (Throwable e) {
					destroy();
					throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e)
							: new RuntimeException(e);
				}
			}

			private boolean testInternal(Consumer<? super X> action) throws Throwable {
				if (spliterator == null)
					spliterator = futureIterable.spliterator();
				Optional<Future<X>> blockingFutureOp = null;
				while (!Objects.equals(spliterator, Spliterators.emptySpliterator())) {
					var nextRef = Muto.<Future<X>>create();
					if (!(spliterator.tryAdvance(nextRef))) {
						closeIterable();
						spliterator = Spliterators.emptySpliterator();
						submitMap.put(submitIndex.incrementAndGet(), Optional.empty());
						flushComplete();
						break;
					}
					var next = nextRef.get();
					if (next == null)
						continue;
					if (next.isDone() && !preserveOrder) {
						blockingFutureOp = Optional.of(next);
						break;
					} else {
						var nextIndex = submitIndex.incrementAndGet();
						submitMap.put(nextIndex, Optional.of(next));
						callback(next, () -> {
							if (!preserveOrder && submitMap.remove(nextIndex) != null)
								resultQueue.add(Optional.of(next));
							flushComplete();
						});
					}
					blockingFutureOp = resultQueue.poll();
					if (blockingFutureOp != null)
						break;
				}
				if (blockingFutureOp == null)
					blockingFutureOp = resultQueue.take();
				if (blockingFutureOp.isEmpty()) {
					resultQueue.put(Optional.empty());
					return false;
				}
				var completion = Completion.get(blockingFutureOp.get());
				if (completion.isSuccess()) {
					action.accept(completion.result());
					return true;
				}
				if (onError != null) {
					var nextOp = onError.apply(completion.failure());
					if (nextOp != null) {
						nextOp.ifPresent(action);
						return true;
					}
				}
				throw completion.failure();
			}

			private void flushComplete() {
				while (true) {
					Long nextIndex;
					if (resultIndex == null)
						nextIndex = Optional.ofNullable(submitMap.keySet().iterator())
								.filter(Iterator::hasNext)
								.map(Iterator::next)
								.orElse(null);
					else
						nextIndex = resultIndex.get() + 1;
					if (nextIndex == null)
						break;
					var nextOp = submitMap.get(nextIndex);
					if (nextOp == null || (nextOp.isPresent() && !nextOp.get().isDone()))
						break;
					if (resultIndex != null)
						resultIndex.compareAndSet(nextIndex - 1, nextIndex);
					nextOp = submitMap.remove(nextIndex);
					if (nextOp == null)
						// another thread removed it?
						continue;
					resultQueue.add(nextOp);
				}
			}

			private void closeIterable() {
				CoreReflections.tryCast(futureIterable, BaseStream.class).ifPresent(Streams::close);
			}

			private void destroy() {
				try {
					closeIterable();
				} finally {
					while (!submitMap.isEmpty()) {
						var keyIter = submitMap.keySet().iterator();
						var futureOp = keyIter.hasNext() ? submitMap.remove(keyIter.next()) : null;
						if (futureOp != null && futureOp.isPresent())
							futureOp.get().cancel(true);
					}
				}
			}
		};
		var completeStream = Streams.produce(producer);
		completeStream = completeStream.onClose(producer::destroy);
		return completeStream;
	}

	// callback result logging

	public static <X, F extends Future<X>> F logResultTrace(F future, Object... arguments) {
		return logResult(future, Level.TRACE, arguments);
	}

	public static <X, F extends Future<X>> F logResultDebug(F future, Object... arguments) {
		return logResult(future, Level.DEBUG, arguments);
	}

	public static <X, F extends Future<X>> F logResultInfo(F future, Object... arguments) {
		return logResult(future, Level.INFO, arguments);
	}

	// callback failure logging

	public static <X, F extends Future<X>> F logFailureTrace(F future, boolean suppressCancelFailures,
			Object... arguments) {
		return logFailure(future, Level.TRACE, suppressCancelFailures, arguments);
	}

	public static <X, F extends Future<X>> F logFailureDebug(F future, boolean suppressCancelFailures,
			Object... arguments) {
		return logFailure(future, Level.DEBUG, suppressCancelFailures, arguments);
	}

	public static <X, F extends Future<X>> F logFailureWarn(F future, boolean suppressCancelFailures,
			Object... arguments) {
		return logFailure(future, Level.WARN, suppressCancelFailures, arguments);
	}

	public static <X, F extends Future<X>> F logFailureError(F future, boolean suppressCancelFailures,
			Object... arguments) {
		return logFailure(future, Level.ERROR, suppressCancelFailures, arguments);
	}

	// callback logging

	public static <X, F extends Future<X>> F logResult(F future, Level level, Object... arguments) {
		return log(future, null, level, completion -> {
			if (!completion.isSuccess())
				return null;
			return () -> {
				var stream = Optional.ofNullable(arguments).map(Stream::of).orElse(Stream.empty());
				stream = Stream.concat(stream, Stream.ofNullable(completion.result()));
				return stream.iterator();
			};
		});
	}

	public static <X, F extends Future<X>> F logFailure(F future, Level level, boolean suppressCancelFailures,
			Object... arguments) {
		return log(future, null, level, completion -> {
			if (completion.isSuccess())
				return null;
			if (suppressCancelFailures && Throws.isHalt(completion.failure()))
				return null;
			if (ShutdownNotifier.INSTANCE.isShuttingDown())
				return null;
			return () -> {
				Stream<Object> argumentStream = Optional.ofNullable(arguments).map(Stream::of).orElseGet(Stream::empty);
				if (arguments == null || Stream.of(arguments).noneMatch(completion.failure()::equals))
					argumentStream = Stream.concat(argumentStream, Stream.of(completion.failure()));
				return argumentStream.iterator();
			};
		});
	}

	public static <X, F extends Future<X>> F log(F future, Logger logger, Level level,
			Function<Completion<? super X>, Iterable<Object>> argumentFunction) {
		Objects.requireNonNull(future);
		if (argumentFunction == null)
			return future;
		if (logger == null)
			logger = Logging.logger(getCallingClass());
		LogConsumer logConsumer = Logging.tryGetLevelConsumer(logger, level).orElse(null);
		if (logConsumer == null)
			return future;
		return callback(future, (v, t) -> {
			log(logConsumer, argumentFunction.apply(Completion.of(v, t)));
		});
	}

	private static void log(LogConsumer logConsumer, Iterable<Object> arguments) {
		var argumentIter = Optional.ofNullable(arguments).map(Iterable::iterator).orElse(null);
		if (argumentIter == null || !argumentIter.hasNext())
			return;
		final Object firstArgument;
		final Executor executor;
		{
			var next = argumentIter.next();
			if (next instanceof Executor) {
				executor = (Executor) next;
				if (!argumentIter.hasNext())
					return;
				firstArgument = argumentIter.next();
			} else {
				firstArgument = next;
				executor = SameThreadSubmitterExecutor.instance();
			}
		}
		executor.execute(() -> {
			var streamBuilder = Stream.builder();
			String format;
			if (firstArgument instanceof Throwable) {
				format = ((Throwable) firstArgument).getMessage();
				streamBuilder.accept(firstArgument);
			} else
				format = Objects.toString(firstArgument);
			argumentIter.forEachRemaining(streamBuilder);
			logConsumer.accept(format, streamBuilder.build().toArray());
		});
	}

	// scraps

	public static Scrapable onScrapCancel(Scrapable scrapable, Future<?> future, boolean mayInterruptIfRunning) {
		return onScrapCancel(scrapable, future, mayInterruptIfRunning, null);
	}

	public static Scrapable onScrapCancel(Scrapable scrapable, Future<?> future, boolean mayInterruptIfRunning,
			Executor executor) {
		Objects.requireNonNull(scrapable);
		Objects.requireNonNull(future);
		if (future.isDone())
			return Scrapable.complete();
		var registration = scrapable.onScrap(() -> {
			Futures.cancel(future, mayInterruptIfRunning);
		});
		Futures.callback(future, () -> {
			registration.scrap();
		}, executor);
		return registration;
	}

	public static BooleanSupplier onCallCancel(RunnableListenerHelper listenerHelper, Future<?> future,
			boolean mayInterruptIfRunning) {
		return onCallCancel(listenerHelper, future, mayInterruptIfRunning, null, null);
	}

	public static BooleanSupplier onCallCancel(RunnableListenerHelper listenerHelper, Future<?> future,
			boolean mayInterruptIfRunning, Executor executor) {
		return onCallCancel(listenerHelper, future, mayInterruptIfRunning, executor, executor);
	}

	public static BooleanSupplier onCallCancel(RunnableListenerHelper listenerHelper, Future<?> future,
			boolean mayInterruptIfRunning, Executor queueExecutor, Executor inThreadExecutionExecutor) {
		Objects.requireNonNull(listenerHelper);
		Objects.requireNonNull(future);
		Runnable listener = () -> future.cancel(mayInterruptIfRunning);
		listenerHelper.addListener(listener);
		BooleanSupplier registration = () -> listenerHelper.removeListener(listener);
		Futures.callback(future, () -> registration.getAsBoolean());
		return registration;
	}

	public static Optional<String> getState(Future<?> future) {
		var stateStream = FutureTransform.apply(StateFunction.class, future);
		stateStream = stateStream.filter(Predicate.not(String::isBlank));
		return stateStream.findFirst();
	}

	public static boolean isInterrupted(Future<?> future) {
		if (getState(future).filter(v -> v.toLowerCase().contains("interrupt")).isPresent())
			return true;
		if (FutureTransform.apply(InterruptedPredicate.class, future).anyMatch(v -> v))
			return true;
		var interrupted = Completion.tryGet(future).map(Completion::isInterruption).orElse(false);
		return interrupted;
	}

	public static StreamEx<Runnable> streamContainedRunnables(Object input) {
		return streamContained(Arrays.asList(input), DistinctInstancePredicate.create()).select(Runnable.class);
	}

	public static StreamEx<Callable<?>> streamContainedCallables(Object input) {
		return streamContained(Arrays.asList(input), DistinctInstancePredicate.create()).select(Callable.class)
				.map(v -> (Callable<?>) v);
	}

	private static StreamEx<Object> streamContained(Iterable<? extends Object> inputs,
			Predicate<? super Object> filter) {
		var stream = Streams.<Object>of(inputs).nonNull().filter(filter);
		stream = stream.flatMap(v -> {
			var sb = Stream.<StreamEx<Object>>builder();
			if (v instanceof RunnableContainer)
				sb.add(streamContained(Arrays.asList(((RunnableContainer) v).getContainedRunnable()), filter));
			if (v instanceof CallableContainer)
				sb.add(streamContained(Arrays.asList(((CallableContainer<?>) v).getContainedCallable()), filter));
			if (v instanceof ListenableFutureProxy)
				sb.add(streamContained(Arrays.asList(((ListenableFutureProxy<?>) v).unwrap()), filter));
			if (v instanceof com.lfp.joe.core.classpath.Delegating)
				sb.add(streamContained(((com.lfp.joe.core.classpath.Delegating) v).getTargets(), filter));
			return Streams.of(sb.build()).flatMap(Function.identity()).append(v);
		});
		return stream;
	}

	public static <X> ListenableFuture<X> scheduleWhile(ScheduleWhileRequest<X> request) {
		Objects.requireNonNull(request);
		var scheduler = request.scheduler();
		Callable<X> task = () -> {
			try {
				return request.task().call();
			} catch (Throwable t) {
				return request.errorHandler().apply(t);
			}
		};
		ListenableFuture<X> startingFuture;
		if (Durations.isPostive(request.initialDelay()))
			startingFuture = scheduler.submitScheduled(task, request.initialDelay().toMillis());
		else if (request.initialRunAsync()) {
			startingFuture = scheduler.submit(task);
		} else
			startingFuture = SameThreadSubmitterExecutor.instance().submit(task);
		var timeoutMillis = Durations.max().equals(request.timeout()) ? -1 : request.timeout().toMillis();
		ListenableFuture<X> lfuture = FutureUtils.scheduleWhile(scheduler, request.delay().toMillis(), startingFuture,
				task, request.loopTest(), timeoutMillis, request.timeoutProvideLastValue());
		lfuture.failureCallback(t -> {
			var logFunction = request.errorLogFunction();
			var logConsumer = logFunction.apply(lfuture);
			if (logConsumer != null)
				logConsumer.log("schedule while error:{}", request, t);
		});
		return lfuture;
	}

	private static Optional<LogConsumer> tryGetLevelConsumer(Level level) {
		var logger = Logging.logger(getCallingClass());
		return Logging.tryGetLevelConsumer(logger, level);
	}

	private static Class<?> getCallingClass() {
		return CoreReflections.getCallingClass((i, sf) -> {
			var sfDeclaringClass = sf.getDeclaringClass();
			var ctStream = Stream.concat(Stream.of(sfDeclaringClass),
					Stream.ofNullable(sfDeclaringClass.getDeclaringClass()));
			ctStream = ctStream.filter(Objects::nonNull).distinct();
			var skip = ctStream.anyMatch(v -> {
				if (v.isAnonymousClass())
					return false;
				for (var loggerSkipClassType : LOGGER_SKIP_CLASS_TYPES)
					if (loggerSkipClassType.isAssignableFrom(v))
						return true;
				return false;
			});
			return !skip;
		});
	}

	@Value.Immutable
	public static abstract class ScheduleWhileRequestDef<X> {

		protected static final ThrowingFunction<Throwable, ?, Exception> ERROR_HANDLER_DEFAULT = t -> {
			if (t instanceof Exception)
				throw (Exception) t;
			throw new Exception(t);
		};
		protected static final Predicate<?> LOOP_TEST_DEFAULT = v -> true;
		protected static final LogConsumer LOG_CONSUMER_NO_OP = (format, args) -> {};

		@Value.Default
		SubmitterScheduler scheduler() {
			return Threads.Pools.centralPool();
		}

		@Value.Default
		Duration initialDelay() {
			return Duration.ZERO;
		}

		@Value.Default
		boolean initialRunAsync() {
			return true;
		}

		abstract Duration delay();

		@Nullable
		abstract Callable<X> task();

		@Value.Default
		Predicate<X> loopTest() {
			return (Predicate<X>) LOOP_TEST_DEFAULT;
		}

		@Value.Default
		Duration timeout() {
			return Durations.max();
		}

		@Value.Default
		boolean timeoutProvideLastValue() {
			return false;
		}

		@Value.Default
		ThrowingFunction<Throwable, X, Exception> errorHandler() {
			return (ThrowingFunction<Throwable, X, Exception>) ERROR_HANDLER_DEFAULT;
		}

		@Value.Default
		Function<ListenableFuture<X>, LogConsumer> errorLogFunction() {
			var levelConsumer = tryGetLevelConsumer(Level.WARN).orElse(null);
			if (levelConsumer == null)
				return f -> LOG_CONSUMER_NO_OP;
			return f -> {
				if (f.isCancelled())
					return LOG_CONSUMER_NO_OP;
				return levelConsumer;
			};
		}

		public ListenableFuture<X> schedule() {
			return scheduleWhile(ScheduleWhileRequest.copyOf(this));
		}

		@Value.Check
		protected void validate() {
			if (!Durations.isNegativeOrZero(initialDelay()) && !initialRunAsync())
				throw new IllegalArgumentException("initial run must be async if delayed:" + initialDelay());
		}

		public static ScheduleWhileRequest.Builder<Void> builder(Runnable task) {
			return builder(Executors.callable(task, null));
		}

		public static <X> ScheduleWhileRequest.Builder<X> builder(Callable<X> task) {
			return ScheduleWhileRequest.<X>builder().task(task);
		}

		@Deprecated
		public static ScheduleWhileRequest.Builder<Nada> builder(Duration delay, Runnable task,
				Supplier<Boolean> looptest) {
			var blder = ScheduleWhileRequest.<Nada>builder();
			if (delay != null)
				blder.delay(delay);
			if (task != null)
				blder.task(Executors.callable(task, Nada.get()));
			if (looptest != null)
				blder.loopTest(nil -> Boolean.TRUE.equals(looptest.get()));
			return blder;
		}

		@Deprecated
		public static <X> ScheduleWhileRequest.Builder<X> builder(Duration delay, Callable<X> task,
				Predicate<X> looptest) {
			var blder = ScheduleWhileRequest.<X>builder();
			if (delay != null)
				blder.delay(delay);
			if (task != null)
				blder.task(task);
			if (looptest != null)
				blder.loopTest(looptest);
			return blder;
		}
	}

	public static void main(String[] args) throws InterruptedException {
		var csFutrueTask = new CSFutureTask<>(() -> {
			while (!Thread.currentThread().isInterrupted()) {
				Thread.sleep(2_000);
				System.out.println("running");
			}
			return null;
		});
		CoreTasks.executor().execute(csFutrueTask);
		System.out.println(Threads.Futures.isInterrupted(csFutrueTask));
		Thread.sleep(2_000);
		csFutrueTask.cancel(true);
		System.out.println(Threads.Futures.isInterrupted(csFutrueTask));

		var futureStream = IntStreamEx.range(0, 100).mapToObj(v -> {
			if (v > 50 && v < 60) {
				return CompletableFuture.completedFuture(v);
			}
			var future = Threads.Pools.centralPool().submit(() -> {

				try {
					System.out.println("starting:" + v);

					if (v > 90) {
						Thread.sleep(5_000);
						throw new RuntimeException(">90");
					} else {
						var max = v < 50 ? 3_000 : 8_000;
						var min = 1_000;
						final long mod = max - min + 1L;
						final int next = (int) (Math.abs(new Random().nextLong() % mod) + min);
						Thread.sleep(next);
						if (v > 40 && v % 2 == 0) {
							// throw new RuntimeException("even number!");
						}
					}

					System.out.println("done:" + v);
					return v;
				} catch (Throwable t) {
					System.err.println(v + ": " + t.getMessage());
					throw t;
				}
			});
			return future;
		});

		try

		{
			var resultStream = Futures.completeStream(futureStream, true, false);
			resultStream.forEach(v -> {
				System.out.println(v);
			});
			Thread.sleep(100);
		} catch (Throwable t) {
			t.printStackTrace();
		}
		System.err.println("DONE_ALL");
		Thread.currentThread().join();
	}

}
