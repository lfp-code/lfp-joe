package com.lfp.joe.threads.executor;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

public class ForkJoinPoolExt extends ForkJoinPool implements AutoCloseable {

	private boolean shutdownNowOnClose;

	public ForkJoinPoolExt() {
		super();
	}

	public ForkJoinPoolExt(int parallelism) {
		super(parallelism);
	}

	public ForkJoinPoolExt(int parallelism, ForkJoinWorkerThreadFactory factory, UncaughtExceptionHandler handler,
			boolean asyncMode) {
		super(parallelism, factory, handler, asyncMode);
	}

	public ForkJoinPoolExt(int parallelism, ForkJoinWorkerThreadFactory factory, UncaughtExceptionHandler handler,
			boolean asyncMode, int corePoolSize, int maximumPoolSize, int minimumRunnable,
			Predicate<? super ForkJoinPool> saturate, long keepAliveTime, TimeUnit unit) {
		super(parallelism, factory, handler, asyncMode, corePoolSize, maximumPoolSize, minimumRunnable, saturate,
				keepAliveTime, unit);
	}

	public ForkJoinPoolExt shutdownNowOnClose() {
		this.shutdownNowOnClose = true;
		return this;
	}

	public ForkJoinPoolExt shutdownOnClose() {
		this.shutdownNowOnClose = false;
		return this;
	}

	@Override
	public void close() {
		if (shutdownNowOnClose)
			super.shutdownNow();
		else
			super.shutdown();
	}

}
