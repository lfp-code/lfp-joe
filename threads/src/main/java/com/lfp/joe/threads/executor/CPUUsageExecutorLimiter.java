package com.lfp.joe.threads.executor;

import java.time.Duration;
import java.util.Date;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.threads.cpu.AbstractCPUUsageLimiter;
import com.lfp.joe.threads.cpu.CPUUsageLimiter;

public class CPUUsageExecutorLimiter extends SubmitterSchedulerLFP.Limiter implements CPUUsageLimiter {
	private AbstractCPUUsageLimiter cpuLimiter;

	public CPUUsageExecutorLimiter(Executor executor, double cpuTargetUsage, Duration updateInterval) {
		this(executor, cpuTargetUsage, updateInterval, null, null);
	}

	public CPUUsageExecutorLimiter(Executor executor, double cpuTargetUsage, Duration updateInterval,
			Integer initialMaxConcurrency, Double cpuTargetUsageRange) {
		super(executor,
				Optional.ofNullable(initialMaxConcurrency).orElseGet(() -> MachineConfig.logicalCoreCount()),
				DEFAULT_LIMIT_FUTURE_LISTENER_EXECUTION);
		this.cpuLimiter = new AbstractCPUUsageLimiter(cpuTargetUsage, updateInterval, cpuTargetUsageRange) {

			@Override
			protected void setMaxConcurrency(int maxConcurrency) {
				CPUUsageExecutorLimiter.this.setMaxConcurrency(maxConcurrency);
			}

			@Override
			protected boolean hasCapacity() {
				return CPUUsageExecutorLimiter.this.waitingTasks.isEmpty();
			}

			@Override
			public int getMaxConcurrency() {
				return CPUUsageExecutorLimiter.this.getMaxConcurrency();
			}
		};
	}

	@Override
	public boolean isScrapped() {
		return this.cpuLimiter.isScrapped();
	}

	@Override
	public boolean scrap() {
		return this.cpuLimiter.scrap();
	}

	@Override
	public Scrapable onScrap(Runnable runnable) {
		return this.cpuLimiter.onScrap(runnable);
	}

	@Override
	public void enableLogging(Logger logger) {
		this.cpuLimiter.enableLogging(logger);

	}

	@Override
	public void disableLogging() {
		this.cpuLimiter.disableLogging();

	}

	public static void main(String[] args) throws InterruptedException {
		var exec = Executors.newCachedThreadPool();
		var limiter = new CPUUsageExecutorLimiter(exec, .80, Duration.ofSeconds(2));
		limiter.enableLogging();
		var threadIndex = new AtomicLong(-1);
		Runnable task = () -> {
			System.out.println("starting thread:" + threadIndex.incrementAndGet());
			var quitAt = new Date(System.currentTimeMillis() + Duration.ofSeconds(45).toMillis());
			var rng = new Random();
			var store = 1;
			long counter = -1;

			while (!Thread.currentThread().isInterrupted() && new Date().before(quitAt)) {
				counter++;
				if (counter == 500) {
					counter = -1;
					Threads.sleepUnchecked(1);
				}
				double r = rng.nextFloat();
				double v = Math.sin(Math.cos(Math.sin(Math.cos(r))));
				store *= v;
			}
		};
		for (int i = 0; i < 10_000; i++)
			limiter.execute(task);
		Thread.currentThread().join();
	}

}
