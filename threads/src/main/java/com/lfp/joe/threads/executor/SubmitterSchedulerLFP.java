package com.lfp.joe.threads.executor;

import java.lang.reflect.Constructor;
import java.lang.reflect.Proxy;
import java.time.Duration;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Delayed;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.LongBinaryOperator;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.threadly.concurrent.RunnableCallableAdapter;
import org.threadly.concurrent.RunnableContainer;
import org.threadly.concurrent.SubmitterScheduler;
import org.threadly.concurrent.event.RunnableListenerHelper;
import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.ListenableFutureTask;
import org.threadly.concurrent.future.SettableListenableFuture;
import org.threadly.concurrent.wrapper.compatibility.ListenableScheduledFuture;
import org.threadly.concurrent.wrapper.interceptor.ExecutorTaskInterceptor;
import org.threadly.concurrent.wrapper.limiter.ExecutorLimiter;
import org.threadly.concurrent.wrapper.traceability.ThreadRenamingRunnable;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.MethodRequest;
import com.lfp.joe.core.classpath.InterfaceSet;
import com.lfp.joe.core.classpath.InvocationHandlerLFP;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Durations;
import com.lfp.joe.core.function.FNV;
import com.lfp.joe.core.function.Sequencer;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.threads.future.ImmutableDelayed;
import com.lfp.joe.threads.future.ListenableFutureProxy;
import com.lfp.joe.threads.future.ListenableFutureWrapper;

import one.util.streamex.IntStreamEx;

@SuppressWarnings("unchecked")
public interface SubmitterSchedulerLFP extends ExecutorServices.Wrapper, ScheduledExecutorService, SubmitterScheduler {

	@Override
	default ListenableFuture<?> submit(Runnable task) {
		return ExecutorServices.Wrapper.super.submit(task);
	}

	@Override
	default <T> ListenableFuture<T> submit(Runnable task, T result) {
		return ExecutorServices.Wrapper.super.submit(task, result);
	}

	@Override
	default <T> ListenableFuture<T> submit(Callable<T> task) {
		return ExecutorServices.Wrapper.super.submit(task);
	}

	default SubmitterSchedulerLFP.Limiter limit() {
		return limitCoreMultiplier(1);
	}

	default SubmitterSchedulerLFP.Limiter limitCoreMultiplier(int multiplier) {
		return limit(MachineConfig.logicalCoreCount() * multiplier);
	}

	default SubmitterSchedulerLFP.Limiter limit(int parallelism) {
		return new Limiter(this, parallelism);
	}

	/**
	 * SubmitterScheduler
	 */

	@Override
	default void schedule(Runnable task, long delayInMs) {
		if (delayInMs > 0)
			CompletableFuture.delayedExecutor(delayInMs, TimeUnit.MILLISECONDS, this).execute(task);
		else
			execute(task);
	}

	@Override
	default ListenableFuture<?> submitScheduled(Runnable task, long delayInMs) {
		return submitScheduled(task, null, delayInMs);
	}

	@Override
	default <T> ListenableFuture<T> submitScheduled(Runnable task, T result, long delayInMs) {
		return submitScheduled(RunnableCallableAdapter.adapt(task, result), delayInMs);
	}

	@Override
	default <T> ListenableFuture<T> submitScheduled(Callable<T> task, long delayInMs) {
		var lfTask = new ListenableFutureTask<>(task, this);
		schedule(lfTask, delayInMs);
		return lfTask;
	}

	@Override
	default void scheduleWithFixedDelay(Runnable task, long initialDelay, long recurringDelay) {
		var delayed = ImmutableDelayed.builder()
				.initial(Duration.ofMillis(initialDelay))
				.delay(Duration.ofMillis(recurringDelay))
				.build();
		scheduleWithFixedDelay(task, delayed, true);
	}

	@Override
	default void scheduleAtFixedRate(Runnable task, long initialDelay, long period) {
		var delayed = ImmutableDelayed.builder()
				.initial(Duration.ofMillis(initialDelay))
				.period(Duration.ofMillis(period))
				.build();
		FutureUtils.executeWhile(() -> {
			var delayInMs = delayed.getDelay(TimeUnit.MILLISECONDS);
			delayed.advance();
			delayInMs = Math.max(0, delayInMs);
			var future = submitScheduled(task, delayInMs);
			return future.flatMapFailure(Throwable.class, t -> {
				if (future.isCancelled())
					return FutureUtils.immediateFailureFuture(t);
				Const.LOGGER.warn("fixed rate error. delayed:{}", delayed, t);
				return FutureUtils.immediateResultFuture(null);
			});
		}, nil -> !isDoneFuture(task));
	}

	/**
	 * ScheduledExecutorService
	 */

	@Override
	default ListenableScheduledFuture<?> schedule(Runnable command, long delay, TimeUnit unit) {
		return schedule(RunnableCallableAdapter.adapt(command, null), delay, unit);
	}

	@Override
	default <V> ListenableScheduledFuture<V> schedule(Callable<V> task, long delay, TimeUnit unit) {
		var delayed = ImmutableDelayed.builder().initial(Durations.from(delay, unit)).build();
		var lfuture = submitScheduled(task, delayed.getDelay(TimeUnit.MILLISECONDS));
		return createListenableScheduledFuture(lfuture, delayed);
	}

	@Override
	default ListenableScheduledFuture<?> scheduleAtFixedRate(Runnable command, long initialDelay, long period,
			TimeUnit unit) {
		var delayed = ImmutableDelayed.builder()
				.initial(Durations.from(initialDelay, unit))
				.delay(Durations.from(period, unit))
				.build();
		var sfuture = new Const.SettableListenableFutureExt<>(false);
		Consumer<ListenableFuture<?>> futureLink = f -> {
			Runnable listener = () -> f.cancel(true);
			sfuture.listener(listener);
			f.listener(() -> {
				sfuture.getListenerHelper().removeListener(listener);
			});
			f.failureCallback(sfuture::setFailure);
		};
		var executeWhileFuture = FutureUtils.executeWhile(() -> {
			var delayInMs = delayed.getDelay(TimeUnit.MILLISECONDS);
			delayed.advance();
			var scheduleFuture = submitScheduled(() -> {
				// does not block
			}, delayInMs);
			futureLink.accept(scheduleFuture);
			scheduleFuture.resultCallback(nil -> {
				var submitFuture = submit(command);
				futureLink.accept(submitFuture);
			});
			return scheduleFuture;
		}, nil -> {
			if (isDoneFuture(command))
				return false;
			if (sfuture.isDone())
				return false;
			return true;
		});
		futureLink.accept(executeWhileFuture);
		return createListenableScheduledFuture(sfuture, delayed);
	}

	@Override
	default ListenableScheduledFuture<?> scheduleWithFixedDelay(Runnable task, long initialDelay, long delay,
			TimeUnit unit) {
		var delayed = ImmutableDelayed.builder()
				.initial(Durations.from(initialDelay, unit))
				.delay(Durations.from(delay, unit))
				.build();
		var lfuture = scheduleWithFixedDelay(task, delayed, false);
		return createListenableScheduledFuture(lfuture, delayed);
	}

	private ListenableFuture<?> scheduleWithFixedDelay(Runnable task, ImmutableDelayed delayed,
			boolean suppressFailures) {
		return FutureUtils.executeWhile(() -> {
			var future = submitScheduled(task, delayed.getDelay(TimeUnit.MILLISECONDS)).listener(delayed::advance);
			if (!suppressFailures)
				return future;
			return future.flatMapFailure(Throwable.class, t -> {
				if (future.isCancelled())
					return FutureUtils.immediateFailureFuture(t);
				Const.LOGGER.warn("fixed delay error. delayed:{}", delayed, t);
				return FutureUtils.immediateResultFuture(null);
			});
		}, nil -> !isDoneFuture(task));
	}

	public static SubmitterSchedulerLFP from(Executor executor) {
		Objects.requireNonNull(executor);
		if (executor instanceof SubmitterSchedulerLFP)
			return (SubmitterSchedulerLFP) executor;
		var interfaceSet = new InterfaceSet(Predicate.not(Proxy::isProxyClass), SubmitterSchedulerLFP.class,
				ExecutorServices.Wrapper.class, executor);
		InvocationHandlerLFP invocationHandler = request -> {
			var method = request.method();
			if (ExecutorContainer.GET_CONTAINED_EXECUTOR_METHOD.equals(method))
				return () -> executor;
			if (method.getDeclaringClass().isInstance(executor))
				return () -> request.invoke(executor);
			var methodLookupReq = MethodRequest.of(executor.getClass(), method.getReturnType(), method.getName(),
					method.getParameterTypes());
			var methodLookup = MemberCache.tryGetMethod(methodLookupReq, true).orElse(null);
			if (methodLookup != null)
				return () -> methodLookup.invoke(executor, request.args());
			return null;
		};
		return invocationHandler.newProxyInstance(interfaceSet.toArray());
	}

	private static boolean isDoneFuture(Object task) {
		if (task == null)
			return false;
		if (task instanceof Future)
			return ((Future<?>) task).isDone();
		if (task instanceof Callable && task instanceof RunnableContainer) {
			var containedRunnable = ((RunnableContainer) task).getContainedRunnable();
			if (task != containedRunnable && isDoneFuture(containedRunnable))
				return true;
		}
		return false;
	}

	private static <V> ListenableScheduledFuture<V> createListenableScheduledFuture(
			ListenableFuture<V> listenableFuture, Delayed delayed) {
		return (ListenableScheduledFuture<V>) Throws.unchecked(() -> {
			return Const.ScheduledFutureDelegate_CONSTRUCTOR.newInstance(listenableFuture, delayed);
		});
	}

	public static class Limiter extends ExecutorLimiter implements SubmitterSchedulerLFP {

		public Limiter(Executor executor, int maxConcurrency) {
			this(executor, maxConcurrency, DEFAULT_LIMIT_FUTURE_LISTENER_EXECUTION);
		}

		public Limiter(Executor executor, int maxConcurrency, boolean limitFutureListenersExecution) {
			super(new LimiterExecutor(executor, maxConcurrency, limitFutureListenersExecution), maxConcurrency,
					limitFutureListenersExecution);
		}

		@Override
		public Executor getContainedExecutor() {
			return executor;
		}

		protected static class LimiterExecutor extends ExecutorTaskInterceptor implements Executor, ExecutorContainer {
			private static final Sequencer SEQUENCER = new Sequencer();

			private final String threadName;

			public LimiterExecutor(Executor executor, int maxConcurrency, boolean limitFutureListenersExecution) {
				super(executor);
				String callingClassName;
				{
					callingClassName = CoreReflections
							.getCallingClass(LimiterExecutor.class, Limiter.class, SubmitterSchedulerLFP.class)
							.getSimpleName()
							.split(Pattern.quote("$"), 1)[0];
					if (callingClassName.isBlank())
						callingClassName = null;
				}
				String sequenceIdHash = Long.toHexString(FNV.hash64(SEQUENCER.next().values().toArray()));
				String threadName = Stream
						.of(callingClassName, Limiter.class.getSimpleName(), "{" + maxConcurrency + "}", sequenceIdHash)
						.filter(Objects::nonNull)
						.collect(Collectors.joining("-"));
				if (DEFAULT_LIMIT_FUTURE_LISTENER_EXECUTION != limitFutureListenersExecution)
					threadName += "-[" + limitFutureListenersExecution + "]";
				this.threadName = threadName;
			}

			@Override
			public Executor getContainedExecutor() {
				return super.parentExecutor;
			}

			@Override
			public Runnable wrapTask(Runnable task) {
				task = new ThreadRenamingRunnable(task, threadName, false);
				return task;
			}

		}

	}

	static enum Const {
		;

		private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(SubmitterSchedulerLFP.class);
		private static final Constructor<?> ScheduledFutureDelegate_CONSTRUCTOR = Throws.unchecked(() -> {
			var className = ListenableScheduledFuture.class.getPackageName()
					+ ".AbstractExecutorServiceWrapper$ScheduledFutureDelegate";
			var classType = Class.forName(className, false, Thread.currentThread().getContextClassLoader());
			var ctor = classType.getDeclaredConstructor(ListenableFuture.class, Delayed.class);
			ctor.setAccessible(true);
			return ctor;
		});

		private static class SettableListenableFutureExt<X> extends SettableListenableFuture<X> {

			public SettableListenableFutureExt(boolean throwIfAlreadyComplete) {
				super(throwIfAlreadyComplete);
			}

			public RunnableListenerHelper getListenerHelper() {
				return this.listenerHelper;
			}
		}

	}

	public static void main(String[] args) throws InterruptedException {
		var exec = Threads.Pools.centralPool();
		var limited = exec.limit(4);
		var runningThreads = new AtomicInteger();
		var maxThreads = new AtomicInteger();
		LongBinaryOperator randomInclusive = (min, max) -> {
			var random = new Random();
			return random.nextLong() % (max - min) + max;
		};
		var futures = IntStreamEx.range(100).mapToObj(i -> {
			return limited.submitScheduled(() -> {
				try {
					var count = runningThreads.incrementAndGet();
					maxThreads.updateAndGet(v -> Math.max(v, count));
					var out = String.format("%s executing - %s - %s", i, maxThreads.get(),
							Thread.currentThread().getName());
					System.out.println(out);
					Thread.sleep(randomInclusive.applyAsLong(250, 1750));
					return null;
				} finally {
					runningThreads.decrementAndGet();
				}
			}, randomInclusive.applyAsLong(250, 1_500));

		}).map(v -> {
			return ListenableFutureProxy.from(v, null, ListenableFutureWrapper.class);
		}).toList();
		System.out.println(futures.size());
		var count = Streams.of(futures).flatMap(v -> Threads.Futures.streamContainedRunnables(v)).distinct().count();
		System.out.println(count);
		Thread.sleep(4_000);
		limited.shutdownNow();
		while (true) {
			var notDone = Streams.of(futures).filter(Predicate.not(Future::isDone)).count();
			if (notDone == 0)
				break;
			System.out.println("not done:" + notDone);
			Thread.sleep(1_000);
		}
		Thread.currentThread().join();
	}

}
