package com.lfp.joe.threads.future.transform;

import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.function.BiConsumer;

import com.lfp.joe.core.classpath.Instances.Autowired;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.threads.Threads;

@SuppressWarnings({ "unchecked", "rawtypes" })

@Autowired
public class CallbackConsumerJdk extends CallbackConsumer.Abs<Future> {

	@Override
	protected Class<? extends Future> classType() {
		return Future.class;
	}

	@Override
	protected Optional<Boolean> applyInternal(Future future, BiConsumer<?, ? super Throwable> action,
			Executor executor) {
		if (future instanceof CompletionStage)
			return applyInternal((CompletionStage) future, action, executor);
		return Optional.empty();
	}

	protected Optional<Boolean> applyInternal(CompletionStage future, BiConsumer<?, ? super Throwable> action,
			Executor executor) {
		CompletionStage completionStage = future;
		if (executor == null)
			completionStage.whenComplete(action);
		else
			completionStage.whenCompleteAsync(action, executor);
		return Optional.of(true);
	}

	public static void main(String[] args) throws InterruptedException {
		var ft = new FutureTask<>(() -> {
			throw new IllegalArgumentException("rats");
		});
		Threads.Futures.callback(ft, completion -> {
			System.out.println(completion);
		});
		CoreTasks.executor().execute(ft);
		Thread.currentThread().join();
	}
}
