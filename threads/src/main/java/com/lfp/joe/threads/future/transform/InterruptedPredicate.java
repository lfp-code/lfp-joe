package com.lfp.joe.threads.future.transform;

public interface InterruptedPredicate extends FutureTransform<Void, Boolean> {

}
