package com.lfp.joe.threads.executor;

import java.lang.reflect.Method;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.function.Predicate;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.DistinctInstancePredicate;
import com.lfp.joe.core.function.Throws;

import one.util.streamex.StreamEx;

public interface ExecutorContainer {

	public static final Method GET_CONTAINED_EXECUTOR_METHOD = Throws
			.unchecked(() -> new Object() {}.getClass().getEnclosingClass().getMethod("getContainedExecutor"));

	Executor getContainedExecutor();

	public static StreamEx<Executor> streamContainedExecutors(Executor executor) {
		Predicate<Executor> hasNext = Objects::nonNull;
		hasNext = hasNext.and(DistinctInstancePredicate.create());
		return StreamEx.iterate(executor, hasNext, prev -> {
			return CoreReflections.tryCast(prev, ExecutorContainer.class)
					.map(ExecutorContainer::getContainedExecutor)
					.orElse(null);
		});
	}

	public static void main(String[] args) {
		System.out.println(GET_CONTAINED_EXECUTOR_METHOD);
	}

}
