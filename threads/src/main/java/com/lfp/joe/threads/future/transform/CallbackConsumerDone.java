package com.lfp.joe.threads.future.transform;

import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.function.BiConsumer;

import com.lfp.joe.core.classpath.Instances.Autowired;
import com.lfp.joe.core.process.future.Completion;

@SuppressWarnings("rawtypes")

@Autowired(priority = Integer.MAX_VALUE)
public class CallbackConsumerDone extends CallbackConsumer.Abs<Future> {

	@Override
	protected Class<? extends Future> classType() {
		return Future.class;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Optional<Boolean> applyInternal(Future future, BiConsumer<?, ? super Throwable> action,
			Executor executor) {
		Optional<Completion> completionOp = Completion.tryGet(future);
		return completionOp.map(completion -> {
			return completion.complete(action, executor);
		});
	}

}
