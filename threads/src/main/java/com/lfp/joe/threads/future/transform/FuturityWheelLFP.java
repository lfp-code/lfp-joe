package com.lfp.joe.threads.future.transform;

import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import javax.annotation.Nullable;

import org.immutables.value.Value;
import org.jctools.queues.MpscChunkedArrayQueue;
import org.jctools.util.Pow2;
import org.threadly.concurrent.wrapper.traceability.ThreadRenamingRunnable;

import com.lfp.joe.core.classpath.ImmutableInvocationHandlerLFP.InvocationHandlerRequest;
import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.InvocationHandlerLFP;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.function.Muto;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.core.process.future.Completion;
import com.lfp.joe.core.process.future.InterruptCancellationException;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.threads.future.transform.ImmutableFuturityWheelLFP.CacheKey;
import com.spikhalskiy.futurity.FuturityWheel;

@SuppressWarnings("unchecked")
@Value.Enclosing
public class FuturityWheelLFP extends FuturityWheel {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();

	private static final Map<CacheKey<?>, CompletableFuture<?>> SHIFT_CACHE = new ConcurrentHashMap<>();
	private static final ScheduledExecutorService NO_OP_SCHEDULED_EXECUTOR_SERVICE;
	static {
		var invocationHandler = new InvocationHandlerLFP() {

			@Override
			public ThrowingSupplier<Object, Throwable> getInvoker(InvocationHandlerRequest request) throws Throwable {
				return null;
			}

			@Override
			public ThrowingSupplier<Object, Throwable> getDefaultInvoker(InvocationHandlerRequest request) {
				var invoker = InvocationHandlerLFP.super.getDefaultInvoker(request);
				if (invoker != null)
					return invoker;
				return () -> null;
			}
		};
		NO_OP_SCHEDULED_EXECUTOR_SERVICE = invocationHandler.newProxyInstance(ScheduledExecutorService.class);
	}

	public static final FuturityWheelLFP INSTANCE = new FuturityWheelLFP();

	private FuturityWheelLFP() {
		super(NO_OP_SCHEDULED_EXECUTOR_SERVICE, TimeUnit.MILLISECONDS.toNanos(1), TimeUnit.MILLISECONDS.toNanos(1) / 2);
		var taskSubmissions = new MpscChunkedArrayQueue<>(50, Pow2.MAX_POW2, true);
		setFieldValue("taskSubmissions", taskSubmissions);
		var defaultThreadFactory = Executors.defaultThreadFactory();
		var executorService = Executors.newScheduledThreadPool(1, r -> {
			r = new ThreadRenamingRunnable(r, THIS_CLASS.getSimpleName(), true);
			var thread = defaultThreadFactory.newThread(r);
			thread.setDaemon(true);
			return thread;
		});
		setFieldValue("executorService", executorService);
		var scheduledFuture = executorService.scheduleAtFixedRate(WORK, basicPoolPeriodNs, basicPoolPeriodNs,
				TimeUnit.NANOSECONDS);
		setFieldValue("scheduledFuture", scheduledFuture);
	}

	@Override
	public <V> CompletableFuture<V> shift(Future<V> future) {
		return asCompletableFuture(future, null);
	}

	public <V> CompletionStage<V> asCompletionStage(Future<V> future, Executor executor) {
		Objects.requireNonNull(future);
		if (future instanceof CompletionStage)
			return (CompletionStage<V>) future;
		return asCompletableFuture(future, executor);
	}

	public <V> CompletableFuture<V> asCompletableFuture(Future<V> future, Executor executor) {
		return asCompletableFuture(future, null, executor);
	}

	public <V> CompletableFuture<V> asCompletableFuture(Future<V> future, Optional<Boolean> cancellationFallback,
			Executor executor) {
		Objects.requireNonNull(future);
		return Optional.ofNullable(FuturityWheel.trivialCast(future)).or(() -> {
			if (!future.isDone())
				return Optional.empty();
			return Optional.of(Completion.getNow(future).toFuture());
		}).orElseGet(() -> {
			var cacheKeyRef = Muto.<CacheKey<V>>create();
			var cFuture = (CompletableFuture<V>) SHIFT_CACHE.computeIfAbsent(CacheKey.of(future, cancellationFallback),
					k -> {
						cacheKeyRef.set((CacheKey<V>) k);
						return this.createCompletableFuture(k);
					});
			cacheKeyRef.tryGet().ifPresent(k -> {
				cFuture.whenComplete((v, t) -> {
					SHIFT_CACHE.remove(k);
				});
			});
			return withExecutor(cFuture, executor);
		});
	}

	protected <V> CompletableFuture<V> createCompletableFuture(CacheKey<V> cacheKey) {
		var future = cacheKey.future();
		var cfuture = new CompletableFuture<V>() {

			@Override
			public boolean cancel(boolean mayInterruptIfRunning) {
				future.cancel(mayInterruptIfRunning);
				if (mayInterruptIfRunning)
					super.completeExceptionally(new InterruptCancellationException());
				else
					super.cancel(mayInterruptIfRunning);
				return isCancelled();
			}

		};
		cfuture.whenComplete((nilv, nilt) -> {
			cacheKey.cancellationFallback().ifPresent(v -> future.cancel(v));
		});
		submit(future, cfuture);
		return cfuture;
	}

	private void setFieldValue(String fieldName, Object value) {
		MemberCache.setFieldValue(FieldRequest.of(FuturityWheel.class, null, fieldName), this, value);
	}

	private static <V> CompletableFuture<V> withExecutor(CompletableFuture<V> cFuture, Executor executor) {
		executor = Optional.ofNullable(executor).orElseGet(Threads.Pools::centralPool);
		return cFuture.handleAsync((v, t) -> {
			return Completion.of(v, t).toFuture();
		}, executor).thenCompose(Function.identity());
	}

	@Value.Immutable
	static abstract class AbstractCacheKey<X> {

		@Value.Parameter(order = 0)
		abstract Future<X> future();

		@Nullable
		@Value.Parameter(order = 1)
		abstract Optional<Boolean> cancellationFallback();

		@Value.Check
		protected AbstractCacheKey<X> check() {
			if (cancellationFallback() == null)
				return CacheKey.copyOf(this).withCancellationFallback(Threads.Futures.cancellationDefault());
			return this;
		}
	}

	public static void main(String[] args) {
		var ck = CacheKey.of(new FutureTask<>(Date::new), null);
		System.out.println(ck);
		NO_OP_SCHEDULED_EXECUTOR_SERVICE.execute(() -> {});
		System.out.println("done");
	}

}
