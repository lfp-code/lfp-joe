package com.lfp.joe.threads.batcher;

import java.time.Duration;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

import org.threadly.concurrent.SubmitterExecutor;
import org.threadly.concurrent.SubmitterScheduler;
import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.SettableListenableFuture;
import org.threadly.concurrent.wrapper.SubmitterExecutorAdapter;

import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;

import one.util.streamex.StreamEx;

public abstract class AsyncBatcher<X, Y> implements Function<X, ListenableFuture<Y>> {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private final Map<Nada, List<Entry<X, SettableListenableFuture<Y>>>> futureMap = new ConcurrentHashMap<>(1);
	private final SubmitterExecutor executor;
	private final SubmitterScheduler submitterScheduler;
	private int maxBatchSize = -1;
	private Entry<Date, ListenableFuture<?>> scheduledFlushEntry;

	public AsyncBatcher(Executor executor) {
		this(executor, Threads.Pools.centralPool());
	}

	public AsyncBatcher(Executor executor, SubmitterScheduler submitterScheduler) {
		this.executor = SubmitterExecutorAdapter.adaptExecutor(executor);
		this.submitterScheduler = Objects.requireNonNull(submitterScheduler);
	}

	public List<Entry<X, SettableListenableFuture<Y>>> getCurrentRequests() {
		return access((list, cb) -> {
			List<Entry<X, SettableListenableFuture<Y>>> listCopy = list == null ? List.of() : List.copyOf(list);
			cb.accept(listCopy);
			return list;
		});
	}

	public ListenableFuture<List<Entry<X, Y>>> getCurrentRequestsCompleteFuture() {
		return getCurrentRequestsCompleteFuture(false);
	}

	public ListenableFuture<List<Entry<X, Y>>> getCurrentRequestsCompleteFuture(boolean ignoreFailedFutures) {
		var futures = Streams.of(getCurrentRequests()).map(v -> v.getValue().map(vv -> {
			return new SimpleImmutableEntry<>(v.getKey(), vv);
		})).toList();
		return FutureUtils.makeResultListFuture(futures, ignoreFailedFutures);
	}

	@Override
	public ListenableFuture<Y> apply(X input) {
		return apply(input, null, -1);
	}

	public ListenableFuture<Y> apply(X input, Duration maxDuration) {
		return apply(input, maxDuration, -1);
	}

	public ListenableFuture<Y> apply(X input, Duration maxDuration, int maxBatchSize) {
		if (maxBatchSize != -1 && maxBatchSize < 1)
			throw new IllegalArgumentException("invalid maxBatchSize:" + maxBatchSize);
		return access((list, cb) -> {
			if (list == null)
				list = new ArrayList<>();
			var ent = new SimpleImmutableEntry<>(input, new SettableListenableFuture<Y>(false));
			cb.accept(ent.getValue());
			list.add(ent);
			var scheduledFlushAt = Optional.ofNullable(scheduledFlushEntry).map(Entry::getKey).orElse(null);
			var scheduledFlushFuture = Optional.ofNullable(scheduledFlushEntry).map(Entry::getValue).orElse(null);
			if (this.maxBatchSize == -1 || this.maxBatchSize > maxBatchSize)
				this.maxBatchSize = maxBatchSize;
			if (this.maxBatchSize >= 0 && list.size() > this.maxBatchSize) {
				Threads.Futures.cancel(scheduledFlushFuture, true);
				flush(list);
				return null;
			}
			if (maxDuration != null) {
				var flushAt = new Date(System.currentTimeMillis() + maxDuration.toMillis());
				if (scheduledFlushAt == null
						|| new Date(System.currentTimeMillis() + maxDuration.toMillis()).before(scheduledFlushAt)) {
					Threads.Futures.cancel(scheduledFlushFuture, true);
					var future = this.submitterScheduler.submitScheduled(() -> flush(), maxDuration.toMillis());
					future = future.failureCallback(t -> onError(t));
					this.scheduledFlushEntry = new SimpleImmutableEntry<>(flushAt, future);
				}
			}
			return list;
		});
	}

	protected void onError(Throwable failure) {
		if (Throws.isHalt(failure))
			return;
		logger.error("error during flush", failure);
	}

	protected void flush() {
		this.access((list, cb) -> {
			flush(list);
			return null;
		});
	}

	protected void flush(List<Entry<X, SettableListenableFuture<Y>>> entryList) {
		if (entryList == null || entryList.isEmpty())
			return;
		var future = this.executor.submit(() -> {
			Map<X, Y> resultMap = getResultMap(Streams.of(entryList).map(v -> v.getKey()).distinct());
			for (var ent : entryList) {
				var result = resultMap.get(ent.getKey());
				ent.getValue().setResult(result);
			}
		});
		future.failureCallback(t -> {
			entryList.forEach(v -> v.getValue().setFailure(t));
		});
	}

	protected <Z> Z access(
			BiFunction<? super List<Entry<X, SettableListenableFuture<Y>>>, Consumer<Z>, ? extends List<Entry<X, SettableListenableFuture<Y>>>> remappingFunction) {
		var resultRef = new AtomicReference<Z>();
		futureMap.compute(Nada.get(), (k, v) -> {
			if (remappingFunction != null)
				v = remappingFunction.apply(v, result -> resultRef.set(result));
			return v;
		});
		return resultRef.get();
	}

	protected abstract Map<X, Y> getResultMap(StreamEx<X> keys);

}
