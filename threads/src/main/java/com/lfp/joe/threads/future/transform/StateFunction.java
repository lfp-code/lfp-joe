package com.lfp.joe.threads.future.transform;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.Instances.Autowired;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.stream.Streams;

import one.util.streamex.StreamEx;

@SuppressWarnings("rawtypes")

@Autowired
public class StateFunction extends FutureTransform.Abs<Void, String, Future> {

	@Override
	protected Class<? extends Future> classType() {
		return Future.class;
	}

	@Override
	protected Optional<String> applyInternal(Future future, Void input) {
		var stateValueToField = streamFields(future).mapToEntry(Throws.functionUnchecked(v -> v.get(future)))
				.mapValues(v -> CoreReflections.tryCast(v, int.class).orElse(null));
		for (var ent : stateValueToField) {
			var field = ent.getKey();
			var stateValue = ent.getValue();
			var stateName = getStateName(field, stateValue);
			if (stateName != null)
				return Optional.of(stateName);
		}
		return Optional.empty();
	}

	private String getStateName(Field stateField, Integer stateValue) {
		if (stateField == null || stateValue == null || stateValue < 0)
			return null;
		var declaringClassType = stateField.getDeclaringClass();
		return Streams.of(CoreReflections.streamFields(declaringClassType, false))
				.filter(v -> CoreReflections.isAssignableFrom(v.getType(), int.class))
				.filter(v -> Modifier.isStatic(v.getModifiers()))
				.map(CoreReflections::setAccessible)
				.filter(v -> Objects.equals(stateValue, Throws.unchecked(() -> (Integer) v.get(null))))
				.map(Field::getName)
				.findFirst()
				.orElse(null);

	}

	private StreamEx<Field> streamFields(Future future) {
		var field = MemberCache.tryGetField(FieldRequest.of(future.getClass(), int.class, "state"), true).orElse(null);
		if (field == null || Modifier.isStatic(field.getModifiers()))
			return Streams.empty();
		return Streams.of(CoreReflections.setAccessible(field));
	}

	public static void main(String[] args) throws ClassNotFoundException, IOException {
		var func = new StateFunction();
		var fut = new FutureTask<Void>(() -> null);
		for (var cancel : List.of(Optional.<Boolean>empty(), Optional.of(true))) {
			if (cancel.isPresent())
				fut.cancel(cancel.get());
			System.out.println(func.apply(fut, null));
		}
	}

}
