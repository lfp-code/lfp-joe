package com.lfp.joe.threads.future.transform;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;
import com.lfp.joe.core.classpath.ImmutableMemberCache.MethodRequest;
import com.lfp.joe.core.classpath.Instances.Autowired;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.function.Throws;

@SuppressWarnings("rawtypes")

@Autowired(requiredClassNames = "com.google.common.util.concurrent.ListenableFuture")
public class InterruptedPredicateGuava extends FutureTransform.Abs<Void, Boolean, ListenableFuture>
		implements InterruptedPredicate {

	@Override
	protected Class<? extends ListenableFuture> classType() {
		return ListenableFuture.class;
	}

	@Override
	protected Optional<Boolean> applyInternal(ListenableFuture future, Void input) {
		var methodIter = MemberCache
				.tryGetMethod(MethodRequest.of(future.getClass(), boolean.class, "wasInterrupted"), true)
				.stream()
				.iterator();
		while (methodIter.hasNext()) {
			var method = methodIter.next();
			boolean interrupted = (boolean) Throws.unchecked(() -> method.invoke(future));
			return Optional.of(interrupted);
		}
		return Optional.empty();
	}

	public static void main(String[] args) {
		var pred = new InterruptedPredicateGuava();
		System.out.println(pred.apply(new CompletableFuture<>(), null));
		var sfuture = SettableFuture.create();
		System.out.println(pred.apply(sfuture, null));
		sfuture.cancel(true);
		System.out.println(pred.apply(sfuture, null));
	}

}
