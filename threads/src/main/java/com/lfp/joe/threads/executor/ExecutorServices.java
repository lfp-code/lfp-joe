package com.lfp.joe.threads.executor;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.ListenableFutureTask;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.threads.Threads;

@SuppressWarnings("unchecked")
public class ExecutorServices {

	public static <U, L extends ListenableFuture<U> & Runnable> L newTaskFor(Executor executor, Runnable runnable,
			U result) {
		if (result == null && ListenableFuture.class.isInstance(runnable))
			return (L) runnable;
		return (L) new ListenableFutureTask<U>(runnable, result, executor);
	}

	public static <U, L extends ListenableFuture<U> & Runnable> L newTaskFor(Executor executor, Callable<U> callable) {
		if (ListenableFuture.class.isInstance(callable) && Runnable.class.isInstance(callable))
			return (L) callable;
		return (L) new ListenableFutureTask<U>(callable, executor);
	}

	public static interface Invoker extends ExecutorService {

		@Override
		default <U> List<Future<U>> invokeAll(Collection<? extends Callable<U>> tasks) throws InterruptedException {
			return new InvokerExecutorService(this).invokeAll(tasks);
		}

		@Override
		default <U> List<Future<U>> invokeAll(Collection<? extends Callable<U>> tasks, long timeout, TimeUnit unit)
				throws InterruptedException {
			return new InvokerExecutorService(this).invokeAll(tasks, timeout, unit);
		}

		@Override
		default <U> U invokeAny(Collection<? extends Callable<U>> tasks)
				throws InterruptedException, ExecutionException {
			return new InvokerExecutorService(this).invokeAny(tasks);
		}

		@Override
		default <U> U invokeAny(Collection<? extends Callable<U>> tasks, long timeout, TimeUnit unit)
				throws InterruptedException, ExecutionException, TimeoutException {
			return new InvokerExecutorService(this).invokeAny(tasks, timeout, unit);
		}

	}

	public static interface Wrapper extends ExecutorService, ExecutorContainer {

		@Override
		default void execute(Runnable command) {
			getContainedExecutor().execute(command);
		}

		@Override
		default boolean isShutdown() {
			return CoreReflections.tryCast(getContainedExecutor(), ExecutorService.class)
					.map(ExecutorService::isShutdown)
					.orElse(false);
		}

		@Override
		default boolean isTerminated() {
			return CoreReflections.tryCast(getContainedExecutor(), ExecutorService.class)
					.map(ExecutorService::isTerminated)
					.orElse(false);
		}

		@Override
		default void shutdown() {
			CoreReflections.tryCast(getContainedExecutor(), ExecutorService.class)
					.orElseThrow(UnsupportedOperationException::new)
					.shutdown();

		}

		@Override
		default List<Runnable> shutdownNow() {
			return CoreReflections.tryCast(getContainedExecutor(), ExecutorService.class)
					.orElseThrow(UnsupportedOperationException::new)
					.shutdownNow();
		}

		@Override
		default boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
			var executorService = CoreReflections.tryCast(getContainedExecutor(), ExecutorService.class).orElse(null);
			if (executorService != null)
				return executorService.awaitTermination(timeout, unit);
			return false;
		}

		@Override
		default ListenableFuture<?> submit(Runnable task) {
			var executorService = CoreReflections.tryCast(getContainedExecutor(), ExecutorService.class).orElse(null);
			if (executorService != null)
				return Threads.Futures.asListenable(executorService.submit(task));
			var lfutureTask = newTaskFor(getContainedExecutor(), task, null);
			getContainedExecutor().execute(lfutureTask);
			return lfutureTask;

		}

		@Override
		default <T> ListenableFuture<T> submit(Runnable task, T result) {
			var executorService = CoreReflections.tryCast(getContainedExecutor(), ExecutorService.class).orElse(null);
			if (executorService != null)
				return Threads.Futures.asListenable(executorService.submit(task, result));
			var lfutureTask = newTaskFor(getContainedExecutor(), task, result);
			getContainedExecutor().execute(lfutureTask);
			return lfutureTask;

		}

		@Override
		default <T> ListenableFuture<T> submit(Callable<T> task) {
			var executorService = CoreReflections.tryCast(getContainedExecutor(), ExecutorService.class).orElse(null);
			if (executorService != null)
				return Threads.Futures.asListenable(executorService.submit(task));
			var lfutureTask = newTaskFor(getContainedExecutor(), task);
			getContainedExecutor().execute(lfutureTask);
			return lfutureTask;

		}

		@Override
		default <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException {
			return CoreReflections.tryCast(getContainedExecutor(), ExecutorService.class)
					.orElseGet(() -> new InvokerExecutorService(getContainedExecutor()))
					.invokeAll(tasks);
		}

		@Override
		default <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit)
				throws InterruptedException {
			return CoreReflections.tryCast(getContainedExecutor(), ExecutorService.class)
					.orElseGet(() -> new InvokerExecutorService(getContainedExecutor()))
					.invokeAll(tasks, timeout, unit);
		}

		@Override
		default <T> T invokeAny(Collection<? extends Callable<T>> tasks)
				throws InterruptedException, ExecutionException {
			return CoreReflections.tryCast(getContainedExecutor(), ExecutorService.class)
					.orElseGet(() -> new InvokerExecutorService(getContainedExecutor()))
					.invokeAny(tasks);
		}

		@Override
		default <T> T invokeAny(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit)
				throws InterruptedException, ExecutionException, TimeoutException {
			return CoreReflections.tryCast(getContainedExecutor(), ExecutorService.class)
					.orElseGet(() -> new InvokerExecutorService(getContainedExecutor()))
					.invokeAny(tasks, timeout, unit);
		}

	}

	public static interface ShutdownUnsupported extends ExecutorServices.Wrapper {

		@Override
		default void shutdown() {
			throw new UnsupportedOperationException();
		}

		@Override
		default List<Runnable> shutdownNow() {
			throw new UnsupportedOperationException();
		}
	}

	private static class InvokerExecutorService extends AbstractExecutorService implements ExecutorServices.Wrapper {

		private final Executor executor;

		public InvokerExecutorService(Executor executor) {
			super();
			this.executor = executor;
		}

		@Override
		public Executor getContainedExecutor() {
			return executor;
		}

		@Override
		public void execute(Runnable command) {
			executor.execute(command);
		}

		@Override
		public ListenableFuture<?> submit(Runnable task) {
			return ExecutorServices.Wrapper.super.submit(task);
		}

		@Override
		public <T> ListenableFuture<T> submit(Runnable task, T result) {
			return ExecutorServices.Wrapper.super.submit(task, result);
		}

		@Override
		public <T> ListenableFuture<T> submit(Callable<T> task) {
			return ExecutorServices.Wrapper.super.submit(task);
		}

	}

}
