package com.lfp.joe.threads.future;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

import org.threadly.concurrent.future.ListenableFuture;

import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.process.future.Completion;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;

public class FutureTracker extends Scrapable.Impl implements Function<Future<?>, Scrapable> {

	private final AtomicReference<Boolean> cancelAll = new AtomicReference<>();

	@Override
	public Scrapable apply(Future<?> future) {
		return apply(future, false);
	}

	public Scrapable apply(Future<?> future, boolean disableAutoUnregister) {
		var registration = onScrap(new FutureScrapTask<>(future, cancelAll));
		if (!disableAutoUnregister)
			Threads.Futures.callback(future, registration::scrap);
		return registration;
	}

	public boolean add(Future<?> future) {
		apply(future);
		return true;
	}

	public ListenableFuture<Nada> asFutureAll(boolean scrap) {
		var futures = getAllFutures(scrap, scrap);
		return Threads.Futures.makeCompleteFuture(futures);
	}

	public void blockAll(boolean scrap) throws InterruptedException {
		Completion.get(asFutureAll(scrap));
	}

	public List<Future<?>> cancelAll(boolean scrap, boolean mayInterruptIfRunning) {
		if (scrap)
			cancelAll.compareAndSet(null, mayInterruptIfRunning);
		var futures = removeAll(scrap);
		for (var future : futures)
			future.cancel(mayInterruptIfRunning);
		return futures;
	}

	public List<Future<?>> removeAll(boolean scrap) {
		return getAllFutures(scrap, true);
	}

	protected List<Future<?>> getAllFutures(boolean scrap, boolean remove) {
		return super.<List<Future<?>>>accessScrapTaskStore(stsOp -> {
			var iter = stsOp.map(Iterable::iterator).orElseGet(Collections::emptyIterator);
			return Streams.<Future<?>>produce(action -> {
				if (!iter.hasNext())
					return false;
				var scrapTask = iter.next();
				if (scrapTask instanceof FutureScrapTask) {
					if (remove)
						iter.remove();
					action.accept(((FutureScrapTask<?>) scrapTask).input());
				}
				return true;
			}).toImmutableList();
		}, false, scrap).orElseGet(Collections::emptyList);
	}

	protected static class FutureScrapTask<X> extends ScrapTask<Future<X>> {

		protected FutureScrapTask(Future<X> input, AtomicReference<Boolean> cancelAll) {
			super(input,
					v -> Optional.ofNullable(cancelAll.get())
							.or(Threads.Futures::cancellationDefault)
							.ifPresent(v::cancel));
		}

	}

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		var ft = new FutureTracker();
		var map = new ConcurrentHashMap<Future<?>, Integer>();
		List<Future<?>> removes = new ArrayList<>();
		for (int i = 0; i < 100; i++) {
			// Throws.unchecked(() -> Thread.sleep(50));
			var index = i;
			var future = Threads.Pools.centralPool().submit(() -> {
				System.out.println("starting:" + index + " - " + Thread.currentThread().getName());
				try {
					Thread.sleep(index < 50 ? 5_000 : 2_000);
					return new Date();
				} catch (Throwable t) {
					System.err.println(t);
					throw t;
				}
			});
			if (index > 40 && index < 50)
				removes.add(future);
			map.put(future, index);
			ft.apply(future);
		}
		if (true) {
			var future = ft.asFutureAll(false);
			future.listener(() -> {
				System.out.println("all complete unsrcrapped");
			});
		}
		if (false) {
			var future = ft.asFutureAll(true);
			future.listener(() -> {
				System.out.println("all complete scrap");
			});
		}

		System.out.println(removes.stream().filter(v -> ft.tryRemove(v).isPresent()).count());
		if (false) {
			var complete = ft.removeAll(true);
			complete.forEach(future -> {
				System.out.println(map.get(future) + " - " + Threads.Futures.getState(future));
			});
		}
		var future = Threads.Pools.computationPool().submit(() -> {
			Thread.sleep(30_000);
			return new Date();
		});
		{
			ft.apply(future);
			System.out.println(Threads.Futures.isInterrupted(future));
		}

		for (int i = 0; i < 2; i++) {
			System.out.println(ft);
			Thread.sleep(3_000);
			var futures = i == 0 ? ft.cancelAll(false, true) : ft.cancelAll(true, true);
			Map<String, Long> statemap = new HashMap<>();
			futures.stream().forEach(f -> {
				statemap.compute(Threads.Futures.getState(f).orElse(""), (k, v) -> v == null ? 1 : v + 1);
			});
			System.out.println(statemap);
		}
		System.out.println(ft);
		{
			ft.apply(future);
			System.out.println(Threads.Futures.isInterrupted(future));
		}
		System.out.println(ft);
		future.get();
	}

}
