package test;

import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.stream.Stream;

import com.lfp.joe.core.function.Throws.ThrowingRunnable;
import com.lfp.joe.threads.executor.ExecutorServiceAdapter;

import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;

public class ShutdownTest {

	public static void main(String[] args) throws InterruptedException {
		ExecutorServiceAdapter executorService;
		{
			var executor = Executors.newFixedThreadPool(1);
			executorService = new ExecutorServiceAdapter(executor::execute);
		}
		var commands = streamCommands("execute", 20, () -> Thread.sleep(1_000)).map(v -> new FutureTask<>(v, null))
				.toList();
		commands.forEach(v -> executorService.submitScheduled(v, 2_000));
		// executorService.shutdown();
		// System.out.println(executorService.awaitTermination(10, TimeUnit.SECONDS));
		Thread.sleep(5_000);
		var runnables = executorService.shutdownNow();
		print(false, "shutdownNow:%s", runnables.size());
		runnables.stream().peek(v -> printSummary(v)).forEach(Runnable::run);
	}

	private static StreamEx<Runnable> streamCommands(String name, int count, ThrowingRunnable<?> command) {
		return IntStreamEx.range(count).<Runnable>mapToObj(i -> {
			print(false, "%s requesting:%s", name, i);
			return () -> {
				print(false, "%s executing:%s", name, i);
				try {
					if (command != null)
						command.run();
					print(false, "%s success:%s", name, i);
				} catch (Throwable e) {
					print(true, "%s error:%s - %s", name, i, e);
				}
			};
		});
	}

	private static void printSummary(Runnable command) {
		print(false, "%s", command);
	}

	private static void print(boolean error, String template, Object... args) {
		var out = String.format(
				"%s - " + template, Stream
						.<Object>concat(Stream.of(Thread.currentThread().getName()),
								Stream.ofNullable(args).flatMap(Stream::of))
						.toArray());
		if (error)
			System.err.println(out);
		else
			System.out.println(out);
	}
}
