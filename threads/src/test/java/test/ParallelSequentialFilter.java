package test;

import com.lfp.joe.core.function.Throws;
import com.lfp.joe.stream.ForkJoinPoolContext;
import com.lfp.joe.stream.Streams;

import one.util.streamex.IntStreamEx;

public class ParallelSequentialFilter {

	public static void main(String[] args) throws InterruptedException {
		var stream = IntStreamEx.range(20).boxed().sequential().filter(v -> {
			if (v == 5) {
				throw new RuntimeException("test");
			}
			var currentThread = Thread.currentThread();
			System.out.println("filter - " + currentThread);
			Throws.unchecked(() -> Thread.sleep(1_000));
			return true;
		}).sequential();
		stream = stream.chain(Streams.parallelSequential(() -> ForkJoinPoolContext.create(4))).peek(v -> {
			var currentThread = Thread.currentThread();
			System.out.println("map - " + currentThread);
		});
		System.out.println(stream.count());
		System.err.println("done");
		Thread.currentThread().join();
	}
}
