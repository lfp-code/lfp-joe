package test;

import java.time.Duration;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.threads.Threads;

public class LoopTest {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) throws InterruptedException {
		Callable<Nada> task = () -> {
			System.out.println("the time is:" + new Date());
			return Nada.get();
		};
		var scrapable = Scrapable.create();
		scrapable.onScrap(() -> {
			System.out.println("scrappy");
		});
		var latch = new CountDownLatch(1);
		scrapable.onScrap(latch::countDown);
		loop(scrapable, task, Duration.ofSeconds(1));
		Threads.Pools.centralPool().schedule(scrapable::scrap, Duration.ofSeconds(10).toMillis());
		latch.await();
	}

	private static void loop(Scrapable scrapable, Callable<Nada> task, Duration interval) {
		var future = Threads.Pools.centralPool().submitScheduled(() -> {
			task.call();
			loop(scrapable, task, interval);
			return Nada.get();
		}, interval.toMillis());
		future.failureCallback(t -> {
			if (!future.isCancelled())
				logger.error("error", t);
		});
		Threads.Futures.onScrapCancel(scrapable, future, true);

	}
}
