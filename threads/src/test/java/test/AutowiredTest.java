package test;

import java.util.Date;
import java.util.stream.Collectors;

import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.threads.future.transform.FutureTransform;

public class AutowiredTest {

	public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			var startedAt = new Date();
			Instances.streamAutowired(FutureTransform.class).collect(Collectors.toList());
			System.out.println(String.format("%s:%s", i, System.currentTimeMillis() - startedAt.getTime()));
		}
	}
}
