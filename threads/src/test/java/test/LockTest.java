package test;

import java.time.Duration;
import java.util.concurrent.locks.Condition;

import com.lfp.joe.core.lock.AutoCloseableLock;
import com.lfp.joe.threads.Threads;

public class LockTest {

	private static final AutoCloseableLock LOCK = AutoCloseableLock.create();

	private static final Condition FINISH_THREAD = LOCK.newCondition();

	public static void main(String[] args) {
		new Thread(() -> {
			try (var l = LOCK.lockAndGet()) {
				while (true) {
					System.out.println("initial");
					Thread.sleep(1_000);
				}
			} catch (InterruptedException e) {
				throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e) : new RuntimeException(e);
			}
		}).start();
		for (int i = 0; i < 100; i++) {
			var index = i;
			new Thread(() -> {
				try (var l = LOCK.lockAndGet()) {
					System.out.println("started: " + index);
					System.out.println("awaiting: " + index);
					FINISH_THREAD.await();
					System.out.println("done: " + index);
				} catch (InterruptedException e) {
					throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e)
							: new RuntimeException(e);
				}
			}).start();
		}
		System.out.println("unlock before");
		try (var l = LOCK.lockAndGet()) {
			System.out.println("unlock start");
			Threads.sleepUnchecked(Duration.ofSeconds(3));
			System.out.println("unlocking");
			FINISH_THREAD.signalAll();
		}
	}

}
