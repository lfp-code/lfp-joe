package test;

import java.util.ServiceLoader;
import java.util.ServiceLoader.Provider;

import javax.annotation.processing.Processor;

public class ProviderTest {
	public static void main(String[] args) {
		ServiceLoader.load(Processor.class).stream().map(Provider::type).forEach(System.out::println);
	}
}
