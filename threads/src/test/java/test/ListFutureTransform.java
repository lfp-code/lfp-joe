package test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.threads.future.transform.FutureTransform;

import one.util.streamex.StreamEx;

public class ListFutureTransform {

	public static void main(String[] args) {
		var ctStream = findAllClassesUsingClassLoader(FutureTransform.class.getPackageName());
		ctStream = ctStream.filter(v -> !v.getName().contains("$"));
		ctStream = ctStream.filter(FutureTransform.class::isAssignableFrom);
		ctStream.forEach(v -> {
			var name = v.getName();
			var newName = name.replace("com.lfp.joe.threads.future", "com.lfp.joe.threads.future.transform");
			System.out.println(String.format("findReplace.put(\"%s\", \"%s\");", name, newName));
		});
	}

	public static StreamEx<Class> findAllClassesUsingClassLoader(String packageName) {
		InputStream stream = ClassLoader.getSystemClassLoader().getResourceAsStream(packageName.replaceAll("[.]", "/"));
		BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
		var ctStream = reader.lines().filter(line -> line.endsWith(".class")).map(line -> getClass(line, packageName));
		return StreamEx.of(ctStream).nonNull().distinct();
	}

	private static Class getClass(String className, String packageName) {
		var name = packageName + "." + className.substring(0, className.lastIndexOf('.'));
		return CoreReflections.tryForName(name).orElse(null);
	}
}
