package test;

import java.util.Date;
import java.util.concurrent.FutureTask;

import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.threads.Threads;

public class CallbackTest implements Runnable {

	private static final CallbackTest INSTANCE = Instances.get();

	@Override
	public void run() {
		var ft = new FutureTask<>(Date::new);
		Threads.Futures.callback(ft, () -> {
			System.out.println("done");
		});
		ft.run();
	}

	public static void main(String[] args) {
		CallbackTest.INSTANCE.run();
	}

}
