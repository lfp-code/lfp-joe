package test;

import java.time.Duration;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicLong;

import org.threadly.concurrent.RunnableCallableAdapter;

import com.lfp.joe.threads.ImmutableFutures.ScheduleWhileRequest;
import com.lfp.joe.threads.Threads;

public class ScheduleWhileTest3 {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		var counter = new AtomicLong();
		Runnable task = () -> {
			if (counter.incrementAndGet() > 10)
				throw new CancellationException("error");
			System.out.println("no error");
		};
		var future = Threads.Futures.scheduleWhile(ScheduleWhileRequest.builder().delay(Duration.ofSeconds(1))
				.task(RunnableCallableAdapter.adapt(task, null)).build());
		Thread.sleep(5_000);
		future.cancel(true);
		System.out.println(future.get());
	}
}
