package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

public class ListenablePactCodeGen {

	public static void main(String[] args) throws IOException {
		String code = "	@Override\n"
				+ "	public <R> ListenableFuture<R> map(Function<? super T, ? extends R> mapper) {\n"
				+ "		return delegate.map(mapper);\n" + "	}\n" + "\n" + "	@Override\n"
				+ "	public <R> ListenableFuture<R> map(Function<? super T, ? extends R> mapper, Executor executor) {\n"
				+ "		return delegate.map(mapper, executor);\n" + "	}\n" + "\n" + "	@Override\n"
				+ "	public <R> ListenableFuture<R> map(Function<? super T, ? extends R> mapper, Executor executor,\n"
				+ "			ListenerOptimizationStrategy optimizeExecution) {\n"
				+ "		return delegate.map(mapper, executor, optimizeExecution);\n" + "	}\n" + "\n" + "	@Override\n"
				+ "	public <R> ListenableFuture<R> throwMap(Function<? super T, ? extends R> mapper) {\n"
				+ "		return delegate.throwMap(mapper);\n" + "	}\n" + "\n" + "	@Override\n"
				+ "	public <R> ListenableFuture<R> throwMap(Function<? super T, ? extends R> mapper, Executor executor) {\n"
				+ "		return delegate.throwMap(mapper, executor);\n" + "	}\n" + "\n" + "	@Override\n"
				+ "	public <R> ListenableFuture<R> throwMap(Function<? super T, ? extends R> mapper, Executor executor,\n"
				+ "			ListenerOptimizationStrategy optimizeExecution) {\n"
				+ "		return delegate.throwMap(mapper, executor, optimizeExecution);\n" + "	}\n" + "\n"
				+ "	@Override\n" + "	public <R> ListenableFuture<R> flatMap(ListenableFuture<R> future) {\n"
				+ "		return delegate.flatMap(future);\n" + "	}\n" + "\n" + "	@Override\n"
				+ "	public <R> ListenableFuture<R> flatMap(Function<? super T, ListenableFuture<R>> mapper) {\n"
				+ "		return delegate.flatMap(mapper);\n" + "	}\n" + "\n" + "	@Override\n"
				+ "	public <R> ListenableFuture<R> flatMap(Function<? super T, ListenableFuture<R>> mapper, Executor executor) {\n"
				+ "		return delegate.flatMap(mapper, executor);\n" + "	}\n" + "\n" + "	@Override\n"
				+ "	public <R> ListenableFuture<R> flatMap(Function<? super T, ListenableFuture<R>> mapper, Executor executor,\n"
				+ "			ListenerOptimizationStrategy optimizeExecution) {\n"
				+ "		return delegate.flatMap(mapper, executor, optimizeExecution);\n" + "	}\n" + "\n"
				+ "	@Override\n"
				+ "	public <TT extends Throwable> ListenableFuture<T> mapFailure(Class<TT> throwableType,\n"
				+ "			Function<? super TT, ? extends T> mapper) {\n"
				+ "		return delegate.mapFailure(throwableType, mapper);\n" + "	}\n" + "\n" + "	@Override\n"
				+ "	public <TT extends Throwable> ListenableFuture<T> mapFailure(Class<TT> throwableType,\n"
				+ "			Function<? super TT, ? extends T> mapper, Executor executor) {\n"
				+ "		return delegate.mapFailure(throwableType, mapper, executor);\n" + "	}\n" + "\n" + "	@Override\n"
				+ "	public <TT extends Throwable> ListenableFuture<T> mapFailure(Class<TT> throwableType,\n"
				+ "			Function<? super TT, ? extends T> mapper, Executor executor,\n"
				+ "			ListenerOptimizationStrategy optimizeExecution) {\n"
				+ "		return delegate.mapFailure(throwableType, mapper, executor, optimizeExecution);\n" + "	}\n"
				+ "\n" + "	@Override\n"
				+ "	public <TT extends Throwable> ListenableFuture<T> flatMapFailure(Class<TT> throwableType,\n"
				+ "			Function<? super TT, ListenableFuture<T>> mapper) {\n"
				+ "		return delegate.flatMapFailure(throwableType, mapper);\n" + "	}\n" + "\n" + "	@Override\n"
				+ "	public <TT extends Throwable> ListenableFuture<T> flatMapFailure(Class<TT> throwableType,\n"
				+ "			Function<? super TT, ListenableFuture<T>> mapper, Executor executor) {\n"
				+ "		return delegate.flatMapFailure(throwableType, mapper, executor);\n" + "	}\n" + "\n"
				+ "	@Override\n"
				+ "	public <TT extends Throwable> ListenableFuture<T> flatMapFailure(Class<TT> throwableType,\n"
				+ "			Function<? super TT, ListenableFuture<T>> mapper, Executor executor,\n"
				+ "			ListenerOptimizationStrategy optimizeExecution) {\n"
				+ "		return delegate.flatMapFailure(throwableType, mapper, executor, optimizeExecution);\n"
				+ "	}	";
		try (var reader = new BufferedReader(new StringReader(code))) {
			var lineIter = reader.lines().iterator();
			while (lineIter.hasNext())
				System.out.println(modifyLine(lineIter.next()));
		}
	}

	private static String modifyLine(String line) {
		var delegateToken = "delegate.";
		var splitAt = line.indexOf(delegateToken);
		if (splitAt < 0)
			return line;
		var result = line.substring(0, splitAt);
		var methodInvoke = line.substring(splitAt + delegateToken.length());
		methodInvoke = methodInvoke.substring(0, methodInvoke.lastIndexOf(";"));
		String template = "getFuture(ListenableFuture.class).map(v -> v.%s).orElseGet(() -> ListenableFuture.super.%s);";
		result += String.format(template, methodInvoke, methodInvoke);
		return result;
	}
}
