package test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;

import com.lfp.joe.threads.Threads;
import com.lfp.joe.threads.executor.SubmitterSchedulerLFP;

public class ThreadTest {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) throws InterruptedException {
		SubmitterSchedulerLFP pool = Threads.Pools.centralPool();
		for (int i = 0; i < 100; i++) {
			pool = pool.limit(50);
		}
		// pool = SubmitterSchedulerLFP.create(Executors.newFixedThreadPool(4));
		// pool = pool.limit(2);
		List<ListenableFuture<?>> futures = new ArrayList<>();
		var threadIndexTracker = new AtomicLong();
		Set<String> threadNames = new HashSet<>();
		for (int i = 0; i < 100; i++) {
			var future = pool.submitScheduled(() -> {
				long threadIndex = threadIndexTracker.incrementAndGet();
				var threadName = Thread.currentThread().getName();
				boolean newThread = threadNames.add(threadName);
				logger.info("starting:{} threadIndex:{} newThread:{}", threadName, threadIndex, newThread);
				Threads.sleepUnchecked(Duration.ofSeconds(3));
				logger.info("finished:{} threadIndex:{} newThread:{}", threadName, threadIndex, newThread);
			}, Duration.ofSeconds(5).toMillis());
			futures.add(future);
		}
		FutureUtils.blockTillAllComplete(futures);
		System.exit(0);
	}
}
