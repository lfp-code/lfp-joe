package com.lfp.joe.okhttp.interceptor;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.utils.Utils;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;

public class CurrentChainInterceptor implements Interceptor, Function<OkHttpClient.Builder, OkHttpClient.Builder> {

	private static final MemoizedSupplier<CurrentChainInterceptor> INSTANCE_S = Utils.Functions
			.memoize(() -> new CurrentChainInterceptor(true, true));

	public static CurrentChainInterceptor getDefault() {
		return INSTANCE_S.get();
	}

	private static final Map<Thread, Chain> THREAD_MAP = new ConcurrentHashMap<>();

	public static Optional<Chain> tryGet() {
		var result = THREAD_MAP.get(Thread.currentThread());
		return Optional.ofNullable(result);
	}

	private final boolean networkInterceptors;
	private final boolean interceptors;

	public CurrentChainInterceptor(boolean interceptors, boolean networkInterceptors) {
		this.interceptors = interceptors;
		this.networkInterceptors = networkInterceptors;
	}

	@Override
	public OkHttpClient.Builder apply(OkHttpClient.Builder clientBuilder) {
		if (this.interceptors && !clientBuilder.interceptors().contains(this))
			clientBuilder = clientBuilder.addInterceptor(this);
		if (this.networkInterceptors && !clientBuilder.networkInterceptors().contains(this))
			clientBuilder = clientBuilder.addNetworkInterceptor(this);
		return clientBuilder;
	}

	@Override
	public Response intercept(Chain chain) throws IOException {
		var thread = Thread.currentThread();
		THREAD_MAP.put(thread, chain);
		try {
			return chain.proceed(chain.request());
		} finally {
			THREAD_MAP.remove(thread);
		}
	}

}
