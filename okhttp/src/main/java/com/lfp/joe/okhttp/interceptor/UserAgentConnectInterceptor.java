package com.lfp.joe.okhttp.interceptor;

import java.io.IOException;
import java.util.Objects;

import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.okhttp.reflection.OkHttpReflections;
import com.lfp.joe.utils.Utils;

import okhttp3.Interceptor;
import okhttp3.Response;
import okhttp3.internal.Version;
import okhttp3.internal.connection.ConnectInterceptor;

public class UserAgentConnectInterceptor implements Interceptor {

	@Override
	public Response intercept(Chain chain) throws IOException {
		var interceptors = OkHttpReflections.Calls.getInterceptors(chain).orElse(null);
		if (interceptors == null || interceptors.isEmpty())
			return chain.proceed(chain.request());
		for (int i = 0; i < interceptors.size(); i++) {
			var interceptor = interceptors.get(i);
			if (!(interceptor instanceof ConnectInterceptor))
				continue;
			interceptor = createWrapper((ConnectInterceptor) interceptor);
			interceptors.set(i, interceptor);
		}
		return chain.proceed(chain.request());
	}

	protected Interceptor createWrapper(ConnectInterceptor interceptor) {
		return new Wrapper(interceptor);
	}

	protected static class Wrapper implements Interceptor {

		private ConnectInterceptor connectInterceptor;

		public Wrapper(ConnectInterceptor connectInterceptor) {
			this.connectInterceptor = Objects.requireNonNull(connectInterceptor);
		}

		@Override
		public Response intercept(Chain chain) throws IOException {
			modifyRequest(chain);
			return this.connectInterceptor.intercept(chain);
		}

		private void modifyRequest(Chain chain) {
			var request = chain.request();
			var userAgent = request.header(Headers.USER_AGENT);
			if (userAgent == null)
				return;
			if(Version.userAgent().equals(userAgent))
				return;
			var call = chain.call();
			var originalRequest = OkHttpReflections.Calls.getOriginalRequest(call).orElse(null);
			if (originalRequest == null)
				return;
			if (Utils.Strings.equals(userAgent, originalRequest.header(Headers.USER_AGENT)))
				return;
			originalRequest = originalRequest.newBuilder().header(Headers.USER_AGENT, userAgent).build();
			OkHttpReflections.Calls.setOriginalRequest(call, originalRequest);
		}

	}

}
