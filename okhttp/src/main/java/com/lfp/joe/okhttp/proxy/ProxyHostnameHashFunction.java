package com.lfp.joe.okhttp.proxy;

import java.net.URI;
import java.util.ArrayList;
import java.util.function.Function;

import com.lfp.joe.net.dns.DNSs;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

/*
 * I believe that OKHttp pools connection with similar hosts/ports. this is an attempt to disrupt that
 */
public enum ProxyHostnameHashFunction implements Function<Proxy, Proxy> {
	INSTANCE;

	@Override
	public Proxy apply(Proxy proxy) {
		if (proxy == null)
			return proxy;
		var hostname = proxy.getHostname();
		if (Utils.Strings.isBlank(hostname))
			return proxy;
		if (URIs.isLocalhost(hostname))
			return proxy;
		String ipAddress = IPs.isValidIpAddress(hostname) ? hostname : null;
		if (URIs.isLocalhost(ipAddress))
			return proxy;
		if (Utils.Strings.isBlank(ipAddress))
			ipAddress = DNSs.parseIPAddressFromWildcardDNSHostname(hostname).orElse(null);
		if (Utils.Strings.isBlank(ipAddress))
			ipAddress = Utils.Functions.catching(() -> DNSs.getIPAdress(hostname), t -> null);
		if (Utils.Strings.isBlank(ipAddress))
			return proxy;
		var hashParts = new ArrayList<Object>();
		{
			hashParts.add(proxy.getType());
			hashParts.add(ipAddress);
			hashParts.add(proxy.getPort());
			hashParts.add(proxy.getUsername().orElse(""));
			hashParts.add(proxy.getPassword().orElse(""));
		}
		String hash;
		{
			var hashBytes = Utils.Crypto.hashMD5(hashParts.toArray());
			hash = hashBytes.encodeBase32();
			hash = Utils.Strings.stripEnd(hash, "=");
			hash = hash.toLowerCase();
			hash = Utils.Strings.substring(hash, 0, 10);
		}
		var newHostname = DNSs.toWildcardDNSHostname(ipAddress, hash);
		var newProxy = proxy.toBuilder().hostname(newHostname).build();
		return newProxy;
	}

	public static void main(String[] args) {
		var proxy = Proxy.builder(URI.create("http://s1.airproxy.io:31107")).build();
		proxy = ProxyHostnameHashFunction.INSTANCE.apply(proxy);
		System.out.println(Serials.Gsons.getPretty().toJson(proxy));
	}
}
