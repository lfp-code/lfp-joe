package com.lfp.joe.okhttp.call;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

import org.joda.beans.Bean;
import org.joda.beans.BeanDefinition;
import org.joda.beans.ImmutableBean;
import org.joda.beans.ImmutableDefaults;
import org.joda.beans.JodaBeanUtils;
import org.joda.beans.MetaProperty;
import org.joda.beans.Property;
import org.joda.beans.PropertyDefinition;
import org.joda.beans.impl.direct.DirectFieldsBeanBuilder;
import org.joda.beans.impl.direct.DirectMetaBean;
import org.joda.beans.impl.direct.DirectMetaProperty;
import org.joda.beans.impl.direct.DirectMetaPropertyMap;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.net.html.Jsoups;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.okhttp.reflection.OkHttpReflections;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.bytes.AutoCloseInputStream;
import com.lfp.joe.utils.bytes.CountingInputStream;
import com.lfp.joe.core.function.Throws.ThrowingFunction;

import at.favre.lib.bytes.Bytes;
import okhttp3.MediaType;
import okhttp3.Response;
import okhttp3.ResponseBody;

@BeanDefinition
public class ResponseContext<X> implements ImmutableBean, Scrapable {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String CONTENT_TYPE_JSON_TEMPLATE = "application/json; charset=%s";
	private static final String CONTENT_TYPE_HTML_TEMPLATE = "text/html; charset=%s";
	private static final String CONTENT_TYPE_TEXT_TEMPLATE = "text/plain; charset=%s";
	@PropertyDefinition(validate = "validate")
	private final Response response;
	@PropertyDefinition
	private final X parsedBody;
	@PropertyDefinition(get = "", validate = "notNull")
	private final Scrapable scrapableDelegate;

	private transient HeaderMap _headerMap;

	public HeaderMap getHeaderMap() {
		if (_headerMap == null)
			synchronized (this) {
				if (_headerMap == null)
					_headerMap = Ok.Calls.headerMap(response);
			}
		return _headerMap;
	}

	public boolean isHtml() {
		return Ok.Calls.contentTypeMatches(response, "text", "html", null);
	}

	public boolean isJson() {
		return Ok.Calls.contentTypeMatches(response, "application", "json", null);
	}

	public boolean isText() {
		return Ok.Calls.contentTypeMatches(response, "text", null, null);
	}

	public boolean isSuccessful() {
		return this.response.isSuccessful();
	}

	public ResponseContext<X> validateContentType(MediaType acceptableContentType) throws IOException {
		Ok.Calls.validateContentType(response, acceptableContentType);
		return this;
	}

	public ResponseContext<X> validateContentType(String type, String subType, Charset charset) throws IOException {
		Ok.Calls.validateContentType(response, type, subType, charset);
		return this;
	}

	public ResponseContext<X> validateStatusCode(int... acceptableStatusCodes) throws IOException {
		Ok.Calls.validateStatusCode(this.response, acceptableStatusCodes);
		return this;
	}

	public ResponseContext<X> validateSuccess() throws IOException {
		return validateStatusCode(2);
	}

	public ResponseContext<Document> parseHtml() throws IOException {
		return parseHtml(null);
	}

	public ResponseContext<Document> parseHtml(ParseOptions parseOptions) throws IOException {
		Charset charset = Ok.Calls.tryGetCharset(this.response.headers()).orElse(StandardCharsets.UTF_8);
		var contentType = Optional.of(Ok.Calls.contentTypeMatches(response, "text", "html", null))
				.filter(Boolean.TRUE::equals).flatMap(v -> Ok.Calls.getContentType(getResponse()))
				.orElse(MediaType.parse(String.format(CONTENT_TYPE_HTML_TEMPLATE, charset)));
		return parse(r -> {
			if (shouldReturnNull(this.response, parseOptions, "text", "html"))
				return null;
			Document doc;
			try {
				doc = Jsoups.parse(modifyInputStream(r, parseOptions), charset, this.response.request().url().uri());
			} catch (Exception e) {
				if (e instanceof IOException && Utils.Strings.containsIgnoreCase(e.getMessage(), "zero bytes"))
					doc = Jsoup.parse(Utils.Bits.from("<html/>").inputStream(), charset.name(),
							this.response.request().url().toString());
				else
					throw Utils.Exceptions.as(e, IOException.class);
			}
			return doc;
		}, v -> {
			Supplier<Bytes> bytesGetter = () -> {
				if (v == null)
					return Utils.Bits.empty();
				return Utils.Bits.from(v.outerHtml().getBytes(charset));
			};
			return Ok.Calls.createResponseBody(() -> contentType,
					Utils.Functions.memoize(() -> Long.valueOf(bytesGetter.get().length())),
					() -> bytesGetter.get().inputStream());
		});
	}

	public ResponseContext<String> parseText() throws IOException {
		return parseText(null);
	}

	public ResponseContext<String> parseText(ParseOptions parseOptions) throws IOException {
		Charset charset = Ok.Calls.tryGetCharset(this.response.headers()).orElse(StandardCharsets.UTF_8);
		var contentType = Optional.of(Ok.Calls.contentTypeMatches(response, "text", null, null))
				.filter(Boolean.TRUE::equals).flatMap(v -> Ok.Calls.getContentType(getResponse()))
				.orElse(MediaType.parse(String.format(CONTENT_TYPE_TEXT_TEMPLATE, charset)));
		AtomicLong byteCountRef = new AtomicLong(0);
		return parse(r -> {
			if (shouldReturnNull(this.response, parseOptions, "text", null))
				return null;
			var bytes = Utils.Bits.from(modifyInputStream(getResponse(), parseOptions));
			byteCountRef.set(bytes.length());
			return bytes.encodeCharset(charset);
		}, v -> {
			Supplier<Bytes> bytesGetter = () -> {
				if (v == null)
					return Utils.Bits.empty();
				return Utils.Bits.from(v.getBytes(charset));
			};
			return Ok.Calls.createResponseBody(() -> contentType, () -> byteCountRef.get(),
					() -> bytesGetter.get().inputStream());
		});
	}

	public ResponseContext<JsonElement> parseJson() throws IOException {
		return parseJson((ParseOptions) null);
	}

	public ResponseContext<JsonElement> parseJson(ParseOptions parseOptions) throws IOException {
		return parseJson(JsonElement.class, parseOptions);
	}

	public <Y> ResponseContext<Y> parseJson(Class<Y> classType) throws IOException {
		return parseJson(classType, null);
	}

	public <Y> ResponseContext<Y> parseJson(Class<Y> classType, ParseOptions parseOptions) throws IOException {
		return parseJson(classType == null ? null : TypeToken.of(classType), parseOptions);
	}

	public <Y> ResponseContext<Y> parseJson(TypeToken<Y> typeToken) throws IOException {
		return parseJson(typeToken, null);
	}

	public <Y> ResponseContext<Y> parseJson(TypeToken<Y> typeToken, ParseOptions parseOptions) throws IOException {
		return parseJson(null, typeToken, parseOptions);
	}

	public <Y> ResponseContext<Y> parseJson(Gson gson, TypeToken<Y> typeToken, ParseOptions parseOptions)
			throws IOException {
		Objects.requireNonNull(typeToken);
		var contentType = MediaType.parse(String.format(CONTENT_TYPE_JSON_TEMPLATE, StandardCharsets.UTF_8));
		return parse(r -> {
			if (shouldReturnNull(this.response, parseOptions, "application", "json"))
				return null;
			return Serials.Gsons.fromStream(gson != null ? gson : Serials.Gsons.get(),
					modifyInputStream(getResponse(), parseOptions), typeToken);
		}, v -> {
			Supplier<Bytes> bytesGetter = () -> {
				if (v == null)
					return Utils.Bits.empty();
				return Serials.Gsons.toBytes(gson != null ? gson : Serials.Gsons.get(), v);
			};
			return Ok.Calls.createResponseBody(() -> contentType,
					Utils.Functions.memoize(() -> Long.valueOf(bytesGetter.get().length())),
					() -> bytesGetter.get().inputStream());
		});
	}

	public <Y> ResponseContext<Y> parse(ThrowingFunction<Response, Y, IOException> responseParser) throws IOException {
		return parse(responseParser, null);
	}

	public <Y> ResponseContext<Y> parse(ThrowingFunction<Response, Y, IOException> responseParser,
			ThrowingFunction<Y, ResponseBody, IOException> responseBodyGenerator) throws IOException {
		Objects.requireNonNull(responseParser);
		try {
			Y parsedResult = responseParser.apply(this.getResponse());
			AtomicReference<Response> responseLoggingReference = new AtomicReference<>(this.response);
			ResponseBody newResponseBody = null;
			if (responseBodyGenerator != null)
				newResponseBody = responseBodyGenerator.apply(parsedResult);
			if (newResponseBody == null)
				newResponseBody = Ok.Calls.createResponseBody(Ok.Calls.getContentType(getResponse()).orElse(null), 0,
						() -> {
							throw Ok.Calls.createException(responseLoggingReference.get(),
									"response parsing did not produce a response body");
						});
			var newResponse = Ok.Calls.withResponseBody(this.response, newResponseBody);
			responseLoggingReference.set(newResponse);
			var builder = ResponseContext.builder(newResponse, parsedResult);
			return builder.build();
		} finally {
			this.scrap();
		}

	}

	public ResponseContext<Optional<File>> flushToDisk() throws IOException {
		return flushToDisk(null);
	}

	public ResponseContext<Optional<File>> flushToDisk(
			ThrowingFunction<InputStream, InputStream, IOException> inputStreamModifier) throws IOException {
		try {
			Scrapable scrapable = Scrapable.create();
			File file;
			ResponseBody newResponseBody;
			if (this.response.body().contentLength() == 0) {
				file = null;
				newResponseBody = null;
			} else {
				file = Utils.Files.tempFile(THIS_CLASS, "v1", Utils.Crypto.getSecureRandomString() + ".response");
				try (CountingInputStream cis = modifyInputStream(this.response, inputStreamModifier);
						OutputStream os = new FileOutputStream(file)) {
					Utils.Bits.copy(cis, os);
				} catch (Exception e) {
					file.delete();
					throw Utils.Exceptions.as(e, IOException.class);
				}
				var contentType = Ok.Calls.getContentType(this.response).orElse(null);
				newResponseBody = Ok.Calls.createResponseBody(contentType, file.length(), () -> {
					scrapable.validateNotScrapped();
					AutoCloseInputStream is = new AutoCloseInputStream(new FileInputStream(file));
					var listener = scrapable.onScrap(() -> {
						try {
							is.close();
						} catch (Exception e) {
							// suppress
						}
					});
					is.addListener(nil -> {
						listener.scrap();
					});
					return is;
				});
			}
			var newResponse = Ok.Calls.withResponseBody(this.response, newResponseBody);
			var builder = ResponseContext.builder(newResponse, Optional.ofNullable(file));
			builder.scrapableDelegate(scrapable);
			ResponseContext<Optional<File>> responseContext = builder.build();
			if (file != null)
				responseContext.onScrap(() -> {
					file.delete();
				});
			return responseContext;
		} finally {
			this.scrap();
		}
	}

	public ResponseContext<Void> flushToNull() throws IOException {
		return flushToNull(null);
	}

	public ResponseContext<Void> flushToNull(
			ThrowingFunction<InputStream, InputStream, IOException> inputStreamModifier) throws IOException {
		try {
			ResponseBody newResponseBody;
			if (this.response.body().contentLength() == 0) {
				newResponseBody = null;
			} else {
				try (InputStream is = modifyInputStream(this.response, inputStreamModifier);) {
					Utils.Bits.copy(is, OutputStream.nullOutputStream());
				}
				var contentType = Ok.Calls.getContentType(this.response).orElse(null);
				newResponseBody = Ok.Calls.createResponseBody(contentType);
			}
			var newResponse = Ok.Calls.withResponseBody(this.response, newResponseBody);
			return ResponseContext.builder(newResponse).build();
		} finally {
			this.scrap();
		}
	}

	public ResponseContext<Bytes> flushToMemory() throws IOException {
		return flushToMemory(null);
	}

	public ResponseContext<Bytes> flushToMemory(
			ThrowingFunction<InputStream, InputStream, IOException> inputStreamModifier) throws IOException {
		try {
			Bytes bytes;
			ResponseBody newResponseBody;
			if (this.response.body().contentLength() == 0) {
				bytes = Bytes.empty();
				newResponseBody = null;
			} else {
				try (InputStream is = modifyInputStream(this.response, inputStreamModifier);) {
					bytes = Utils.Bits.from(is);
				}
				var contentType = Ok.Calls.getContentType(this.response).orElse(null);
				newResponseBody = Ok.Calls.createResponseBody(contentType, bytes.length(), () -> bytes.inputStream());
			}
			var newResponse = Ok.Calls.withResponseBody(this.response, newResponseBody);
			var builder = ResponseContext.builder(newResponse, bytes);
			return builder.build();
		} finally {
			this.scrap();
		}
	}

	@Override
	public boolean isScrapped() {
		return scrapableDelegate.isScrapped();
	}

	@Override
	public Scrapable onScrap(Runnable task) {
		return scrapableDelegate.onScrap(task);
	}

	@Override
	public boolean scrap() {
		Ok.Calls.close(response);
		return scrapableDelegate.scrap();
	}

	public static ResponseContext.Builder<Void> builder(Response response) {
		return builder(response, null);
	}

	public static ResponseContext<Void> build(Response response) {
		return builder(response).build();
	}

	@SuppressWarnings("unchecked")
	public static <X> ResponseContext.Builder<X> builder(Response response, X parsedBody) {
		return (Builder<X>) ResponseContext.builder().response(response).parsedBody(parsedBody);
	}

	public static <X> ResponseContext<X> build(Response response, X parsedBody) {
		return builder(response, parsedBody).build();
	}

	private static void validate(Object value, String propertyName) {
		ResponseContext.Meta<?> meta = ResponseContext.meta();
		if (meta.response().name().equals(propertyName)) {
			Response response = Utils.Types.tryCast(value, Response.class).orElse(null);
			Objects.requireNonNull(response);
			if (response.body() != null)
				return;
			var contentType = Ok.Calls.getContentType(response).orElse(null);
			var emptyResponseBody = Ok.Calls.createResponseBody(contentType);
			OkHttpReflections.Responses.setResponseBody(response, emptyResponseBody);
			return;
		}
	}

	@SuppressWarnings("rawtypes")
	@ImmutableDefaults
	private static void applyDefaults(Builder builder) {
		builder.scrapableDelegate(Scrapable.create());
	}

	private static CountingInputStream modifyInputStream(Response response, ParseOptions parseOptions)
			throws IOException {
		return modifyInputStream(response, parseOptions == null ? null : parseOptions.getInputStreamModifier());
	}

	private static CountingInputStream modifyInputStream(Response response,
			ThrowingFunction<InputStream, InputStream, IOException> inputStreamModifier) throws IOException {
		InputStream inputStream = response == null || response.body() == null ? null : response.body().byteStream();
		if (inputStream == null)
			inputStream = Utils.Bits.emptyInputStream();
		if (inputStreamModifier != null)
			inputStream = inputStreamModifier.apply(inputStream);
		if (inputStream == null)
			inputStream = Utils.Bits.emptyInputStream();
		if (inputStream instanceof CountingInputStream)
			return (CountingInputStream) inputStream;
		return new CountingInputStream(inputStream);
	}

	private static boolean shouldReturnNull(Response response, ParseOptions parseOptions, String type, String subType)
			throws IOException {
		if (parseOptions == null)
			return false;
		var contentTypeCheck = parseOptions.getContentTypeCheck();
		if (ParseOptions.ContentTypeCheck.STRICT.equals(contentTypeCheck))
			Ok.Calls.validateContentType(response, type, subType, null);
		else if (ParseOptions.ContentTypeCheck.REQUIRED.equals(contentTypeCheck)) {
			if (!Ok.Calls.contentTypeMatches(response, type, subType, null))
				return true;
		}
		return false;
	}

	public static void main(String[] args) {
		JodaBeans.updateCode();
	}

	// ------------------------- AUTOGENERATED START -------------------------
	/// CLOVER:OFF
	/**
	 * The meta-bean for {@code ResponseContext}.
	 * 
	 * @return the meta-bean, not null
	 */
	@SuppressWarnings("rawtypes")
	public static ResponseContext.Meta meta() {
		return ResponseContext.Meta.INSTANCE;
	}

	/**
	 * The meta-bean for {@code ResponseContext}.
	 * 
	 * @param <R> the bean's generic type
	 * @param cls the bean's generic type
	 * @return the meta-bean, not null
	 */
	@SuppressWarnings("unchecked")
	public static <R> ResponseContext.Meta<R> metaResponseContext(Class<R> cls) {
		return ResponseContext.Meta.INSTANCE;
	}

	static {
		JodaBeanUtils.registerMetaBean(ResponseContext.Meta.INSTANCE);
	}

	/**
	 * Returns a builder used to create an instance of the bean.
	 * 
	 * @param <X> the type
	 * @return the builder, not null
	 */
	public static <X> ResponseContext.Builder<X> builder() {
		return new ResponseContext.Builder<X>();
	}

	/**
	 * Restricted constructor.
	 * 
	 * @param builder the builder to copy from, not null
	 */
	protected ResponseContext(ResponseContext.Builder<X> builder) {
		validate(builder.response, "response");
		JodaBeanUtils.notNull(builder.scrapableDelegate, "scrapableDelegate");
		this.response = builder.response;
		this.parsedBody = builder.parsedBody;
		this.scrapableDelegate = builder.scrapableDelegate;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ResponseContext.Meta<X> metaBean() {
		return ResponseContext.Meta.INSTANCE;
	}

	@Override
	public <R> Property<R> property(String propertyName) {
		return metaBean().<R>metaProperty(propertyName).createProperty(this);
	}

	@Override
	public Set<String> propertyNames() {
		return metaBean().metaPropertyMap().keySet();
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the response.
	 * 
	 * @return the value of the property
	 */
	public Response getResponse() {
		return response;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the parsedBody.
	 * 
	 * @return the value of the property
	 */
	public X getParsedBody() {
		return parsedBody;
	}

	// -----------------------------------------------------------------------
	/**
	 * Returns a builder that allows this bean to be mutated.
	 * 
	 * @return the mutable builder, not null
	 */
	public Builder<X> toBuilder() {
		return new Builder<X>(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj != null && obj.getClass() == this.getClass()) {
			ResponseContext<?> other = (ResponseContext<?>) obj;
			return JodaBeanUtils.equal(response, other.response) && JodaBeanUtils.equal(parsedBody, other.parsedBody)
					&& JodaBeanUtils.equal(scrapableDelegate, other.scrapableDelegate);
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = getClass().hashCode();
		hash = hash * 31 + JodaBeanUtils.hashCode(response);
		hash = hash * 31 + JodaBeanUtils.hashCode(parsedBody);
		hash = hash * 31 + JodaBeanUtils.hashCode(scrapableDelegate);
		return hash;
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder(128);
		buf.append("ResponseContext{");
		int len = buf.length();
		toString(buf);
		if (buf.length() > len) {
			buf.setLength(buf.length() - 2);
		}
		buf.append('}');
		return buf.toString();
	}

	protected void toString(StringBuilder buf) {
		buf.append("response").append('=').append(JodaBeanUtils.toString(response)).append(',').append(' ');
		buf.append("parsedBody").append('=').append(JodaBeanUtils.toString(parsedBody)).append(',').append(' ');
		buf.append("scrapableDelegate").append('=').append(JodaBeanUtils.toString(scrapableDelegate)).append(',')
				.append(' ');
	}

	// -----------------------------------------------------------------------
	/**
	 * The meta-bean for {@code ResponseContext}.
	 * 
	 * @param <X> the type
	 */
	public static class Meta<X> extends DirectMetaBean {
		/**
		 * The singleton instance of the meta-bean.
		 */
		@SuppressWarnings("rawtypes")
		static final Meta INSTANCE = new Meta();

		/**
		 * The meta-property for the {@code response} property.
		 */
		private final MetaProperty<Response> response = DirectMetaProperty.ofImmutable(this, "response",
				ResponseContext.class, Response.class);
		/**
		 * The meta-property for the {@code parsedBody} property.
		 */
		@SuppressWarnings({ "unchecked", "rawtypes" })
		private final MetaProperty<X> parsedBody = (DirectMetaProperty) DirectMetaProperty.ofImmutable(this,
				"parsedBody", ResponseContext.class, Object.class);
		/**
		 * The meta-property for the {@code scrapableDelegate} property.
		 */
		private final MetaProperty<Scrapable> scrapableDelegate = DirectMetaProperty.ofImmutable(this,
				"scrapableDelegate", ResponseContext.class, Scrapable.class);
		/**
		 * The meta-properties.
		 */
		private final Map<String, MetaProperty<?>> metaPropertyMap$ = new DirectMetaPropertyMap(this, null, "response",
				"parsedBody", "scrapableDelegate");

		/**
		 * Restricted constructor.
		 */
		protected Meta() {
		}

		@Override
		protected MetaProperty<?> metaPropertyGet(String propertyName) {
			switch (propertyName.hashCode()) {
			case -340323263: // response
				return response;
			case -977503917: // parsedBody
				return parsedBody;
			case 374403920: // scrapableDelegate
				return scrapableDelegate;
			}
			return super.metaPropertyGet(propertyName);
		}

		@Override
		public ResponseContext.Builder<X> builder() {
			return new ResponseContext.Builder<X>();
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		@Override
		public Class<? extends ResponseContext<X>> beanType() {
			return (Class) ResponseContext.class;
		}

		@Override
		public Map<String, MetaProperty<?>> metaPropertyMap() {
			return metaPropertyMap$;
		}

		// -----------------------------------------------------------------------
		/**
		 * The meta-property for the {@code response} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Response> response() {
			return response;
		}

		/**
		 * The meta-property for the {@code parsedBody} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<X> parsedBody() {
			return parsedBody;
		}

		/**
		 * The meta-property for the {@code scrapableDelegate} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Scrapable> scrapableDelegate() {
			return scrapableDelegate;
		}

		// -----------------------------------------------------------------------
		@Override
		protected Object propertyGet(Bean bean, String propertyName, boolean quiet) {
			switch (propertyName.hashCode()) {
			case -340323263: // response
				return ((ResponseContext<?>) bean).getResponse();
			case -977503917: // parsedBody
				return ((ResponseContext<?>) bean).getParsedBody();
			case 374403920: // scrapableDelegate
				return ((ResponseContext<?>) bean).scrapableDelegate;
			}
			return super.propertyGet(bean, propertyName, quiet);
		}

		@Override
		protected void propertySet(Bean bean, String propertyName, Object newValue, boolean quiet) {
			metaProperty(propertyName);
			if (quiet) {
				return;
			}
			throw new UnsupportedOperationException("Property cannot be written: " + propertyName);
		}

	}

	// -----------------------------------------------------------------------
	/**
	 * The bean-builder for {@code ResponseContext}.
	 * 
	 * @param <X> the type
	 */
	public static class Builder<X> extends DirectFieldsBeanBuilder<ResponseContext<X>> {

		private Response response;
		private X parsedBody;
		private Scrapable scrapableDelegate;

		/**
		 * Restricted constructor.
		 */
		protected Builder() {
			applyDefaults(this);
		}

		/**
		 * Restricted copy constructor.
		 * 
		 * @param beanToCopy the bean to copy from, not null
		 */
		protected Builder(ResponseContext<X> beanToCopy) {
			this.response = beanToCopy.getResponse();
			this.parsedBody = beanToCopy.getParsedBody();
			this.scrapableDelegate = beanToCopy.scrapableDelegate;
		}

		// -----------------------------------------------------------------------
		@Override
		public Object get(String propertyName) {
			switch (propertyName.hashCode()) {
			case -340323263: // response
				return response;
			case -977503917: // parsedBody
				return parsedBody;
			case 374403920: // scrapableDelegate
				return scrapableDelegate;
			default:
				throw new NoSuchElementException("Unknown property: " + propertyName);
			}
		}

		@SuppressWarnings("unchecked")
		@Override
		public Builder<X> set(String propertyName, Object newValue) {
			switch (propertyName.hashCode()) {
			case -340323263: // response
				this.response = (Response) newValue;
				break;
			case -977503917: // parsedBody
				this.parsedBody = (X) newValue;
				break;
			case 374403920: // scrapableDelegate
				this.scrapableDelegate = (Scrapable) newValue;
				break;
			default:
				throw new NoSuchElementException("Unknown property: " + propertyName);
			}
			return this;
		}

		@Override
		public Builder<X> set(MetaProperty<?> property, Object value) {
			super.set(property, value);
			return this;
		}

		/**
		 * @deprecated Use Joda-Convert in application code
		 */
		@Override
		@Deprecated
		public Builder<X> setString(String propertyName, String value) {
			setString(meta().metaProperty(propertyName), value);
			return this;
		}

		/**
		 * @deprecated Use Joda-Convert in application code
		 */
		@Override
		@Deprecated
		public Builder<X> setString(MetaProperty<?> property, String value) {
			super.setString(property, value);
			return this;
		}

		/**
		 * @deprecated Loop in application code
		 */
		@Override
		@Deprecated
		public Builder<X> setAll(Map<String, ? extends Object> propertyValueMap) {
			super.setAll(propertyValueMap);
			return this;
		}

		@Override
		public ResponseContext<X> build() {
			return new ResponseContext<X>(this);
		}

		// -----------------------------------------------------------------------
		/**
		 * Sets the response.
		 * 
		 * @param response the new value
		 * @return this, for chaining, not null
		 */
		public Builder<X> response(Response response) {
			validate(response, "response");
			this.response = response;
			return this;
		}

		/**
		 * Sets the parsedBody.
		 * 
		 * @param parsedBody the new value
		 * @return this, for chaining, not null
		 */
		public Builder<X> parsedBody(X parsedBody) {
			this.parsedBody = parsedBody;
			return this;
		}

		/**
		 * Sets the scrapableDelegate.
		 * 
		 * @param scrapableDelegate the new value, not null
		 * @return this, for chaining, not null
		 */
		public Builder<X> scrapableDelegate(Scrapable scrapableDelegate) {
			JodaBeanUtils.notNull(scrapableDelegate, "scrapableDelegate");
			this.scrapableDelegate = scrapableDelegate;
			return this;
		}

		// -----------------------------------------------------------------------
		@Override
		public String toString() {
			StringBuilder buf = new StringBuilder(128);
			buf.append("ResponseContext.Builder{");
			int len = buf.length();
			toString(buf);
			if (buf.length() > len) {
				buf.setLength(buf.length() - 2);
			}
			buf.append('}');
			return buf.toString();
		}

		protected void toString(StringBuilder buf) {
			buf.append("response").append('=').append(JodaBeanUtils.toString(response)).append(',').append(' ');
			buf.append("parsedBody").append('=').append(JodaBeanUtils.toString(parsedBody)).append(',').append(' ');
			buf.append("scrapableDelegate").append('=').append(JodaBeanUtils.toString(scrapableDelegate)).append(',')
					.append(' ');
		}

	}

	/// CLOVER:ON
	// -------------------------- AUTOGENERATED END --------------------------
}
