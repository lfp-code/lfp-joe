package com.lfp.joe.okhttp.interceptor;

import java.io.IOException;
import java.util.Optional;
import java.util.function.Function;

import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Hashable;

import at.favre.lib.bytes.Bytes;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public enum RequestUUIDInterceptor implements Interceptor, Function<OkHttpClient.Builder, OkHttpClient.Builder> {
	INSTANCE;

	public static class RequestUUIDTag implements Hashable {

		private Bytes _hash;

		@Override
		public Bytes hash() {
			if (_hash == null)
				synchronized (this) {
					if (_hash == null)
						_hash = Utils.Crypto.hashMD5(Utils.Crypto.getSecureRandomString());
				}
			return _hash;
		}

	}

	public static Optional<RequestUUIDTag> tryGetUUID(Request request) {
		var result = request.tag(RequestUUIDTag.class);
		return Optional.ofNullable(result);
	}

	@Override
	public OkHttpClient.Builder apply(OkHttpClient.Builder clientBuilder) {
		if (!clientBuilder.interceptors().contains(this))
			clientBuilder = clientBuilder.addInterceptor(this);
		if (!clientBuilder.networkInterceptors().contains(this))
			clientBuilder = clientBuilder.addNetworkInterceptor(this);
		return clientBuilder;
	}

	@Override
	public Response intercept(Chain chain) throws IOException {
		var request = chain.request().newBuilder().tag(RequestUUIDTag.class, new RequestUUIDTag()).build();
		return chain.proceed(request);
	}

}
