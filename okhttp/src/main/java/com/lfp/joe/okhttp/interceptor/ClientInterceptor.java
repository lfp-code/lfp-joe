package com.lfp.joe.okhttp.interceptor;

import java.io.IOException;

import com.lfp.joe.okhttp.Ok;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;

public interface ClientInterceptor extends Interceptor {

	default Response intercept(Chain chain) throws IOException {
		OkHttpClient client = Ok.Clients.getClient(chain.call()).get();
		return intercept(client, chain);
	}

	Response intercept(OkHttpClient client, Chain chain) throws IOException;

}
