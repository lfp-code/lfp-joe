package com.lfp.joe.okhttp.interceptor;

import java.io.IOException;
import java.net.URI;
import java.util.Objects;

import org.jsoup.nodes.Element;

import com.lfp.joe.net.html.Jsoups;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.utils.Utils;

import de.xn__ho_hia.storage_unit.StorageUnit;
import de.xn__ho_hia.storage_unit.StorageUnits;
import okhttp3.Interceptor;
import okhttp3.Response;

public class MetaRefreshRedirectInterceptor implements Interceptor {
	private final StorageUnit<?> maxPeek;

	public MetaRefreshRedirectInterceptor() {
		this(StorageUnits.kilobyte(128));
	}

	public MetaRefreshRedirectInterceptor(StorageUnit<?> maxPeek) {
		this.maxPeek = Objects.requireNonNull(maxPeek);
	}

	@Override
	public Response intercept(Chain chain) throws IOException {
		var request = chain.request();
		var response = chain.proceed(request);
		if (!Ok.Calls.contentTypeMatches(response, null, "html", null))
			return response;
		URI nextURI = Utils.Functions.catching(() -> {
			return Ok.Calls.peekDocument(response, this.maxPeek.longValue(), doc -> {
				try (var metaStream = Jsoups.select(doc, "meta");) {
					for (var el : metaStream) {
						URI uri = parseURI(el);
						if (uri != null)
							return uri;
					}
				}
				return null;
			});
		}, t -> null);
		if (nextURI == null)
			return response;
		return response;
	}

	private static URI parseURI(Element meta) {
		if (meta == null)
			return null;
		var equivFound = Utils.Lots.stream(meta.attributes()).filter(v -> {
			if (!"http-equiv".equalsIgnoreCase(v.getKey()))
				return false;
			if (!"refresh".equalsIgnoreCase(v.getValue()))
				return false;
			return true;
		}).findFirst().isPresent();
		if (!equivFound)
			return null;
		for (var attr : meta.attributes()) {
			if (!"content".equalsIgnoreCase(attr.getKey()))
				continue;
			var str = attr.getValue();
			var post = Utils.Strings.substringAfter(str, "url=");
			var uri = URIs.parse(post).orElse(null);
			if (uri == null)
				uri = URIs.parse(str).orElse(null);
			if (uri != null)
				return uri;
		}
		return null;
	}

}
