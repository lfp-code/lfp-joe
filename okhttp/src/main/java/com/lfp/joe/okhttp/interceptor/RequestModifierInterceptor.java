package com.lfp.joe.okhttp.interceptor;

import java.io.IOException;
import java.util.Objects;
import java.util.function.Function;

import com.lfp.joe.okhttp.reflection.OkHttpReflections;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public interface RequestModifierInterceptor extends Interceptor, Function<OkHttpClient.Builder, OkHttpClient.Builder> {

	@Override
	default OkHttpClient.Builder apply(OkHttpClient.Builder clientBuilder) {
		return apply(clientBuilder, false);
	}

	default OkHttpClient.Builder apply(OkHttpClient.Builder clientBuilder, boolean disableNetworkInterceptor) {
		Objects.requireNonNull(clientBuilder);
		if (!disableNetworkInterceptor)
			clientBuilder.addNetworkInterceptor(this);
		clientBuilder.addInterceptor(this);
		return clientBuilder;
	}

	@Override
	default Response intercept(Chain chain) throws IOException {
		var call = chain.call();
		Request request;
		if (call != null) {
			var callRequest = call.request();
			var newCallRequest = modifyRequest(true, callRequest);
			if (newCallRequest != callRequest)
				OkHttpReflections.Calls.setRequest(call, newCallRequest);
			request = chain.request();
		} else {
			request = modifyRequest(false, chain.request());
		}
		return chain.proceed(request);
	}

	Request modifyRequest(boolean isNetworkRequest, Request request) throws IOException;

}
