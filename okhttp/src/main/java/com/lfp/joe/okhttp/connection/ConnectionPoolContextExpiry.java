package com.lfp.joe.okhttp.connection;

import java.time.Duration;
import java.util.Objects;
import java.util.function.Supplier;

import com.lfp.joe.cache.caffeine.Caffeines;
import com.lfp.joe.cache.caffeine.TTLExpiry;

import org.apache.commons.lang3.Validate;
import org.checkerframework.checker.nullness.qual.NonNull;

public class ConnectionPoolContextExpiry<X> implements TTLExpiry<X, ConnectionPoolContext> {

	private final Duration ttlDuration;

	public ConnectionPoolContextExpiry(Duration ttlDuration) {
		Objects.requireNonNull(ttlDuration);
		Validate.isTrue(ttlDuration.toMillis() > 0);
		this.ttlDuration = ttlDuration;
	}

	@Override
	public Duration timeToLive(boolean write, @NonNull X key, @NonNull ConnectionPoolContext value,
			@NonNull Supplier<Duration> noChangeSupplier) {
		var connectionCount = value.getConnectionPool().connectionCount();
		if (connectionCount > 0)
			return Caffeines.MAX_TTL;
		var current = noChangeSupplier.get();
		if (current == null)
			return this.ttlDuration;
		if (current.isNegative() || current.isZero())
			return Duration.ZERO;
		if (this.ttlDuration.minus(current).isNegative())
			return this.ttlDuration;
		return current;
	}

}
