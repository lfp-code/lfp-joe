package com.lfp.joe.okhttp.interceptor;

import static java.net.HttpURLConnection.HTTP_MOVED_PERM;
import static java.net.HttpURLConnection.HTTP_MOVED_TEMP;
import static java.net.HttpURLConnection.HTTP_MULT_CHOICE;
import static java.net.HttpURLConnection.HTTP_SEE_OTHER;
import static okhttp3.internal.Util.sameConnection;

import java.io.IOException;
import java.util.Iterator;

import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.utils.Utils;

import org.apache.commons.lang3.Validate;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.internal.http.HttpMethod;
import one.util.streamex.IntStreamEx;

public class FollowRedirectsInterceptor implements Interceptor {

	private static final int DEFAULT_MAX_REDIRECTS = 21;
	private int maxRedirects;
	private boolean disableFollowSslRedirects;

	public FollowRedirectsInterceptor() {
		this(false);
	}

	public FollowRedirectsInterceptor(boolean disableFollowSslRedirects) {
		this(disableFollowSslRedirects, DEFAULT_MAX_REDIRECTS);
	}

	public FollowRedirectsInterceptor(boolean disableFollowSslRedirects, int maxRedirects) {
		this.disableFollowSslRedirects = disableFollowSslRedirects;
		Validate.isTrue(maxRedirects == -1 || maxRedirects > 0, "invalid max redirects:%s", maxRedirects);
		this.maxRedirects = maxRedirects;
	}

	@Override
	public Response intercept(Chain chain) throws IOException {
		Response response = chain.proceed(chain.request());
		Iterator<Integer> attemptIter;
		if (this.maxRedirects == -1)
			attemptIter = Utils.Lots.newIterator(il -> -1);
		else
			attemptIter = IntStreamEx.range(this.maxRedirects).iterator();
		int attemptIndex = -1;
		while (attemptIter.hasNext()) {
			attemptIndex = attemptIter.next();
			var nextRequest = getNextRequest(chain, response);
			if (nextRequest == null)
				return response;
			response.close();
			if (!attemptIter.hasNext())
				break;
			response = chain.proceed(nextRequest);
		}
		throw Ok.Calls.createException(response, String.format("too many redirects:%s", attemptIndex + 1));
	}

	private Request getNextRequest(Chain chain, Response response) {
		if (!isRedirectStatusCode(response))
			return null;
		String location = response.header(Headers.LOCATION);
		if (location == null)
			return null;
		HttpUrl url = response.request().url().resolve(location);

		// Don't follow redirects to unsupported protocols.
		if (url == null)
			return null;

		// If configured, don't follow redirects between SSL and non-SSL.
		boolean sameScheme = url.scheme().equals(response.request().url().scheme());
		if (!sameScheme && disableFollowSslRedirects)
			return null;
		final String method = response.request().method();
		// Most redirects don't include a request body.
		Request.Builder requestBuilder = response.request().newBuilder();
		if (HttpMethod.permitsRequestBody(method)) {
			final boolean maintainBody = HttpMethod.redirectsWithBody(method);
			if (HttpMethod.redirectsToGet(method)) {
				requestBuilder.method("GET", null);
			} else {
				RequestBody requestBody = maintainBody ? response.request().body() : null;
				requestBuilder.method(method, requestBody);
			}
			if (!maintainBody) {
				requestBuilder.removeHeader("Transfer-Encoding");
				requestBuilder.removeHeader("Content-Length");
				requestBuilder.removeHeader("Content-Type");
			}
		}

		// When redirecting across hosts, drop all authentication headers. This
		// is potentially annoying to the application layer since they have no
		// way to retain them.
		if (!sameConnection(response.request().url(), url)) {
			requestBuilder.removeHeader("Authorization");
		}

		return requestBuilder.url(url).build();
	}

	private static boolean isRedirectStatusCode(Response response) {
		var sc = response.code();
		switch (sc) {
		case HTTP_MULT_CHOICE:
		case HTTP_MOVED_PERM:
		case HTTP_MOVED_TEMP:
		case HTTP_SEE_OTHER:
			return true;
		}
		return false;
	}

}
