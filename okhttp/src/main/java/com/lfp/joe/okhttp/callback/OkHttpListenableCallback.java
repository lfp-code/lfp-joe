package com.lfp.joe.okhttp.callback;

import java.io.IOException;
import java.util.Objects;

import com.lfp.joe.core.function.Throws.ThrowingBiFunction;
import com.lfp.joe.okhttp.Ok;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class OkHttpListenableCallback<X>
		extends ListenableCallback.Abs<X, Call, Response, IOException, IOException> implements Callback {

	public static <X> OkHttpListenableCallback<X> create(Call call,
			ThrowingBiFunction<Call, Response, X, IOException> responseMapper) {
		Objects.requireNonNull(call);
		var callback = new OkHttpListenableCallback<X>(responseMapper);
		callback.listener(() -> call.cancel());
		call.enqueue(callback);
		return callback;
	}

	public OkHttpListenableCallback(ThrowingBiFunction<Call, Response, X, IOException> mapper) {
		super(mapper);
	}

	@Override
	protected Throwable uncaughtExceptionResponse(Call call, Response response, Throwable t) {
		var e = Ok.Calls.createException(response, UNCAUGHT_ERROR_MESSAGE);
		e.initCause(t);
		return e;
	}

	@Override
	protected IOException alreadyCompleteExceptionResponse(Call call, Response response) {
		return Ok.Calls.createException(response, ALREADY_INVOKED_ERROR_MESSAGE);
	}

	@Override
	protected RuntimeException alreadyCompleteExceptionFailure(Call call, IOException e) {
		return new IllegalStateException(ALREADY_INVOKED_ERROR_MESSAGE + " - request:" + call.request(), e);
	}

	@Override
	protected void closeResponse(Call call, Response response) {
		Ok.Calls.close(response);
	}

}
