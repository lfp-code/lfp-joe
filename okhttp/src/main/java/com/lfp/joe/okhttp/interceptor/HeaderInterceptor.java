package com.lfp.joe.okhttp.interceptor;

import java.io.IOException;

import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public abstract class HeaderInterceptor implements Interceptor {

	public static HeaderInterceptor create(ThrowingSupplier<HeaderMap, IOException> headerSupplier,
			String... removeHeaderNames) {
		return create(headerSupplier, false, false, removeHeaderNames);
	}

	public static HeaderInterceptor create(ThrowingSupplier<HeaderMap, IOException> headerSupplier,
			boolean modifyIfPresent, boolean addHeaders, String... removeHeaderNames) {
		return create(headerSupplier == null ? null : nil -> headerSupplier.get(), modifyIfPresent, addHeaders,
				removeHeaderNames);
	}

	public static HeaderInterceptor create(ThrowingFunction<Chain, HeaderMap, IOException> headerFunction,
			String... removeHeaderNames) {
		return create(headerFunction, false, false, removeHeaderNames);
	}

	public static HeaderInterceptor create(ThrowingFunction<Chain, HeaderMap, IOException> headerFunction,
			boolean modifyIfPresent, boolean addHeaders, String... removeHeaderNames) {
		return new HeaderInterceptor(modifyIfPresent, addHeaders, removeHeaderNames) {

			@Override
			protected HeaderMap getHeaderMap(Chain chain) throws IOException {
				if (headerFunction == null)
					return HeaderMap.of();
				return headerFunction.apply(chain);
			}

		};
	}

	private boolean modifyIfPresent;
	private boolean addHeaders;
	private String[] removeHeaderNames;

	public HeaderInterceptor(boolean modifyIfPresent, boolean addHeaders, String... removeHeaderNames) {
		this.modifyIfPresent = modifyIfPresent;
		this.addHeaders = addHeaders;
		this.removeHeaderNames = removeHeaderNames;
	}

	@Override
	public Response intercept(Chain chain) throws IOException {
		return chain.proceed(modifyRequest(chain));
	}

	protected Request modifyRequest(Chain chain) throws IOException {
		var request = chain.request();
		HeaderMap headerMap = getHeaderMap(chain);
		if (headerMap == null || headerMap.isEmpty())
			return request;
		var requestHeaders = Ok.Calls.headerMap(request);
		boolean mod = false;
		for (var name : headerMap.stream().keys()) {
			if (!modifyIfPresent && requestHeaders.contains(name))
				continue;
			if (!addHeaders)
				requestHeaders = requestHeaders.withRemoved(name);
			requestHeaders = requestHeaders.with(name, headerMap.allValues(name).toArray(String[]::new));
			mod = true;
		}
		if (!mod)
			return request;
		var rb = request.newBuilder();
		for (var name : requestHeaders.stream().keys()) {
			rb = rb.removeHeader(name);
			for (var value : requestHeaders.allValues(name))
				rb = rb.addHeader(name, value);
		}
		return rb.build();
	}

	protected Request removeHeaderNames(Request request) {
		if (this.removeHeaderNames == null || this.removeHeaderNames.length == 0)
			return request;
		var rb = request.newBuilder();
		for (var removeHeaderName : removeHeaderNames)
			if (removeHeaderName != null)
				rb.removeHeader(removeHeaderName);
		return rb.build();
	}

	protected abstract HeaderMap getHeaderMap(Chain chain) throws IOException;
}
