package com.lfp.joe.okhttp.callback;

import java.util.Date;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.SettableListenableFuture;

import com.google.common.base.Objects;
import com.lfp.joe.core.function.Throws.ThrowingBiFunction;

public interface ListenableCallback<X, C, R, RT extends Throwable, FT extends Throwable>
		extends ListenableFuture<X> {

	void onResponse(C call, R response) throws RT;

	void onFailure(C call, FT e);

	Optional<Date> getCompletedAt();

	public static abstract class Abs<X, C, R, RT extends Throwable, FT extends Throwable>
			extends SettableListenableFuture<X> implements ListenableCallback<X, C, R, RT, FT> {
		protected static final String ALREADY_INVOKED_ERROR_MESSAGE = "callback already invoked";
		protected static final String UNCAUGHT_ERROR_MESSAGE = "uncaught error";

		private AtomicReference<Long> completeAt = new AtomicReference<Long>();
		private ThrowingBiFunction<C, R, X, RT> mapper;

		public Abs(ThrowingBiFunction<C, R, X, RT> mapper) {
			super(false);
			this.mapper = mapper;
		}

		@Override
		public void onResponse(C call, R response) throws RT {
			if (completeAt.compareAndSet(null, System.currentTimeMillis()))
				onResponseOnce(call, response);
			else {
				closeResponse(call, response);
				throw alreadyCompleteExceptionResponse(call, response);
			}
		}

		protected void onResponseOnce(C call, R response) {
			X mapped = null;
			try {
				if (this.isCancelled())
					return;
				mapped = this.mapper == null ? null : this.mapper.apply(call, response);
				this.setResult(mapped);
			} catch (Throwable t) {
				this.setFailure(uncaughtExceptionResponse(call, response, t));
			} finally {
				// DONT close if mapper returned identity
				this.mapper = null;
				if (response != null && !Objects.equal(response, mapped))
					closeResponse(call, response);
			}

		}

		@Override
		public void onFailure(C call, FT e) {
			try {
				if (completeAt.compareAndSet(null, System.currentTimeMillis()) && this.setFailure(e))
					return;
				throw alreadyCompleteExceptionFailure(call, e);
			} finally {
				if (call != null)
					closeFailure(call);
			}
		}

		@Override
		public Optional<Date> getCompletedAt() {
			return Optional.ofNullable(completeAt.get()).map(Date::new);
		}

		protected abstract void closeResponse(C call, R response);

		protected void closeFailure(C call) {
			// no operation by default
		}

		protected abstract Throwable uncaughtExceptionResponse(C call, R response, Throwable t);

		protected abstract RT alreadyCompleteExceptionResponse(C call, R response);

		protected abstract RuntimeException alreadyCompleteExceptionFailure(C call, FT e);
	}

}
