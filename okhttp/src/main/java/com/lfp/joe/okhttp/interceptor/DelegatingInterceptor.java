package com.lfp.joe.okhttp.interceptor;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.okhttp.call.ResponseContext;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Response;

public abstract class DelegatingInterceptor implements Interceptor {
	private long maxRetries;

	public DelegatingInterceptor() {
		this(21);
	}

	public DelegatingInterceptor(long maxRetries) {
		Validate.isTrue(maxRetries > 0 || maxRetries == -1, "invalid maxRetries:%s", maxRetries);
		this.maxRetries = maxRetries;
	}

	@Override
	public Response intercept(Chain chain) throws IOException {
		Result result = null;
		long attemptIndex = -1;
		while (getResponseContext(result, true).isEmpty()) {
			attemptIndex++;
			if (attemptIndex >= maxRetries)
				break;
			Set<AutoCloseable> autoCloseTracking = Utils.Lots.newConcurrentHashSet();
			Consumer<AutoCloseable> autoCloseConsumer = ac -> {
				if (ac != null)
					autoCloseTracking.add(ac);
			};
			try {
				var interceptResult = intercept(chain, attemptIndex, autoCloseConsumer);
				if (interceptResult != null) {
					autoCloseConsumer.accept(interceptResult.responseContext);
					result = interceptResult;
				}
			} finally {
				var rtx = getResponseContext(result, true).orElse(null);
				for (var autoClose : autoCloseTracking) {
					if (Objects.equals(autoClose, rtx))
						continue;
					try {
						autoClose.close();
					} catch (Exception e) {
						throw Utils.Exceptions.as(e, IOException.class);
					}
				}
			}
		}
		var rtx = getResponseContext(result, true).orElse(null);
		if (rtx != null)
			return rtx.getResponse();
		var failureResponse = getResponseContext(result, false).map(ResponseContext::getResponse).orElse(null);
		if (failureResponse == null) {
			int code = StatusCodes.INTERNAL_SERVER_ERROR;
			Bytes body = Utils.Bits.from(StatusCodes.getReason(code));
			var responseBody = Ok.Calls.createResponseBody(MediaType.parse("text/plain"), body);
			failureResponse = Ok.Calls.createResponse(chain.request(), code, responseBody);
		}
		throw Ok.Calls.createException(failureResponse,
				"error during delegate intercept. attemptIndex:" + attemptIndex);

	}

	protected abstract Result intercept(Chain chain, long attemptIndex, Consumer<AutoCloseable> closeTracking)
			throws IOException;

	protected static Result complete(ResponseContext<?> responseContext) {
		return new Result(true, responseContext);
	}

	protected static Result proceed(ResponseContext<?> responseContext) {
		return new Result(false, responseContext);
	}

	private static Optional<ResponseContext<?>> getResponseContext(Result result, boolean requireCompleted) {
		if (result == null)
			return Optional.empty();
		if (requireCompleted && !result.complete)
			return Optional.empty();
		return Optional.ofNullable(result.responseContext);
	}

	public static class Result {
		public final boolean complete;
		public final ResponseContext<?> responseContext;

		public Result(boolean complete, ResponseContext<?> responseContext) {
			super();
			this.complete = complete;
			if (this.complete)
				Objects.requireNonNull(responseContext);
			this.responseContext = responseContext;
		}
	}
}
