package com.lfp.joe.okhttp.cookie;

import java.io.File;

import com.lfp.joe.net.cookie.CookieStoreLFP;
import com.lfp.joe.net.cookie.Cookies;

public class CookieJars {

	public static CookieStoreJar create() {
		return create(Cookies.createCookieStore());
	}

	public static CookieStoreJar create(File file) {
		return create(Cookies.createCookieStore(file));
	}

	public static CookieStoreJar create(String cookieFolderUUID) {
		return create(Cookies.createCookieStore(cookieFolderUUID));
	}

	public static CookieStoreJar create(CookieStoreLFP cookieStore) {
		return new CookieStoreJar.Impl(cookieStore);
	}
}
