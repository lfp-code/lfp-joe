package com.lfp.joe.okhttp.interceptor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;

import org.apache.commons.lang3.Validate;
import org.brotli.dec.BrotliInputStream;

import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingFunction;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.BufferedSource;
import okio.Okio;
import okio.Source;
import one.util.streamex.StreamEx;

public class ContentEncodingInterceptor implements Interceptor {

	public static ContentEncodingInterceptor createDefault() {
		ContentEncodingInterceptor contentEncodingInterceptor = new ContentEncodingInterceptor();
		contentEncodingInterceptor.add("gzip", true, source -> Okio.source(new GZIPInputStream(source.inputStream())));
		contentEncodingInterceptor.add("deflate", true,
				source -> Okio.source(new InflaterInputStream(source.inputStream())));
		contentEncodingInterceptor.add("br", true, source -> Okio.source(new BrotliInputStream(source.inputStream())));
		return contentEncodingInterceptor;
	}

	private final Map<Entry<String, Boolean>, ThrowingFunction<BufferedSource, Source, IOException>> mappings = new ConcurrentHashMap<>();
	private final List<String> order = new ArrayList<>();

	public boolean add(String contentEncoding, boolean ignoreCase,
			ThrowingFunction<BufferedSource, Source, IOException> function) {
		if (ignoreCase)
			contentEncoding = Utils.Strings.lowerCase(contentEncoding);
		Validate.isTrue(Utils.Strings.isNotBlank(contentEncoding), "contentEncoding is required");
		Objects.requireNonNull(function);
		AtomicBoolean mod = new AtomicBoolean();
		mappings.computeIfAbsent(Utils.Lots.entry(contentEncoding, ignoreCase), k -> {
			order.add(k.getKey());
			mod.set(true);
			return function;
		});
		return mod.get();
	}

	@Override
	public Response intercept(Chain chain) throws IOException {
		if (mappings.isEmpty())
			return chain.proceed(chain.request());
		String value = Utils.Lots.stream(mappings.keySet()).sortedBy(k -> {
			int index = order.indexOf(k.getKey());
			return index >= 0 ? index : Integer.MAX_VALUE;
		}).map(Entry::getKey).map(String::toLowerCase).distinct().joining(", ");
		Request request = chain.request().newBuilder().header(Headers.ACCEPT_ENCODING, value).build();
		Response response = chain.proceed(request);
		if (response.headers() == null)
			return response;
		ThrowingFunction<BufferedSource, Source, IOException> inputStreamModifier = getInputStreamModifier(response);
		if (inputStreamModifier == null)
			return response;
		ResponseBody body = response.body();
		Source source = Objects.requireNonNull(inputStreamModifier.apply(body.source()));
		BufferedSource decompressedSource = Utils.Types.tryCast(source, BufferedSource.class)
				.orElse(Okio.buffer(source));
		return response.newBuilder().removeHeader(com.lfp.joe.net.http.headers.Headers.CONTENT_ENCODING)
				.body(ResponseBody.create(body.contentType(), -1l, decompressedSource)).build();
	}

	private ThrowingFunction<BufferedSource, Source, IOException> getInputStreamModifier(Response response) {
		var headerStream = com.lfp.joe.net.http.headers.Headers.stream(Ok.Calls.streamHeaders(response.headers()));
		StreamEx<String> ceStream = com.lfp.joe.net.http.headers.Headers
				.stream(headerStream, com.lfp.joe.net.http.headers.Headers.CONTENT_ENCODING).values();
		ThrowingFunction<BufferedSource, Source, IOException> isModifier = ceStream.map(v -> {
			for (Entry<String, Boolean> key : mappings.keySet()) {
				boolean ignoreCase = Boolean.TRUE.equals(key.getValue());
				boolean match;
				if (ignoreCase)
					match = Utils.Strings.equalsIgnoreCase(key.getKey(), v);
				else
					match = Utils.Strings.equals(key.getKey(), v);
				if (match)
					return mappings.get(key);
			}
			return null;
		}).nonNull().findFirst().orElse(null);
		return isModifier;
	}

}
