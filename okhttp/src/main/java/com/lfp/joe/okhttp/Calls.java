package com.lfp.joe.okhttp;

import com.lfp.joe.core.lot.Lot; import com.lfp.joe.core.lot.AbstractLot; import java.io.IOException;
import java.io.InputStream;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.http.HttpTimeoutException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeoutException;
import java.util.function.Supplier;
import java.util.stream.Stream;

import com.lfp.joe.net.cookie.Cookie;
import com.lfp.joe.net.cookie.Cookies;
import com.lfp.joe.net.html.FormData;
import com.lfp.joe.net.html.Jsoups;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.media.MediaTypes;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.okhttp.call.ResponseContext;
import com.lfp.joe.okhttp.reflection.OkHttpReflections;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.bytes.LazyInputStream;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.core.lot.AbstractLot;

import javax.annotation.Nullable;

import org.apache.commons.lang3.Validate;
import org.jsoup.Connection.Method;
import org.jsoup.nodes.Document;

import at.favre.lib.bytes.Bytes;
import de.xn__ho_hia.storage_unit.StorageUnit;
import de.xn__ho_hia.storage_unit.StorageUnits;
import io.mikael.urlbuilder.UrlBuilder;
import okhttp3.FormBody;
import okhttp3.Interceptor.Chain;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public class Calls {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static ResponseContext<Void> execute(OkHttpClient client, String url) throws IOException {
		return execute(client, url == null ? null : new Request.Builder().url(url).build());
	}

	public static ResponseContext<Void> execute(OkHttpClient client, URI uri) throws IOException {
		return execute(client, uri == null ? null : new Request.Builder().url(uri.toString()).build());
	}

	public static ResponseContext<Void> execute(OkHttpClient client, Request request) throws IOException {
		return execute(client, request, response -> {
			ResponseContext.Builder<Void> responseMapper = ResponseContext.builder(response);
			return responseMapper.build();
		}, false);
	}

	public static <X> X execute(OkHttpClient client, Request request,
			ThrowingFunction<Response, X, IOException> function) throws IOException {
		return execute(client, request, function, true);
	}

	private static <X> X execute(OkHttpClient client, Request request,
			ThrowingFunction<Response, X, IOException> function, boolean closeResponse) throws IOException {
		Objects.requireNonNull(client);
		Objects.requireNonNull(request);
		Objects.requireNonNull(function);
		Response response = null;
		try {
			response = client.newCall(request).execute();
			return function.apply(response);
		} catch (Throwable t) {
			Ok.Calls.close(response);
			throw Utils.Exceptions.as(t, IOException.class);
		} finally {
			if (closeResponse)
				Ok.Calls.close(response);
		}
	}

	public static Optional<Bytes> tryReadBody(Response response, StorageUnit<?> limit) {
		Objects.requireNonNull(response);
		return tryReadBody(response.body(), limit);
	}

	public static Optional<Bytes> tryReadBody(ResponseBody responseBody, StorageUnit<?> limit) {
		if (responseBody == null)
			return Optional.empty();
		try (InputStream is = Utils.Bits.limit(responseBody.byteStream(), limit)) {
			var bytes = Utils.Bits.from(is);
			if (!bytes.isEmpty())
				return Optional.of(bytes);
		} catch (Exception e) {
			// suppress
		}
		return Optional.empty();
	}

	public static Optional<Charset> tryGetCharset(okhttp3.Headers headers) {
		if (headers == null)
			return Optional.empty();
		var mm = headers.toMultimap();
		return Headers.tryGetCharset(Utils.Lots.streamMultimap(mm));
	}

	public static Optional<String> tryReadBodyAsString(Response response, StorageUnit<?> limit) {
		Objects.requireNonNull(response);
		return tryReadBodyAsString(response.body(), tryGetCharset(response.headers()).orElse(null), limit);
	}

	public static Optional<String> tryReadBodyAsString(ResponseBody responseBody, Charset charset,
			StorageUnit<?> limit) {
		var op = tryReadBody(responseBody, limit);
		if (op.isEmpty())
			return Optional.empty();
		if (charset == null)
			charset = StandardCharsets.UTF_8;
		try {
			return Optional.of(new String(op.get().array(), charset));
		} catch (Exception e) {
			// suppress
		}
		return Optional.empty();
	}

	public static Optional<Proxy> getProxy(Response response) {
		if (response == null)
			return Optional.empty();
		var proxyOp = streamRequests(response).map(v -> v.tag(Proxy.class)).nonNull().findFirst();
		if (proxyOp.isPresent())
			return proxyOp;
		proxyOp = Ok.Clients.getClient(response).flatMap(Ok.Clients::getProxy);
		return proxyOp;
	}

	@SuppressWarnings("resource")
	public static Map<String, Object> getSummary(Response response, boolean includeHeaders,
			StorageUnit<?> bodyReadAmount) {
		Objects.requireNonNull(response);
		Map<String, Object> parts = new LinkedHashMap<>();
		Request request = response.request();
		parts.put("resCode", response.code());
		parts.put("resMessage", response.message());
		Object lengthMsg;
		{
			long length = getContentLength(response);
			if (length < 0)
				lengthMsg = null;
			else if (length >= StorageUnits.megabyte(1).longValue())
				lengthMsg = StorageUnits.bytes(length).asMegabyte().toString().replaceAll(" ", "").toLowerCase();
			else
				lengthMsg = length;
		}
		parts.put("resLength", lengthMsg);
		if (includeHeaders)
			parts.put("resHeaders", Ok.Calls.streamHeaders(response.headers()).grouping());
		else
			parts.put("resContentType", Ok.Calls.getContentType(response).orElse(null));
		if (bodyReadAmount != null) {
			var resBody = tryReadBodyAsString(response, bodyReadAmount).orElse(null);
			parts.put("resBody", resBody);
		}
		parts.put("reqURL", request.url().toString());
		if (includeHeaders)
			parts.put("reqHeaders", Ok.Calls.streamHeaders(response.headers()).grouping());
		int redirectCount = 0;
		Response initialResponse = response.priorResponse();
		while (initialResponse != null) {
			redirectCount++;
			Response pr = initialResponse.priorResponse();
			if (pr == null)
				break;
			initialResponse = pr;
		}
		if (redirectCount > 0) {
			parts.put("reqURLInitial", initialResponse.request().url().toString());
			parts.put("reqRedirectCount", redirectCount);
		}
		return parts;
	}

	public static IOException createException(Response response, @Nullable String errorMessage) {
		return createException(response, errorMessage, false, null);
	}

	public static IOException createException(Response response, @Nullable String errorMessage, boolean includeHeaders,
			StorageUnit<?> bodyReadAmount) {
		var summary = new LinkedHashMap<String, Object>();
		if (Utils.Strings.isNotBlank(errorMessage))
			summary.put("", errorMessage);
		summary.putAll(getSummary(response, includeHeaders, bodyReadAmount));
		Proxy proxy = getProxy(response).orElse(null);
		if (proxy != null)
			summary.put("proxy", proxy);
		String msg = Utils.Lots.stream(summary).nonNullValues().mapValues(Objects::toString)
				.filterValues(Utils.Strings::isNotBlank).map(ent -> {
					if (Utils.Strings.isBlank(ent.getKey()))
						return ent.getValue();
					return String.format("%s:%s", ent.getKey(), ent.getValue());
				}).joining(", ");
		return new IOException(msg);
	}

	public static void validateStatusCode(Response response, int... acceptableStatusCodes) throws IOException {
		validate(response, nil -> {
			var responseCode = response == null ? null : response.code();
			return StatusCodes.matches(responseCode, acceptableStatusCodes);
		}, () -> {
			var acceptableStatusCodesSummary = Utils.Lots.stream(acceptableStatusCodes).distinct().mapToObj(v -> v)
					.toList();
			String msg = String.format("invalid status code. acceptableStatusCodes:%s", acceptableStatusCodesSummary);
			return msg;
		});
	}

	public static void validateContentType(Response response, MediaType acceptableContentType) throws IOException {
		validateContentType(response, nil -> contentTypeMatches(response, acceptableContentType));
	}

	public static void validateContentType(Response response, String type, String subtype, Charset charset)
			throws IOException {
		validateContentType(response, nil -> contentTypeMatches(response, type, subtype, charset));
	}

	public static void validateContentType(Response response,
			ThrowingFunction<Optional<MediaType>, Boolean, IOException> validator) throws IOException {
		validate(response, nil -> {
			var contentTypeOp = getContentType(response);
			return validator.apply(contentTypeOp);
		}, () -> "invalid content type");
	}

	public static void validate(Response response, ThrowingFunction<Response, Boolean, IOException> validator,
			Supplier<String> errorMessageSupplier) throws IOException {
		Objects.requireNonNull(response);
		if (validator == null)
			return;
		if (Boolean.TRUE.equals(validator.apply(response)))
			return;
		try {
			throw createException(response, errorMessageSupplier == null ? null : errorMessageSupplier.get());
		} finally {
			Ok.Calls.close(response);
		}
	}

	public static boolean contentTypeMatches(Response response, MediaType acceptableContentType) {
		return contentTypeMatches(response, acceptableContentType == null ? null : acceptableContentType.type(),
				acceptableContentType == null ? null : acceptableContentType.subtype(),
				acceptableContentType == null ? null : acceptableContentType.charset());
	}

	public static boolean contentTypeMatches(Response response, String type, String subtype, Charset charset) {
		return MediaTypes.matches(getContentType(response).flatMap(MediaTypes::tryParse).orElse(null), type, subtype,
				charset);
	}

	public static RequestBody createRequestBody(MediaType contentType) {
		return createRequestBody(contentType, (Bytes) null);
	}

	public static RequestBody createRequestBody(MediaType contentType, Bytes bytes) {
		return createRequestBody(contentType, bytes == null ? null : () -> bytes.inputStream());
	}

	public static RequestBody createRequestBody(MediaType contentType,
			ThrowingSupplier<InputStream, IOException> inputStreamSupplier) {
		return new RequestBody() {

			@Override
			public MediaType contentType() {
				return contentType;
			}

			@Override
			public void writeTo(BufferedSink sink) throws IOException {
				try (var is = inputStreamSupplier == null ? Utils.Bits.emptyInputStream() : inputStreamSupplier.get()) {
					var source = Okio.source(is);
					sink.writeAll(source);
				}
			}
		};

	}

	public static ResponseBody createResponseBody(MediaType contentType) {
		return createResponseBody(contentType, 0, () -> Utils.Bits.emptyInputStream());
	}

	public static ResponseBody createResponseBody(MediaType contentType, Bytes bytes) {
		return createResponseBody(contentType, bytes == null ? 0 : bytes.length(),
				bytes == null ? null : bytes::inputStream);
	}

	public static ResponseBody createResponseBody(MediaType contentType, long contentLength,
			ThrowingSupplier<InputStream, IOException> inputStreamSupplier) {
		return createResponseBody(contentType == null ? null : () -> contentType, () -> contentLength,
				inputStreamSupplier);
	}

	public static <X> ResponseBody createResponseBody(Supplier<MediaType> contentTypeSupplier,
			Supplier<Long> contentLengthSupplier, ThrowingSupplier<InputStream, IOException> sourceSupplier) {
		return new ResponseBody() {

			@Override
			public MediaType contentType() {
				if (contentTypeSupplier == null)
					return null;
				return contentTypeSupplier.get();
			}

			@Override
			public long contentLength() {
				if (contentLengthSupplier == null)
					return -1;
				var contentLength = Optional.ofNullable(contentLengthSupplier.get()).orElse(-1l);
				Validate.isTrue(contentLength >= -1, "invalid contentLength:%s", contentLength);
				return contentLength;
			}

			@Override
			public BufferedSource source() {
				LazyInputStream lazyInputStream = new LazyInputStream(() -> {
					if (sourceSupplier == null)
						return Utils.Bits.emptyInputStream();
					InputStream is = sourceSupplier.get();
					if (is == null)
						return Utils.Bits.emptyInputStream();
					return is;
				});
				return Okio.buffer(Okio.source(lazyInputStream));
			}

		};
	}

	public static Response createResponse(Request request, int responseStatusCode, ResponseBody responseBody) {
		Response.Builder rb = new Response.Builder();
		rb = rb.code(responseStatusCode);
		rb = rb.message(StatusCodes.getReason(responseStatusCode));
		rb = rb.protocol(Protocol.HTTP_1_1);
		rb = rb.request(request);
		return withResponseBody(rb, responseBody);
	}

	public static Response withResponseBody(Response response, ResponseBody responseBody) {
		Objects.requireNonNull(response);
		return withResponseBody(response.newBuilder(), responseBody);
	}

	public static Response withResponseBody(Response.Builder responseBuilder, ResponseBody responseBody) {
		var headersB = OkHttpReflections.Responses.getHeaders(responseBuilder).orElse(null);
		var headers = syncHeaders(headersB, responseBody);
		responseBuilder.headers(headers);
		responseBuilder.body(responseBody);
		return responseBuilder.build();
	}

	public static okhttp3.Headers syncHeaders(okhttp3.Headers headers, ResponseBody responseBody) {
		return syncHeaders(headers == null ? null : headers.newBuilder(), responseBody);
	}

	public static okhttp3.Headers syncHeaders(okhttp3.Headers.Builder headersB, ResponseBody responseBody) {
		if (headersB == null)
			headersB = new okhttp3.Headers.Builder();
		headersB.removeAll(Headers.CONTENT_LENGTH);
		headersB.removeAll(Headers.CONTENT_TYPE);
		if (responseBody != null) {
			if (responseBody.contentType() != null)
				headersB.set(Headers.CONTENT_TYPE, responseBody.contentType().toString());
			headersB.set(Headers.CONTENT_LENGTH, responseBody.contentLength() + "");
		} else
			headersB.set(Headers.CONTENT_LENGTH, 0 + "");
		return headersB.build();
	}

	public static HeaderMap headerMap(Response response) {
		return headerMap(response == null ? null : response.headers());
	}

	public static HeaderMap headerMap(Request request) {
		return headerMap(request == null ? null : request.headers());
	}

	public static HeaderMap headerMap(okhttp3.Headers headers) {
		if (headers == null)
			return HeaderMap.of();
		return HeaderMap.of(headers.toMultimap());
	}

	public static EntryStream<String, String> streamHeaders(Response response, String... nameFilters) {
		return streamHeaders(response == null ? null : response.headers(), nameFilters);
	}

	public static EntryStream<String, String> streamHeaders(Request request, String... nameFilters) {
		return streamHeaders(request == null ? null : request.headers(), nameFilters);
	}

	public static EntryStream<String, String> streamHeaders(okhttp3.Headers headers, String... nameFilters) {
		if (headers == null)
			return EntryStream.empty();
		return Headers.stream(headers.toMultimap(), nameFilters);
	}

	public static EntryStream<URI, Cookie> streamCookies(Response response) {
		if (response == null)
			return EntryStream.empty();
		var uri = response.request().url().uri();
		return Cookies.parseResponseCookies(streamHeaders(response)).mapToEntry(v -> uri, v -> v);
	}

	public static StreamEx<Request> streamRequests(Response... requests) {
		return streamRequests(false, Utils.Lots.stream(requests));
	}

	public static StreamEx<Request> streamRequests(Stream<Response> requests) {
		return streamRequests(false, requests);
	}

	public static StreamEx<Request> streamRequests(boolean reverse, Stream<Response> responses) {
		return streamResponses(reverse, responses).map(Response::request).nonNull();
	}

	public static StreamEx<Response> streamResponses(Response... responses) {
		return streamResponses(false, Utils.Lots.stream(responses));
	}

	public static StreamEx<Response> streamResponses(Stream<Response> responses) {
		return streamResponses(false, responses);
	}

	public static StreamEx<Response> streamResponses(boolean reverse, Stream<Response> responses) {
		if (responses == null)
			return StreamEx.empty();
		var iterStream = Utils.Lots.stream(responses).nonNull().map(response -> {
			return new AbstractLot.Indexed<Response>() {

				private Response currentResponse = response;

				@Override
				protected Response computeNext(long index) {
					if (currentResponse == null)
						return end();
					Response result = currentResponse;
					currentResponse = result.priorResponse();
					return result;
				}

			};
		});
		var stream = Utils.Lots.flatMapSpliterators(iterStream);
		if (!reverse)
			return stream;
		return Utils.Lots.stream(Utils.Lots.reverseView(stream.toList()));
	}

	public static long getContentLength(Response response) {
		Objects.requireNonNull(response);
		ResponseBody body = response.body();
		if (body != null)
			return body.contentLength();
		var values = response.headers(Headers.CONTENT_LENGTH);
		var stream = Utils.Lots.stream(values).filter(Utils.Strings::isNumeric)
				.map(v -> Utils.Functions.catching(() -> Long.valueOf(v), t -> null));
		stream = stream.nonNull();
		stream = stream.filter(v -> v >= -1);
		return stream.findFirst().orElse(-1l);
	}

	public static Optional<MediaType> getContentType(Response response) {
		Objects.requireNonNull(response);
		ResponseBody body = response.body();
		if (body != null) {
			var contentType = body.contentType();
			if (contentType != null)
				return Optional.of(contentType);
		}
		var values = response.headers(Headers.CONTENT_TYPE);
		return Utils.Lots.stream(values).filter(Utils.Strings::isNotBlank)
				.map(v -> Utils.Functions.catching(() -> MediaType.parse(v), t -> null)).nonNull().findFirst();
	}

	public static boolean close(Response response) {
		if (response == null)
			return false;
		ResponseBody body = response.body();
		if (body == null)
			return false;
		response.close();
		return true;
	}

	public static Request toRequest(FormData formData) {
		Objects.requireNonNull(formData);
		URI uri = formData.getActionURI();
		if (Method.GET.equals(formData.getMethod())) {
			var urlb = UrlBuilder.fromUri(uri);
			for (var ent : formData.stream())
				urlb = urlb.addParameter(ent.getKey(), ent.getValue());
			uri = urlb.toUri();
		}
		var rb = new Request.Builder().url(uri.toString());
		if (!Method.GET.equals(formData.getMethod())) {
			var fbb = new FormBody.Builder();
			for (var ent : formData.stream())
				fbb = fbb.add(ent.getKey(), ent.getValue());
			rb = rb.method(formData.getMethod().toString(), fbb.build());
		}
		return rb.build();
	}

	public static Optional<Request> tryGetOriginalRequest(Chain chain) {
		var call = Optional.ofNullable(chain).map(v -> v.call()).orElse(null);
		return OkHttpReflections.Calls.getOriginalRequest(call);
	}

	public static <X> X peekResponse(Response response, long byteCount,
			ThrowingFunction<ResponseBody, X, IOException> peekFunction) throws IOException {
		Objects.requireNonNull(response);
		Objects.requireNonNull(peekFunction);
		ResponseBody responseBody = response.body() == null ? null : response.peekBody(byteCount);
		try {
			return peekFunction.apply(responseBody);
		} finally {
			if (responseBody != null)
				responseBody.close();
		}
	}

	public static <X> X peekDocument(Response response, long byteCount,
			ThrowingFunction<Document, X, IOException> peekFunction) throws IOException {
		return peekResponse(response, byteCount, peekFunction == null ? null : responseBody -> {
			Document doc;
			if (responseBody == null)
				doc = null;
			else {
				var charset = Ok.Calls.tryGetCharset(response.headers()).orElse(null);
				var uri = response.request().url().uri();
				doc = Jsoups.parse(responseBody.byteStream(), charset, uri);
			}
			return peekFunction.apply(doc);
		});
	}

	public static Document toDocument(Response response) throws IOException {
		Objects.requireNonNull(response);
		var headers = response.headers();
		Map<String, List<String>> headerMM = headers == null ? Map.of() : headers.toMultimap();
		try (response; var is = response.body().byteStream()) {
			return Jsoups.parse(is, headerMM, response.request().url().uri());
		}
	}

	public static void main(String[] args) throws IOException {
		try (var resp = Calls.execute(Ok.Clients.get(), "https://i.imgur.com/lMiDok3.png").flushToDisk()) {
			System.out.println(resp.getResponse().body().contentLength());
			System.out.println(resp.getParsedBody().get().getAbsolutePath());
		}
		System.err.println("done");
	}
}