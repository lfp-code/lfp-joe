package com.lfp.joe.okhttp;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.Validate;
import org.jsoup.Jsoup;
import org.xbill.DNS.Type;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.auth.AuthenticatorLFP;
import com.lfp.joe.net.config.NetConfig;
import com.lfp.joe.net.cookie.CookieStoreLFP;
import com.lfp.joe.net.dns.DNSResolvers;
import com.lfp.joe.net.dns.DNSs;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.net.proxy.JdkProxy;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.net.proxy.ProxyAuthenticator;
import com.lfp.joe.net.proxy.ProxyEnabled;
import com.lfp.joe.net.socket.socks.jsocks.JsocksSocks5SocketFactory;
import com.lfp.joe.net.ssl.SSLs;
import com.lfp.joe.okhttp.config.OkHttpClientConfig;
import com.lfp.joe.okhttp.connection.ConnectionPoolContext;
import com.lfp.joe.okhttp.connection.ConnectionPoolContextExpiry;
import com.lfp.joe.okhttp.cookie.CookieStoreJar;
import com.lfp.joe.okhttp.interceptor.ConnectionLowercaseInterceptor;
import com.lfp.joe.okhttp.interceptor.ContentEncodingInterceptor;
import com.lfp.joe.okhttp.interceptor.UserAgentConnectInterceptor;
import com.lfp.joe.okhttp.proxy.ProxyHostnameHashFunction;
import com.lfp.joe.okhttp.reflection.OkHttpReflections;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import okhttp3.Authenticator;
import okhttp3.Call;
import okhttp3.ConnectionPool;
import okhttp3.Credentials;
import okhttp3.Dispatcher;
import okhttp3.Dns;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import one.util.streamex.StreamEx;

public class Clients {

	public static final Void _NetConfig_INIT = NetConfig.init();
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final MemoizedSupplier<OkHttpClient> DEFAULT_INSTANCE_S = Utils.Functions
			.memoize(() -> createDefaultInstanceBuilder().build());
	private static final Dns IPV4_DNS = new Dns() {

		private final Cache<String, List<InetAddress>> cache = Caffeine.newBuilder()
				.maximumSize(Configs.get(OkHttpClientConfig.class).maxProxyDNSCacheSize())
				.expireAfterWrite(Duration.ofMinutes(1))
				.build();

		@Override
		public List<InetAddress> lookup(String hostname) throws UnknownHostException {
			List<InetAddress> result;
			if (IPs.isValidIpAddress(hostname))
				result = Arrays.asList(InetAddress.getByName(hostname));
			else
				result = cache.get(hostname, nil -> {
					return DNSResolvers
							.streamInetAddresses(hostname, DNSResolvers.getRandomDNSServerSet(), null, Type.A)
							.toList();
				});
			return result;
		}
	};
	private static final LoadingCache<Proxy, ConnectionPoolContext> SOCKS_5_CONTEXT_CACHE = Caffeine.newBuilder()
			.expireAfter(
					new ConnectionPoolContextExpiry<Proxy>(Configs.get(OkHttpClientConfig.class).proxyEnvironmentTTL()))
			.build(proxy -> {
				ConnectionPoolContext.Builder peb = ConnectionPoolContext.builder();
				peb = peb.connectionPool(createConnectionPool());
				List<Function<OkHttpClient.Builder, OkHttpClient.Builder>> builderModifiers = new ArrayList<>();
				builderModifiers.add(b -> b.dispatcher(createDispatcher()));
				if (Configs.get(OkHttpClientConfig.class).socks5AuthenticatorEnabled()) {
					AuthenticatorLFP.getDefault().add(new ProxyAuthenticator(proxy));
					builderModifiers.add(b -> b.proxy(proxy.asJdkProxy()));
				} else {
					builderModifiers.add(b -> b.proxy(java.net.Proxy.NO_PROXY));
					builderModifiers.add(b -> b.socketFactory(new JsocksSocks5SocketFactory(proxy)));
				}
				return peb.build();
			});

	private static OkHttpClient.Builder createDefaultInstanceBuilder() {
		OkHttpClientConfig config = Configs.get(OkHttpClientConfig.class);
		OkHttpClient.Builder okb = new OkHttpClient.Builder();
		{// network interceptors
			// stupid goloang needs keep-alive in all lowercase, fucks up traefik
			okb = okb.addNetworkInterceptor(ConnectionLowercaseInterceptor.INSTANCE);
			okb = okb.addNetworkInterceptor(ContentEncodingInterceptor.createDefault());
		}
		{// interceptors
			// add user agent to connect requests
			okb = okb.addInterceptor(new UserAgentConnectInterceptor());
		}
		okb = okb.followSslRedirects(true);
		okb = okb.retryOnConnectionFailure(true);
		okb = okb.connectTimeout(config.connectTimeout());
		okb = okb.readTimeout(config.readTimeout());
		okb = okb.writeTimeout(config.writeTimeout());
		okb = okb.callTimeout(config.callTimeout());
		okb = okb.connectionPool(createConnectionPool());
		okb = okb.dispatcher(createDispatcher());
		return okb;
	}

	public static OkHttpClient.Builder builderDefault() {
		return builderDefault(null);
	}

	public static OkHttpClient.Builder builderDefault(Proxy proxy) {
		OkHttpClient.Builder okb = DEFAULT_INSTANCE_S.get().newBuilder();
		okb = setProxy(okb, proxy);
		return okb;
	}

	private static ConnectionPool createConnectionPool() {
		OkHttpClientConfig config = Configs.get(OkHttpClientConfig.class);
		ConnectionPool cp = new ConnectionPool(
				config.connectionPoolMaxIdleConnectionsCoreMultiplier() * Utils.Machine.logicalProcessorCount(),
				config.connectionPoolIdleKeepAlive().toMillis(), TimeUnit.MILLISECONDS);
		return cp;
	}

	private static Dispatcher createDispatcher() {
		OkHttpClientConfig config = Configs.get(OkHttpClientConfig.class);
		Dispatcher dispatcher = new Dispatcher(Threads.Executors.asExecutorService(CoreTasks.executor()));
		dispatcher.setMaxRequests(config.dispatcherMaxRequestsCoreMultiplier() * Utils.Machine.logicalProcessorCount());
		dispatcher.setMaxRequestsPerHost(
				config.dispatcherMaxRequestsPerHostCoreMultiplier() * Utils.Machine.logicalProcessorCount());
		return dispatcher;
	}

	private static OkHttpClient.Builder setProxy(OkHttpClient.Builder okb, final Proxy clientProxy) {
		if (clientProxy == null)
			return okb;
		okb = okb.dns(IPV4_DNS);
		var finalProxy = ProxyHostnameHashFunction.INSTANCE.apply(clientProxy);
		okb = okb.addInterceptor(chain -> {
			var requestB = chain.request().newBuilder();
			requestB.tag(Proxy.class, finalProxy);
			return chain.proceed(requestB.build());
		});
		if (!finalProxy.isAuthenticationEnabled())
			return okb = okb.proxy(finalProxy.asJdkProxy());
		if (Proxy.Type.SOCKS_5.equals(finalProxy.getType())) {
			ConnectionPoolContext connectionPoolContext = SOCKS_5_CONTEXT_CACHE.get(finalProxy);
			okb = connectionPoolContext.apply(okb);
		} else {
			okb = okb.proxy(finalProxy.asJdkProxy());
			okb = okb.proxyAuthenticator(createProxyAuthenticator(finalProxy).orElse(Authenticator.NONE));
		}
		return okb;

	}

	public static Optional<Authenticator> createProxyAuthenticator(Proxy proxy) {
		if (proxy == null || !proxy.isAuthenticationEnabled())
			return Optional.empty();
		String authorizationHeader = Credentials.basic(proxy.getUsername().orElse(""), proxy.getPassword().orElse(""));
		Authenticator result = (route, response) -> {
			return response.request().newBuilder().header(Headers.PROXY_AUTHORIZATION, authorizationHeader).build();
		};
		return Optional.of(result);
	}

	public static OkHttpClient get() {
		return get(null);
	}

	public static OkHttpClient get(Proxy proxy) {
		if (proxy == null)
			return DEFAULT_INSTANCE_S.get();
		var cb = builderDefault(proxy);
		if (MachineConfig.isDeveloper()
				&& ("localhost".equals(proxy.getHostname()) || "127.0.0.1".equals(proxy.getHostname())))
			cb = Ok.Clients.trustAllSSL(cb);
		return cb.build();
	}

	public static Optional<Proxy> getProxy(OkHttpClient client) {
		if (client == null)
			return Optional.empty();
		Optional<Proxy> proxyOp = toProxy(client.proxy());
		if (proxyOp.isPresent())
			return proxyOp;
		proxyOp = Utils.Types.tryCast(client.socketFactory(), ProxyEnabled.class).map(ProxyEnabled::getProxy);
		return proxyOp;
	}

	public static Optional<Proxy> getProxy(OkHttpClient.Builder clientBuilder) {
		java.net.Proxy proxy = OkHttpReflections.Clients.getProxy(clientBuilder).orElse(null);
		return toProxy(proxy);
	}

	public static Optional<CookieStoreLFP> getCookieStore(OkHttpClient client) {
		if (client == null)
			return Optional.empty();
		return Utils.Types.tryCast(client.cookieJar(), CookieStoreJar.class).map(CookieStoreJar::getCookieStore);
	}

	private static Optional<Proxy> toProxy(java.net.Proxy jProxy) {
		if (jProxy == null || java.net.Proxy.NO_PROXY.equals(jProxy))
			return Optional.empty();
		if (jProxy instanceof JdkProxy)
			return Optional.of(((JdkProxy) jProxy).getProxy());
		return Optional.of(Proxy.builder(jProxy).build());
	}

	public static SocketFactory getSocketFactory(OkHttpClient.Builder builder) {
		SocketFactory factory = OkHttpReflections.Clients.getSocketFactory(builder).orElse(null);
		return Objects.requireNonNull(factory);
	}

	public static Optional<SSLSocketFactory> getSSLSocketFactory(OkHttpClient.Builder builder) {
		SSLSocketFactory factory = OkHttpReflections.Clients.getSSLSocketFactory(builder).orElse(null);
		return Optional.ofNullable(factory);
	}

	public static OkHttpClient trustAllSSL(OkHttpClient client) {
		return trustAllSSL(client.newBuilder()).build();
	}

	public static OkHttpClient.Builder trustAllSSL(OkHttpClient.Builder clientBuilder) {
		Objects.requireNonNull(clientBuilder);
		return clientBuilder.sslSocketFactory(SSLs.trustAllContext().getSocketFactory(), SSLs.trustAllManager())
				.hostnameVerifier((hostname, session) -> true);
	}

	public static OkHttpClient withTimeouts(OkHttpClient client, Duration timeout) {
		return setTimeouts(client.newBuilder(), timeout).build();
	}

	public static OkHttpClient withTimeouts(OkHttpClient client, Duration connectTimeout, Duration readTimeout,
			Duration writeTimeout, Duration callTimeout) {
		return setTimeouts(client.newBuilder(), connectTimeout, readTimeout, writeTimeout, callTimeout).build();
	}

	public static OkHttpClient.Builder setTimeouts(OkHttpClient.Builder clientBuilder, Duration timeout) {
		Duration callTimeout = timeout == null ? null : timeout.plus(timeout.dividedBy(2));
		return setTimeouts(clientBuilder, timeout, timeout, timeout, callTimeout);
	}

	public static OkHttpClient.Builder setTimeouts(OkHttpClient.Builder clientBuilder, Duration connectTimeout,
			Duration readTimeout, Duration writeTimeout, Duration callTimeout) {
		Objects.requireNonNull(clientBuilder);
		Map<BiFunction<OkHttpClient.Builder, Duration, OkHttpClient.Builder>, Duration> setterMap = new HashedMap<>();
		setterMap.put((cb, v) -> cb.connectTimeout(v), connectTimeout);
		setterMap.put((cb, v) -> cb.readTimeout(v), readTimeout);
		setterMap.put((cb, v) -> cb.writeTimeout(v), writeTimeout);
		setterMap.put((cb, v) -> cb.callTimeout(v), callTimeout);
		for (var ent : setterMap.entrySet()) {
			var setter = ent.getKey();
			var value = ent.getValue();
			if (value == null)
				continue;
			clientBuilder = setter.apply(clientBuilder, value);
		}
		return clientBuilder;
	}

	public static Optional<OkHttpClient> getClient(Call call) {
		return OkHttpReflections.Clients.get(call);
	}

	public static Optional<OkHttpClient> getClient(Response response) {
		if (response == null)
			return Optional.empty();
		var exchange = OkHttpReflections.Exchanges.get(response).orElse(null);
		if (exchange == null)
			exchange = OkHttpReflections.Exchanges.get(response.networkResponse()).orElse(null);
		var call = OkHttpReflections.Calls.getCall(exchange).orElse(null);
		return getClient(call);
	}

	public static String getIPAddress(OkHttpClient client) {
		Objects.requireNonNull(client);
		return IPs.getIPAddress(uri -> {
			var rb = new Request.Builder();
			rb.url(uri.toString());
			rb.header(Headers.CONNECTION, "close");
			rb.header(Headers.PROXY_CONNECTION, "close");
			return client.newCall(rb.build()).execute().body().byteStream();
		});
	}

	public static void main(String[] args) throws IOException {
		Supplier<StreamEx<Proxy>> proxyStreamSupplier = () -> {
			Proxy proxy = Serials.Gsons.get()
					.fromJson("{\r\n" + "	\"type\": \"SOCKS_5\",\r\n"
							+ "	\"hostname\": \"iqczk7augm5uqftt.157.245.82.171.nip.io\",\r\n"
							+ "	\"port\": 33080,\r\n" + "	\"username\": \"zChPn6fcGmPMz4kVufgLkPU\",\r\n"
							+ "	\"password\": \"z5gQuuSYGhwcdFpR9m7t2gbkhEVZPqaoP2Vbxog9GhUEFu8s\"\r\n" + "}\r\n" + "",
							Proxy.class);
			return StreamEx.of(IntStream.range(0, Integer.MAX_VALUE).mapToObj(i -> i)).map(i -> {
				String hostname = proxy.getHostname();
				hostname = DNSs.toWildcardDNSHostname(hostname, "host" + i);
				return proxy.toBuilder().hostname(hostname).build();
			});
		};
		for (int i = 0;; i++) {
			OkHttpClient client = Ok.Clients
					.get(proxyStreamSupplier.get().skip(Utils.Crypto.getRandomInclusive(0, 10_000)).findFirst().get());
			try (Response response = client.newCall(
					new Request.Builder().url("https://twitter.com/i/search/timeline?f=users&q=galimmo&max_position=")
							.build())
					.execute()) {
				Validate.isTrue(response.isSuccessful());
				String html = response.body().string();
				System.out.println(Jsoup.parse(html).title());
				System.out.println(Ok.Clients.getProxy(client).orElse(null));
			}
			// System.out.println(scrapable.scrap());
		}
	}
}