package com.lfp.joe.okhttp.call;

import java.io.IOException;
import java.net.http.HttpRequest;
import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.utils.Utils;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;
import one.util.streamex.StreamEx;

public class HttpRequestBody extends RequestBody {

	public static Optional<HttpRequestBody> from(HttpRequest httpRequest) {
		if (httpRequest == null || httpRequest.bodyPublisher().isEmpty())
			return Optional.empty();
		return Optional.of(new HttpRequestBody(httpRequest));
	}

	private HttpRequest httpRequest;

	public HttpRequestBody(HttpRequest httpRequest) {
		this.httpRequest = Objects.requireNonNull(httpRequest);
		Validate.isTrue(httpRequest.bodyPublisher().isPresent());
	}

	@Override
	public MediaType contentType() {
		var values = HeaderMap.of(this.httpRequest.headers()).stream(Headers.CONTENT_TYPE).values();
		StreamEx<MediaType> mediaTypes = values.map(v -> {
			return Utils.Functions.catching(() -> MediaType.parse(v), t -> null);
		});
		mediaTypes = mediaTypes.nonNull();
		return mediaTypes.findFirst().orElseGet(() -> {
			return MediaType.parse(com.google.common.net.MediaType.OCTET_STREAM.toString());
		});
	}

	@Override
	public void writeTo(BufferedSink sink) throws IOException {
		var bodyPublisher = this.httpRequest.bodyPublisher().get();
		var errorRef = new AtomicReference<Throwable>();
		var latch = new CountDownLatch(1);
		bodyPublisher.subscribe(new Subscriber<ByteBuffer>() {

			@Override
			public void onSubscribe(Subscription subscription) {
			}

			@Override
			public void onNext(ByteBuffer item) {
				Utils.Functions.unchecked(() -> sink.write(item));
			}

			@Override
			public void onError(Throwable throwable) {
				errorRef.set(throwable);
				latch.countDown();
			}

			@Override
			public void onComplete() {
				latch.countDown();
			}
		});
		try {
			latch.await();
		} catch (InterruptedException e) {
			if (errorRef.get() == null)
				errorRef.set(e);
		}
		if (errorRef.get() != null)
			throw Utils.Exceptions.as(errorRef.get(), IOException.class);

	}
}
