package com.lfp.joe.okhttp.interceptor;

import java.io.IOException;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.headers.UserAgents;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

public class UserAgentInterceptor extends HeaderInterceptor {

	private static final MemoizedSupplier<UserAgentInterceptor> DEFAULT_INSTANCE_S = Utils.Functions
			.memoize(() -> new UserAgentInterceptor(() -> {
				return UserAgents.CHROME_LATEST.get();
			}));

	public static UserAgentInterceptor getBrowserDefault() {
		return DEFAULT_INSTANCE_S.get();
	}

	private final ThrowingFunction<Chain, String, IOException> userAgentFunction;

	public UserAgentInterceptor(String userAgent) {
		this(userAgent == null ? null : () -> userAgent);
	}

	public UserAgentInterceptor(ThrowingSupplier<String, IOException> userAgentSupplier) {
		this(userAgentSupplier == null ? null : chain -> userAgentSupplier.get());
	}

	public UserAgentInterceptor(ThrowingFunction<Chain, String, IOException> userAgentFunction) {
		super(false, false, Headers.USER_AGENT);
		this.userAgentFunction = userAgentFunction;
	}

	@Override
	protected HeaderMap getHeaderMap(Chain chain) throws IOException {
		if (this.userAgentFunction == null)
			return HeaderMap.of();
		var userAgent = this.userAgentFunction.apply(chain);
		if (userAgent == null)
			return HeaderMap.of();
		return HeaderMap.of(Headers.USER_AGENT, userAgent);
	}

}
