package com.lfp.joe.okhttp.interceptor;

import java.io.IOException;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.headers.UserAgents;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.okhttp.config.OkHttpClientConfig;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

public class BrowserHeaderInterceptor extends UserAgentInterceptor {

	private static final MemoizedSupplier<BrowserHeaderInterceptor> DEFAULT_INSTANCE_S = Utils.Functions
			.memoize(() -> new BrowserHeaderInterceptor(() -> {
				return UserAgents.CHROME_LATEST.get();
			}));

	public static BrowserHeaderInterceptor getBrowserDefault() {
		return DEFAULT_INSTANCE_S.get();
	}

	public BrowserHeaderInterceptor(String userAgent) {
		super(userAgent);
	}

	public BrowserHeaderInterceptor(ThrowingFunction<Chain, String, IOException> userAgentFunction) {
		super(userAgentFunction);
	}

	public BrowserHeaderInterceptor(ThrowingSupplier<String, IOException> userAgentSupplier) {
		super(userAgentSupplier);
	}

	@Override
	protected HeaderMap getHeaderMap(Chain chain) throws IOException {
		var defaultBrowserHeaders = getDefaultBrowserHeaders();
		var superHeaderMap = super.getHeaderMap(chain);
		if (superHeaderMap == null || superHeaderMap.isEmpty())
			return defaultBrowserHeaders;
		for (var name : superHeaderMap.stream().keys())
			if (!defaultBrowserHeaders.contains(name))
				defaultBrowserHeaders = defaultBrowserHeaders.with(name,
						superHeaderMap.allValues(name).toArray(String[]::new));
		return defaultBrowserHeaders;
	}

	protected HeaderMap getDefaultBrowserHeaders() throws IOException {
		return HeaderMap.of(Configs.get(OkHttpClientConfig.class).defaultBrowserHeaders());
	}

}
