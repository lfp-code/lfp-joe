package com.lfp.joe.okhttp.interceptor;

import java.io.IOException;
import java.time.Duration;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import org.immutables.value.Value;

import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.NumeralFunctions.QuintConsumer;
import com.lfp.joe.core.function.NumeralFunctions.TriFunction;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.log.Logging;
import com.lfp.joe.okhttp.interceptor.ImmutableRetryInterceptor.RetryInterceptorFilter;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.utils.Utils;

import ch.qos.logback.classic.Level;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

@ValueLFP.Style
@Value.Enclosing
public class RetryInterceptor extends CopyOnWriteArrayList<RetryInterceptorFilter> implements Interceptor {

	private static final long serialVersionUID = 8000813430747510764L;
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	@Override
	public Response intercept(Chain chain) throws IOException {
		Map<RetryInterceptorFilter, AtomicInteger> filterMap = null;
		var request = chain.request();
		Duration delay = null;
		while (!Thread.currentThread().isInterrupted()) {
			Response response = null;
			Throwable failure = null;
			try {
				if (delay != null) {
					Thread.sleep(delay.toMillis());
					delay = null;
				}
				response = chain.proceed(request);
			} catch (Throwable t) {
				failure = t;
			}
			boolean retry = false;
			var retryFilterIter = this.iterator();
			while (!Thread.currentThread().isInterrupted() && !retry && retryFilterIter.hasNext()) {
				if (filterMap == null)
					filterMap = new LinkedHashMap<>();
				var filter = retryFilterIter.next();
				var counter = filterMap.computeIfAbsent(filter, nil -> new AtomicInteger());
				if (counter.get() >= filter.maxRetries())
					continue;
				if (!Boolean.TRUE.equals(filter.retryFunction().apply(request, response, failure)))
					continue;
				counter.incrementAndGet();
				filter.onRetry().accept(filter, counter.get(), request, response, failure);
				request = Optional.ofNullable(filter.requestFunction().apply(request, response, failure))
						.orElse(request);
				delay = filter.delay().orElse(null);
				retry = true;
				if (response != null)
					response.close();
			}
			if (retry)
				continue;
			if (response != null)
				return response;
			if (failure instanceof IOException)
				throw (IOException) failure;
			throw Throws.unchecked(failure);
		}
		throw new IOException("interrupted intercept");
	}

	protected static String getSummary(RetryInterceptorFilter filter, Integer retries, Request request,
			Response response, Throwable failure) {
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("name", filter.name().orElse(null));
		data.put("delay", filter.delay().orElse(null));
		data.put("maxRetries", filter.maxRetries());
		data.put("retries", retries);
		data.put("request", request);
		data.put("response", response);
		data.put("failure", Optional.ofNullable(failure).map(Object::getClass).map(Class::getName).orElse(null));
		data.put("failureMessage", Optional.ofNullable(failure).map(Throwable::getMessage).orElse(null));
		var estream = EntryStreams.of(data).mapValues(v -> {
			return Optional.ofNullable(v).map(Object::toString).map(Utils.Strings::trimToNull).orElse(null);
		});
		estream = estream.nonNullValues();
		var message = estream.map(ent -> String.format("%s=%s", ent.getKey(), ent.getValue())).joining(" ");
		return message;
	}

	protected static void logSummary(Level level, RetryInterceptorFilter filter, Integer retries, Request request,
			Response response, Throwable failure) {
		Logging.log(LOGGER, level, getSummary(filter, retries, request, response, failure));
	}

	@Value.Immutable
	public static abstract class AbstractRetryInterceptorFilter {

		public abstract Optional<String> name();

		public abstract Optional<Duration> delay();

		public abstract int maxRetries();

		public abstract TriFunction<Request, Response, ? super Throwable, Boolean> retryFunction();

		@Value.Default
		public TriFunction<Request, Response, ? super Throwable, Request> requestFunction() {
			return (request, response, failure) -> request;
		}

		@Value.Default
		public QuintConsumer<RetryInterceptorFilter, Integer, Request, Response, ? super Throwable> onRetry() {
			return (filter, retries, request, response, failure) -> {
				logSummary(Level.WARN, filter, retries, request, response, failure);
			};
		}

	}

}
