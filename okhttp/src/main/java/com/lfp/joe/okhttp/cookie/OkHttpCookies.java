package com.lfp.joe.okhttp.cookie;

import java.net.HttpCookie;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;

import org.apache.commons.lang3.Validate;

import com.github.throwable.beanref.BeanProperty;
import com.github.throwable.beanref.BeanRef;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.net.cookie.Cookie;
import com.lfp.joe.utils.Utils;

import okhttp3.HttpUrl;

public class OkHttpCookies {
	private static final Date END_OF_TIME = new Date(253402214400000l - Duration.ofDays(1).toMillis());
	private static final MemoizedSupplier<List<BiConsumer<okhttp3.Cookie, Cookie.Builder>>> COOKIE_BUILDER_MAPPER = Utils.Functions
			.memoize(() -> {
				var bps = BeanRef.$(okhttp3.Cookie.class).all();
				Cookie.Meta meta = Cookie.meta();
				List<BeanProperty<okhttp3.Cookie, ?>> nonMappedBPs = new ArrayList<>();
				{
					nonMappedBPs.add(BeanRef.$(okhttp3.Cookie::expiresAt));
					nonMappedBPs.add(BeanRef.$(okhttp3.Cookie::persistent));
					nonMappedBPs.add(BeanRef.$(okhttp3.Cookie::hostOnly));
					for (var bp : BeanRef.$(okhttp3.Cookie.class).all()) {
						var path = bp.getPath();
						if (Utils.Strings.startsWith(path, "-deprecated"))
							nonMappedBPs.add((BeanProperty<okhttp3.Cookie, ?>) bp);
					}
				}
				List<BiConsumer<okhttp3.Cookie, Cookie.Builder>> result = new ArrayList<>();
				for (var bp : bps) {
					if (nonMappedBPs.contains(bp))
						continue;
					var path = bp.getPath();
					Validate.isTrue(meta.metaPropertyExists(path), "can't map bean property to cookie meta property:%s",
							path);
					var mp = meta.metaProperty(path);
					result.add((c, cb) -> cb.set(mp, bp.get(c)));
				}
				result.add((cookie, cookieBuilder) -> {
					var expiresAtMillis = cookie.expiresAt();
					int maxAge;
					if (expiresAtMillis >= END_OF_TIME.getTime())
						maxAge = -1;
					else if (expiresAtMillis < 0)
						maxAge = 0;
					else {
						long ttlMilli = expiresAtMillis - System.currentTimeMillis();
						if (ttlMilli <= 0)
							maxAge = 0;
						else
							maxAge = (int) Duration.ofMillis(ttlMilli).toSeconds();
					}
					cookieBuilder.maxAge(maxAge);
				});
				return result;
			});

	public static Cookie toCookie(okhttp3.Cookie cookie) {
		if (cookie == null)
			return null;
		var cookieBuilder = Cookie.builder();
		COOKIE_BUILDER_MAPPER.get().forEach(v -> v.accept(cookie, cookieBuilder));
		var result = cookieBuilder.build();
		if (result.isExpired())
			return null;
		return result;
	}

	public static HttpCookie toHttpCookie(okhttp3.Cookie cookie) {
		var joeCookie = toCookie(cookie);
		return joeCookie == null ? null : joeCookie.toHttpCookie();
	}

	public static okhttp3.Cookie toRequestCookie(HttpUrl url, Cookie cookie) {
		if (cookie == null)
			return null;
		Function<String, String> domainStrip = s -> {
			if (s == null)
				return "";
			while (Utils.Strings.startsWithIgnoreCase(s, "."))
				s = s.substring(1);
			if (Utils.Strings.isBlank(s))
				return "";
			return s;
		};
		String domain = domainStrip.apply(cookie.getDomain());
		if (Utils.Strings.isBlank(domain))
			domain = domainStrip.apply(url.host());
		if (Utils.Strings.isBlank(domain))
			domain = null;
		return new okhttp3.Cookie.Builder().domain(domain).name(cookie.getName()).value(cookie.getValue()).build();
	}

	public static okhttp3.Cookie toRequestCookie(HttpUrl url, HttpCookie cookie) {
		return toRequestCookie(url, cookie == null ? null : Cookie.build(cookie));
	}
}
