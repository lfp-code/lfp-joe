package com.lfp.joe.okhttp.interceptor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.lang3.StringUtils;

import com.lfp.joe.utils.Utils;

import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Response;

public enum ConnectionLowercaseInterceptor implements Interceptor {
	INSTANCE;

	@Override
	public Response intercept(Chain chain) throws IOException {
		var request = chain.request();
		if (request == null)
			return chain.proceed(request);
		Headers headers = request.headers();
		if (headers == null)
			return chain.proceed(request);
		var size = headers.size();
		if (size <= 0)
			return chain.proceed(request);
		boolean mod = false;
		Headers.Builder headersB = new Headers.Builder();
		for (int i = 0; i < size; i++) {
			var name = headers.name(i);
			if (shouldLowerCase(name)) {
				mod = true;
				name = name.toLowerCase();
			}
			var value = headers.value(i);
			headersB.add(name, value);
		}
		if (!mod)
			return chain.proceed(request);
		return chain.proceed(request.newBuilder().headers(headersB.build()).build());
	}

	private static boolean shouldLowerCase(String name) {
		if (Utils.Strings.equalsIgnoreCase(name, com.lfp.joe.net.http.headers.Headers.CONNECTION))
			return true;
		if (Utils.Strings.endsWithIgnoreCase(name, "-" + com.lfp.joe.net.http.headers.Headers.CONNECTION))
			return true;
		return false;
	}

}
