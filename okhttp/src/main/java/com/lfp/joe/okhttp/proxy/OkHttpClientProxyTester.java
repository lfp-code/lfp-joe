package com.lfp.joe.okhttp.proxy;

import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.net.proxy.test.ProxyTester;
import com.lfp.joe.net.proxy.test.ProxyTesterImpl;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.core.function.Throws.ThrowingFunction;

import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;

public class OkHttpClientProxyTester extends ProxyTesterImpl {

	public static final OkHttpClientProxyTester getDefault() {
		return new OkHttpClientProxyTester();
	}

	private static final ThrowingFunction<Request, Response, IOException> DEFAULT_ENGINE = req -> {
		ConnectionPool connectionPool = new ConnectionPool(0, Duration.ofMillis(1).toMillis(), TimeUnit.MILLISECONDS);
		OkHttpClient client = Ok.Clients.builderDefault(req.proxy).connectionPool(connectionPool).build();
		okhttp3.Request.Builder rb = new okhttp3.Request.Builder().url(req.uri.toString());
		for (String k : req.headers.keySet()) {
			for (String v : req.headers.get(k))
				rb = rb.addHeader(k, v);
		}
		okhttp3.Response response = client.newCall(rb.build()).execute();
		return Response.builder().withStatusCode(response.code()).withBody(() -> response.body().byteStream())
				.withCloseable(response::close).build();
	};

	public OkHttpClientProxyTester() {
		super(DEFAULT_ENGINE);
	}

	public static void main(String[] args) throws IOException {
		Proxy proxy = Serials.Gsons.get().fromJson(
				"{\"type\":\"SOCKS_5\",\"hostname\":\"192.161.53.145\",\"port\":61336,\"username\":\"reggiepierce\",\"password\":\"Cl3aDfVc3DWeEUNLRf3CTydm5arz\"}\r\n"
						+ "",
				Proxy.class);
		OkHttpClientProxyTester.getDefault().validate(proxy);
		System.out.println("GOOD");
	}

}
