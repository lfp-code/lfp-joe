package com.lfp.joe.okhttp.ssl;

import java.security.cert.CertificateException;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiFunction;

import javax.net.ssl.SSLException;
import javax.net.ssl.X509TrustManager;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.net.ssl.ErrorCatchingX509TrustManager;
import com.lfp.joe.net.ssl.SSLs;
import com.lfp.joe.okhttp.interceptor.CurrentChainInterceptor;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingBiConsumer;

import okhttp3.Interceptor.Chain;
import okhttp3.OkHttpClient;
import okhttp3.internal.tls.OkHostnameVerifier;

public class SSLErrorConsumer implements
		BiFunction<OkHttpClient.Builder, ThrowingBiConsumer<Optional<Chain>, Exception, CertificateException>, OkHttpClient.Builder> {

	private static final MemoizedSupplier<SSLErrorConsumer> INSTANCE_S = Utils.Functions.memoize(() -> {
		X509TrustManager trustManager;
		{
			var trustManagers = Utils.Lots.stream(SSLs.trustManagersDefault()).select(X509TrustManager.class)
					.toArray(X509TrustManager.class);
			trustManager = SSLs.concat(trustManagers);
		}
		return new SSLErrorConsumer(trustManager);
	});

	public static SSLErrorConsumer getDefault() {
		return INSTANCE_S.get();
	}

	private final X509TrustManager trustManager;

	public SSLErrorConsumer(X509TrustManager trustManager) {
		this.trustManager = Objects.requireNonNull(trustManager);

	}

	@Override
	public OkHttpClient.Builder apply(OkHttpClient.Builder clientBuilder,
			ThrowingBiConsumer<Optional<Chain>, Exception, CertificateException> onError) {
		clientBuilder = CurrentChainInterceptor.getDefault().apply(clientBuilder);
		clientBuilder = clientBuilder.hostnameVerifier((host, session) -> {
			try {
				if (!OkHostnameVerifier.INSTANCE.verify(host, session))
					onError(onError, new SSLException("host verification failed:" + host));
			} catch (Exception e) {
				Utils.Functions.unchecked(() -> onError(onError, e));
			}
			return true;
		});
		var trustManager = new ErrorCatchingX509TrustManager(this.trustManager, e -> {
			onError(onError, e);
		});
		var sf = SSLs.createSSLSocketFactory(trustManager);
		return clientBuilder.sslSocketFactory(sf, trustManager);
	}

	private void onError(ThrowingBiConsumer<Optional<Chain>, Exception, CertificateException> onError,
			Exception error) throws CertificateException {
		var chainOp = CurrentChainInterceptor.tryGet();
		if (onError != null)
			onError.accept(chainOp, error);
		else {
			if (error instanceof CertificateException)
				throw (CertificateException) error;
			throw Utils.Exceptions.asRuntimeException(error);
		}

	}
}
