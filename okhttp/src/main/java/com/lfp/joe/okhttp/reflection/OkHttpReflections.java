package com.lfp.joe.okhttp.reflection;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldsRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

import okhttp3.Call;
import okhttp3.Interceptor;
import okhttp3.Interceptor.Chain;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.connection.Exchange;
import okhttp3.internal.http.RealInterceptorChain;

@SuppressWarnings("unchecked")
public class OkHttpReflections {

	private static final Class<? extends Call> RealCall_CLASS_TYPE = CoreReflections
			.tryForName(Call.class.getPackageName() + ".RealCall")
			.map(v -> (Class<? extends Call>) v)
			.get();

	static <X> Field getField(Class<X> classType, Class<?> fieldType, String nameFilter, boolean nameFilterExact) {
		{
			var field = MemberCache
					.tryGetField(FieldRequest.of(classType, fieldType, nameFilter).withAllTypes(false), true)
					.orElse(null);
			if (field != null)
				return field;
		}
		if (!nameFilterExact) {
			var fieldStream = Streams.of(MemberCache.getFields(FieldsRequest.of(classType).withAllTypes(false)));
			fieldStream = fieldStream.filter(v -> CoreReflections.isAssignableFrom(fieldType, v.getType()));
			fieldStream = fieldStream.filter(v -> Utils.Strings.containsIgnoreCase(v.getName(), nameFilter));
			fieldStream = fieldStream.limit(2);
			var fields = fieldStream.toList();
			if (fields.size() == 1)
				return fields.get(0);
		}
		throw new IllegalArgumentException(
				String.format("field lookup failed. classType:%s fieldType:%s nameFilter:%s nameFilterExact:%s",
						classType, fieldType, nameFilter, nameFilterExact));
	}

	static <X, Y> void setFieldValue(X object, Class<Y> fieldType, String nameFilter, Y value,
			boolean nameFilterExact) {
		Throws.unchecked(
				() -> getField(object == null ? null : object.getClass(), fieldType, nameFilter, nameFilterExact)
						.set(object, value));
	}

	static <X, Y> Y getFieldValue(X object, Class<Y> fieldType, String nameFilter, boolean nameFilterExact) {
		return (Y) Throws.unchecked(() -> {
			var field = getField(object == null ? null : object.getClass(), fieldType, nameFilter, nameFilterExact);
			return field.get(object);
		});
	}

	public static interface Calls {

		static Optional<List<Interceptor>> getInterceptors(Chain chain) {
			RealInterceptorChain realInterceptorChain = Utils.Types.tryCast(chain, RealInterceptorChain.class)
					.orElse(null);
			if (realInterceptorChain == null)
				return Optional.empty();
			List<Interceptor> result = getFieldValue(realInterceptorChain, List.class, "interceptors", true);
			return Optional.ofNullable(result);
		}

		static Optional<Call> getCall(Exchange input) {
			if (input == null)
				return Optional.empty();
			Call result = getFieldValue(input, Call.class, "call", true);
			return Optional.ofNullable(result);
		}

		static Optional<Request> getOriginalRequest(Call call) {
			if (call == null)
				return Optional.empty();
			Request result = getFieldValue(call, Request.class, "originalRequest", true);
			return Optional.ofNullable(result);
		}

		static void setOriginalRequest(Call call, Request request) {
			Objects.requireNonNull(call);
			setFieldValue(call, Request.class, "originalRequest", request, true);
		}

		static void setRequest(Call input, Request request) {
			setFieldValue(input, Request.class, "request", request, false);
		}

		static Map<Class<?>, Object> getTags(Request request) {
			if (request == null)
				return Map.of();
			Map<Class<?>, Object> result = getFieldValue(request, Map.class, "tags", false);
			if (result == null)
				return Map.of();
			return Utils.Lots.unmodifiable(result);
		}

	}

	public static interface Exchanges {

		static Optional<Exchange> get(Response input) {
			if (input == null)
				return Optional.empty();
			Exchange result = getFieldValue(input, Exchange.class, "exchange", true);
			return Optional.ofNullable(result);
		}

	}

	public static interface Clients {

		static Optional<OkHttpClient> get(Call input) {
			if (input == null)
				return Optional.empty();
			if (!RealCall_CLASS_TYPE.isAssignableFrom(input.getClass()))
				return Optional.empty();
			OkHttpClient result = getFieldValue(input, OkHttpClient.class, "client", true);
			return Optional.ofNullable(result);
		}

		static Optional<java.net.Proxy> getProxy(OkHttpClient.Builder input) {
			if (input == null)
				return Optional.empty();
			java.net.Proxy result = getFieldValue(input, java.net.Proxy.class, "proxy", true);
			return Optional.ofNullable(result);
		}

		static Optional<SocketFactory> getSocketFactory(OkHttpClient.Builder input) {
			if (input == null)
				return Optional.empty();
			SocketFactory result = getFieldValue(input, SocketFactory.class, "socketFactory", true);
			return Optional.ofNullable(result);
		}

		static Optional<SSLSocketFactory> getSSLSocketFactory(OkHttpClient.Builder input) {
			if (input == null)
				return Optional.empty();
			SSLSocketFactory result = getFieldValue(input, SSLSocketFactory.class, "sslSocketFactory", true);
			return Optional.ofNullable(result);
		}
	}

	public static interface Responses {

		static void setPriorResponse(Response response, Response priorResponse) {
			setFieldValue(response, Response.class, "priorResponse", priorResponse, true);
		}

		static void setResponseBody(Response response, ResponseBody responseBody) {
			setFieldValue(response, ResponseBody.class, "body", responseBody, true);
		}

		static Optional<okhttp3.Headers.Builder> getHeaders(Response.Builder input) {
			if (input == null)
				return Optional.empty();
			okhttp3.Headers.Builder result = getFieldValue(input, okhttp3.Headers.Builder.class, "headers", true);
			return Optional.ofNullable(result);
		}
	}

}
