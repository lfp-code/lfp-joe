package com.lfp.joe.okhttp.config;

import java.time.Duration;
import java.util.List;
import java.util.Map;

import com.lfp.joe.properties.converter.DurationConverter;
import com.lfp.joe.properties.converter.JsonConverter;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.ConverterClass;

public interface OkHttpClientConfig extends Config {

	@DefaultValue("512000") // ~512kb
	long maxMappableBodyMemoryBytes();

	@ConverterClass(DurationConverter.class)
	@DefaultValue("60 sec") // 10 minutes
	Duration proxyEnvironmentTTL();

	@DefaultValue("false")
	boolean socks5AuthenticatorEnabled();

	@ConverterClass(DurationConverter.class)
	@DefaultValue("15 sec")
	Duration connectTimeout();

	@ConverterClass(DurationConverter.class)
	@DefaultValue("30 sec")
	Duration readTimeout();

	@ConverterClass(DurationConverter.class)
	@DefaultValue("30 sec")
	Duration writeTimeout();

	@ConverterClass(DurationConverter.class)
	@DefaultValue("45 sec")
	Duration callTimeout();

	@DefaultValue("250")
	int connectionPoolMaxIdleConnectionsCoreMultiplier();

	@ConverterClass(DurationConverter.class)
	@DefaultValue("5 min")
	Duration connectionPoolIdleKeepAlive();

	@DefaultValue("1000")
	int dispatcherMaxRequestsCoreMultiplier();

	@DefaultValue("500")
	int dispatcherMaxRequestsPerHostCoreMultiplier();

	@DefaultValue("true")
	boolean useWildcardDNSIndexOnDuplicateProxy();

	@DefaultValue("3")
	int maxRetryProxyConnection();

	@DefaultValue("1000000")
	long maxProxyDNSCacheSize();

	@ConverterClass(JsonConverter.class)
	@DefaultValue("{\"Accept\":[\"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9\"],\"Accept-Encoding\":[\"gzip, deflate, br\"],\"Accept-Language\":[\"en-US,en;q=0.9\"],\"Upgrade-Insecure-Requests\":[\"1\"]}")
	Map<String, List<String>> defaultBrowserHeaders();
}
