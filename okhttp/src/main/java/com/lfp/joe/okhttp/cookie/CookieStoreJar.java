package com.lfp.joe.okhttp.cookie;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import com.lfp.joe.net.cookie.CookieStoreLFP;
import com.lfp.joe.utils.Utils;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;

public interface CookieStoreJar extends CookieJar {

	CookieStoreLFP getCookieStore();

	@Override
	default void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
		if (url == null || cookies == null)
			return;
		var cookieStream = Utils.Lots.stream(cookies).map(OkHttpCookies::toHttpCookie);
		var cookieList = cookieStream.toList();
		if (cookieList.isEmpty())
			return;
		var cookieStore = getCookieStore();
		if (cookieStore == null)
			return;
		cookieStore.add(url.uri(), cookieList);
	}

	@Override
	default List<Cookie> loadForRequest(HttpUrl url) {
		if (url == null)
			return Collections.emptyList();
		var cookieStore = getCookieStore();
		if (cookieStore == null)
			return Collections.emptyList();
		var cookies = cookieStore.get(url.uri());
		if (cookies == null || cookies.isEmpty())
			return Collections.emptyList();
		var result = Utils.Lots.stream(cookies).map(v -> OkHttpCookies.toRequestCookie(url, v)).nonNull().toList();
		return result;
	}

	public static class Impl implements CookieStoreJar {

		private final CookieStoreLFP cookieStore;

		public Impl(CookieStoreLFP cookieStore) {
			this.cookieStore = Objects.requireNonNull(cookieStore);
		}

		@Override
		public CookieStoreLFP getCookieStore() {
			return this.cookieStore;
		}
	}

}
