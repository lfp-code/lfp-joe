package test;

import java.io.IOException;
import java.net.URI;

import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.okhttp.interceptor.UserAgentInterceptor;

import okhttp3.Request;

public class UserAgentTest {

	public static void main(String[] args) throws IOException {
		var cb = Ok.Clients.get(Proxy.builder(URI.create("http://localhost:8888")).build()).newBuilder();
		cb.addInterceptor(UserAgentInterceptor.getBrowserDefault());
		cb = Ok.Clients.trustAllSSL(cb);
		var client = cb.build();
		var rb = new Request.Builder().url("https://api.ipify.org");
		try (var rctx = Ok.Calls.execute(client, rb.build()).validateSuccess()) {
			System.out.println(rctx.parseText().getParsedBody());
		}
	}
}
