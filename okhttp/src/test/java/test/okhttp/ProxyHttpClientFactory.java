package test.okhttp;

import com.budjb.httprequests.HttpClient;
import com.budjb.httprequests.HttpClientFactory;
import com.lfp.joe.net.proxy.Proxy;

public interface ProxyHttpClientFactory extends HttpClientFactory {

	@Override
	default HttpClient createHttpClient() {
		return createHttpClient(null);
	}

	 HttpClient createHttpClient(Proxy proxy) ;
}
