package test.okhttp;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import com.budjb.httprequests.HttpClient;
import com.budjb.httprequests.HttpClientFactory;
import com.budjb.httprequests.HttpResponse;
import com.budjb.httprequests.converter.EntityConverterManager;
import com.budjb.httprequests.converter.bundled.StringEntityReader;
import com.budjb.httprequests.exception.UnsupportedConversionException;

public class HttpRequestsTest {

	public static void main(String[] args) throws IOException, URISyntaxException, UnsupportedConversionException {
		EntityConverterManager entityConverterManager = new EntityConverterManager(List.of(new StringEntityReader()));
		HttpClientFactory factory = new OkHttpHttpClientFactory(entityConverterManager);
		HttpClient client = factory.createHttpClient();
		try (HttpResponse response = client.get("https://api.ipify.org");) {
			System.out.println(response.getEntity(String.class));
		}
	}
}
