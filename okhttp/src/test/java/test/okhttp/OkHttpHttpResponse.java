package test.okhttp;

import java.io.IOException;
import java.io.InputStream;

import com.budjb.httprequests.HttpEntity;
import com.budjb.httprequests.HttpRequest;
import com.budjb.httprequests.HttpResponse;
import com.budjb.httprequests.MultiValuedMap;
import com.budjb.httprequests.converter.EntityConverterManager;
import com.budjb.httprequests.exception.EntityException;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.utils.Utils;

import okhttp3.MediaType;
import okhttp3.Response;

public class OkHttpHttpResponse extends HttpResponse {

	public OkHttpHttpResponse(HttpRequest httpRequest, EntityConverterManager converterManager, Response response)
			throws EntityException, IOException {
		super(converterManager, httpRequest, response.code(), convertHeaders(response), createEntity(response));
	}

	private static MultiValuedMap convertHeaders(Response response) {
		var mvMap = new MultiValuedMap();
		Ok.Calls.streamHeaders(response.headers()).forEach(ent -> {
			mvMap.add(ent.getKey(), ent.getValue());
		});
		return mvMap;
	}

	private static HttpEntity createEntity(Response response) throws EntityException, IOException {
		var responseBody = response.body();
		InputStream is = responseBody == null ? null : responseBody.byteStream();
		if (is == null)
			is = Utils.Bits.emptyInputStream();
		var contentTypeOp = Ok.Calls.getContentType(response);
		var contentTypeStr = contentTypeOp.map(Object::toString).orElse(null);
		var charsetStr = contentTypeOp.map(MediaType::charset).map(Object::toString).orElse(null);
		return new HttpEntity(is, contentTypeStr, charsetStr);
	}
}
