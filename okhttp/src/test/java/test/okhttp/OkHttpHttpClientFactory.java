package test.okhttp;

import com.budjb.httprequests.AbstractHttpClientFactory;
import com.budjb.httprequests.HttpClient;
import com.budjb.httprequests.converter.EntityConverterManager;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.okhttp.Ok;

public class OkHttpHttpClientFactory extends AbstractHttpClientFactory implements ProxyHttpClientFactory {

	protected OkHttpHttpClientFactory(EntityConverterManager entityConverterManager) {
		super(entityConverterManager);
	}

	@Override
	public HttpClient createHttpClient(Proxy proxy) {
		return new OkHttpHttpClient(Ok.Clients.get(proxy), this.getConverterManager());
	}

}
