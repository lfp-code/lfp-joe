package test.okhttp;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.Duration;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;

import com.budjb.httprequests.AbstractHttpClient;
import com.budjb.httprequests.HttpContext;
import com.budjb.httprequests.HttpEntity;
import com.budjb.httprequests.HttpRequest;
import com.budjb.httprequests.HttpResponse;
import com.budjb.httprequests.converter.EntityConverterManager;
import com.budjb.httprequests.filter.HttpClientFilterProcessor;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.ssl.SSLs;
import com.lfp.joe.okhttp.interceptor.UserAgentInterceptor;
import com.lfp.joe.utils.Utils;

import io.mikael.urlbuilder.UrlBuilder;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okio.BufferedSink;
import okio.Okio;

public class OkHttpHttpClient extends AbstractHttpClient {

	private final OkHttpClient baseClient;

	/**
	 * Constructor.
	 *
	 * @param entityConverterManager Converter manager.
	 */
	public OkHttpHttpClient(OkHttpClient baseClient, EntityConverterManager entityConverterManager) {
		super(entityConverterManager);
		this.baseClient = Objects.requireNonNull(baseClient);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected HttpResponse execute(HttpContext context, HttpEntity httpEntity,
			HttpClientFilterProcessor filterProcessor) throws IOException, GeneralSecurityException {
		var httpRequest = context.getRequest();
		var httpMethod = context.getMethod();
		var urlb = UrlBuilder.fromString(httpRequest.getUri());
		var queryParameters = httpRequest.getQueryParameters();
		if (queryParameters != null)
			for (var key : queryParameters.keySet()) {
				var values = Optional.ofNullable(queryParameters.get(key)).orElse(Collections.emptyList());
				for (var value : values)
					urlb = urlb.addParameter(key, value);
			}
		var rb = new Request.Builder().url(urlb.toUrl());
		var headers = httpRequest.getHeaders();
		if (headers != null)
			for (var name : headers.keySet()) {
				var values = Optional.ofNullable(headers.get(name)).orElse(Collections.emptyList());
				for (var value : values)
					rb = rb.header(name, value);
			}
		RequestBody requestBody;
		if (httpEntity == null)
			requestBody = null;
		else
			requestBody = new RequestBody() {

				@Override
				public void writeTo(BufferedSink sink) throws IOException {
					try (var is = httpEntity.getInputStream();) {
						Utils.Bits.copy(is, sink.outputStream());
					}
				}

				@Override
				public MediaType contentType() {
					var valueStream = Utils.Lots.stream(headers == null ? null : headers.get(Headers.CONTENT_TYPE))
							.filter(Utils.Strings::isNotBlank);
					var value = valueStream.findFirst().orElse(null);
					if (Utils.Strings.isBlank(value))
						value = httpEntity.getFullContentType();
					if (value == null)
						return null;
					return MediaType.parse(value);
				}
			};
		rb.method(httpMethod.toString(), requestBody);
		OkHttpClient client = createClient(httpRequest, filterProcessor);
		var response = client.newCall(rb.build()).execute();
		return new OkHttpHttpResponse(httpRequest, getConverterManager(), response);
	}

	private OkHttpClient createClient(HttpRequest request, HttpClientFilterProcessor filterProcessor)
			throws GeneralSecurityException {
		var builder = this.baseClient.newBuilder();
		if (!request.isSslValidated()) {
			builder = builder.hostnameVerifier((hostname, session) -> true);
			var trustManager = SSLs.trustAllManager();
			builder = builder.sslSocketFactory(SSLs.createSSLSocketFactory(trustManager), trustManager);
		}
		builder.connectTimeout(Duration.ofMillis(request.getConnectionTimeout()));
		builder.readTimeout(Duration.ofMillis(request.getReadTimeout()));
		if (!request.isFollowRedirects()) {
			builder.followRedirects(false);
			builder.followSslRedirects(false);
		}
		if (filterProcessor != null)
			builder.addInterceptor(chain -> {
				var okRequest = interceptRequest(filterProcessor, chain.request());
				return chain.proceed(okRequest);
			});
		var headers = request.getHeaders();
		if (headers != null) {
			var valueStream = Utils.Lots.stream(headers.get(Headers.USER_AGENT));
			valueStream = valueStream.filter(Utils.Strings::isNotBlank);
			var userAgentOp = valueStream.findFirst();
			if (userAgentOp.isPresent())
				builder.addInterceptor(new UserAgentInterceptor(userAgentOp.get()));
		}
		return builder.build();
	}

	private Request interceptRequest(HttpClientFilterProcessor filterProcessor, Request request) {
		if (filterProcessor == null || request == null)
			return request;
		var requestBody = request.body();
		if (requestBody == null)
			return request;
		var filteredRequestBody = new RequestBody() {

			@Override
			public void writeTo(BufferedSink sink) throws IOException {
				var os = sink.outputStream();
				os = filterProcessor.filterOutputStream(os);
				requestBody.writeTo(Okio.buffer(Okio.sink(os)));
			}

			@Override
			public MediaType contentType() {
				return requestBody.contentType();
			}
		};
		return request.newBuilder().method(request.method(), filteredRequestBody).build();
	}

}