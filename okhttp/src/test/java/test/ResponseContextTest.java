package test;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import org.jsoup.nodes.Document;

import com.google.gson.JsonElement;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.okhttp.call.ParseOptions;
import com.lfp.joe.okhttp.call.ResponseContext;
import com.lfp.joe.okhttp.call.ResponseContext;
import com.lfp.joe.utils.Bits;
import com.lfp.joe.utils.Utils;

import de.xn__ho_hia.storage_unit.StorageUnits;
import okhttp3.Request;

public class ResponseContextTest {

	public static void main(String[] args) throws IOException {
		String url = "https://yahoo.com/";
		var client = Ok.Clients.get();
		var call = client.newCall(new Request.Builder().url(url).build());
		ResponseContext<Void> response = ResponseContext.build(call.execute());
		ResponseContext<Optional<File>> flushed = response.flushToDisk();
		System.out.println(flushed.getParsedBody().orElse(null));
		System.out.println(flushed.getParsedBody().map(File::exists).orElse(null));
		System.out.println(flushed.getResponse().body().contentLength());
		System.out.println(flushed.getResponse().body().contentType());
		var flushedMem = flushed.flushToMemory();
		if (false) {
			ResponseContext<JsonElement> jsonParsed = flushedMem.parseJson();
			System.out.println(flushed.getParsedBody().map(File::exists).orElse(null));
			System.out.println(jsonParsed.getParsedBody());
			System.out.println(Ok.Calls.getContentLength(jsonParsed.getResponse()));
			System.out.println(Ok.Calls.getContentType(jsonParsed.getResponse()));
		} else if (true) {
			ResponseContext<String> textParsed = flushedMem.parseText(ParseOptions.builder()
					.inputStreamModifier(is -> Utils.Bits.limit(is, StorageUnits.bytes(10))).build());
			System.out.println(flushed.getParsedBody().map(File::exists).orElse(null));
			System.out.println(textParsed.getParsedBody());
			System.out.println(Ok.Calls.getContentLength(textParsed.getResponse()));
			System.out.println(Ok.Calls.getContentType(textParsed.getResponse()));
			System.out.println(Bits.from(textParsed.getResponse().body().byteStream()).length());
		} else {
			ResponseContext<Document> parsed = flushedMem.parseHtml();
			System.out.println(flushed.getParsedBody().map(File::exists).orElse(null));
			System.out.println(parsed.getParsedBody());
			System.out.println(Ok.Calls.getContentLength(parsed.getResponse()));
			System.out.println(Ok.Calls.getContentType(parsed.getResponse()));
		}
	}
}
