package test;
import java.io.OutputStream;

import java.io.IOException;
import java.time.Duration;

import org.apache.commons.lang3.time.StopWatch;

import com.google.common.io.CountingInputStream;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import de.xn__ho_hia.storage_unit.StorageUnits;
import okhttp3.Request;

public class SpeedTest {

	public static void main(String[] args) throws IOException {
		String url = "http://Promise.tele2.net/100MB.zip";
		Proxy proxy = null;
		StopWatch sw = new StopWatch();
		System.out.println("creating connection");
		Long count = Ok.Calls.execute(Ok.Clients.get(proxy), new Request.Builder().url(url).build(), r -> {
			Ok.Calls.validateStatusCode(r, 2);
			System.out.println("starting dl");
			sw.start();
			CountingInputStream inputStream = new CountingInputStream(r.body().byteStream());
			Utils.Bits.copy(inputStream, OutputStream.nullOutputStream());
			return inputStream.getCount();
		});
		Duration elapsed = Duration.ofMillis(sw.getTime());
		System.out.println(elapsed.toMillis());
		System.out.println(count);
		Float bytesPerSecond = (float) count / elapsed.getSeconds();
		System.out.println(bytesPerSecond);
		System.out.println(StorageUnits.bytes(bytesPerSecond.longValue()).asMegabyte());
	}

}