package com.lfp.joe.retry;

import java.time.Duration;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import com.evanlennick.retry4j.Status;
import com.evanlennick.retry4j.exception.RetriesExhaustedException;
import com.evanlennick.retry4j.exception.UnexpectedException;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

public class Retrys {

	public static <X, T extends Throwable> X execute(Integer maxNumberOfTries, Duration delayBetweenTries,
			ThrowingFunction<Integer, X, T> loader) throws T {
		var builder = RetryRequest.<X, T>builder();
		if (maxNumberOfTries != null)
			builder.maxNumberOfTries(maxNumberOfTries);
		if (delayBetweenTries != null)
			builder.delayBetweenTries(delayBetweenTries);
		builder.loader(loader);
		return execute(builder.build());
	}

	@SuppressWarnings("unchecked")
	public static <X, T extends Throwable> X execute(RetryRequest<X, T> retryRequest) throws T {
		Objects.requireNonNull(retryRequest);
		var callExecutor = retryRequest.callExecutor();
		var attempt = new AtomicInteger(-1);
		Set<Throwable> causes = new HashSet<>();
		Status<X> status = null;
		try {
			status = callExecutor.execute(() -> {
				try {
					return retryRequest.loader().apply(attempt.incrementAndGet());
				} catch (Throwable t) {
					causes.add(t);
					throw Utils.Exceptions.asException(t);
				}
			});
			return status.getResult();
		} catch (RetriesExhaustedException retriesExhaustedException) {
			causes.add(retriesExhaustedException.getCause());
		} catch (UnexpectedException unexpectedException) {
			causes.add(unexpectedException.getCause());
		}
		var causeStream = Streams.of(causes);
		if (status != null)
			causeStream = causeStream.append(Streams.of(status).map(Status::getLastExceptionThatCausedRetry));
		var causeIter = causeStream.nonNull().distinct().iterator();
		T cause = null;
		while (causeIter.hasNext())
			cause = (T) Throws.combine(cause, causeIter.next());
		throw cause;
	}
}
