package com.lfp.joe.retry;

import java.time.Duration;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.immutables.value.Value;

import com.evanlennick.retry4j.CallExecutor;
import com.evanlennick.retry4j.CallExecutorBuilder;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.Throws.ThrowingFunction;

@Value.Immutable
@ValueLFP.Style
public abstract class AbstractRetryRequest<X, T extends Throwable> {

	private static final Predicate<Exception> RETRY_ON_EXCEPTION_FILTER_DEFAULT = e -> true;

	public abstract ThrowingFunction<Integer, X, T> loader();

	public abstract OptionalInt maxNumberOfTries();

	public abstract Optional<Duration> delayBetweenTries();

	@Value.Default
	public Consumer<RetryConfigBuilderLFP> retryConfigModifier() {
		return b -> {};
	}

	@Value.Default
	public Consumer<CallExecutorBuilder<X>> callExecutorModifier() {
		return b -> {};
	}

	@Value.Default
	public Predicate<Exception> retryOnExceptionFilter() {
		return RETRY_ON_EXCEPTION_FILTER_DEFAULT;
	}

	@Value.Derived
	public CallExecutor<X> callExecutor() {
		RetryConfigBuilderLFP retryConfigBuilder;
		retryConfigBuilder = new RetryConfigBuilderLFP();
		retryConfigBuilder.withNoWaitBackoff();

		retryConfigBuilder.setValidationEnabled(false);
		{
			if (maxNumberOfTries().isPresent())
				retryConfigBuilder.withMaxNumberOfTries(maxNumberOfTries().getAsInt());
			if (delayBetweenTries().isPresent())
				retryConfigBuilder.withDelayBetweenTries(delayBetweenTries().get());
			if (RETRY_ON_EXCEPTION_FILTER_DEFAULT.equals(retryOnExceptionFilter()))
				retryConfigBuilder.retryOnAnyException();
			else
				retryConfigBuilder.retryOnCustomExceptionLogic(retryOnExceptionFilter()::test);
			retryConfigModifier().accept(retryConfigBuilder);
		}
		retryConfigBuilder.setValidationEnabled(true);
		CallExecutorBuilder<X> callExecutorBuilder = new CallExecutorBuilder<X>();
		callExecutorBuilder.config(retryConfigBuilder.build());
		{
			callExecutorModifier().accept(callExecutorBuilder);
		}
		return callExecutorBuilder.build();
	}

	@Value.Check
	AbstractRetryRequest<X, T> normalize() {
		if (maxNumberOfTries().isPresent() && maxNumberOfTries().getAsInt() == -1)
			return RetryRequest.copyOf(this).withMaxNumberOfTries(OptionalInt.empty());
		if (delayBetweenTries().isPresent() && delayBetweenTries().get().isZero())
			return RetryRequest.copyOf(this).withDelayBetweenTries(Optional.empty());
		return this;
	}

	public static <U, T extends Throwable> RetryRequest.Builder<U, T> builder(ThrowingFunction<Integer, U, T> loader) {
		var builder = RetryRequest.<U, T>builder();
		if (loader != null)
			builder.loader(loader);
		return builder;
	}

	public static void main(String[] args) {
		System.out.println("hi");
	}

}
