package com.lfp.joe.retry;

import com.evanlennick.retry4j.backoff.BackoffStrategy;
import com.evanlennick.retry4j.config.RetryConfigBuilder;

public class RetryConfigBuilderLFP extends RetryConfigBuilder {

	public RetryConfigBuilderLFP() {
		super();
	}

	public RetryConfigBuilderLFP(boolean validationEnabled) {
		super(validationEnabled);
	}

	@Override
	public RetryConfigBuilder withBackoffStrategy(BackoffStrategy backoffStrategy) {
		boolean validationEnabled = this.isValidationEnabled();
		if (validationEnabled)
			this.setValidationEnabled(false);
		try {
			return super.withBackoffStrategy(backoffStrategy);
		} finally {
			if (validationEnabled)
				this.setValidationEnabled(true);
		}
	}

}
