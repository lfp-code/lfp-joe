package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.lfp.joe.retry.RetryRequest;
import com.lfp.joe.retry.Retrys;

public class RetrysTest {

	public static void main(String[] args) throws IOException {
		var request = RetryRequest.<InputStream, IOException>builder(nil -> {
			return new FileInputStream(new File("."));
		}).maxNumberOfTries(100).build();
		var result = Retrys.execute(request);
		System.out.println(result.available());
	}
}
