package com.lfp.joe.utils.function;

import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Functions;

public interface RateLimiter {

	public static final RateLimiter NOOP = new RateLimiter() {

		@Override
		public void acquire(long numberOfPermits) throws InterruptedException {}

		@Override
		public boolean tryAcquire(long numberOfPermits) {
			return true;
		}

		@Override
		public Future<Void> acquireAsync(long numberOfPermits) {
			return CompletableFuture.completedFuture(null);
		}

		@Override
		public Future<Boolean> tryAcquireAsync(long numberOfPermits) {
			return CompletableFuture.completedFuture(true);
		}
	};

	default void acquire() throws InterruptedException {
		acquire(1);
	}

	void acquire(long numberOfPermits) throws InterruptedException;

	default boolean tryAcquire() {
		return tryAcquire(1);
	}

	boolean tryAcquire(long numberOfPermits);

	default Future<Void> acquireAsync() {
		return acquireAsync(1);
	}

	Future<Void> acquireAsync(long numberOfPermits);

	default Future<Boolean> tryAcquireAsync() {
		return tryAcquireAsync(1);
	}

	Future<Boolean> tryAcquireAsync(long numberOfPermits);

	public static RateLimiter create(long permits, Duration interval) {
		return new RateLimiter.Impl(permits, interval);
	}

	public static class Impl implements RateLimiter {

		private DelayQueue<Token> delayedQueue = new DelayQueue<>();
		private Duration interval;

		public Impl(long permits, Duration interval) {
			this.interval = Objects.requireNonNull(interval);
			Validate.isTrue(permits > 0, "at least one permit required");
			for (long i = 0; i < permits; i++)
				delayedQueue.add(new Token(Duration.ZERO));
		}

		@Override
		public void acquire(long numberOfPermits) throws InterruptedException {
			for (long i = 0; i < numberOfPermits; i++) {
				boolean takeSuccess = false;
				try {
					this.delayedQueue.take();
					takeSuccess = true;
				} finally {
					if (takeSuccess)
						this.delayedQueue.add(new Token(this.interval));
				}
			}
		}

		@Override
		public boolean tryAcquire(long numberOfPermits) {
			for (long i = 0; i < numberOfPermits; i++) {
				boolean takeSuccess = false;
				try {
					var token = this.delayedQueue.poll();
					if (token == null)
						return false;
					takeSuccess = true;
				} finally {
					if (takeSuccess)
						this.delayedQueue.add(new Token(this.interval));
				}
			}
			return true;
		}

		@Override
		public Future<Void> acquireAsync(long numberOfPermits) {
			// avoid this, very inefficient
			return Threads.Pools.centralPool().submit(() -> {
				acquire(numberOfPermits);
				return null;
			});
		}

		@Override
		public Future<Boolean> tryAcquireAsync(long numberOfPermits) {
			// avoid this, very inefficient
			return Threads.Pools.centralPool().submit(() -> {
				return Functions.unchecked(() -> tryAcquire(numberOfPermits));
			});
		}

		private static class Token implements Delayed {

			/** the point in time at which this token can be consumed */
			private long earliestExecution;

			/**
			 * Create a new token that will be consumable in
			 * <code>earlistesExecutionInMs</code> milliseconds.
			 *
			 * @param earlistesExecutionInMs the time in milliseconds one has to wait until
			 *                               this token can be consumed.
			 */
			public Token(Duration interval) {
				this.earliestExecution = System.currentTimeMillis() + interval.toMillis();
			}

			@Override
			public int compareTo(Delayed o) {
				return Long.valueOf(this.earliestExecution).compareTo(((Token) o).earliestExecution);
			}

			@Override
			public long getDelay(TimeUnit unit) {
				long delay = this.earliestExecution - System.currentTimeMillis();
				delay = Math.max(0, delay);
				return unit.convert(delay, TimeUnit.MILLISECONDS);
			}

		}

	}

}
