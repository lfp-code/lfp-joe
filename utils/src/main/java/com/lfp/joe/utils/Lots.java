package com.lfp.joe.utils;

import java.io.InputStream;
import java.lang.reflect.Array;
import java.time.Duration;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.Spliterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntSupplier;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.BaseStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.threadly.concurrent.wrapper.SubmitterExecutorAdapter;

import com.googlecode.concurrentlinkedhashmap.ConcurrentLinkedHashMap;
import com.lfp.joe.core.function.ImmutableEntry;
import com.lfp.joe.core.function.Muto;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.lot.Lot;
import com.lfp.joe.stream.StreamContextAccessor;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.function.ComparableWrapper;
import com.lfp.joe.utils.lots.Buffer;
import com.lfp.joe.utils.lots.LotLoader;
import com.lfp.joe.utils.lots.collection.IteratorCollection;
import com.lfp.joe.utils.lots.stream.StreamInputStream;

import one.util.streamex.AbstractStreamEx;
import one.util.streamex.DoubleStreamEx;
import one.util.streamex.EntryStream;
import one.util.streamex.IntStreamEx;
import one.util.streamex.LongStreamEx;
import one.util.streamex.StreamEx;

@SuppressWarnings("unchecked")
public class Lots {

	protected Lots() {}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	/*
	 *
	 * stream stuff
	 *
	 */

	@SuppressWarnings({ "unchecked", "rawtypes" })

	public static Optional<StreamEx<Object>> tryStream(Object value) {
		if (value == null)
			return Optional.empty();
		final StreamEx<Object> stream;
		if (value instanceof BaseStream)
			stream = stream((BaseStream) value);
		else if (value instanceof Enumeration)
			stream = stream((Enumeration) value);
		else if (value instanceof Spliterator)
			stream = stream((Spliterator) value);
		else if (value instanceof Iterator)
			stream = stream((Iterator) value);
		else if (value instanceof Iterable)
			stream = stream((Iterable) value);
		else if (value.getClass().isArray()) {
			var length = Array.getLength(value);
			stream = StreamEx.produce(new Predicate<Consumer<? super Object>>() {

				private int index = -1;

				@Override
				public boolean test(Consumer<? super Object> action) {
					index++;
					if (index >= length)
						return false;
					action.accept(Array.get(value, index));
					return true;
				}
			});
		} else
			stream = null;
		return Optional.ofNullable(stream);
	}

	// primitive streams

	public static IntStreamEx stream(int value) {
		return IntStreamEx.of(value);
	}

	public static IntStreamEx stream(int... values) {
		return values == null || values.length == 0 ? IntStreamEx.empty() : IntStreamEx.of(values);
	}

	public static LongStreamEx stream(long value) {
		return LongStreamEx.of(value);
	}

	public static LongStreamEx stream(long... values) {
		return values == null || values.length == 0 ? LongStreamEx.empty() : LongStreamEx.of(values);
	}

	public static StreamEx<Character> stream(char value) {
		return StreamEx.of(value);
	}

	public static StreamEx<Character> stream(char... values) {
		return values == null || values.length == 0 ? StreamEx.empty() : IntStreamEx.of(values).mapToObj(v -> (char) v);
	}

	public static DoubleStreamEx stream(double value) {
		return DoubleStreamEx.of(value);
	}

	public static DoubleStreamEx stream(double... values) {
		return values == null || values.length == 0 ? DoubleStreamEx.empty() : DoubleStreamEx.of(values);
	}

	// streams

	/**
	 * @param <T> the type of stream elements
	 */
	public static <T> StreamEx<T> stream() {
		return StreamEx.empty();
	}

	/**
	 * @param <T> the type of stream elements
	 */
	public static <T> StreamEx<T> stream(T element) {
		return StreamEx.of(element);
	}

	/**
	 * @param <T> the type of stream elements
	 */
	@SafeVarargs
	public static <T> StreamEx<T> stream(T... elements) {
		if (elements == null || elements.length == 0)
			return StreamEx.empty();
		return StreamEx.of(elements);
	}

	/**
	 * @param <X> the type of stream elements
	 */
	public static <X> StreamEx<X> stream(Enumeration<? extends X> input) {
		if (input == null)
			return StreamEx.empty();
		return Streams.of(input);
	}

	/**
	 * @param <X> the type of stream elements
	 */
	public static <X> StreamEx<X> stream(Iterator<? extends X> input) {
		if (input == null)
			return StreamEx.empty();
		return StreamEx.of(input);
	}

	/**
	 * @param <X> the type of stream elements
	 */
	public static <X> StreamEx<X> stream(Spliterator<? extends X> input) {
		if (input == null)
			return StreamEx.empty();
		return StreamEx.of(input);
	}

	/**
	 * @param <X> the type of stream elements
	 */

	public static <X> StreamEx<X> stream(Iterable<? extends X> input) {
		if (input == null)
			return StreamEx.empty();
		if (input instanceof Stream)
			return stream((Stream<X>) input);
		return stream(input.spliterator());
	}

	/**
	 * @param <X> the type of stream elements
	 */

	public static <X> StreamEx<X> stream(BaseStream<? extends X, ?> input) {
		if (input == null)
			return StreamEx.empty();
		if (input instanceof StreamEx)
			return (StreamEx<X>) input;
		if (input instanceof Stream)
			return StreamEx.of((Stream<X>) input);
		var iter = input.iterator();
		return stream(iter);
	}

	/**
	 * @param <X> the type of stream elements
	 */
	@Deprecated
	public static <X> StreamEx<X> stream(Lot<? extends X> input) {
		if (input == null)
			return StreamEx.empty();
		StreamEx<X> stream = StreamEx.of(input.spliterator());
		return stream.onClose(input::close);
	}

	/**
	 * @param <X> the type of stream elements
	 */

	public static <X> StreamEx<X> stream(StreamEx<? extends X> input) {
		if (input == null)
			return StreamEx.empty();
		return (StreamEx<X>) input;
	}

	/**
	 * @param <X> the type of stream elements
	 */
	@Deprecated
	public static <X> StreamEx<X> stream(Function<LotLoader<X>, X> loader) {
		if (loader == null)
			return StreamEx.empty();
		Iterator<X> iter = newIterator(loader);
		return stream(iter);
	}

	public static <K, V> EntryStream<K, V> stream(Map<? extends K, ? extends V> map) {
		if (map == null)
			return EntryStream.empty();
		return (EntryStream<K, V>) EntryStream.of(map);
	}

	public static <K, V> EntryStream<K, V> stream(Iterable<? extends K> keyIble,
			Function<K, Iterable<? extends V>> valueFunction) {
		if (keyIble == null)
			return EntryStream.empty();
		StreamEx<K> keyStream = stream(keyIble);
		return keyStream.map(k -> {
			Iterable<? extends V> values = valueFunction == null ? null : valueFunction.apply(k);
			if (values == null)
				return null;
			EntryStream<K, V> estream = stream(values).mapToEntry(nil -> k).invert().mapValues(Function.identity());
			return estream;
		}).nonNull().chain(Lots::flatMap).chain(Lots::streamEntries);
	}

	public static <K, V> EntryStream<K, V> streamMultimap(Map<? extends K, ? extends Iterable<V>> multiMap) {
		if (multiMap == null)
			return EntryStream.empty();
		return stream(multiMap.keySet(), multiMap::get);
	}

	// stream entries

	public static <K, V> EntryStream<K, V> streamEntries(Iterator<? extends Entry<K, V>> input) {
		return streamEntries(input == null ? null : stream(input));
	}

	public static <K, V> EntryStream<K, V> streamEntries(Spliterator<? extends Entry<K, V>> input) {
		return streamEntries(input == null ? null : stream(input));
	}

	@Deprecated
	public static <K, V> EntryStream<K, V> streamEntries(Lot<? extends Entry<K, V>> input) {
		return streamEntries(input == null ? null : stream(input));
	}

	public static <K, V> EntryStream<K, V> streamEntries(BaseStream<? extends Entry<K, V>, ?> input) {
		return streamEntries(input == null ? null : stream(input));
	}

	public static <K, V> EntryStream<K, V> streamEntries(Iterable<? extends Entry<K, V>> input) {
		return streamEntries(input == null ? null : stream(input));
	}

	public static <K, V> EntryStream<K, V> streamEntries(StreamEx<? extends Entry<K, V>> input) {
		if (input == null)
			return EntryStream.empty();
		return input.mapToEntry(ent -> ent == null ? null : ent.getKey(), ent -> ent == null ? null : ent.getValue());
	}

	public static <K, V> Function<? super AbstractStreamEx<? extends Entry<K, V>, ?>, EntryStream<K, V>> entries() {
		return s -> {
			if (s == null)
				return EntryStream.empty();
			if (s instanceof EntryStream)
				return (EntryStream<K, V>) s;
			if (s instanceof StreamEx)
				return ((StreamEx<? extends Entry<K, V>>) s).mapToEntry(Entry::getKey, Entry::getValue);
			return EntryStream.of(s);
		};
	}

	// flatMapAll

	public static StreamEx<Object> flatMapAll(Object input) {
		if (input == null)
			return StreamEx.empty();
		var streamOp = tryStream(input);
		if (streamOp.isEmpty())
			return StreamEx.of(input);
		var streams = streamOp.get().map(v -> flatMapAll(v));
		return flatMap(streams);
	}

	// flatMapIterators

	public static <X> StreamEx<X> flatMapIterators(Iterator<? extends Iterator<? extends X>> inputs) {
		return flatMap(inputs == null ? null : stream(inputs).map(Lots::stream));
	}

	public static <X> StreamEx<X> flatMapIterators(Spliterator<? extends Iterator<? extends X>> inputs) {
		return flatMap(inputs == null ? null : stream(inputs).map(Lots::stream));
	}

	public static <X> StreamEx<X> flatMapIterators(Iterable<? extends Iterator<? extends X>> inputs) {
		return flatMap(inputs == null ? null : stream(inputs).map(Lots::stream));
	}

	// flatMapSpliterators

	public static <X> StreamEx<X> flatMapSpliterators(Iterator<? extends Spliterator<? extends X>> inputs) {
		return flatMap(inputs == null ? null : stream(inputs).map(Lots::stream));
	}

	public static <X> StreamEx<X> flatMapSpliterators(Spliterator<? extends Spliterator<? extends X>> inputs) {
		return flatMap(inputs == null ? null : stream(inputs).map(Lots::stream));
	}

	public static <X> StreamEx<X> flatMapSpliterators(Iterable<? extends Spliterator<? extends X>> inputs) {
		return flatMap(inputs == null ? null : stream(inputs).map(Lots::stream));
	}

	// flatMapIterables

	public static <X> StreamEx<X> flatMapIterables(Iterator<? extends Iterable<? extends X>> inputs) {
		return flatMap(inputs == null ? null : stream(inputs).map(Lots::stream));
	}

	public static <X> StreamEx<X> flatMapIterables(Spliterator<? extends Iterable<? extends X>> inputs) {
		return flatMap(inputs == null ? null : stream(inputs).map(Lots::stream));
	}

	public static <X> StreamEx<X> flatMapIterables(Iterable<? extends Iterable<? extends X>> inputs) {
		return flatMap(inputs == null ? null : stream(inputs).map(Lots::stream));
	}

	// flatMap

	public static <X> StreamEx<X> flatMap(Iterator<? extends BaseStream<? extends X, ?>> inputs) {
		return flatMap(inputs == null ? null : stream(inputs));
	}

	public static <X> StreamEx<X> flatMap(Spliterator<? extends BaseStream<? extends X, ?>> inputs) {
		return flatMap(inputs == null ? null : stream(inputs));
	}

	public static <X> StreamEx<X> flatMap(Iterable<? extends BaseStream<? extends X, ?>> inputs) {
		return flatMap(inputs == null ? null : stream(inputs));
	}

	public static <X> StreamEx<X> flatMap(BaseStream<? extends BaseStream<? extends X, ?>, ?> inputs) {
		return flatMap(inputs == null ? null : stream(inputs));
	}

	public static <X> StreamEx<X> flatMap(StreamEx<? extends BaseStream<? extends X, ?>> inputs) {
		if (inputs == null)
			return StreamEx.empty();
		return inputs.chain(Streams.flatMap());
	}

	// flatMapEntries

	public static <K, V> EntryStream<K, V> flatMapEntries(
			Iterator<? extends BaseStream<? extends Entry<K, V>, ?>> inputs) {
		return flatMapEntries(inputs == null ? null : stream(inputs));
	}

	public static <K, V> EntryStream<K, V> flatMapEntries(
			Spliterator<? extends BaseStream<? extends Entry<K, V>, ?>> inputs) {
		return flatMapEntries(inputs == null ? null : stream(inputs));
	}

	public static <K, V> EntryStream<K, V> flatMapEntries(
			Iterable<? extends BaseStream<? extends Entry<K, V>, ?>> inputs) {
		if (inputs == null)
			return EntryStream.empty();
		return flatMap(inputs).chain(Lots::streamEntries);
	}

	public static Integer validateBatchSize(Number batchSize) {
		return validateBatchSize(batchSize, false);
	}

	public static Integer validateBatchSize(Number batchSize, boolean positiveOnly) {
		Integer batchSizeInt = batchSize == null ? null : batchSize.intValue();
		boolean valid = batchSizeInt != null && ((!positiveOnly && batchSizeInt == -1) || batchSizeInt > 0);
		Validate.isTrue(valid, "invalid batchSize:%s", batchSize);
		return batchSizeInt;
	}

	public static boolean close(Iterator<?> iterator) {
		return close(iterator, false);
	}

	public static boolean close(Iterable<?> ible) {
		return close(ible, false);
	}

	private static boolean close(Object input, boolean includeAutoCloseable) {
		if (input == null)
			return false;
		if (input instanceof Lot) {
			((Lot<?>) input).close();
			return true;
		}
		if (input instanceof BaseStream) {
			((BaseStream<?, ?>) input).close();
			return true;
		}
		if (includeAutoCloseable && input instanceof AutoCloseable) {
			Functions.unchecked(() -> ((AutoCloseable) input).close());
			return true;
		}
		return false;
	}

	public static <X> StreamEx<List<X>> buffer(Iterable<? extends X> input, Number flushSize) {
		return Streams.of(input).chain(Streams.buffer(flushSize));
	}

	public static <X> StreamEx<List<X>> buffer(Iterable<? extends X> input, Duration flushInterval) {
		return Streams.of(input).chain(Streams.buffer(flushInterval));
	}

	public static <X> StreamEx<List<X>> buffer(Iterable<? extends X> input, Number flushSize, Duration flushInterval) {
		return Streams.of(input).chain(Streams.buffer(flushSize, flushInterval));
	}

	public static <X> StreamEx<List<X>> buffer(Iterable<? extends X> input,
			Function<Buffer<X>, Boolean> batchFlushFunction) {
		if (input == null)
			return StreamEx.empty();
		if (batchFlushFunction == null)
			return stream(input).map(Arrays::asList);
		var bufferStream = StreamEx.produce(new Predicate<Consumer<? super List<X>>>() {

			private Spliterator<? extends X> spliterator;
			private Buffer<X> buffer;
			private long index = -1;

			@Override
			public boolean test(Consumer<? super List<X>> callback) {
				if (spliterator == null)
					spliterator = input.spliterator();
				var advance = spliterator.tryAdvance(v -> {
					if (buffer == null) {
						index++;
						buffer = new Buffer<>(index);
					}
					buffer.add(v);
				});
				if (!advance) {
					if (buffer != null) {
						callback.accept(buffer);
						buffer = null;
					}
					return false;
				}
				if (buffer == null) {
					index++;
					buffer = new Buffer<>(index);
				}
				if (Boolean.TRUE.equals(batchFlushFunction.apply(buffer))) {
					callback.accept(buffer);
					buffer = null;
				}
				return true;
			}
		});
		bufferStream = withContext(bufferStream, input);
		return bufferStream;

	}

	public static <T> Function<? super AbstractStreamEx<? extends T, ?>, StreamEx<List<T>>> buffer(Number size,
			Duration flushInterval) {
		return s -> {
			StreamEx<T> stream = s != null ? s.map(v -> (T) v) : null;
			return buffer(stream, size, flushInterval);
		};
	}

	public static <X> Entry<List<X>, StreamEx<X>> fetch(Iterable<? extends X> ible, long limit) {
		Validate.isTrue(limit >= -1, "invalid limit:%s", limit);
		if (limit == 0)
			return fetch(ible, null);
		if (limit == -1)
			return fetch(ible, (i, v) -> true);
		return fetch(ible, (i, v) -> {
			return i < limit;
		});
	}

	public static <X> Entry<List<X>, StreamEx<X>> fetch(Iterable<? extends X> ible, BiPredicate<Long, X> predicate) {
		if (ible == null)
			return entry(List.of(), StreamEx.empty());
		if (predicate == null)
			return entry(List.of(), stream(ible));
		var stream = stream(ible);
		var streamContext = StreamContextAccessor.of(stream);
		var iterator = stream.iterator();
		long index = -1;
		List<X> list = null;
		Stream<X> prependStream = null;
		while (iterator.hasNext()) {
			var next = iterator.next();
			index++;
			if (!predicate.test(index, next)) {
				prependStream = Stream.of(next);
				break;
			}
			if (list == null)
				list = new ArrayList<X>();
			list.add(next);
		}
		if (list == null)
			list = List.of();
		else
			list = unmodifiable(list);
		StreamEx<X> resultStream = streamContext.applyToStream(Streams.of(iterator));
		if (prependStream != null)
			resultStream = resultStream.prepend(prependStream);
		return entry(list, resultStream);
	}

	public static <X> StreamEx<X> requireNonEmpty(Iterable<X> ible) {
		var hasNext = hasNext(ible);
		if (!Boolean.TRUE.equals(hasNext.getKey()))
			throw new NoSuchElementException("stream is empty");
		return hasNext.getValue();
	}

	public static <X> StreamEx<X> defer(Supplier<Iterable<X>> ibleSupplier) {
		if (ibleSupplier == null)
			return StreamEx.empty();
		var stream = StreamEx.of(ibleSupplier).nonNull().map(Supplier::get).chain(v -> {
			return Utils.Lots.flatMapIterables(v);
		});
		return stream;
	}

	public static <X> StreamEx<X> timeout(Iterable<? extends X> input, Duration timeout) {
		return timeout(input, timeout, null);
	}

	public static <X> StreamEx<X> timeout(Iterable<? extends X> input, Duration timeout,
			ThrowingFunction<TimeoutException, Iterable<X>, TimeoutException> onTimeout) {
		return timeout(input, timeout, onTimeout, null);
	}

	public static <X> StreamEx<X> timeout(Iterable<? extends X> input, Duration timeout,
			ThrowingFunction<TimeoutException, Iterable<X>, TimeoutException> onTimeout, Executor executor) {
		if (input == null)
			return StreamEx.empty();
		StreamEx<X> stream = stream(input);
		if (timeout == null)
			return stream;
		var inputIter = stream.iterator();
		var submitterExecutor = Optional.ofNullable(executor)
				.map(SubmitterExecutorAdapter::adaptExecutor)
				.orElseGet(Threads.Pools::centralPool);
		var resultStream = Streams.<X>produce(action -> {
			var opFuture = submitterExecutor.submit(() -> {
				if (!inputIter.hasNext())
					return null;
				return Optional.ofNullable(inputIter.next());
			});
			try {
				var op = opFuture.get(timeout.toMillis(), TimeUnit.MILLISECONDS);
				if (op == null)
					return false;
				action.accept(op.orElse(null));
				return true;
			} catch (Throwable t1) {
				if (t1 instanceof TimeoutException && onTimeout != null) {
					try {
						Streams.of(onTimeout.apply((TimeoutException) t1)).forEach(action);
						return false;
					} catch (Throwable t2) {
						t1 = Throws.combine(t2);
					}
				}
				throw Throws.unchecked(t1);
			} finally {
				opFuture.cancel(true);
			}
		});
		return Utils.Lots.withContext(resultStream, stream);
	}

	public static <X> Optional<X> advanceOrNull(Spliterator<X> spliterator) {
		if (spliterator == null)
			return null;
		Muto<X> nextRef = Muto.create();
		if (!spliterator.tryAdvance(nextRef::set))
			return null;
		return Optional.ofNullable(nextRef.get());
	}

	public static <X> X one(Iterable<? extends X> ible) {
		X result;
		if (ible == null)
			result = null;
		else
			result = stream(ible).reduce((f, s) -> {
				throw new IllegalArgumentException("stream produced multiple values");
			}).orElse(null);
		Objects.requireNonNull(result, () -> "stream did not produce single value");
		return result;
	}

	public static <X> EntryStream<Long, X> indexed(Stream<? extends X> stream) {
		if (stream == null)
			return EntryStream.empty();
		return stream(stream).zipWith(LongStream.range(0, Long.MAX_VALUE)).invert().mapValues(Function.identity());
	}

	public static <X> EntryStream<Boolean, X> hasNextStream(Iterable<? extends X> ible) {
		return stream(ible).map(v -> entry(true, v))
				.mapLast(ent -> ent.withKey(false))
				.mapToEntry(Entry::getKey, Entry::getValue);
	}

	public static <X> ImmutableEntry<Boolean, StreamEx<X>> hasNext(Iterable<? extends X> ible) {
		if (ible == null)
			return entry(false, StreamEx.empty());
		var stream = stream(ible);
		var iterator = stream.iterator();
		boolean hasNext = iterator.hasNext();
		StreamEx<X> resultStream = stream(iterator);
		resultStream = withContext(resultStream, ible);
		return entry(hasNext, resultStream);
	}

	public static <X> InputStream toInputStream(Iterable<? extends X> ible, Function<X, byte[]> serializeFunction) {
		if (ible == null)
			return Bits.emptyInputStream();
		Objects.requireNonNull(serializeFunction);
		Stream<byte[]> barrStream = stream(ible).map(v -> serializeFunction.apply(v));
		return new StreamInputStream(barrStream);
	}

	public static <X> StreamEx<X> onComplete(Iterable<? extends X> ible, Runnable listener) {
		StreamEx<X> stream = stream(ible);
		if (listener == null)
			return stream;
		Runnable listenerWrap = new Runnable() {

			private boolean listenerRun;

			@Override
			public void run() {
				if (!listenerRun)
					synchronized (this) {
						if (!listenerRun) {
							listenerRun = true;
							listener.run();
						}
					}
			}
		};
		stream = stream.onClose(listenerWrap);
		stream = stream.append(StreamEx.produce(action -> {
			listenerWrap.run();
			return false;
		}));
		return stream;
	}

	public static <X> StreamEx<X> onClose(Iterable<? extends X> result, BaseStream<?, ?>... streams) {
		return onClose(result, stream(streams).nonNull().map(v -> (Runnable) v::close).toArray(Runnable[]::new));
	}

	public static <X> StreamEx<X> onClose(Iterable<? extends X> result, StreamEx<?>... streams) {
		return onClose(result, (BaseStream<?, ?>[]) streams);
	}

	public static <X> StreamEx<X> onClose(Iterable<? extends X> result, Iterable<?>... ibles) {
		return onClose(result, stream(ibles).nonNull().map(v -> (Runnable) () -> close(v)).toArray(Runnable[]::new));
	}

	public static <X> StreamEx<X> onClose(Iterable<? extends X> result, Runnable... tasks) {
		StreamEx<X> resultEx = stream(result);
		if (tasks == null || tasks.length == 0)
			return resultEx;
		for (var task : tasks) {
			if (task == null)
				continue;
			resultEx = resultEx.onClose(task::run);
		}
		return resultEx;
	}

	public static <S extends AbstractStreamEx<?, ?>> S withContext(S stream, Iterable<?> iterable) {
		return withContext(stream, iterable, false);
	}

	@SuppressWarnings({ "unchecked", "rawtypes", "resource" })
	public static <S extends AbstractStreamEx<?, ?>> S withContext(S stream, Iterable<?> iterable,
			boolean disableForkJoinPoolLink) {
		Objects.requireNonNull(stream);
		if (iterable instanceof Stream) {
			Stream other = (Stream) iterable;
			if (other.isParallel()) {
				var fjp = !disableForkJoinPoolLink ? StreamContextAccessor.of(other).tryGetForkJoinPool().orElse(null)
						: null;
				if (fjp != null)
					stream = (S) stream.parallel(fjp);
				else
					stream = (S) stream.parallel();
			} else
				stream = (S) stream.sequential();

		}
		stream = (S) stream.onClose(() -> close(iterable));
		return stream;
	}

	public static <X> Collection<X> asCollection(Iterable<? extends X> input) {
		return asCollection(input, null);
	}

	public static <X> Collection<X> asCollection(Iterable<? extends X> input, IntSupplier sizeSupplier) {
		if (input == null)
			return List.of();
		if (input instanceof Collection)
			return (Collection<X>) input;
		if (input instanceof Iterator)
			return asCollection((Iterator<X>) input, sizeSupplier);
		return asCollection(input.iterator(), sizeSupplier);
	}

	public static <X> Collection<X> asCollection(Iterator<? extends X> input) {
		return asCollection(input, null);
	}

	public static <X> Collection<X> asCollection(Iterator<? extends X> input, IntSupplier sizeSupplier) {
		if (input == null)
			return List.of();
		if (input instanceof Collection)
			return (Collection<X>) input;
		return new IteratorCollection<X>(input, sizeSupplier);

	}

	public static boolean equals(Stream<?> stream1, Stream<?> stream2) {
		var iter1 = stream1 == null ? Collections.emptyIterator() : stream1.iterator();
		var iter2 = stream2 == null ? Collections.emptyIterator() : stream2.iterator();
		while (iter1.hasNext() && iter2.hasNext())
			if (!Objects.equals(iter1.next(), iter2.next()))
				return false;
		return !iter1.hasNext() && !iter2.hasNext();
	}

	/*
	 *
	 * enum stuff
	 *
	 */

	public static <X> Enumeration<X> enumerate(Iterable<X> ible) {
		return enumerate(ible == null ? null : ible.iterator());
	}

	public static <X> Enumeration<X> enumerate(Stream<X> stream) {
		return enumerate(stream == null ? null : stream.iterator());
	}

	public static <X> Enumeration<X> enumerate(StreamEx<X> stream) {
		return enumerate(stream == null ? null : stream.iterator());
	}

	public static <X> Enumeration<X> enumerate(Iterator<X> iterator) {
		return new Enumeration<X>() {

			@Override
			public boolean hasMoreElements() {
				return iterator == null ? false : iterator.hasNext();
			}

			@Override
			public X nextElement() {
				if (iterator == null)
					throw new NoSuchElementException();
				return iterator.next();
			}
		};
	}

	/*
	 *
	 * iterator stuff
	 *
	 */
	@Deprecated
	public static <X> Iterator<X> newIterator(Function<LotLoader<X>, X> loader) {
		if (loader == null)
			return Collections.emptyIterator();
		return new LotLoader.Abs<X>() {

			@Override
			protected X computeNext(long index) {
				return loader.apply(this);
			}
		};
	}

	/*
	 *
	 * list stuff
	 *
	 */

	public static <X> List<X> reverseView(List<X> list) {
		if (list == null)
			return Collections.emptyList();
		return new AbstractList<X>() {

			@Override
			public X get(int i) {
				return list.get(list.size() - i - 1);
			}

			@Override
			public int size() {
				return list.size();
			}

		};
	}

	/*
	 *
	 * map stuff
	 *
	 */
	public static <K, V> ImmutableEntry<K, V> entry(K key, V value) {
		return ImmutableEntry.of(key, value);
	}

	public static <K, V> ConcurrentLinkedHashMap<K, V> newConcurrentLinkedHashMap() {
		return new ConcurrentLinkedHashMap.Builder<K, V>().maximumWeightedCapacity(Long.MAX_VALUE - Integer.MAX_VALUE)
				.build();
	}

	public static <E> Set<E> newConcurrentHashSet() {
		return newConcurrentHashSet(null);
	}

	public static <E> Set<E> newConcurrentHashSet(Iterable<? extends E> iterable) {
		return newConcurrentHashSet(null, iterable);
	}

	private static <E> Set<E> newConcurrentHashSet(ConcurrentMap<E, Boolean> backingMap,
			Iterable<? extends E> iterable) {
		if (backingMap == null)
			backingMap = new ConcurrentHashMap<E, Boolean>();
		Set<E> set = Collections.newSetFromMap(backingMap);
		if (iterable != null)
			if (iterable instanceof Collection)
				set.addAll((Collection<E>) iterable);
			else
				iterable.forEach(set::add);
		return set;
	}

	/*
	 *
	 * sort stuff
	 *
	 */

	private static final Comparator<?> COMPARATOR_NO_OP = (v1, v2) -> 0;

	public static <X> Comparator<X> comparatorNoOp() {
		return (Comparator<X>) COMPARATOR_NO_OP;
	}

	public static <X> Comparator<X> comparatorNaturalOrder() {
		return (Comparator<X>) Comparator.naturalOrder();
	}

	public static <X> Comparator<X> comparatorNullsLast() {
		return Comparator.nullsLast(comparatorNoOp());
	}

	public static <X> Comparator<X> comparatorNullsFirst() {
		return Comparator.nullsFirst(comparatorNoOp());
	}

	public static <X, C> Function<X, ComparableWrapper<X>> comparableFactory(Function<X, C> keyExtractor) {
		Comparator<X> comparator;
		if (keyExtractor == null)
			comparator = null;
		else
			comparator = Comparator.comparing(keyExtractor, (v1, v2) -> {
				return new CompareToBuilder().append(v1, v2).toComparison();
			});
		return value -> new ComparableWrapper<X>(value, comparator);
	}

	public static <X> Function<X, ComparableWrapper<X>> comparableFactory(Comparator<X> comparator) {
		return value -> new ComparableWrapper<X>(value, comparator);
	}

	/*
	 *
	 * unmodifiable stuff
	 *
	 */

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <K, V> Map<K, V> unmodifiable(Map<? extends K, ? extends V> input) {
		if (input == null)
			return Map.of();
		if (input instanceof NavigableMap)
			return Collections.unmodifiableMap((NavigableMap) input);
		if (input instanceof SortedMap)
			return Collections.unmodifiableMap((SortedMap) input);
		return Collections.unmodifiableMap(input);
	}

	public static <K, V> SortedMap<K, V> unmodifiable(SortedMap<? extends K, ? extends V> input) {
		if (input == null)
			input = Collections.emptySortedMap();
		return (SortedMap<K, V>) Collections.unmodifiableSortedMap(input);
	}

	public static <K, V> NavigableMap<K, V> unmodifiable(NavigableMap<? extends K, ? extends V> input) {
		if (input == null)
			input = Collections.emptyNavigableMap();
		return (NavigableMap<K, V>) Collections.unmodifiableSortedMap(input);
	}

	public static <X> Collection<X> unmodifiable(Collection<? extends X> input) {
		if (input == null)
			return List.of();
		return Collections.unmodifiableCollection(input);
	}

	public static <X> Set<X> unmodifiable(Set<? extends X> input) {
		if (input == null)
			return Set.of();
		return Collections.unmodifiableSet(input);
	}

	public static <X> List<X> unmodifiable(List<? extends X> input) {
		if (input == null)
			return List.of();
		return Collections.unmodifiableList(input);
	}

	public static <X> SortedSet<X> unmodifiable(SortedSet<? extends X> input) {
		if (input == null)
			input = Collections.emptySortedSet();
		return (SortedSet<X>) Collections.unmodifiableSortedSet(input);
	}

	public static <X> NavigableSet<X> unmodifiable(NavigableSet<? extends X> input) {
		if (input == null)
			input = Collections.emptyNavigableSet();
		return (NavigableSet<X>) Collections.unmodifiableNavigableSet(input);
	}

	public static <X> Optional<X> last(Iterator<X> input) {
		return last(input == null ? null : () -> input);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static <X> Optional<X> last(Iterable<X> input) {
		if (input == null)
			return Optional.empty();
		if (input instanceof List) {
			var size = ((List) input).size();
			if (size == 0)
				return Optional.empty();
			X result = (X) ((List) input).get(size - 1);
			return Optional.of(result);
		} else
			return stream(input).reduce((f, s) -> s);
	}

	public static <X> X get(Iterable<X> input, int index) {
		return get(input, index, null);
	}

	public static <X> X get(Iterable<X> input, int index, X orElse) {
		if (input instanceof List) {
			var list = ((List<X>) input);
			if (list.size() > index)
				return list.get(index);
		} else if (input != null && index >= 0) {
			var iter = input.iterator();
			for (int i = 0; iter != null && iter.hasNext(); i++) {
				var next = iter.next();
				if (i == index)
					return next;
			}
		}
		if (orElse != null && index >= 0)
			return orElse;
		throw new IndexOutOfBoundsException(index);
	}

}