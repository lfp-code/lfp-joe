package com.lfp.joe.utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Stack;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.CompareToBuilder;

import com.github.jknack.handlebars.Template;
import com.github.jknack.handlebars.internal.HbsParserFactory;
import com.google.re2j.Matcher;
import com.google.re2j.Pattern;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.log.LogFilter;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.strings.EmptyTemplateLoader;
import com.lfp.joe.utils.strings.HandlebarsLFP;
import com.lfp.joe.utils.strings.PatternLFP;
import com.lfp.joe.utils.strings.Templates;

import ch.qos.logback.classic.Level;
import ch.qos.logback.core.spi.FilterReply;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public class Strings extends StringUtils {

	protected Strings() {

	}

	private static final PatternLFP NEW_LINE_PATTERN = PatternLFP.compile("\\r?\\n");

	public static String newLine() {
		return MachineConfig.getDefaultLineSeperator();
	}

	private static final MemoizedSupplier<Void> HANDLEBARS_LOG_INIT = Utils.Functions.memoize(() -> {
		LogFilter.addFilter(ctx -> {
			if (!HbsParserFactory.class.getName().equals(ctx.getEvent().getLoggerName()))
				return FilterReply.NEUTRAL;
			Level level = ctx.getEvent().getLevel();
			if (Level.DEBUG.isGreaterOrEqual(level))
				return FilterReply.DENY;
			return FilterReply.NEUTRAL;
		});
		return null;
	});

	private static final MemoizedSupplier<HandlebarsLFP> HANDLEBARS_DEFAULT_INSTANCE_S = Utils.Functions.memoize(() -> {
		return newHandlebars();
	});

	private static HandlebarsLFP newHandlebars() {
		HANDLEBARS_LOG_INIT.get();
		return new HandlebarsLFP(new EmptyTemplateLoader());
	}

	public static Template templateCompileInline(String template) {
		return templateCompileInline(template, null, null);
	}

	public static Template templateCompileInline(String template, String startDelimiter, String endDelimiter) {
		return Templates.compileInline(HANDLEBARS_DEFAULT_INSTANCE_S.get(), template, startDelimiter, endDelimiter);
	}

	public static String templateApply(Template template, Map<String, ? extends Object> mappings) {
		Objects.requireNonNull(template);
		try {
			return template.apply(mappings == null ? Collections.emptyMap() : mappings);
		} catch (IOException e) {
			throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
					? java.lang.RuntimeException.class.cast(e)
					: new java.lang.RuntimeException(e);
		}
	}

	public static String templateApply(String templateStr, Map<String, ? extends Object> mappings) {
		Template template = templateCompileInline(templateStr);
		return templateApply(template, mappings);

	}

	public static String templateApply(Template template, Object... data) {
		Map<String, Object> dataMap = new HashMap<>();
		for (int i = 0; data != null && i < data.length; i = i + 2) {
			Object keyObj = data[i];
			String key = keyObj == null ? null : (String) keyObj;
			Object val = data.length <= i + 1 ? null : data[i + 1];
			dataMap.put(key, val);
		}
		return templateApply(template, dataMap);
	}

	public static String templateApply(String templateStr, Object... data) {
		Template template = templateCompileInline(templateStr);
		return templateApply(template, data);
	}

	public static Optional<String> firstNonBlank(Iterable<? extends String> ible) {
		return firstNonBlank(ible == null ? null : ible.iterator());
	}

	public static Optional<String> firstNonBlank(Iterator<? extends String> iterator) {
		if (iterator == null)
			return Optional.empty();
		while (iterator.hasNext()) {
			var next = iterator.next();
			if (isBlank(next))
				continue;
			return Optional.of(next);
		}
		return Optional.empty();
	}

	public static Optional<String> trimToNullOptional(String str) {
		str = trimToNull(str);
		return Optional.ofNullable(str);
	}

	public static Optional<String> toStringOptional(Object input) {
		return Optional.ofNullable(input).map(Object::toString);
	}

	public static String toString(InputStream inputStream) throws IOException {
		return toString(inputStream, StandardCharsets.UTF_8);
	}

	public static String toString(InputStream inputStream, Charset charset) throws IOException {
		if (inputStream == null)
			return null;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Bits.copy(inputStream, baos);
		return new String(baos.toByteArray(), StandardCharsets.UTF_8);
	}

	public static StreamEx<String> streamLines(InputStream inputStream) {
		return streamLines(inputStream, null);
	}

	public static StreamEx<String> streamLines(InputStream inputStream, Charset charset) {
		return streamLines(inputStream, charset, null);
	}

	public static StreamEx<String> streamLines(InputStream inputStream, Charset charset, Integer bufferSize) {
		if (inputStream == null)
			return StreamEx.empty();
		if (charset == null)
			charset = Bits.getDefaultCharset();
		if (bufferSize == null)
			bufferSize = Bits.getDefaultBufferSize();
		var isr = new InputStreamReader(inputStream, charset);
		var br = new BufferedReader(isr, bufferSize);
		var result = StreamEx.of(br.lines());
		return Lots.onComplete(result, () -> Exceptions.closeQuietly(Level.WARN, br, isr));
	}

	public static StreamEx<String> streamLines(CharSequence input) {
		return streamLines(input, false, false);
	}

	public static StreamEx<String> streamLines(CharSequence input, boolean notBlank) {
		return streamLines(input, notBlank);
	}

	public static StreamEx<String> streamLines(CharSequence input, boolean notBlank, boolean trim) {
		var stream = streamSplit(input, NEW_LINE_PATTERN);
		if (notBlank && trim)
			stream = stream.mapPartial(Strings::trimToNullOptional);
		else if (notBlank)
			stream = stream.filter(Strings::isNotBlank);
		else if (trim)
			stream = stream.map(Strings::trim);
		return stream;
	}

	public static StreamEx<String> streamSplit(String input, String regex) {
		return streamSplit(input, regex, false);
	}

	public static StreamEx<String> streamSplit(String input, String regex, boolean ignoreCase) {
		return streamSplit(input, compileRegex(regex, ignoreCase ? Pattern.CASE_INSENSITIVE : null));
	}

	public static StreamEx<String> streamSplit(CharSequence input, PatternLFP pattern) {
		if (input == null)
			return StreamEx.empty();
		var entryIterator = streamMatchIndexes(input, pattern).iterator();
		if (!entryIterator.hasNext())
			return StreamEx.of(input.toString());
		var producer = new Predicate<Consumer<? super String>>() {

			private Integer previousEnd;

			@Override
			public boolean test(Consumer<? super String> cb) {
				if (!entryIterator.hasNext()) {
					if (previousEnd != null) {
						var subString = substring(input.toString(), previousEnd, input.length());
						cb.accept(subString);
					}
					return false;
				}
				var entry = entryIterator.next();
				var subStringEnd = entry.getKey();
				var subStringStart = Optional.ofNullable(this.previousEnd).orElse(0);
				var subString = substring(input.toString(), subStringStart, subStringEnd);
				cb.accept(subString);
				this.previousEnd = entry.getValue();
				return true;
			}
		};
		return StreamEx.produce(producer);
	}

	public static String joinNewLine(Iterable<?> iterable) {
		if (iterable == null)
			return "";
		return Streams.of(iterable).joining(Objects.toString(newLine()));
	}

	public static Character charAt(String result, Integer index) {
		if (result == null || index == null || index < 0 || index >= result.length())
			return null;
		return result.charAt(index);
	}

	public static EntryStream<Integer, Integer> streamMatchIndexes(String input, String regex) {
		return streamMatchIndexes(input, regex, false);
	}

	public static EntryStream<Integer, Integer> streamMatchIndexes(String input, String regex, boolean ignoreCase) {
		return streamMatchIndexes(input, compileRegex(regex, ignoreCase ? Pattern.CASE_INSENSITIVE : null));
	}

	public static EntryStream<Integer, Integer> streamMatchIndexes(CharSequence input, PatternLFP pattern) {
		Objects.requireNonNull(pattern, "pattern required");
		if (input == null)
			return EntryStream.empty();
		var producer = new Predicate<Consumer<? super Entry<Integer, Integer>>>() {

			private Matcher matcher;

			@Override
			public boolean test(Consumer<? super Entry<Integer, Integer>> cb) {
				if (matcher == null)
					matcher = pattern.matcher(input);
				if (!matcher.find()) {
					matcher = null;
					return false;
				}
				cb.accept(Map.entry(matcher.start(), matcher.end()));
				return true;
			}
		};
		return StreamEx.produce(producer).mapToEntry(Entry::getKey, Entry::getValue);
	}

	public static String[] splitWithDelimiters(CharSequence input, String pattern) {
		return splitWithDelimiters(input, pattern == null ? null : PatternLFP.compile(pattern));
	}

	public static String[] splitWithDelimiters(CharSequence input, java.util.regex.Pattern pattern) {
		return splitWithDelimiters(input, PatternLFP.compile(pattern));
	}

	public static String[] splitWithDelimiters(CharSequence input, PatternLFP pattern) {
		Objects.requireNonNull(pattern);
		if (input == null)
			return new String[] {};
		Matcher matcher = pattern.matcher(input);
		int start = 0;
		List<String> result = new ArrayList<>();
		while (matcher.find()) {
			result.add(input.subSequence(start, matcher.start()).toString());
			result.add(matcher.group());
			start = matcher.end();
		}
		if (start != input.length())
			result.add(input.subSequence(start, input.length()).toString());
		return result.toArray(String[]::new);
	}

	public static boolean removePrefixMatches(Collection<String> collection) {
		return removePrefixMatches(collection, false);
	}

	public static boolean removePrefixMatches(Collection<String> collection, boolean ignoreCase) {
		if (collection == null || collection.size() < 2)
			return false;
		boolean mod = false;
		var iter = collection.iterator();
		for (var i = 0; iter.hasNext(); i++) {
			var next = iter.next();
			var iterOther = collection.iterator();
			for (int j = 0; iterOther.hasNext(); j++) {
				var nextOther = iterOther.next();
				if (i == j)
					continue;
				boolean remove;
				if (ignoreCase)
					remove = StringUtils.startsWithIgnoreCase(nextOther, next);
				else
					remove = StringUtils.startsWith(nextOther, next);
				if (remove) {
					iter.remove();
					i--;
					mod = true;
					break;
				}
			}
		}
		return mod;
	}

	// hopefully hides compatibility issues with lang3
	public static boolean equalsAnyIgnoreCase(final CharSequence string, final CharSequence... searchStrings) {
		if (ArrayUtils.isNotEmpty(searchStrings))
			for (final CharSequence next : searchStrings)
				if (equalsIgnoreCase(string, next))
					return true;
		return false;
	}

	// hopefully hides compatibility issues with lang3
	public static boolean isAllBlank(final CharSequence... css) {
		if (css == null || css.length == 0)
			return true;
		for (var cs : css)
			if (!StringUtils.isBlank(cs))
				return false;
		return true;
	}

	// hopefully hides compatibility issues with lang3
	public static int compareIgnoreCase(final String str1, final String str2) {
		if (str1 == null || str2 == null)
			return new CompareToBuilder().append(str1, str2).toComparison();
		return String.CASE_INSENSITIVE_ORDER.compare(str1, str2);
	}

	public static <X> Comparator<X> comparatorIgnoreCase(Function<X, String> mapping) {
		return (v1, v2) -> {
			if (mapping == null)
				return 0;
			var str1 = mapping.apply(v1);
			var str2 = mapping.apply(v2);
			return compareIgnoreCase(str1, str2);
		};
	}

	// allows one dot in numbers
	public static boolean isNumeric(final CharSequence cs, char... skipCharacters) {
		if (isEmpty(cs))
			return false;
		final int sz = cs.length();
		boolean dotFound = false;
		boolean negFound = false;
		for (int i = 0; i < sz; i++) {
			var charAt = cs.charAt(i);
			var skip = Streams.of(skipCharacters).nonNull().anyMatch(v -> Objects.equals(charAt, v));
			if (skip)
				continue;
			if (!negFound && i == 0 && Objects.equals('-', charAt)) {
				negFound = true;
				continue;
			}
			if (!dotFound && Objects.equals('.', charAt)) {
				dotFound = true;
				continue;
			}
			if (!Character.isDigit(charAt))
				return false;
		}
		if (sz == 1 && (dotFound || negFound))
			return false;
		return true;
	}

	public static Optional<Number> parseNumber(String text) {
		var skipCharacters = new char[] { ',' };
		if (!Strings.isNumeric(text, skipCharacters))
			return Optional.empty();
		for (var skipCharacter : skipCharacters) {
			while (true) {
				var index = text.indexOf(skipCharacter);
				if (index < 0)
					break;
				text = text.substring(0, index) + text.substring(index + 1);
			}
		}
		boolean parseDouble;
		int dotIndex = text.indexOf('.');
		if (dotIndex < 0)
			parseDouble = false;
		else {
			if (dotIndex == text.length() - 1) {
				text = text.substring(0, text.length() - 1);
				parseDouble = false;
			} else
				parseDouble = true;
		}
		Number value;
		if (parseDouble)
			value = Functions.catching(text, v -> Double.valueOf(v), t -> null);
		else
			value = Functions.catching(text, v -> Long.valueOf(v), t -> null);
		return Optional.ofNullable(value);
	}

	public static int[] indexesOf(CharSequence seq, String searchSeq) {
		int index = StringUtils.indexOf(seq, searchSeq);
		if (index < 0)
			return new int[0];
		List<Integer> result = new ArrayList<>();
		while (index >= 0) {
			result.add(index);
			index = StringUtils.indexOf(seq, searchSeq, index + 1);
		}
		return Streams.of(result).mapToInt(v -> v).toArray();
	}

	public static String removeANSIEscapeSequences(String output) {
		if (output == null)
			return output;
		return output.replaceAll("\u001B\\[[;\\d]*[ -/]*[@-~]", "");
	}

	public static Entry<Integer, Integer> getBoundaries(String input, Character open, Character close) {
		return getBoundaries(input, open == null ? null : Objects.toString(open),
				close == null ? null : Objects.toString(close));
	}

	public static Entry<Integer, Integer> getBoundaries(String input, CharSequence open, CharSequence close) {
		if (StringUtils.isBlank(input) || StringUtils.isBlank(open) || StringUtils.isBlank(close))
			return null;
		int index = StringUtils.indexOf(input, open);
		if (index < 0)
			return null;
		int i;
		// Stack to store opening brackets.
		Stack<Integer> st = new Stack<>();
		// Traverse through string starting from
		// given index.
		for (i = index; i < input.length(); i++)
			// If current character is an
			// opening bracket push it in stack.
			if (StringUtils.equals(StringUtils.substring(input, i, i + open.length()), open))
				st.push((int) input.charAt(i));
			else if (StringUtils.equals(StringUtils.substring(input, i, i + close.length()), close)) {
				st.pop();
				if (st.empty())
					return Lots.entry(index, i);
			}
		// If no matching closing bracket
		// is found.
		return null;
	}

	public static int getBoundaryEndIndex(String input, Character open, Character close) {
		Entry<Integer, Integer> ent = getBoundaries(input, open, close);
		return ent == null ? -1 : ent.getValue();
	}

	public static PatternLFP compileRegex(String regex) {
		return PatternLFP.compile(regex);
	}

	public static PatternLFP compileRegex(String regex, Integer flags) {
		Objects.requireNonNull(regex, "regex required");
		if (flags == null)
			return compileRegex(regex);
		return PatternLFP.compile(regex, flags);
	}

	public static void main(String[] args) {
		var list = Streams.of("neat", "nea", "ne", "neato", "neaty", "cool-dude", "cool-guy", "cool-", "cool-du", "coo")
				.toList();
		System.out.println(list);
		System.out.println(removePrefixMatches(list));
		System.out.println(list);

		var p1 = java.util.regex.Pattern.compile("\\n", java.util.regex.Pattern.CASE_INSENSITIVE);
		var p2 = java.util.regex.Pattern.compile("\\n");
		System.out.println(p1.toString() + " - " + p1.flags());
		System.out.println(p2.toString() + " - " + p2.flags());

		Streams.of("").map(Utils.Strings::trimToNull).nonNull();
		Streams.of("").mapPartial(Strings::trimToNullOptional).nonNull();
		System.out
				.println(StreamEx.of("1", "", " ", "\t", null, " 2").mapPartial(Strings::trimToNullOptional).toList());

		System.out.println(parseNumber("."));
		System.out.println(parseNumber("100."));
		System.out.println(parseNumber("1,000.1"));
		System.out.println(parseNumber(".1"));
	}
}
