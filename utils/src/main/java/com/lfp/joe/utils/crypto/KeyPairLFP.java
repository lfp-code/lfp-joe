package com.lfp.joe.utils.crypto;

import java.security.PrivateKey;
import java.security.PublicKey;

public interface KeyPairLFP<PRV extends PrivateKey, PUB extends PublicKey> {

	PRV getPrivateKey();

	PUB getPublicKey();
}
