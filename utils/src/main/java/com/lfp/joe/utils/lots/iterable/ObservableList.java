package com.lfp.joe.utils.lots.iterable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import eu.mihosoft.vcollections.VList;
import vjavax.observer.Subscription;
import vjavax.observer.collection.CollectionChange;
import vjavax.observer.collection.CollectionChangeEvent;
import vjavax.observer.collection.CollectionChangeListener;

public class ObservableList<X> implements List<X> {

	private final VList<X> delegate;

	public ObservableList() {
		this(new ArrayList<>());
	}

	public ObservableList(List<X> backingList) {
		this.delegate = VList.newInstance(backingList);
	}

	public Subscription addChangeListener(CollectionChangeListener<X, List<X>, CollectionChange<X>> listener) {
		CollectionChangeListener<X, List<X>, CollectionChange<X>> listenerWrap = listener == null ? null
				: new CollectionChangeListener<>() {

					@Override
					public void onChange(CollectionChangeEvent<X, List<X>, CollectionChange<X>> evt) {
						listener.onChange(new CollectionChangeEvent<X, List<X>, CollectionChange<X>>() {

							@Override
							public CollectionChange<X> added() {
								return evt.added();
							}

							@Override
							public CollectionChange<X> removed() {
								return evt.removed();
							}

							@Override
							public ObservableList<X> source() {
								return ObservableList.this;
							}
						});

					}
				};
		return this.delegate.addChangeListener(listenerWrap);
	}

	@Override
	public void forEach(Consumer<? super X> action) {
		delegate.forEach(action);
	}

	@Override
	public int size() {
		return delegate.size();
	}

	@Override
	public boolean isEmpty() {
		return delegate.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return delegate.contains(o);
	}

	@Override
	public Iterator<X> iterator() {
		return delegate.iterator();
	}

	@Override
	public Object[] toArray() {
		return delegate.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return delegate.toArray(a);
	}

	@Override
	public boolean add(X e) {
		return delegate.add(e);
	}

	@Override
	public boolean remove(Object o) {
		return delegate.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return delegate.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends X> c) {
		return delegate.addAll(c);
	}

	@Override
	public boolean addAll(int index, Collection<? extends X> c) {
		return delegate.addAll(index, c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return delegate.removeAll(c);
	}

	@Override
	public <T> T[] toArray(IntFunction<T[]> generator) {
		return delegate.toArray(generator);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return delegate.retainAll(c);
	}

	@Override
	public void replaceAll(UnaryOperator<X> operator) {
		delegate.replaceAll(operator);
	}

	@Override
	public void sort(Comparator<? super X> c) {
		delegate.sort(c);
	}

	@Override
	public void clear() {
		delegate.clear();
	}

	@Override
	public boolean equals(Object o) {
		return delegate.equals(o);
	}

	@Override
	public int hashCode() {
		return delegate.hashCode();
	}

	@Override
	public X get(int index) {
		return delegate.get(index);
	}

	@Override
	public boolean removeIf(Predicate<? super X> filter) {
		return delegate.removeIf(filter);
	}

	@Override
	public X set(int index, X element) {
		return delegate.set(index, element);
	}

	@Override
	public void add(int index, X element) {
		delegate.add(index, element);
	}

	@Override
	public X remove(int index) {
		return delegate.remove(index);
	}

	@Override
	public int indexOf(Object o) {
		return delegate.indexOf(o);
	}

	@Override
	public int lastIndexOf(Object o) {
		return delegate.lastIndexOf(o);
	}

	@Override
	public ListIterator<X> listIterator() {
		return delegate.listIterator();
	}

	@Override
	public ListIterator<X> listIterator(int index) {
		return delegate.listIterator(index);
	}

	@Override
	public List<X> subList(int fromIndex, int toIndex) {
		return delegate.subList(fromIndex, toIndex);
	}

	@Override
	public Spliterator<X> spliterator() {
		return delegate.spliterator();
	}

	@Override
	public Stream<X> stream() {
		return delegate.stream();
	}

	@Override
	public Stream<X> parallelStream() {
		return delegate.parallelStream();
	}
}
