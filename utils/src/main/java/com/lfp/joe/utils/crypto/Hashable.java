package com.lfp.joe.utils.crypto;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Proxy;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.google.re2j.Pattern;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.MethodsRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.function.FNV;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.introspector.IntrospectorLFP;
import com.lfp.joe.introspector.PropertyDescriptor;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Crypto;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.StreamEx;

public interface Hashable {

	Bytes hash();

	default Bytes hash(int limitLength) {
		Validate.isTrue(limitLength >= -1, "invalid length:%s", limitLength);
		Bytes hash = hash();
		if (limitLength != -1 && hash.length() > limitLength) {
			byte[] subArr = Arrays.copyOfRange(hash.array(), 0, limitLength);
			hash = Bytes.from(subArr);
		}
		return hash;
	}

	default String uuid() {
		return uuid(-1);
	}

	default String uuid(int limitBytes) {
		Bytes hashed = hash(limitBytes);
		if (hashed == null)
			return null;
		String encoded = hashed.encodeBase32();
		encoded = StringUtils.stripEnd(encoded, "=");
		encoded = encoded.toLowerCase();
		return encoded;
	}

	public static interface Basic extends Hashable {

		@Override
		default Bytes hash() {
			return Hashable.basicHash(this);
		}
	}

	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Disabled {}

	@SafeVarargs
	public static <X> Bytes basicHash(X bean, BiPredicate<PropertyDescriptor, PropertyAccessor>... predicates) {
		return basicHash(Crypto.getMessageDigestMD5(), bean, predicates);
	}

	@SafeVarargs
	public static <X> Bytes basicHash(MessageDigest messageDigest, X bean,
			BiPredicate<PropertyDescriptor, PropertyAccessor>... predicates) {
		Objects.requireNonNull(messageDigest);
		Objects.requireNonNull(bean);
		var beanType = CoreReflections.getNamedClassType(bean.getClass());
		var beanInfo = IntrospectorLFP.getBeanInfo(beanType);
		var propertyDescriptors = Streams.of(beanInfo.getPropertyDescriptors());
		propertyDescriptors = propertyDescriptors.filter(v -> {
			return Const.CLASS_TYPE_TO_PROPERTY_DESCRIPTOR_FILTER.test(beanType, v);
		});
		var pdToAccessor = propertyDescriptors.<PropertyAccessor>mapToEntry(v -> {
			return new PropertyAccessor() {

				private Optional<Object> valueOp;

				@Override
				public Object get() {
					if (valueOp != null)
						return valueOp.orElse(null);
					return Throws.unchecked(() -> v.getReadMethod().invoke(bean));
				}

				@Override
				public void accept(Object value) {
					valueOp = Optional.ofNullable(value);
				}
			};
		});
		pdToAccessor = pdToAccessor.sortedBy(ent -> ent.getKey().getName());
		if (predicates != null && predicates.length > 0)
			pdToAccessor = pdToAccessor.filter(ent -> {
				for (var predicate : predicates)
					if (predicate != null && !predicate.test(ent.getKey(), ent.getValue()))
						return false;
				return true;
			});
		var hashInputs = pdToAccessor.<Object>flatMap(ent -> {
			var pd = ent.getKey();
			var value = ent.getValue().get();
			StreamEx<Object> valueStream;
			if (value instanceof Map) {
				var map = (Map<?, ?>) value;
				valueStream = Streams.of(map.keySet()).sorted(Const.SORTER).flatMap(k -> {
					return Streams.of(k, map.get(k));
				});
			} else {
				if (value instanceof Collection)
					valueStream = Streams.of((Collection<?>) value);
				else
					valueStream = Streams.of(Arrays.asList(value));
				if (!List.class.isAssignableFrom(pd.getPropertyType()))
					valueStream = valueStream.sorted(Const.SORTER);
			}
			return valueStream.prepend(pd.getName());
		});
		if (isValidClassType(beanType))
			hashInputs = hashInputs.prepend(beanType);
		else {
			var prepend = Streams.of(CoreReflections.streamTypes(beanType))
					.filter(v -> isValidClassType(v))
					.distinct()
					.sortedBy(Class::getName)
					.toImmutableList();
			hashInputs = hashInputs.prepend(prepend);

		}
		var hashInputArr = hashInputs.toArray();
		return Crypto.hash(messageDigest, hashInputArr);
	}

	public static int hashCode(Hashable hashable) {
		final int prime = 31;
		int result = 1;
		Bytes hashed = hashable.hash();
		result = prime * result + ((hashed == null) ? 0 : hashed.hashCode());
		return result;
	}

	public static boolean equals(Hashable hashable, Object obj) {
		if (hashable == obj)
			return true;
		if ((obj == null) || (hashable.getClass() != obj.getClass()))
			return false;
		Hashable other = (Hashable) obj;
		Bytes hashed = hashable.hash();
		Bytes hashedOther = other.hash();
		if (hashed == null) {
			if (hashedOther != null)
				return false;
		} else if (!hashed.equals(hashedOther))
			return false;
		return true;
	}

	private static boolean isValidClassType(Class<?> classType) {
		if (classType == null)
			return false;
		if (Proxy.isProxyClass(classType))
			return false;
		if (!CoreReflections.isNamedClassType(classType))
			return false;
		if (CoreReflections.isBootModuleClassType(classType))
			return false;
		{
			var className = classType.getName();
			if (Stream.of("$Proxy", "_$$_").anyMatch(v -> StringUtils.contains(className, v)))
				return false;
		}
		return true;
	}

	public static interface PropertyAccessor extends Supplier<Object>, Consumer<Object> {}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	static enum Const {
		;

		private static final Cache<Entry<Class<?>, String>, Boolean> FILTER_METHODS_CACHE = Caffeine.newBuilder()
				.weakKeys()
				.weakValues()
				.build();
		private static final List<Pattern> FILTER_METHOD_NAME_PATTERNS = List.of(
				Pattern.compile("^meta", Pattern.CASE_INSENSITIVE),
				Pattern.compile("^builder", Pattern.CASE_INSENSITIVE),
				Pattern.compile("builder$", Pattern.CASE_INSENSITIVE));
		private static final BiPredicate<Class<?>, PropertyDescriptor> CLASS_TYPE_TO_PROPERTY_DESCRIPTOR_FILTER;
		static {
			BiPredicate<Class<?>, PropertyDescriptor> predicate = (ct, pd) -> ct != null;
			predicate = predicate.and((ct, pd) -> pd != null && pd.getReadMethod() != null);
			predicate = predicate.and((ct, pd) -> {
				Entry<Class<?>, String> cacheKey = Map.entry(CoreReflections.getNamedClassType(ct), pd.getName());
				return FILTER_METHODS_CACHE.get(cacheKey, nil -> {
					var readMethod = pd.getReadMethod();
					if (readMethod.getAnnotation(Disabled.class) != null)
						return false;
					if (FILTER_METHOD_NAME_PATTERNS.stream().anyMatch(v -> v.matcher(readMethod.getName()).find()))
						return false;
					boolean success = Streams
							.of(MemberCache.getMethods(MethodsRequest.of(readMethod.getDeclaringClass())))
							.filter(v -> CoreReflections.compatibleMethodInvoke(v, readMethod))
							.noneMatch(method -> {
								// noneMatch compatible with Hashable method
								if (Hashable.class.equals(method.getDeclaringClass()))
									return true;
								// noneMatch compatible with Disabled method
								if (method.getAnnotation(Disabled.class) != null)
									return true;
								return false;
							});
					return success;
				});
			});
			CLASS_TYPE_TO_PROPERTY_DESCRIPTOR_FILTER = predicate;
		}
		private static final Comparator<Object> SORTER;
		static {
			Comparator<Object> comparator = (v1, v2) -> {
				if (v1 instanceof Comparable && v2 instanceof Comparable)
					return ((Comparable) v1).compareTo(v2);
				return 0;
			};
			comparator = Comparator.nullsLast(comparator);
			SORTER = comparator;
		}

	}

	public static void main(String[] args) {
		var pattern = Pattern.compile("^meta", Pattern.CASE_INSENSITIVE);
		System.out.println(pattern.matcher("METABean").find());
		FNV.hash64(new Date());
		FNV.hash64(Bytes.from(67), Bytes.from(67));
		System.out.println(basicHash(Utils.Crypto.getMessageDigestFNV(), new Date(0)).encodeHex());
		System.out.println(basicHash(Utils.Crypto.getMessageDigestFNV(), new Date(1)).encodeHex());
		System.out.println(basicHash(Utils.Crypto.getMessageDigestFNV(), new Date(0)).encodeHex());
		var date = new Date(0) {

			String hat() {
				return "";
			}

		};
		System.out.println(basicHash(Utils.Crypto.getMessageDigestFNV(), date).encodeHex());
	}
}
