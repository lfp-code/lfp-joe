package com.lfp.joe.utils.function;

import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;
import java.util.function.Function;

public class ReadWriteAccessor<X> {

	private final ReadWriteLock rwLock = new ReentrantReadWriteLock();
	private final X backingObject;

	public ReadWriteAccessor(X backingObject) {
		this.backingObject = backingObject;
	}

	public void readAccess(Consumer<X> accessor) {
		this.readAccess(accessor == null ? null : v -> {
			accessor.accept(v);
			return null;
		});
	}

	public <Y> Y readAccess(Function<X, Y> accessor) {
		return access(rwLock.readLock(), accessor);
	}

	public void writeAccess(Consumer<X> accessor) {
		this.writeAccess(accessor == null ? null : v -> {
			accessor.accept(v);
			return null;
		});
	}

	public <Y> Y writeAccess(Function<X, Y> accessor) {
		return access(rwLock.writeLock(), accessor);
	}

	public <Y> Y access(Lock lock, Function<X, Y> accessor) {
		Objects.requireNonNull(accessor);
		lock.lock();
		try {
			return accessor.apply(backingObject);
		} finally {
			lock.unlock();
		}
	}
}
