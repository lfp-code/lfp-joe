package com.lfp.joe.utils.lots.stream;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Spliterator;
import java.util.stream.Stream;

//from https://gist.github.com/stephenhand/292cdd8bba7a452d83c51c00d9ef113c
public class StreamInputStream extends InputStream {
	private final Stream<byte[]> source;
	private Spliterator<byte[]> spliterator = null;
	private ByteArrayInputStream currentItemByteStream = null;

	public StreamInputStream(Stream<byte[]> source) {
		this.source = source;
	}

	@Override
	public int read() throws IOException {
		if (source == null)
			return -1;
		if (spliterator == null)
			synchronized (this) {
				if (spliterator == null)
					spliterator = source.spliterator();
			}
		try {
			if (currentItemByteStream == null)
				synchronized (this) {
					if (currentItemByteStream == null)
						if (!spliterator.tryAdvance(bytes -> currentItemByteStream = new ByteArrayInputStream(bytes))) {
							source.close();
							return -1;
						}
				}
			int ret = currentItemByteStream.read();
			if (ret == -1) {
				currentItemByteStream = null;
				return read();
			}
			return ret;
		} catch (Throwable t) {
			source.close();
			throw t;
		}
	}

	@Override
	public void close() throws IOException {
		try {
			if (source != null)
				source.close();
		} finally {
			super.close();
		}
	}
}