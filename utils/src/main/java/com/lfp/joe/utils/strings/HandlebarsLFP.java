package com.lfp.joe.utils.strings;

import java.lang.reflect.Field;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.io.TemplateLoader;
import com.lfp.joe.utils.Functions;

public class HandlebarsLFP extends Handlebars {

	private static final Field Handlebars_startDelimiter_FIELD = com.lfp.joe.utils.Utils.Functions.unchecked(() -> {
		var field = Handlebars.class.getDeclaredField("startDelimiter");
		field.setAccessible(true);
		return field;
	});
	private static final Field Handlebars_endDelimiter_FIELD = com.lfp.joe.utils.Utils.Functions.unchecked(() -> {
		var field = Handlebars.class.getDeclaredField("endDelimiter");
		field.setAccessible(true);
		return field;
	});



	public HandlebarsLFP() {
		super();
	}

	public HandlebarsLFP(TemplateLoader loader) {
		super(loader);
	}

	public String getStartDelimiter() {
		return (String) Functions.unchecked(() -> Handlebars_startDelimiter_FIELD.get(this));
	}

	public String getEndDelimiter() {
		return (String) Functions.unchecked(() -> Handlebars_endDelimiter_FIELD.get(this));
	}

	public static void main(String[] args) {
		var test = new HandlebarsLFP();
		System.out.println(test.getStartDelimiter());
		System.out.println(test.getEndDelimiter());
	}
}
