package com.lfp.joe.utils.function;

import java.time.Duration;

public interface Semaphore {

	public static Semaphore create(int permits) {
		return create(permits, true);
	}

	public static Semaphore create(int permits, boolean fair) {
		return new SemaphoreImpl(permits, fair);
	}

	default void acquire() throws InterruptedException {
		acquire(1);
	}

	void acquire(int permits) throws InterruptedException;

	default boolean tryAcquire() {
		return tryAcquire(1);
	}

	boolean tryAcquire(int permits);

	default boolean tryAcquire(Duration timeout) throws InterruptedException {
		return tryAcquire(1, timeout);
	}

	boolean tryAcquire(int permits, Duration timeout) throws InterruptedException;

	default void release() {
		release(1);
	}

	void release(int permits);

	int availablePermits();

	int drainPermits();

	void addPermits(int permits);

}