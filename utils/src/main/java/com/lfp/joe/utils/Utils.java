package com.lfp.joe.utils;

public class Utils {

	public static class Functions extends com.lfp.joe.utils.Functions {
		private Functions() {}
	}

	public static class Lots extends com.lfp.joe.utils.Lots {
		private Lots() {}
	}

	public static class Strings extends com.lfp.joe.utils.Strings {
		private Strings() {}
	}

	public static class Resources extends com.lfp.joe.utils.Resources {
		private Resources() {}
	}

	public static class Bits extends com.lfp.joe.utils.Bits {
		private Bits() {}
	}

	public static class Exceptions extends com.lfp.joe.utils.Exceptions {
		private Exceptions() {}
	}

	public static class Types extends com.lfp.joe.utils.Types {
		private Types() {}
	}

	public static class Files extends com.lfp.joe.utils.Files {
		private Files() {}
	}

	public static class Machine extends com.lfp.joe.utils.Machine {
		private Machine() {}
	}

	public static class Crypto extends com.lfp.joe.utils.Crypto {
		private Crypto() {}
	}

	public static class Times extends com.lfp.joe.utils.Times {
		private Times() {}
	}

}
