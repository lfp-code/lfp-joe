package com.lfp.joe.utils.function;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import com.github.throwable.beanref.BeanPath;
import com.lfp.joe.core.function.Throws.ThrowingBiFunction;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

public interface PropertyCache {

	// gets

	default <X> X get(BeanPath<?, X> beanPath) {
		return get(beanPath, null);
	}

	default <X, T extends Throwable> X get(BeanPath<?, X> beanPath, ThrowingSupplier<X, T> loader) throws T {
		return get(beanPath == null ? null : beanPath.getPath(), beanPath == null ? null : beanPath.getType(), loader);
	}

	default <X, T extends Throwable> X get(String path, Class<X> requiredType) throws T {
		return get(path, requiredType, null);
	}

	default <X, T extends Throwable> X get(String path, Class<X> requiredType, ThrowingSupplier<X, T> loader) throws T {
		X value = get(path, loader);
		if (value == null || requiredType == null)
			return value;
		if (!(requiredType.isAssignableFrom(value.getClass()))) {
			String msg = String.format("loader produced incompatible type. path:%s requiredType:%s resultType:%s", path,
					requiredType, value.getClass());
			throw new IllegalArgumentException(msg);
		}
		return value;
	}

	default <X> X get(String key) {
		return get(key, (ThrowingSupplier<X, RuntimeException>) null);
	}

	default <X, T extends Throwable> X get(String key, ThrowingSupplier<X, T> loader) throws T {
		return compute(key, loader == null ? null : (k, v) -> {
			return v != null ? v : loader.get();
		});
	}

	// load if null

	default <X, T extends Throwable> X loadIfNull(X value, String path, Class<X> requiredType,
			ThrowingSupplier<X, T> loader) throws T {
		if (value != null)
			return value;
		return get(path, requiredType, loader);
	}

	default <X, T extends Throwable> X loadIfNull(X value, BeanPath<?, X> beanPath, ThrowingSupplier<X, T> loader)
			throws T {
		if (value != null)
			return value;
		return get(beanPath, loader);
	}

	default <X, T extends Throwable> X loadIfNull(X value, String key, ThrowingSupplier<X, T> loader) throws T {
		if (value != null)
			return value;
		return get(key, loader);
	}

	// invalidates

	default <X> X invalidate(BeanPath<?, X> beanPath) {
		return invalidate(beanPath == null ? null : beanPath.getPath());
	}

	default <X> X invalidate(String key) {
		CallbackRemappingFunction<X, X, RuntimeException> remappingFunction = (k, v, cb) -> {
			cb.accept(v);
			return null;
		};
		return compute(key, remappingFunction);
	}

	// computes

	default <X, Y, T extends Throwable> Y compute(String key, CallbackRemappingFunction<X, Y, T> remappingFunction)
			throws T {
		var resultRef = new AtomicReference<Y>();
		ThrowingBiFunction<String, X, X, T> remappingFunctionWrapper = remappingFunction == null ? null : (k, v) -> {
			return remappingFunction.apply(k, v, resultRef::set);
		};
		compute(key, remappingFunctionWrapper);
		return resultRef.get();
	}

	<X, T extends Throwable> X compute(String key, ThrowingBiFunction<String, X, X, T> remappingFunction) throws T;

	public static interface CallbackRemappingFunction<X, Y, T extends Throwable> {

		public X apply(String key, X currentValue, Consumer<Y> callback) throws T;

	}

	public static class Impl implements PropertyCache {

		private final Map<String, Object> cache = new ConcurrentHashMap<>();

		@SuppressWarnings("unchecked")
		@Override
		public <X, T extends Throwable> X compute(String key, ThrowingBiFunction<String, X, X, T> remappingFunction)
				throws T {
			if (key == null || key.isBlank())
				throw new IllegalArgumentException("a non blank key is required");
			if (remappingFunction == null)
				return (X) cache.get(key);
			var errorRef = new AtomicReference<Throwable>();
			X result = (X) cache.compute(key, (k, v) -> {
				try {
					return remappingFunction.apply(k, (X) v);
				} catch (Throwable error) {
					errorRef.set(error);
					return v;
				}
			});
			if (errorRef.get() != null)
				throw (T) errorRef.get();
			return result;
		}

		@Override
		public String toString() {
			return "Impl [cache=" + cache + "]";
		}

	}

	public static PropertyCache create() {
		return new PropertyCache.Impl();
	}
}
