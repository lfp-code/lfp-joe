package com.lfp.joe.utils.function;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.Validate;

import one.util.streamex.EntryStream;

@SuppressWarnings("serial")
public class SemaphoreImpl extends java.util.concurrent.Semaphore implements Semaphore {

	private final Object maxPermitsLock = new Object();

	/**
	 * how many permits are allowed as governed by this semaphore. Access must be
	 * synchronized on this object.
	 */
	private volatile int maxPermits;

	public SemaphoreImpl(int maxPermits) {
		this(maxPermits, false);
	}

	public SemaphoreImpl(int maxPermits, boolean fair) {
		super(validatePositivePermits(maxPermits), fair);
		this.maxPermits = maxPermits;
	}

	/**
	 * Returns the number of max permits. This method may not reflect the changes of
	 * a concurrent {@link #setMaxPermits(int)} operation.
	 *
	 * @return
	 */
	public int getMaxPermits() {
		return maxPermits;
	}

	/*
	 * Must be synchronized because the underlying int is not thread safe
	 */

	/**
	 * Set the max number of permits. Must be greater than zero.
	 * <p/>
	 * Note that if there are more than the new max number of permits currently
	 * outstanding, any currently blocking threads or any new threads that start to
	 * block after the call will wait until enough permits have been released to
	 * have the number of outstanding permits fall below the new maximum. In other
	 * words, it does what you probably think it should.
	 *
	 * @param newMax
	 */
	public void setMaxPermits(int newMax) {
		validatePositivePermits(newMax);
		synchronized (maxPermitsLock) {
			int delta = newMax - this.maxPermits;
			if (delta == 0)
				return;
			else if (delta > 0)
				// new max is higher, so release that many permits
				this.release(delta);
			else
				// delta < 0.
				// reducePermits needs a positive #, though.
				this.reducePermits(-1 * delta);
			this.maxPermits = newMax;
		}
	}

	@Override
	public void addPermits(int permits) {
		synchronized (maxPermitsLock) {
			var newMax = this.maxPermits + permits;
			setMaxPermits(newMax);
		}
	}

	@Override
	public boolean tryAcquire(int permits, Duration timeout) throws InterruptedException {
		if (timeout == null)
			return tryAcquire(permits);
		return tryAcquire(permits, timeout.toMillis(), TimeUnit.MILLISECONDS);
	}

	@Override
	public String toString() {
		var out = EntryStream.of("semaphore", super.toString(), "maxPermits", maxPermits)
				.map(ent -> String.format("%s=%s", ent.getKey(), ent.getValue())).joining(", ");
		out = String.format("%s.%s{%s}", Semaphore.class.getSimpleName(), this.getClass().getSimpleName(), out);
		return out;
	}

	private static int validatePositivePermits(int permits) {
		Validate.isTrue(permits > 0, "invalid permit count:%s", permits);
		return permits;
	}

}