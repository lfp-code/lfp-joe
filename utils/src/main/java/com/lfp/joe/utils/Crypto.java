package com.lfp.joe.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.zip.Adler32;
import java.util.zip.Checksum;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;
import com.lfp.joe.core.function.FNV;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.utils.crypto.CertificateParser;
import com.lfp.joe.utils.crypto.MessageDigestUpdater;
import com.lfp.joe.utils.crypto.RSAKeyPair;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;

public class Crypto {
	protected Crypto() {};

	public static final char[] STANDARD_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toLowerCase().toCharArray();
	public static final int STANDARD_LENGTH = 25;

	private static final MemoizedSupplier<SecureRandom> SECURE_RANDOM_S = Utils.Functions.memoize(() -> {
		var nanoTime = System.nanoTime();
		ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
		buffer.putLong(nanoTime);
		return new SecureRandom(buffer.array());
	});
	private static final MemoizedSupplier<Random> RANDOM_S = Utils.Functions
			.memoize(() -> new Random(SECURE_RANDOM_S.get().nextLong()));

	public static SecureRandom getSecureRandom() {
		return SECURE_RANDOM_S.get();
	}

	public static Random getRandom() {
		return RANDOM_S.get();
	}

	public static String getSecureRandomString() {
		return getSecureRandomString(STANDARD_LENGTH);
	}

	public static String getSecureRandomString(int length) {
		return getSecureRandomString(length, STANDARD_ALPHABET);
	}

	public static String getSecureRandomString(int length, char[] alphabet) {
		return getRandomString(SECURE_RANDOM_S.get(), length, alphabet);
	}

	public static String getRandomString() {
		return getRandomString(STANDARD_LENGTH);
	}

	public static String getRandomString(int length) {
		return getRandomString(length, STANDARD_ALPHABET);
	}

	public static String getRandomString(int length, char[] alphabet) {
		return getRandomString(RANDOM_S.get(), length, alphabet);
	}

	public static String getRandomString(Random random, int length, char[] alphabet) {
		Objects.requireNonNull(alphabet);
		Validate.isTrue(alphabet.length > 0, "alphabet required");
		return NanoIdUtils.randomNanoId(random, alphabet, length);
	}

	public static int getRandomInclusive(int value1, int value2) {
		return getRandomInclusive(getRandom(), value1, value2);
	}

	public static int getRandomInclusiveSecure(int value1, int value2) {
		return getRandomInclusive(getSecureRandom(), value1, value2);
	}

	protected static int getRandomInclusive(Random random, int value1, int value2) {
		int max = Math.max(value1, value2);
		int min = Math.min(value1, value2);
		return random.nextInt((max - min) + 1) + min;
	}

	public static long getRandomInclusive(long value1, long value2) {
		long max = Math.max(value1, value2);
		long min = Math.min(value1, value2);
		if (min == max)
			return min;
		return ThreadLocalRandom.current().nextLong(min, max + 1);
	}

	public static MessageDigest getMessageDigestAdler32() {
		return new MessageDigest(Adler32.class.getSimpleName()) {

			private final Checksum checksum = new Adler32();

			@Override
			protected void engineUpdate(byte input) {
				checksum.update(input);
			}

			@Override
			protected void engineUpdate(byte[] input, int offset, int len) {
				checksum.update(input, offset, len);
			}

			@Override
			protected byte[] engineDigest() {
				var value = checksum.getValue();
				return BigInteger.valueOf(value).toByteArray();
			}

			@Override
			protected void engineReset() {
				checksum.reset();
			}
		};
	}

	public static MessageDigest getMessageDigestFNV() {
		return FNV.messageDigest();
	}

	public static MessageDigest getMessageDigestMD5() {
		return Throws.unchecked(() -> MessageDigest.getInstance("MD5"));
	}

	public static MessageDigest getMessageDigestSHA256() {
		return Throws.unchecked(() -> MessageDigest.getInstance("SHA-256"));
	}

	public static MessageDigest getMessageDigestSHA512() {
		return Throws.unchecked(() -> MessageDigest.getInstance("SHA-512"));
	}

	public static Cipher getAESCipher() {
		return Throws.unchecked(() -> Cipher.getInstance("AES/ECB/PKCS5Padding"));
	}

	public static Signature getSignatureMD5WithRSA() {
		return Throws.unchecked(() -> Signature.getInstance("MD5WithRSA"));
	}

	public static Signature getSignatureSHA256WithRSA() {
		return Throws.unchecked(() -> Signature.getInstance("SHA256WithRSA"));
	}

	public static Signature getSignatureSHA512WithRSA() {
		return Throws.unchecked(() -> Signature.getInstance("SHA512WithRSA"));
	}

	public static Mac getHmacMD5() {
		return Throws.unchecked(() -> Mac.getInstance("HmacMD5"));
	}

	public static Mac getHmacSHA256() {
		return Throws.unchecked(() -> Mac.getInstance("HmacSHA256"));
	}

	public static Mac getHmacSHA512() {
		return Throws.unchecked(() -> Mac.getInstance("HmacSHA512"));
	}

	public static Bytes hashFNV(Object... values) {
		return hash(FNV.messageDigest(), values);
	}

	public static Bytes hashMD5(Object... values) {
		return hash(getMessageDigestMD5(), values);
	}

	public static Bytes hashSHA512(Object... values) {
		return hash(getMessageDigestSHA512(), values);
	}

	public static Bytes hash(MessageDigest messageDigest, Object... values) {
		Objects.requireNonNull(messageDigest);
		if (values == null)
			return Bytes.from(messageDigest.digest());
		update(messageDigest, values);
		return Bytes.from(messageDigest.digest());
	}

	public static void update(MessageDigest messageDigest, Object input) {
		MessageDigestUpdater.get().accept(messageDigest, input);
	}

	public static Bytes createSignature(Mac hmac, String secret, Bytes data) {
		Validate.isTrue(StringUtils.isNotBlank(secret), "secret is required");
		return createSignature(hmac, Bits.from(secret), data);
	}

	public static Bytes createSignature(Mac hmac, Bytes secret, Bytes data) {
		Objects.requireNonNull(hmac);
		Objects.requireNonNull(secret);
		Objects.requireNonNull(data);
		SecretKeySpec keySpec = new SecretKeySpec(secret.array(), hmac.getAlgorithm());
		Throws.unchecked(() -> hmac.init(keySpec));
		byte[] macData = hmac.doFinal(data.array());
		return Bytes.from(macData);
	}

	public static Function<Bytes, Bytes> createSignatureFunction(Mac hmac, String secret) {
		return b -> Crypto.createSignature(hmac, secret, b);
	}

	public static Function<Bytes, Bytes> createSignatureFunction(Mac hmac, Bytes secret) {
		return b -> Crypto.createSignature(hmac, secret, b);
	}

	public static boolean verifySignature(Signature signature, PublicKey publicKey, Bytes expectedSignature,
			Bytes payload) {
		Objects.requireNonNull(signature);
		Objects.requireNonNull(publicKey);
		Objects.requireNonNull(expectedSignature);
		Objects.requireNonNull(payload);
		// throw here, as we want to know if key is invalid
		Throws.unchecked(() -> signature.initVerify(publicKey));
		try {
			signature.update(payload.array());
			return signature.verify(expectedSignature.array());
		} catch (SignatureException e) {
			// suppress if validation fails
		}
		return false;
	}

	public static Bytes sign(Signature signature, PrivateKey privateKey, Bytes payload) {
		Objects.requireNonNull(signature);
		Objects.requireNonNull(privateKey);
		Objects.requireNonNull(payload);
		try {
			signature.initSign(privateKey);
			signature.update(payload.array());
			return Bytes.from(signature.sign());
		} catch (SignatureException | InvalidKeyException e) {
			throw Exceptions.asRuntimeException(e);
		}
	}

	public static RSAKeyPair generateRSAKeyPair() {
		try {
			KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
			kpg.initialize(4096);
			KeyPair keyPair = kpg.generateKeyPair();
			return RSAKeyPair.create(keyPair);
		} catch (NoSuchAlgorithmException e) {
			throw Exceptions.asRuntimeException(e);
		}

	}

	public static RSAPublicKey generateRSAPublicKey(RSAPrivateKey rsaPrivateKey) {
		Objects.requireNonNull(rsaPrivateKey);
		var rsaPrivateCrtKey = Types.tryCast(rsaPrivateKey, RSAPrivateCrtKey.class).orElse(null);
		Validate.isTrue(rsaPrivateCrtKey != null, "invalid key type:%s", rsaPrivateKey.getClass().getName());
		PublicKey publicKey;
		try {
			publicKey = KeyFactory.getInstance("RSA")
					.generatePublic(
							new RSAPublicKeySpec(rsaPrivateCrtKey.getModulus(), rsaPrivateCrtKey.getPublicExponent()));
		} catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
			throw Exceptions.asRuntimeException(e);
		}
		return (RSAPublicKey) publicKey;
	}

	public static SecretKeySpec createSecretKey() {
		return createSecretKey(256);
	}

	public static SecretKeySpec createSecretKey(int length) {
		byte[] barr = new byte[length];
		Crypto.getSecureRandom().nextBytes(barr);
		return createSecretKey(Bytes.from(barr));
	}

	public static SecretKeySpec createSecretKey(Bytes keyBytes) {
		Objects.requireNonNull(keyBytes, "key bytes required");
		var messageDigest = getMessageDigestSHA512();
		var barr = messageDigest.digest(keyBytes.array());
		barr = Arrays.copyOf(barr, 16);
		return new SecretKeySpec(barr, "AES");
	}

	public static String encodeSecretKeySpec(SecretKeySpec secretKeySpec) {
		Objects.requireNonNull(secretKeySpec);
		String encodedKey = Base64.getEncoder().encodeToString(secretKeySpec.getEncoded());
		return encodedKey;
	}

	public static SecretKeySpec decodeSecretKeySpec(String encodedKey) {
		Validate.notBlank(encodedKey);
		byte[] decodedKey = Base64.getDecoder().decode(encodedKey);
		return new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
	}

	public static Bytes encrypt(SecretKeySpec secretKey, Bytes data)
			throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
		Objects.requireNonNull(secretKey);
		Objects.requireNonNull(data);
		Cipher cipher = getAESCipher();
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		var result = cipher.doFinal(data.array());
		return Bytes.from(result);

	}

	public static Bytes decrypt(SecretKeySpec secretKey, Bytes data)
			throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
		Objects.requireNonNull(secretKey);
		Objects.requireNonNull(data);
		Cipher cipher = getAESCipher();
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		var result = cipher.doFinal(data.array());
		return Bytes.from(result);
	}

	public static String toString(Certificate... certificates) {
		return encodePEM("CERTIFICATE", Utils.Lots.stream(certificates).nonNull().map(t -> {
			try {
				return t.getEncoded();
			} catch (CertificateEncodingException e1) {
				throw RuntimeException.class.isInstance(e1) ? RuntimeException.class.cast(e1)
						: new RuntimeException(e1);
			}
		}));
	}

	public static StreamEx<? extends Certificate> parseCertificates(String... input) {
		return Utils.Lots.stream(input).map(CertificateParser::parse).chain(Utils.Lots::flatMapIterables);
	}

	public static StreamEx<? extends Certificate> parseCertificates(Bytes... input) {
		return Utils.Lots.stream(input).map(CertificateParser::parse).chain(Utils.Lots::flatMapIterables);
	}

	public static StreamEx<? extends Certificate> parseCertificates(InputStream... input) {
		return Utils.Lots.stream(input).map(t -> {
			try {
				return CertificateParser.parse(t);
			} catch (IOException e) {
				throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e) : new RuntimeException(e);
			}
		}).chain(Utils.Lots::flatMapIterables);
	}

	public static String encodePEM(Certificate... certificates) {
		return encodePEM("CERTIFICATE", Utils.Lots.stream(certificates).nonNull().map(t -> {
			try {
				return t.getEncoded();
			} catch (CertificateEncodingException e1) {
				throw RuntimeException.class.isInstance(e1) ? RuntimeException.class.cast(e1)
						: new RuntimeException(e1);
			}
		}));
	}

	public static String encodePEM(PrivateKey... privateKeys) {
		return encodePEM("PRIVATE KEY", Utils.Lots.stream(privateKeys).nonNull().map(Key::getEncoded));
	}

	public static String encodePEM(PublicKey... publicKeys) {
		return encodePEM("PUBLIC KEY", Utils.Lots.stream(publicKeys).nonNull().map(Key::getEncoded));
	}

	private static String encodePEM(String name, Iterable<byte[]> encodedValues) {
		if (encodedValues == null)
			return "";
		var lines = new ArrayList<String>();
		var newLine = Objects.toString(Utils.Strings.newLine());
		for (var encodedValue : encodedValues) {
			if (encodedValue == null)
				continue;
			lines.add(String.format("-----BEGIN %s-----", name));
			lines.add(java.util.Base64.getMimeEncoder(64, newLine.getBytes(Utils.Bits.getDefaultCharset()))
					.encodeToString(encodedValue));
			lines.add(String.format("-----END %s-----", name));
		}
		return lines.stream().collect(Collectors.joining(newLine));
	}

	public static void main(String[] args) {

		for (int i = 0; i < 5; i++) {
			var random = getRandom();
			var val = random.nextInt(5);
			System.out.println(val);
		}
		{
			byte[] barr = "thisis".getBytes(StandardCharsets.UTF_8);
			Bytes hash = hash(getMessageDigestSHA512(), barr, null, Bytes.from("atest"), "");
			System.out.println(hash.encodeHex());
		}
		{
			byte[] barr = "thisis".getBytes(StandardCharsets.UTF_8);
			Byte[] boxed = IntStreamEx.range(barr.length).mapToObj(i -> barr[i]).toArray(Byte[]::new);
			Bytes hash = hash(getMessageDigestSHA512(), boxed, null, Bytes.from("atest"), "");
			System.out.println(hash.encodeHex());
		}
		{
			Bytes hash = hash(getMessageDigestSHA512(),
					(Object) new Object[] { new String[] { null, "this", "is", "a", "test" } });
			System.out.println(hash.encodeHex());
		}
		{
			Bytes hash = hash(getMessageDigestSHA512(), "this", "is", "a", "test");
			System.out.println(hash.encodeHex());
		}
		{
			Bytes hash = hash(getMessageDigestSHA512(), List.of("this", "is", "a", "test"));
			System.out.println(hash.encodeHex());
		}

		{
			Bytes hash = hash(getMessageDigestSHA512(), "thisis", null, "atest");
			System.out.println(hash.encodeHex());
		}
		{
			Bytes hash = hash(getMessageDigestSHA512(), "thisis", null, new ByteArrayInputStream("atest".getBytes()),
					"");
			System.out.println(hash.encodeHex());
		}

		{
			Bytes hash = hash(getMessageDigestSHA512(), "thisis", null,
					new File("C:\\Users\\reggi\\Desktop\\test.txt"));
			System.out.println("photo");
			System.out.println(hash.encodeHex());
		}
	}
}
