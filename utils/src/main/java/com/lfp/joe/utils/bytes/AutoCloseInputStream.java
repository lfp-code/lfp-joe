package com.lfp.joe.utils.bytes;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.input.ClosedInputStream;

import com.lfp.joe.core.function.Throws;
import com.lfp.joe.utils.Bits;

/**
 * Proxy stream that closes and discards the underlying stream as soon as the
 * end of input has been reached or when the stream is explicitly closed. Not
 * even a reference to the underlying stream is kept after it has been closed,
 * so any allocated in-memory buffers can be freed even if the client
 * application still keeps a reference to the proxy stream.
 * <p>
 * This class is typically used to release any resources related to an open
 * stream as soon as possible even if the client application (by not explicitly
 * closing the stream when no longer needed) or the underlying stream (by not
 * releasing resources once the last byte has been read) do not do that.
 *
 * @version $Id: AutoCloseInputStream.java 1586350 2014-04-10 15:57:20Z ggregory
 *          $
 * @since 1.4
 */
public class AutoCloseInputStream extends CompletionNotifierInputStream {

	private static final InputStream CLOSED_INPUT_STREAM;
	static {
		var bais = new ByteArrayInputStream(new byte[0]);
		Throws.unchecked(bais::close);
		CLOSED_INPUT_STREAM = bais;
	}

	/**
	 * Creates an automatically closing proxy for the given input stream.
	 *
	 * @param in underlying input stream
	 */
	public AutoCloseInputStream(final InputStream in) {
		super(in);
	}

	/**
	 * Closes the underlying input stream and replaces the reference to it with a
	 * {@link ClosedInputStream} instance.
	 * <p>
	 * This method is automatically called by the read methods when the end of input
	 * has been reached.
	 * <p>
	 * Note that it is safe to call this method any number of times. The original
	 * underlying input stream is closed and discarded only once when this method is
	 * first called.
	 *
	 * @throws IOException if the underlying input stream can not be closed
	 */
	@Override
	public void close() throws IOException {
		super.close();
		in = CLOSED_INPUT_STREAM;
	}

	/**
	 * Automatically closes the stream if the end of stream was reached.
	 *
	 * @param n number of bytes read, or -1 if no more bytes are available
	 * @throws IOException if the stream could not be closed
	 * @since 2.0
	 */
	@Override
	protected void afterRead(final int n) throws IOException {
		super.afterRead(n);
		if (n == Bits.endOfFile())
			close();
	}

	/**
	 * Ensures that the stream is closed before it gets garbage-collected. As
	 * mentioned in {@link #close()}, this is a no-op if the stream has already been
	 * closed.
	 *
	 * @throws Throwable if an error occurs
	 */
	@Override
	protected void finalize() throws Throwable {
		close();
		super.finalize();
	}

}