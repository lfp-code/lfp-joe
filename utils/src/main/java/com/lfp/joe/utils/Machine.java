package com.lfp.joe.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.process.ShutdownNotifier;

import one.util.streamex.EntryStream;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;
import oshi.SystemInfo;
import oshi.hardware.CentralProcessor;
import oshi.software.os.OperatingSystem;
import oshi.software.os.linux.LinuxOperatingSystem;
import oshi.software.os.mac.MacOperatingSystem;
import oshi.software.os.unix.freebsd.FreeBsdOperatingSystem;
import oshi.software.os.unix.solaris.SolarisOperatingSystem;
import oshi.software.os.windows.WindowsOperatingSystem;

public class Machine {
	protected Machine() {
	};

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();

	private static final MemoizedSupplier<SystemInfo> SYSTEM_INFO_S = Utils.Functions.memoize(() -> {
		return new SystemInfo();
	});

	private static final MemoizedSupplier<Integer> LOGICAL_PROCESSOR_COUNT_S = Utils.Functions.memoize(() -> {
		try {
			SystemInfo systemInfo = SYSTEM_INFO_S.get();
			CentralProcessor processor = systemInfo.getHardware().getProcessor();
			return processor.getLogicalProcessorCount();
		} catch (Throwable t) {
			org.slf4j.LoggerFactory.getLogger(THIS_CLASS).warn(
					"core count lookup failed. falling back from {} to {}. msg:{}", SystemInfo.class.getName(),
					MachineConfig.class.getName(), t.getMessage());
			return MachineConfig.logicalCoreCount();
		}
	});
	private static final MemoizedSupplier<Consumer<Map<String, Object>>> ENVIRONMENT_VARIABLE_SETTER_S = Utils.Functions
			.memoize(() -> {
				return createEnvironmentSetter();
			}, null);

	public static SystemInfo systemInfo() {
		return SYSTEM_INFO_S.get();
	}

	public static int logicalProcessorCount() {
		return LOGICAL_PROCESSOR_COUNT_S.get();
	}

	public static boolean isOperatingSystem(Class<? extends OperatingSystem> operatingSystemType) {
		Objects.requireNonNull(operatingSystemType);
		OperatingSystem os = systemInfo().getOperatingSystem();
		return operatingSystemType.isAssignableFrom(os.getClass());
	}

	public static boolean isWindows() {
		return isOperatingSystem(WindowsOperatingSystem.class);
	}

	public static boolean isMac() {
		return isOperatingSystem(MacOperatingSystem.class);
	}

	public static boolean isLinux() {
		return isOperatingSystem(LinuxOperatingSystem.class);
	}

	public static boolean isUnix() {
		if (isMac() || isLinux() || isOperatingSystem(FreeBsdOperatingSystem.class)
				|| isOperatingSystem(SolarisOperatingSystem.class))
			return true;
		return false;
	}

	public static boolean isShuttingDown() {
		return ShutdownNotifier.INSTANCE.isShuttingDown();
	}

	public static boolean addShutdownListener(AutoCloseable listener) {
		return ShutdownNotifier.INSTANCE.addListener(listener);
	}

	public static boolean removeShutdownListener(AutoCloseable listener) {
		return ShutdownNotifier.INSTANCE.removeListener(listener);
	}

	public static Map<String, String> getEnvironmentVariables(String... readFilePostfixes) {
		if (readFilePostfixes == null || readFilePostfixes.length == 0)
			return Collections.unmodifiableMap(System.getenv());
		var env = streamEnvironmentVariables((String) null, readFilePostfixes).flatMapValues(Collection::stream)
				.toCustomMap(LinkedHashMap::new);
		return Collections.unmodifiableMap(env);
	}

	public static EntryStream<String, String> streamEnvironmentVariables() {
		return streamEnvironmentVariables((String) null).flatMapValues(Collection::stream);
	}

	public static EntryStream<String, List<String>> streamEnvironmentVariables(String indexDelimiter,
			String... readFilePostfixes) {
		return streamEnvironmentVariables(System.getenv(), indexDelimiter, readFilePostfixes);
	}

	public static EntryStream<String, String> streamEnvironmentVariables(InputStream inputStream) throws IOException {
		return streamEnvironmentVariables(inputStream, null).flatMapValues(Collection::stream);
	}

	public static EntryStream<String, List<String>> streamEnvironmentVariables(InputStream inputStream,
			String indexDelimiter, String... readFilePostfixes) throws IOException {
		Map<?, ?> env;
		if (inputStream == null)
			env = null;
		else {
			Properties properties = new Properties();
			try (inputStream) {
				properties.load(inputStream);
			}
			env = properties;
		}
		return streamEnvironmentVariables(env, indexDelimiter, readFilePostfixes);
	}

	public static EntryStream<String, List<String>> streamEnvironmentVariables(Map<?, ?> env, String indexDelimiter,
			String... readFilePostfixes) {
		Function<String, Iterable<? extends String>> readFileNameFunction;
		if (readFilePostfixes == null || readFilePostfixes.length == 0)
			readFileNameFunction = null;
		else
			readFileNameFunction = name -> {
				var readFilePostfixesStream = StreamEx.of(readFilePostfixes);
				readFilePostfixesStream = readFilePostfixesStream.nonNull();
				readFilePostfixesStream = readFilePostfixesStream.filter(Predicate.not(String::isEmpty));
				readFilePostfixesStream = readFilePostfixesStream.distinct();
				return readFilePostfixesStream.map(readFilePostfix -> {
					var splitAt = name.lastIndexOf(readFilePostfix);
					if (splitAt < 0)
						return null;
					var nameNew = name.substring(0, splitAt);
					return nameNew;
				});
			};
		return streamEnvironmentVariables(env, indexDelimiter, readFileNameFunction);
	}

	public static EntryStream<String, List<String>> streamEnvironmentVariables(Map<?, ?> env, String indexDelimiter,
			Function<String, Iterable<? extends String>> readFileNameFunction) {
		if (env == null || env.isEmpty())
			return EntryStream.empty();
		EntryStream<String, String> nameValueStream = Utils.Lots.stream(env).nonNullKeys().mapKeys(Object::toString)
				.nonNullValues().mapValues(Object::toString);
		nameValueStream = nameValueStream.filterKeys(Utils.Strings::isNotBlank).filterValues(Utils.Strings::isNotBlank);
		if (readFileNameFunction == null)
			return nameValueStream.mapValues(List::of);
		Predicate<Object> notNullOrEmpty = v -> {
			if (v == null)
				return false;
			if (v instanceof CharSequence && v.toString().isBlank())
				return false;
			return true;
		};
		nameValueStream = nameValueStream.nonNullKeys().mapValues(v -> v != null ? v : "");
		StreamEx<EntryStream<String, String>> nameValueStreams = nameValueStream.map(ent -> {
			var name = ent.getKey();
			var value = ent.getValue();
			if (value.isEmpty())
				return EntryStream.of(name, value);
			var nameNewStream = Optional.ofNullable(readFileNameFunction.apply(name)).map(Iterable::iterator)
					.map(StreamEx::of).orElseGet(StreamEx::empty).map(v -> (String) v);
			nameNewStream = nameNewStream.filter(notNullOrEmpty);
			nameNewStream = nameNewStream.filter(v -> !notNullOrEmpty.test(env.get(v)));
			EntryStream<String, String> estream = nameNewStream.mapToEntry(nameNew -> {
				File valueFile;
				try {
					valueFile = Optional.ofNullable(Paths.get(value)).map(Path::toFile).filter(File::exists)
							.orElse(null);
				} catch (InvalidPathException ex) {
					return null;
				}
				if (valueFile == null)
					return null;
				try {
					return Files.readFileToString(valueFile, StandardCharsets.UTF_8);
				} catch (IOException e) {
					throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e)
							: new RuntimeException(e);
				}
			});
			estream = estream.prepend(Map.of(name, value));
			return estream;
		});
		nameValueStream = nameValueStreams.flatMap(Function.identity()).mapToEntry(Entry::getKey, Entry::getValue)
				.nonNullValues();
		if (!notNullOrEmpty.test(indexDelimiter))
			return nameValueStream.mapValues(List::of);
		EntryStream<String, Entry<String, Integer>> nameValueIndexStream = nameValueStream.map(ent -> {
			var name = ent.getKey();
			var value = ent.getValue();
			var splitAt = name.lastIndexOf(indexDelimiter);
			if (splitAt < 0)
				return Map.entry(name, Map.entry(value, 0));
			var indexStr = name.substring(splitAt + indexDelimiter.length());
			if (!notNullOrEmpty.test(indexStr) || !indexStr.matches("\\d+"))
				return Map.entry(name, Map.entry(value, 0));
			var index = Integer.valueOf(indexStr);
			var nameNew = name.substring(0, splitAt);
			if (!notNullOrEmpty.test(nameNew))
				return Map.entry(name, Map.entry(value, 0));
			return Map.entry(nameNew, Map.entry(value, index));
		}).mapToEntry(Entry::getKey, Entry::getValue);
		var nameValueIndexMap = nameValueIndexStream.grouping();
		return EntryStream.of(nameValueIndexMap).mapValues(v -> {
			var maxIndex = StreamEx.of(v).mapToInt(Entry::getValue).max().orElse(0);
			if (maxIndex == 0)
				return StreamEx.of(v).map(Entry::getKey).toImmutableList();
			return IntStreamEx.range(maxIndex + 1).mapToObj(i -> {
				return StreamEx.of(v).filter(ent -> Objects.equals(i, ent.getValue())).map(Entry::getKey).findFirst()
						.orElse("");
			}).toImmutableList();
		});
	}

	public static void setEnvironmentVariable(String key, Object value) {
		var envMap = new HashMap<String, Object>(1);
		envMap.put(key, value);
		setEnvironmentVariables(envMap);
	}

	public static void setEnvironmentVariables(Map<String, Object> envMap) {
		ENVIRONMENT_VARIABLE_SETTER_S.get().accept(envMap);
	}

	@SuppressWarnings("unchecked")
	private static Consumer<Map<String, Object>> createEnvironmentSetter() throws IllegalArgumentException,
			IllegalAccessException, ClassNotFoundException, NoSuchFieldException, SecurityException {
		List<Map<String, String>> maps = new ArrayList<>();
		try {
			var processEnvironmentClass = CoreReflections.tryForName("java.lang.ProcessEnvironment").get();
			Field theEnvironmentField = processEnvironmentClass.getDeclaredField("theEnvironment");
			theEnvironmentField.setAccessible(true);
			maps.add((Map<String, String>) theEnvironmentField.get(null));
			Field theCaseInsensitiveEnvironmentField = processEnvironmentClass
					.getDeclaredField("theCaseInsensitiveEnvironment");
			theCaseInsensitiveEnvironmentField.setAccessible(true);
			maps.add((Map<String, String>) theCaseInsensitiveEnvironmentField.get(null));
		} catch (Exception e) {
			maps.clear();
			var unmodifiableMapClass = CoreReflections.tryForName("java.util.Collections$UnmodifiableMap").get();
			Field mField = unmodifiableMapClass.getDeclaredField("m");
			mField.setAccessible(true);
			maps.add((Map<String, String>) mField.get(System.getenv()));
		}
		maps.removeIf(Objects::isNull);
		Validate.isTrue(!maps.isEmpty(), "writable environment variable map not found");
		return putMap -> {
			if (putMap == null)
				return;
			for (var ent : putMap.entrySet()) {
				if (ent.getKey() == null || ent.getKey().isEmpty())
					continue;
				for (var map : maps)
					if (ent.getValue() == null)
						map.remove(ent.getKey());
					else
						map.put(ent.getKey(), Objects.toString(ent.getValue()));
			}
		};
	}

	public static void main(String[] args) {
		System.out.println(CoreReflections.tryForName("java.lang.ProcessEnvironment").get());
		System.out.println(CoreReflections.tryForName("java.util.Collections$UnmodifiableMap").get());
		setEnvironmentVariable("cool", "dude");
		System.out.println(System.getenv("cool"));
	}
}
