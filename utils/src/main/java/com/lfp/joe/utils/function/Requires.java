package com.lfp.joe.utils.function;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.core.function.Asserts;
import com.lfp.joe.core.function.Asserts.AssertException;
import com.lfp.joe.core.function.Asserts.FailureContext;
import com.lfp.joe.utils.Utils;

public class Requires extends Validate {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static <I, X extends I> X isInstanceOf(I input, Class<X> classType) throws AssertException {
		return isInstanceOf(input, classType, null);
	}

	@SuppressWarnings("unchecked")
	public static <I, X extends I> X isInstanceOf(I input, Class<X> classType, Consumer<FailureContext<I>> callback)
			throws AssertException {
		return (X) Asserts.Ext.assertion(String.format("isInstanceOf[%s]", classType), input, v -> {
			return classType != null && v != null && Utils.Types.isAssignableFrom(classType, v.getClass());
		}, callback);
	}

	public static <X extends Number> X isPositive(X input) throws AssertException {
		return isPositive(input, null);
	}

	public static <X extends Number> X isPositive(X input, Consumer<FailureContext<X>> callback)
			throws AssertException {
		return Asserts.Ext.assertion("isPositive", input, v -> {
			return v != null && v.longValue() > 0;
		}, callback);
	}

	public static <X extends Number> X isPositiveOrNegativeOne(X input) {
		return isPositiveOrNegativeOne(input, null);
	}

	public static <X extends Number> X isPositiveOrNegativeOne(X input, Consumer<FailureContext<X>> callback)
			throws AssertException {
		return Asserts.Ext.assertion("isPositiveOrNegativeOne", input, v -> {
			if (v == null)
				return false;
			var longValue = v.longValue();
			if (longValue > 0 || Objects.equals(longValue, -1))
				return true;
			return false;
		}, callback);
	}

	/*
	 * Assert delegates
	 */

	public static <X> X nonNull(X value) throws AssertException {
		return Asserts.nonNull(value);
	}

	public static <X> X nonNull(X value, Consumer<FailureContext<X>> failureCallback) throws AssertException {
		return Asserts.nonNull(value, failureCallback);
	}

	public static void isTrue(Boolean expression) throws AssertException {
		Asserts.isTrue(expression);
	}

	public static void isTrue(Boolean expression, Consumer<FailureContext<Boolean>> failureCallback)
			throws AssertException {
		Asserts.isTrue(expression, failureCallback);
	}

	public static <X> X validate(X value, Predicate<X> validator) throws AssertException {
		return Asserts.validate(value, validator);
	}

	public static <X> X validate(X value, Predicate<X> validator, Consumer<FailureContext<X>> failureCallback)
			throws AssertException {
		return Asserts.validate(value, validator, failureCallback);
	}

	public static void main(String[] args) {
		System.out.println(Double.valueOf(2.5).longValue());
		Requires.isPositive(2);
		Requires.isTrue(Boolean.FALSE);
	}

}
