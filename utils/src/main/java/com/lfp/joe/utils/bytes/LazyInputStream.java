package com.lfp.joe.utils.bytes;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Optional;

import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

public class LazyInputStream extends InputStream {

	private final Object inputStreamMutex = new Object();
	private InputStream inputStream;
	protected ThrowingSupplier<InputStream, IOException> inputStreamSupplier;

	protected LazyInputStream() {}

	public LazyInputStream(ThrowingSupplier<InputStream, IOException> inputStreamSupplier) {
		this.inputStreamSupplier = inputStreamSupplier;
	}

	private final InputStream getDelegate() throws IOException {
		return getDelegate(false);
	}

	private final InputStream getDelegate(boolean disableCreate) throws IOException {
		if (inputStream == null)
			synchronized (inputStreamMutex) {
				if (inputStream == null) {
					var is = disableCreate || inputStreamSupplier == null ? null : inputStreamSupplier.get();
					this.inputStream = Optional.ofNullable(is).orElseGet(InputStream::nullInputStream);
					this.inputStreamSupplier = null;
				}
			}
		return inputStream;
	}

	@Override
	public void close() throws IOException {
		close(false);
	}

	public void close(boolean disableCreate) throws IOException {
		getDelegate(disableCreate).close();
	}

	@Override
	public String toString() {
		return Optional.ofNullable(inputStream).map(Object::toString).map(v -> {
			return String.format("%s{inputStream=%s}", this.getClass().getSimpleName(), v);
		}).orElseGet(() -> {
			return super.toString();
		});
	}

	// unchecked delegates

	@Override
	public boolean markSupported() {
		return Throws.unchecked(() -> getDelegate()).markSupported();
	}

	@Override
	public void mark(int readlimit) {
		Throws.unchecked(() -> getDelegate()).mark(readlimit);
	}

	// throwing delegates

	@Override
	public int read() throws IOException {
		return getDelegate().read();
	}

	@Override
	public int read(byte[] b) throws IOException {
		return getDelegate().read(b);
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		return getDelegate().read(b, off, len);
	}

	@Override
	public byte[] readAllBytes() throws IOException {
		return getDelegate().readAllBytes();
	}

	@Override
	public byte[] readNBytes(int len) throws IOException {
		return getDelegate().readNBytes(len);
	}

	@Override
	public int readNBytes(byte[] b, int off, int len) throws IOException {
		return getDelegate().readNBytes(b, off, len);
	}

	@Override
	public long skip(long n) throws IOException {
		return getDelegate().skip(n);
	}

	@Override
	public int available() throws IOException {
		return getDelegate().available();
	}

	@Override
	public void reset() throws IOException {
		getDelegate().reset();
	}

	@Override
	public long transferTo(OutputStream out) throws IOException {
		return getDelegate().transferTo(out);
	}

}
