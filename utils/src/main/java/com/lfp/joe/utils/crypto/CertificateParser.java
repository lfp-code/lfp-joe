package com.lfp.joe.utils.crypto;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Supplier;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.StreamEx;

public class CertificateParser {

	protected CertificateParser() {}

	private static final String BEGIN_CERTIFICATE_TOKEN = "-BEGIN CERTIFICATE-";
	private static final String END_CERTIFICATE_TOKEN = "-END CERTIFICATE-";
	private static final String DELIM_PADDING = "----";
	private static final String BEGIN_CERTIFICATE_DELIM = String.format("%s%s%s", DELIM_PADDING,
			BEGIN_CERTIFICATE_TOKEN, DELIM_PADDING);
	private static final String END_CERTIFICATE_DELIM = String.format("%s%s%s", DELIM_PADDING, END_CERTIFICATE_TOKEN,
			DELIM_PADDING);

	private static final MemoizedSupplier<List<String>> CERTIFICATE_FACTORY_TYPE_LIST_S = Utils.Functions
			.memoize(() -> {
				List<String> types = new ArrayList<>();
				var providers = Security.getProviders();
				for (var provider : providers) {
					for (var service : provider.getServices()) {
						for (var type : Arrays.asList(service.getAlgorithm(), service.getType())) {
							if (Utils.Strings.isBlank(type))
								continue;
							var factory = Utils.Functions.catching(() -> CertificateFactory.getInstance(type),
									t -> null);
							if (factory != null)
								types.add(type);
						}
					}
				}
				var typeStream = Utils.Lots.stream(types);
				Comparator<String> sorter = (v1, v2) -> 0;
				sorter = sorter.thenComparing(v -> {
					if (Utils.Strings.equalsIgnoreCase(v, "x.509"))
						return 0;
					if (Utils.Strings.equalsIgnoreCase(v, "x509"))
						return 1;
					return Integer.MAX_VALUE;
				});
				sorter = sorter.thenComparing(v -> {
					return -1 * v.chars().map(vv -> Character.isUpperCase(vv) ? 1 : 0).sum();
				});
				typeStream = typeStream.sorted(sorter);
				typeStream = typeStream.distinct(String::toLowerCase);
				return typeStream.toImmutableList();
			});

	public static List<? extends Certificate> parse(String input) {
		if (Utils.Strings.isBlank(input))
			return List.of();
		var candidateStream = StreamEx.of(false, true).map(base64Decode -> {
			if (!base64Decode)
				return input;
			else if (Utils.Bits.isBase64(input))
				return Utils.Functions.catching(() -> Utils.Bits.parseBase64(input).encodeUtf8(), t -> null);
			else
				return null;
		});
		candidateStream = candidateStream.filter(Utils.Strings::isNotBlank).distinct();
		for (var candidate : candidateStream) {
			var bytes = Utils.Bits.from(candidate);
			var certList = parse(bytes);
			if (!certList.isEmpty())
				return certList;
		}
		return List.of();
	}

	public static List<? extends Certificate> parse(Bytes bytes) {
		if (bytes == null)
			return List.of();
		var bytesStream = Streams.<Supplier<Bytes>>of(() -> bytes, () -> {
			String input;
			try {
				input = bytes.encodeUtf8();
			} catch (Throwable t) {
				return null;
			}
			if (!Utils.Strings.contains(input, BEGIN_CERTIFICATE_TOKEN))
				input = BEGIN_CERTIFICATE_DELIM + Utils.Strings.newLine() + input;
			if (!Utils.Strings.contains(input, END_CERTIFICATE_TOKEN))
				input = input + Utils.Strings.newLine() + END_CERTIFICATE_DELIM;
			return Utils.Bits.from(input);

		}).map(Supplier::get).nonNull().distinct();
		return bytesStream.map(v -> {
			return Throws.unchecked(() -> parse(v.inputStream()));
		}).filter(v -> v.size() > 0).findFirst().orElseGet(List::of);
	}

	public static List<? extends Certificate> parse(InputStream certificateInputStream) throws IOException {
		if (certificateInputStream == null)
			return List.of();
		var types = CERTIFICATE_FACTORY_TYPE_LIST_S.get();
		if (types.isEmpty())
			return List.of();
		Supplier<InputStream> certificateInputStreamSupplier;
		if (types.size() == 1)
			certificateInputStreamSupplier = () -> certificateInputStream;
		else {
			byte[] barr;
			try (certificateInputStream; var baos = new ByteArrayOutputStream();) {
				certificateInputStream.transferTo(baos);
				barr = baos.toByteArray();
			}
			certificateInputStreamSupplier = () -> new ByteArrayInputStream(barr);
		}
		for (var type : types) {
			try (var is = certificateInputStreamSupplier.get()) {
				var certs = CertificateFactory.getInstance(type).generateCertificates(is);
				var certsList = Utils.Lots.stream(certs).nonNull().toImmutableList();
				if (!certsList.isEmpty())
					return certsList;
			} catch (CertificateException e) {
				// suppress parsing errors
			}
		}
		return List.of();
	}

	public static void main(String[] args) {
		var input = "MIIEFzCCA5ygAwIBAgIRAMCE9lhniE+xfy4bi4N+NDQwCgYIKoZIzj0EAwMwSzEL\n"
				+ "MAkGA1UEBhMCQVQxEDAOBgNVBAoTB1plcm9TU0wxKjAoBgNVBAMTIVplcm9TU0wg\n"
				+ "RUNDIERvbWFpbiBTZWN1cmUgU2l0ZSBDQTAeFw0yMjAyMTUwMDAwMDBaFw0yMjA1\n"
				+ "MTYyMzU5NTlaMCUxIzAhBgNVBAMTGnJlZ2dpZS1waWVyY2UtZGV2Lmxhc3NvLnRt\n"
				+ "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEBMSlp4bnH/pVjmKUeQPoeXwUh5zE\n"
				+ "29Npj24/3T3oEkcWgSua0CPfweARDnIre7teRReuvUwZj8NWjvCJ/RvC9KOCAoUw\n"
				+ "ggKBMB8GA1UdIwQYMBaAFA9r5kvOOUeu9n6QHnnwMJGSyF+jMB0GA1UdDgQWBBTI\n"
				+ "MRoo0MJ3rb9++PwxIoUe5XXYfTAOBgNVHQ8BAf8EBAMCB4AwDAYDVR0TAQH/BAIw\n"
				+ "ADAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIwSQYDVR0gBEIwQDA0Bgsr\n"
				+ "BgEEAbIxAQICTjAlMCMGCCsGAQUFBwIBFhdodHRwczovL3NlY3RpZ28uY29tL0NQ\n"
				+ "UzAIBgZngQwBAgEwgYgGCCsGAQUFBwEBBHwwejBLBggrBgEFBQcwAoY/aHR0cDov\n"
				+ "L3plcm9zc2wuY3J0LnNlY3RpZ28uY29tL1plcm9TU0xFQ0NEb21haW5TZWN1cmVT\n"
				+ "aXRlQ0EuY3J0MCsGCCsGAQUFBzABhh9odHRwOi8vemVyb3NzbC5vY3NwLnNlY3Rp\n"
				+ "Z28uY29tMIIBAwYKKwYBBAHWeQIEAgSB9ASB8QDvAHYARqVV63X6kSAwtaKJafTz\n"
				+ "fREsQXS+/Um4havy/HD+bUcAAAF+/g+EXAAABAMARzBFAiEA4+CtpVaxVkUv/zJc\n"
				+ "C6iF7ztyPpRZC5XCIyxtFmq2IukCIEWGYv5Sj//H7Scz3HgwCJHCdmNlVkgIVjYP\n"
				+ "us7euCHiAHUAQcjKsd8iRkoQxqE6CUKHXk4xixsD6+tLx2jwkGKWBvYAAAF+/g+E\n"
				+ "4AAABAMARjBEAiAyWeGGjyXXKYU2COMgPqjRBBxExO6yaUkxiTkgH+yfRQIgcsDV\n"
				+ "HjrDLyNQ2rGIzoiTwpk9Z6c0/ZVgIDfD4dIn3wQwJQYDVR0RBB4wHIIacmVnZ2ll\n"
				+ "LXBpZXJjZS1kZXYubGFzc28udG0wCgYIKoZIzj0EAwMDaQAwZgIxALVUKM6Px/Cc\n"
				+ "9PXtsvNPCjonOPLB33OOMlUAvZQKRdiq0b7i471YgNepq9vh02A5bAIxAMsa6IkF\n"
				+ "Igm0/REc36ieucuh+CF9EBW1nKfsZsX3DQ+/vK6yjyGnpjCxIJ1WiAdEqQ==";
		var certs = parse(input);
		System.out.println(certs);
	}

}
