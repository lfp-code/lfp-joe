package com.lfp.joe.utils.bytes;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.utils.Bits;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

import at.favre.lib.bytes.Bytes;

public interface InputStreamSource extends Scrapable {

	InputStream openStream() throws IOException;

	default Bytes asBytes() throws IOException {
		try (var is = openStream()) {
			return Bits.from(is);
		}
	}

	public static InputStreamSource empty() {
		return InputStreamSource.create(() -> Bits.emptyInputStream());
	}

	public static InputStreamSource create(Bytes bytes) {
		return create(bytes == null ? null : bytes::inputStream);
	}

	public static InputStreamSource create(File file) {
		return create(file == null ? null : () -> new FileInputStreamLFP(file));
	}

	public static InputStreamSource create(ThrowingSupplier<InputStream, IOException> inputStreamSupplier) {
		return new InputStreamSource.Impl(inputStreamSupplier);
	}

	public static abstract class Abs extends Scrapable.Impl implements InputStreamSource {

	}


	public static class Impl extends Abs {

		private final ThrowingSupplier<InputStream, IOException> inputStreamSupplier;

		public Impl(ThrowingSupplier<InputStream, IOException> inputStreamSupplier) {
			super();
			this.inputStreamSupplier = Objects.requireNonNull(inputStreamSupplier);
		}

		@Override
		public InputStream openStream() throws IOException {
			return inputStreamSupplier.get();
		}

	}

}
