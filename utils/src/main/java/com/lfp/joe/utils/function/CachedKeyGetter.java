package com.lfp.joe.utils.function;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.utils.Lots;

import one.util.streamex.EntryStream;
import one.util.streamex.IntStreamEx;

public class CachedKeyGetter<X, K, V> implements Function<K, Optional<V>> {

	private final Iterable<? extends X> iterable;
	private final Function<X, Iterable<? extends K>> keyFunction;
	private final Function<X, Iterable<? extends V>> valueFunction;

	private long currentIndex = -1;
	private Iterator<? extends X> _iterator;
	private Optional<Map<K, V>> _mappingCacheOp;
	private Map<Long, K> _indexCache;

	public CachedKeyGetter(Iterable<? extends X> iterable, Function<X, Iterable<? extends K>> keyFunction,
			Function<X, Iterable<? extends V>> valueFunction) {
		this.iterable = iterable;
		this.keyFunction = keyFunction;
		this.valueFunction = valueFunction;
	}

	public EntryStream<K, V> stream() {
		return IntStreamEx.range(Integer.MAX_VALUE).mapToObj(index -> {
			var cache = getMappingCache();
			if (cache == null)
				return null;
			boolean quitAdvance = false;
			while (true) {
				var ent = getEntryAt(index);
				if (ent != null)
					return ent;
				if (quitAdvance)
					return null;
				if (!advance())
					quitAdvance = true;
			}
		}).takeWhile(Objects::nonNull).chain(EntryStreams::of);
	}

	@Override
	public Optional<V> apply(K key) {
		if (key == null || this.iterable == null || this.keyFunction == null || this.valueFunction == null)
			return Optional.empty();
		return load(key);
	}

	private Iterator<? extends X> getIterator() {
		if (_iterator == null)
			synchronized (this) {
				if (_iterator == null)
					_iterator = Optional.ofNullable(iterable.iterator()).orElse(Collections.emptyIterator());
			}
		return _iterator;
	}

	private Map<K, V> getMappingCache() {
		if (_mappingCacheOp == null)
			synchronized (this) {
				if (_mappingCacheOp == null)
					if (!getIterator().hasNext())
						_mappingCacheOp = Optional.empty();
					else
						// no need for concurrent as we have to lock for loads
						_mappingCacheOp = Optional.of(new HashMap<>());
			}
		return _mappingCacheOp.orElse(null);
	}

	private Entry<K, V> getEntryAt(long index) {
		if (index < 0)
			return null;
		var key = Optional.ofNullable(_indexCache).map(v -> v.get(index)).orElse(null);
		if (key == null)
			return null;
		var value = Optional.ofNullable(getMappingCache()).map(v -> v.get(key)).orElse(null);
		if (value == null)
			return null;
		return Lots.entry(key, value);
	}

	private Optional<V> load(K key) {
		var cache = getMappingCache();
		if (cache == null)
			return Optional.empty();
		boolean quitAdvance = false;
		while (true) {
			var fromCache = cache.get(key);
			if (fromCache != null)
				return Optional.of(fromCache);
			if (quitAdvance)
				return Optional.empty();
			if (!advance())
				quitAdvance = true;
		}
	}

	private boolean advance() {
		var mappingCache = getMappingCache();
		if (mappingCache == null)
			return false;
		synchronized (this) {
			var iterator = this.getIterator();
			while (iterator.hasNext()) {
				Iterable<? extends K> cacheKeys;
				Iterable<? extends V> cacheValues;
				{// filter out null values and keys
					var next = iterator.next();
					if (next == null)
						continue;
					cacheKeys = keyFunction.apply(next);
					if (cacheKeys == null)
						continue;
					cacheValues = valueFunction.apply(next);
					if (cacheValues == null)
						continue;
				}
				var startIndex = this.currentIndex;
				for (var cacheKey : cacheKeys) {
					if (cacheKey == null)
						continue;
					for (var cacheValue : cacheValues) {
						if (cacheValue == null)
							continue;
						mappingCache.computeIfAbsent(cacheKey, nil -> {
							if (_indexCache == null)
								_indexCache = new HashMap<>();
							this.currentIndex++;
							_indexCache.put(this.currentIndex, cacheKey);
							return cacheValue;
						});
					}
				}
				if (this.currentIndex > startIndex)
					return true;
			}
		}
		return false;
	}

}
