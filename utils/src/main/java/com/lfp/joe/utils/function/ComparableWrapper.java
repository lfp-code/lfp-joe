package com.lfp.joe.utils.function;

import java.util.Comparator;
import java.util.function.Supplier;

import com.lfp.joe.utils.Lots;

public class ComparableWrapper<X> implements Supplier<X>, Comparable<ComparableWrapper<X>> {

	private final X value;
	private final Comparator<X> comparator;

	public ComparableWrapper(X value, Comparator<X> comparator) {
		this.value = value;
		this.comparator = comparator;
	}

	@Override
	public int compareTo(ComparableWrapper<X> cw) {
		Comparator<ComparableWrapper<X>> cwComparator = Lots.comparatorNullsLast();
		if (this.comparator != null)
			cwComparator = cwComparator.thenComparing((cw1, cw2) -> {
				return this.comparator.compare(cw1.get(), cw2.get());
			});
		return cwComparator.compare(this, cw);
	}

	@Override
	public X get() {
		return this.value;
	}

}
