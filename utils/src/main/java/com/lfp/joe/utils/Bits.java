
package com.lfp.joe.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.SequenceInputStream;
import java.io.Serializable;
import java.lang.ref.SoftReference;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.IntBuffer;
import java.nio.charset.Charset;
import java.text.Normalizer;
import java.util.Arrays;
import java.util.Base64;
import java.util.BitSet;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import java.util.WeakHashMap;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.ComputeStore;
import com.lfp.joe.core.function.Throws.ThrowingBiConsumer;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.bytes.ByteBufferBackedInputStream;
import com.lfp.joe.utils.bytes.LimitedInputStream;
import com.lfp.joe.utils.bytes.LineConsumeOutputStream;

import at.favre.lib.bytes.BinaryToTextEncoding;
import at.favre.lib.bytes.Bytes;
import de.xn__ho_hia.storage_unit.StorageUnit;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;

/**
 * Auto generated static delegate class of {@link Bytes}
 *
 * @see Bytes
 */
public class Bits {

	protected Bits() {

	}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final ComputeStore<Class<?>, Optional<Entry<Method, Boolean>>> FROM_CACHE = ComputeStore
			.from(new WeakHashMap<>(), SoftReference::new);
	private static final int DEFAULT_BUFFER_SIZE = 8192;
	private static final byte DEFAULT_BYTE_STREAM_DELIMITER = 31; // unit seperator ascii
	private static final Pattern BASE_64_PATTERN = Pattern
			.compile("(?:[A-Za-z0-9+/]{4}\\n?)*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)");
	private static final Pattern HEXADECIMAL_PATTERN = Pattern.compile("[0-9a-fA-F]+");
	private static final int EOF = -1;
	private static final byte[] EMPTY_BYTE_ARRAY = new byte[0];

	public static byte[] emptyByteArray() {
		return EMPTY_BYTE_ARRAY;
	}

	public static int getDefaultBufferSize() {
		return DEFAULT_BUFFER_SIZE;
	}

	public static Charset getDefaultCharset() {
		return MachineConfig.getDefaultCharset();
	}

	public static byte getDefaultByteStreamDelimiter() {
		return DEFAULT_BYTE_STREAM_DELIMITER;
	}

	public static int getEof() {
		return EOF;
	}

	@CanIgnoreReturnValue
	public static long copy(InputStream from, OutputStream to) throws IOException {
		Objects.requireNonNull(to);
		return copy(from, (buf, len) -> to.write(buf, 0, len));
	}

	public static long copy(InputStream from, ThrowingBiConsumer<byte[], Integer, IOException> bufferConsumer)
			throws IOException {
		Objects.requireNonNull(from);
		Objects.requireNonNull(bufferConsumer);
		byte[] buf = new byte[DEFAULT_BUFFER_SIZE];
		long total = 0;
		while (true) {
			int len = from.read(buf);
			if (len == -1)
				break;
			bufferConsumer.accept(buf, len);
			total += len;
		}
		return total;
	}

	public static BufferedReader bufferedReader(InputStream inputStream) {
		var isr = new InputStreamReader(inputStream, Bits.getDefaultCharset());
		var br = new BufferedReader(isr, Bits.getDefaultBufferSize());
		return br;
	}

	public static BufferedWriter bufferedWriter(OutputStream outputStream) {
		var osw = new OutputStreamWriter(outputStream, Bits.getDefaultCharset());
		var bw = new BufferedWriter(osw, Bits.getDefaultBufferSize());
		return bw;
	}

	public static InputStream limit(InputStream inputStream, StorageUnit<?> limit) {
		return limit(inputStream, limit == null ? -1 : limit.longValue());
	}

	public static InputStream limit(InputStream inputStream, long byteLimit) {
		return limit(inputStream, byteLimit, false);
	}

	public static InputStream limit(InputStream inputStream, long byteLimit, boolean disableDelegateClose) {
		Objects.requireNonNull(inputStream);
		Validate.isTrue(byteLimit >= -1, "invalid byteLimit:%s", byteLimit);
		if (byteLimit == -1)
			return inputStream;
		else
			return new LimitedInputStream(inputStream, byteLimit, disableDelegateClose);
	}

	public static InputStream peek(InputStream inputStream, StorageUnit<?> limit,
			ThrowingFunction<InputStream, InputStream, IOException> peeker) throws IOException {
		return peek(inputStream, limit == null ? -1 : limit.longValue(), peeker);
	}

	public static InputStream peek(final InputStream inputStream, long byteLimit,
			ThrowingFunction<InputStream, InputStream, IOException> peeker) throws IOException {
		var peekedInputStream = limit(inputStream, byteLimit, true);
		if (peeker == null)
			return new SequenceInputStream(peekedInputStream, inputStream);
		if (peeker != null) {
			var modifiedInputStream = peeker.apply(peekedInputStream);
			if (modifiedInputStream != null && modifiedInputStream != peekedInputStream) {
				peekedInputStream.close();
				peekedInputStream = modifiedInputStream;
			}
		}
		return new SequenceInputStream(peekedInputStream, inputStream);
	}

	public static InputStream emptyInputStream() {
		return new ByteArrayInputStream(EMPTY_BYTE_ARRAY);
	}

	public static byte endOfFile() {
		return -1;
	}

	public static OutputStream lineConsumerOutputStream(Consumer<String> lineConsumer) {
		return lineConsumerOutputStream(lineConsumer, null);
	}

	public static OutputStream lineConsumerOutputStream(Consumer<String> lineConsumer, Charset charset) {
		Objects.requireNonNull(lineConsumer);
		if (charset == null)
			charset = getDefaultCharset();
		;
		return new LineConsumeOutputStream(charset) {

			@Override
			protected void processLine(String line) {
				lineConsumer.accept(line);
			}
		};
	}

	public static Bytes concat(Bytes... bytes) {
		if (bytes == null)
			return Bytes.empty();
		return Bytes.from(bytes);
	}

	public static byte[] concat(byte[] barr, byte singleByte) {
		if (barr == null)
			return new byte[] { singleByte };
		byte[] result = new byte[barr.length + 1];
		System.arraycopy(barr, 0, result, 0, barr.length);
		result[result.length - 1] = singleByte;
		return result;
	}

	public static byte[] concat(byte[]... barrs) {
		if (barrs == null || barrs.length == 0)
			return emptyByteArray();
		if (barrs.length == 1)
			return Optional.ofNullable(barrs[0]).orElseGet(() -> emptyByteArray());
		var length = Stream.of(barrs).filter(Objects::nonNull).mapToInt(v -> v.length).sum();
		if (length == 0)
			return emptyByteArray();
		byte[] result = new byte[length];
		int offset = 0;
		for (var barr : barrs) {
			if (barr == null)
				continue;
			System.arraycopy(barr, 0, result, offset, barr.length);
			offset += barr.length;
		}
		return result;
	}

	public static byte[] toByteArray(int value) {
		return new byte[] { (byte) ((value >> 24) & 0xff), (byte) ((value >> 16) & 0xff), (byte) ((value >> 8) & 0xff),
				(byte) ((value >> 0) & 0xff), };
	}

	public static byte[] toByteArray(long value) {
		byte[] result = new byte[Long.BYTES];
		for (int i = 7; i >= 0; i--) {
			result[i] = (byte) (value & 0xFF);
			value >>= 8;
		}
		return result;
	}

	public static Bytes from(ByteBuffer buffer) {
		if (buffer == null)
			return null;
		if (buffer.hasArray())
			return Utils.Bits.from(buffer.array());
		try (ByteBufferBackedInputStream is = new ByteBufferBackedInputStream(buffer)) {
			return Bytes.from(is);
		} catch (IOException e) {
			throw Exceptions.asRuntimeException(e);
		}
	}

	public static Optional<Bytes> tryFrom(Object value) {
		if (value == null)
			return Optional.of(Bytes.empty());
		var classType = CoreReflections.getNamedClassType(value.getClass());
		var methodToArrInvokeOp = FROM_CACHE.computeIfAbsent(classType, () -> {
			var methodStream = Streams.of(CoreReflections.streamMethods(THIS_CLASS, true))
					.append(CoreReflections.streamMethods(Bytes.class, true));
			methodStream = methodStream.filter(m -> Modifier.isStatic(m.getModifiers()));
			methodStream = methodStream.filter(m -> Modifier.isPublic(m.getModifiers()));
			methodStream = methodStream.filter(m -> "from".equals(m.getName()));
			methodStream = methodStream.filter(m -> m.getParameterCount() == 1);
			var methodToArrInvokeStream = methodStream.mapToEntry(m -> {
				Class<?> pt = m.getParameterTypes()[0];
				if (CoreReflections.isAssignableFrom(pt, classType))
					return false;
				if (pt.isArray()) {
					Class<? extends Object> arrayType = java.lang.reflect.Array.newInstance(classType, 0).getClass();
					if (CoreReflections.isAssignableFrom(pt, arrayType))
						return true;
				}
				return null;
			});
			methodToArrInvokeStream = methodToArrInvokeStream.nonNullValues();
			methodToArrInvokeStream = methodToArrInvokeStream.sortedBy(ent -> {
				return ent.getKey().getParameterTypes()[0].equals(classType) ? 0 : 1;
			});
			return methodToArrInvokeStream.findFirst();
		});
		if (methodToArrInvokeOp.isEmpty())
			return Optional.empty();
		var method = methodToArrInvokeOp.get().getKey();
		var arrInvoke = methodToArrInvokeOp.get().getValue();
		Object arg;
		if (arrInvoke) {
			Object[] arr = (Object[]) java.lang.reflect.Array.newInstance(value.getClass(), 1);
			arr[0] = value;
			arg = arr;
		} else
			arg = value;
		Bytes bytes = Functions.unchecked(() -> (Bytes) method.invoke(null, arg));
		return Optional.ofNullable(bytes);
	}

	public static Bytes from(CharSequence charSequence) {
		return Bytes.from(charSequence == null ? null : charSequence.toString().getBytes(Bits.getDefaultCharset()));
	}

	public static ByteBuffer deepCopy(ByteBuffer orig) {
		int pos = orig.position(), lim = orig.limit();
		try {
			orig.position(0).limit(orig.capacity()); // set range to entire buffer
			ByteBuffer toReturn = deepCopyVisible(orig); // deep copy range
			toReturn.position(pos).limit(lim); // set range to original
			return toReturn;
		} finally // do in finally in case something goes wrong we don't bork the orig
		{
			orig.position(pos).limit(lim); // restore original
		}
	}

	private static ByteBuffer deepCopyVisible(ByteBuffer orig) {
		int pos = orig.position();
		try {
			ByteBuffer toReturn;
			// try to maintain implementation to keep performance
			if (orig.isDirect())
				toReturn = ByteBuffer.allocateDirect(orig.remaining());
			else
				toReturn = ByteBuffer.allocate(orig.remaining());

			toReturn.put(orig);
			toReturn.order(orig.order());

			return toReturn.position(0);
		} finally {
			orig.position(pos);
		}
	}

	public static byte[][] partition(final byte[] barr, final int partitionSize) {
		final int length = barr.length;
		final byte[][] dest = new byte[(length + partitionSize - 1) / partitionSize][];
		int destIndex = 0;
		int stopIndex = 0;

		for (int startIndex = 0; startIndex + partitionSize <= length; startIndex += partitionSize) {
			stopIndex += partitionSize;
			dest[destIndex++] = Arrays.copyOfRange(barr, startIndex, stopIndex);
		}
		if (stopIndex < length)
			dest[destIndex] = Arrays.copyOfRange(barr, stopIndex, length);
		return dest;
	}

	public static List<byte[]> split(byte[] array, byte[] delimiter) {
		List<byte[]> byteArrays = new LinkedList<>();
		if (delimiter.length == 0)
			return byteArrays;
		int begin = 0;
		outer: for (int i = 0; i < array.length - delimiter.length + 1; i++) {
			for (int j = 0; j < delimiter.length; j++)
				if (array[i + j] != delimiter[j])
					continue outer;
			if (begin != i)
				byteArrays.add(Arrays.copyOfRange(array, begin, i));
			begin = i + delimiter.length;
		}
		if (begin < array.length)
			byteArrays.add(Arrays.copyOfRange(array, begin, array.length));
		return byteArrays;
	}

	public static StreamEx<Bytes> extractHexadecimal(String input) {
		return extract(input, HEXADECIMAL_PATTERN, v -> Bytes.parseHex(v));
	}

	public static StreamEx<Bytes> extractBase64(String input) {
		return extract(input, BASE_64_PATTERN, v -> Bytes.parseBase64(v));
	}

	private static StreamEx<Bytes> extract(String input, Pattern pattern, Function<String, Bytes> parser) {
		if (StringUtils.isBlank(input))
			return StreamEx.empty();
		Matcher m = pattern.matcher(input);
		StreamEx<String> matchStream = Streams.<String>produce(action -> {
			if (!m.find())
				return false;
			action.accept(m.group());
			return true;
		});
		matchStream = matchStream.filter(Utils.Strings::isNotBlank);
		var result = matchStream.map(v -> {
			var bytes = Functions.catching(() -> parser.apply(v), t -> null);
			return bytes;
		});
		result = result.nonNull().filter(v -> v.length() > 0);
		return result;
	}

	public static Bytes parseBase64Url(CharSequence base64String) {
		if (base64String == null || base64String.length() == 0)
			return Bytes.empty();
		var barr = Base64.getUrlDecoder().decode(base64String.toString());
		return Bytes.from(barr);
	}

	public static boolean isBase64(CharSequence charSequence) {
		if (charSequence == null)
			return false;
		for (int i = 0; i < charSequence.length(); i++)
			if (!isBase64(charSequence.charAt(i)))
				return false;
		return true;
	}

	public static boolean isBase64(Character c) {
		if (c == null)
			return false;
		switch (c) {
		case '+':
		case '-':
		case '/':
		case '=':
		case '_':
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case 'A':
		case 'B':
		case 'C':
		case 'D':
		case 'E':
		case 'F':
		case 'G':
		case 'H':
		case 'I':
		case 'J':
		case 'K':
		case 'L':
		case 'M':
		case 'N':
		case 'O':
		case 'P':
		case 'Q':
		case 'R':
		case 'S':
		case 'T':
		case 'U':
		case 'V':
		case 'W':
		case 'X':
		case 'Y':
		case 'Z':
		case 'a':
		case 'b':
		case 'c':
		case 'd':
		case 'e':
		case 'f':
		case 'g':
		case 'h':
		case 'i':
		case 'j':
		case 'k':
		case 'l':
		case 'm':
		case 'n':
		case 'o':
		case 'p':
		case 'q':
		case 'r':
		case 's':
		case 't':
		case 'u':
		case 'v':
		case 'w':
		case 'x':
		case 'y':
		case 'z':
			return true;
		default:
			break;
		}
		return false;
	}

	public static boolean isHexadecimal(CharSequence charSequence) {
		if (charSequence == null)
			return false;
		var length = charSequence.length();
		for (int i = 0; i < length; i++)
			if (!isHexadecimal(charSequence.charAt(i)))
				return false;
		return true;
	}

	public static boolean isHexadecimal(Character c) {
		if (c == null)
			return false;
		switch (c) {
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case 'a':
		case 'b':
		case 'c':
		case 'd':
		case 'e':
		case 'f':
		case 'A':
		case 'B':
		case 'C':
		case 'D':
		case 'E':
		case 'F':
			return true;
		default:
			return false;
		}
	}

	public static Bytes serialize(Serializable serializable) {
		if (serializable == null)
			return empty();
		try (var baos = new ByteArrayOutputStream(); var oos = new ObjectOutputStream(baos)) {
			oos.writeObject(serializable);
			return from(baos.toByteArray());
		} catch (IOException e) {
			throw Exceptions.asRuntimeException(e);
		}
	}

	public static <X extends Serializable> X deserialize(Bytes bytes) {
		return deserialize(bytes == null ? null : bytes.inputStream());
	}

	@SuppressWarnings("unchecked")
	public static <X extends Serializable> X deserialize(InputStream inputStream) {
		if (inputStream == null)
			return null;
		try (inputStream; var ois = new ObjectInputStream(inputStream)) {
			return (X) ois.readObject();
		} catch (IOException | ClassNotFoundException e) {
			throw Exceptions.asRuntimeException(e);
		}
	}

	public static StreamEx<Byte> streamBytes(byte[] barr) {
		return streamBytes(barr == null ? null : Bytes.from(barr));
	}

	public static StreamEx<Byte> streamBytes(Bytes bytes) {
		if (bytes == null || bytes.length() == 0)
			return StreamEx.empty();
		return IntStreamEx.range(0, bytes.length()).mapToObj(bytes::byteAt);
	}

	/**
	 * ++++++++++++++++++++ BEGIN STATIC DELEGATE METHODS ++++++++++++++++++++
	 **/

	/**
	 * Delegate method of {@link Bytes#allocate(int)}
	 *
	 * @see Bytes#allocate(int)
	 */
	public static Bytes allocate(int length) {
		return Bytes.allocate(length);
	}

	/**
	 * Delegate method of {@link Bytes#allocate(int, byte)}
	 *
	 * @see Bytes#allocate(int, byte)
	 */
	public static Bytes allocate(int length, byte defaultValue) {
		return Bytes.allocate(length, defaultValue);
	}

	/**
	 * Delegate method of {@link Bytes#empty()}
	 *
	 * @see Bytes#empty()
	 */
	public static Bytes empty() {
		return Bytes.empty();
	}

	/**
	 * Delegate method of {@link Bytes#wrap(Bytes)}
	 *
	 * @see Bytes#wrap(Bytes)
	 */
	public static Bytes wrap(Bytes bytes) {
		return Bytes.wrap(bytes);
	}

	/**
	 * Delegate method of {@link Bytes#wrapNullSafe(byte[])}
	 *
	 * @see Bytes#wrapNullSafe(byte[])
	 */
	public static Bytes wrapNullSafe(byte[] array) {
		return Bytes.wrapNullSafe(array);
	}

	/**
	 * Delegate method of {@link Bytes#wrap(byte[])}
	 *
	 * @see Bytes#wrap(byte[])
	 */
	public static Bytes wrap(byte[] array) {
		return Bytes.wrap(array);
	}

	/**
	 * Delegate method of {@link Bytes#wrap(byte[], ByteOrder)}
	 *
	 * @see Bytes#wrap(byte[], ByteOrder)
	 */
	public static Bytes wrap(byte[] array, ByteOrder byteOrder) {
		return Bytes.wrap(array, byteOrder);
	}

	/**
	 * Delegate method of {@link Bytes#from(byte[])}
	 *
	 * @see Bytes#from(byte[])
	 */
	public static Bytes from(byte[] byteArrayToCopy) {
		return Bytes.from(byteArrayToCopy);
	}

	/**
	 * Delegate method of {@link Bytes#fromNullSafe(byte[])}
	 *
	 * @see Bytes#fromNullSafe(byte[])
	 */
	public static Bytes fromNullSafe(byte[] byteArrayToCopy) {
		return Bytes.fromNullSafe(byteArrayToCopy);
	}

	/**
	 * Delegate method of {@link Bytes#from(byte[], int, int)}
	 *
	 * @see Bytes#from(byte[], int, int)
	 */
	public static Bytes from(byte[] array, int offset, int length) {
		return Bytes.from(array, offset, length);
	}

	/**
	 * Delegate method of {@link Bytes#from(byte[]...)}
	 *
	 * @see Bytes#from(byte[]...)
	 */
	public static Bytes from(byte[]... moreArrays) {
		return Bytes.from(moreArrays);
	}

	/**
	 * Delegate method of {@link Bytes#from(Bytes...)}
	 *
	 * @see Bytes#from(Bytes...)
	 */
	public static Bytes from(Bytes... moreBytes) {
		return Bytes.from(moreBytes);
	}

	/**
	 * Delegate method of {@link Bytes#from(Collection<Byte>)}
	 *
	 * @see Bytes#from(Collection<Byte>)
	 */
	public static Bytes from(Collection<Byte> bytesCollection) {
		return Bytes.from(bytesCollection);
	}

	/**
	 * Delegate method of {@link Bytes#from(Byte[])}
	 *
	 * @see Bytes#from(Byte[])
	 */
	public static Bytes from(Byte[] boxedObjectArray) {
		return Bytes.from(boxedObjectArray);
	}

	/**
	 * Delegate method of {@link Bytes#from(byte)}
	 *
	 * @see Bytes#from(byte)
	 */
	public static Bytes from(byte singleByte) {
		return Bytes.from(singleByte);
	}

	/**
	 * Delegate method of {@link Bytes#from(byte, byte...)}
	 *
	 * @see Bytes#from(byte, byte...)
	 */
	public static Bytes from(byte firstByte, byte... moreBytes) {
		return Bytes.from(firstByte, moreBytes);
	}

	/**
	 * Delegate method of {@link Bytes#from(boolean)}
	 *
	 * @see Bytes#from(boolean)
	 */
	public static Bytes from(boolean booleanValue) {
		return Bytes.from(booleanValue);
	}

	/**
	 * Delegate method of {@link Bytes#from(char)}
	 *
	 * @see Bytes#from(char)
	 */
	public static Bytes from(char char2Byte) {
		return Bytes.from(char2Byte);
	}

	/**
	 * Delegate method of {@link Bytes#from(short)}
	 *
	 * @see Bytes#from(short)
	 */
	public static Bytes from(short short2Byte) {
		return Bytes.from(short2Byte);
	}

	/**
	 * Delegate method of {@link Bytes#from(int)}
	 *
	 * @see Bytes#from(int)
	 */
	public static Bytes from(int integer4byte) {
		return Bytes.from(integer4byte);
	}

	/**
	 * Delegate method of {@link Bytes#from(int...)}
	 *
	 * @see Bytes#from(int...)
	 */
	public static Bytes from(int... intArray) {
		return Bytes.from(intArray);
	}

	/**
	 * Delegate method of {@link Bytes#from(long)}
	 *
	 * @see Bytes#from(long)
	 */
	public static Bytes from(long long8byte) {
		return Bytes.from(long8byte);
	}

	/**
	 * Delegate method of {@link Bytes#from(long...)}
	 *
	 * @see Bytes#from(long...)
	 */
	public static Bytes from(long... longArray) {
		return Bytes.from(longArray);
	}

	/**
	 * Delegate method of {@link Bytes#from(float)}
	 *
	 * @see Bytes#from(float)
	 */
	public static Bytes from(float float4byte) {
		return Bytes.from(float4byte);
	}

	/**
	 * Delegate method of {@link Bytes#from(double)}
	 *
	 * @see Bytes#from(double)
	 */
	public static Bytes from(double double8Byte) {
		return Bytes.from(double8Byte);
	}

	/**
	 * Delegate method of {@link Bytes#from(CharBuffer)}
	 *
	 * @see Bytes#from(CharBuffer)
	 */
	public static Bytes from(CharBuffer buffer) {
		return Bytes.from(buffer);
	}

	/**
	 * Delegate method of {@link Bytes#from(IntBuffer)}
	 *
	 * @see Bytes#from(IntBuffer)
	 */
	public static Bytes from(IntBuffer buffer) {
		return Bytes.from(buffer);
	}

	/**
	 * Delegate method of {@link Bytes#from(BitSet)}
	 *
	 * @see Bytes#from(BitSet)
	 */
	public static Bytes from(BitSet set) {
		return Bytes.from(set);
	}

	/**
	 * Delegate method of {@link Bytes#from(BigInteger)}
	 *
	 * @see Bytes#from(BigInteger)
	 */
	public static Bytes from(BigInteger bigInteger) {
		return Bytes.from(bigInteger);
	}

	/**
	 * Delegate method of {@link Bytes#from(InputStream)}
	 *
	 * @see Bytes#from(InputStream)
	 */
	public static Bytes from(InputStream stream) {
		return Bytes.from(stream);
	}

	/**
	 * Delegate method of {@link Bytes#from(InputStream, int)}
	 *
	 * @see Bytes#from(InputStream, int)
	 */
	public static Bytes from(InputStream stream, int maxLength) {
		return Bytes.from(stream, maxLength);
	}

	/**
	 * Delegate method of {@link Bytes#from(DataInput, int)}
	 *
	 * @see Bytes#from(DataInput, int)
	 */
	public static Bytes from(DataInput dataInput, int length) {
		return Bytes.from(dataInput, length);
	}

	/**
	 * Delegate method of {@link Bytes#from(File)}
	 *
	 * @see Bytes#from(File)
	 */
	public static Bytes from(File file) {
		return Bytes.from(file);
	}

	/**
	 * Delegate method of {@link Bytes#from(File, int, int)}
	 *
	 * @see Bytes#from(File, int, int)
	 */
	public static Bytes from(File file, int offset, int length) {
		return Bytes.from(file, offset, length);
	}

	/**
	 * Delegate method of {@link Bytes#from(CharSequence, Normalizer.Form)}
	 *
	 * @see Bytes#from(CharSequence, Normalizer.Form)
	 */
	public static Bytes from(CharSequence utf8String, Normalizer.Form form) {
		return Bytes.from(utf8String, form);
	}

	/**
	 * Delegate method of {@link Bytes#from(CharSequence, Charset)}
	 *
	 * @see Bytes#from(CharSequence, Charset)
	 */
	public static Bytes from(CharSequence string, Charset charset) {
		return Bytes.from(string, charset);
	}

	/**
	 * Delegate method of {@link Bytes#from(char[])}
	 *
	 * @see Bytes#from(char[])
	 */
	public static Bytes from(char[] charArray) {
		return Bytes.from(charArray);
	}

	/**
	 * Delegate method of {@link Bytes#from(char[], Charset)}
	 *
	 * @see Bytes#from(char[], Charset)
	 */
	public static Bytes from(char[] charArray, Charset charset) {
		return Bytes.from(charArray, charset);
	}

	/**
	 * Delegate method of {@link Bytes#from(char[], Charset, int, int)}
	 *
	 * @see Bytes#from(char[], Charset, int, int)
	 */
	public static Bytes from(char[] charArray, Charset charset, int offset, int length) {
		return Bytes.from(charArray, charset, offset, length);
	}

	/**
	 * Delegate method of {@link Bytes#from(UUID)}
	 *
	 * @see Bytes#from(UUID)
	 */
	public static Bytes from(UUID uuid) {
		return Bytes.from(uuid);
	}

	/**
	 * Delegate method of {@link Bytes#parseBinary(CharSequence)}
	 *
	 * @see Bytes#parseBinary(CharSequence)
	 */
	public static Bytes parseBinary(CharSequence binaryString) {
		return Bytes.parseBinary(binaryString);
	}

	/**
	 * Delegate method of {@link Bytes#parseOctal(CharSequence)}
	 *
	 * @see Bytes#parseOctal(CharSequence)
	 */
	public static Bytes parseOctal(CharSequence octalString) {
		return Bytes.parseOctal(octalString);
	}

	/**
	 * Delegate method of {@link Bytes#parseDec(CharSequence)}
	 *
	 * @see Bytes#parseDec(CharSequence)
	 */
	public static Bytes parseDec(CharSequence decString) {
		return Bytes.parseDec(decString);
	}

	/**
	 * Delegate method of {@link Bytes#parseRadix(CharSequence, int)}
	 *
	 * @see Bytes#parseRadix(CharSequence, int)
	 */
	public static Bytes parseRadix(CharSequence radixNumberString, int radix) {
		return Bytes.parseRadix(radixNumberString, radix);
	}

	/**
	 * Delegate method of {@link Bytes#parseHex(CharSequence)}
	 *
	 * @see Bytes#parseHex(CharSequence)
	 */
	public static Bytes parseHex(CharSequence hexString) {
		return Bytes.parseHex(hexString);
	}

	/**
	 * Delegate method of {@link Bytes#parseBase32(CharSequence)}
	 *
	 * @see Bytes#parseBase32(CharSequence)
	 */
	public static Bytes parseBase32(CharSequence base32Rfc4648String) {
		return Bytes.parseBase32(base32Rfc4648String);
	}

	/**
	 * Delegate method of {@link Bytes#parseBase36(CharSequence)}
	 *
	 * @see Bytes#parseBase36(CharSequence)
	 */
	public static Bytes parseBase36(CharSequence base36String) {
		return Bytes.parseBase36(base36String);
	}

	/**
	 * Delegate method of {@link Bytes#parseBase64(CharSequence)}
	 *
	 * @see Bytes#parseBase64(CharSequence)
	 */
	public static Bytes parseBase64(CharSequence base64String) {
		return Bytes.parseBase64(base64String);
	}

	/**
	 * Delegate method of
	 * {@link Bytes#parse(CharSequence, BinaryToTextEncoding.Decoder)}
	 *
	 * @see Bytes#parse(CharSequence, BinaryToTextEncoding.Decoder)
	 */
	public static Bytes parse(CharSequence encoded, BinaryToTextEncoding.Decoder decoder) {
		return Bytes.parse(encoded, decoder);
	}

	/**
	 * Delegate method of {@link Bytes#random(int)}
	 *
	 * @see Bytes#random(int)
	 */
	public static Bytes random(int length) {
		return Bytes.random(length);
	}

	/**
	 * Delegate method of {@link Bytes#unsecureRandom(int)}
	 *
	 * @see Bytes#unsecureRandom(int)
	 */
	public static Bytes unsecureRandom(int length) {
		return Bytes.unsecureRandom(length);
	}

	/**
	 * Delegate method of {@link Bytes#unsecureRandom(int, long)}
	 *
	 * @see Bytes#unsecureRandom(int, long)
	 */
	public static Bytes unsecureRandom(int length, long seed) {
		return Bytes.unsecureRandom(length, seed);
	}

	/**
	 * Delegate method of {@link Bytes#random(int, Random)}
	 *
	 * @see Bytes#random(int, Random)
	 */
	public static Bytes random(int length, Random random) {
		return Bytes.random(length, random);
	}

	public static void main(String[] args) {
		System.out.println(Utils.Bits.tryFrom(10l).orElse(null));
		System.out.println(Utils.Bits.tryFrom(new byte[][] { "hi".getBytes(), "yo".getBytes() }).orElse(null));

		String input = "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n"
				+ "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "A\n" + "A \n" + "B\n"
				+ "B \n" + "C\n" + "C \n" + "D\n" + "D \n" + "E\n" + "E \n" + "F\n" + "F \n" + "G\n" + "G \n" + "H\n"
				+ "H \n" + "I\n" + "I \n" + "J\n" + "J \n" + "K\n" + "K \n" + "L\n" + "L \n" + "M\n" + "M \n" + "N\n"
				+ "N \n" + "O\n" + "O \n" + "P\n" + "P \n" + "Q\n" + "Q \n" + "R\n" + "R \n" + "S\n" + "S \n" + "T\n"
				+ "T \n" + "U\n" + "U \n" + "V\n" + "V \n" + "W\n" + "W \n" + "X\n" + "X \n" + "Y\n" + "Y \n" + "Z\n"
				+ "Z \n" + "a\n" + "a \n" + "b\n" + "b \n" + "c\n" + "c \n" + "d\n" + "d \n" + "e\n" + "e \n" + "f\n"
				+ "f \n" + "g\n" + "g \n" + "h\n" + "h \n" + "i\n" + "i \n" + "j\n" + "j \n" + "k\n" + "k \n" + "l\n"
				+ "l \n" + "m\n" + "m \n" + "n\n" + "n \n" + "o\n" + "o \n" + "p\n" + "p \n" + "q\n" + "q \n" + "r\n"
				+ "r \n" + "s\n" + "s \n" + "t\n" + "t \n" + "u\n" + "u \n" + "v\n" + "v \n" + "w\n" + "w\n" + "x\n"
				+ "x\n" + "y\n" + "y\n" + "z\n" + "z\n" + "0\n" + "0\n" + "1\n" + "1\n" + "2\n" + "2\n" + "3\n" + "3\n"
				+ "4\n" + "4\n" + "5\n" + "5\n" + "6\n" + "6\n" + "7\n" + "7\n" + "8\n" + "8\n" + "9\n" + "9\n" + "+\n"
				+ "-\n" + "/\n" + "_=";
		var charStream = Strings.streamLines(input)
				.map(v -> v.trim())
				.flatMap(v -> v.chars().mapToObj(vv -> vv))
				.map(v -> (char) (int) v);
		charStream = charStream.distinct();
		charStream = charStream.sorted();
		System.out.println(charStream.map(v -> String.format("case '%s':\n", v)).joining());
	}

}