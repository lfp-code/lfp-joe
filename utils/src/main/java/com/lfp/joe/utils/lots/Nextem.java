package com.lfp.joe.utils.lots;

import java.util.Objects;
import java.util.function.BooleanSupplier;

import com.lfp.joe.core.function.Muto;

public interface Nextem<T> extends Muto<T> {

	boolean hasNext();

	public static <T> Nextem<T> create(T value, boolean hasNext) {
		return create(value, () -> hasNext);
	}

	public static <T> Nextem<T> create(T value, BooleanSupplier hasNext) {
		return new Impl<>(value, hasNext);
	}

	static class Impl<T> extends Muto.Impl<T> implements Nextem<T> {

		private final BooleanSupplier hasNext;

		public Impl(T value, BooleanSupplier hasNext) {
			super(value);
			this.hasNext = Objects.requireNonNull(hasNext);
		}

		@Override
		public boolean hasNext() {
			return hasNext.getAsBoolean();
		}

	}

}
