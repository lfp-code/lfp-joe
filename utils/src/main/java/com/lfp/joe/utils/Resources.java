package com.lfp.joe.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.StackWalker.StackFrame;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.jar.JarFile;
import java.util.regex.Pattern;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.io.FileExt;
import com.lfp.joe.stream.Streams;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.StreamEx;

public class Resources {

	protected Resources() {}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final Object VERSION = 0;
	private static final List<String> DEFAULT_RESOURCE_LOCATIONS = Arrays.asList(null, "src/main/resources/");
	private static final List<String> LOCATION_PREPENDS_EMPTY = Arrays.asList(new String[] { null });
	private static final LoadingCache<String, Optional<FileExt>> LOCATION_TO_FILE_CACHE = Caffeine.newBuilder()
			.maximumSize(25 * 1_000)
			.softValues()
			.build(location -> {
				return Optional.ofNullable(loadFile(location));
			});
	private static final LoadingCache<String, Optional<URL>> LOCATION_TO_URL_CACHE = Caffeine.newBuilder()
			.maximumSize(25 * 1_000)
			.softValues()
			.build(location -> {
				return Optional.ofNullable(loadURL(location));
			});

	public static Optional<String> getResourceAsString(String location) {
		var is = getResourceInputStream(location);
		if (is == null)
			return Optional.empty();
		try (is) {
			var barr = is.readAllBytes();
			return Optional.of(new String(barr, MachineConfig.getDefaultCharset()));
		} catch (Exception e) {
			// suppress
			if (MachineConfig.isDeveloper())
				LOGGER.warn("string read error. location:{}", location, e);
		}
		return Optional.empty();
	}

	@SuppressWarnings("resource")
	public static InputStream getResourceInputStream(String location) {
		location = Utils.Strings.trimToNull(location);
		if (location == null)
			return null;
		var lookupLocationStreamSupplier = streamLookupLocations(location).chain(Streams.cached());
		for (var lookupLocation : lookupLocationStreamSupplier.get()) {
			var inputStream = LOCATION_TO_URL_CACHE.get(lookupLocation)
					.flatMap(url -> tryReadInputSteram(url, v -> v))
					.orElse(null);
			if (inputStream != null)
				return inputStream;
		}
		for (var lookupLocation : lookupLocationStreamSupplier.get()) {
			var file = LOCATION_TO_FILE_CACHE.get(lookupLocation).orElse(null);
			if (file != null) {
				try {
					var is = new FileInputStream(file);
					if (is != null)
						return is;
				} catch (Exception e) {
					// suppress
					if (MachineConfig.isDeveloper())
						LOGGER.warn("file stream error. file:{} location:{}", file.getAbsolutePath(), lookupLocation,
								e);
				}
			}
		}
		return null;
	}

	public static FileExt getResourceFile(String location) {
		location = Utils.Strings.trimToNull(location);
		if (location == null)
			return null;
		for (var lookupLocation : streamLookupLocations(location)) {
			var file = LOCATION_TO_FILE_CACHE.get(lookupLocation).orElse(null);
			if (file != null)
				return file;
		}
		return null;
	}

	private static URL loadURL(String lookupLocation) {
		var clIter = CoreReflections.streamDefaultClassLoaders().iterator();
		while (clIter.hasNext()) {
			var classloader = clIter.next();
			URL url = Functions.catching(() -> classloader.getResource(lookupLocation), t -> null);
			if (url != null)
				return url;
		}
		return null;
	}

	private static FileExt loadFile(String lookupLocation) {
		var url = LOCATION_TO_URL_CACHE.get(lookupLocation).orElse(null);
		if (url != null) {
			{
				var file = getFileFromFileName(url.getFile());
				if (file != null)
					return file;
			}
			{
				var file = getFileFromInputStream(url);
				if (file != null)
					return file;
			}
		}
		return null;
	}

	private static StreamEx<String> streamLookupLocations(String location) {
		if (Utils.Strings.isBlank(location))
			return StreamEx.empty();
		var locationPrependStream = StreamEx.of(false, true).flatMap(lookupCallingClass -> {
			if (!lookupCallingClass)
				return LOCATION_PREPENDS_EMPTY.stream();
			var callingClassType = CoreReflections.getCallingClass(new BiPredicate<Integer, StackFrame>() {

				private boolean foundResources = false;

				@Override
				public boolean test(Integer index, StackFrame sf) {
					if (index == 0)
						return false;
					var declaringClass = sf.getDeclaringClass();
					if (!foundResources) {
						if (!Resources.class.isAssignableFrom(declaringClass))
							return false;
						foundResources = true;
						return false;
					}
					if (Resources.class.isAssignableFrom(declaringClass))
						return false;
					return true;
				}
			});
			var packageName = Optional.ofNullable(callingClassType.getPackageName()).orElse(null);
			var stream = Streams.of(packageName, callingClassType.getName());
			stream = stream.filter(Utils.Strings::isNotBlank);
			stream = stream.map(v -> v.replaceAll(Pattern.quote("."), "/") + "/");
			return stream;
		});
		// allow null!
		locationPrependStream = locationPrependStream.distinct();
		var locationStream = locationPrependStream.flatMap(locationPrepend -> {
			return Streams.of(DEFAULT_RESOURCE_LOCATIONS).map(resourceLocation -> {
				String lookup = location;
				if (locationPrepend != null)
					lookup = locationPrepend + lookup;
				if (resourceLocation != null)
					lookup = resourceLocation + lookup;
				return lookup;
			});
		});
		locationStream = locationStream.distinct();
		locationStream = locationStream.filter(Utils.Strings::isNotBlank);
		return locationStream;
	}

	private static FileExt getFileFromFileName(String fileName) {
		if (Utils.Strings.isBlank(fileName))
			return null;
		try {// direct path
			var file = new FileExt(fileName);
			if (file.exists())
				return file;
		} catch (Exception e) {
			// suppress
			if (MachineConfig.isDeveloper())
				LOGGER.warn("fileName direct lookup error. fileName:{}", fileName, e);
		}
		try {// unzip jar
			var jarEntry = getJarEntry(fileName);
			if (jarEntry != null) {
				var file = getFile(jarEntry.getKey(), jarEntry.getValue());
				if (file.exists())
					return file;
			}
		} catch (Exception e) {
			// suppress
			if (MachineConfig.isDeveloper())
				LOGGER.warn("fileName jar lookup error. fileName:{}", fileName, e);
		}
		return null;
	}

	private static FileExt getFileFromInputStream(URL url) {
		if (url == null)
			return null;
		Bytes hash = tryReadInputSteram(url, Utils.Crypto::hashMD5).orElse(null);
		if (hash == null)
			return null;
		var dir = Utils.Files.tempFile(THIS_CLASS, VERSION, "input-stream-source", hash.encodeHex());
		var file = new FileExt(dir, "resource.bin");
		try {
			Utils.Files.configureDirectoryLocked(dir, (nil, elapsed) -> {
				return file.exists();
			}, nil -> {
				try (var is = tryReadInputSteram(url, v -> v).get(); var fos = new FileOutputStream(file)) {
					is.transferTo(fos);
				}
			});
			return file;
		} catch (Exception e) {
			// suppress
			if (MachineConfig.isDeveloper())
				LOGGER.warn("url stream transfer error. url:{} file:{}", url, file.getAbsolutePath(), e);
		}
		return null;
	}

	private static <U> Optional<U> tryReadInputSteram(URL url,
			ThrowingFunction<InputStream, U, ? extends Exception> reader) {
		if (url == null || reader == null)
			return Optional.empty();
		try {
			var is = url.openStream();
			if (is == null)
				return Optional.empty();
			U result = reader.apply(is);
			if (is != result)
				// only close if NOT identity
				is.close();
			return Optional.ofNullable(result);
		} catch (Exception e) {
			// suppress
			if (MachineConfig.isDeveloper())
				LOGGER.warn("url stream read error. url:{} reader:{}", url, reader, e);
		}
		return Optional.empty();
	}

	private static Entry<File, String> getJarEntry(String fileName) {
		fileName = Utils.Strings.substringAfter(fileName, ":");
		fileName = Utils.Strings.stripStart(fileName, "/");
		var parts = fileName.split(Pattern.quote(".jar!"), 2);
		if (parts.length != 2)
			return null;
		var jarPath = parts[0] + ".jar";
		var resourcePath = Utils.Strings.stripStart(parts[1], "/");
		if (Utils.Strings.isBlank(resourcePath))
			return null;
		var jarFile = new File(jarPath);
		if (!jarFile.exists())
			return null;
		return Map.entry(jarFile, resourcePath);
	}

	private static FileExt getFile(File jarFile, String resourcePath) throws IOException {
		var resourceDirPath = Utils.Strings.substringBeforeLast(resourcePath, "/");
		if (Utils.Strings.isBlank(resourceDirPath))
			return null;
		var unzipDir = new File(Utils.Files.tempFile(THIS_CLASS, VERSION, "jar-source",
				Utils.Crypto.hashMD5(jarFile.getAbsolutePath())), resourceDirPath);
		Utils.Files.configureDirectoryLocked(unzipDir, (nil, elapsed) -> {
			return elapsed.toMillis() <= (System.currentTimeMillis() - jarFile.lastModified());
		}, nil -> {
			unzipJar(jarFile, resourceDirPath, unzipDir);
		});
		return new FileExt(unzipDir, resourcePath);
	}

	private static void unzipJar(File jarFile, String resourceDirPath, File unzipDir) throws IOException {
		try (var jar = new JarFile(jarFile)) {
			var jarEntries = jar.entries();
			while (jarEntries.hasMoreElements()) {
				var jarEntry = jarEntries.nextElement();
				var name = jarEntry.getName();
				if (Utils.Strings.endsWith(name, ".class"))
					continue;
				if (!name.startsWith(resourceDirPath + "/"))
					continue;
				var unzipFile = new File(unzipDir, name);
				if (jarEntry.isDirectory()) {
					unzipFile.mkdirs();
					continue;
				}
				try (var is = jar.getInputStream(jarEntry); var fos = new FileOutputStream(unzipFile)) {
					is.transferTo(fos);
				}
			}
		}
	}

	public static void main(String[] args) {

	}

}
