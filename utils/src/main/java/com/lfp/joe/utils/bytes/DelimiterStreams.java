package com.lfp.joe.utils.bytes;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import com.lfp.joe.core.function.Throws;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Bits;
import com.lfp.joe.utils.Lots;
import com.lfp.joe.utils.lots.stream.StreamInputStream;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.StreamEx;

public class DelimiterStreams {

	public static InputStream fromBytesStream(Stream<Bytes> stream) {
		return fromBytesStream(stream, Bits.getDefaultByteStreamDelimiter());
	}

	public static InputStream fromBytesStream(Stream<Bytes> stream, byte delimiter) {
		if (stream == null)
			return Bits.emptyInputStream();
		var entStream = StreamEx.of(stream).map(v -> {
			return Lots.entry(true, v);
		}).mapLast(ent -> ent.withKey(false));
		var barrStream = entStream.map(ent -> {
			var hasMore = ent.getKey();
			var barr = ent.getValue().array();
			if (!hasMore)
				return barr;
			return Bits.concat(barr, delimiter);
		});
		return new StreamInputStream(barrStream);
	}

	public static StreamEx<Bytes> toBytesStream(InputStream inputStream) {
		return toBytesStream(inputStream, Bits.getDefaultByteStreamDelimiter());
	}

	public static StreamEx<Bytes> toBytesStream(InputStream inputStream, byte delimiter) {
		if (inputStream == null)
			return StreamEx.empty();
		var producer = new Predicate<Consumer<? super byte[]>>() {

			private final List<BufferEntry> buffers = new ArrayList<>();

			@Override
			public boolean test(Consumer<? super byte[]> action) {
				for (int i = 0; i < 2; i++) {
					if (buffers.size() > 0) {
						var bufferEntry = buffers.get(0);
						if (bufferEntry.isComplete()) {
							buffers.remove(0);
							var barr = bufferEntry.getByteArray();
							if (barr.length == 0)
								return false;
							action.accept(barr);
							return true;
						}
					}
					Throws.unchecked(() -> fillByteArray(inputStream, delimiter, buffers));
				}
				throw new IllegalStateException("failed to find delimiter:" + delimiter);
			}
		};
		var bytesStream = Streams.<byte[]>produce(producer).map(Bytes::from);
		return bytesStream.chain(Streams.onComplete(() -> {
			Throws.unchecked(inputStream::close);
		}));
	}

	private static void fillByteArray(InputStream inputStream, byte delimiter, List<BufferEntry> bufferTracker)
			throws IOException {
		BiConsumer<byte[], Boolean> appender = (barr, complete) -> {
			BufferEntry bufferEntry;
			{
				var bufferEntryLast = !bufferTracker.isEmpty() ? bufferTracker.get(bufferTracker.size() - 1) : null;
				if (bufferEntryLast == null || bufferEntryLast.isComplete()) {
					bufferEntry = new BufferEntry();
					bufferTracker.add(bufferEntry);
				} else
					bufferEntry = bufferEntryLast;
			}
			bufferEntry.append(barr);
			if (complete)
				bufferEntry.markComplete();
		};
		var bufferSize = Bits.getDefaultBufferSize();
		boolean endOfData = false;
		while (!endOfData) {
			var buffer = inputStream.readNBytes(bufferSize);
			endOfData = buffer.length < bufferSize;
			var lastDelimiterIndex = -1;
			for (int i = 0; i < buffer.length; i++) {
				if (buffer[i] != delimiter)
					continue;
				var range = Arrays.copyOfRange(buffer, lastDelimiterIndex + 1, i);
				lastDelimiterIndex = i;
				appender.accept(range, true);
			}
			if (lastDelimiterIndex == -1)
				appender.accept(buffer, endOfData);
			else {
				var remaining = Arrays.copyOfRange(buffer, lastDelimiterIndex + 1, buffer.length);
				appender.accept(remaining, endOfData);
			}
		}
	}

	private static class BufferEntry {

		private byte[] byteArray;
		private boolean complete;

		public void append(byte[] byteArray) {
			this.byteArray = Bits.concat(this.byteArray, byteArray);
		}

		public boolean isComplete() {
			return complete;
		}

		public void markComplete() {
			this.complete = true;
		}

		public byte[] getByteArray() {
			if (byteArray == null)
				return Bits.emptyByteArray();
			return byteArray;
		}
	}

}