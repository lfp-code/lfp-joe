package com.lfp.joe.utils;

import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.reflect.TypeUtils;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.Primitives;
import com.lfp.joe.stream.Streams;

import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public class Types {
	protected Types() {};

	public static StreamEx<Class<?>> streamPrimitiveTypes() {
		return EntryStream.of(Primitives.getPrimitiveNameToTypeMap()).values();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <X> Optional<X> getPrimitiveDefault(Type type) {
		return (Optional) getPrimitiveDefault(toClass(type));
	}

	@SuppressWarnings("unchecked")
	public static <X> Optional<X> getPrimitiveDefault(Class<X> classType) {
		X value = (X) Primitives.getDefaultValue(classType);
		return Optional.ofNullable(value);
	}

	public static Class<?> wrapperToPrimitive(Class<?> classType) {
		if (classType == null)
			return null;
		if (classType.isPrimitive())
			return classType;
		else {
			Class<?> primitiveClassType = ClassUtils.wrapperToPrimitive(classType);
			return primitiveClassType != null ? primitiveClassType : classType;
		}
	}

	public static Class<?> primitiveToWrapper(Class<?> classType) {
		if (classType == null)
			return null;
		if (!classType.isPrimitive())
			return classType;
		else {
			Class<?> wrapperClassType = ClassUtils.primitiveToWrapper(classType);
			return wrapperClassType != null ? wrapperClassType : classType;
		}
	}

	public static boolean isPrimitive(Object object) {
		return isPrimitiveType(object == null ? null : object.getClass());
	}

	public static boolean isPrimitiveOrString(Object object) {
		return isPrimitiveOrStringType(object == null ? null : object.getClass());
	}

	public static boolean isPrimitiveType(Class<?> classType) {
		if (classType == null)
			return false;
		if (classType.isPrimitive())
			return true;
		return wrapperToPrimitive(classType).isPrimitive();
	}

	public static boolean isPrimitiveOrStringType(Class<?> classType) {
		if (isPrimitiveType(classType))
			return true;
		return classType != null && String.class.isAssignableFrom(classType);
	}

	public static Class<?> toClass(Type type) {
		if (type == null)
			return null;
		Iterable<Type> checkTypeIterable = () -> Streams.<Supplier<Type>>of(() -> type, () -> {
			if (type instanceof WildcardType) {
				WildcardType wct = ((WildcardType) type);
				Type[] upperBounds = wct.getUpperBounds();
				if (upperBounds != null && upperBounds.length == 1)
					return upperBounds[0];
			}
			return null;
		}).map(Supplier::get).nonNull().distinct().iterator();
		for (var checkType : checkTypeIterable)
			if (checkType instanceof Class)
				return CoreReflections.getNamedClassType((Class<?>) checkType);
		for (var checkType : checkTypeIterable) {
			var rawType = TypeUtils.getRawType(checkType, checkType);
			if (rawType != null)
				return CoreReflections.getNamedClassType(rawType);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static Class<? extends Enum<?>> toEnumClass(Type type) {
		Class<?> classType = toClass(type);
		if ((classType == null) || !Enum.class.isAssignableFrom(classType))
			return null;
		if (classType.isAnonymousClass())
			classType = classType.getEnclosingClass();
		return (Class<? extends Enum<?>>) classType;
	}

	public static boolean isVoid(Type type) {
		if (type == null)
			return false;
		if (Void.class.equals(type) || void.class.equals(type) || Void.TYPE.equals(type))
			return true;
		return false;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <X> Optional<X> tryCast(Object value, Class<X> castType) {
		Objects.requireNonNull(castType);
		if (value == null)
			return Optional.empty();
		for (var primitiveToWrapper : Arrays.asList(false, true)) {
			if (primitiveToWrapper) {
				var mappedCastType = primitiveToWrapper(castType);
				if (Objects.equals(mappedCastType, castType)) {
					mappedCastType = wrapperToPrimitive(castType);
					if (Objects.equals(mappedCastType, castType))
						break;
				}
				castType = (Class) mappedCastType;
			}
			if (castType.isAssignableFrom(value.getClass()))
				return Optional.of((X) value);
		}
		return Optional.empty();
	}

	public static <X> X cast(Object value, Class<X> castType) throws IllegalArgumentException {
		X castedValue = tryCast(value, castType).orElse(null);
		if (castedValue == null)
			throw new IllegalArgumentException(
					String.format("cast from:%s to:%s failed", value == null ? null : value.getClass(), castType));
		return castedValue;
	}

	@SuppressWarnings("unchecked")
	public static <X> X newInstanceUnchecked(Class<X> classType, Object... args) {
		return (X) CoreReflections.newInstanceUnchecked(toClass(classType), args);
	}

	public static boolean isAssignableFrom(Class<?> toClass, Class<?> cls) {
		return CoreReflections.isAssignableFrom(toClass, cls);
	}

	public static boolean equals(Class<?> classType1, Class<? extends Object> classType2) {
		return Objects.equals(primitiveToWrapper(classType1), primitiveToWrapper(classType2));
	}

	public static void main(String[] args) {
		int[] intArr = new int[5];
		intArr[0] = Integer.valueOf("69");
		intArr[1] = 5;
		System.out.println(((Object) intArr[0]).getClass());
		System.out.println(((Object) intArr[1]).getClass());
		System.out.println((int) Integer.valueOf("69"));
		System.out.println(int.class.isAssignableFrom(Integer.class));
		System.out.println(Integer.class.isAssignableFrom(int.class));
		System.out.println(streamPrimitiveTypes().toList());
		System.out.println(getPrimitiveDefault(String.class));
		System.out.println(getPrimitiveDefault(int.class));
		System.out.println(getPrimitiveDefault(Integer.class));
	}
}
