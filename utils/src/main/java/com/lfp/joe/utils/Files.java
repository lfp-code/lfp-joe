package com.lfp.joe.utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.lang3.Validate;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.lfp.joe.core.classpath.Primitives;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.function.Throws.ThrowingConsumer;
import com.lfp.joe.core.io.FileExt;
import com.lfp.joe.core.lock.FileLocks;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.function.ImmutableKeyGenerator.KeyGeneratorOptions;
import com.lfp.joe.utils.function.KeyGenerator;

import one.util.streamex.StreamEx;

public class Files {
	protected Files() {};

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String CONFIGURE_LOCK_FILE_NAME = String.format("%s.configure.lock", THIS_CLASS.getName());
	private static final String CONFIGURE_COMPLETE_FILE_NAME = String.format("%s.configure.complete",
			THIS_CLASS.getName());

	private static final Cache<String, Nada> MKDIRS_CACHE = Caffeine.newBuilder()
			.expireAfterAccess(Duration.ofSeconds(1)).maximumSize(100_000).build();

	public static FileExt tempDirectory() {
		return tempDirectory(false);
	}

	public static FileExt tempDirectory(boolean root) {
		var dir = MachineConfig.getTempDirectory(root);
		return FileExt.from(dir);
	}

	public static FileExt tempFile(Object... parts) {
		return tempFile(false, FileExt::new, parts);
	}

	public static FileExt tempFileRoot(Object... parts) {

		return tempFile(true, FileExt::new, parts);
	}

	private static <F extends File> F tempFile(boolean root, BiFunction<File, String, F> function, Object... parts) {
		var parent = tempDirectory(root);
		var path = tempFilePath(parts);
		F file = function.apply(parent, path);
		makeParentDirectories(file);
		return file;
	}

	private static String tempFilePath(Object... parts) {
		List<String> keyList = new ArrayList<>();
		var partIter = Utils.Lots.stream(parts).nonNull().iterator();
		while (partIter.hasNext()) {
			var part = partIter.next();
			boolean toString;
			if (Primitives.getPrimitiveType(part.getClass()).isPresent())
				toString = true;
			else if (part instanceof CharSequence)
				toString = true;
			else
				toString = false;
			if (toString) {
				var partStr = part.toString();
				if (Utils.Strings.isBlank(partStr))
					continue;
				try {
					Paths.get(partStr);
					keyList.add(partStr);
					continue;
				} catch (InvalidPathException e) {
					// suppress
				}
			}
			var optionsBuilder = KeyGeneratorOptions.builder();
			optionsBuilder = optionsBuilder.keyParts(Arrays.asList(part));
			optionsBuilder.digitPrefix("");
			optionsBuilder.allowEmpty(true);
			var key = KeyGenerator.apply(optionsBuilder.build());
			if (Utils.Strings.isBlank(key))
				continue;
			keyList.add(key);
		}
		if (keyList.isEmpty())
			keyList.add(UUID.randomUUID().toString());
		return keyList.stream().collect(Collectors.joining("/"));
	}

	public static void makeParentDirectories(File file) {
		Objects.requireNonNull(file);
		var parentFile = file.getParentFile();
		if (parentFile == null)
			return;
		MKDIRS_CACHE.get(FileExt.getUniquePath(parentFile), nil -> {
			Throws.unchecked(() -> java.nio.file.Files.createDirectories(parentFile.toPath()));
			return Nada.get();
		});
	}

	public static boolean delete(File file) {
		return delete(file, null);
	}

	public static boolean delete(File file, Predicate<File> filter) {
		if ((file == null) || (filter != null && !filter.test(file)))
			return false;
		boolean subFileDelete = false;
		var subFiles = file.listFiles();
		if (subFiles != null)
			for (File subFile : subFiles)
				if (delete(subFile, filter))
					subFileDelete = true;
		return file.delete() || subFileDelete;
	}

	public static boolean deleteDirectoryContents(File file) {
		return deleteDirectoryContents(file, null);
	}

	// same as delete, just skip top level
	public static boolean deleteDirectoryContents(File file, Predicate<File> filter) {
		if (file == null || (filter != null && !filter.test(file)))
			return false;
		var subFiles = file.listFiles();
		if (subFiles == null)
			return false;
		boolean subFileDelete = false;
		for (File subFile : subFiles)
			if (delete(subFile, filter))
				subFileDelete = true;
		return subFileDelete;
	}

	public static String readFileToString(File idFile) throws FileNotFoundException, IOException {
		return readFileToString(idFile, Bits.getDefaultCharset());
	}

	public static String readFileToString(File idFile, Charset charset) throws FileNotFoundException, IOException {
		Objects.requireNonNull(charset);
		try (FileInputStream fis = new FileInputStream(idFile);
				ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			Bits.copy(fis, baos);
			return new String(baos.toByteArray(), charset);
		}
	}

	public static Entry<String, Optional<String>> getNameAndExtension(File file) throws IOException {
		if (file == null)
			return null;
		var filename = file.getAbsoluteFile().getName();
		if (Utils.Strings.isBlank(filename))
			return null;
		var splitAt = Utils.Strings.lastIndexOf(filename, ".");
		// gt zero bc .filename is not 'filename' extension
		if (splitAt <= 0)
			return Map.entry(filename, Optional.empty());
		var name = filename.substring(0, splitAt);
		var ext = filename.substring(splitAt + 1);
		return Map.entry(name, Optional.of(ext));
	}

	public static BufferedReader bufferedReader(File file) throws FileNotFoundException {
		return new BufferedReader(new InputStreamReader(new FileInputStream(file), Bits.getDefaultCharset()),
				Bits.getDefaultBufferSize());
	}

	public static StreamEx<String> streamLines(File file) throws FileNotFoundException {
		var br = bufferedReader(file);
		var stream = StreamEx.of(br.lines());
		stream = stream.chain(Streams.onComplete(Throws.runnableUnchecked(br::close)));
		return stream;
	}

	public static File configureDirectoryLocked(File directory, BiPredicate<File, Duration> validator,
			ThrowingConsumer<File, IOException> configureFunction) throws IOException {
		return configureDirectoryLockedInternal(FileExt.from(directory), validator, configureFunction);
	}

	private static File configureDirectoryLockedInternal(FileExt directory, BiPredicate<File, Duration> validator,
			ThrowingConsumer<File, IOException> configureFunction) throws IOException {
		Objects.requireNonNull(directory);
		Validate.isTrue(!directory.exists() || directory.isDirectory(), "invalid directory:%s", directory);
		directory.mkdirs();
		var lockFile = new FileExt(directory, CONFIGURE_LOCK_FILE_NAME);
		var completeFile = new FileExt(directory, CONFIGURE_COMPLETE_FILE_NAME);
		FileLocks.write(lockFile, (fc, os) -> {
			boolean shouldPrepare;
			if (!completeFile.exists())
				shouldPrepare = true;
			else if (validator != null && !validator.test(completeFile,
					Duration.ofMillis(System.currentTimeMillis()).minusMillis(completeFile.creationTime().toMillis())))
				shouldPrepare = true;
			else
				shouldPrepare = false;
			if (!shouldPrepare)
				return;
			completeFile.delete();
			var lockFileAbsolutePath = lockFile.getAbsolutePath();
			Utils.Files.deleteDirectoryContents(directory, v -> {
				return !lockFileAbsolutePath.equals(v.getAbsolutePath());
			});
			if (configureFunction != null)
				configureFunction.accept(directory);
			java.nio.file.Files.writeString(completeFile.toPath(), Boolean.TRUE.toString());
		});
		return directory;
	}

	public static void main(String[] args) {
		var file = Utils.Files.tempFile(1, 2, 3, "(", "     a", new Date(), "cool.txt");
		System.out.println(file.getAbsolutePath());
	}
}
