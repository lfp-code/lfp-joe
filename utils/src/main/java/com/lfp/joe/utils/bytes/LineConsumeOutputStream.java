package com.lfp.joe.utils.bytes;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.Validate;
import org.zeroturnaround.exec.stream.LogOutputStream;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.utils.Utils;

public abstract class LineConsumeOutputStream extends LogOutputStream {

	private static final MemoizedSupplier<Field> LogOutputStream_buffer_FIELD = Utils.Functions.memoize(() -> {
		var fields = CoreReflections.streamFields(LogOutputStream.class, false)
				.filter(f -> "buffer".equals(f.getName()))
				.filter(f -> ByteArrayOutputStream.class.isAssignableFrom(f.getType()))
				.limit(2)
				.collect(Collectors.toList());
		Validate.isTrue(fields.size() == 1, "could not find field");
		Field field = fields.get(0);
		field.setAccessible(true);
		return field;
	});
	private Charset charset;

	public LineConsumeOutputStream() {
		this(StandardCharsets.UTF_8);
	}

	public LineConsumeOutputStream(Charset charset) {
		this.charset = Objects.requireNonNull(charset);
	}

	protected ByteArrayOutputStream getBuffer() {
		try {
			return (ByteArrayOutputStream) LogOutputStream_buffer_FIELD.get().get(this);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
					? java.lang.RuntimeException.class.cast(e)
					: new java.lang.RuntimeException(e);
		}
	}

	@Override
	protected void processBuffer() {
		var buffer = getBuffer();
		processLine(buffer.toString(charset));
		buffer.reset();
	}

}
