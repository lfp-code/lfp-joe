package com.lfp.joe.utils.crypto;

import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Objects;

public interface RSAKeyPair extends KeyPairLFP<RSAPrivateKey, RSAPublicKey> {

	public static RSAKeyPair create(KeyPair keyPair) {
		Objects.requireNonNull(keyPair);
		RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) Objects.requireNonNull(keyPair.getPrivate());
		RSAPublicKey rsaPublicKey = (RSAPublicKey) Objects.requireNonNull(keyPair.getPublic());
		return new RSAKeyPair() {

			@Override
			public RSAPrivateKey getPrivateKey() {
				return rsaPrivateKey;
			}

			@Override
			public RSAPublicKey getPublicKey() {
				return rsaPublicKey;
			}
		};
	}

}
