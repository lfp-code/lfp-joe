package com.lfp.joe.utils.lots;

import java.time.Duration;
import java.util.ArrayList;

import com.lfp.joe.utils.Lots;

import one.util.streamex.StreamEx;

public class Buffer<E> extends ArrayList<E> {

	private final long createdAt = System.currentTimeMillis();
	private final long index;

	public Buffer(long index) {
		super();
		this.index = index;
	}

	public long getIndex() {
		return index;
	}

	public Duration elapsed() {
		var elapsed = System.currentTimeMillis() - createdAt;
		return Duration.ofMillis(Math.max(0, elapsed));
	}

	public boolean isExpired(Duration ttl) {
		if (ttl == null)
			return false;
		var ttlMillis = ttl.toMillis();
		if (ttlMillis <= 0)
			return true;
		return elapsed().toMillis() > ttlMillis;
	}

	@Override
	public StreamEx<E> stream() {
		return Lots.stream(super.stream());
	}

}
