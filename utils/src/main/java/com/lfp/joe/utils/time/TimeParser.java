package com.lfp.joe.utils.time;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.joestelmach.natty.DateGroup;
import com.joestelmach.natty.Parser;
import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.log.LogFilter;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Functions;
import com.lfp.joe.utils.Strings;
import com.lfp.joe.utils.Utils;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.spi.FilterReply;
import one.util.streamex.StreamEx;

public interface TimeParser {

	public static TimeParser instance() {
		return Instances.get(TimeParser.class, () -> {
			return new TimeParser.Impl(new Parser());
		});
	}

	StreamEx<DateGroup> streamDateGroups(String input);

	Optional<Date> tryParseDate(String input);

	Optional<Duration> tryParseDuration(String input);

	public static class Impl implements TimeParser {

		private static final Pattern DURATION_SPLIT_PATTERN = Pattern.compile("(?<=[0-9])[^0-9].*?\\b");
		private static final String DATE_FORMAT_PATTERN = "EEE MMM dd HH:mm:ss zzz yyyy";
		static {
			LogFilter.addFilter(ctx -> {
				ILoggingEvent event = ctx.getEvent();
				if (!Parser.class.getName().equals(event.getLoggerName()))
					return FilterReply.NEUTRAL;
				if (!event.getLevel().isGreaterOrEqual(Level.WARN))
					return FilterReply.DENY;
				return FilterReply.ACCEPT;
			});
		}

		private final Parser parser;

		protected Impl(Parser parser) {
			this.parser = Objects.requireNonNull(parser);
		}

		@Override
		public StreamEx<DateGroup> streamDateGroups(String input) {
			if (StringUtils.isBlank(input))
				return StreamEx.empty();
			List<DateGroup> parsed = Throws.attempt(() -> parser.parse(input)).orElse(null);
			return Streams.of(parsed).nonNull();
		}

		@Override
		public Optional<Date> tryParseDate(String input) {
			input = Utils.Strings.lowerCase(Utils.Strings.trimToNull(input));
			if (input == null)
				return Optional.empty();
			if (input.chars().allMatch(v -> Character.isDigit((char) v)))
				return Optional.of(new Date(Long.valueOf(input)));
			if (Utils.Strings.containsWhitespace(input)) {
				var dateFormat = new SimpleDateFormat(DATE_FORMAT_PATTERN, Locale.US);
				var date = Throws.attempt(input, v -> {
					return dateFormat.parse(v);
				}).orElse(null);
				if (date != null)
					return Optional.of(date);
			}
			StreamEx<Date> stream = streamDateGroups(input).map(DateGroup::getDates).flatMap(Streams::of);
			stream = stream.nonNull();
			Date date = stream.findFirst().orElse(null);
			if (date != null)
				return Optional.of(date);
			return tryParseDuration(input).map(v -> new Date(System.currentTimeMillis() + v.toMillis()));
		}

		@Override
		public Optional<Duration> tryParseDuration(String input) {
			input = Utils.Strings.lowerCase(Utils.Strings.trimToNull(input));
			if (input == null)
				return Optional.empty();
			{// string parse
				var duration = parseDuration(input);
				if (duration != null)
					return Optional.of(duration);
			}
			{// natty parse
				var duration = parseDurationNatty(input);
				if (duration != null)
					return Optional.of(duration);
			}
			return Optional.empty();
		}

		private Duration parseDurationNatty(String input) {
			List<Supplier<String>> candidates = new ArrayList<>();
			candidates.add(Functions.memoize(() -> {
				StringBuilder sb = new StringBuilder();
				Matcher matcher = DURATION_SPLIT_PATTERN.matcher(input);
				int previousEnd = 0;
				while (matcher.find()) {
					sb.append(input.substring(previousEnd, matcher.start()));
					previousEnd = matcher.end();
					String matched = matcher.group();
					if ("y".equalsIgnoreCase(matched))
						matched = "year";
					else if ("mon".equalsIgnoreCase(matched))
						matched = "month";
					else if ("d".equalsIgnoreCase(matched))
						matched = "day";
					else if ("h".equalsIgnoreCase(matched))
						matched = "hour";
					else if ("m".equalsIgnoreCase(matched))
						matched = "minute";
					else if ("w".equalsIgnoreCase(matched))
						matched = "week";
					else if ("s".equalsIgnoreCase(matched))
						matched = "second";
					matched = " " + matched;
					sb.append(matched);
				}
				sb.append(input.substring(previousEnd, input.length()));
				return sb.toString();
			}));
			candidates.add(() -> input);
			int size = candidates.size();
			for (int i = 0; i < size; i++) {
				int index = i;
				candidates.add(() -> {
					return candidates.get(index).get() + " from now";
				});
			}
			for (Supplier<String> candidate : candidates) {
				Date now = new Date();
				String candidateText = candidate.get();
				StreamEx<Date> dateStream = streamDateGroups(candidateText).map(DateGroup::getDates)
						.flatMap(Streams::of);
				dateStream = dateStream.nonNull();
				dateStream = dateStream.distinct();
				StreamEx<Duration> durationStream = dateStream.map(v -> v.getTime() - now.getTime())
						.map(Duration::ofMillis);
				durationStream = durationStream.filter(d -> !d.isNegative());
				List<Duration> durations = durationStream.toList();
				if (durations.size() == 1)
					return durations.get(0);
			}
			return null;
		}

		private static Duration parseDuration(String input) {
			Long number;
			String unitString;
			{
				String[] parts = splitNumericAndChar(input);
				String numberString = parts[0];
				if (numberString.isEmpty() || !Strings.isNumeric(numberString))
					return null;
				number = Functions.catching(() -> Long.valueOf(numberString), t -> null);
				if (number == null)
					return null;
				unitString = parts[1];
			}
			if (unitString.length() > 2 && !unitString.endsWith("s"))
				unitString = unitString + "s";
			boolean estimate = false;
			ChronoUnit units = null;
			// note that this is deliberately case-sensitive
			switch (unitString) {
			case "ns":
			case "nanos":
			case "nanoseconds":
				units = ChronoUnit.NANOS;
				break;
			case "us":
			case "µs":
			case "micros":
			case "microseconds":
				units = ChronoUnit.MICROS;
				break;
			case "":
			case "ms":
			case "millis":
			case "milliseconds":
				units = ChronoUnit.MILLIS;
				break;
			case "s":
			case "secs":
			case "seconds":
				units = ChronoUnit.SECONDS;
				break;
			case "m":
			case "mins":
			case "minutes":
				units = ChronoUnit.MINUTES;
				break;
			case "h":
			case "hours":
				units = ChronoUnit.HOURS;
				break;
			case "d":
			case "days":
				units = ChronoUnit.DAYS;
				break;
			case "w":
			case "weeks":
				estimate = true;
				units = ChronoUnit.WEEKS;
				break;
			case "mons":
			case "months":
				estimate = true;
				units = ChronoUnit.MONTHS;
				break;
			case "y":
			case "years":
				estimate = true;
				units = ChronoUnit.YEARS;
				break;
			default: {
				estimate = true;
				for (var chronoUnit : ChronoUnit.values()) {
					var name = StringUtils.lowerCase(chronoUnit.name());
					if (!StringUtils.endsWith(name, "s"))
						name += "s";
					if (StringUtils.equals(name, unitString))
						units = chronoUnit;
				}
			}
			}
			if (units == null)
				return null;
			if (estimate) {
				LocalDate now = LocalDate.now();
				LocalDate futureDate = now.plus(number, units);
				Instant instant = futureDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
				long millis = instant.toEpochMilli();
				number = millis - System.currentTimeMillis();
				units = ChronoUnit.MILLIS;
			}
			return Duration.of(number, units);
		}

		private static String[] splitNumericAndChar(String input) {
			int i = input.length() - 1;
			while (i >= 0) {
				char c = input.charAt(i);
				if (!Character.isLetter(c))
					break;
				i -= 1;
			}
			String numberString = StringUtils.trim(input.substring(0, i + 1));
			String unitString = StringUtils.trim(input.substring(i + 1));
			unitString = StringUtils.lowerCase(unitString);
			return new String[] { numberString, unitString };
		}
	}

	public static void main(String[] args) {
		System.out.println(new Date());
		Duration dur = TimeParser.instance().tryParseDuration("1 Week").orElse(null);
		System.out.println(dur);
		System.out.println(TimeParser.instance().tryParseDate("1 week"));
		System.out.println(TimeParser.instance().tryParseDate("0"));
		System.out.println(TimeParser.instance().tryParseDate(new Date(0).toString()));
	}
}
