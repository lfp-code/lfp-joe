package com.lfp.joe.utils.zip;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Optional;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;

import org.zeroturnaround.zip.ZipEntrySource;

import com.lfp.joe.utils.Bits;
import com.lfp.joe.utils.Functions;
import com.lfp.joe.utils.bytes.AutoCloseInputStream;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

import at.favre.lib.bytes.Bytes;

public class InputStreamSource implements ZipEntrySource {

	private final String path;
	private final Optional<Bytes> bytes;
	private final ThrowingSupplier<InputStream, IOException> inputStreamSupplier;
	private final long time;
	private final int compressionMethod;
	private final long crc;

	public InputStreamSource(String path, ThrowingSupplier<InputStream, IOException> inputStreamSupplier) {
		this(path, inputStreamSupplier, System.currentTimeMillis());
	}

	public InputStreamSource(String path, ThrowingSupplier<InputStream, IOException> inputStreamSupplier, long time) {
		this(path, inputStreamSupplier, time, -1);
	}

	public InputStreamSource(String path, ThrowingSupplier<InputStream, IOException> inputStreamSupplier,
			int compressionMethod) {
		this(path, inputStreamSupplier, System.currentTimeMillis(), compressionMethod);
	}

	@SuppressWarnings("resource")
	public InputStreamSource(String path, ThrowingSupplier<InputStream, IOException> inputStreamSupplier, long time,
			int compressionMethod) {
		Objects.requireNonNull(inputStreamSupplier);
		this.path = path;
		this.time = time;
		this.compressionMethod = compressionMethod;
		if (compressionMethod != -1) {
			var is = Functions.unchecked(() -> inputStreamSupplier.get());
			bytes = Optional.of(Bits.from(is));
			Functions.unchecked(is::close);
			this.inputStreamSupplier = () -> bytes.get().inputStream();
			CRC32 crc32 = new CRC32();
			crc32.update(bytes.get().array());
			this.crc = crc32.getValue();
		} else {
			bytes = Optional.empty();
			this.inputStreamSupplier = () -> new AutoCloseInputStream(inputStreamSupplier.get());
			this.crc = -1;
		}
	}

	@Override
	public String getPath() {
		return path;
	}

	@Override
	public ZipEntry getEntry() {
		ZipEntry entry = new ZipEntry(path);
		if (bytes.isPresent())
			entry.setSize(bytes.get().length());
		if (compressionMethod != -1)
			entry.setMethod(compressionMethod);
		if (crc != -1L)
			entry.setCrc(crc);
		entry.setTime(time);
		return entry;
	}

	@Override
	public InputStream getInputStream() throws IOException {
		return this.inputStreamSupplier.get();
	}

	@Override
	public String toString() {
		return "InputStreamSource[" + path + "]";
	}

}
