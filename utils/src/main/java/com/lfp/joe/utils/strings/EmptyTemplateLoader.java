package com.lfp.joe.utils.strings;

import java.io.IOException;
import java.nio.charset.Charset;

import com.github.jknack.handlebars.io.TemplateLoader;
import com.github.jknack.handlebars.io.TemplateSource;
import com.lfp.joe.utils.Strings;

public class EmptyTemplateLoader implements TemplateLoader {

	@Override
	public TemplateSource sourceAt(String location) throws IOException {
		return null;
	}

	@Override
	public String resolve(String location) {
		return null;
	}

	@Override
	public String getPrefix() {
		return null;
	}

	@Override
	public String getSuffix() {
		return null;
	}

	@Override
	public void setPrefix(String prefix) {

	}

	@Override
	public void setSuffix(String suffix) {

	}

	@Override
	public void setCharset(Charset charset) {

	}

	@Override
	public Charset getCharset() {
		return null;
	}

	public static void main(String[] args) {
		String template = "cool {{{hat}}}";
		System.out.println(Strings.templateApply(template, "hat", "fuzzy guy"));
	}

}
