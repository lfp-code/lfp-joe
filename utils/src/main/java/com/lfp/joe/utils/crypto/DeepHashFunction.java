package com.lfp.joe.utils.crypto;

import java.security.MessageDigest;
import java.util.Iterator;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Stream;

import com.lfp.joe.utils.Crypto;
import com.lfp.joe.utils.Types;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.EntryStream;

public enum DeepHashFunction implements BiFunction<MessageDigest, Object, Bytes>, BiConsumer<MessageDigest, Object> {
	INSTANCE;

	@Override
	public Bytes apply(MessageDigest messageDigest, Object value) {
		return apply(messageDigest, value, null);
	}

	public Bytes apply(MessageDigest messageDigest, Object value, Function<Object, Object> valueConverter) {
		accept(messageDigest, value, valueConverter);
		return Bytes.from(messageDigest.digest());
	}

	@Override
	public void accept(MessageDigest messageDigest, Object value) {
		accept(messageDigest, value, null);
	}

	public void accept(MessageDigest messageDigest, Object value, Function<Object, Object> valueConverter) {
		accept(messageDigest, value, valueConverter, 0);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected void accept(MessageDigest messageDigest, Object value, Function<Object, Object> valueConverter,
			int depth) {
		Crypto.update(messageDigest, depth);
		if (valueConverter != null)
			value = valueConverter.apply(value);
		if (value == null)
			return;
		if (value instanceof Map) {
			Crypto.update(messageDigest, "$map");
			var estream = EntryStream.of((Map<?, ?>) value);
			estream = estream.sortedBy(e -> Types.tryCast(e.getKey(), Comparable.class).orElse(null));
			int index = -1;
			for (var ent : estream) {
				index++;
				Crypto.update(messageDigest, index);
				this.accept(messageDigest, ent.getKey(), valueConverter, depth + 1);
				this.accept(messageDigest, ent.getValue(), valueConverter, depth + 1);
			}
			return;
		}
		if (value instanceof Iterable)
			value = ((Iterable) value).iterator();
		else if (value instanceof Stream)
			value = ((Stream) value).iterator();

		if (value instanceof Iterator) {
			Crypto.update(messageDigest, "$iter");
			int index = -1;
			var iter = (Iterator) value;
			while (iter.hasNext()) {
				index++;
				Crypto.update(messageDigest, index);
				var subValue = iter.next();
				this.accept(messageDigest, subValue, valueConverter, depth + 1);
			}
		} else
			Crypto.update(messageDigest, value);
	}

}
