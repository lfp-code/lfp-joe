package com.lfp.joe.utils.lots.track;

import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;

import com.lfp.joe.utils.Lots;

public interface UniqueTracker<X> {

	boolean add(X element);

	long count();

	@SuppressWarnings("unchecked")
	public static abstract class Abs<X, T> implements UniqueTracker<X> {

		private final Predicate<T> backingPredicate;
		private final Consumer<X>[] onNewElementListeners;
		private long count = 0;

		public Abs(Consumer<X>... onNewElementListeners) {
			this(new Predicate<T>() {

				private Set<T> _backingSet;

				@Override
				public boolean test(T t) {
					if (_backingSet == null)
						synchronized (this) {
							if (_backingSet == null)
								_backingSet = Lots.newConcurrentHashSet();
						}
					return _backingSet.add(t);
				}
			}, onNewElementListeners);
		}

		public Abs(Predicate<T> backingPredicate, Consumer<X>... onNewElementListeners) {
			this.backingPredicate = Objects.requireNonNull(backingPredicate);
			this.onNewElementListeners = onNewElementListeners;
		}

		@Override
		public boolean add(X element) {
			T tracer = getTracer(element);
			Objects.requireNonNull(tracer, () -> "the produced tracer is null:" + element);
			var result = backingPredicate.test(tracer);
			if (!result)
				return false;
			count++;
			if (onNewElementListeners != null)
				for (var onNewElementListener : onNewElementListeners)
					if (onNewElementListener != null)
						onNewElementListener.accept(element);
			return result;
		}

		@Override
		public long count() {
			return count;
		}

		protected abstract T getTracer(X element);

	}
}
