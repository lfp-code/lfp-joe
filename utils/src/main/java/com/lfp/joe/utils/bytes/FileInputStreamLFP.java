package com.lfp.joe.utils.bytes;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.util.Optional;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.utils.Functions;
import com.lfp.joe.utils.Types;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

public class FileInputStreamLFP extends FileInputStream {

	private static final MemoizedSupplier<Field> PATH_FIELD_S = Utils.Functions.memoize(() -> {
		var stream = CoreReflections.streamFields(FileInputStream.class, false)
				.filter(f -> "path".equals(f.getName()))
				.filter(f -> String.class.isAssignableFrom(f.getType()));
		stream = stream.map(v -> {
			v.setAccessible(true);
			return v;
		});
		var fieldList = StreamEx.of(stream).limit(2).toList();
		Validate.isTrue(fieldList.size() == 1, "could not get path field");
		return fieldList.get(0);
	});

	private final MemoizedSupplier<File> fileSupplier = Utils.Functions.memoize(() -> {
		var field = PATH_FIELD_S.get();
		Object pathObj = Functions.unchecked(() -> field.get(FileInputStreamLFP.this));
		var path = Types.tryCast(pathObj, String.class).filter(Utils.Strings::isNotBlank).orElse(null);
		if (path == null)
			return null;
		File file = new File(path);
		if (!file.exists())
			return null;
		return file;
	});

	public FileInputStreamLFP(File file) throws FileNotFoundException {
		super(file);
	}

	public FileInputStreamLFP(FileDescriptor fdObj) {
		super(fdObj);
	}

	public FileInputStreamLFP(String name) throws FileNotFoundException {
		super(name);
	}

	public Optional<File> getFile() {
		return Optional.ofNullable(fileSupplier.get());
	}

}
