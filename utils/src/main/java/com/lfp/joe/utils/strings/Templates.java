package com.lfp.joe.utils.strings;

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.github.jknack.handlebars.Template;
import com.lfp.joe.utils.Functions;
import com.lfp.joe.utils.Strings;

public class Templates {

	public static Template compileInline(HandlebarsLFP handlebars, String templateStr, String startDelimiter,
			String endDelimiter) {
		return Functions
				.unchecked(() -> compileInlineInternal(handlebars, templateStr, startDelimiter, endDelimiter));
	}

	private static Template compileInlineInternal(HandlebarsLFP handlebars, String templateStr, String startDelimiter,
			String endDelimiter) throws IOException {
		Objects.requireNonNull(handlebars);
		Objects.requireNonNull(handlebars);
		if (StringUtils.isBlank(startDelimiter))
			startDelimiter = handlebars.getStartDelimiter();
		if (StringUtils.isBlank(endDelimiter))
			endDelimiter = handlebars.getEndDelimiter();
		return handlebars.compileInline(normalizeInput(templateStr), startDelimiter, endDelimiter);

	}

	private static String normalizeInput(String input) {
		if (input == null)
			return "";
		if (StringUtils.isBlank(input))
			return input;
		input = replaceAutoIndents(input);
		input = replaceEscapedDelimiters(input);
		return input;
	}

	private static String replaceEscapedDelimiters(String input) {
		String escStartReplace = "{{";
		String escStart = "//" + escStartReplace;
		String escEndReplace = "}}";
		String escEnd = escEndReplace + "//";
		String[] chunks = Strings.splitWithDelimiters(input, Pattern.quote(escStart));
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < chunks.length; i++) {
			String chunk = chunks[i];
			if (!StringUtils.equals(chunk, escStart)) {
				sb.append(chunk);
				continue;
			}
			String next = chunks.length > i + 1 ? chunks[i + 1] : null;
			int nextIndexOf = StringUtils.indexOf(next, escEnd);
			if (nextIndexOf < 0) {
				sb = sb.append(chunk);
				continue;
			}
			sb.append(escStartReplace);
			String pre = next.substring(0, nextIndexOf);
			String post = next.substring(nextIndexOf + escEnd.length());
			next = pre + escEndReplace + post;
			chunks[i + 1] = next;
		}
		return sb.toString();
	}

	private static String replaceAutoIndents(String input) {
		input = StringUtils.replace(input, " } }", "}}");
		input = StringUtils.replace(input, "}} }", "}}}");
		input = StringUtils.replace(input, "{ { ", "{{");
		input = StringUtils.replace(input, "{ {{", "{{{");
		return input;
	}

	public static void main(String[] args) {
		String text = "//{ {{ code1 } } } // //{{code2}}//";
		System.out.println(normalizeInput(text));
		System.out.println(normalizeInput("hatttt"));
		System.out.println(Arrays.asList("hat".split("8")));
		System.out.println(Arrays.asList("8hat".split("8")));
		System.out.println(Arrays.asList("hat8".split("8")));
	}

}
