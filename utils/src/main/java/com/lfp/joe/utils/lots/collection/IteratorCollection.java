package com.lfp.joe.utils.lots.collection;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.Spliterators.AbstractSpliterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.function.IntSupplier;
import java.util.stream.Stream;

import com.lfp.joe.core.function.EmptyFunctions;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.utils.Utils;

import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;

public class IteratorCollection<X> extends AbstractCollection<X> implements Scrapable {

	private final Scrapable scrapable = Scrapable.create();
	private final Lock valueListLock = new ReentrantLock();
	private List<X> valueList;
	private Iterator<? extends X> iterator;
	private IntSupplier sizeSupplier;

	public IteratorCollection(Iterator<? extends X> backingIterator, IntSupplier sizeSupplier) {
		super();
		this.iterator = backingIterator;
		this.sizeSupplier = sizeSupplier;
		this.onScrap(() -> {
			valueListLock.lock();
			try {
				Optional.ofNullable(valueList).ifPresent(Collection::clear);
				valueList = null;
				iterator = null;
			} finally {
				valueListLock.unlock();
			}
		});
	}

	@Override
	public Spliterator<X> spliterator() {
		var valueListSize = Optional.ofNullable(valueList).map(Collection::size).orElse(null);
		var sizeSupplierResult = Optional.ofNullable(sizeSupplier).map(IntSupplier::getAsInt).orElse(null);
		var estimatedSize = Stream.of(valueListSize, sizeSupplierResult)
				.filter(Objects::nonNull)
				.filter(v -> v >= 0)
				.mapToLong(v -> v)
				.max()
				.orElse(Long.MAX_VALUE);
		return new AbstractSpliterator<X>(estimatedSize, 0) {

			private final AtomicInteger indexTracker = new AtomicInteger(-1);

			@Override
			public boolean tryAdvance(Consumer<? super X> action) {
				var index = indexTracker.incrementAndGet();
				while (true) {
					for (var lock : Arrays.asList(null, valueListLock)) {
						Optional.ofNullable(lock).ifPresent(Lock::lock);
						try {
							var iteratorComplete = iterator == null;
							var nextOp = Optional.ofNullable(valueList)
									.filter(v -> v.size() > index)
									.map(v -> Optional.ofNullable(v.get(index)))
									.orElse(null);
							if (nextOp != null) {
								action.accept(nextOp.orElse(null));
								return true;
							}
							if (iteratorComplete)
								return false;
							if (lock == null)
								continue;
							if (!iterator.hasNext()) {
								iterator = null;
								continue;
							}
							if (valueList == null)
								valueList = new ArrayList<>();
							valueList.add(iterator.next());
						} finally {
							Optional.ofNullable(lock).ifPresent(Lock::unlock);
						}

					}
				}
			}

		};
	}

	@Override
	public StreamEx<X> stream() {
		return StreamEx.of(spliterator());
	}

	@Override
	public Iterator<X> iterator() {
		return Spliterators.iterator(spliterator());
	}

	@Override
	public int size() {
		Spliterator<X> spliterator = null;
		while (true) {
			var iteratorComplete = iterator == null;
			var valueListSize = Optional.ofNullable(valueList).map(Collection::size).orElse(0);
			if (iteratorComplete)
				return valueListSize;
			if (sizeSupplier != null)
				return Math.max(sizeSupplier.getAsInt(), valueListSize);
			if (spliterator == null)
				spliterator = this.spliterator();
			else
				spliterator.tryAdvance(EmptyFunctions.consumer());
		}
	}

	@Override
	public boolean isEmpty() {
		if (iterator == null)
			return Optional.ofNullable(valueList).map(Collection::isEmpty).orElse(true);
		if (spliterator().tryAdvance(EmptyFunctions.consumer()))
			return false;
		return true;
	}

	@Override
	public boolean isScrapped() {
		return scrapable.isScrapped();
	}

	@Override
	public boolean scrap() {
		return scrapable.scrap();
	}

	@Override
	public Scrapable onScrap(Runnable runnable) {
		return scrapable.onScrap(runnable);
	}

	public static void main(String[] args) {
		var iter = IntStreamEx.range(100).mapToObj(v -> "message-" + v).iterator();
		var coll = new IteratorCollection<>(iter, () -> 4);
		System.out.println(coll.isEmpty());
		var index = -1;
		for (int i = 0; i < 10; i++) {
			var collIter = coll.iterator();
			for (int j = 0; j < Utils.Crypto.getRandomInclusive(2, 50) && collIter.hasNext(); j++)
				System.out.println(collIter.next());
			System.out.println(coll.size());

		}
		coll.scrap();
		System.out.println(coll.size());
	}

}
