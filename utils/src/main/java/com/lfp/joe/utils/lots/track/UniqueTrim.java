package com.lfp.joe.utils.lots.track;

import java.util.Optional;

public interface UniqueTrim<B, X> {

	Optional<B> uniqueTrim(UniqueTracker<X> uniqueTracker);
}
