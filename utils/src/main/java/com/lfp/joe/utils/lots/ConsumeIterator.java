package com.lfp.joe.utils.lots;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

public interface ConsumeIterator<X> extends Consumer<X>, BiConsumer<X, Boolean>, Iterator<X>, Scrapable {

	public static <X> ConsumeIterator<X> create() {
		return new Impl<>();
	}

	@Override
	default void accept(X value) {
		this.accept(value, true);
	}

	@Override
	default void accept(X value, Boolean hasNext) {
		accept(Nextem.create(value, Boolean.TRUE.equals(hasNext)));
	}

	void accept(Nextem<X> nextem);

	StreamEx<X> stream();

	boolean complete();

	boolean isComplete();

	@SuppressWarnings("rawtypes")
	static class Impl<X> implements ConsumeIterator<X>, Scrapable.Delegating {

		private static final Nextem END_MARKER = createEndMarker();
		private static final Collection END_COLLECTION = createEndCollection();
		private final Scrapable _delegateScrapable = Scrapable.create();
		private final Supplier<BlockingQueue<Nextem<X>>> queueFactory;
		private boolean _complete = false;
		private BlockingQueue<Nextem<X>> _blockingQueue;
		private StreamEx<X> _stream;
		private Iterator<X> _iterator;

		public Impl() {
			this(LinkedBlockingQueue::new);
		}

		@SuppressWarnings("unchecked")
		public Impl(Supplier<BlockingQueue<Nextem<X>>> queueFactory) {
			this.queueFactory = Objects.requireNonNull(queueFactory);
			this.onScrap(() -> {
				Optional.ofNullable(_blockingQueue).ifPresent(v -> v.drainTo(END_COLLECTION));
				this.complete();
			});
		}

		@Override
		public void accept(Nextem<X> nextem) {
			if (isComplete())
				return;
			Objects.requireNonNull(nextem);
			try {
				getBlockingQueue().put(nextem);
			} catch (InterruptedException e) {
				throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e) : new RuntimeException(e);
			}
		}

		@Override
		public boolean hasNext() {
			return getIterator().hasNext();
		}

		@Override
		public X next() {
			return getIterator().next();
		}

		@Override
		public StreamEx<X> stream() {
			if (_stream == null)
				synchronized (this) {
					if (_stream == null)
						_stream = createStream();
				}
			return _stream;
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean complete() {
			if (!_complete)
				synchronized (this) {
					if (!_complete) {
						var queue = getBlockingQueue();
						queue.add(END_MARKER);
						_complete = true;
						return true;
					}
				}
			return false;
		}

		@Override
		public boolean isComplete() {
			return _complete;
		}

		protected Iterator<X> getIterator() {
			if (_iterator == null)
				synchronized (this) {
					if (_iterator == null)
						_iterator = stream().iterator();
				}
			return _iterator;
		}

		protected BlockingQueue<Nextem<X>> getBlockingQueue() {
			if (_blockingQueue == null)
				synchronized (this) {
					if (_blockingQueue == null)
						_blockingQueue = Objects.requireNonNull(this.queueFactory.get());
				}
			return _blockingQueue;
		}

		protected StreamEx<X> createStream() {
			StreamEx<X> stream = StreamEx.produce(new Predicate<Consumer<? super X>>() {

				@Override
				public boolean test(Consumer<? super X> action) {
					Nextem<X> nextem;
					try {
						nextem = getBlockingQueue().take();
					} catch (InterruptedException e) {
						throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e)
								: new RuntimeException(e);
					}
					if (END_MARKER == nextem)
						return false;
					action.accept(nextem.get());
					return nextem.hasNext();
				}
			});
			this.onScrap(stream::close);
			stream = stream.onClose(this::close);
			stream = Utils.Lots.onComplete(stream, this::scrap);
			return stream;
		}

		@Override
		public Scrapable _delegateScrapable() {
			return _delegateScrapable;
		}

		private static <U> Nextem<U> createEndMarker() {
			return new Nextem<U>() {

				@Override
				public U get() {
					return null;
				}

				@Override
				public void set(Object newValue) {}

				@Override
				public boolean hasNext() {
					return false;
				}

			};
		}

		private static Collection createEndCollection() {
			return new AbstractCollection() {

				@Override
				public Iterator iterator() {
					return Collections.emptyIterator();
				}

				@Override
				public int size() {
					return 0;
				}

				@Override
				public boolean add(Object e) {
					return false;
				}
			};
		}

	}

}
