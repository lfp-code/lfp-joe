package com.lfp.joe.utils.strings;

import java.util.Map;
import java.util.Objects;

import com.google.re2j.Matcher;
import com.google.re2j.Pattern;

public class PatternLFP {

	public static PatternLFP compile(String regex) {
		return new PatternLFP(Pattern.compile(regex));
	}

	public static PatternLFP compile(String regex, int flags) {
		return new PatternLFP(Pattern.compile(regex, flags));
	}

	public static PatternLFP compile(java.util.regex.Pattern pattern) {
		return new PatternLFP(pattern == null ? null : Pattern.compile(pattern.toString(), pattern.flags()));
	}

	public static String quote(String input) {
		return java.util.regex.Pattern.quote(input);
	}

	private final Pattern delegate;

	protected PatternLFP(Pattern delegate) {
		this.delegate = Objects.requireNonNull(delegate);
	}

	public java.util.regex.Pattern asJdkPattern() {
		return java.util.regex.Pattern.compile(pattern(), flags());
	}

	public void reset() {
		delegate.reset();
	}

	public int flags() {
		return delegate.flags();
	}

	public String pattern() {
		return delegate.pattern();
	}

	public boolean matches(String input) {
		return delegate.matches(input);
	}

	public boolean matches(byte[] input) {
		return delegate.matches(input);
	}

	public Matcher matcher(CharSequence input) {
		return delegate.matcher(input);
	}

	public Matcher matcher(byte[] input) {
		return delegate.matcher(input);
	}

	public String[] split(String input) {
		return delegate.split(input);
	}

	public String[] split(String input, int limit) {
		return delegate.split(input, limit);
	}

	@Override
	public String toString() {
		return delegate.toString();
	}

	public int groupCount() {
		return delegate.groupCount();
	}

	public Map<String, Integer> namedGroups() {
		return delegate.namedGroups();
	}

	@Override
	public boolean equals(Object o) {
		return delegate.equals(o);
	}

	@Override
	public int hashCode() {
		return delegate.hashCode();
	}

}
