package com.lfp.joe.utils;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.stream.Stream;

import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.log.Logging;
import com.lfp.joe.stream.Streams;

import ch.qos.logback.classic.Level;
import one.util.streamex.StreamEx;

@SuppressWarnings("unchecked")
public class Exceptions {

	protected Exceptions() {

	}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final Map<Class<? extends Throwable>, Optional<Constructor<?>>> EXCEPTION_CONSTRUCTOR_CACHE = new ConcurrentHashMap<>();

	public static <T extends Throwable> T as(Throwable throwable, Class<T> exceptionType) {
		Objects.requireNonNull(throwable, () -> "can't cast null throwable to type:" + exceptionType);
		if (exceptionType == null)
			throw new IllegalArgumentException("can't cast throwable to null type", throwable);
		if (exceptionType.isAssignableFrom(throwable.getClass()))
			return (T) throwable;
		Optional<Constructor<?>> op = EXCEPTION_CONSTRUCTOR_CACHE.computeIfAbsent(exceptionType, nil -> {
			Stream<Constructor<?>> stream = Arrays.asList(exceptionType.getConstructors()).stream();
			stream = stream.filter(v -> {
				if (v.getParameterCount() == 1 && Throwable.class.isAssignableFrom(v.getParameterTypes()[0]))
					return true;
				if (v.getParameterCount() == 2 && String.class.isAssignableFrom(v.getParameterTypes()[0])
						&& Throwable.class.isAssignableFrom(v.getParameterTypes()[1]))
					return true;
				return false;
			});
			stream = stream.sorted(Comparator.comparing(v -> v.getParameterCount()));
			stream = stream.map(v -> {
				v.setAccessible(true);
				return v;
			});
			return stream.findFirst();
		});
		if (op.isPresent()) {
			Constructor<?> ctor = op.get();
			try {
				if (ctor.getParameterCount() == 1)
					return (T) ctor.newInstance(throwable);
				if (ctor.getParameterCount() == 2)
					return (T) ctor.newInstance(throwable.getMessage(), throwable);
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				// suppress
			}
		}
		throw new IllegalArgumentException("can't cast throwable to type:" + exceptionType, throwable);
	}

	public static <T extends Throwable> T asOrThrowRuntime(Throwable throwable, Class<T> exceptionType) throws T {
		if (exceptionType == null)
			throw asRuntimeException(throwable);
		if (exceptionType.isInstance(throwable))
			return exceptionType.cast(throwable);
		var unwrapped = unwrapExecutionExceptions(throwable).filter(exceptionType::isInstance).map(exceptionType::cast)
				.orElse(null);
		if (unwrapped != null)
			return unwrapped;
		throw asRuntimeException(throwable);
	}

	public static RuntimeException asRuntimeException(Throwable throwable) {
		if (throwable instanceof RuntimeException)
			return (RuntimeException) throwable;
		Class<? extends RuntimeException> exceptionType;
		if (throwable instanceof IOException)
			exceptionType = UncheckedIOException.class;
		else
			exceptionType = RuntimeException.class;
		return as(throwable, exceptionType);
	}

	public static Exception asException(Throwable throwable) {
		return as(throwable, Exception.class);
	}

	public static boolean isCancelException(Throwable throwable) {
		return Throws.isHalt(throwable);
	}

	public static boolean isTimeoutException(Throwable throwable) {
		return Throws.isTimeout(throwable);
	}

	public static StreamEx<Throwable> streamCauses(Throwable throwable) {
		return Streams.of(Throws.streamCauses(throwable));
	}

	public static Optional<Throwable> unwrapExecutionExceptions(Throwable throwable) {
		while (throwable instanceof ExecutionException || throwable instanceof CompletionException)
			throwable = throwable.getCause();
		return Optional.ofNullable(throwable);
	}

	public static void appendMessage(Throwable throwable, String... messages) {
		Objects.requireNonNull(throwable);
		var currentMessage = Optional.ofNullable(throwable).map(Throwable::getMessage).orElse(null);
		var messageStream = Lots.stream(currentMessage);
		messageStream = messageStream.append(Lots.stream(messages));
		messageStream = messageStream.map(Utils.Strings::trimToNull);
		messageStream = messageStream.nonNull().distinct();
		var hasNext = Lots.hasNext(messageStream);
		if (!hasNext.getKey())
			return;
		messageStream = hasNext.getValue();
		var newMessage = messageStream.joining(" ");
		if (Objects.equals(currentMessage, newMessage))
			return;
		setMessage(throwable, newMessage);
	}

	public static void setMessage(Throwable throwable, String message) {
		Objects.requireNonNull(throwable);
		var req = FieldRequest.<Throwable, String>builder().declaringClass(Throwable.class).type(String.class)
				.name("detailMessage").build();
		MemberCache.setFieldValue(req, throwable, message);
	}

	public static void setCause(Throwable throwable, Throwable cause) {
		Throws.setCause(throwable, cause);
	}

	public static void closeQuietly(Level logLevel, AutoCloseable... closeables) {
		closeQuietly(logLevel, closeables == null ? null : Arrays.asList(closeables));
	}

	public static void closeQuietly(Level logLevel, Iterable<? extends AutoCloseable> closeables) {
		if (closeables == null)
			return;
		if (logLevel == null)
			logLevel = Level.WARN;
		org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
		var logConsumer = Logging.levelConsumer(logger, logLevel);
		Lots.stream(closeables).nonNull().forEach(c -> {
			try {
				c.close();
			} catch (Exception e) {
				logConsumer.accept("error during close", new Object[] { e });
			}
		});
	}
}