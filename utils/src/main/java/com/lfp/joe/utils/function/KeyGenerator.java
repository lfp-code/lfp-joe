package com.lfp.joe.utils.function;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.immutables.value.Value;

import org.apache.commons.lang3.Validate;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Base58;
import com.lfp.joe.utils.crypto.Hashable;
import com.lfp.joe.utils.function.ImmutableKeyGenerator.KeyGeneratorOptions;
import com.lfp.joe.utils.strings.PatternLFP;

import at.favre.lib.bytes.Bytes;
import edu.umd.cs.findbugs.annotations.Nullable;
import one.util.streamex.EntryStream;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;

@Value.Enclosing
public class KeyGenerator {

	private KeyGenerator() {}

	private static final int MAX_KEY_LENGTH_DEFAULT = 200;
	private static final String JOIN_DEFAULT = "_";
	private static final String DIGIT_PREFIX_DEFAULT = "x" + JOIN_DEFAULT;
	private static final PatternLFP REPLACE_PATTERN = PatternLFP.compile("[^a-zA-Z0-9]+");

	public static String apply(Object... objects) {
		var objectList = Streams.of(objects).nonNull().toList();
		var keyGeneratorOptionsList = removeKeyGeneratorOptions(objectList);
		if (keyGeneratorOptionsList.isEmpty())
			return apply(KeyGeneratorOptions.builder().keyParts(objectList).build());
		if (keyGeneratorOptionsList.size() == 1) {
			var keyGeneratorOptions = keyGeneratorOptionsList.get(0);
			var keyParts = Streams.<Object>of(keyGeneratorOptions.keyParts()).append(objectList);
			keyGeneratorOptions = keyGeneratorOptions.withKeyParts(keyParts);
			return apply(keyGeneratorOptions);
		}
		throw new IllegalArgumentException("multiple key generator options supplied:" + Objects.toString(objects));
	}

	public static String apply(KeyGeneratorOptions options) {
		Objects.requireNonNull(options);
		var keyPartStream = flattenKeyParts(options.keyParts()).map(v -> generateKeyPart(v, options)).nonNull();
		var keyPartSplitStream = keyPartStream.map(v -> streamSplitRegions(v)).chain(Streams.flatMap())
				.chain(EntryStream::of);
		var maxLength = options.maxLength();
		var resultBuilder = new StringBuilder();
		MessageDigest replaceMessageDigest = null;
		var index = -1;
		for (var ent : keyPartSplitStream) {
			index++;
			var replace = ent.getKey();
			var value = ent.getValue();
			if (!replace && (maxLength == null || resultBuilder.length() < maxLength)) {
				if (resultBuilder.length() != 0)
					resultBuilder.append(options.join());
				resultBuilder.append(value);
			} else {
				if (replaceMessageDigest == null)
					replaceMessageDigest = Utils.Crypto.getMessageDigestMD5();
				replaceMessageDigest.update(Bytes.from(index).array());
				replaceMessageDigest.update(value.getBytes(StandardCharsets.UTF_8));
			}

		}
		var result = resultBuilder.toString();
		if (replaceMessageDigest != null) {
			result += options.join() + Base58.encode(replaceMessageDigest.digest());
			result = result.replaceAll(String.format("%s{2,}", options.join()), options.join());
			result = Utils.Strings.removeStart(result, options.join());
			result = Utils.Strings.removeEnd(result, options.join());
		}
		if (Utils.Strings.isNotBlank(options.digitPrefix())) {
			// digit prefix
			var charAt = Utils.Strings.charAt(result, 0);
			if (charAt != null && Character.isDigit(charAt))
				result = options.digitPrefix() + result;
		}
		// trim with hash
		if (maxLength != null && result.length() > maxLength) {
			var hashAppend = Base58.encodeBytes(Utils.Crypto.hashMD5(result));
			var prefix = Utils.Strings.substring(result, 0, maxLength - hashAppend.length());
			if (prefix.length() > 1 && !Utils.Strings.endsWith(prefix, options.join())) {
				hashAppend = options.join() + hashAppend;
				prefix = Utils.Strings.substring(result, 0, prefix.length() - options.join().length());
			}
			if (hashAppend.length() > maxLength)
				throw new IllegalArgumentException(String.format("hash append length (%s) exceeds maxLength (%s)",
						hashAppend.length(), maxLength));
			result = prefix + hashAppend;
		}

		if (!options.allowEmpty() && Utils.Strings.isBlank(result))
			throw new IllegalArgumentException("generated key is empt. options:" + options);
		return result;
	}

	@SuppressWarnings({ "unchecked" })
	private static StreamEx<Object> flattenKeyParts(Object part) {
		if (Nada.get().equals(part))
			return StreamEx.empty();
		var isBlank = CoreReflections.tryCast(part, CharSequence.class).map(Utils.Strings::isBlank).orElse(false);
		if (isBlank)
			return StreamEx.empty();
		if (!(part instanceof Bytes)) {
			var stream = Utils.Lots.tryStream(part).orElse(null);
			if (stream != null) {
				StreamEx<Object> result = StreamEx.empty();
				for (var obj : stream)
					result = result.append(flattenKeyParts(obj));
				return result;
			}
		}
		if (part instanceof Entry) {
			Entry<Object, Object> entry = (Entry<Object, Object>) part;
			return flattenKeyParts(Arrays.asList(entry.getKey(), entry.getValue()));
		}
		if (part instanceof Map) {
			Map<Object, Object> map = (Map<Object, Object>) part;
			var estream = Utils.Lots.stream(map).nonNull();
			estream = estream.sorted((k1, k2) -> {
				return new CompareToBuilder().append(k1, k2).toComparison();
			});
			return flattenKeyParts(estream);
		}
		return StreamEx.of(part);
	}

	private static String generateKeyPart(Object part, KeyGeneratorOptions options) {
		if (Nada.get().equals(part))
			return null;
		Optional<String> castOp = Optional.empty();
		castOp = castOp.or(() -> tryGenerateKeyPart(part, options, byte[].class, Base58::encode));
		castOp = castOp.or(() -> tryGenerateKeyPart(part, options, Bytes.class, Base58::encodeBytes));
		castOp = castOp.or(() -> tryGenerateKeyPart(part, options, Class.class, Class::getName));
		castOp = castOp.or(() -> tryGenerateKeyPart(part, options, Hashable.class, Hashable::hash));
		var result = Utils.Strings.trimToNull(castOp.orElse(null));
		if (result == null)
			result = Utils.Strings.trimToNull(part.toString());
		return result;
	}

	private static <X> Optional<String> tryGenerateKeyPart(Object part, KeyGeneratorOptions options, Class<X> classType,
			Function<X, Object> mapper) {
		Objects.requireNonNull(classType);
		Objects.requireNonNull(mapper);
		return Utils.Types.tryCast(part, classType).map(mapper).map(v -> generateKeyPart(v, options));
	}

	private static List<KeyGeneratorOptions> removeKeyGeneratorOptions(List<Object> objectList) {
		List<KeyGeneratorOptions> result = null;
		var objectIter = objectList.iterator();
		while (objectIter.hasNext()) {
			var next = objectIter.next();
			if (next instanceof KeyGeneratorOptions.Builder)
				next = ((KeyGeneratorOptions.Builder) next).build();
			var keyGeneratorOptions = CoreReflections.tryCast(next, AbstractKeyGeneratorOptions.class)
					.map(KeyGeneratorOptions::copyOf).orElse(null);
			if (keyGeneratorOptions == null)
				continue;
			objectIter.remove();
			result = Optional.ofNullable(result).orElseGet(ArrayList::new);
			result.add(keyGeneratorOptions);
		}
		return Optional.ofNullable(result).orElseGet(List::of);
	}

	private static EntryStream<Boolean, String> streamSplitRegions(String keyPart) {
		var matcher = REPLACE_PATTERN.matcher(keyPart);
		var producer = new Predicate<Consumer<? super Entry<Boolean, String>>>() {

			private int lastRegionEnd = 0;

			@Override
			public boolean test(Consumer<? super Entry<Boolean, String>> action) {
				var startIndex = lastRegionEnd;
				if (matcher.find()) {
					action.accept(Map.entry(false, keyPart.substring(lastRegionEnd, matcher.start())));
					action.accept(Map.entry(true, keyPart.substring(matcher.start(), matcher.end())));
					lastRegionEnd = matcher.end();
					return true;
				}
				if (startIndex < keyPart.length())
					action.accept(Map.entry(false, keyPart.substring(startIndex, keyPart.length())));
				return false;
			}
		};
		return Streams.produce(producer).chain(EntryStreams::of);
	}

	@Value.Style(typeAbstract = { "Abstract*" }, typeImmutable = "*", jdkOnly = true)
	@Value.Immutable
	public static abstract class AbstractKeyGeneratorOptions {

		@Nullable
		public abstract Iterable<? extends Object> keyParts();

		@Nullable
		@Value.Default
		public Integer maxLength() {
			return MAX_KEY_LENGTH_DEFAULT;
		}

		@Value.Default
		public String join() {
			return JOIN_DEFAULT;
		}

		@Value.Default
		public String digitPrefix() {
			return DIGIT_PREFIX_DEFAULT;
		}

		@Value.Default
		public boolean allowEmpty() {
			return false;
		}

		@Value.Check
		AbstractKeyGeneratorOptions normalize() {
			if (maxLength() == -1)
				return KeyGeneratorOptions.copyOf(this).withMaxLength(null);
			Validate.isTrue(maxLength() == null || maxLength() > 0);
			if (keyParts() == null || !(keyParts() instanceof List))
				return KeyGeneratorOptions.copyOf(this).withKeyParts(Streams.of(keyParts()).toList());
			return this;
		}

	}

	public static void main(String[] args) {
		var split = "com.lfp.joe.===__Resources....";
		System.out.println(KeyGenerator.apply("com.lfp.joe.Resources"));
		Object[] objects = new Object[] { 69, Nada.class.getName(),
				"What I find remarkable is that this text has been the industry's standard dummy text ever since some printer in the 1500s took a galley of type and scrambled it to make a type specimen book; it has survived not only four centuries of letter-by-letter resetting but even the leap into electronic typesetting, essentially unchanged except for an occasional 'ing' or 'y' thrown in. It's ironic that when the then-understood Latin was scrambled, it became as incomprehensible as Greek; the phrase 'it's Greek to me' and 'greeking' have common semantic roots!” (The editors published his letter in a correction headlined “Lorem OopsumWhat I find remarkable is that this text has been the industry's standard dummy text ever since some printer in the 1500s took a galley of type and scrambled it to make a type specimen book; it has survived not only four centuries of letter-by-letter resetting but even the leap into electronic typesetting, essentially unchanged except for an occasional 'ing' or 'y' thrown in. It's ironic that when the then-understood Latin was scrambled, it became as incomprehensible as Greek; the phrase 'it's Greek to me' and 'greeking' have common semantic roots!” (The editors published his letter in a correction headlined “Lorem OopsumWhat I find remarkable is that this text has been the industry's standard dummy text ever since some printer in the 1500s took a galley of type and scrambled it to make a type specimen book; it has survived not only four centuries of letter-by-letter resetting but even the leap into electronic typesetting, essentially unchanged except for an occasional 'ing' or 'y' thrown in. It's ironic that when the then-understood Latin was scrambled, it became as incomprehensible as Greek; the phrase 'it's Greek to me' and 'greeking' have common semantic roots!” (The editors published his letter in a correction headlined “Lorem OopsumWhat I find remarkable is that this text has been the industry's standard dummy text ever since some printer in the 1500s took a galley of type and scrambled it to make a type specimen book; it has survived not only four centuries of letter-by-letter resetting but even the leap into electronic typesetting, essentially unchanged except for an occasional 'ing' or 'y' thrown in. It's ironic that when the then-understood Latin was scrambled, it became as incomprehensible as Greek; the phrase 'it's Greek to me' and 'greeking' have common semantic roots!” (The editors published his letter in a correction headlined “Lorem OopsumWhat I find remarkable is that this text has been the industry's standard dummy text ever since some printer in the 1500s took a galley of type and scrambled it to make a type specimen book; it has survived not only four centuries of letter-by-letter resetting but even the leap into electronic typesetting, essentially unchanged except for an occasional 'ing' or 'y' thrown in. It's ironic that when the then-understood Latin was scrambled, it became as incomprehensible as Greek; the phrase 'it's Greek to me' and 'greeking' have common semantic roots!” (The editors published his letter in a correction headlined “Lorem OopsumWhat I find remarkable is that this text has been the industry's standard dummy text ever since some printer in the 1500s took a galley of type and scrambled it to make a type specimen book; it has survived not only four centuries of letter-by-letter resetting but even the leap into electronic typesetting, essentially unchanged except for an occasional 'ing' or 'y' thrown in. It's ironic that when the then-understood Latin was scrambled, it became as incomprehensible as Greek; the phrase 'it's Greek to me' and 'greeking' have common semantic roots!” (The editors published his letter in a correction headlined “Lorem Oopsumthis))is&cool",
				"Validate.isTrue(prefixLength >= 0, \"hash longer than maxLength:%s hashLength:%s\", maxLength,Whether to also include visible fields from super class. Field is visible if it's public, protected. It's also visible if it has a default scope and it's in the same package. Private fields are never visible, therefore not set.Whether to also include visible fields from super class. Field is visible if it's public, protected. It's also visible if it has a default scope and it's in the same package. Private fields are never visible, therefore not set.Whether to also include visible fields from super class. Field is visible if it's public, protected. It's also visible if it has a default scope and it's in the same package. Private fields are never visible, therefore not set.Whether to also include visible fields from super class. Field is visible if it's public, protected. It's also visible if it has a default scope and it's in the same package. Private fields are never visible, therefore not set.Whether to also include visible fields from super class. Field is visible if it's public, protected. It's also visible if it has a default scope and it's in the same package. Private fields are never visible, therefore not set.",
				"    ", null };
		System.out.println(KeyGenerator.apply(objects));
		{
			var options = KeyGeneratorOptions.builder().keyParts(Streams.of(objects)).maxLength(25).build();
			System.out.println(KeyGenerator.apply(options));
		}
		{
			var objectStream = IntStreamEx.range(50).boxed().flatMap(nil -> Streams.of(objects));
			var options = KeyGeneratorOptions.builder().keyParts(objectStream).build();
			System.out.println(KeyGenerator.apply(options));
		}
	}

}
