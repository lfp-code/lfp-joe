package com.lfp.joe.utils;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.function.Throws.ThrowingBiFunction;
import com.lfp.joe.core.function.Throws.ThrowingConsumer;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.function.Throws.ThrowingRunnable;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.utils.function.CachedKeyGetter;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class Functions {

	protected Functions() {

	}

	private static final Consumer DO_NOTHING_CONSUMER = v -> {};

	public static <T> Consumer<T> doNothingConsumer() {
		return DO_NOTHING_CONSUMER;
	}

	private static final Runnable DO_NOTHING_RUNNABLE = () -> {};

	public static Runnable doNothingRunnable() {
		return DO_NOTHING_RUNNABLE;
	}

	private static final Function DO_NOTHING_FUNCTION = nil -> null;

	public static <T, R> Function<T, R> doNothingFunction() {
		return DO_NOTHING_FUNCTION;
	}

	private static final BiFunction BI_FUNCTION_FIRST = (f, s) -> f;

	public static <F, S> BiFunction<F, S, F> biFunctionFirst() {
		return BI_FUNCTION_FIRST;
	}

	private static final BiFunction BI_FUNCTION_SECOND = (f, s) -> s;

	public static <F, S> BiFunction<F, S, S> biFunctionSecond() {
		return BI_FUNCTION_SECOND;
	}

	public static void unchecked(ThrowingRunnable<?> runnable) {
		Throws.unchecked(runnable);
	}

	public static <X> X unchecked(ThrowingSupplier<X, ?> supplier) {
		return Throws.unchecked(supplier);
	}

	public static <I, X> X unchecked(I input, ThrowingFunction<I, X, ?> throwingFunction) {
		return Throws.unchecked(input, throwingFunction);
	}

	public static <I> void unchecked(I input, ThrowingConsumer<I, ?> throwingConsumer) {
		Throws.unchecked(input, throwingConsumer);
	}

	public static <X> X catching(Callable<X> callable, Function<Exception, X> catchFunction) {
		ThrowingSupplier<X, Exception> sup = callable == null ? null : callable::call;
		return sup.onError(catchFunction).get();
	}

	public static <E extends Exception> void catching(ThrowingRunnable<E> runnable, Consumer<E> catchConsumer) {
		runnable.onError(catchConsumer).run();
	}

	public static <I, X, E extends Exception> X catching(I input, ThrowingFunction<I, X, E> function,
			Function<E, X> catchFunction) {
		return function.onError(catchFunction).apply(input);
	}

	public static MemoizedSupplier<Void> memoize(Runnable runnable) {
		return new MemoizedSupplier.Impl<Void>(runnable == null ? null : () -> {
			runnable.run();
			return null;
		});
	}

	public static <X> MemoizedSupplier<X> memoize(Supplier<X> supplier) {
		return new MemoizedSupplier.Impl<>(supplier);
	}

	public static <X> MemoizedSupplier<X> memoizeInstance(Class<X> classType, Function<Exception, X> catchFunction) {
		return memoize(classType == null ? null : () -> CoreReflections.newInstanceUnchecked(classType), catchFunction);
	}

	public static <E extends Exception> MemoizedSupplier<Void> memoize(ThrowingRunnable<E> runnable,
			Consumer<E> catchConsumer) {
		return new MemoizedSupplier.Impl<>(() -> {
			runnable.onError(catchConsumer).run();
			return null;
		});
	}

	public static <X> MemoizedSupplier<X> memoize(Callable<X> callable, Function<Exception, X> catchFunction) {
		return new MemoizedSupplier.Impl<>(() -> {
			return catching(callable, catchFunction);
		});
	}

	public static Supplier<Void> asSupplier(Runnable runnable) {
		if (runnable == null)
			return null;
		return () -> {
			runnable.run();
			return null;
		};
	}

	public static Callable<Void> asCallable(Runnable runnable) {
		if (runnable == null)
			return null;
		return () -> {
			runnable.run();
			return null;
		};
	}

	public static <K, V, T extends Throwable> V compute(Map<K, V> map, K key, ThrowingBiFunction<K, V, V, T> loader)
			throws T {
		Objects.requireNonNull(map);
		return map.compute(key, (k, v) -> {
			if (loader != null)
				loader.unchecked().apply(k, v);
			return v;
		});
	}

	public static <K, V, T extends Throwable> V computeIfAbsent(Map<K, V> map, K key, ThrowingFunction<K, V, T> loader)
			throws T {
		Objects.requireNonNull(map);
		return map.computeIfAbsent(key, k -> {
			if (loader != null)
				return loader.unchecked().apply(k);
			return null;
		});
	}

	public static <K, V> CachedKeyGetter<Entry<K, V>, K, V> newCachedKeyGetter(
			Iterable<? extends Entry<K, V>> iterable) {
		return newCachedKeyGetter(iterable, ent -> {
			return Optional.ofNullable(ent.getKey()).map(List::of).orElse(null);
		}, ent -> {
			return Optional.ofNullable(ent.getValue()).map(List::of).orElse(null);
		});
	}

	public static <X, K> CachedKeyGetter<X, K, X> newCachedKeyGetter(Iterable<? extends X> iterable,
			Function<X, Iterable<? extends K>> keyFunction) {
		return newCachedKeyGetter(iterable, keyFunction, v -> Optional.ofNullable(v).map(List::of).orElse(null));
	}

	public static <X, K, V> CachedKeyGetter<X, K, V> newCachedKeyGetter(Iterable<? extends X> iterable,
			Function<X, Iterable<? extends K>> keyFunction, Function<X, Iterable<? extends V>> valueFunction) {
		return new CachedKeyGetter<>(iterable, keyFunction, valueFunction);
	}

}
