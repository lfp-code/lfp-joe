package com.lfp.joe.utils;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.chrono.ChronoLocalDate;
import java.time.chrono.ChronoLocalDateTime;
import java.time.chrono.ChronoZonedDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.TimeZone;
import java.util.function.Function;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.MethodRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.utils.time.TimeParser;

public class Times {
	protected Times() {};

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final ZoneId DEFAULT_ZONE_ID = ZoneOffset.UTC;

	public static final ZoneId getDefaultZoneId() {
		return DEFAULT_ZONE_ID;
	}

	public static final ZoneOffset getDefaultZoneOffset() {
		return toOffset(getDefaultZoneId());
	}

	public static final ZoneOffset toOffset(ZoneId zoneId) {
		return Optional.ofNullable(zoneId)
				.map(TimeZone::getTimeZone)
				.map(TimeZone::getRawOffset)
				.map(Duration::ofMillis)
				.map(Duration::getSeconds)
				.map(Number::intValue)
				.map(ZoneOffset::ofTotalSeconds)
				.orElse(null);
	}

	public static ZonedDateTime now() {
		return now(null);
	}

	public static ZonedDateTime now(Function<ZonedDateTime, ZonedDateTime> modifier) {
		var zdt = ZonedDateTime.now(getDefaultZoneId());
		if (modifier != null)
			zdt = Objects.requireNonNull(modifier.apply(zdt));
		return zdt;
	}

	public static boolean isToday(ZonedDateTime dateTime) {
		if (dateTime == null)
			return false;
		return !dateTime.isBefore(startOfDay());
	}

	public static ZonedDateTime startOfDay() {
		return startOfDay(now());
	}

	public static ZonedDateTime startOfDay(ZonedDateTime dateTime) {
		if (dateTime == null)
			return null;
		return dateTime.truncatedTo(ChronoUnit.DAYS);
	}

	public static ZonedDateTime startOfWeek() {
		return startOfWeek(now());
	}

	public static ZonedDateTime startOfWeek(ZonedDateTime dateTime) {
		if (dateTime == null)
			return null;
		return dateTime.with(ChronoField.DAY_OF_WEEK, 1).truncatedTo(ChronoUnit.DAYS);
	}

	public static ZonedDateTime startOfMonth() {
		return startOfMonth(now());
	}

	public static ZonedDateTime startOfMonth(ZonedDateTime dateTime) {
		if (dateTime == null)
			return null;
		return dateTime.with(TemporalAdjusters.firstDayOfMonth()).truncatedTo(ChronoUnit.DAYS);
	}

	public static ZonedDateTime startOfYear() {
		return startOfYear(now());
	}

	public static ZonedDateTime startOfYear(ZonedDateTime dateTime) {
		if (dateTime == null)
			return null;
		return dateTime.with(TemporalAdjusters.firstDayOfYear()).truncatedTo(ChronoUnit.DAYS);
	}

	public static ZonedDateTime toDateTime(Date date) {
		return toDateTime(date, null);
	}

	public static ZonedDateTime toDateTime(Date date, ZoneId zoneId) {
		if (date == null)
			return null;
		zoneId = Optional.ofNullable(zoneId).orElse(getDefaultZoneId());
		return ZonedDateTime.ofInstant(date.toInstant(), zoneId);
	}

	// date conversion

	public static Date nowDate() {
		return nowDate(null);
	}

	public static Date nowDate(Function<ZonedDateTime, ZonedDateTime> modifier) {
		return modifyDate(new Date(), modifier);
	}

	public static Date modifyDate(Date date, Function<ZonedDateTime, ZonedDateTime> modifier) {
		if (date == null)
			return null;
		if (modifier == null)
			return date;
		var zdt = Objects.requireNonNull(modifier.apply(toDateTime(date)));
		return Date.from(zdt.toInstant());
	}

	public static Date toDate(Temporal temporal) {
		return toDate(temporal, null);
	}

	public static Date toDate(Temporal temporal, ZoneId zoneId) {
		if (temporal == null)
			return null;
		var instant = toInstant(temporal, zoneId);
		return Date.from(instant);
	}

	public static Date truncateTo(Date date, Duration roundDownInterval) {
		var timeEpochMillis = date == null ? null : date.getTime();
		long x = timeEpochMillis / roundDownInterval.toMillis();
		timeEpochMillis = x * roundDownInterval.toMillis();
		return new Date(timeEpochMillis);
	}

	// instant conversion

	public static Instant toInstant(final Temporal temporal) {
		return toInstant(temporal, null);
	}

	@SuppressWarnings("unchecked")
	public static Instant toInstant(final Temporal temporal, ZoneId zoneId) {
		if (temporal == null)
			return null;
		if (temporal instanceof Instant)
			return (Instant) temporal;
		if (zoneId == null)
			return toInstant(temporal, getDefaultZoneId());
		return CoreReflections.tryCast(temporal, Instant.class).or(() -> {
			// ChronoZonedDateTime
			return CoreReflections.tryCast(temporal, ChronoZonedDateTime.class).map(ChronoZonedDateTime::toInstant);
		}).or(() -> {
			// ChronoLocalDateTime
			return CoreReflections.tryCast(temporal, ChronoLocalDateTime.class).map(v -> {
				return toInstant(v.atZone(zoneId), zoneId);
			});
		}).or(() -> {
			// ChronoLocalDate
			return CoreReflections.tryCast(temporal, ChronoLocalDate.class).map(v -> {
				return toInstant(v.atTime(LocalTime.now(zoneId)), zoneId);
			});
		}).or(() -> {
			// LocalTime
			return CoreReflections.tryCast(temporal, LocalTime.class).map(v -> {
				return toInstant(v.atDate(LocalDate.now(zoneId)), zoneId);
			});
		}).orElseGet(() -> {
			// hail mary
			var req = MethodRequest.of(temporal.getClass(), Instant.class, "toInstant");
			return MemberCache.invokeMethod((MethodRequest<Temporal, Instant>) req, temporal);
		});
	}

	// parsing

	public static Optional<Date> tryParseDate(String text) {
		return TimeParser.instance().tryParseDate(text);
	}

	public static Optional<Duration> tryParseDuration(String input) {
		return TimeParser.instance().tryParseDuration(input);
	}

	public static void main(String[] args) {
		System.out.println(tryParseDuration("1 min"));
	}

}
