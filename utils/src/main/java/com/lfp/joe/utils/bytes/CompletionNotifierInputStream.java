package com.lfp.joe.utils.bytes;

import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

import com.lfp.joe.core.function.CompletionNotifier;
import com.lfp.joe.utils.Bits;

public class CompletionNotifierInputStream extends CountingInputStream implements CompletionNotifier<Long> {

	private final CompletionNotifier<Long> _completionNotifier = CompletionNotifier.create();

	/**
	 * Creates an automatically closing proxy for the given input stream.
	 *
	 * @param in underlying input stream
	 */
	public CompletionNotifierInputStream(final InputStream in) {
		super(in);
	}

	/**
	 * Closes the underlying input stream and replaces the reference to it with a
	 * empty instance.
	 * <p>
	 * This method is automatically called by the read methods when the end of input
	 * has been reached.
	 * <p>
	 * Note that it is safe to call this method any number of times. The original
	 * underlying input stream is closed and discarded only once when this method is
	 * first called.
	 *
	 * @throws IOException if the underlying input stream can not be closed
	 */
	@Override
	public void close() throws IOException {
		super.close();
		this.complete(this.getByteCount());
	}

	/**
	 * Automatically closes the stream if the end of stream was reached.
	 *
	 * @param n number of bytes read, or -1 if no more bytes are available
	 * @since 2.0
	 */
	@Override
	protected void afterRead(final int n) throws IOException {
		super.afterRead(n);
		if (n == Bits.endOfFile())
			this.complete(this.getByteCount());
	}

	@Override
	public boolean complete(Long result) {
		return _completionNotifier.complete(result);
	}

	@Override
	public boolean isComplete() {
		return _completionNotifier.isComplete();
	}

	@Override
	public boolean addListener(Consumer<? super Long> listener) {
		return _completionNotifier.addListener(listener);
	}

	@Override
	public boolean removeListener(Consumer<? super Long> listener) {
		return _completionNotifier.removeListener(listener);
	}

	@Override
	public Long block(Duration timeout) throws InterruptedException, TimeoutException {
		return _completionNotifier.block(timeout);
	}

}