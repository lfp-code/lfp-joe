package com.lfp.joe.utils.crypto;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.URI;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.function.BiConsumer;
import java.util.function.BooleanSupplier;

import org.apache.commons.io.input.CharSequenceInputStream;
import org.threadly.concurrent.SubmitterExecutor;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.wrapper.SubmitterExecutorAdapter;
import org.threadly.concurrent.wrapper.traceability.ThreadRenamingRunnable;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.EmptyArrays;
import com.lfp.joe.core.function.FNV;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Once.SupplierOnce;
import com.lfp.joe.core.function.Sequencer;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.function.Throws.ThrowingBiConsumer;
import com.lfp.joe.core.function.Throws.ThrowingBiFunction;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.log.LogConsumer;
import com.lfp.joe.core.log.Logging;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.core.process.future.Completion;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import ch.qos.logback.classic.Level;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;

@SuppressWarnings("unchecked")
public class MessageDigestUpdater implements BiConsumer<MessageDigest, Object> {

	/*
	 * this cache is crucial to a lot of apis. limit outside services like
	 * MachineConfig loaders or reflection. very easy for it to cause a deadlock
	 */

	private static final SupplierOnce<MessageDigestUpdater> INSTANCE_ONCE = SupplierOnce.of(MessageDigestUpdater::new);

	public static MessageDigestUpdater get() {
		return INSTANCE_ONCE.get();
	}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final LogConsumer LOG_CONSUMER;
	static {
		boolean verboseLogging = false;
		if (verboseLogging)
			LOG_CONSUMER = Logging.levelConsumer(LOGGER, Level.INFO);
		else
			LOG_CONSUMER = LogConsumer.NO_OP;
	}
	private static final Cache<Hashable, Object> HASHABLE_CACHE = Caffeine.newBuilder().weakKeys().softValues().build();

	private final List<ThrowingFunction<? super Object, ? extends Object, ? extends IOException>> transformFunctions;
	private final List<ThrowingBiFunction<? super Object, MessageDigest, Boolean, ? extends IOException>> updateFunctions;
	private final SubmitterExecutor concurrentExecutor;

	protected MessageDigestUpdater() {
		this.transformFunctions = Streams
				.<ThrowingFunction<? super Object, ? extends Object, ? extends IOException>>of(
						buildTransformFunctions())
				.nonNull()
				.toImmutableList();
		this.updateFunctions = Streams
				.<ThrowingBiFunction<? super Object, MessageDigest, Boolean, ? extends IOException>>of(
						buildUpdateFuntions())
				.nonNull()
				.toImmutableList();
		this.concurrentExecutor = SubmitterExecutorAdapter.adaptExecutor(buildConcurrentExecutor());

	}

	@Override
	public void accept(MessageDigest messageDigest, Object input) {
		Objects.requireNonNull(messageDigest, () -> MessageDigest.class.getName() + " required");
		if (input == null)
			return;
		var queue = new LinkedBlockingQueue<Future<Object>>();
		BooleanSupplier queuePeek = () -> Optional.ofNullable(queue.peek()).filter(Future::isDone).isPresent();
		BooleanSupplier queuePoll = () -> Optional.ofNullable(queue.poll()).map(Completion::join).map(completion -> {
			Throws.unchecked(messageDigest, completion.resultOrThrow(), this::update);
			return true;
		}).orElse(false);
		try {
			flatten(input).forEachOrdered(nextInput -> {
				queue.add(concurrentExecutor.submit(() -> transform(nextInput)));
				if (queuePeek.getAsBoolean())
					queuePoll.getAsBoolean();
			});
			while (queuePoll.getAsBoolean()) {}
		} catch (Throwable t) {
			Streams.generate(queue::poll).takeWhile(Objects::nonNull).forEach(future -> {
				var result = Completion.tryGet(future).map(Completion::result).orElse(null);
				future.cancel(true);
				close(result);
			});
			throw t;
		}

	}

	protected Object transform(Object input) throws IOException {
		try {
			if (Hashable.class.isInstance(input))
				return HASHABLE_CACHE.get((Hashable) input, Throws.functionUnchecked(this::transformLoad));
			return transformLoad(input);
		} catch (Throwable t) {
			close(input);
			throw t;
		}
	}

	protected Object transformLoad(Object input) throws IOException {
		// transform objects
		boolean mod;
		do {
			mod = false;
			for (var transformFunction : transformFunctions) {
				var transformed = transformFunction.apply(input);
				if (transformed == null || transformed == input)
					continue;
				LogConsumer.log(LOG_CONSUMER, input, (l, v) -> {
					l.log("transform {}[{}] -> {}[{}]", v.getClass().getName(), v, transformed.getClass().getName(),
							transformed);
				});
				close(input);
				input = transformed;
				mod = true;
				break;
			}
		} while (mod);
		return input;
	}

	protected void update(MessageDigest messageDigest, Object input) throws IOException {
		try {
			// update objects
			for (var updateFunction : updateFunctions) {
				var result = updateFunction.apply(input, messageDigest);
				if (result == null || !result)
					continue;
				LogConsumer.log(LOG_CONSUMER, input, (l, v) -> {
					l.log("write {}[{}] with {}", v.getClass().getName(), v, updateFunction);
				});
				return;
			}
		} finally {
			close(input);
		}
		var errorMessage = String.format("write failed. input:%s messageDigest:%s", input.getClass().getName(),
				messageDigest.getAlgorithm());
		throw new IllegalArgumentException(errorMessage);
	}

	protected Iterable<? extends ThrowingFunction<? super Object, ? extends Object, ? extends IOException>> buildTransformFunctions() {
		var functions = new ArrayList<ThrowingFunction<? super Object, ? extends Object, ? extends IOException>>();
		functions.add(createTransformFunction(Hashable.class, Hashable::hash));
		functions.add(createTransformFunction(Enum.class, Enum::name));
		functions.add(createTransformFunction(URI.class, Object::toString));
		functions.add(createTransformFunction(URL.class, Object::toString));
		functions.add(createTransformFunction(Bytes.class, Bytes::array));
		functions.add(createTransformFunction(Class.class, v -> CoreReflections.getNamedClassType(v).getName()));
		functions.add(createTransformFunction(Path.class, Path::toFile));
		functions.add(createTransformFunction(File.class, v -> {
			return !v.exists() ? EmptyArrays.ofByte() : new FileInputStream(v);
		}));
		functions.add(createTransformFunction(CharSequence.class, v -> {
			return new CharSequenceInputStream(v, Utils.Bits.getDefaultCharset());
		}));
		return functions;
	}

	protected Iterable<? extends ThrowingBiFunction<? super Object, MessageDigest, Boolean, ? extends IOException>> buildUpdateFuntions() {
		var functions = new ArrayList<ThrowingBiFunction<? super Object, MessageDigest, Boolean, ? extends IOException>>();
		functions.add(createUpdateFuntion(byte.class, (v, md) -> md.update(v)));
		functions.add(createUpdateFuntion(byte[].class, (v, md) -> md.update(v)));
		functions.add(createUpdateFuntion(ByteBuffer.class, (v, md) -> md.update(v)));
		functions.add(createWriteDigestOutputStreamFunction(InputStream.class, (v, dos) -> {
			try (v) {
				v.transferTo(dos);
			}
		}));
		functions.add(createWriteDigestOutputStreamFunction(Serializable.class, (v, dos) -> {
			try (var oos = new ObjectOutputStream(dos)) {
				oos.writeObject(v);
			}
		}));
		return functions;
	}

	protected Executor buildConcurrentExecutor() {
		String threadName = MessageDigestUpdater.class.getSimpleName() + "-"
				+ Long.toHexString(FNV.hash64(Sequencer.instance().next().values().toArray()));
		String threadNameDirect = threadName + "-direct";
		var threadThrottle = new Semaphore(MachineConfig.logicalCoreCount());
		Executor executor = command -> {
			if (threadThrottle.tryAcquire()) {
				((ListenableFuture<?>) command).listener(threadThrottle::release);
				CoreTasks.executor().execute(new ThreadRenamingRunnable(command, threadName, false));
			} else
				new ThreadRenamingRunnable(command, threadNameDirect, false).run();
		};
		return executor;
	}

	protected static <X, Y> ThrowingFunction<? super Object, ? extends Object, ? extends IOException> createTransformFunction(
			Class<X> inputType, ThrowingFunction<? super X, ? extends Y, ? extends IOException> action) {
		Objects.requireNonNull(inputType);
		Objects.requireNonNull(action);
		return input -> {
			if (input == null || !CoreReflections.isInstance(inputType, input))
				return input;
			return action.apply((X) input);
		};
	}

	protected static <X, Y> ThrowingBiFunction<? super Object, MessageDigest, Boolean, ? extends IOException> createUpdateFuntion(
			Class<X> inputType, ThrowingBiConsumer<? super X, MessageDigest, ? extends IOException> action) {
		Objects.requireNonNull(inputType);
		Objects.requireNonNull(action);
		return (input, md) -> {
			if (input == null || !CoreReflections.isInstance(inputType, input))
				return false;
			action.accept((X) input, md);
			return true;
		};
	}

	protected static <X, Y> ThrowingBiFunction<? super Object, MessageDigest, Boolean, ? extends IOException> createWriteDigestOutputStreamFunction(
			Class<X> inputType, ThrowingBiConsumer<? super X, DigestOutputStream, ? extends IOException> action) {
		Objects.requireNonNull(action);
		return createUpdateFuntion(inputType, (input, md) -> {
			try (var dos = new DigestOutputStream(OutputStream.nullOutputStream(), md)) {
				action.accept(input, dos);
			}
		});
	}

	private static StreamEx<Object> flatten(Object input) {
		return Streams.flatten(input, v -> {
			if (v == null || Nada.get().equals(v))
				return Streams.empty();
			if (Hashable.class.isInstance(v) || Bytes.class.isInstance(v))
				return Streams.of(v);
			if (byte[].class.isInstance(v))
				return Streams.of(v);
			return null;
		});
	}

	protected static void close(Object input) {
		if (!(input instanceof AutoCloseable))
			return;
		LogConsumer.log(LOG_CONSUMER, l -> l.log("closing {}[{}]", input.getClass().getName(), input));
		Throws.unchecked(((AutoCloseable) input), AutoCloseable::close);
	}

	public static void main(String[] args) {
		Runnable runnable = () -> {};
		for (int i = 0; i < 10; i++) {
			var startedAt = System.currentTimeMillis();
			var md = Utils.Crypto.getMessageDigestMD5();
			MessageDigestUpdater.get().accept(md, new Hashable() {

				@Override
				public Bytes hash() {
					return Bytes.from(69);
				}
			});
			MessageDigestUpdater.get().accept(md, new Date(0));
			MessageDigestUpdater.get().accept(md, new MessageDigestUpdater().getClass());
			MessageDigestUpdater.get().accept(md, URI.create("http://yahoo.com"));
			MessageDigestUpdater.get().accept(md, runnable.getClass());
			MessageDigestUpdater.get().accept(md, new File("test"));
			System.out.println(Bytes.from(md.digest()).encodeHex());
			MessageDigestUpdater.get()
					.accept(md, IntStreamEx.range(500).boxed().map(nil -> UUID.randomUUID()).toList());

			System.out.println(System.currentTimeMillis() - startedAt);

		}
	}

}
