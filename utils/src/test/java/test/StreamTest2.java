package test;

import java.util.function.BiFunction;
import java.util.function.Function;

import com.lfp.joe.utils.Utils;

import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;

public class StreamTest2 {

	public static void main(String[] args) {
		{
			var iter = createStream().chain(Utils.Lots::flatMap).iterator();
			while (iter.hasNext()) {
				System.out.println("complete: " + iter.next());
			}
		}
		{
			var iter = createStream().chain(Utils.Lots::flatMap).iterator();
			while (iter.hasNext()) {
				System.out.println("complete: " + iter.next());
			}
		}
		{
			var iter = createStream().flatMap(Function.identity()).iterator();
			while (iter.hasNext()) {
				System.out.println("complete: " + iter.next());
			}
		}
		{
			createStream().flatMap(Function.identity()).forEach(v -> {
				System.out.println("complete: " + v);
			});
		}
	}

	private static StreamEx<StreamEx<String>> createStream() {
		BiFunction<String, Integer, StreamEx<String>> streamFunc = (name, count) -> {
			return IntStreamEx.range(count).mapToObj(v -> {
				var message = String.format("%s - %s/%s", name, v, count);
				System.out.println("generating: " + message);
				Utils.Functions.unchecked(() -> Thread.sleep(500));
				return message;
			});
		};
		return IntStreamEx.range(3).mapToObj(v -> {
			StreamEx<String> next = streamFunc.apply("stream " + v, 5);
			return next;
		});
	}

}
