package test;

import java.util.stream.Collectors;

import com.lfp.joe.utils.Utils;

import one.util.streamex.IntStreamEx;

public class CloseStreamTest {

	public static void main(String[] args) {
		var stream = IntStreamEx.range(10).mapToObj(v -> v);
		stream = stream.parallel().map(v -> {
			try {
				Thread.sleep(Utils.Crypto.getRandomInclusive(100, 1000));
			} catch (InterruptedException e) {
				throw java.util.Optional.ofNullable(e).filter(java.lang.RuntimeException.class::isInstance)
						.map(java.lang.RuntimeException.class::cast).orElseGet(java.lang.RuntimeException::new);
			}
			return v;
		});
		stream = stream.parallel();
		if (true) {
			stream = Utils.Lots.onComplete(stream, () -> {
				System.out.println("done");
			});

			stream.forEach(v -> {
				System.out.println(v);
			});
		} else {
			var list = stream.collect(Collectors.toList());
			System.out.println(list);
		}

	}

}
