package test;

import java.time.Duration;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Stream;

import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

public class ParallelMapTest {

	public static void main(String[] args) {
		var input = Stream.iterate(0, v -> {
			return v < 24;
		}, v -> {
			return v + 1;
		});
		var stream = StreamEx.of(input).map(v -> {
			System.out.println("submiting:" + v);
			return v;
		});
		var mappedStream = Utils.Lots.parallelMap(stream, v -> {
			System.out.println("mapping started:" + v + " thread:" + Thread.currentThread().getName());
			Utils.Functions.unchecked(() -> Thread.sleep(Duration.ofSeconds(5).toMillis()));
			System.out.println("mapping complete:" + v);
			return v;
		}, ops -> {
			ops.executorCoreCount();
		});
		var fjp = new ForkJoinPool(100);
		try (mappedStream;) {
			mappedStream.parallel(fjp).forEach(v -> {
				System.out.println("completed:" + v);
			});
		} finally {
			fjp.shutdownNow();
		}
		System.err.println("done");
	}

}
