package test;

import com.lfp.joe.core.lot.Lot; import com.lfp.joe.core.lot.AbstractLot; import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.lot.AbstractLot;

import one.util.streamex.StreamEx;

public class StreamTest {

	public static void main(String[] args) {
		// set first iterable max index randomly
		int MAX_INDEX = ThreadLocalRandom.current().nextInt(5, 10 + 1);
		System.out.println("max index: " + MAX_INDEX);
		// create slow iterable
		Iterable<String> iterable1 = () -> new AbstractLot.Indexed<String>() {

			private int index = -1;

			@Override
			protected String computeNext(long index) {
				index++;
				if (index >= MAX_INDEX) {
					return this.end();
				}
				System.out.println("dummy computing index: " + index);
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					throw new java.lang.RuntimeException(e);
				}
				return "iterable " + index;
			}
		};
		// create list
		Iterable<String> iterable2 = Arrays.asList("list index 1", "list index 2", "list index 3");
		// create a stream supplier
		Supplier<Stream<String>> streamSupplier = () -> Arrays.asList(iterable1, iterable2).stream()
				.flatMap(i -> StreamSupport.stream(i.spliterator(), false));
		{
			System.out.println("\n***testing lfp joe***");
			var ibleStream = Utils.Lots.stream(Arrays.asList(iterable1, iterable2));
			ibleStream.flatMap(v -> Utils.Lots.stream(v)).forEach(str -> {
				System.out.println("for each - " + str);
			});
		}
		{
			System.out.println("\n***testing lfp joe***");
			var ibleStream = StreamEx.of(Arrays.asList(iterable1, iterable2));
			Iterator<String> iter = Utils.Lots.flatMapIterables(ibleStream).iterator();
			while (iter.hasNext()) {
				System.out.println("lfp-joe - " + iter.next());
			}
		}

		{ // print using for each
			System.out.println("\n***testing streamEx***");
			var ibleStream = StreamEx.of(Arrays.asList(iterable1, iterable2));
			ibleStream.flatMap(v -> StreamEx.of(v.spliterator())).forEach(str -> {
				System.out.println("for each - " + str);
			});
		}
		{ // print using for each
			System.out.println("\n***testing for each***");
			streamSupplier.get().forEach(str -> {
				System.out.println("for each - " + str);
			});
		}
		{
			System.out.println("\n***testing iterator***");
			Iterator<String> iter = streamSupplier.get().iterator();
			while (iter.hasNext()) {
				System.out.println("iterator - " + iter.next());
			}
		}

	}

}