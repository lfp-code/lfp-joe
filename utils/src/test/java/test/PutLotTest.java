package test;

import com.lfp.joe.threads.Threads;
import java.util.ArrayList;
import java.util.List;
import java.util.Spliterators;
import java.util.concurrent.ExecutionException;

import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.core.process.future.Pact;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.lots.PutLot;

import net.tascalate.concurrent.Pact;

public class PutLotTest {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		var putsi = PutLot.create();
		var iter = Spliterators.iterator(putsi);
		new Thread(() -> {
			Utils.Functions.unchecked(() -> Thread.sleep(1_000));
			// putsi.close();
		}).start();
		// System.out.println(iter.hasNext());
		List<Pact<?>> futures = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			if (true) {
				putsi.put("complete quick");
				// piter.complete();
			}
			var attempt = i;
			var future = Threads.Pools.centralPool().submit(() -> {
				Thread.sleep(Utils.Crypto.getRandomInclusive(500, 2000));
				var message = "complete " + attempt;
				System.out.println("generated:" + message);
				if (attempt == 5) {
					putsi.close();
				}
				return message;
			});
			future.whenComplete((v, t) -> {
				if (t != null)
					t.printStackTrace();
				else {
					var put = putsi.put(v);
					System.out.println("put:" + v + " success:" + put);
				}
			});
			futures.add(future);
		}
		var allFutures = Pact.all(futures);
		allFutures.whenComplete((v, t) -> putsi.close());
		var stream = Utils.Lots.stream(iter);
		// stream = stream.limit(5);
		stream.forEach(v -> {
			System.out.println("read complete:" + v);
		});
		System.out.println("for each done");
		allFutures.get();
		System.out.println(iter.hasNext());
		Thread.sleep(500);
		System.err.println("done");
	}

}
