package test;

import java.time.Duration;
import java.util.Arrays;

import com.lfp.joe.core.function.Durations;
import com.lfp.joe.utils.Resources;

import one.util.streamex.IntStreamEx;

public class ResourcesTest {

	public static void main(String[] args) {
		var arr = IntStreamEx.range(25).boxed().map(v -> {
			var nanoTime = System.nanoTime();
			var file = Resources.getResourceFile("resource.txt");
			var elapsed = System.nanoTime() - nanoTime;
			System.out.println(file);
			return Durations.toMillis(Duration.ofNanos(elapsed));
		}).toArray();
		System.out.println(Arrays.toString(arr));
	}
}
