package test;

import java.util.Date;

import com.lfp.joe.introspector.IntrospectorLFP;

public class HashableTest {

	public static void main(String[] args) {

		var pds = IntrospectorLFP.getBeanInfo(TestDate.class).getPropertyDescriptors();
		for (var pd : pds)
			System.out.println(pd.getName());
	}

	private static class TestDate extends Date {
		@Override
		public long getTime() {
			return 0;
		}

		public String test() {
			return null;
		}

		@Override
		public int hashCode() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return "";
		}
	}
}
