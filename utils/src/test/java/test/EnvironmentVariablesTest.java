package test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;

import com.lfp.joe.utils.Utils;

import one.util.streamex.EntryStream;

public class EnvironmentVariablesTest {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) throws IOException {
		File outFile0 = Utils.Files.tempFile(THIS_CLASS, "outFile0.txt");
		File outFile1 = Utils.Files.tempFile(THIS_CLASS, "outFile1.txt");
		for (var file : List.of(outFile0, outFile1)) {
			Files.writeString(file.toPath(), String.format("test file %s", file.getName()), StandardCharsets.UTF_8);
		}
		Utils.Machine.setEnvironmentVariable("TEST_FILE", outFile0.getAbsolutePath());
		Utils.Machine.setEnvironmentVariable("TEST_12_FILE", outFile1.getAbsolutePath());
		EntryStream.of(System.getenv()).forEach(ent -> {
			System.out.println(ent);
		});
		Utils.Machine.streamEnvironmentVariables("_", "_FILE").forEach(ent -> {
			System.out.println(ent);
		});
	}

}
