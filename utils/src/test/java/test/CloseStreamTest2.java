package test;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.lfp.joe.utils.Utils;

import one.util.streamex.IntStreamEx;

public class CloseStreamTest2 {

	public static void main(String[] args) {
		var streams = IntStreamEx.range(10).mapToObj(v -> {
			System.out.println("mapping:" + v);
			return Stream.of(v).onClose(() -> {
				System.out.println("inner stream onClose");
			});
		}).onClose(() -> {
			System.out.println("streams onClose");
		});
		try (streams) {
			var stream = Utils.Lots.flatMap(streams);
			stream = stream.onClose(() -> {
				System.out.println("stream onClose");
			});
			stream = Utils.Lots.onComplete(stream, () -> {
				System.out.println("stream onComplete");
			});
			var list = stream.collect(Collectors.toList());
			System.out.println(list);
		}
	}

}
