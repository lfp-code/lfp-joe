package test;

import java.util.concurrent.Callable;

public class LogTest {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) {
		Callable<String> c = null;
		try {
			c.call();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e) : new RuntimeException(e);
		}
		logger.info("wow");
	}

}
