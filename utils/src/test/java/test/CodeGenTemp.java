package test;

import java.util.regex.Pattern;

import com.lfp.joe.utils.Utils;

public class CodeGenTemp {

	public static void main(String[] args) {
		String code = "public int hashCode() {\n" + "		return xxx.hashCode();\n" + "	}\n" + "\n"
				+ "	public boolean equals(Object obj) {\n" + "		return xxx.equals(obj);\n" + "	}\n" + "\n"
				+ "	public List<String> getCommand() {\n" + "		return xxx.getCommand();\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor command(List<String> command) {\n" + "		return xxx.command(command);\n"
				+ "	}\n" + "\n" + "	public ProcessExecutor command(String... command) {\n"
				+ "		return xxx.command(command);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor command(Iterable<String> command) {\n"
				+ "		return xxx.command(command);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor commandSplit(String commandWithArgs) {\n"
				+ "		return xxx.commandSplit(commandWithArgs);\n" + "	}\n" + "\n"
				+ "	public File getDirectory() {\n" + "		return xxx.getDirectory();\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor directory(File directory) {\n" + "		return xxx.directory(directory);\n"
				+ "	}\n" + "\n" + "	public Map<String, String> getEnvironment() {\n"
				+ "		return xxx.getEnvironment();\n" + "	}\n" + "\n" + "	public String toString() {\n"
				+ "		return xxx.toString();\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor environment(Map<String, String> env) {\n"
				+ "		return xxx.environment(env);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor environment(String name, String value) {\n"
				+ "		return xxx.environment(name, value);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor redirectErrorStream(boolean redirectErrorStream) {\n"
				+ "		return xxx.redirectErrorStream(redirectErrorStream);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor exitValueAny() {\n" + "		return xxx.exitValueAny();\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor exitValueNormal() {\n" + "		return xxx.exitValueNormal();\n" + "	}\n"
				+ "\n" + "	public ProcessExecutor exitValue(Integer exitValue) {\n"
				+ "		return xxx.exitValue(exitValue);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor exitValues(Integer... exitValues) {\n"
				+ "		return xxx.exitValues(exitValues);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor exitValues(int[] exitValues) {\n"
				+ "		return xxx.exitValues(exitValues);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor timeout(long timeout, TimeUnit unit) {\n"
				+ "		return xxx.timeout(timeout, unit);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor stopper(ProcessStopper stopper) {\n" + "		return xxx.stopper(stopper);\n"
				+ "	}\n" + "\n" + "	public ExecuteStreamHandler streams() {\n" + "		return xxx.streams();\n"
				+ "	}\n" + "\n" + "	public ProcessExecutor streams(ExecuteStreamHandler streams) {\n"
				+ "		return xxx.streams(streams);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor closeTimeout(long timeout, TimeUnit unit) {\n"
				+ "		return xxx.closeTimeout(timeout, unit);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor redirectInput(InputStream input) {\n"
				+ "		return xxx.redirectInput(input);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor redirectOutput(OutputStream output) {\n"
				+ "		return xxx.redirectOutput(output);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor redirectError(OutputStream output) {\n"
				+ "		return xxx.redirectError(output);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor redirectOutputAlsoTo(OutputStream output) {\n"
				+ "		return xxx.redirectOutputAlsoTo(output);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor redirectErrorAlsoTo(OutputStream output) {\n"
				+ "		return xxx.redirectErrorAlsoTo(output);\n" + "	}\n" + "\n"
				+ "	public PumpStreamHandler pumps() {\n" + "		return xxx.pumps();\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor readOutput(boolean readOutput) {\n"
				+ "		return xxx.readOutput(readOutput);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor info(Logger log) {\n" + "		return xxx.info(log);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor debug(Logger log) {\n" + "		return xxx.debug(log);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor info(String name) {\n" + "		return xxx.info(name);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor debug(String name) {\n" + "		return xxx.debug(name);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor info() {\n" + "		return xxx.info();\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor debug() {\n" + "		return xxx.debug();\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor redirectOutputAsInfo(Logger log) {\n"
				+ "		return xxx.redirectOutputAsInfo(log);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor redirectOutputAsDebug(Logger log) {\n"
				+ "		return xxx.redirectOutputAsDebug(log);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor redirectOutputAsInfo(String name) {\n"
				+ "		return xxx.redirectOutputAsInfo(name);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor redirectOutputAsDebug(String name) {\n"
				+ "		return xxx.redirectOutputAsDebug(name);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor redirectOutputAsInfo() {\n" + "		return xxx.redirectOutputAsInfo();\n"
				+ "	}\n" + "\n" + "	public ProcessExecutor redirectOutputAsDebug() {\n"
				+ "		return xxx.redirectOutputAsDebug();\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor redirectErrorAsInfo(Logger log) {\n"
				+ "		return xxx.redirectErrorAsInfo(log);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor redirectErrorAsDebug(Logger log) {\n"
				+ "		return xxx.redirectErrorAsDebug(log);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor redirectErrorAsInfo(String name) {\n"
				+ "		return xxx.redirectErrorAsInfo(name);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor redirectErrorAsDebug(String name) {\n"
				+ "		return xxx.redirectErrorAsDebug(name);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor redirectErrorAsInfo() {\n" + "		return xxx.redirectErrorAsInfo();\n"
				+ "	}\n" + "\n" + "	public ProcessExecutor redirectErrorAsDebug() {\n"
				+ "		return xxx.redirectErrorAsDebug();\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor addDestroyer(ProcessDestroyer destroyer) {\n"
				+ "		return xxx.addDestroyer(destroyer);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor destroyer(ProcessDestroyer destroyer) {\n"
				+ "		return xxx.destroyer(destroyer);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor destroyOnExit() {\n" + "		return xxx.destroyOnExit();\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor listener(ProcessListener listener) {\n"
				+ "		return xxx.listener(listener);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor addListener(ProcessListener listener) {\n"
				+ "		return xxx.addListener(listener);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor removeListener(ProcessListener listener) {\n"
				+ "		return xxx.removeListener(listener);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor removeListeners(Class<? extends ProcessListener> listenerType) {\n"
				+ "		return xxx.removeListeners(listenerType);\n" + "	}\n" + "\n"
				+ "	public ProcessExecutor clearListeners() {\n" + "		return xxx.clearListeners();\n" + "	}\n"
				+ "\n" + "	public ProcessExecutor setMessageLogger(MessageLogger messageLogger) {\n"
				+ "		return xxx.setMessageLogger(messageLogger);\n" + "	}\n" + "\n"
				+ "	public ProcessResult execute()\n"
				+ "			throws IOException, InterruptedException, TimeoutException, InvalidExitValueException {\n"
				+ "		return xxx.execute();\n" + "	}\n" + "\n"
				+ "	public ProcessResult executeNoTimeout() throws IOException, InterruptedException, InvalidExitValueException {\n"
				+ "		return xxx.executeNoTimeout();\n" + "	}\n" + "\n"
				+ "	public void checkExitValue(ProcessResult result) throws InvalidExitValueException {\n"
				+ "		xxx.checkExitValue(result);\n" + "	}";
		var pattern = Pattern.compile("public ProcessExecutor ");
		var matcher = pattern.matcher(code);
		while (matcher.find()) {
			var pre = matcher.group();
			var post = code.substring(matcher.end());
			var entry = Utils.Strings.getBoundaries(post, "{", "}");
			post = post.substring(0, entry.getValue() + 1);
			var method = pre + post;
			method = method.replace("public ProcessExecutor ", "public ProcessExecutorLFP ");
			System.out.println(method);
		}

	}
}
