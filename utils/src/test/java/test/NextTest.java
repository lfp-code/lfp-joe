package test;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

import com.lfp.joe.core.function.Throws;

public class NextTest {

	public static void main(String[] args) throws InterruptedException {
		var arr = new int[] { 0 };
		var stream = Stream.iterate(Optional.<Integer>empty(), v -> arr[0] != Integer.MIN_VALUE, prev -> {
			synchronized (arr) {
				var nextIndex = arr[0] + 1;
				if (nextIndex >= 100)
					return null;
				System.out.println("starting:" + nextIndex);
				Throws.unchecked(() -> Thread.sleep(50));
				System.out.println("done:" + nextIndex);
				arr[0] = nextIndex;
				return Optional.of(nextIndex);
			}
		});
		var iterator = stream.spliterator();
		for (int i = 0; i < 10; i++) {
			new Thread(() -> {
				while (iterator.hasNext())
					System.out.println(Arrays.asList(iterator.next()));
			}).start();
		}
		Thread.currentThread().join();
	}

}
