package test;

import com.lfp.joe.core.lot.Lot; import com.lfp.joe.core.lot.AbstractLot; import com.lfp.joe.core.lot.AbstractLot;
import com.lfp.joe.core.lot.Lot;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

public class StreamTest3 {

	public static void main(String[] args) {
		{
			var streams = createStream().map(v -> {
				return v.onClose(() -> {
					System.out.println("inner stream close");
				});
			}).onClose(() -> {
				System.out.println("stream close");
			});
			var stream = streams.chain(Utils.Lots::flatMap);
			stream = stream.onClose(() -> {
				System.out.println("flat map close");
			});
			try (var streamF = stream;) {
				var iter = streamF.iterator();
				while (iter.hasNext()) {
					System.out.println("complete: " + iter.next());
				}
			}
		}
	}

	private static StreamEx<StreamEx<String>> createStream() {
		var lot = new AbstractLot.Indexed<Lot<String>>() {

			@Override
			protected Lot<String> computeNext(long index1) {
				if (index1 >= 3)
					return this.end();
				var innerLot = new AbstractLot.Indexed<String>() {

					@Override
					protected String computeNext(long index2) {
						var count = 5;
						if (index2 >= count)
							return this.end();
						var message = String.format("stream - %s/%s", index2 + 1, count);
						System.out.println("generating: " + message);
						Utils.Functions.unchecked(() -> Thread.sleep(500));
						return message;
					}
				};
				innerLot.onScrap(() -> {
					System.out.println("innerLot close");
				});
				return innerLot;
			}
		};
		return Utils.Lots.stream(lot).map(Utils.Lots::stream);
	}

}