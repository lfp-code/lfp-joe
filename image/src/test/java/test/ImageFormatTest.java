package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import com.lfp.joe.image.Images;

public class ImageFormatTest {

	public static void main(String[] args) throws IOException {
		File temp = new File("temp/test");
		for (var image : temp.listFiles())
			printImageFormat(image);
	}

	private static void printImageFormat(File image) throws IOException {
		try (var fis = new FileInputStream(image);) {
			Images.streamFormats(fis).forEach(v -> {
				System.out.printf("%s - %s\n", image.getName(), v);
			});
		}
	}
}
