package test;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import com.lfp.joe.utils.Utils;

import javax.imageio.ImageIO;

import com.github.kilianB.hashAlgorithms.AverageHash;
import com.github.kilianB.hashAlgorithms.HashingAlgorithm;
import com.github.kilianB.hashAlgorithms.WaveletHash;

public class JImageHashTest {

	public static void main(String[] args) throws IOException {
		File temp = new File("temp/test");
		var hasher = new WaveletHash(64, 4);
		for (var image : temp.listFiles())
			hash(hasher, image);
	}

	private static void hash(HashingAlgorithm hasher, File image) throws IOException {
		BufferedImage bi = ImageIO.read(image);
		var hash = hasher.hash(bi);
		String out = String.format("%s - %s", image.getName(), Utils.Bits.from(hash.toByteArray()).encodeHex());
		System.out.println(out);
	}
}
