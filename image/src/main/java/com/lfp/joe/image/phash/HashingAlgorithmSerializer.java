package com.lfp.joe.image.phash;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.lang3.time.StopWatch;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.kilianB.hashAlgorithms.HashingAlgorithm;
import com.github.kilianB.hashAlgorithms.PerceptiveHash;
import com.github.throwable.beanref.BeanPath;
import com.github.throwable.beanref.BeanRef;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.re2j.Pattern;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.Primitives;
import com.lfp.joe.core.function.Asserts;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.reflections.reflection.MemberPredicate;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.serial.serializer.Serializer;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtConstructor;
import javassist.NotFoundException;
import javassist.bytecode.LocalVariableAttribute;
import one.util.streamex.EntryStream;

@SuppressWarnings({ "unchecked", "rawtypes" })
@Serializer(value = HashingAlgorithm.class, hierarchy = true)
public class HashingAlgorithmSerializer
		implements JsonSerializer<HashingAlgorithm>, JsonDeserializer<HashingAlgorithm> {

	public static final HashingAlgorithmSerializer INSTANCE = new HashingAlgorithmSerializer();

	private static final String DELIMITER = "_";
	private static final MemoizedSupplier<Map<Class<? extends HashingAlgorithm>, ReflectionContext>> HASHING_ALGORITHM_MAPPINGS_S = MemoizedSupplier
			.create(() -> lookupHashingAlgorithmMappings());
	private static final Cache<Bytes, String> SERIALIZATION_CACHE = Caches
			.newCaffeineBuilder(-1, null, Duration.ofSeconds(1))
			.build();

	public JsonElement serialize(HashingAlgorithm src) {
		Type typeOfSrc = src == null ? HashingAlgorithm.class : src.getClass();
		return serialize(src, typeOfSrc, Serials.Gsons.serializationContext(Serials.Gsons.get()));
	}

	@Override
	public JsonElement serialize(HashingAlgorithm src, Type typeOfSrc, JsonSerializationContext context) {
		if (src == null)
			return JsonNull.INSTANCE;
		var str = trySerializeString(src).orElse(null);
		if (str != null)
			return new JsonPrimitive(str);
		return context.serialize(Utils.Bits.serialize(src), Bytes.class);
	}

	public HashingAlgorithm deserialize(JsonElement json) {
		return deserialize(json, HashingAlgorithm.class, Serials.Gsons.deserializationContext(Serials.Gsons.get()));
	}

	@Override
	public HashingAlgorithm deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		if (json == null || json.isJsonNull())
			return null;
		var str = Serials.Gsons.tryGetAsString(json).orElse(null);
		var hashingAlgorithm = tryDeserializeString(str).orElse(null);
		if (hashingAlgorithm != null)
			return hashingAlgorithm;
		Bytes bytes = context.deserialize(json, Bytes.class);
		return Utils.Bits.deserialize(bytes);
	}

	private static Optional<String> trySerializeString(HashingAlgorithm hashingAlgorithm) {
		if (hashingAlgorithm == null)
			return Optional.empty();
		var reflectionContext = HASHING_ALGORITHM_MAPPINGS_S.get().get(hashingAlgorithm.getClass());
		if (reflectionContext == null)
			return Optional.empty();
		var hash = reflectionContext.hash(hashingAlgorithm);
		var str = SERIALIZATION_CACHE.get(hash, nil -> {
			return Throws.unchecked(() -> serialize(hashingAlgorithm, reflectionContext, hash));
		});
		if (Utils.Strings.isBlank(str))
			return Optional.empty();
		return Optional.of(str);
	}

	private static Optional<HashingAlgorithm> tryDeserializeString(String input) {
		if (Utils.Strings.isBlank(input))
			return Optional.empty();
		List<String> parts = Arrays.asList(input.split(Pattern.quote(DELIMITER)));
		if (parts.size() < 2)
			return Optional.empty();
		var className = parts.get(0);
		ReflectionContext reflectionContext = null;
		{
			List<Predicate<Class<? extends HashingAlgorithm>>> predicates = new ArrayList<>(2);
			predicates.add(v -> v.getName().equals(className));
			predicates.add(v -> v.getSimpleName().equals(className));
			var mappings = HASHING_ALGORITHM_MAPPINGS_S.get();
			for (var predicate : predicates) {
				var estream = Utils.Lots.stream(mappings);
				estream = estream.filterKeys(v -> {
					if (!predicate.test(v))
						return false;
					return true;
				});
				estream = estream.filterValues(v -> v.constructorFields.size() == parts.size() - 1);
				reflectionContext = estream.values().findFirst().orElse(null);
				if (reflectionContext != null)
					break;
			}
		}
		if (reflectionContext == null)
			return Optional.empty();
		var initArgs = EntryStream.of(reflectionContext.constructorFields).map(ent -> {
			var strArgument = parts.get(ent.getKey() + 1);
			var field = ent.getValue();
			if (Utils.Strings.isBlank(strArgument))
				return Primitives.getDefaultValue(field.getType());
			if (String.class.isAssignableFrom(field.getType()))
				return strArgument;
			var wrapperType = Primitives.getWrapperType(field.getType()).orElse(null);
			if (wrapperType != null)
				return Primitives.valueOf(wrapperType, strArgument);
			throw Asserts.createException("unable to parse argument:%s type:%s", strArgument, field.getType());
		}).toArray();
		HashingAlgorithm result;
		try {
			result = reflectionContext.constructor.newInstance(initArgs);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e) : new RuntimeException(e);
		}
		return Optional.of(result);
	}

	private static String serialize(HashingAlgorithm hashingAlgorithm, ReflectionContext reflectionContext, Bytes hash)
			throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Object[] initArgs = Utils.Lots.stream(reflectionContext.constructorFields).map(v -> {
			var value = Throws.unchecked(() -> v.get(hashingAlgorithm));
			return value;
		}).toArray();
		var newInstance = reflectionContext.constructor.newInstance(initArgs);
		var newInstanceHash = reflectionContext.hash(newInstance);
		if (!hash.equals(newInstanceHash))
			return null;
		return Utils.Lots.stream(initArgs)
				.prepend(hashingAlgorithm.getClass().getSimpleName())
				.map(v -> v == null ? "" : v)
				.joining(DELIMITER);
	}

	private static Map<Class<? extends HashingAlgorithm>, ReflectionContext> lookupHashingAlgorithmMappings() {
		var stream = JavaCode.Reflections.streamSubTypesOf(HashingAlgorithm.class);
		stream = stream.filter(v -> {
			if (v.isInterface())
				return false;
			if (Modifier.isAbstract(v.getModifiers()))
				return false;
			return true;
		});
		var estream = stream.mapToEntry(v -> {
			var reflectionContexts = Throws.unchecked(() -> lookupReflectionContexts(v));
			if (reflectionContexts == null || reflectionContexts.size() != 1)
				return null;
			return reflectionContexts.get(0);
		});
		estream = estream.nonNullValues();
		var map = estream.toImmutableMap();
		return map;
	}

	private static List<ReflectionContext> lookupReflectionContexts(Class<? extends HashingAlgorithm> classType)
			throws NotFoundException {
		List<ReflectionContext> reflectionContexts = new ArrayList<>();
		ClassPool pool = ClassPool.getDefault();
		var ctClass = pool.getCtClass(classType.getName());
		for (var ctor : ctClass.getConstructors()) {
			List<Entry<Class<?>, String>> argumentEntries = lookupConstructorArgumentEntries(ctor);
			if (argumentEntries.isEmpty())
				continue;
			var fieldStream = CoreReflections.streamFields(classType, true)
					.filter(MemberPredicate.create().withModifierNotSTATIC())
					.map(v -> {
						v.setAccessible(true);
						return v;
					});
			var fields = fieldStream.collect(Collectors.toUnmodifiableList());
			var constructorFields = getConstructorFields(classType, argumentEntries, fields);
			if (constructorFields.isEmpty())
				continue;
			var constructor = getConstructor(classType, constructorFields);
			if (constructor == null)
				continue;
			reflectionContexts.add(new ReflectionContext(classType, constructor, fields, constructorFields));
		}
		return reflectionContexts;
	}

	private static List<Entry<Class<?>, String>> lookupConstructorArgumentEntries(CtConstructor ctor)
			throws NotFoundException {
		var methodInfo = ctor.getMethodInfo();
		if (methodInfo == null)
			return List.of();
		var parameterTypes = Utils.Functions.catching(() -> ctor.getParameterTypes(), t -> null);
		if (parameterTypes == null || parameterTypes.length == 0)
			return List.of();
		LocalVariableAttribute nameTable = (LocalVariableAttribute) methodInfo.getCodeAttribute()
				.getAttribute(LocalVariableAttribute.tag);
		if (nameTable == null)
			return List.of();
		var tableLength = nameTable.tableLength();
		if (parameterTypes.length != (tableLength - 1))
			return List.of();
		List<Entry<Class<?>, String>> argumentEntries = new ArrayList<>();
		for (int i = 1; i < tableLength; i++) {
			var variableName = nameTable.variableName(i);
			if (Utils.Strings.isBlank(variableName))
				return List.of();
			CtClass ctClass = parameterTypes[i - 1];
			var ctClassName = ctClass.getName();
			var parameterType = CoreReflections.tryForName(ctClassName).orElse(null);
			if (parameterType == null)
				return List.of();
			argumentEntries.add(Map.entry(parameterType, variableName));
		}
		return argumentEntries;
	}

	private static List<Field> getConstructorFields(Class<? extends HashingAlgorithm> classType,
			List<Entry<Class<?>, String>> argumentEntries, List<Field> fields) {
		List<Field> constructorFields = new ArrayList<>();
		for (var argumentEntry : argumentEntries) {
			Predicate<Field> fieldPredicate;
			{
				var fieldType = argumentEntry.getKey();
				var fieldName = argumentEntry.getValue();
				var memberPredicate = MemberPredicate.create().withModifierNotSTATIC().withName(fieldName);
				fieldPredicate = memberPredicate::test;
				fieldPredicate = fieldPredicate.and(v -> Utils.Types.isAssignableFrom(v.getType(), fieldType));
			}
			Field field = fields.stream().filter(fieldPredicate).findFirst().orElse(null);
			if (field == null)
				return List.of();
			constructorFields.add(field);
		}
		return Collections.unmodifiableList(constructorFields);
	}

	private static Constructor<? extends HashingAlgorithm> getConstructor(Class<? extends HashingAlgorithm> classType,
			List<Field> constructorFields) {
		for (var ctor : JavaCode.Reflections.getConstructors(classType)) {
			var ptypes = ctor.getParameterTypes();
			if (ptypes == null || ptypes.length != constructorFields.size())
				continue;
			var valid = true;
			for (int i = 0; i < ptypes.length && valid; i++) {
				if (!Utils.Types.isAssignableFrom(ptypes[i], constructorFields.get(i).getType()))
					valid = false;
			}
			if (!valid)
				continue;
			return ctor;
		}
		return null;
	}

	private static class ReflectionContext {

		private final Set<BeanPath> beanPaths;

		public final Constructor<? extends HashingAlgorithm> constructor;

		public final List<Field> fields;

		public final List<Field> constructorFields;

		public ReflectionContext(Class<? extends HashingAlgorithm> classType,
				Constructor<? extends HashingAlgorithm> constructor, List<Field> fields,
				List<Field> constructorFields) {
			super();
			this.beanPaths = Utils.Lots.stream(BeanRef.$(Objects.requireNonNull(classType)).all())
					.map(v -> (BeanPath) v)
					.toImmutableSet();
			this.constructor = Objects.requireNonNull(constructor);
			Predicate<List<Field>> fieldPredicate = v -> v != null && !v.isEmpty();
			this.fields = Asserts.validate(fields, fieldPredicate, ctx -> ctx.exceptionArguments("fields required"));
			this.constructorFields = Asserts.validate(constructorFields, fieldPredicate,
					ctx -> ctx.exceptionArguments("constructorFields required"));
		}

		public Bytes hash(HashingAlgorithm hashingAlgorithm) {
			Objects.requireNonNull(hashingAlgorithm);
			beanPaths.forEach(bp -> {
				bp.get(hashingAlgorithm);
			});
			var md = Utils.Crypto.getMessageDigestMD5();
			Utils.Crypto.update(md, hashingAlgorithm.getClass());
			Utils.Crypto.update(md, hashingAlgorithm.algorithmId());
			for (var field : fields) {
				Utils.Crypto.update(md, field.getName());
				var value = Throws.unchecked(() -> field.get(hashingAlgorithm));
				Utils.Crypto.update(md, value);
			}
			return Utils.Bits.from(md.digest());
		}

	}

	public static void main(String[] args) throws NotFoundException, InterruptedException {
		for (int j = 0; j < 1; j++) {
			if (j > 0)
				Thread.sleep(5_000);
			for (int i = 0; i < 10; i++) {
				var sw = i == 0 ? null : StopWatch.createStarted();
				var json = HashingAlgorithmSerializer.INSTANCE.serialize(new PerceptiveHash(64));
				System.out.println(json);
				var ha = HashingAlgorithmSerializer.INSTANCE.deserialize(json);
				System.out.println(ha);
				if (sw != null)
					System.out.println(sw.getTime());
			}
		}
		System.err.println("done");
	}

}
