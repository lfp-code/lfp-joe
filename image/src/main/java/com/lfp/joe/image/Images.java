package com.lfp.joe.image;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import com.github.kilianB.graphics.FastPixel;
import com.github.kilianB.hashAlgorithms.HashingAlgorithm;
import com.github.kilianB.hashAlgorithms.PerceptiveHash;
import com.google.common.net.MediaType;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.io.FileExt;
import com.lfp.joe.core.producer.Producer;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.headers.UserAgents;
import com.lfp.joe.net.http.request.HttpRequests;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.reflections.reflection.MemberPredicate;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.bytes.CountingOutputStream;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;

public class Images {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final List<Supplier<MessageDigest>> MESSAGE_DIGESTS_DEFAULT;
	static {
		List<Supplier<MessageDigest>> list = new ArrayList<>();
		list.add(Utils.Crypto::getMessageDigestMD5);
		list.add(Utils.Crypto::getMessageDigestSHA256);
		list.add(Utils.Crypto::getMessageDigestSHA512);
		MESSAGE_DIGESTS_DEFAULT = Collections.unmodifiableList(list);
	}
	private static final List<HashingAlgorithm> HASHING_ALGORITHMS_DEFAULT = List.of(new PerceptiveHash(1024));
	protected static final Nada FAST_PIXEL_LOGGER_FIX;
	static {
		@SuppressWarnings("unchecked")
		var loggerField = JavaCode.Reflections
				.streamFields(FastPixel.class, true, MemberPredicate.create().withName("LOGGER").withModifierSTATIC())
				.findFirst().orElse(null);
		if (loggerField != null) {
			java.util.logging.Logger logger = (Logger) Throws.unchecked(() -> loggerField.get(null));
			logger.setLevel(java.util.logging.Level.OFF);
		}
		FAST_PIXEL_LOGGER_FIX = Nada.get();
	}

	public static FileExt convert(InputStream inputStream, ImageFormat imageFormat) throws IOException {
		Objects.requireNonNull(inputStream);
		Objects.requireNonNull(imageFormat);
		var outFile = Utils.Files.tempFile(THIS_CLASS,
				String.format("%s.%s", Utils.Crypto.getRandomString(), imageFormat.getExtension()));
		try (inputStream) {
			BufferedImage image = ImageIO.read(inputStream);
			BufferedImage result = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);
			if (ImageFormat.JPEG.equals(imageFormat))
				result.createGraphics().drawImage(image, 0, 0, Color.WHITE, null);
			else
				result.createGraphics().drawImage(image, 0, 0, null);
			ImageIO.write(result, imageFormat.name().toLowerCase(), outFile);
		}
		return outFile;
	}

	public static StreamEx<ImageReader> streamReaders(InputStream inputStream) {
		if (inputStream == null)
			return StreamEx.empty();
		Scrapable onComplete = Scrapable.create();
		onComplete.onScrap(Throws.runnableUnchecked(inputStream::close));
		var producerPredicate = new BiPredicate<Producer<ImageReader>, Consumer<? super ImageReader>>() {

			private Iterator<ImageReader> imageReaders;

			@Override
			public boolean test(Producer<ImageReader> producer, Consumer<? super ImageReader> action) {
				if (producer.getRequestIndex() == 0) {
					ImageInputStream iis = Utils.Functions.catching(() -> {
						return ImageIO.createImageInputStream(inputStream);
					}, t -> null);
					if (iis != null) {
						onComplete.onScrap(Throws.runnableUnchecked(iis::close));
						imageReaders = Utils.Functions.catching(() -> ImageIO.getImageReaders(iis), t -> null);
					}
				}
				if (imageReaders != null && imageReaders.hasNext()) {
					action.accept(imageReaders.next());
					return true;
				}
				return false;
			}
		};
		var stream = Utils.Lots.stream(Producer.create(producerPredicate));
		stream = stream.nonNull();
		stream = Utils.Lots.onComplete(stream, onComplete::scrap);
		return stream;
	}

	public static StreamEx<ImageFormat> streamFormats(InputStream inputStream) {
		return streamReaders(inputStream).map(v -> ImageFormat.tryGet(v).orElse(null)).nonNull().distinct();
	}

	public static Bytes perceptualHash(InputStream inputStream, HashingAlgorithm hasher) throws IOException {
		BufferedImage bi = ImageIO.read(inputStream);
		var hash = hasher.hash(bi);
		return Utils.Bits.from(hash.toByteArray());
	}

	public static ImageContext context(URI uri) throws IOException {
		return context(uri, null, null);
	}

	public static ImageContext context(URI uri, Iterable<? extends MessageDigest> messageDigests,
			Iterable<? extends HashingAlgorithm> hashingAlgorithms) throws IOException {
		var request = HttpRequests.request().uri(uri).header(Headers.USER_AGENT, UserAgents.CHROME_LATEST.get());
		var response = HttpClients.send(request);
		try (var body = response.body()) {
			StatusCodes.validate(response.statusCode(), 2);
			var context = context(body, HeaderMap.of(response.headers()), messageDigests, hashingAlgorithms);
			context = context.toBuilder().uri(uri).build();
			return context;
		}
	}

	public static ImageContext context(InputStream inputStream) throws IOException {
		return context(inputStream, null, null, null);
	}

	public static ImageContext context(InputStream inputStream, Iterable<? extends Entry<String, String>> headerEntries,
			Iterable<? extends MessageDigest> messageDigests, Iterable<? extends HashingAlgorithm> hashingAlgorithms)
			throws IOException {
		final StreamEx<MessageDigest> messageDigestStream;
		{
			StreamEx<MessageDigest> stream = Utils.Lots.stream(messageDigests);
			stream = stream.nonNull();
			stream = stream.ifEmpty(Utils.Lots.defer(() -> {
				return Utils.Lots.stream(MESSAGE_DIGESTS_DEFAULT).map(Supplier::get);
			}));
			stream = stream.nonNull().distinct();
			stream = stream.distinct(MessageDigest::getAlgorithm);
			messageDigestStream = stream;
		}
		final StreamEx<HashingAlgorithm> hashingAlgorithmStream;
		{
			if (hashingAlgorithms == null)
				hashingAlgorithms = Utils.Lots.stream(HASHING_ALGORITHMS_DEFAULT);
			StreamEx<HashingAlgorithm> stream = Utils.Lots.stream(hashingAlgorithms);
			stream = stream.nonNull().distinct();
			hashingAlgorithmStream = stream;
		}
		var tempFile = Utils.Files.tempFile(THIS_CLASS, Utils.Crypto.getRandomString() + ".bin").deleteAllOnScrap(true);
		try (inputStream; tempFile) {
			var fos = new FileOutputStream(tempFile);
			var cos = new CountingOutputStream(fos);
			var algToDigestOutputStream = new LinkedHashMap<String, DigestOutputStream>();
			for (var md : messageDigestStream) {
				var algorithm = md.getAlgorithm();
				if (algToDigestOutputStream.isEmpty())
					algToDigestOutputStream.put(algorithm, new DigestOutputStream(cos, md));
				else {
					var dos = Utils.Lots.last(algToDigestOutputStream.values()).get();
					algToDigestOutputStream.put(algorithm, new DigestOutputStream(dos, md));
				}
			}
			try (inputStream; fos; cos;) {
				var dos = Utils.Lots.last(algToDigestOutputStream.values()).get();
				Utils.Bits.copy(inputStream, dos);
			} finally {
				for (var dos : algToDigestOutputStream.values())
					dos.close();
			}
			var blder = ImageContext.builder();
			{
				Map<String, Bytes> contentHashes = Utils.Lots.stream(algToDigestOutputStream)
						.mapValues(v -> Utils.Bits.from(v.getMessageDigest().digest())).toCustomMap(LinkedHashMap::new);
				blder.contentHashes(contentHashes);
			}
			{
				MediaType contentType = Headers.tryGetMediaType(headerEntries).filter(v -> {
					for (var imageFormat : ImageFormat.values())
						if (imageFormat.asMediaType().equals(v))
							return true;
					return false;
				}).orElse(null);
				if (contentType == null) {
					try (var imageFormats = streamFormats(new FileInputStream(tempFile))) {
						contentType = imageFormats.map(ImageFormat::asMediaType).findFirst().get();
					}
				}
				var headerMap = HeaderMap.of(headerEntries);
				headerMap = headerMap.withReplace(Headers.CONTENT_TYPE, contentType.toString());
				headerMap = headerMap.withReplace(Headers.CONTENT_LENGTH, Objects.toString(cos.getCount()));
				blder.headers(headerMap);
			}
			{
				Map<HashingAlgorithm, Bytes> perceptualHashes = new LinkedHashMap<>();
				for (var hashingAlgorithm : hashingAlgorithmStream) {
					try (var is = new FileInputStream(tempFile)) {
						var perceptualHash = perceptualHash(is, hashingAlgorithm);
						perceptualHashes.put(hashingAlgorithm, perceptualHash);
					}
				}
				blder.perceptualHashes(perceptualHashes);
			}
			return blder.build();
		}
	}

	public static void main(String[] args) throws IOException {
		try (var f = Utils.Files.tempFile("test-123").deleteAllOnScrap(true)) {
			f.mkdirs();
			var coolFile = new FileExt(f, "cool.txt");
			try (coolFile) {
				var writer = coolFile.outputStream().writer();
				var lineStream = IntStreamEx.range(1_000).mapToObj(v -> "line-" + v);
				writer.lines(lineStream);
				writer.flush();
				System.out.println(f.getAbsolutePath());
				try (var lines = coolFile.inputStream().reader().lines()) {
					lines.forEach(v -> {
						System.out.println(v);
					});
				}
				var hash = Utils.Crypto.hashMD5(coolFile.inputStream());
				System.out.println(hash.encodeHex());
			}
			System.out.println(Utils.Crypto.hashMD5(coolFile).encodeHex());
			try (var br = coolFile.inputStream(StandardCharsets.UTF_16BE, null, true).reader()) {
				System.out.println(br.lines().count());
			}
// System.out.println(Utils.Crypto.hashMD5(coolFile.inputStream()).encodeHex());
			System.out.println("done");
		}

		var context = context(URI.create(
				"https://play-lh.googleusercontent.com/xdjWKko-a2DFtICA6tfP0vXSBIDTFl_iprhVQb9HnS-rGjyR1wZckqM721qHsF-2z0U=w3000-h1584-rw"));
		System.out.println(Serials.Gsons.getPretty().toJson(context));
	}
}