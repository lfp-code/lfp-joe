package com.lfp.joe.image;

import java.util.Map.Entry;
import java.util.Optional;

import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.utils.Utils;

import javax.imageio.ImageReader;

import com.google.common.net.MediaType;

import one.util.streamex.StreamEx;

public enum ImageFormat {
	JPEG {
		@Override
		public String getExtension() {
			return "jpg";
		}

	},
	PNG {
		@Override
		public String getExtension() {
			return "png";
		}
	},
	WEBP {
		@Override
		public String getExtension() {
			return "webp";
		}

	};

	public MediaType asMediaType() {
		return MediaType.parse("image/" + this.getExtension().toLowerCase());
	}

	public abstract String getExtension();

	protected StreamEx<String> streamFormatNames() {
		return Utils.Lots.stream(this.name(), this.getExtension()).filter(Utils.Strings::isNotBlank)
				.distinct(v -> v.toLowerCase());
	}

	public static Optional<ImageFormat> tryGet(String input) {
		input = Utils.Strings.trimToNull(input);
		if (input == null)
			return Optional.empty();
		for (var value : ImageFormat.values()) {
			for (var formatName : value.streamFormatNames()) {
				if (Utils.Strings.equalsIgnoreCase(formatName, input))
					return Optional.of(value);
			}
		}
		return Optional.empty();
	}

	public static Optional<ImageFormat> tryGet(ImageReader imageReader) {
		if (imageReader == null)
			return Optional.empty();
		var imageReaderFormatName = Utils.Functions.catching(() -> imageReader.getFormatName(), t -> null);
		return tryGet(imageReaderFormatName);
	}

	public static Optional<ImageFormat> tryGet(Iterable<? extends Entry<String, String>> headers) {
		if (headers == null)
			return Optional.empty();
		var mediaTypeSubType = Headers.tryGetMediaType(headers).map(MediaType::subtype).orElse(null);
		return tryGet(mediaTypeSubType);

	}
}
