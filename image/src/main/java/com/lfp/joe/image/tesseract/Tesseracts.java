package com.lfp.joe.image.tesseract;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.process.ProcessLFP;
import com.lfp.joe.process.Procs;
import com.lfp.joe.process.Which;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

public class Tesseracts {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 0;
	private static final String EXEC_NAME = "tesseract";
	private static final MemoizedSupplier<File> EXEC_S = Utils.Functions.memoize(() -> {
		return Which.get(EXEC_NAME)
				.orElseThrow(() -> new IllegalArgumentException(String.format("%s not found on system", EXEC_NAME)));
	});
	private static final String OUTPUT_BASE = "output";
	private static final String HOCR = "hocr";

	public static CompletableFuture<List<OCRLine>> ocr(File input) {
		Objects.requireNonNull(input);
		Validate.isTrue(input.exists(), "file does not exist:%s", input);
		File tempDir = Utils.Files.tempFile(THIS_CLASS, VERSION, Utils.Crypto.getRandomString());
		tempDir.mkdirs();
		var command = StreamEx
				.of(EXEC_S.get().getAbsolutePath(), "-l", "eng", input.getAbsolutePath(), OUTPUT_BASE, HOCR)
				.joining(" ");
		var cfuture = new CompletableFuture<List<OCRLine>>();
		ProcessLFP pr;
		try {
			pr = Procs.start(command, cfg -> {
				cfg.disableOutputLog();
				cfg.directory(tempDir);
			});
		} catch (IOException e) {
			cfuture.completeExceptionally(e);
			return cfuture;
		}
		Threads.Futures.callbackFailure(pr, t -> cfuture.completeExceptionally(t));
		Threads.Futures.callback(pr, () -> cfuture.complete(List.of()));
		Threads.Futures.callback(cfuture, () -> pr.cancel(true));
		return cfuture;
	}

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		var file = new File("C:\\Users\\reggi\\Desktop\\input.png");
		Tesseracts.ocr(file).get();
		System.out.println(file.getAbsolutePath());
	}

}
