package test;

import java.time.Duration;
import java.util.Date;

import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;

import reactor.core.publisher.Flux;

public class StreamTest {

	public static void main(String[] args) throws InterruptedException {
		var flux = Flux.<Date>create(sink -> {
			Threads.Pools.computationPool().scheduleAtFixedRate(() -> {
				sink.next(new Date());
			}, Duration.ofSeconds(1).toMillis(), Duration.ofSeconds(1).toMillis());

		});
		Streams.of(flux.toStream(1)).forEach(v -> {
			System.out.println(v);
		});
	}
}
