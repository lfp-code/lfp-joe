package test;

import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.threeten.bp.Duration;

import com.lfp.joe.core.function.Muto;

public class DelayedTest {

	public static void main(String[] args) throws InterruptedException {
		var executor = Executors.newScheduledThreadPool(1);
		try {
			run(executor);
			Thread.sleep(10_000);
		} finally {
			executor.shutdownNow();
		}
	}

	private static void run(ScheduledExecutorService executor) {
		{
			var sf = executor.schedule(() -> {
				System.out.println("schedule");
			}, Duration.ofSeconds(1).toMillis(), TimeUnit.MILLISECONDS);
			System.out.println(sf.getDelay(TimeUnit.MILLISECONDS));
		}
		{
			Muto<ScheduledFuture<?>> ref = Muto.create();
			var sf = executor.scheduleAtFixedRate(() -> {
				System.out.println("scheduleAtFixedRate - "
						+ Optional.ofNullable(ref.get()).map(v -> v.getDelay(TimeUnit.MILLISECONDS)).orElse(null));
			}, Duration.ofSeconds(1).toMillis(), Duration.ofSeconds(1).toMillis(), TimeUnit.MILLISECONDS);
			ref.set(sf);
			System.out.println(sf.getDelay(TimeUnit.MILLISECONDS));
		}
		{
			Muto<ScheduledFuture<?>> ref = Muto.create();
			var sf = executor.scheduleWithFixedDelay(() -> {
				System.out.println("scheduleWithFixedDelay - "
						+ Optional.ofNullable(ref.get()).map(v -> v.getDelay(TimeUnit.MILLISECONDS)).orElse(null));
			}, Duration.ofSeconds(1).toMillis(), Duration.ofSeconds(1).toMillis(), TimeUnit.MILLISECONDS);
			ref.set(sf);
			System.out.println(sf.getDelay(TimeUnit.MILLISECONDS));
		}

	}
}
