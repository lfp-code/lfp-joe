package test;

import java.time.Duration;

import com.lfp.joe.reactor.Scheduli;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.Loggers;

public class IteratorTest {

	public static void main(String[] args) {
		Loggers.useVerboseConsoleLoggers();
		var attemptFlux = Flux.just(0l);
		Flux<Long> lazyDelay = Flux.create(sink -> {
			Mono.delay(Duration.ofSeconds(1), Scheduli.unlimited()).subscribe(sink::next, sink::error, sink::complete,
					sink.currentContext());
		});
		attemptFlux = attemptFlux.concatWith(lazyDelay);
		var flux = attemptFlux;
		// flux = flux.publishOn(Scheduli.unlimited());
		flux = flux.subscribeOn(Scheduli.unlimited());
		flux = flux.log();
		flux = flux.doOnNext(v -> {
			System.out.println(v);
		});
		flux.blockLast();
		System.err.println("done");
	}
}
