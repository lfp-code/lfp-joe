package test;

import java.time.Duration;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Durations;
import com.lfp.joe.reactor.Fluxi;
import com.lfp.joe.reactor.support.ParallelMapOptions;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Futures;

import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;
import reactor.core.publisher.Flux;

public class ParallelMapTest2 {

	public static void main(String[] args) {
		var fjp = new ForkJoinPool(MachineConfig.logicalCoreCount());
		var cachedThreadPool = Executors.newCachedThreadPool();
		for (int i = 0; i < 10; i++) {
			for (var parallel : IntStreamEx.range(5).boxed()) {
				var list = IntStreamEx.range(250_000).mapToObj(v -> v + UUID.randomUUID().toString()).toList();
				var stream = Streams.of(list);
				var nanoTime = System.nanoTime();
				Set<String> threadNames = Collections.newSetFromMap(new ConcurrentHashMap<>());
				if (parallel == 0) {
					Function<String, String> mapper = v -> {
						threadNames.add(Thread.currentThread().getName());
						return v.endsWith("c") ? v : null;
					};
					stream = Fluxi
							.parallelMap(Flux.fromStream(stream),
									ParallelMapOptions.<String, String>builder().mapper(mapper)
											.executor(cachedThreadPool).ordered(false).build())
							.as(v -> StreamEx.of(v.toStream()));
				} else if (parallel == 1)
					stream = stream.filter(v -> {
						threadNames.add(Thread.currentThread().getName());
						return v.endsWith("c");
					});
				else if (parallel == 2) {
					stream = stream.parallel().map(v -> {
						threadNames.add(Thread.currentThread().getName());
						return v.endsWith("c") ? Optional.ofNullable(v) : null;
					}).nonNull().chain(Streams.orElse(null));
				} else if (parallel == 3) {
					stream = stream.parallel(fjp).filter(v -> {
						threadNames.add(Thread.currentThread().getName());
						return v.endsWith("c");
					});
				} else if (parallel == 4) {
					var executor = Executors.newFixedThreadPool(MachineConfig.logicalCoreCount());
					stream = stream.map(v -> {
						return executor.submit(() -> {
							threadNames.add(Thread.currentThread().getName());
							return v.endsWith("c") ? v : null;
						});
					}).chain(Futures::completeStream).nonNull();
				}
				var count = new AtomicLong();
				stream.forEachOrdered(v -> count.incrementAndGet());
				System.out.println(count.get());
				System.out
						.println(parallel + ": " + Durations.toMillis(Duration.ofNanos(System.nanoTime() - nanoTime)));
				System.out.println(threadNames);
			}

		}
	}
}
