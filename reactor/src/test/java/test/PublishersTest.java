package test;

import java.util.stream.IntStream;
import java.util.stream.Stream;

import com.lfp.joe.utils.Utils;

import reactor.core.publisher.Flux;

public class PublishersTest {

	public static void main(String[] args) {
		Stream<String> slowInput = IntStream.range(0, 100).mapToObj(i -> {
			System.out.println(Thread.currentThread().getName());
			Utils.Functions.unchecked(() -> Thread.sleep(1000));
			return "msg " + i;
		});
		Flux<String> flux = Flux.fromStream(slowInput).limitRate(1);
		Stream<String> outStream = flux.toStream();
		outStream.forEach(v -> {
			System.out.println(Thread.currentThread().getName());
			System.out.println(v);
			Utils.Functions.unchecked(() -> Thread.sleep(5_000));
		});
	}

}
