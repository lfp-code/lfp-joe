package test;

import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.process.future.Completion;
import com.lfp.joe.reactor.Fluxi;
import com.lfp.joe.reactor.support.ParallelMapOptions;

import one.util.streamex.StreamEx;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class ParallelMapTest {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final Completion CMPLETE_MARKER = Completion.of((Object) null);

	public static void main(String[] args) throws InterruptedException {
		try {
			run();
		} catch (Throwable t) {
			t.printStackTrace();
		}
		System.err.println("done");
		Thread.currentThread().join();
	}

	public static void run() throws InterruptedException {
		Function<Integer, Integer> mapper = v -> {
			LOGGER.info("starting:{} thread:{}", v, Thread.currentThread().getName());
			Throws.unchecked(() -> {
				var sleepMillis = ThreadLocalRandom.current().nextLong(1_000, 5_000);
				Thread.sleep(sleepMillis);
			});
			if (v > 20 && v < 22 && true) {
				Throws.unchecked(() -> {
					var sleepMillis = ThreadLocalRandom.current().nextLong(10_000);
					Thread.sleep(sleepMillis);
				});
			}
			if (v == 22 && false)
				throw new RuntimeException("fun");
			LOGGER.info("done:{} thread:{}", v, Thread.currentThread().getName());
			return v;
		};
		var producer = new Predicate<Consumer<? super Integer>>() {
			int index = 0;

			@Override
			public boolean test(Consumer<? super Integer> action) {
				var v = index++;
				if (v >= 100)
					return false;
				if (v == 50 && false)
					throw new RuntimeException();
				LOGGER.info("produce start:{} thread:{}", v, Thread.currentThread().getName());
				Throws.unchecked(() -> Thread.sleep(200));
				LOGGER.info("produce end:{} thread:{}", v, Thread.currentThread().getName());
				action.accept(v);
				return true;
			}
		};
		var stream = StreamEx.produce(producer);
		var mappedFlux = Fluxi.parallelMap(Fluxi.from(stream),
				ParallelMapOptions.<Integer, Integer>builder().mapper(mapper).ordered(false).build());
		mappedFlux.as(Fluxi::toStream).forEach(v -> {
			System.out.println("COMPLETE:" + v);
		});

	}

}
