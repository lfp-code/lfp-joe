package test;

import java.time.Duration;
import java.util.Arrays;

import com.lfp.joe.reactor.Fluxi;
import com.lfp.joe.utils.Utils;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class TimeoutStreamTest {

	public static void main(String[] args) throws InterruptedException {
		var flux = Flux.range(0, 20).flatMap(v -> {
			var mono = Mono.just("message: " + v);
			if (v > 5)
				mono = mono.delayElement(Duration.ofSeconds(5));
			return mono;
		});
		var stream = Fluxi.toStream(flux);
		stream = stream.onClose(() -> {
			System.out.println("closed!");
		});
		stream = stream.map(v -> {
			System.out.println("got " + v);
			return v;
		});
		stream = Utils.Lots.timeout(stream, Duration.ofSeconds(2), t -> {
			System.out.println("timed out!");
			return Arrays.asList("timeout 1", "timeout 2");
		});
		stream = stream.onClose(() -> {
			System.out.println("closed timeout!");
		});
		try (var streamFinal = stream) {
			streamFinal.forEach(v -> {
				System.out.println("done:" + v);
			});
		}
		Thread.sleep(500);
		System.err.println("done");
	}

}
