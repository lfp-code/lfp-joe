package test;

import java.time.Duration;

import com.lfp.joe.reactor.Fluxi;
import com.lfp.joe.reactor.Monos;
import com.lfp.joe.threads.Threads;

import one.util.streamex.IntStreamEx;
import reactor.core.publisher.Flux;

public class FutureTest {

	public static void main(String[] args) {
		var attempts = IntStreamEx.range(10).mapToObj(v -> v).toList();
		var outComeFlux = Flux.fromIterable(attempts).flatMap(v -> {
			var future = Threads.Pools.centralPool().submit(() -> {
				if (v == 5)
					Threads.sleepRandom(Duration.ofSeconds(5), Duration.ofSeconds(15));
				else
					Threads.sleepRandom(100, 1000);
				return v;
			});
			future.listener(() -> {
				System.out.println("future done:" + v);
			});
			return Monos.fromFuture(future);
		});
		Fluxi.toStream(outComeFlux).forEach(v -> {
			System.out.println("stream done:" + v);
		});
	}

}
