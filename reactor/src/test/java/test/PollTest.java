package test;

import java.time.Duration;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import com.lfp.joe.reactor.Monos;
import com.lfp.joe.reactor.Scheduli;
import com.lfp.joe.utils.Utils;

import reactor.core.publisher.Mono;
import reactor.retry.Repeat;

public class PollTest {

	public static void main(String[] args) throws InterruptedException {
		try {
			var index = new AtomicLong();
			var repeat = Repeat.onlyIf(v -> true).fixedBackoff(Duration.ofSeconds(1));
			Mono<Date> mono = Mono.defer(() -> {
				System.out.println("scheduling:" + Thread.currentThread().getName());
				var future = Scheduli.unlimited().schedule(() -> {
					System.out.println("starting:" + Thread.currentThread().getName());
				}, Duration.ofMillis(Utils.Crypto.getRandomInclusive(1000, 2000)).toMillis(), TimeUnit.MILLISECONDS);
				var resultFuture = future.map(nil -> {
					if (index.incrementAndGet() > 5)
						return new Date();
					return null;
				});
				return Monos.fromFuture(resultFuture);
			});
			var repeatFlux = mono.repeatWhenEmpty(repeat);
			repeatFlux.doOnNext(v -> {
				System.out.println(v);
			}).timeout(Duration.ofSeconds(30)).block();
		} catch (Throwable t) {
			t.printStackTrace();
		} finally {
			System.err.println("done");
		}
	}

}
