package test;

import java.time.Duration;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Predicate;

import com.lfp.joe.reactor.Fluxi;

import reactor.core.publisher.Flux;

public class BufferTest {

	public static void main(String[] args) {
		var indexInput = new AtomicLong(-1);
		var flux = Flux.range(0, 31 * 3).delayElements(Duration.ofMillis(100)).map(v -> {
			System.out.println("starting:" + indexInput.incrementAndGet());
			return v;
		}).map(v -> UUID.randomUUID().toString());
		Predicate<String> flushPredicate = v -> v.contains("69");
		var bufferFlux = Fluxi.buffer(flux, ops -> {
			ops.flushPredicate(flushPredicate);
			ops.maxSize(15);
			ops.maxTime(Duration.ofSeconds(5));
		});
		var stream = bufferFlux.toStream();
		var index = new AtomicLong(-1);
		stream.forEach(v -> {
			var size = v.size();
			var match = v.stream().anyMatch(flushPredicate);
			System.out.println(String.format("%s - %s - %s", index.addAndGet(size), size, match));
		});
	}

}
