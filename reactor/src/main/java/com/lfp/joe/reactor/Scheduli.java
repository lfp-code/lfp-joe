package com.lfp.joe.reactor;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.function.Function;
import java.util.function.Supplier;

import com.lfp.joe.reactor.support.ReactorSubmitterScheduler;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.threads.executor.ExecutorContainer;

public abstract class Scheduli {

	private static final Function<Supplier<Executor>, ReactorSubmitterScheduler> INSTANCE_FUNCTION = new Function<Supplier<Executor>, ReactorSubmitterScheduler>() {

		private final Map<Supplier<Executor>, ReactorSubmitterScheduler> instances = new ConcurrentHashMap<>();

		@Override
		public ReactorSubmitterScheduler apply(Supplier<Executor> s) {
			return instances.computeIfAbsent(s, nil -> createScheduler(s.get()));
		}
	};
	private static final Supplier<Executor> UNLIMITED_S = () -> Threads.Pools.centralPool();
	private static final Supplier<Executor> COMPUTATION_S = () -> Threads.Pools.computationPool();
	private static final Supplier<Executor> LOW_PRIORITY_S = () -> Threads.Pools.lowPriorityPool();

	public static ReactorSubmitterScheduler unlimited() {
		return INSTANCE_FUNCTION.apply(UNLIMITED_S);
	}

	public static ReactorSubmitterScheduler computation() {
		return INSTANCE_FUNCTION.apply(COMPUTATION_S);
	}

	public static ReactorSubmitterScheduler lowPriority() {
		return INSTANCE_FUNCTION.apply(LOW_PRIORITY_S);
	}

	public static ReactorSubmitterScheduler createScheduler(Executor executor) {
		return createScheduler(executor, Boolean.TRUE.equals(Threads.Futures.cancellationDefault().orElse(false)));
	}

	public static ReactorSubmitterScheduler createScheduler(Executor executor, boolean interruptTasksOnDispose) {
		Objects.requireNonNull(executor);
		if (executor instanceof ReactorSubmitterScheduler
				&& ((ReactorSubmitterScheduler) executor).interruptOnDispose() == interruptTasksOnDispose)
			return (ReactorSubmitterScheduler) executor;
		executor = ExecutorContainer.streamContainedExecutors(executor).filter(v -> {
			return !ReactorSubmitterScheduler.class.isInstance(v);
		}).findFirst().orElse(executor);
		return new ReactorSubmitterScheduler.Impl(executor, interruptTasksOnDispose);
	}

}
