package com.lfp.joe.reactor;

import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Stream;

import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.reactor.support.DisposableLFP;

import one.util.streamex.StreamEx;
import reactor.core.Disposable;
import reactor.core.Disposables;

public class Dispos {

	protected Dispos() {}

	// transform

	public static DisposableLFP.Composite create() {
		return from();
	}

	public static DisposableLFP.Composite from(Disposable... disposables) {
		return fromInternal(toArray(Function.identity(), disposables));
	}

	public static DisposableLFP.Composite from(Iterable<Disposable> disposables) {
		return fromInternal(toArray(Function.identity(), disposables));
	}

	public static DisposableLFP.Composite fromTask(Runnable... tasks) {
		return fromInternal(toArray(v -> toDisposable(v), tasks));
	}

	public static DisposableLFP.Composite fromTask(Iterable<Runnable> tasks) {
		return fromInternal(toArray(v -> toDisposable(v), tasks));
	}

	public static DisposableLFP.Composite fromScrapable(Scrapable... scrapables) {
		return fromInternal(toArray(v -> toDisposable(v), scrapables));
	}

	public static DisposableLFP.Composite fromScrapable(Iterable<Scrapable> scrapables) {
		return fromInternal(toArray(v -> toDisposable(v), scrapables));
	}

	// fast to arrays as these could bottleneck
	private static DisposableLFP.Composite fromInternal(Disposable[] disposables) {
		return new DisposableLFP.Composite.Impl(Disposables.composite(disposables));
	}

	private static Disposable toDisposable(Runnable runnable) {
		if (runnable == null)
			return null;
		if (runnable instanceof Disposable)
			return (Disposable) runnable;
		return () -> runnable.run();
	}

	private static Disposable toDisposable(Scrapable scrapable) {
		if (scrapable == null)
			return null;
		if (scrapable instanceof Disposable)
			return (Disposable) scrapable;
		return new Disposable() {

			@Override
			public void dispose() {
				scrapable.scrap();
			}

			@Override
			public boolean isDisposed() {
				return scrapable.isScrapped();
			}
		};
	}

	@SuppressWarnings("unchecked")
	private static <X> Disposable[] toArray(Function<X, Disposable> transform, X... inputs) {
		return toArray(transform, inputs == null ? null : Stream.of(inputs));
	}

	private static <X> Disposable[] toArray(Function<X, Disposable> transform, Iterable<X> input) {
		var iter = input == null ? null : input.iterator();
		Stream<X> stream = iter == null ? null : StreamEx.of(iter);
		return toArray(transform, stream);
	}

	private static <X> Disposable[] toArray(Function<X, Disposable> transform, Stream<X> stream) {
		Objects.requireNonNull(transform);
		if (stream == null)
			return new Disposable[0];
		return stream.map(transform).filter(Objects::nonNull).distinct().toArray(Disposable[]::new);
	}

	public static interface InterruptOnDispose {

		boolean interruptOnDispose();
	}

}
