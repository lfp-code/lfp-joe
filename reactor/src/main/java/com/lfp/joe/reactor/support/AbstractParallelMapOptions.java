package com.lfp.joe.reactor.support;

import java.util.concurrent.Executor;
import java.util.function.Function;

import org.immutables.value.Value;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.process.executor.CoreTasks;

@ValueLFP.Style
@Value.Immutable
public abstract class AbstractParallelMapOptions<X, R> {

	@Value.Parameter(order = 0)
	public abstract Function<? super X, R> mapper();

	@Value.Default
	public boolean ordered() {
		return true;
	}

	@Value.Default
	public int parallelism() {
		return MachineConfig.logicalCoreCount();
	}

	@Value.Default
	public Executor executor() {
		return CoreTasks.executor();
	}

}
