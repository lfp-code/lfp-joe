package com.lfp.joe.reactor.support;

import java.lang.reflect.Constructor;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.Semaphore;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.ConstructorRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.function.Throws;

import reactor.pool.AllocationStrategy;

public class AllocationStrategiesLFP {

	private AllocationStrategiesLFP() {}

	private static final Class<?> AllocationStrategies_CLASS_TYPE = CoreReflections
			.tryForName(AllocationStrategy.class.getPackageName() + ".AllocationStrategies").get();
	private static final Constructor<? extends AllocationStrategy> UnboundedAllocationStrategy_CONSTRUCTOR = getAllocationSrategyConstructor(
			"UnboundedAllocationStrategy");
	private static final Constructor<? extends AllocationStrategy> SizeBasedAllocationStrategy_CONSTRUCTOR = getAllocationSrategyConstructor(
			"SizeBasedAllocationStrategy", Integer.class, Integer.class);

	public static AllocationStrategy unbounded() {
		return Throws.unchecked(() -> UnboundedAllocationStrategy_CONSTRUCTOR.newInstance());
	}

	public static AllocationStrategy sizeBased(int min, int max) {
		return Throws.unchecked(() -> SizeBasedAllocationStrategy_CONSTRUCTOR.newInstance(min, max));
	}

	public static AllocationStrategy semaphore(AllocationStrategy allocationStrategy, Semaphore semaphore) {
		Objects.requireNonNull(allocationStrategy);
		Objects.requireNonNull(semaphore);
		return new DelegatingAllocationStrategy(allocationStrategy) {

			@Override
			public int getPermits(int desired) {
				var permits = super.getPermits(desired);
				if (permits == 0)
					return permits;
				var acquired = 0;
				while (acquired < permits && semaphore.tryAcquire())
					acquired++;
				var returned = permits - acquired;
				if (returned > 0)
					super.returnPermits(returned);
				return acquired;
			}

			@Override
			public void returnPermits(int returned) {
				semaphore.release(returned);
				super.returnPermits(returned);
			}
		};
	}

	@SuppressWarnings("unchecked")
	private static Constructor<? extends AllocationStrategy> getAllocationSrategyConstructor(String className,
			Class<?>... parameterTypes) {
		for (var ct : AllocationStrategies_CLASS_TYPE.getDeclaredClasses()) {
			if (!ct.getSimpleName().equals(className))
				continue;
			var ctor = MemberCache.tryGetConstructor(ConstructorRequest.of(ct, parameterTypes), false).orElse(null);
			if (ctor == null)
				continue;
			return (Constructor<? extends AllocationStrategy>) ctor;
		}
		throw new RuntimeException(
				new NoSuchMethodException(String.format("%s - %s", className, Arrays.toString(parameterTypes))));
	}

	private static class DelegatingAllocationStrategy implements AllocationStrategy {

		private final AllocationStrategy allocationStrategy;

		public DelegatingAllocationStrategy(AllocationStrategy allocationStrategy) {
			super();
			this.allocationStrategy = Objects.requireNonNull(allocationStrategy);
		}

		/**
		 * @return
		 * @see reactor.pool.AllocationStrategy#estimatePermitCount()
		 */
		@Override
		public int estimatePermitCount() {
			return allocationStrategy.estimatePermitCount();
		}

		/**
		 * @param desired
		 * @return
		 * @see reactor.pool.AllocationStrategy#getPermits(int)
		 */
		@Override
		public int getPermits(int desired) {
			return allocationStrategy.getPermits(desired);
		}

		/**
		 * @return
		 * @see reactor.pool.AllocationStrategy#permitGranted()
		 */
		@Override
		public int permitGranted() {
			return allocationStrategy.permitGranted();
		}

		/**
		 * @return
		 * @see reactor.pool.AllocationStrategy#permitMinimum()
		 */
		@Override
		public int permitMinimum() {
			return allocationStrategy.permitMinimum();
		}

		/**
		 * @return
		 * @see reactor.pool.AllocationStrategy#permitMaximum()
		 */
		@Override
		public int permitMaximum() {
			return allocationStrategy.permitMaximum();
		}

		/**
		 * @param returned
		 * @see reactor.pool.AllocationStrategy#returnPermits(int)
		 */
		@Override
		public void returnPermits(int returned) {
			allocationStrategy.returnPermits(returned);
		}
	}

	public static void main(String[] args) {
		System.out.println(AllocationStrategiesLFP.sizeBased(4, 5));
	}
}
