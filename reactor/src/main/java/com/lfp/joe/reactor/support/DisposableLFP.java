package com.lfp.joe.reactor.support;

import java.util.Collection;
import java.util.Objects;

import reactor.core.Disposable;
import reactor.core.Disposables;

public interface DisposableLFP extends Disposable {

	boolean tryDispose();

	public static class Impl implements DisposableLFP {

		private final Disposable delegate;

		public Impl(Disposable delegate) {
			super();
			this.delegate = Objects.requireNonNull(delegate);
		}

		@Override
		public boolean tryDispose() {
			if (!this.isDisposed())
				synchronized (this) {
					if (!this.isDisposed()) {
						getDelegate().dispose();
						return true;
					}
				}
			return false;
		}

		@Override
		public void dispose() {
			tryDispose();
		}

		@Override
		public boolean isDisposed() {
			return getDelegate().isDisposed();
		}

		protected Disposable getDelegate() {
			return this.delegate;
		}

	}

	static interface Composite extends DisposableLFP, Disposable.Composite {

		public static class Impl extends DisposableLFP.Impl implements DisposableLFP.Composite {

			public Impl() {
				this(Disposables.composite());
			}

			public Impl(Disposable.Composite delegate) {
				super(delegate);
			}

			protected Disposable.Composite getDelegate() {
				return (Disposable.Composite) super.getDelegate();
			}

			@Override
			public boolean add(Disposable d) {
				return getDelegate().add(d);
			}

			@Override
			public boolean addAll(Collection<? extends Disposable> ds) {
				return getDelegate().addAll(ds);
			}

			@Override
			public boolean remove(Disposable d) {
				return getDelegate().remove(d);
			}

			@Override
			public int size() {
				return getDelegate().size();
			}
		}

	}

	static interface Swap extends DisposableLFP, Disposable.Swap {

		public static class Impl extends DisposableLFP.Impl implements DisposableLFP.Swap {

			public Impl() {
				this(Disposables.swap());
			}
			
			public Impl(Disposable.Swap delegate) {
				super(delegate);
			}

			protected Disposable.Swap getDelegate() {
				return (Disposable.Swap) super.getDelegate();
			}

			@Override
			public boolean update(Disposable next) {
				return getDelegate().update(next);
			}

			@Override
			public boolean replace(Disposable next) {
				return getDelegate().replace(next);
			}

			@Override
			public Disposable get() {
				return getDelegate().get();
			}
		}
	}
}
