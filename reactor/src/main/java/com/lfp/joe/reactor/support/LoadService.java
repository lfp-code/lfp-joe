package com.lfp.joe.reactor.support;

import java.util.Map.Entry;
import java.util.concurrent.Executor;

import org.reactivestreams.Publisher;

import com.lfp.joe.threads.Threads;

import reactor.core.publisher.Flux;

public interface LoadService<K, V> {

	default Flux<Entry<K, V>> load(Executor loadExecutor, Publisher<K> keyPublisher) {
		return load(Threads.Pools.centralPool(), loadExecutor, keyPublisher);
	}

	Flux<Entry<K, V>> load(Executor readExecutor, Executor loadExecutor, Publisher<K> keyPublisher);

}
