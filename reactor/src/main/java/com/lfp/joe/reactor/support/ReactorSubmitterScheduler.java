package com.lfp.joe.reactor.support;

import java.time.Duration;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Stream;

import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.ListenableFutureTask;
import org.threadly.concurrent.wrapper.compatibility.ListenableScheduledFuture;

import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.reactor.Scheduli;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.threads.executor.ExecutorServices.ShutdownUnsupported;
import com.lfp.joe.threads.executor.SubmitterSchedulerLFP;
import com.lfp.joe.threads.future.ListenableFutureProxy;

import reactor.core.Disposable;
import reactor.core.Exceptions;
import reactor.core.scheduler.Scheduler;

public interface ReactorSubmitterScheduler extends SubmitterSchedulerLFP, Scheduler {

	default boolean interruptOnDispose() {
		return Boolean.TRUE.equals(Threads.Futures.cancellationDefault().orElse(null));
	}

	@Override
	default DisposableListenableFuture schedule(Runnable task) {
		return ReactorSubmitterScheduler.Impl
				.asDisposableListenableFuture(() -> SubmitterSchedulerLFP.super.submit(task), interruptOnDispose());
	}

	@Override
	default DisposableListenableFuture.Scheduled schedule(Runnable task, long delay, TimeUnit unit) {
		return (DisposableListenableFuture.Scheduled) ReactorSubmitterScheduler.Impl.asDisposableListenableFuture(
				() -> SubmitterSchedulerLFP.super.schedule(task, delay, unit), interruptOnDispose());
	}

	@Override
	default DisposableListenableFuture.Scheduled schedulePeriodically(Runnable task, long initialDelay, long period,
			TimeUnit unit) {
		return (DisposableListenableFuture.Scheduled) ReactorSubmitterScheduler.Impl.asDisposableListenableFuture(
				() -> SubmitterSchedulerLFP.super.scheduleAtFixedRate(task, initialDelay, period, unit),
				interruptOnDispose());
	}

	@Override
	default Worker createWorker() {
		return new Impl.ExecutorWorker(getContainedExecutor(), interruptOnDispose());
	}

	@Override
	default boolean isDisposed() {
		var executor = getContainedExecutor();
		if (executor instanceof Disposable)
			return ((Disposable) executor).isDisposed();
		if (executor instanceof ExecutorService)
			return ((ExecutorService) executor).isShutdown();
		return false;
	}

	@Override
	default void dispose() {
		var executor = getContainedExecutor();
		if (executor instanceof Disposable) {
			((Disposable) executor).dispose();
			return;
		}
		if (executor instanceof ExecutorService && !(executor instanceof ShutdownUnsupported)) {
			((ExecutorService) executor).shutdownNow();
			return;
		}

	}

	public static interface DisposableListenableFuture extends Disposable, ListenableFuture<Object> {

		@Override
		default boolean isDisposed() {
			return this.isDone();
		}

		public static interface CancelOnDispose extends DisposableListenableFuture {

			@Override
			default void dispose() {
				this.cancel(false);
			}
		}

		public static interface CancelInterruptOnDispose extends DisposableListenableFuture {

			@Override
			default void dispose() {
				this.cancel(true);
			}
		}

		public static interface Scheduled extends DisposableListenableFuture, ListenableScheduledFuture<Object> {}

	}

	public static class Impl implements ReactorSubmitterScheduler {

		private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
		private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

		private final Executor executor;
		private final boolean interruptTasksOnDispose;

		public Impl(Executor executor, boolean interruptTasksOnDispose) {
			this.executor = Objects.requireNonNull(executor);
			this.interruptTasksOnDispose = interruptTasksOnDispose;
		}

		@Override
		public Executor getContainedExecutor() {
			return executor;
		}

		@Override
		public boolean interruptOnDispose() {
			return interruptTasksOnDispose;
		}

		protected static <U> DisposableListenableFuture asDisposableListenableFuture(
				ThrowingSupplier<Future<?>, ?> supplier, boolean interruptOnDispose) {
			try {
				var future = supplier.get();
				return createDisposableFuture(future, interruptOnDispose);
			} catch (Throwable ex) {
				ReactorSubmitterScheduler.Impl.LOGGER.warn("{} schedule error", Scheduler.Worker.class.getSimpleName(),
						ex);
				throw Exceptions.failWithRejected(ex);
			}
		}

		protected static DisposableListenableFuture createDisposableFuture(Future<?> future,
				boolean interruptOnDispose) {
			var ifaces = Stream.<Class<?>>builder();
			if (interruptOnDispose)
				ifaces.add(DisposableListenableFuture.CancelInterruptOnDispose.class);
			else
				ifaces.add(DisposableListenableFuture.CancelOnDispose.class);
			if (future instanceof ScheduledFuture)
				ifaces.add(DisposableListenableFuture.Scheduled.class);
			var lfuture = ListenableFutureProxy.from(future, List.of(), ifaces.build().toArray(Class<?>[]::new));
			return (DisposableListenableFuture) lfuture;
		}

		public static class ExecutorWorker extends DisposableLFP.Composite.Impl implements Scheduler.Worker {

			private final ReentrantReadWriteLock disposeLock = new ReentrantReadWriteLock();
			private final SubmitterSchedulerLFP executor;
			private boolean interruptOnDispose;

			public ExecutorWorker(Executor executor, boolean interruptOnDispose) {
				this.executor = SubmitterSchedulerLFP.from(executor);
				this.interruptOnDispose = interruptOnDispose;
			}

			@Override
			public Disposable schedule(Runnable task) {
				Objects.requireNonNull(task, "task");
				var lfutureTask = new ListenableFutureTask<>(task);
				var disposable = new Disposable() {

					@Override
					public void dispose() {
						lfutureTask.cancel(interruptOnDispose);
					}

					@Override
					public boolean isDisposed() {
						return lfutureTask.isDone();
					}
				};
				failWithRejectedIfNotAdded(disposable);
				lfutureTask.listener(() -> super.remove(disposable));
				try {
					executor.execute(lfutureTask);
				} catch (Throwable ex) {
					super.remove(disposable);
					LOGGER.warn("{} execute error", Scheduler.Worker.class.getSimpleName(), ex);
					throw Exceptions.failWithRejected(ex);
				}
				return disposable;
			}

			@Override
			public DisposableListenableFuture.Scheduled schedule(Runnable task, long delay, TimeUnit unit) {
				Objects.requireNonNull(task, "task");
				failWithRejectedIfDisposed();
				disposeLock.readLock().lock();
				try {
					failWithRejectedIfDisposed();
					var future = (DisposableListenableFuture.Scheduled) asDisposableListenableFuture(
							() -> executor.schedule(task, delay, unit), interruptOnDispose);
					return failWithRejectedIfNotAdded(future);
				} finally {
					disposeLock.readLock().unlock();
				}
			}

			@Override
			public DisposableListenableFuture.Scheduled schedulePeriodically(Runnable task, long initialDelay,
					long period, TimeUnit unit) {
				Objects.requireNonNull(task, "task");
				failWithRejectedIfDisposed();
				disposeLock.readLock().lock();
				try {
					failWithRejectedIfDisposed();
					var future = (DisposableListenableFuture.Scheduled) asDisposableListenableFuture(
							() -> executor.scheduleAtFixedRate(task, initialDelay, period, unit), interruptOnDispose);
					return failWithRejectedIfNotAdded(future);
				} finally {
					disposeLock.readLock().unlock();
				}

			}

			@Override
			public void dispose() {
				if (isDisposed())
					return;
				disposeLock.writeLock().lock();
				try {
					if (!isDisposed())
						super.dispose();
				} finally {
					disposeLock.writeLock().unlock();
				}
			}

			private void failWithRejectedIfDisposed() {
				if (super.isDisposed())
					throw Exceptions.failWithRejected();
			}

			private <U extends Disposable> U failWithRejectedIfNotAdded(U disposable) {
				if (!super.add(disposable))
					throw Exceptions.failWithRejected();
				return disposable;
			}
		}
	}

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		Scheduli.unlimited().submit(() -> {
			System.out.println("heeeeyy");
		}).get();
		var f = Scheduli.unlimited().schedule(new FutureTask<Object>(() -> {
			try {
				while (!Thread.currentThread().isInterrupted()) {
					Thread.sleep(1000);
					System.out.println("awaiting interrupt - " + Thread.currentThread().getName());
				}
			} catch (Throwable t) {
				System.err.println(t.getMessage());
			} finally {
				System.out.println("done");
			}
			return null;
		}));
		((ListenableFuture<?>) f).failureCallback(t -> System.err.println(t.getMessage()));
		Scheduli.unlimited().submitScheduled(() -> f.dispose(), 3_000);
		var future = Scheduli.unlimited().schedulePeriodically(() -> {
			System.out.println("hi there - " + Thread.currentThread().getName());
		}, 1, 1, TimeUnit.SECONDS);
		Threads.sleep(Duration.ofSeconds(5));
		System.out.println(future.isDisposed());
		future.dispose();
		System.out.println(future.isDone());
		System.out.println(future.isDisposed());
		System.out.println(future.isCompletedExceptionally());
		Scheduli.unlimited().dispose();
		System.out.println(Scheduli.unlimited().isDisposed());
	}
}
