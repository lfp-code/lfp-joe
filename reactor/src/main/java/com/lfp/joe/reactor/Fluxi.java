package com.lfp.joe.reactor;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.Future;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.immutables.value.Value;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.Durations;
import com.lfp.joe.core.function.ImmutableEntry;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.log.Logging;
import com.lfp.joe.core.process.future.CSFutureTask;
import com.lfp.joe.reactor.support.ParallelMapOptions;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.core.function.Muto.MutoBoolean;

import ch.qos.logback.classic.Level;
import one.util.streamex.StreamEx;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

@ValueLFP.Style
@Value.Enclosing
public abstract class Fluxi {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final Logger logger = LoggerFactory.getLogger(THIS_CLASS);
	private static final boolean LOG_ON_DONE = MachineConfig.isDeveloper() && false;
	private static final String DEFAULT_LOG_MESSAGE_ERROR = "error in flux";
	private static final boolean DO_ON_DONE_COMPLETE = false;
	private static final boolean DO_ON_DONE_AFTER_TERMINATE = false;
	private static final boolean DO_ON_DONE_FINALLY = false;

	public static <X> Flux<X> subscribeOn(Publisher<X> publisher, Scheduler... schedulerCandidates) {
		return onExecutor(publisher, (sched, flux) -> flux.subscribeOn(sched), schedulerCandidates);
	}

	public static <X> Flux<X> publishOn(Publisher<X> publisher, Scheduler... schedulerCandidates) {
		return onExecutor(publisher, (sched, flux) -> flux.publishOn(sched), schedulerCandidates);
	}

	private static <X> Flux<X> onExecutor(Publisher<X> publisher, BiFunction<Scheduler, Flux<X>, Flux<X>> applyConsumer,
			Scheduler... schedulerCandidates) {
		Flux<X> flux;
		if (publisher == null)
			flux = Flux.empty();
		else
			flux = Flux.from(publisher);
		if (schedulerCandidates != null)
			for (Scheduler scheduler : schedulerCandidates) {
				if (scheduler == null)
					continue;
				var resultFlux = applyConsumer.apply(scheduler, flux);
				if (resultFlux != null)
					return resultFlux;
			}
		return flux;
	}

	/*
	 * from lots
	 */

	public static <X> Flux<X> from(Publisher<X> publisher) {
		return from(publisher, null);
	}

	public static <X> Flux<X> from(Publisher<X> publisher, Scheduler subscribeOn) {
		return fromInternal(publisher, subscribeOn);
	}

	public static <X> Flux<X> from(StreamEx<X> stream) {
		return from(stream, null);
	}

	public static <X> Flux<X> from(StreamEx<X> stream, Scheduler subscribeOn) {
		return fromInternal(stream, subscribeOn);
	}

	public static <X> Flux<X> from(Stream<X> stream) {
		return from(stream, null);
	}

	public static <X> Flux<X> from(Stream<X> stream, Scheduler subscribeOn) {
		return fromInternal(stream, subscribeOn);

	}

	public static <X> Flux<X> from(Iterable<X> iterable) {
		return from(iterable, null);
	}

	public static <X> Flux<X> from(Iterable<X> iterable, Scheduler subscribeOn) {
		return fromInternal(iterable, subscribeOn);
	}

	public static <X> Flux<X> from(Iterator<X> iterator) {
		return from(iterator, null);
	}

	public static <X> Flux<X> from(Iterator<X> iterator, Scheduler subscribeOn) {
		return fromInternal(iterator, subscribeOn);
	}

	@SuppressWarnings("unchecked")
	protected static <X> Flux<X> fromInternal(Object input, Scheduler subscribeOn) {
		if (input == null)
			return subscribeOn(Flux.empty(), subscribeOn);
		boolean tryClose;
		Flux<X> flux;
		// publisher -> flux
		if (input instanceof Publisher) {
			tryClose = false;
			flux = Flux.from((Publisher<? extends X>) input);
		} else {
			Function<Stream<X>, Stream<X>> streamNormalizer = s -> s.filter(Objects::nonNull);
			// stream -> flux
			if (input instanceof Stream) {
				tryClose = false;
				var stream = streamNormalizer.apply((Stream<X>) input);
				flux = Flux.fromStream(stream);
			} else {
				// (spliterator || iterator) -> spliterator -> stream -> flux
				tryClose = true;
				if (input instanceof Spliterator) {
					var stream = streamNormalizer.apply(StreamSupport.stream((Spliterator<X>) input, false));
					flux = Flux.fromStream(stream);
				} else if (input instanceof Iterator) {
					var spliterator = Spliterators.spliterator(((Iterator<X>) input), Long.MAX_VALUE, 0);
					var stream = streamNormalizer.apply(StreamSupport.stream(spliterator, false));
					flux = Flux.fromStream(stream);
				} else {
					// (collection || iterable || array) -> iterable -> flux
					Function<Stream<X>, Iterator<X>> streamToIterator = streamNormalizer.andThen(Stream::iterator);
					if (input instanceof Collection) {
						Iterable<X> iterable = () -> streamToIterator.apply(((Collection<X>) input).stream());
						flux = Flux.fromIterable(iterable);
					} else if (input instanceof Iterable) {
						Iterable<X> iterable = () -> streamToIterator
								.apply(StreamSupport.stream(((Iterable<X>) input).spliterator(), false));
						flux = Flux.fromIterable(iterable);
					} else if (input.getClass().isArray()) {
						Iterable<X> iterable = () -> streamToIterator.apply(Stream.of((X[]) input));
						flux = Flux.fromIterable(iterable);
					} else
						throw new UnsupportedOperationException("invalid flux input:" + input);
				}
			}
		}
		if (tryClose && input instanceof AutoCloseable)
			flux = flux.doOnTerminate(Throws.runnableUnchecked(() -> ((AutoCloseable) input).close()));
		return subscribeOn(flux, subscribeOn);

	}

	public static <X> Flux<X> fromFutures(Iterable<? extends Future<X>> ible) {
		return fromFutures(ible, null);
	}

	public static <X> Flux<X> fromFutures(Iterable<? extends Future<X>> ible, Scheduler subscribeOn) {
		var futureFlux = Fluxi.from(Streams.of(ible).nonNull().distinct(), subscribeOn);
		return futureFlux.flatMap(Monos::fromFuture);
	}

	public static <X> StreamEx<X> toStream(Publisher<X> publisher) {
		return toStream(publisher, 1);
	}

	public static <X> StreamEx<X> toStream(Publisher<X> publisher, Integer batchSize) {
		if (publisher == null)
			return StreamEx.empty();
		var flux = Flux.from(publisher);
		var stream = Optional.ofNullable(batchSize).map(v -> flux.toStream(v)).orElseGet(flux::toStream);
		return Streams.of(stream);
	}

	public static <X> Flux<X> timeout(Publisher<X> publisher, Duration firstTimeout,
			Function<X, Duration> nextTimeoutFunction, Publisher<? extends X> fallback) {
		var flux = Fluxi.from(publisher);
		var first = Mono.just(0L);
		if (firstTimeout != null)
			first = first.delayElement(firstTimeout);
		flux = flux.timeout(first, v -> {
			var next = Mono.just(0L);
			Duration nextTimeout = nextTimeoutFunction == null ? null : nextTimeoutFunction.apply(v);
			if (nextTimeout != null)
				next = next.delayElement(nextTimeout);
			return next;
		}, fallback);
		return flux;
	}

	public static <X> Flux<List<X>> buffer(Publisher<? extends X> publisher, Number maxSize) {
		return buffer(publisher, maxSize, null);
	}

	public static <X> Flux<List<X>> buffer(Publisher<? extends X> publisher, Duration maxTime) {
		return buffer(publisher, null, maxTime);
	}

	public static <X> Flux<List<X>> buffer(Publisher<? extends X> publisher, Number maxSize, Duration maxTime) {
		if (publisher == null)
			return Flux.empty();
		var flux = Flux.<X>from(publisher);
		maxSize = Optional.ofNullable(maxSize).map(Number::intValue).filter(v -> v != -1).orElse(null);
		maxTime = Optional.ofNullable(maxTime).filter(Predicate.not(Durations::isMax)).orElse(null);
		if (maxSize == null && maxTime == null)
			return flux.collectList().flux();
		if (maxTime == null)
			return flux.buffer(maxSize.intValue());
		if (maxSize == null)
			return flux.buffer(maxTime);
		return flux.bufferTimeout(maxSize.intValue(), maxTime);

	}

	public static <X, R> Flux<R> parallelMap(Publisher<X> publisher, Function<X, R> mapper) {
		return parallelMap(publisher, ParallelMapOptions.of(mapper));
	}

	public static <X, R> Flux<R> parallelMap(Publisher<X> publisher, ParallelMapOptions<X, R> options) {
		Objects.requireNonNull(options);
		if (publisher == null)
			return Flux.empty();
		Function<? super X, ? extends Publisher<? extends R>> mapper = v -> {
			return Monos.fromFuture(() -> {
				var futureTask = new CSFutureTask<>(() -> options.mapper().apply(v));
				options.executor().execute(futureTask);
				return futureTask;
			});
		};
		var flux = Flux.from(publisher);
		if (options.ordered())
			return flux.flatMapSequential(mapper, options.parallelism());
		return flux.flatMap(mapper, options.parallelism());
	}

	public static <X> Flux<X> parallelFilter(Publisher<X> publisher, Predicate<X> filter) {
		Function<X, Entry<X, Boolean>> mapper;
		if (filter == null)
			mapper = null;
		else
			mapper = v -> ImmutableEntry.of(v, filter.test(v));
		return parallelFilter(publisher, ParallelMapOptions.of(mapper));
	}

	public static <X> Flux<X> parallelFilter(Publisher<X> publisher, ParallelMapOptions<X, Entry<X, Boolean>> options) {
		return parallelMap(publisher, options)
				.as(flux -> flux.filter(ent -> Boolean.TRUE.equals(ent.getValue())).map(Entry::getKey));
	}

	public static <X> Flux<X> doOnDone(Publisher<X> publisher, Runnable task) {
		Flux<X> flux = Fluxi.from(publisher);
		if (task == null)
			return flux;
		var taskDisposable = Dispos.fromTask(task);
		flux = flux.doOnCancel(() -> {
			if (taskDisposable.tryDispose() && LOG_ON_DONE)
				logger.info("publisher done:{} task:{}", "doOnCancel", task);
		});
		flux = flux.doOnTerminate(() -> {
			if (taskDisposable.tryDispose() && LOG_ON_DONE)
				logger.info("publisher done:{} task:{}", "doOnTerminate", task);
		});
		// should be handled by terminate
		if (DO_ON_DONE_COMPLETE)
			flux = flux.doOnComplete(() -> {
				if (taskDisposable.tryDispose() && LOG_ON_DONE)
					logger.info("publisher done:{} task:{}", "doOnComplete", task);
			});
		// i think this will keep a listener in memory
		if (DO_ON_DONE_AFTER_TERMINATE)
			flux = flux.doAfterTerminate(() -> {
				if (taskDisposable.tryDispose() && LOG_ON_DONE)
					logger.info("publisher done:{} task:{}", "doAfterTerminate", task);
			});
		// i think this will keep a listener in memory
		if (DO_ON_DONE_FINALLY)
			flux = flux.doFinally(nil -> {
				if (taskDisposable.tryDispose() && LOG_ON_DONE)
					logger.info("publisher done:{} task:{}", "doFinally", task);
			});
		return flux;
	}

	public static <X> FluxSink<X> doOnDone(FluxSink<X> fluxSink, Runnable task) {
		Objects.requireNonNull(fluxSink);
		if (task == null)
			return fluxSink;
		var taskDisposable = Dispos.fromTask(task);
		fluxSink = fluxSink.onDispose(() -> {
			if (taskDisposable.tryDispose() && LOG_ON_DONE)
				logger.info("fluxSink done:{} task:{}", "onDispose", task);
		});
		fluxSink = fluxSink.onCancel(() -> {
			if (taskDisposable.tryDispose() && LOG_ON_DONE)
				logger.info("fluxSink done:{} task:{}", "onCancel", task);
		});
		return fluxSink;
	}

	public static <X> void linkTermination(Future<?> future, FluxSink<X> sink, Disposable... disposables) {
		var taskDisposable = Dispos.from(disposables);
		Threads.Futures.callback(future, (v, t) -> {
			if (t != null)
				sink.error(t);
			else
				sink.complete();
			taskDisposable.dispose();
		});
		sink.onCancel(() -> {
			future.cancel(true);
		});
		sink.onDispose(() -> {
			future.cancel(true);
		});
	}

	public static <X> void link(Publisher<X> publisher, FluxSink<X> sink, Disposable... disposables) {
		var flux = Fluxi.from(publisher);
		var subscription = flux.subscribe(sink::next, sink::error, sink::complete, sink.currentContext());
		var disposableStream = Streams.of(subscription).append(Streams.of(disposables));
		var disposable = Dispos.from(disposableStream);
		doOnDone(sink, disposable::dispose);
	}

	// logError: publisher

	public static <X> Flux<X> logError(Publisher<X> publisher) {
		return logError(publisher, (String) null);
	}

	// logError: publisher + args

	public static <X> Flux<X> logError(Publisher<X> publisher, String message, Object... arguments) {
		return logError(publisher, false, message, arguments);
	}

	// logError: publisher, suppressOnCancel

	public static <X> Flux<X> logError(Publisher<X> publisher, boolean suppressOnCancel) {
		return logError(publisher, false, (String) null);
	}

	// logError: publisher, suppressOnCancel + args

	public static <X> Flux<X> logError(Publisher<X> publisher, boolean suppressOnCancel, String message,
			Object... arguments) {
		return logError(null, publisher, suppressOnCancel, message, arguments);
	}

	// logError: logger, publisher, suppressOnCancel

	public static <X> Flux<X> logError(Logger logger, Publisher<X> publisher, boolean suppressOnCancel) {
		return logError(logger, null, publisher, suppressOnCancel, (String) null);
	}

	// logError: logger, publisher, suppressOnCancel + args

	public static <X> Flux<X> logError(Logger logger, Publisher<X> publisher, boolean suppressOnCancel, String message,
			Object... arguments) {
		return logError(logger, null, publisher, suppressOnCancel, message, arguments);
	}

	// logError: logger, level, publisher, suppressOnCancel

	public static <X> Flux<X> logError(Logger logger, Level level, Publisher<X> publisher, boolean suppressOnCancel) {
		return logError(logger, level, publisher, suppressOnCancel, (String) null);
	}

	// logError: logger, level, publisher, suppressOnCancel + args

	public static <X> Flux<X> logError(Logger logger, Level level, Publisher<X> publisher, boolean suppressOnCancel,
			String message, Object... arguments) {
		if (logger == null) {
			var callingClass = CoreReflections.getCallingClass(Fluxi.class);
			if (callingClass != null)
				logger = LoggerFactory.getLogger(callingClass);
			else
				logger = Fluxi.logger;
		}
		if (level == null)
			level = Level.ERROR;
		Flux<X> flux = Fluxi.from(publisher);
		MutoBoolean fluxCanceled;
		if (!suppressOnCancel)
			fluxCanceled = null;
		else {
			fluxCanceled = MutoBoolean.create();
			flux = flux.doOnCancel(() -> fluxCanceled.accept(true));
		}
		var logConsumer = Logging.levelConsumer(logger, level);
		return flux.doOnError(t -> {
			if (fluxCanceled != null && fluxCanceled.isTrue())
				return;
			var argStream = arguments == null ? Stream.empty() : Stream.of(arguments);
			argStream = Stream.concat(argStream, Stream.of(t));
			argStream = argStream.filter(Predicate.not(Objects::isNull));
			logConsumer.accept(Optional.ofNullable(message).orElse(DEFAULT_LOG_MESSAGE_ERROR), argStream.toArray());
		});
	}

	public static <X> FluxSink<X> onScrapComplete(Scrapable scrapable, FluxSink<X> fs) {
		return doOnScrap(scrapable, fs, () -> {
			fs.complete();
		});
	}

	public static <X> FluxSink<X> onScrapError(Scrapable scrapable, FluxSink<X> fs, Supplier<Throwable> errorSupplier) {
		Objects.requireNonNull(errorSupplier);
		return doOnScrap(scrapable, fs, () -> {
			var error = errorSupplier.get();
			fs.error(error);
		});
	}

	public static <X> FluxSink<X> doOnScrap(Scrapable scrapable, FluxSink<X> fs, Runnable task) {
		Objects.requireNonNull(scrapable);
		Objects.requireNonNull(fs);
		if (task == null)
			return fs;
		var listener = scrapable.onScrap(task);
		return Fluxi.doOnDone(fs, listener::scrap);
	}

	public static <T> Flux<T> onAssembly(Flux<T> source) {
		return FluxAccess.onAssembly(source);
	}

	private static abstract class FluxAccess extends Flux<Object> {

		public static <T> Flux<T> onAssembly(Flux<T> source) {
			return Flux.onAssembly(source);
		}
	}

	public static void main(String[] args) {
		var list = Arrays.asList(new Date(), null, new Date());

		Fluxi.fromInternal(new Object[] { new Date(), null, new Date() }, null).subscribe(System.out::println);
	}

}
