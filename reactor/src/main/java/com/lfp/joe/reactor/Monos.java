package com.lfp.joe.reactor;

import java.time.Duration;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.reactivestreams.Publisher;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.SettableListenableFuture;

import com.lfp.joe.core.process.future.Completion;
import com.lfp.joe.threads.Threads;

import reactor.core.publisher.Mono;
import reactor.core.publisher.MonoSink;
import reactor.core.publisher.SignalType;

public class Monos {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	@SuppressWarnings("unchecked")
	public static <X> Mono<X> fromFuture(Future<? extends X> future) {
		final Mono<X> futureMono;
		if (future == null)
			futureMono = Mono.empty();
		else if (future.isDone()) {
			futureMono = Completion.getNow(future).map(Mono::justOrEmpty, Mono::error);
		} else {
			Consumer<SignalType> onFinally = signalType -> {
				if (SignalType.CANCEL.equals(signalType))
					future.cancel(true);
			};
			if (future instanceof CompletableFuture) {
				// ONLY do CompletableFuture NOT CompletionStage
				// Mono.fromFuture requires cancel exceptions to be immediate
				futureMono = Mono.fromFuture((CompletableFuture<X>) future).doFinally(onFinally);
			} else {
				var futureSinkConsumer = new Consumer<MonoSink<X>>() {

					@Override
					public void accept(MonoSink<X> sink) {
						if (future.isDone()) {
							var completion = Completion.join(future);
							complete(completion.result(), completion.failure(), sink);
						} else {
							sink = sink.onCancel(() -> {
								future.cancel(true);
							});
							sink = sink.onDispose(() -> {
								future.cancel(true);
							});
							callback(future, sink);
						}
					}

					private void callback(Future<? extends X> future, MonoSink<X> sink) {
						Threads.Futures.callback(future, (v, t) -> complete(v, t, sink));
					}

					private void complete(X result, Throwable failure, MonoSink<X> sink) {
						if (failure != null)
							sink.error(failure);
						else if (result != null)
							sink.success(result);
						else
							sink.success();
					}
				};
				futureMono = Mono.<X>create(futureSinkConsumer).doFinally(onFinally);
			}
		}
		return onAssembly(futureMono);

	}

	public static <X> Mono<X> fromFuture(Supplier<? extends Future<? extends X>> futureSupplier) {
		Objects.requireNonNull(futureSupplier);
		return Mono.defer(() -> fromFuture(futureSupplier.get()));
	}

	public static <X> ListenableFuture<X> toFuture(Mono<X> mono) {
		var sfuture = new SettableListenableFuture<X>(false);
		var subscription = mono.subscribe(v -> {
			sfuture.setResult(v);
		}, t -> {
			boolean failure = sfuture.setFailure(t);
			if (failure)
				t.printStackTrace();
		}, () -> {
			sfuture.setResult(null);
		});
		sfuture.listener(() -> {
			subscription.dispose();
		});
		return sfuture;
	}

	public static <X> Mono<X> doOnDone(Publisher<X> publisher, Runnable task) {
		var mono = Optional.ofNullable(publisher).map(Mono::from).orElseGet(Mono::empty);
		if (task == null)
			return mono;
		mono = mono.doOnTerminate(task);
		return mono;
	}

	public static <X> void linkTermination(Future<?> future, MonoSink<X> sink) {
		Threads.Futures.callback(future, (v, t) -> {
			if (t != null)
				sink.error(t);
			else
				sink.success();
		});
		linkTerminationSink(future, sink);
	}

	public static <X> void linkCompletion(Future<? extends X> future, MonoSink<X> sink) {
		Threads.Futures.callback(future, (v, t) -> {
			if (t != null)
				sink.error(t);
			else if (v != null)
				sink.success(v);
			else
				sink.success();
		});
		linkTerminationSink(future, sink);
	}

	private static <X> void linkTerminationSink(Future<?> future, MonoSink<X> sink) {
		sink.onCancel(() -> future.cancel(true));
		sink.onDispose(() -> future.cancel(true));
	}

	protected static <T> Mono<T> onAssembly(Mono<T> source) {
		return MonoAccess.onAssembly(source);
	}

	private static abstract class MonoAccess extends Mono<Object> {

		public static <T> Mono<T> onAssembly(Mono<T> source) {
			return Mono.onAssembly(source);
		}
	}

	public static void main(String[] args) throws InterruptedException {
		var future = Threads.Pools.centralPool().submit(() -> {
			Thread.sleep(5_000);
			return new Date();
		});
		var mono = Monos.fromFuture(future);
		var disposable = mono.subscribeOn(Scheduli.unlimited()).subscribe(v -> {
			System.out.println(v);
		});
		if (true)
			Threads.Pools.centralPool().submitScheduled(disposable::dispose, Duration.ofSeconds(2).toMillis());
		else
			Threads.Pools.centralPool().submitScheduled(() -> future.cancel(true), Duration.ofSeconds(2).toMillis());
		Thread.currentThread().join();
	}
}
