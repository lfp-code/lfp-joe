package com.lfp.joe.reactor.support;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.function.Function;
import java.util.function.Predicate;

import org.reactivestreams.Publisher;

import com.lfp.joe.core.function.Asserts;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.reactor.Fluxi;
import com.lfp.joe.reactor.Scheduli;
import com.lfp.joe.threads.executor.SubmitterSchedulerLFP;
import com.lfp.joe.utils.Utils;

import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;
import reactor.util.function.Tuples;

public abstract class AbstractLoadService<G, K, V> implements LoadService<K, V> {

	private final Predicate<KeyBuffer<K>> keyBufferFlushPredicate;
	private boolean noBuffer;

	public AbstractLoadService(Integer batchSize, Duration bufferTimeout) {
		this(kb -> {
			if (batchSize != null && kb.size() >= batchSize)
				return true;
			if (bufferTimeout != null
					&& (System.currentTimeMillis() - kb.getCreatedAt().getTime()) > bufferTimeout.toMillis())
				return true;
			return false;
		});
		Asserts.isTrue(batchSize != null || bufferTimeout != null, "batchSize or bufferTimeout required");
		if (batchSize != null) {
			Utils.Lots.validateBatchSize(batchSize);
			this.noBuffer = batchSize == 1;
		}
	}

	public AbstractLoadService(Predicate<KeyBuffer<K>> keyBufferFlushPredicate) {
		this.keyBufferFlushPredicate = Objects.requireNonNull(keyBufferFlushPredicate);
	}

	@Override
	public Flux<Entry<K, V>> load(Executor readExecutor, Executor loadExecutor, Publisher<K> keyPublisher) {
		Objects.requireNonNull(readExecutor);
		Objects.requireNonNull(loadExecutor);
		if (keyPublisher == null)
			return Flux.empty();
		Flux<K> keyFlux = Fluxi.from(keyPublisher, Scheduli.createScheduler(readExecutor));
		var resultFlux = load(SubmitterSchedulerLFP.from(loadExecutor), buffer(keyFlux));
		resultFlux = Fluxi.logError(resultFlux, true);
		return resultFlux;
	}

	protected Flux<Entry<K, V>> load(SubmitterSchedulerLFP loadExecutor, Flux<Entry<G, Flux<K>>> keyBufferFlux) {
		return keyBufferFlux.flatMap(ent -> {
			return load(loadExecutor, ent.getKey(), ent.getValue());
		});
	}

	protected Flux<Entry<K, V>> load(SubmitterSchedulerLFP loadExecutor, G grouping, Flux<K> keyBatch) {
		Flux<Entry<K, V>> flux = Flux.create(sink -> {
			var future = loadExecutor.submit(() -> {
				loadAsync(grouping, keyBatch, sink);
				return Nada.get();
			});
			Fluxi.linkTermination(future, sink);
		});
		return flux;
	}

	protected Flux<Entry<G, Flux<K>>> buffer(Flux<K> keyFlux) {
		var groupToKeyFlux = keyFlux.flatMap(k -> {
			var groupings = Utils.Lots.stream(getGroupings(k)).nonNull().distinct();
			return Flux.fromStream(groupings).map(g -> Tuples.of(g, k));
		});
		if (noBuffer)
			return groupToKeyFlux.map(tup -> Utils.Lots.entry(tup.getT1(), Flux.just(tup.getT2())));
		Map<G, KeyBuffer<K>> buffer = new ConcurrentHashMap<>();
		Function<Boolean, Flux<Entry<G, Flux<K>>>> bufferFlusher = force -> {
			force = Boolean.TRUE.equals(force);
			var groupToKeyBufferEntryIter = buffer.entrySet().iterator();
			List<Entry<G, Flux<K>>> entryList = new ArrayList<>();
			while (groupToKeyBufferEntryIter.hasNext()) {
				var groupToKeyBufferEntry = groupToKeyBufferEntryIter.next();
				var group = groupToKeyBufferEntry.getKey();
				var keyBuffer = groupToKeyBufferEntry.getValue();
				if (keyBuffer == null || keyBuffer.isEmpty())
					continue;
				if (!force && !this.keyBufferFlushPredicate.test(keyBuffer))
					continue;
				groupToKeyBufferEntryIter.remove();
				entryList.add(Utils.Lots.entry(group, Flux.fromIterable(keyBuffer)));
			}
			return Flux.fromIterable(entryList);
		};
		Flux<Entry<G, Flux<K>>> resultFlux = groupToKeyFlux.flatMap(tup -> {
			buffer.computeIfAbsent(tup.getT1(), nil -> new KeyBuffer<>()).add(tup.getT2());
			return bufferFlusher.apply(false);
		});
		resultFlux = Flux.concat(resultFlux, Flux.defer(() -> {
			return bufferFlusher.apply(true);
		}));
		return resultFlux;

	};

	protected abstract Iterable<G> getGroupings(K key);

	protected abstract void loadAsync(G grouping, Flux<K> keyBatch, FluxSink<Entry<K, V>> sink) throws Exception;

	@SuppressWarnings("serial")
	public static class KeyBuffer<K> extends ArrayList<K> {

		private final Date createdAt = new Date();

		public Date getCreatedAt() {
			return createdAt;
		}

	}
}
