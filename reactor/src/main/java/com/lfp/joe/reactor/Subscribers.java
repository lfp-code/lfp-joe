package com.lfp.joe.reactor;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import reactor.core.CoreSubscriber;

public class Subscribers {

	public static <X> Subscriber<X> cancel() {
		return new CoreSubscriber<X>() {

			@Override
			public void onSubscribe(Subscription subscription) {
				subscription.cancel();
			}

			@Override
			public void onNext(X t) {
			}

			@Override
			public void onError(Throwable t) {
			}

			@Override
			public void onComplete() {
			}
		};
	}
}
