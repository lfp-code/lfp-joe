package com.lfp.joe.encryption;

import java.security.Key;

import org.encryptor4j.Encryptor;
import org.encryptor4j.factory.KeyFactory;

public class Encryptions {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static Key newKeyFromPassword(String password) {
		return KeyFactory.AES.keyFromPassword(password == null ? null : password.toCharArray());
	}

	public static Encryptor newAESEncryptor() {
		return newAESEncryptor(KeyFactory.AES.randomKey());
	}

	public static Encryptor newAESEncryptor(String password) {
		return newAESEncryptor(newKeyFromPassword(password));
	}

	public static Encryptor newAESEncryptor(Key secretKey) {
		Encryptor encryptor = new Encryptor(secretKey, "AES/CBC/PKCS5Padding", 16);
		return encryptor;
	}

	public static void main(String[] args) {
		Encryptor encryptor = Encryptions.newAESEncryptor("123");

	}

}
