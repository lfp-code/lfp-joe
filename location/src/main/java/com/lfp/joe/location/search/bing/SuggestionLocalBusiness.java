package com.lfp.joe.location.search.bing;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.lfp.joe.utils.Utils;

public class SuggestionLocalBusiness extends AbstractSuggestion {

	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("address")
	@Expose
	private Address address;
	@SerializedName("categories")
	@Expose
	private List<String> categories = null;

	public SuggestionLocalBusiness() {
		if (Utils.Strings.isBlank(getType()))
			this.setType("localBusiness");
	}

	@Override
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getCategories() {
		return categories;
	}

	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

}
