package com.lfp.joe.location.search.bing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.lfp.joe.utils.Utils;

public class SuggestionPlace extends AbstractSuggestion {
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("address")
	@Expose
	private Address address;

	public SuggestionPlace() {
		if (Utils.Strings.isBlank(getType()))
			this.setType("place");
	}

	@Override
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}