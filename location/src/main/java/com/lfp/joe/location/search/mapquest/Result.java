
package com.lfp.joe.location.search.mapquest;

import java.util.Objects;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class Result {

	@SerializedName("@id")
	@Expose
	public String id;
	@SerializedName("latLng")
	@Expose
	public LatLng latLng;
	@SerializedName("address")
	@Expose
	public Address address;
	@SerializedName("displayLatLng")
	@Expose
	public DisplayLatLng displayLatLng;
	@SerializedName("routingLatLng")
	@Expose
	public RoutingLatLng routingLatLng;

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Result other = (Result) obj;
		return Objects.equals(id, other.id);
	}
}
