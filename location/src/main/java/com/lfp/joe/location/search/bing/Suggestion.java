package com.lfp.joe.location.search.bing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Hashable;

import at.favre.lib.bytes.Bytes;

public interface Suggestion extends Hashable {

	String getType();

	String getId();

	String getReadLink();

	String getReadLinkPingSuffix();

	String getFormattingRuleId();

	Geo getGeo();

	Address getAddress();

	@Override
	default Bytes hash() {
		List<Object> parts = new ArrayList<>();
		int index = -1;
		for (var obj : Arrays.asList(getType(), getGeo(), getAddress())) {
			index++;
			parts.add(index);
			if (obj == null)
				continue;
			var je = Serials.Gsons.get().toJsonTree(obj);
			parts.add(Serials.Gsons.deepHash(je));
		}
		return Utils.Crypto.hashMD5(parts.toArray(Object[]::new));
	}

}