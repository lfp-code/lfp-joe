
package com.lfp.joe.location.search.nominatim;

import java.util.List;
import java.util.Map;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NominatimResponse {

	@SerializedName("place_id")
	@Expose
	private Integer placeId;
	@SerializedName("osm_type")
	@Expose
	private String osmType;
	@SerializedName("osm_id")
	@Expose
	private Integer osmId;
	@SerializedName("boundingbox")
	@Expose
	private List<String> boundingbox = null;
	@SerializedName("lat")
	@Expose
	private Double lat;
	@SerializedName("lon")
	@Expose
	private Double lon;
	@SerializedName("display_name")
	@Expose
	private String displayName;
	@SerializedName("place_rank")
	@Expose
	private Integer placeRank;
	@SerializedName("category")
	@Expose
	private String category;
	@SerializedName("type")
	@Expose
	private String type;
	@SerializedName("importance")
	@Expose
	private Double importance;
	@SerializedName("address")
	@Expose
	private Map<String, String> address;

	public Integer getPlaceId() {
		return placeId;
	}

	public void setPlaceId(Integer placeId) {
		this.placeId = placeId;
	}

	public String getOsmType() {
		return osmType;
	}

	public void setOsmType(String osmType) {
		this.osmType = osmType;
	}

	public Integer getOsmId() {
		return osmId;
	}

	public void setOsmId(Integer osmId) {
		this.osmId = osmId;
	}

	public List<String> getBoundingbox() {
		return boundingbox;
	}

	public void setBoundingbox(List<String> boundingbox) {
		this.boundingbox = boundingbox;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Integer getPlaceRank() {
		return placeRank;
	}

	public void setPlaceRank(Integer placeRank) {
		this.placeRank = placeRank;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getImportance() {
		return importance;
	}

	public void setImportance(Double importance) {
		this.importance = importance;
	}

	public Map<String, String> getAddress() {
		return address;
	}

	public void setAddress(Map<String, String> address) {
		this.address = address;
	}

}
