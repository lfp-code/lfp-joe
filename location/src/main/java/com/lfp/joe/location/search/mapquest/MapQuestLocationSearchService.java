package com.lfp.joe.location.search.mapquest;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.google.gson.JsonElement;
import com.google.gson.JsonSyntaxException;
import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.location.Location;
import com.lfp.joe.location.Locations;
import com.lfp.joe.location.config.LocationConfig;
import com.lfp.joe.location.search.AbstractLocationService;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;

public class MapQuestLocationSearchService extends AbstractLocationService.Http<Result> {

	public static MapQuestLocationSearchService getDefault() {
		return Instances.get(MapQuestLocationSearchService.class, MapQuestLocationSearchService::new);
	}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String URL = "https://services-here.aws.mapquest.com/v1/search?count=50&client=yogi&clip=none&query=";
	private static final Map<String, String> REQUEST_HEADERS = Map.of("Referer", "https://www.bing.com/");
	private static final String METADATA_KEY = "metadata";

	protected MapQuestLocationSearchService() {
		super();
	}

	public MapQuestLocationSearchService(
			ThrowingFunction<HttpRequest, HttpResponse<InputStream>, IOException> httpClient) {
		super(httpClient);
	}

	@Override
	protected Iterable<Result> searchInternal(String input, int limit) throws IOException, InterruptedException {
		var unique = new HashSet<String>();
		var pageStream = IntStreamEx.range(Integer.MAX_VALUE);
		pageStream = pageStream.takeWhile(v -> {
			return v == 0 || unique.size() > 1;
		});
		var resultListStream = pageStream.boxed().map(index -> {
			try {
				return searchIndex(input, index);
			} catch (IOException | JsonSyntaxException e) {
				LOGGER.warn("error during lookup. input:{}", input, e);
			}
			return List.<Result>of();
		});
		resultListStream = resultListStream.takeWhileInclusive(resultList -> {
			resultList.removeIf(v -> !unique.add(v.id));
			return !resultList.isEmpty();
		});
		var resultSream = resultListStream.chain(Streams.flatMap(List::stream));
		if (limit != -1)
			resultSream = resultSream.limit(limit);
		return resultSream;
	}

	@Override
	protected StreamEx<Location> mapToLocationInternal(StreamEx<Result> searchResults)
			throws IOException, InterruptedException {
		return searchResults.map(v -> {
			return toLocation(v);
		});
	}

	private List<Result> searchIndex(String input, int index) throws IOException, JsonSyntaxException {
		if (index > 0)
			return List.of();
		var uri = URI.create(URL + URLEncoder.encode(input, MachineConfig.getDefaultCharset()));
		var rb = HttpRequest.newBuilder().uri(uri).GET();
		rb = rb.setHeader("User-Agent", Configs.get(LocationConfig.class).searchUserAgent());
		for (var ent : REQUEST_HEADERS.entrySet())
			rb = rb.setHeader(ent.getKey(), ent.getValue());
		StreamEx<Result> results;
		var response = getHttpClient().apply(rb.build());
		try (var body = response.body()) {
			if (response.statusCode() / 100 != 2)
				throw new IOException("invalid status code:" + response);
			var je = Serials.Gsons.fromStream(body, JsonElement.class);
			results = parseResults(je);
		}
		return results.toList();
	}

	@SuppressWarnings("deprecation")
	private StreamEx<Result> parseResults(JsonElement je) throws JsonSyntaxException {
		var results = Serials.Gsons.tryGetAsJsonArray(je, "results").orElse(null);
		if (results == null)
			return StreamEx.empty();
		return Streams.of(results).map(v -> {
			var result = Serials.Gsons.get().fromJson(v, Result.class);
			return result;
		}).nonNull();
	}

	private Location toLocation(Result result) {
		var blder = Location.builder();
		var latLng = result.latLng;
		Optional.ofNullable(latLng).map(v -> v.lat).ifPresent(blder::latitude);
		Optional.ofNullable(latLng).map(v -> v.lng).ifPresent(blder::longitude);
		Optional.ofNullable(result.address).ifPresent(v -> appendAddress(blder, v));
		return blder.build();
	}

	private void appendAddress(Location.Builder blder, Address address) {
		{
			var mps = Address.meta().metaPropertyIterable();
			var streetStream = Streams.of(mps)
					.filter(v -> v.name().startsWith("address"))
					.map(v -> v.get(address))
					.map(v -> v == null ? null : v.toString());
			streetStream = streetStream.nonNull().flatMap(v -> Streams.of(v.split("\\s+")));
			streetStream = streetStream.mapPartial(Utils.Strings::trimToNullOptional);
			var streetParts = streetStream.toList();
			if (!streetParts.isEmpty()) {
				Utils.Strings.parseNumber(streetParts.get(0)).ifPresent(v -> {
					streetParts.remove(0);
					blder.number(v.longValue());
				});
				if (!streetParts.isEmpty())
					blder.street(Streams.of(streetParts).joining(" "));
			}
		}
		blder.locality(address.getLocality());
		blder.postalCode(address.getPostalCode());
		blder.region(address.getRegion());
		blder.subRegion(address.getCounty());
		Locations.parseCountryCode(address.getCountry()).ifPresent(blder::countryCode);
	}

	public static void main(String[] args) throws IOException {
		var locations = new MapQuestLocationSearchService().searchLocations("1135 parson curry rd malvern pa").toList();
		System.out.println(Serials.Gsons.getPretty().toJson(locations));
	}
}
