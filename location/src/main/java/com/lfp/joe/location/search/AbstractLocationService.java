package com.lfp.joe.location.search;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Redirect;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;

import org.apache.commons.lang3.Validate;
import org.joda.beans.Bean;
import org.joda.beans.JodaBeanUtils;

import com.github.throwable.beanref.BeanRef;
import com.github.throwable.beanref.BeanRoot;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.location.Location;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

public abstract class AbstractLocationService<S> implements LocationService<S> {

	@SuppressWarnings("unchecked")
	@Override
	public StreamEx<S> search(String input, int limit) throws IOException {
		Validate.isTrue(limit >= -1, "invalid limit:{}", limit);
		if (limit == 0)
			return StreamEx.empty();
		input = normalizeInput(input);
		if (input == null)
			return StreamEx.empty();
		Iterable<S> resultIble;
		try {
			resultIble = searchInternal(input, limit);
		} catch (InterruptedException e) {
			throw Utils.Exceptions.as(e, IOException.class);
		}
		var resultStream = Utils.Lots.stream(resultIble);
		resultStream = resultStream.filter(streamFilter());
		resultStream = resultStream.distinct();
		if (limit != -1)
			resultStream = resultStream.limit(limit);
		return resultStream;
	}

	@Override
	public StreamEx<Location> mapToLocation(Iterable<S> searchResults) throws IOException {
		if (searchResults == null)
			return StreamEx.empty();
		Iterable<Location> resultIble;
		try {
			resultIble = mapToLocationInternal(Utils.Lots.stream(searchResults).nonNull());
		} catch (InterruptedException e) {
			throw Utils.Exceptions.as(e, IOException.class);
		}
		var resultStream = Utils.Lots.stream(resultIble);
		resultStream = resultStream.filter(streamFilter());
		resultStream = resultStream.distinct();
		return resultStream;
	}

	protected abstract Iterable<S> searchInternal(String input, int limit) throws IOException, InterruptedException;

	protected abstract StreamEx<Location> mapToLocationInternal(StreamEx<S> searchResults)
			throws IOException, InterruptedException;

	private static String normalizeInput(String input) {
		input = Utils.Strings.trimToNull(input);
		return input;
	}

	@SuppressWarnings("unchecked")
	private static <X> Predicate<X> streamFilter() {
		return v -> {
			if (v == null)
				return false;
			Iterable<Object> values;
			if (v instanceof Bean) {
				var mps = JodaBeanUtils.metaBean(v.getClass()).metaPropertyIterable();
				values = Utils.Lots.stream(mps).map(mp -> mp.get((Bean) v));
			} else {
				BeanRoot<X> beanRef = (BeanRoot<X>) BeanRef.$(v.getClass());
				var bps = beanRef.all();
				if (bps.isEmpty())
					return true;
				values = Utils.Lots.stream(bps).map(bp -> bp.get(v));
			}
			for (var value : values) {
				if (value == null)
					continue;
				if (!(value instanceof CharSequence))
					return true;
				if (Utils.Strings.isNotBlank((CharSequence) value))
					return true;
			}
			return false;

		};
	}

	public static abstract class Http<S> extends AbstractLocationService<S> {
		private final ThrowingFunction<HttpRequest, HttpResponse<InputStream>, IOException> httpClient;

		public Http() {
			this(defaultHttpClient());
		}

		public Http(ThrowingFunction<HttpRequest, HttpResponse<InputStream>, IOException> httpClient) {
			this.httpClient = Objects.requireNonNull(httpClient);
		}

		protected ThrowingFunction<HttpRequest, HttpResponse<InputStream>, IOException> getHttpClient() {
			return httpClient;
		}

		protected static ThrowingFunction<HttpRequest, HttpResponse<InputStream>, IOException> defaultHttpClient() {
			var clientb = HttpClient.newBuilder().followRedirects(Redirect.NORMAL);
			var client = clientb.build();
			return req -> {
				try {
					return client.send(req, BodyHandlers.ofInputStream());
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
			};
		}

		protected static String urlEncode(Object obj) {
			if (obj == null)
				return "";
			String str = Objects.toString(obj);
			return URLEncoder.encode(str, StandardCharsets.UTF_8);
		}

		protected static String queryEncode(Map<String, Object> data) {
			if (data == null)
				return "";
			var estream = Utils.Lots.stream(data).mapKeys(k -> urlEncode(k)).mapValues(v -> urlEncode(v));
			return estream.map(ent -> String.format("%s=%s", ent.getKey(), ent.getValue())).joining("&");
		}

	}

}
