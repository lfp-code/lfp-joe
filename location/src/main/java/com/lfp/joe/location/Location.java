package com.lfp.joe.location;

import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;

import org.joda.beans.Bean;
import org.joda.beans.BeanDefinition;
import org.joda.beans.ImmutableBean;
import org.joda.beans.ImmutablePreBuild;
import org.joda.beans.JodaBeanUtils;
import org.joda.beans.MetaProperty;
import org.joda.beans.Property;
import org.joda.beans.PropertyDefinition;
import org.joda.beans.impl.direct.DirectFieldsBeanBuilder;
import org.joda.beans.impl.direct.DirectMetaBean;
import org.joda.beans.impl.direct.DirectMetaProperty;
import org.joda.beans.impl.direct.DirectMetaPropertyMap;

import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Hashable;
import com.neovisionaries.i18n.CountryCode;

import at.favre.lib.bytes.Bytes;

@BeanDefinition
public class Location implements Hashable, ImmutableBean {

	@PropertyDefinition
	private final Double longitude;

	@PropertyDefinition
	private final Double latitude;

	// 1045
	@PropertyDefinition
	private final Long number;

	// first avenue
	@PropertyDefinition
	private final String street;

	// King of Prussia
	@PropertyDefinition
	private final String locality;

	// Pennsylvania
	@PropertyDefinition
	private final String region;

	// Montgomery County
	@PropertyDefinition
	private final String subRegion;

	// 19406
	@PropertyDefinition
	private final String postalCode;

	// USA
	@PropertyDefinition
	private final CountryCode countryCode;

	// Formatted address
	@PropertyDefinition(get = "manual", equalsHashCodeStyle = "omit")
	private final String text;

	public String getText() {
		if (Utils.Strings.isNotBlank(text))
			return text;
		var regionPostal = Utils.Lots.stream(getRegion(), getPostalCode()).filter(Utils.Strings::isNotBlank)
				.joining(" ");
		var countryName = Optional.ofNullable(getCountryCode()).map(v -> v.getName()).orElse(null);
		var parts = Arrays.asList(Optional.ofNullable(getNumber()).map(Object::toString).orElse(null), getStreet(),
				getLocality(), regionPostal, countryName);
		var result = Utils.Lots.stream(parts).map(Utils.Strings::trim).filter(Utils.Strings::isNotBlank).joining(", ");
		if (Utils.Strings.isNotBlank(result))
			return result;
		if (latitude != null && longitude != null)
			return String.format("%s, %s", latitude, longitude);
		return null;
	}

	public boolean hasLatLong() {
		return this.getLatitude() != null && this.getLongitude() != null;
	}

	@Override
	public Bytes hash() {
		return JodaBeans.hash(Utils.Crypto.getMessageDigestMD5(), this, (mp, v) -> {
			if (meta().text().equals(mp))
				return null;
			return v;
		});
	}

	@Override
	public String toString() {
		return JodaBeans.toString(this);
	}

	public static Location.Builder builder(CountryCode countryCode) {
		return Location.builder().countryCode(countryCode);
	}

	public static Location.Builder builder(Locale locale) {
		return Location.builder().countryCode(Locations.toCountryCode(locale).orElse(null));
	}

	public static Location.Builder builder(String countryCode) {
		return Location.builder().countryCode(Locations.parseCountryCode(countryCode).orElse(null));
	}

	protected static void preBuildProtected(Builder builder) {
		for (var mp : meta().metaPropertyIterable()) {
			var value = JodaBeans.get(builder, mp);
			var valueStr = Utils.Types.tryCast(value, String.class).orElse(null);
			if (valueStr == null)
				continue;
			if (Utils.Strings.isBlank(valueStr))
				builder.set(mp, null);
		}
		// update street
		if (Utils.Strings.isNotBlank(builder.text) && Utils.Strings.isNotBlank(builder.street)
				&& Utils.Strings.startsWithIgnoreCase(builder.text, builder.street)) {
			var text = builder.text;
			text = Utils.Lots.stream(text.split(Pattern.quote(","))).mapFirst(v -> normalizeStreet(v)).joining(",");
			builder.text(text);
		}
		// update country
		if (Utils.Strings.isNotBlank(builder.text) && builder.countryCode != null
				&& !Utils.Strings.containsIgnoreCase(builder.text, builder.countryCode.getName())
				&& !Utils.Strings.endsWithIgnoreCase(builder.text, builder.countryCode.getName())) {
			var text = builder.text;
			text += ", " + builder.countryCode.getName();
			builder.text(text);
		}
		builder.street(normalizeStreet(builder.street));
		builder.region(normalizeRegion(builder.countryCode, builder.region));
	}

	@ImmutablePreBuild
	private static void preBuild(Builder builder) {
		preBuildProtected(builder);
	}

	private static String normalizeStreet(String street) {
		if (Utils.Strings.isBlank(street))
			return street;
		Map<String, String> tokens = Map.of("rd", "Road", "st", "Street");
		for (var ent : tokens.entrySet()) {
			String search = " " + ent.getKey();
			int index = Utils.Strings.indexOfIgnoreCase(street, search);
			if (index < 0)
				continue;
			street = street.substring(0, index) + " " + ent.getValue();
		}
		return street;
	}

	private static String normalizeRegion(CountryCode countryCode, String region) {
		region = Utils.Strings.trimToNull(region);
		if (region == null || region.length() > 2)
			return region;
		if (!CountryCode.US.equals(countryCode))
			return region;
		var usState = USState.parse(region).orElse(null);
		if (usState == null)
			return region;
		return usState.toString();
	}

	public static void main(String[] args) {
		JodaBeans.updateCode();
	}

	// ------------------------- AUTOGENERATED START -------------------------
	/// CLOVER:OFF
	/**
	 * The meta-bean for {@code Location}.
	 * 
	 * @return the meta-bean, not null
	 */
	public static Location.Meta meta() {
		return Location.Meta.INSTANCE;
	}

	static {
		JodaBeanUtils.registerMetaBean(Location.Meta.INSTANCE);
	}

	/**
	 * Returns a builder used to create an instance of the bean.
	 * 
	 * @return the builder, not null
	 */
	public static Location.Builder builder() {
		return new Location.Builder();
	}

	/**
	 * Restricted constructor.
	 * 
	 * @param builder the builder to copy from, not null
	 */
	protected Location(Location.Builder builder) {
		this.longitude = builder.longitude;
		this.latitude = builder.latitude;
		this.number = builder.number;
		this.street = builder.street;
		this.locality = builder.locality;
		this.region = builder.region;
		this.subRegion = builder.subRegion;
		this.postalCode = builder.postalCode;
		this.countryCode = builder.countryCode;
		this.text = builder.text;
	}

	@Override
	public Location.Meta metaBean() {
		return Location.Meta.INSTANCE;
	}

	@Override
	public <R> Property<R> property(String propertyName) {
		return metaBean().<R>metaProperty(propertyName).createProperty(this);
	}

	@Override
	public Set<String> propertyNames() {
		return metaBean().metaPropertyMap().keySet();
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the longitude.
	 * 
	 * @return the value of the property
	 */
	public Double getLongitude() {
		return longitude;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the latitude.
	 * 
	 * @return the value of the property
	 */
	public Double getLatitude() {
		return latitude;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the number.
	 * 
	 * @return the value of the property
	 */
	public Long getNumber() {
		return number;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the street.
	 * 
	 * @return the value of the property
	 */
	public String getStreet() {
		return street;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the locality.
	 * 
	 * @return the value of the property
	 */
	public String getLocality() {
		return locality;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the region.
	 * 
	 * @return the value of the property
	 */
	public String getRegion() {
		return region;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the subRegion.
	 * 
	 * @return the value of the property
	 */
	public String getSubRegion() {
		return subRegion;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the postalCode.
	 * 
	 * @return the value of the property
	 */
	public String getPostalCode() {
		return postalCode;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the countryCode.
	 * 
	 * @return the value of the property
	 */
	public CountryCode getCountryCode() {
		return countryCode;
	}

	// -----------------------------------------------------------------------
	/**
	 * Returns a builder that allows this bean to be mutated.
	 * 
	 * @return the mutable builder, not null
	 */
	public Builder toBuilder() {
		return new Builder(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj != null && obj.getClass() == this.getClass()) {
			Location other = (Location) obj;
			return JodaBeanUtils.equal(longitude, other.longitude) && JodaBeanUtils.equal(latitude, other.latitude)
					&& JodaBeanUtils.equal(number, other.number) && JodaBeanUtils.equal(street, other.street)
					&& JodaBeanUtils.equal(locality, other.locality) && JodaBeanUtils.equal(region, other.region)
					&& JodaBeanUtils.equal(subRegion, other.subRegion)
					&& JodaBeanUtils.equal(postalCode, other.postalCode)
					&& JodaBeanUtils.equal(countryCode, other.countryCode);
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = getClass().hashCode();
		hash = hash * 31 + JodaBeanUtils.hashCode(longitude);
		hash = hash * 31 + JodaBeanUtils.hashCode(latitude);
		hash = hash * 31 + JodaBeanUtils.hashCode(number);
		hash = hash * 31 + JodaBeanUtils.hashCode(street);
		hash = hash * 31 + JodaBeanUtils.hashCode(locality);
		hash = hash * 31 + JodaBeanUtils.hashCode(region);
		hash = hash * 31 + JodaBeanUtils.hashCode(subRegion);
		hash = hash * 31 + JodaBeanUtils.hashCode(postalCode);
		hash = hash * 31 + JodaBeanUtils.hashCode(countryCode);
		return hash;
	}

	// -----------------------------------------------------------------------
	/**
	 * The meta-bean for {@code Location}.
	 */
	public static class Meta extends DirectMetaBean {
		/**
		 * The singleton instance of the meta-bean.
		 */
		static final Meta INSTANCE = new Meta();

		/**
		 * The meta-property for the {@code longitude} property.
		 */
		private final MetaProperty<Double> longitude = DirectMetaProperty.ofImmutable(this, "longitude", Location.class,
				Double.class);
		/**
		 * The meta-property for the {@code latitude} property.
		 */
		private final MetaProperty<Double> latitude = DirectMetaProperty.ofImmutable(this, "latitude", Location.class,
				Double.class);
		/**
		 * The meta-property for the {@code number} property.
		 */
		private final MetaProperty<Long> number = DirectMetaProperty.ofImmutable(this, "number", Location.class,
				Long.class);
		/**
		 * The meta-property for the {@code street} property.
		 */
		private final MetaProperty<String> street = DirectMetaProperty.ofImmutable(this, "street", Location.class,
				String.class);
		/**
		 * The meta-property for the {@code locality} property.
		 */
		private final MetaProperty<String> locality = DirectMetaProperty.ofImmutable(this, "locality", Location.class,
				String.class);
		/**
		 * The meta-property for the {@code region} property.
		 */
		private final MetaProperty<String> region = DirectMetaProperty.ofImmutable(this, "region", Location.class,
				String.class);
		/**
		 * The meta-property for the {@code subRegion} property.
		 */
		private final MetaProperty<String> subRegion = DirectMetaProperty.ofImmutable(this, "subRegion", Location.class,
				String.class);
		/**
		 * The meta-property for the {@code postalCode} property.
		 */
		private final MetaProperty<String> postalCode = DirectMetaProperty.ofImmutable(this, "postalCode",
				Location.class, String.class);
		/**
		 * The meta-property for the {@code countryCode} property.
		 */
		private final MetaProperty<CountryCode> countryCode = DirectMetaProperty.ofImmutable(this, "countryCode",
				Location.class, CountryCode.class);
		/**
		 * The meta-property for the {@code text} property.
		 */
		private final MetaProperty<String> text = DirectMetaProperty.ofImmutable(this, "text", Location.class,
				String.class);
		/**
		 * The meta-properties.
		 */
		private final Map<String, MetaProperty<?>> metaPropertyMap$ = new DirectMetaPropertyMap(this, null, "longitude",
				"latitude", "number", "street", "locality", "region", "subRegion", "postalCode", "countryCode", "text");

		/**
		 * Restricted constructor.
		 */
		protected Meta() {}

		@Override
		protected MetaProperty<?> metaPropertyGet(String propertyName) {
			switch (propertyName.hashCode()) {
			case 137365935: // longitude
				return longitude;
			case -1439978388: // latitude
				return latitude;
			case -1034364087: // number
				return number;
			case -891990013: // street
				return street;
			case 1900805475: // locality
				return locality;
			case -934795532: // region
				return region;
			case -428400300: // subRegion
				return subRegion;
			case 2011152728: // postalCode
				return postalCode;
			case -1477067101: // countryCode
				return countryCode;
			case 3556653: // text
				return text;
			}
			return super.metaPropertyGet(propertyName);
		}

		@Override
		public Location.Builder builder() {
			return new Location.Builder();
		}

		@Override
		public Class<? extends Location> beanType() {
			return Location.class;
		}

		@Override
		public Map<String, MetaProperty<?>> metaPropertyMap() {
			return metaPropertyMap$;
		}

		// -----------------------------------------------------------------------
		/**
		 * The meta-property for the {@code longitude} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Double> longitude() {
			return longitude;
		}

		/**
		 * The meta-property for the {@code latitude} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Double> latitude() {
			return latitude;
		}

		/**
		 * The meta-property for the {@code number} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Long> number() {
			return number;
		}

		/**
		 * The meta-property for the {@code street} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> street() {
			return street;
		}

		/**
		 * The meta-property for the {@code locality} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> locality() {
			return locality;
		}

		/**
		 * The meta-property for the {@code region} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> region() {
			return region;
		}

		/**
		 * The meta-property for the {@code subRegion} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> subRegion() {
			return subRegion;
		}

		/**
		 * The meta-property for the {@code postalCode} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> postalCode() {
			return postalCode;
		}

		/**
		 * The meta-property for the {@code countryCode} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<CountryCode> countryCode() {
			return countryCode;
		}

		/**
		 * The meta-property for the {@code text} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> text() {
			return text;
		}

		// -----------------------------------------------------------------------
		@Override
		protected Object propertyGet(Bean bean, String propertyName, boolean quiet) {
			switch (propertyName.hashCode()) {
			case 137365935: // longitude
				return ((Location) bean).getLongitude();
			case -1439978388: // latitude
				return ((Location) bean).getLatitude();
			case -1034364087: // number
				return ((Location) bean).getNumber();
			case -891990013: // street
				return ((Location) bean).getStreet();
			case 1900805475: // locality
				return ((Location) bean).getLocality();
			case -934795532: // region
				return ((Location) bean).getRegion();
			case -428400300: // subRegion
				return ((Location) bean).getSubRegion();
			case 2011152728: // postalCode
				return ((Location) bean).getPostalCode();
			case -1477067101: // countryCode
				return ((Location) bean).getCountryCode();
			case 3556653: // text
				return ((Location) bean).getText();
			}
			return super.propertyGet(bean, propertyName, quiet);
		}

		@Override
		protected void propertySet(Bean bean, String propertyName, Object newValue, boolean quiet) {
			metaProperty(propertyName);
			if (quiet) {
				return;
			}
			throw new UnsupportedOperationException("Property cannot be written: " + propertyName);
		}

	}

	// -----------------------------------------------------------------------
	/**
	 * The bean-builder for {@code Location}.
	 */
	public static class Builder extends DirectFieldsBeanBuilder<Location> {

		private Double longitude;
		private Double latitude;
		private Long number;
		private String street;
		private String locality;
		private String region;
		private String subRegion;
		private String postalCode;
		private CountryCode countryCode;
		private String text;

		/**
		 * Restricted constructor.
		 */
		protected Builder() {}

		/**
		 * Restricted copy constructor.
		 * 
		 * @param beanToCopy the bean to copy from, not null
		 */
		protected Builder(Location beanToCopy) {
			this.longitude = beanToCopy.getLongitude();
			this.latitude = beanToCopy.getLatitude();
			this.number = beanToCopy.getNumber();
			this.street = beanToCopy.getStreet();
			this.locality = beanToCopy.getLocality();
			this.region = beanToCopy.getRegion();
			this.subRegion = beanToCopy.getSubRegion();
			this.postalCode = beanToCopy.getPostalCode();
			this.countryCode = beanToCopy.getCountryCode();
			this.text = beanToCopy.getText();
		}

		// -----------------------------------------------------------------------
		@Override
		public Object get(String propertyName) {
			switch (propertyName.hashCode()) {
			case 137365935: // longitude
				return longitude;
			case -1439978388: // latitude
				return latitude;
			case -1034364087: // number
				return number;
			case -891990013: // street
				return street;
			case 1900805475: // locality
				return locality;
			case -934795532: // region
				return region;
			case -428400300: // subRegion
				return subRegion;
			case 2011152728: // postalCode
				return postalCode;
			case -1477067101: // countryCode
				return countryCode;
			case 3556653: // text
				return text;
			default:
				throw new NoSuchElementException("Unknown property: " + propertyName);
			}
		}

		@Override
		public Builder set(String propertyName, Object newValue) {
			switch (propertyName.hashCode()) {
			case 137365935: // longitude
				this.longitude = (Double) newValue;
				break;
			case -1439978388: // latitude
				this.latitude = (Double) newValue;
				break;
			case -1034364087: // number
				this.number = (Long) newValue;
				break;
			case -891990013: // street
				this.street = (String) newValue;
				break;
			case 1900805475: // locality
				this.locality = (String) newValue;
				break;
			case -934795532: // region
				this.region = (String) newValue;
				break;
			case -428400300: // subRegion
				this.subRegion = (String) newValue;
				break;
			case 2011152728: // postalCode
				this.postalCode = (String) newValue;
				break;
			case -1477067101: // countryCode
				this.countryCode = (CountryCode) newValue;
				break;
			case 3556653: // text
				this.text = (String) newValue;
				break;
			default:
				throw new NoSuchElementException("Unknown property: " + propertyName);
			}
			return this;
		}

		@Override
		public Builder set(MetaProperty<?> property, Object value) {
			super.set(property, value);
			return this;
		}

		/**
		 * @deprecated Use Joda-Convert in application code
		 */
		@Override
		@Deprecated
		public Builder setString(String propertyName, String value) {
			setString(meta().metaProperty(propertyName), value);
			return this;
		}

		/**
		 * @deprecated Use Joda-Convert in application code
		 */
		@Override
		@Deprecated
		public Builder setString(MetaProperty<?> property, String value) {
			super.setString(property, value);
			return this;
		}

		/**
		 * @deprecated Loop in application code
		 */
		@Override
		@Deprecated
		public Builder setAll(Map<String, ? extends Object> propertyValueMap) {
			super.setAll(propertyValueMap);
			return this;
		}

		@Override
		public Location build() {
			preBuild(this);
			return new Location(this);
		}

		// -----------------------------------------------------------------------
		/**
		 * Sets the longitude.
		 * 
		 * @param longitude the new value
		 * @return this, for chaining, not null
		 */
		public Builder longitude(Double longitude) {
			this.longitude = longitude;
			return this;
		}

		/**
		 * Sets the latitude.
		 * 
		 * @param latitude the new value
		 * @return this, for chaining, not null
		 */
		public Builder latitude(Double latitude) {
			this.latitude = latitude;
			return this;
		}

		/**
		 * Sets the number.
		 * 
		 * @param number the new value
		 * @return this, for chaining, not null
		 */
		public Builder number(Long number) {
			this.number = number;
			return this;
		}

		/**
		 * Sets the street.
		 * 
		 * @param street the new value
		 * @return this, for chaining, not null
		 */
		public Builder street(String street) {
			this.street = street;
			return this;
		}

		/**
		 * Sets the locality.
		 * 
		 * @param locality the new value
		 * @return this, for chaining, not null
		 */
		public Builder locality(String locality) {
			this.locality = locality;
			return this;
		}

		/**
		 * Sets the region.
		 * 
		 * @param region the new value
		 * @return this, for chaining, not null
		 */
		public Builder region(String region) {
			this.region = region;
			return this;
		}

		/**
		 * Sets the subRegion.
		 * 
		 * @param subRegion the new value
		 * @return this, for chaining, not null
		 */
		public Builder subRegion(String subRegion) {
			this.subRegion = subRegion;
			return this;
		}

		/**
		 * Sets the postalCode.
		 * 
		 * @param postalCode the new value
		 * @return this, for chaining, not null
		 */
		public Builder postalCode(String postalCode) {
			this.postalCode = postalCode;
			return this;
		}

		/**
		 * Sets the countryCode.
		 * 
		 * @param countryCode the new value
		 * @return this, for chaining, not null
		 */
		public Builder countryCode(CountryCode countryCode) {
			this.countryCode = countryCode;
			return this;
		}

		/**
		 * Sets the text.
		 * 
		 * @param text the new value
		 * @return this, for chaining, not null
		 */
		public Builder text(String text) {
			this.text = text;
			return this;
		}

		// -----------------------------------------------------------------------
		@Override
		public String toString() {
			StringBuilder buf = new StringBuilder(352);
			buf.append("Location.Builder{");
			int len = buf.length();
			toString(buf);
			if (buf.length() > len) {
				buf.setLength(buf.length() - 2);
			}
			buf.append('}');
			return buf.toString();
		}

		protected void toString(StringBuilder buf) {
			buf.append("longitude").append('=').append(JodaBeanUtils.toString(longitude)).append(',').append(' ');
			buf.append("latitude").append('=').append(JodaBeanUtils.toString(latitude)).append(',').append(' ');
			buf.append("number").append('=').append(JodaBeanUtils.toString(number)).append(',').append(' ');
			buf.append("street").append('=').append(JodaBeanUtils.toString(street)).append(',').append(' ');
			buf.append("locality").append('=').append(JodaBeanUtils.toString(locality)).append(',').append(' ');
			buf.append("region").append('=').append(JodaBeanUtils.toString(region)).append(',').append(' ');
			buf.append("subRegion").append('=').append(JodaBeanUtils.toString(subRegion)).append(',').append(' ');
			buf.append("postalCode").append('=').append(JodaBeanUtils.toString(postalCode)).append(',').append(' ');
			buf.append("countryCode").append('=').append(JodaBeanUtils.toString(countryCode)).append(',').append(' ');
			buf.append("text").append('=').append(JodaBeanUtils.toString(text)).append(',').append(' ');
		}

	}

	/// CLOVER:ON
	// -------------------------- AUTOGENERATED END --------------------------
}
