package com.lfp.joe.location.search.bing;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

import com.github.throwable.beanref.BeanRef;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

public class SuggestionDeserializer implements JsonDeserializer<List<Suggestion>> {

	@Override
	public List<Suggestion> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		var jarr = Serials.Gsons.tryGetAsJsonArray(json).orElse(null);
		if (jarr == null || jarr.size() == 0)
			return Collections.emptyList();
		var stream = Utils.Lots.stream(jarr).map(v -> {
			return deserializeElement(v, context);
		});
		stream=stream.nonNull();
		return stream.toList();
	}

	protected Suggestion deserializeElement(JsonElement json, JsonDeserializationContext context)
			throws JsonParseException {
		if (json == null || !json.isJsonObject())
			return null;
		var typeStr = Serials.Gsons.tryGetAsString(json, "_type").orElse(null);
		Class<? extends Suggestion> classType;
		if ("PostalAddress".equals(typeStr))
			classType = SuggestionPostalAddress.class;
		else if ("Place".equals(typeStr))
			classType = SuggestionPlace.class;
		else if ("LocalBusiness".equals(typeStr))
			classType = SuggestionLocalBusiness.class;
		else
			classType = null;
		if (classType == null)
			return null;
		var addressPath = BeanRef.$(Suggestion::getAddress).getPath();
		var addressMember = Serials.Gsons.tryGetAsJsonObject(json, addressPath).orElse(null);
		if (addressMember == null || addressMember.size() == 0) {
			addressMember = new JsonObject();
			json.getAsJsonObject().remove(addressPath);
			for (var bp : BeanRef.$(Address.class).all()) {
				var value = Serials.Gsons.tryGet(json, bp.getPath()).orElse(null);
				if (value != null)
					addressMember.add(bp.getPath(), value);
			}
			if (addressMember.size() > 0)
				json.getAsJsonObject().add(addressPath, addressMember);
		}
		var result = context.deserialize(json, classType);
		return (Suggestion) result;
	}

}
