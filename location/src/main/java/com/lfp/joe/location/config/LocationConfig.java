package com.lfp.joe.location.config;

import org.aeonbits.owner.Config;

public interface LocationConfig extends Config {

	@DefaultValue("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36")
	String searchUserAgent();

}
