package com.lfp.joe.location.search.bing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.lfp.joe.utils.Utils;

public class SuggestionPostalAddress extends AbstractSuggestion {

	@SerializedName("address")
	@Expose
	private Address address;

	public SuggestionPostalAddress() {
		if (Utils.Strings.isBlank(getType()))
			this.setType("postalAddress");
	}

	@Override
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}
