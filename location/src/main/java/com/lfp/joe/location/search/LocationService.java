package com.lfp.joe.location.search;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

import com.lfp.joe.location.Location;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

public interface LocationService<S> {

	default StreamEx<S> search(String input) throws IOException {
		return search(input, -1);
	}

	StreamEx<S> search(String input, int limit) throws IOException;

	default StreamEx<Location> searchLocations(String input) throws IOException {
		return searchLocations(input, -1);
	}

	default StreamEx<Location> searchLocations(String input, int limit) throws IOException {
		var stream = search(input, limit);
		stream = Utils.Lots.stream(stream);
		return mapToLocation(stream);
	}

	default Optional<Location> mapToLocation(S searchResult) throws IOException {
		if (searchResult == null)
			return Optional.empty();
		return mapToLocation(Arrays.asList(searchResult)).nonNull().findFirst();
	}

	StreamEx<Location> mapToLocation(Iterable<S> searchResults) throws IOException;

}
