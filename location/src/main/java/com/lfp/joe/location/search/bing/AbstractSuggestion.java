package com.lfp.joe.location.search.bing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public abstract class AbstractSuggestion implements Suggestion {

	@SerializedName("_type")
	@Expose
	private String type;
	@SerializedName("id")
	@Expose
	private String id;
	@SerializedName("readLink")
	@Expose
	private String readLink;
	@SerializedName("readLinkPingSuffix")
	@Expose
	private String readLinkPingSuffix;
	@SerializedName("formattingRuleId")
	@Expose
	private String formattingRuleId;
	@SerializedName("geo")
	@Expose
	private Geo geo;

	@Override
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String getReadLink() {
		return readLink;
	}

	public void setReadLink(String readLink) {
		this.readLink = readLink;
	}

	@Override
	public String getReadLinkPingSuffix() {
		return readLinkPingSuffix;
	}

	public void setReadLinkPingSuffix(String readLinkPingSuffix) {
		this.readLinkPingSuffix = readLinkPingSuffix;
	}

	@Override
	public String getFormattingRuleId() {
		return formattingRuleId;
	}

	public void setFormattingRuleId(String formattingRuleId) {
		this.formattingRuleId = formattingRuleId;
	}

	public Geo getGeo() {
		return geo;
	}

	public void setGeo(Geo geo) {
		this.geo = geo;
	}
}
