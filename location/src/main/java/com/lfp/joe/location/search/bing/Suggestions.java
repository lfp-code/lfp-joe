package com.lfp.joe.location.search.bing;

import java.util.List;

import com.google.gson.annotations.JsonAdapter;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

public class Suggestions {

	@JsonAdapter(SuggestionDeserializer.class)
	private List<Suggestion> value;

	public StreamEx<Suggestion> streamValues() {
		return Utils.Lots.stream(value).nonNull().filter(v -> v.getAddress() != null);
	}

	public List<Suggestion> getValue() {
		return value;
	}

	public void setValue(List<Suggestion> value) {
		this.value = value;
	}
}
