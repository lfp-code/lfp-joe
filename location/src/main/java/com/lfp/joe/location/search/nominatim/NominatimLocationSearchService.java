package com.lfp.joe.location.search.nominatim;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.Validate;

import com.google.common.reflect.TypeToken;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.location.Location;
import com.lfp.joe.location.Locations;
import com.lfp.joe.location.search.AbstractLocationService;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.neovisionaries.i18n.CountryCode;

import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public class NominatimLocationSearchService extends AbstractLocationService.Http<NominatimResponse> {

	private static MemoizedSupplier<NominatimLocationSearchService> DEFAULT_INSTANCE_S = Utils.Functions
			.memoize(() -> new NominatimLocationSearchService(null));

	public static NominatimLocationSearchService getDefault() {
		return DEFAULT_INSTANCE_S.get();
	}

	private static final TypeToken<List<NominatimResponse>> RESPONSE_TYPE_TOKEN = new TypeToken<List<NominatimResponse>>() {};
	private static final String URL_BASE = "https://nominatim.openstreetmap.org/search";
	private static final Map<String, Object> DEFAULT_PARAMS = Map.of("format", "jsonv2", "addressdetails", 1);
	private final String email;

	public NominatimLocationSearchService(String email) {
		this(defaultHttpClient(), email);
	}

	public NominatimLocationSearchService(
			ThrowingFunction<HttpRequest, HttpResponse<InputStream>, IOException> httpFunction, String email) {
		super(httpFunction);
		this.email = Utils.Strings.trimToNull(email);
	}

	@Override
	protected Iterable<NominatimResponse> searchInternal(String input, int limit)
			throws IOException, InterruptedException {
		Map<String, Object> params = new LinkedHashMap<>(DEFAULT_PARAMS);
		params.put("limit", limit);
		params.put("q", input);
		if (Utils.Strings.isNotBlank(email))
			params.put("email", email);
		String query = queryEncode(params);
		URI uri = URI.create(URL_BASE + "?" + query);
		var request = HttpRequest.newBuilder(uri).GET().build();
		List<NominatimResponse> nominatimResponses;
		{
			var response = this.getHttpClient().apply(request);
			try (var body = response.body()) {
				Validate.isTrue(response.statusCode() / 100 == 2);
				nominatimResponses = Serials.Gsons.fromStream(body, RESPONSE_TYPE_TOKEN);
			}
		}
		return Utils.Lots.stream(nominatimResponses).nonNull();
	}

	@Override
	protected StreamEx<Location> mapToLocationInternal(StreamEx<NominatimResponse> searchResults)
			throws IOException, InterruptedException {
		return searchResults.map(v -> toLocation(v));
	}

	private static Location toLocation(NominatimResponse nominatimResponse) {
		var locationBuilder = Location.builder();
		locationBuilder.latitude(nominatimResponse.getLat());
		locationBuilder.longitude(nominatimResponse.getLon());
		String street;
		{
			var numberStr = firstElement(nominatimResponse, "house_number", "house_name");
			var numberOp = Utils.Strings.parseNumber(numberStr).map(Number::doubleValue).filter(v -> v % 1 == 0);
			if (numberOp.isPresent()) {
				locationBuilder.number(numberOp.get().longValue());
				numberStr = null;
			}
			var road = firstElement(nominatimResponse, "road", "city_block");
			street = Utils.Lots.stream(numberStr, road).nonNull().joining(" ");
		}
		if (Utils.Strings.isNotBlank(street))
			locationBuilder.street(street);
		locationBuilder.locality(firstElement(nominatimResponse, "city", "town", "city_district", "borough", "village",
				"neighbourhood", "subdivision", "suburb"));
		locationBuilder.region(firstElement(nominatimResponse, "state", "region"));
		locationBuilder.subRegion(firstElement(nominatimResponse, "state_district", "county"));
		locationBuilder.postalCode(firstElement(nominatimResponse, "postcode", "zipcode"));
		var countryCode = streamElements(nominatimResponse, "country_code", "country").values()
				.map(v -> Locations.parseCountryCode(v).orElse(null))
				.nonNull()
				.findFirst()
				.orElse(null);
		locationBuilder.countryCode(countryCode);
		if (!CountryCode.US.equals(countryCode)) {
			var displayName = nominatimResponse.getDisplayName();
			if (Utils.Strings.isNotBlank(displayName))
				locationBuilder.text(displayName);
		}
		return locationBuilder.build();
	}

	private static String firstElement(NominatimResponse nominatimResponse, String... keyFilters) {
		return streamElements(nominatimResponse, keyFilters).values().findFirst().orElse(null);
	}

	private static EntryStream<String, String> streamElements(NominatimResponse nominatimResponse,
			String... keyFilters) {
		if (nominatimResponse == null)
			return EntryStream.empty();
		var estream = Utils.Lots.stream(nominatimResponse.getAddress());
		estream = estream.filterKeys(Utils.Strings::isNotBlank);
		estream = estream.filterValues(Utils.Strings::isNotBlank);
		var keyFiltersList = Utils.Lots.stream(keyFilters).filter(Utils.Strings::isNotBlank).distinct().toList();
		if (keyFiltersList.isEmpty())
			return estream;
		estream = estream.filterKeys(keyFiltersList::contains);
		estream = estream.sortedBy(ent -> {
			var index = keyFiltersList.indexOf(ent.getKey());
			if (index < 0)
				return Integer.MAX_VALUE;
			return index;
		});
		return estream;
	}

	public static void main(String[] args) throws IOException {
		double val = 1.5;
		System.out.println(val % 1);
		var service = new NominatimLocationSearchService(null);
		var list = service.searchLocations("1045 1st ave 19406").toList();
		System.out.println(Serials.Gsons.getPretty().toJson(list));
	}

}
