package com.lfp.joe.location.search.bing;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.time.Duration;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringEscapeUtils;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.google.gson.JsonElement;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.location.Location;
import com.lfp.joe.location.Locations;
import com.lfp.joe.location.config.LocationConfig;
import com.lfp.joe.location.search.AbstractLocationService;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

public class BingMapsLocationSearchService extends AbstractLocationService.Http<Suggestion> {

	private static MemoizedSupplier<BingMapsLocationSearchService> DEFAULT_INSTANCE_S = Utils.Functions
			.memoize(() -> new BingMapsLocationSearchService());

	public static BingMapsLocationSearchService getDefault() {
		return DEFAULT_INSTANCE_S.get();
	}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 1;
	private static final Map<String, String> REQUEST_HEADERS = Map.of("Referer", "https://www.bing.com/");
	private static final Duration AUTO_SUGGEST_APP_ID_REFRESH = Duration.ofMinutes(15);
	private static final String AUTO_SUGGEST_APP_ID_DETECT = "\"autosuggestAppid\"";
	private static final Pattern AUTO_SUGGEST_APP_ID_PATTERN = Pattern.compile("\"autosuggestAppid\"\\s?:\\s?\"\\w*\"");
	private static final Cache<Nada, String> APP_ID_CACHE = Caffeine.newBuilder()
			.expireAfterAccess(Duration.ofSeconds(5))
			.maximumSize(1)
			.build();

	public BingMapsLocationSearchService() {
		super();
	}

	public BingMapsLocationSearchService(
			ThrowingFunction<HttpRequest, HttpResponse<InputStream>, IOException> httpClient) {
		super(httpClient);
	}

	@Override
	protected Iterable<Suggestion> searchInternal(String input, int limit) throws IOException, InterruptedException {
		Suggestion latLongSuggesstion = parseLatLongSuggestion(input);
		if (latLongSuggesstion != null)
			return List.of(latLongSuggesstion);
		Map<String, Object> urlParams = new LinkedHashMap<>();
		{
			urlParams.put("q", input);
			urlParams.put("appid", APP_ID_CACHE.get(Nada.get(), nil -> Utils.Functions.unchecked(() -> loadAppId())));
			urlParams.put("structuredaddress", true);
			if (limit != -1)
				urlParams.put("count", limit);
		}
		StringBuilder urlb = new StringBuilder("https://www.bing.com/api/v6/Places/AutoSuggest");
		var index = -1;
		for (var ent : urlParams.entrySet()) {
			var value = ent.getValue();
			if (value == null)
				continue;
			var valueStr = Utils.Types.tryCast(value, String.class).orElseGet(() -> value.toString());
			if (Utils.Strings.isBlank(valueStr))
				continue;
			index++;
			if (index == 0)
				urlb.append("?");
			else
				urlb.append("&");
			urlb.append(String.format("%s=%s", URLEncoder.encode(ent.getKey(), Utils.Bits.getDefaultCharset()),
					URLEncoder.encode(valueStr, Utils.Bits.getDefaultCharset())));
		}
		Suggestions searchResults = execute(URI.create(urlb.toString()),
				is -> Serials.Gsons.fromStream(is, Suggestions.class));
		if (searchResults == null)
			return StreamEx.empty();
		return searchResults.streamValues();
	}

	@Override
	protected StreamEx<Location> mapToLocationInternal(StreamEx<Suggestion> searchResults)
			throws IOException, InterruptedException {
		var result = searchResults.map(v -> Utils.Functions.unchecked(() -> toLocation(v)));
		return result;
	}

	private String loadAppId() throws IOException, InterruptedException {
		var uri = URI.create("https://maps.bing.com/");
		File file = Utils.Files.tempFile(THIS_CLASS, "app_id", VERSION,
				Utils.Crypto.hashMD5(uri.toString()).encodeHex());
		Date modifiedAt = !file.exists() ? null : new Date(file.lastModified());
		if (modifiedAt != null) {
			long elapsed = System.currentTimeMillis() - modifiedAt.getTime();
			if (elapsed > AUTO_SUGGEST_APP_ID_REFRESH.toMillis())
				file.delete();
		}
		if (file.exists()) {
			var appId = Utils.Files.readFileToString(file);
			if (Utils.Strings.isNotBlank(appId))
				return appId;
		}
		file.delete();
		String appId = execute(uri, is -> {
			return parseAppId(is);
		});
		Files.write(file.toPath(), appId.getBytes(Utils.Bits.getDefaultCharset()));
		return appId;
	}

	private static Suggestion parseLatLongSuggestion(String input) {
		int splitAt = Utils.Strings.indexOf(input, ",");
		if (splitAt < 0)
			return null;
		var pre = Utils.Strings.trim(input.substring(0, splitAt));
		var post = Utils.Strings.trim(input.substring(splitAt + 1, input.length() - 1));
		var lat = Utils.Strings.parseNumber(pre).map(Number::doubleValue).orElse(null);
		var lon = Utils.Strings.parseNumber(post).map(Number::doubleValue).orElse(null);
		if (lat == null || lon == null)
			return null;
		var suggestionPlace = new SuggestionPlace();
		var geo = new Geo();
		geo.setLatitude(lat);
		geo.setLongitude(lon);
		suggestionPlace.setGeo(geo);
		return suggestionPlace;
	}

	@SuppressWarnings("resource")
	private static String parseAppId(InputStream inputStream) throws IOException {
		try (var br = new BufferedReader(new InputStreamReader(inputStream, Utils.Bits.getDefaultCharset()),
				Utils.Bits.getDefaultBufferSize())) {
			StringBuilder buffer = null;
			for (var line : Utils.Lots.stream(br.lines())) {
				if (buffer == null && Utils.Strings.contains(line, AUTO_SUGGEST_APP_ID_DETECT))
					buffer = new StringBuilder();
				if (buffer == null)
					continue;
				buffer = buffer.append(line);
				var matcher = AUTO_SUGGEST_APP_ID_PATTERN.matcher(buffer);
				while (matcher.find()) {
					var group = matcher.group();
					var appId = Utils.Strings.substringBeforeLast(group, "\"");
					appId = Utils.Strings.substringAfterLast(appId, "\"");
					if (Utils.Strings.isNotBlank(appId))
						return appId;
				}
			}
		}
		throw new IOException("unable to detect appId");
	}

	private Location toLocation(Suggestion suggestion) throws IOException, InterruptedException {
		if (suggestion == null)
			return null;
		Address address = suggestion.getAddress();
		Geo geo = suggestion.getGeo();
		if (address == null)
			address = getAddress(geo);
		if (geo == null)
			geo = getGeo(address);
		if (address == null && geo == null)
			return null;
		var meta = Location.meta();
		var locationBuilder = Location.builder();
		if (geo != null)
			locationBuilder.latitude(geo.getLatitude()).longitude(geo.getLongitude());
		var streetAddress = address.getStreetAddress();
		if (Utils.Strings.isNotBlank(streetAddress)) {
			int splitAt = -1;
			for (int i = 0; splitAt < 0 && i < streetAddress.length(); i++) {
				var charAt = streetAddress.charAt(i);
				if (Character.isWhitespace(charAt))
					splitAt = i;
			}
			Long number = null;
			if (splitAt >= 0) {
				var numberStr = streetAddress.substring(0, splitAt);
				var numberOp = Utils.Strings.parseNumber(numberStr).map(Number::doubleValue).filter(v -> v % 1 == 0);
				if (numberOp.isPresent()) {
					number = numberOp.get().longValue();
					streetAddress = Utils.Strings.trimToNull(streetAddress.substring(splitAt));
				}
			}
			locationBuilder.number(number);
			locationBuilder.street(streetAddress);
		}
		locationBuilder.set(meta.locality(), address.getAddressLocality());
		locationBuilder.set(meta.subRegion(), address.getAddressSubregion());
		locationBuilder.set(meta.region(), address.getAddressRegion());
		locationBuilder.set(meta.postalCode(), address.getPostalCode());
		locationBuilder.set(meta.text(), address.getText());
		var cc = Utils.Lots.stream(address.getCountryIso(), address.getAddressCountry())
				.map(v -> Locations.parseCountryCode(v).orElse(null))
				.nonNull()
				.findFirst()
				.orElse(null);
		locationBuilder.countryCode(cc);

		return locationBuilder.build();
	}

	private Geo getGeo(Address address) throws IOException, InterruptedException {
		if (address == null)
			return null;
		var text = address.getText();
		if (Utils.Strings.isBlank(text))
			return null;
		var je = getQueryAttributeJson(text, "data-entity");
		Function<String, Double> degreeGetter = key -> {
			return Serials.Gsons.tryGetAsNumber(je, "routablePoint", key).map(Number::doubleValue).orElse(null);
		};
		var latitude = degreeGetter.apply("latitude");
		var longitude = degreeGetter.apply("longitude");
		if (latitude == null || longitude == null)
			return null;
		Geo geo = new Geo();
		geo.setLatitude(latitude);
		geo.setLongitude(longitude);
		return geo;
	}

	private Address getAddress(Geo geo) throws IOException, InterruptedException {
		if (geo == null)
			return null;
		var je = getQueryAttributeJson(String.format("%s,%s", geo.getLatitude(), geo.getLongitude()), "data-entity");
		var text = Serials.Gsons.tryGetAsString(je, "entity", "address").orElse(null);
		if (Utils.Strings.isBlank(text))
			return null;
		var suggestion = search(text, 1).findFirst().orElse(null);
		if (suggestion == null)
			return null;
		return suggestion.getAddress();
	}

	private JsonElement getQueryAttributeJson(String query, String attribute) throws IOException, InterruptedException {
		attribute = Utils.Strings.stripEnd(attribute, "=");
		if (Utils.Strings.isBlank(query) || Utils.Strings.isBlank(attribute))
			return null;
		var token = attribute + "=";
		String url = "https://www.bing.com/maps/overlaybfpr?" + queryEncode(Map.of("q", query));
		String json = execute(URI.create(url), is -> {
			var lineStream = Utils.Strings.streamLines(is);
			StringBuilder buffer = null;
			for (var line : lineStream) {
				if (buffer == null) {
					int index = Utils.Strings.indexOf(line, token);
					if (index < 0)
						continue;
					line = line.substring(index + token.length(), line.length());
					while (line.length() > 0 && Character.isWhitespace(line.charAt(0)))
						line = line.substring(1);
					if (Utils.Strings.startsWith(line, "\"")) {
						line = line.substring(1);
						buffer = new StringBuilder(line);
					}
				}
				if (buffer != null) {
					int index = Utils.Strings.indexOf(line, "\"");
					if (index >= 0)
						line = line.substring(0, index);
					buffer.append(line);
					if (index >= 0)
						return StringEscapeUtils.unescapeHtml4(buffer.toString());
				}
			}
			return null;
		});
		var boundaries = Utils.Strings.getBoundaries(json, '{', '}');
		if (boundaries == null)
			return null;
		json = json.substring(boundaries.getKey(), boundaries.getValue() + 1);
		if (Utils.Strings.isBlank(json))
			return null;
		return Serials.Gsons.getJsonParser().parse(json);
	}

	private <X> X execute(URI uri, ThrowingFunction<InputStream, X, IOException> responseHandler)
			throws IOException, InterruptedException {
		var rb = HttpRequest.newBuilder().uri(uri).GET();
		rb = rb.setHeader("User-Agent", Configs.get(LocationConfig.class).searchUserAgent());
		for (var ent : REQUEST_HEADERS.entrySet())
			rb = rb.setHeader(ent.getKey(), ent.getValue());
		var response = this.getHttpClient().apply(rb.build());
		try (var body = response.body()) {
			if (response.statusCode() / 100 != 2)
				throw new IOException("invalid status code:" + response);
			boolean force = false;
			if (!force && !MachineConfig.isDeveloper())
				return responseHandler.apply(response.body());
			var bodyBarr = response.body().readAllBytes();
			try (var is = new ByteArrayInputStream(bodyBarr)) {
				return responseHandler.apply(is);
			} catch (Throwable t) {
				logger.warn("response error. uri:{} response:{} body:\n{}", uri, response,
						new String(bodyBarr, MachineConfig.getDefaultCharset()));
				throw t;
			}
		}
	}

	public static void main(String[] args) throws IOException {
		var client = new BingMapsLocationSearchService();
		for (int i = 0; i < 1; i++) {
			var addr = client.searchLocations("1135 parson curry rd malvern pa").toList();
			System.out.println(Serials.Gsons.getPretty().toJson(addr));
		}
	}

}
