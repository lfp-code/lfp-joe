package com.lfp.joe.location;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

import org.apache.commons.lang3.StringUtils;

import com.lfp.joe.location.search.LocationService;
import com.lfp.joe.location.search.bing.BingMapsLocationSearchService;
import com.lfp.joe.location.search.mapquest.MapQuestLocationSearchService;
import com.lfp.joe.location.search.nominatim.NominatimLocationSearchService;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;
import com.neovisionaries.i18n.CountryCode;

import one.util.streamex.StreamEx;

public class Locations {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static Optional<CountryCode> parseCountryCode(String input) {
		if (Utils.Strings.isBlank(input))
			return Optional.empty();
		input = Utils.Strings.trim(input);
		input = Utils.Strings.upperCase(input);
		{
			var cc = CountryCode.getByCode(input);
			if (CountryCode.UNDEFINED.equals(cc))
				cc = null;
			if (cc != null)
				return Optional.of(cc);
		}
		if (input.length() <= 3)
			return Optional.empty();
		CountryCode closest = null;
		for (var cc : CountryCode.values()) {
			var countryName = cc.getName();
			if (!Utils.Strings.startsWithIgnoreCase(countryName, input))
				continue;
			if (closest == null)
				closest = cc;
			else if (closest.getName().length() > countryName.length())
				closest = cc;
		}
		return Optional.ofNullable(closest);
	}

	public static Optional<Locale> parseLocale(String input) {
		var cc = parseCountryCode(input).orElse(null);
		return toLocale(cc);
	}

	public static Optional<CountryCode> toCountryCode(Locale locale) {
		if (locale == null)
			return Optional.empty();
		return parseCountryCode(locale.getCountry());
	}

	public static Optional<Locale> toLocale(CountryCode countryCode) {
		if (countryCode == null)
			return Optional.empty();
		var alpha2 = countryCode.getAlpha2();
		var countryName = countryCode.getName();
		Predicate<Locale> localeTest = l -> {
			if (l == null)
				return false;
			if (Utils.Strings.equalsIgnoreCase(alpha2, l.getCountry()))
				return true;
			if (Utils.Strings.equalsIgnoreCase(countryName, l.getDisplayCountry()))
				return true;
			return false;
		};
		Comparator<Locale> comparator = (v1, v2) -> 0;
		comparator = comparator.thenComparing(v -> {
			return v.toString().length();
		});
		comparator = comparator.thenComparing(v -> {
			return Utils.Strings.equalsIgnoreCase(v.getLanguage(), "en") ? 0 : 1;
		});
		Locale winner = Utils.Lots.stream(Locale.getAvailableLocales()).filter(localeTest).sorted(comparator)
				.findFirst().orElse(null);
		if (winner != null)
			return Optional.of(winner);
		if (Utils.Strings.equalsIgnoreCase(alpha2, "uk"))
			return Optional.of(Locale.UK);
		else if (StringUtils.equalsIgnoreCase(alpha2, "kh"))
			return Optional.of(new Locale("km", "KH"));
		return Optional.empty();
	}

	public static StreamEx<Location> searchLocations(String input) {
		if (input == null)
			return StreamEx.empty();
		input = Utils.Strings.trim(input);
		if (Utils.Strings.isBlank(input))
			return StreamEx.empty();
		List<Supplier<? extends LocationService<?>>> serviceSuppliers = new ArrayList<>();
		serviceSuppliers.add(() -> NominatimLocationSearchService.getDefault());
		serviceSuppliers.add(() -> BingMapsLocationSearchService.getDefault());
		serviceSuppliers.add(() -> MapQuestLocationSearchService.getDefault());
		return searchLocations(input, serviceSuppliers);
	}

	private static StreamEx<Location> searchLocations(String input,
			List<Supplier<? extends LocationService<?>>> serviceSuppliers) {
		var locationStream = Streams.of(serviceSuppliers).map(Supplier::get).nonNull()
				.map(v -> searchLocations(input, v)).chain(Streams.flatMap());
		locationStream = locationStream.nonNull().distinct();
		return locationStream;
	}

	private static StreamEx<Location> searchLocations(String input, LocationService<?> service) {
		var producer = new Predicate<Consumer<? super Location>>() {

			private StreamEx<Location> locationStream;
			private Iterator<Location> locationIterator;

			@Override
			public boolean test(Consumer<? super Location> action) {
				try {
					if (locationIterator == null) {
						locationStream = service.searchLocations(input);
						locationIterator = locationStream.iterator();
					}
					if (locationIterator.hasNext()) {
						action.accept(locationIterator.next());
						return true;
					}
				} catch (Exception e) {
					String msg = String.format("address search failed. service:%s input:%s", service, input);
					logger.warn(msg, e);
				}
				Streams.close(locationStream);
				return false;
			}
		};
		return Streams.produce(producer).chain(Streams.onComplete(() -> {
			Streams.close(producer.locationStream);
		}));
	}

	public static void main(String[] args) {
		var addrs = Locations.searchLocations("1045 1st Avenue, King of Prussia PA, 19406").toList();
		System.out.println(Serials.Gsons.getPretty().toJson(addrs));
		for (var addr : addrs)
			System.out.println(addr.getText());
	}

}
