package com.lfp.joe.location.search.bing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Address {

	@SerializedName("streetAddress")
	@Expose
	private String streetAddress;
	@SerializedName("addressLocality")
	@Expose
	private String addressLocality;
	@SerializedName("addressSubregion")
	@Expose
	private String addressSubregion;
	@SerializedName("addressRegion")
	@Expose
	private String addressRegion;
	@SerializedName("postalCode")
	@Expose
	private String postalCode;
	@SerializedName("addressCountry")
	@Expose
	private String addressCountry;
	@SerializedName("countryIso")
	@Expose
	private String countryIso;
	@SerializedName("text")
	@Expose
	private String text;

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getAddressLocality() {
		return addressLocality;
	}

	public void setAddressLocality(String addressLocality) {
		this.addressLocality = addressLocality;
	}

	public String getAddressSubregion() {
		return addressSubregion;
	}

	public void setAddressSubregion(String addressSubregion) {
		this.addressSubregion = addressSubregion;
	}

	public String getAddressRegion() {
		return addressRegion;
	}

	public void setAddressRegion(String addressRegion) {
		this.addressRegion = addressRegion;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getAddressCountry() {
		return addressCountry;
	}

	public void setAddressCountry(String addressCountry) {
		this.addressCountry = addressCountry;
	}

	public String getCountryIso() {
		return countryIso;
	}

	public void setCountryIso(String countryIso) {
		this.countryIso = countryIso;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
