package com.lfp.joe.properties.converter;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

import org.aeonbits.owner.Converter;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParseException;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

@SuppressWarnings("serial")
public class JsonConverter implements Converter<Object> {

	public static final MemoizedSupplier<JsonConverter> INSTANCE = Utils.Functions.memoize(() -> new JsonConverter());

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	@Override
	public Object convert(Method method, String input) {
		return convert(method, input, false);
	}

	public Object convert(Method method, String input, boolean disableWarnings) {
		if (Utils.Strings.isBlank(input))
			return null;
		Type genericReturnType = method.getGenericReturnType();
		if (genericReturnType == null || Utils.Types.isVoid(genericReturnType))
			return null;
		var gson = Serials.Gsons.get();
		Object result;
		try {
			if (JsonPropertyIterable.class.equals(method.getReturnType()))
				result = deserializeJsonPropertyIterable(gson, genericReturnType, input);
			else
				result = gson.fromJson(input, genericReturnType);
		} catch (Exception e) {
			if (!disableWarnings)
				logger.warn("unable to parse input. type:{} json:{}", genericReturnType, input);
			result = null;
		}
		return result;
	}

	public static class JsonPropertyMap<K, V> extends LinkedHashMap<K, V> {

	}

	public static class JsonPropertyIterable<X> implements Iterable<X> {

		private final List<X> list;

		public JsonPropertyIterable(List<X> list) {
			super();
			this.list = Objects.requireNonNull(list);
		}

		@Override
		public Iterator<X> iterator() {
			return list.iterator();
		}

		@Override
		public String toString() {
			return list.toString();
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static <U> JsonPropertyIterable<U> deserializeJsonPropertyIterable(Gson gson, Type typeOfT, String input)
			throws JsonParseException {
		if (input == null || input.isBlank())
			return null;
		var jarr = gson.fromJson(input, JsonArray.class);
		if (jarr == null)
			return null;
		StreamEx<ParameterizedType> pTypeStream;
		if (typeOfT instanceof ParameterizedType)
			pTypeStream = StreamEx.of((ParameterizedType) typeOfT);
		else if (typeOfT instanceof Class) {
			var ctStream = CoreReflections.streamTypes((Class<?>) typeOfT);
			ctStream = ctStream.filter(JsonPropertyIterable.class::isAssignableFrom);
			ctStream = ctStream.filter(ParameterizedType.class::isAssignableFrom);
			pTypeStream = Streams.of(ctStream).map(ParameterizedType.class::cast);

		} else
			pTypeStream = StreamEx.empty();
		var ataStream = pTypeStream.map(ParameterizedType::getActualTypeArguments);
		ataStream = ataStream.nonNull();
		ataStream = ataStream.filter(v -> v.length == 1);
		Class<?> valueType = ataStream.map(v -> (Class<?>) v[0]).findFirst().orElseGet(() -> (Class) Object.class);
		var list = Streams.of(jarr).map(v -> (U) gson.fromJson(v, valueType)).toList();
		return new JsonPropertyIterable<U>(list);
	}

}
