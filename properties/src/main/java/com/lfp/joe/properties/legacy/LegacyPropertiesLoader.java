package com.lfp.joe.properties.legacy;

import java.io.IOException;
import java.lang.reflect.Method;
import java.time.Duration;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;

import org.aeonbits.owner.Config;

import com.bettercloud.vault.Vault;
import com.bettercloud.vault.VaultException;
import com.bettercloud.vault.response.LogicalResponse;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.lfp.joe.core.classpath.Instances.Autowired;
import com.lfp.joe.core.config.EnvironmentLevel;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.annotation.LocalOnly;
import com.lfp.joe.core.properties.loader.Loaders;
import com.lfp.joe.core.properties.loader.MultiLoader;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.vault.VaultClient;
import com.lfp.joe.vault.VaultConfig;

import at.favre.lib.bytes.Bytes;

@SuppressWarnings("deprecation")
@Autowired(priority = Integer.MIN_VALUE)
public enum LegacyPropertiesLoader implements MultiLoader.ClassTypeLoader {
	INSTANCE;

	private static final MemoizedSupplier<org.slf4j.Logger> LOGGER_S = MemoizedSupplier.create(() -> {
		Class<?> classType = new Object() {}.getClass().getEnclosingClass();
		return org.slf4j.LoggerFactory.getLogger(classType);
	});

	private final Duration LEGACY_PROPERTIES_CACHE_DURATION = Duration.ofSeconds(15);

	private final LoadingCache<Optional<Void>, Optional<Map<String, String>>> legacyPropertiesCache = Caffeine
			.newBuilder()
			.expireAfterWrite(LEGACY_PROPERTIES_CACHE_DURATION)
			.build(nil -> Optional.ofNullable(getLegacyPropertiesFresh()));

	private Map<String, String> getLegacyPropertiesFresh() throws IOException, VaultException {
		Optional<String> op = getValueEnvironmentLevelPath();
		if (!op.isPresent())
			return null;
		Vault client = VaultClient.getDefault().get();
		LogicalResponse response = client.logical()
				.read(op.get() + "/" + Configs.get(LegacyPropertiesConfig.class).secretPath());
		if (response.getRestResponse().getStatus() == 404)
			return null;
		if (response.getRestResponse().getStatus() / 100 != 2)
			throw new IOException(
					"invalida response from vault:" + Bytes.from(response.getRestResponse().getBody()).encodeUtf8());
		Map<String, String> data = response.getData();
		return data;
	}

	// return null if not enabled! not empty!
	public Map<String, String> getLegacyProperties() {
		return legacyPropertiesCache.get(Optional.empty()).orElse(null);
	}

	private Optional<String> getValueEnvironmentLevelPath() {
		EnvironmentLevel environmentLevel = Configs.get(MachineConfig.class).environmentLevel();
		VaultConfig vaultConfig = Configs.get(VaultConfig.class);
		if (vaultConfig == null || vaultConfig.address() == null) {
			LOGGER_S.get().warn("vault not active. disabling legacy properties config");
			return Optional.empty();
		}
		String path = vaultConfig.getPropertiesPath(environmentLevel);
		path = Utils.Strings.lowerCase(path);
		LogicalResponse response;
		try {
			response = VaultClient.getDefault().get().logical().list(path);
		} catch (VaultException e) {
			throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
					? java.lang.RuntimeException.class.cast(e)
					: new java.lang.RuntimeException(e);
		}
		final boolean responseOk;
		if (response.getRestResponse().getStatus() == 404)
			responseOk = true;
		else if (response.getRestResponse().getStatus() / 100 == 2)
			responseOk = true;
		else
			responseOk = false;
		if (!responseOk) {
			LOGGER_S.get()
					.warn("vault user '{}' not authorized on environment level:{}", vaultConfig.username(),
							environmentLevel);
			return Optional.empty();
		}
		return Optional.of(path);
	}

	@Override
	public void load(String instanceId, Properties result, Class<Config> classType,
			Iterable<Entry<Method, Set<String>>> publicNoArgMethodEntries) {
		if (classType.getAnnotation(LocalOnly.class) != null)
			return;
		if (LegacyPropertiesConfig.class.equals(classType))
			return;
		Map<String, String> map = getLegacyProperties();
		Loaders.putAll(result, publicNoArgMethodEntries, map);
	}

}
