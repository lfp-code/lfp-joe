package com.lfp.joe.properties.converter;

import java.lang.reflect.Method;
import java.util.Date;

import org.aeonbits.owner.Converter;

import com.lfp.joe.utils.time.TimeParser;

public class DateConverter implements Converter<Date> {

	@Override
	public Date convert(Method method, String input) {
		return TimeParser.instance().tryParseDate(input).orElse(null);
	}

}
