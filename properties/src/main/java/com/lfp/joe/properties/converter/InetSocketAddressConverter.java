package com.lfp.joe.properties.converter;

import java.lang.reflect.Method;
import java.net.InetSocketAddress;

import org.aeonbits.owner.Converter;

import com.lfp.joe.serial.gson.InetSocketAddressJsonSerializer;

public class InetSocketAddressConverter implements Converter<InetSocketAddress> {

	@Override
	public InetSocketAddress convert(Method method, String input) {
		return InetSocketAddressJsonSerializer.INSTANCE.fromString(input);
	}

}