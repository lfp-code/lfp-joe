package com.lfp.joe.properties.legacy;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.core.properties.annotation.LocalOnly;

@LocalOnly
@Deprecated
public interface LegacyPropertiesConfig extends Config {

	@DefaultValue("com.lfp.joe.properties.legacy.IMPORT")
	String secretPath();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.builder().withIncludeValues(false).withPrependClassNames(true)
				.withSkipPopulated(false).withJsonOutput(false).build());
	}
}
