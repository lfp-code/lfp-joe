package com.lfp.joe.properties.converter;

import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.Duration;

import org.aeonbits.owner.Converter;

import com.lfp.joe.utils.time.TimeParser;

public class DurationConverter implements Converter<Duration> {

	@Override
	public Duration convert(Method method, String input) {
		return TimeParser.instance().tryParseDuration(input).orElse(null);
	}

}