package test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.aeonbits.owner.lfp.OwnerUtilsLFP;

import com.bettercloud.vault.SslConfig;
import com.bettercloud.vault.Vault;
import com.bettercloud.vault.VaultConfig;
import com.bettercloud.vault.VaultException;
import com.bettercloud.vault.response.AuthResponse;
import com.bettercloud.vault.response.LogicalResponse;
import com.lfp.joe.serial.Serials;

public class Auth {
	public static void main(String[] args) throws VaultException {
		System.out.println(OwnerUtilsLFP.delayedExecutor(10, TimeUnit.SECONDS));
		java.net.ProxySelector.setDefault(new ProxySelector() {

			@Override
			public List<Proxy> select(URI uri) {
				Proxy p = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("localhost", 8888));
				return Arrays.asList(p);
			}

			@Override
			public void connectFailed(URI uri, SocketAddress sa, IOException ioe) {
				ioe.printStackTrace();

			}
		});
		VaultConfig config = new VaultConfig().sslConfig(new SslConfig().verify(false))
				.address("https://vault.lasso.tm").build();
		Vault vault = new Vault(config);
		AuthResponse loginByUserPass = vault.auth().loginByUserPass("testuser2", "testuser2");
		String token = loginByUserPass.getAuthClientToken();
		config = config.token(token).build();
		vault = new Vault(config);
		vault.auth().renewSelf();
		LogicalResponse data = vault.logical().read("/properties-dev/DatabaseConfig");
		System.out.println(Serials.Gsons.get().toJson(data.getData()));
	}
}
