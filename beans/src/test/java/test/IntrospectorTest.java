package test;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.lfp.joe.introspector.IntrospectorLFP;
import com.lfp.joe.introspector.PropertyDescriptorTreeNode;

public class IntrospectorTest {

	public static void main(String[] args) {
		var root = IntrospectorLFP.getBeanInfoTree(TestBean.class);
		var map = process(root);
		var gson = new GsonBuilder().setPrettyPrinting().registerTypeAdapterFactory(new TypeAdapterFactory() {

			@SuppressWarnings("unchecked")
			@Override
			public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
				if (!Class.class.isAssignableFrom(type.getRawType()))
					return null;
				var delegate = gson.getDelegateAdapter(this, TypeToken.get(String.class));
				return (TypeAdapter<T>) new TypeAdapter<Class>() {

					@Override
					public void write(JsonWriter out, Class value) throws IOException {
						delegate.write(out, Optional.ofNullable(value).map(v -> v.getName()).orElse(null));
					}

					@Override
					public Class read(JsonReader in) throws IOException {
						// TODO Auto-generated method stub
						return null;
					}
				};
			}
		}).create();
		System.out.println(gson.toJson(map));
	}

	private static Map<String, Object> process(PropertyDescriptorTreeNode node) {
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("name", node.getName());
		if (node.getPropertyType() != null)
			data.put("type", node.getPropertyType());
		var children = node.children().stream().map(v -> process(v)).collect(Collectors.toList());
		if (!children.isEmpty())
			data.put("children", children);
		return data;
	}
}
