package com.lfp.joe.beans.joda;

import java.io.File;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.commons.lang3.Validate;
import org.joda.beans.Bean;
import org.joda.beans.BeanBuilder;
import org.joda.beans.JodaBeanUtils;
import org.joda.beans.MetaBean;
import org.joda.beans.MetaProperty;
import org.joda.beans.PropertyDefinition;
import org.joda.beans.gen.BeanCodeGen;
import org.joda.beans.impl.direct.DirectFieldsBeanBuilder;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.throwable.beanref.BeanPath;
import com.github.throwable.beanref.BeanRef;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.properties.code.PropertyCode;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.DeepHashFunction;
import com.lfp.joe.utils.function.Requires;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public class JodaBeans {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	private static final Cache<MetaProperty<?>, Optional<Field>> META_PROPERTY_TO_FIELD_CACHE = Caffeine.newBuilder()
			.expireAfterAccess(Duration.ofSeconds(1))
			.build();
	private static final Cache<Class<? extends BeanBuilder<?>>, Function<BeanBuilder<?>, MetaBean>> BEAN_BUILDER_META_FUNCTION_CACHE = Caffeine
			.newBuilder()
			.expireAfterAccess(Duration.ofSeconds(1))
			.build();

	public static Optional<Field> getField(MetaProperty<?> metaProperty) {
		Objects.requireNonNull(metaProperty);
		return META_PROPERTY_TO_FIELD_CACHE.get(metaProperty, nil -> {
			String name = metaProperty.name();
			Class<?> declaringType = metaProperty.declaringType();
			Class<?> propertyType = metaProperty.propertyType();
			Field field = CoreReflections.streamFields(declaringType, true)
					.filter(f -> f.getName().equals(name))
					.filter(f -> propertyType.isAssignableFrom(f.getType()))
					.findFirst()
					.orElse(null);
			if (field == null)
				return Optional.empty();
			field.setAccessible(true);
			return Optional.of(field);
		});
	}

	@SuppressWarnings("unchecked")
	public static <B extends Bean, X> B set(B bean, X value, MetaProperty<?>... metaProperties) {
		Objects.requireNonNull(bean, "bean is required");
		{
			int i = 0;
			for (; i < metaProperties.length; i++) {
				Objects.requireNonNull(metaProperties[i], () -> {
					return "meta property null:" + Utils.Lots.stream(metaProperties).toList();
				});
			}
			Validate.isTrue(i > 0, "meta property required");
		}
		Function<Bean, BeanBuilder<? extends Bean>> beanBuilderGen = b -> {
			var builder = JodaBeanUtils.metaBean(b.getClass()).builder();
			JodaBeans.copyToBuilder(b, builder);
			return builder;
		};
		List<Entry<BeanBuilder<? extends Bean>, MetaProperty<?>>> builderChain = new ArrayList<>();
		BeanBuilder<? extends Bean> currentBeanBuilder = beanBuilderGen.apply(bean);
		var mpIter = Arrays.asList(metaProperties).iterator();
		while (mpIter.hasNext()) {
			var mp = mpIter.next();
			builderChain.add(Utils.Lots.entry(currentBeanBuilder, mp));
			if (!mpIter.hasNext())
				break;
			var nextObj = currentBeanBuilder.get(mp);
			Validate.isTrue(nextObj == null || nextObj instanceof Bean, "meta property did not produce bean:%s", mp);
			if (nextObj != null)
				currentBeanBuilder = beanBuilderGen.apply((Bean) nextObj);
			else
				currentBeanBuilder = JodaBeanUtils.metaBean(mp.propertyType()).builder();
		}
		Object currentValue = value;
		while (!builderChain.isEmpty()) {
			var lastEntry = builderChain.remove(builderChain.size() - 1);
			lastEntry.getKey().set(lastEntry.getValue(), currentValue);
			currentValue = lastEntry.getKey().build();
		}
		return (B) currentValue;
	}

	public static <B extends Bean, X> void setUnchecked(B bean, MetaProperty<X> metaProperty, X value) {
		try {
			getField(metaProperty).get().set(bean, value);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
					? java.lang.RuntimeException.class.cast(e)
					: new java.lang.RuntimeException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public static <X, B extends BeanBuilder<? extends Bean>> X get(B builder, MetaProperty<X> metaProperty) {
		Objects.requireNonNull(builder);
		var value = builder.get(metaProperty);
		return (X) value;
	}

	@SuppressWarnings("unchecked")
	public static Optional<MetaBean> getMetaBean(BeanBuilder<?> beanBuilder) {
		if (beanBuilder == null)
			return Optional.empty();
		if (beanBuilder instanceof DirectFieldsBeanBuilder) {
			var declaringClass = beanBuilder.getClass().getDeclaringClass();
			if (declaringClass != null && Bean.class.isAssignableFrom(declaringClass)) {
				var mb = Utils.Functions.catching(() -> JodaBeanUtils.metaBean(declaringClass), t -> null);
				if (mb != null)
					return Optional.of(mb);
			}
		}
		var func = BEAN_BUILDER_META_FUNCTION_CACHE.get((Class<? extends BeanBuilder<?>>) beanBuilder.getClass(),
				ct -> {
					for (String fieldName : Arrays.asList(null, "meta")) {
						Predicate<Field> fieldPredicate = f -> MetaBean.class.isAssignableFrom(f.getType());
						if (fieldName != null)
							fieldPredicate = fieldPredicate.and(f -> fieldName.equals(f.getName()));
						var fieldList = CoreReflections.streamFields(ct, true)
								.filter(fieldPredicate)
								.limit(2)
								.collect(Collectors.toList());
						if (fieldList.size() != 1)
							continue;
						var field = fieldList.get(0);
						field.setAccessible(true);
						return b -> (MetaBean) Throws.unchecked(() -> field.get(b));
					}
					for (String fieldName : Arrays.asList(null, "bean")) {
						Predicate<Field> fieldPredicate = f -> Bean.class.isAssignableFrom(f.getType());
						if (fieldName != null)
							fieldPredicate = fieldPredicate.and(f -> fieldName.equals(f.getName()));
						var fieldList = CoreReflections.streamFields(ct, true)
								.filter(fieldPredicate)
								.limit(2)
								.collect(Collectors.toList());
						if (fieldList.size() != 1)
							continue;
						var field = fieldList.get(0);
						field.setAccessible(true);
						return b -> {
							var bean = (Bean) Throws.unchecked(() -> field.get(b));
							return bean == null ? null : bean.metaBean();
						};
					}
					return Utils.Functions.doNothingFunction();
				});
		if (func == null)
			return Optional.empty();
		MetaBean mb = func.apply(beanBuilder);
		return Optional.ofNullable(mb);
	}

	public static Optional<MetaProperty<?>> getMetaProperty(Class<? extends Bean> classType, String nameOrAlias) {
		return getMetaProperty(classType == null ? null : JodaBeanUtils.metaBean(classType), nameOrAlias);
	}

	public static Optional<MetaProperty<?>> getMetaProperty(MetaBean metaBean, String nameOrAlias) {
		Objects.requireNonNull(metaBean);
		if (Utils.Strings.isBlank(nameOrAlias))
			return Optional.empty();
		MetaProperty<?> directMp = Utils.Functions.catching(() -> {
			if (metaBean.metaPropertyExists(nameOrAlias))
				return metaBean.metaProperty(nameOrAlias);
			return null;
		}, t -> null);
		if (directMp != null)
			return Optional.of(directMp);
		var mpStream = Utils.Lots.stream(metaBean.metaPropertyIterable());
		mpStream = mpStream.filter(mp -> {
			if (nameOrAlias.equals(mp.name()))
				return true;
			try (var annoStream = Utils.Lots.stream(mp.annotations()).select(PropertyDefinition.class)) {
				for (var anno : annoStream) {
					var alias = anno.alias();
					if (nameOrAlias.equals(alias))
						return true;
				}
			}
			return false;
		});
		return mpStream.findFirst();
	}

	public static StreamEx<MetaProperty<?>> streamMetaProperties(Class<?> classType) {
		return streamMetaProperties(classType, false);
	}

	public static StreamEx<MetaProperty<?>> streamMetaProperties(Class<?> classType, boolean scanNested) {
		if (classType == null || !Bean.class.isAssignableFrom(classType))
			return StreamEx.empty();
		var metaBean = JodaBeanUtils.metaBean(classType);
		var streams = Utils.Lots.stream(metaBean.metaPropertyIterable()).map(mp -> {
			StreamEx<MetaProperty<?>> stream = StreamEx.of(mp);
			if (scanNested)
				stream = stream.append(streamMetaProperties(mp.propertyType(), scanNested));
			return stream;
		});
		return Utils.Lots.flatMap(streams);
	}

	public static EntryStream<String, MetaProperty<?>> streamMetaPropertyEntries(Class<? extends Bean> classType) {
		return streamMetaPropertyEntries(classType, false);
	}

	public static EntryStream<String, MetaProperty<?>> streamMetaPropertyEntries(Class<? extends Bean> classType,
			boolean firstOnly) {
		return streamMetaPropertyEntries(classType, firstOnly, false);
	}

	public static EntryStream<String, MetaProperty<?>> streamMetaPropertyEntries(Class<? extends Bean> classType,
			boolean firstOnly, boolean scanNested) {
		StreamEx<MetaProperty<?>> mpStream = streamMetaProperties(classType, scanNested);
		return mpStream.map(mp -> {
			StreamEx<String> nameStream = streamNames(mp);
			if (firstOnly)
				nameStream = nameStream.limit(1);
			StreamEx<Entry<String, MetaProperty<?>>> estream = nameStream.map(v -> Utils.Lots.entry(v, mp));
			return estream;
		}).chain(Utils.Lots::flatMapEntries);
	}

	public static StreamEx<String> streamNames(MetaProperty<?> metaProperty) {
		if (metaProperty == null)
			return StreamEx.empty();
		StreamEx<String> nameStream = StreamEx.empty();
		{
			StreamEx<String> append = StreamEx.produce(action -> {
				var annos = metaProperty.annotations();
				if (annos != null)
					for (var anno : annos)
						if (anno instanceof PropertyDefinition) {
							var alias = ((PropertyDefinition) anno).alias();
							action.accept(alias);
						}
				return false;
			});
			nameStream = nameStream.append(append);
		}
		nameStream = nameStream.append(metaProperty.name());
		nameStream = nameStream.filter(Utils.Strings::isNotBlank);
		nameStream = nameStream.distinct();
		return nameStream;
	}

	public static <X, B extends BeanBuilder<? extends X>> B copyToBuilder(
			Iterable<? extends Entry<String, Object>> entryIterable, B builder) {
		Objects.requireNonNull(builder);
		var metaBean = Requires.nonNull(getMetaBean(builder).orElse(null),
				fc -> fc.exceptionArguments("unable to get metaBean fromBuilder:%s", builder));
		EntryStream<String, Object> estream = Utils.Lots.streamEntries(entryIterable);
		estream = estream.nonNull();
		estream.forEach(ent -> {
			var value = ent.getValue();
			var mp = getMetaProperty(metaBean, ent.getKey()).orElse(null);
			if (mp == null)
				return;
			builder.set(mp, value);
		});
		return builder;
	}

	public static <X, B extends BeanBuilder<? extends X>> B copyToBuilder(X bean, B builder) {
		return copyToBuilder(bean, builder, (propName, value) -> value != null);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <X, B extends BeanBuilder<? extends X>> B copyToBuilder(X bean, B builder,
			BiPredicate<String, Supplier<Object>> copyFilter) {
		Objects.requireNonNull(builder);
		if (bean == null)
			return builder;
		Set<String> props = new HashSet<>();
		if (bean instanceof Bean) {
			var metaBean = ((Bean) bean).metaBean();
			StreamEx<MetaProperty<?>> mpStream = Utils.Lots.stream(metaBean.metaPropertyIterable());
			mpStream = mpStream.filter(mp -> props.add(mp.name()));
			for (MetaProperty<?> mp : mpStream) {
				Supplier<Object> valueSupplier = new MemoizedSupplier.Impl<>(() -> {
					return mp.get((Bean) bean);
				});
				if (!copyFilter.test(mp.name(), valueSupplier))
					continue;
				Utils.Functions.catching(() -> builder.set(mp, valueSupplier.get()), t -> {
					if (t instanceof java.util.NoSuchElementException)
						return;
					throw Utils.Exceptions.asRuntimeException(t);
				});
			}
		}
		StreamEx<BeanPath<X, ?>> bpStream = Utils.Lots.stream((Set) BeanRef.$(bean.getClass()).all());
		bpStream = bpStream.filter(v -> props.add(v.getPath()));
		for (BeanPath<X, ?> bp : bpStream) {
			Supplier<Object> valueSupplier = new MemoizedSupplier.Impl<>(() -> {
				return bp.get(bean);
			});
			if (!copyFilter.test(bp.getPath(), valueSupplier))
				continue;
			Utils.Functions.catching(() -> builder.set(bp.getPath(), valueSupplier.get()), t -> {
				if (t instanceof java.util.NoSuchElementException)
					return;
				throw Utils.Exceptions.asRuntimeException(t);
			});
		}
		return builder;
	}

	public static Bytes hashMD5(Bean bean) {
		return hash(Utils.Crypto.getMessageDigestMD5(), bean);
	}

	public static Bytes hash(MessageDigest messageDigest, Bean bean) {
		return hash(messageDigest, bean, null);
	}

	public static Bytes hash(MessageDigest messageDigest, Bean bean,
			BiFunction<MetaProperty<?>, Object, Object> valueModifier) {
		return hash(messageDigest, bean, null, valueModifier);
	}

	public static Bytes hash(MessageDigest messageDigest, Bean bean, Predicate<MetaProperty<?>> propertyFilter,
			BiFunction<MetaProperty<?>, Object, Object> valueModifier) {
		Objects.requireNonNull(messageDigest);
		Objects.requireNonNull(bean);
		MetaBean metaBean = Objects.requireNonNull(JodaBeanUtils.metaBean(bean.getClass()));
		for (MetaProperty<?> mp : metaBean.metaPropertyIterable()) {
			if (propertyFilter != null && !propertyFilter.test(mp))
				continue;
			Utils.Crypto.update(messageDigest, mp.name());
			Object value = mp.get(bean);
			if (valueModifier != null)
				value = valueModifier.apply(mp, value);
			DeepHashFunction.INSTANCE.accept(messageDigest, value);
		}
		return Bytes.from(messageDigest.digest());
	}

	public static String toString(Object value) {
		if (!(value instanceof Bean))
			return JodaBeanUtils.toString(value);
		StringBuilder builder = new StringBuilder();
		var metaBean = ((Bean) value).metaBean();
		for (var mp : metaBean.metaPropertyIterable()) {
			var mpValue = mp.get(((Bean) value));
			if (mpValue == null)
				continue;
			if (builder.length() > 0)
				builder.append(", ");
			builder.append(String.format("%s=%s", mp.name(), toString(mpValue)));
		}
		return String.format("%s{%s}", value.getClass().getSimpleName(), builder);
	}

	public static void updateCode(Class<? extends Bean> beanClassType) {
		File beanClassFile = null;
		if (beanClassType != null) {
			for (int i = 0; i < 2 && beanClassFile == null; i++) {
				var filePath = new File("").getAbsolutePath() + (i == 0 ? "/src/main/java" : "/src/test/java");
				String name = beanClassType.getName();
				for (String chunk : name.split("\\.")) {
					filePath += "/" + chunk;
				}
				filePath += ".java";
				var file = new File(filePath);
				if (file.exists())
					beanClassFile = file;
			}
		}
		if (beanClassFile == null) {
			String workingDir = System.getProperty("user.dir");
			beanClassFile = new File(workingDir);
		}
		updateCode(beanClassFile, false);
	}

	public static void updateCode(File beanClassFile) {
		updateCode(beanClassFile, false);
	}

	public static void updateCode(File beanClassFile, boolean disableExitOnComplete) {
		Objects.requireNonNull(beanClassFile);
		String config = "-config=jdk";
		try {
			Class<?> classType = Class.forName("com.google.common.collect.ImmutableList", false,
					Thread.currentThread().getContextClassLoader());
			if (classType != null)
				config = "-config=guava";
		} catch (Exception e) {
			logger.info("guava not detected on classpath, using jdk");
		}
		var args = new String[] { "-R", config, beanClassFile.getAbsolutePath() };
		if (disableExitOnComplete) {
			var beanCodeGen = BeanCodeGen.createFromArgs(args);
			Utils.Functions.unchecked(() -> beanCodeGen.process());
		} else
			BeanCodeGen.main(args);
	}

	@SuppressWarnings("unchecked")
	public static void updateCode() {
		Class<?> callingClass = CoreReflections.getCallingClass(THIS_CLASS);
		updateCode((Class<? extends Bean>) callingClass);
	}

	public static void printFields(boolean finalModifier, String propertyDefinitionAttributes,
			PropertyCode<?>... propertyCodes) {
		printFields(finalModifier, propertyDefinitionAttributes,
				propertyCodes == null ? null : Arrays.asList(propertyCodes));
	}

	public static void printFields(boolean finalModifier, String propertyDefinitionAttributes,
			Iterable<? extends PropertyCode<?>> propertyCodes) {
		if (propertyDefinitionAttributes == null) {
			printFields(finalModifier, "", propertyCodes);
			return;
		}
		if (propertyCodes == null)
			return;
		var propertyCodeStream = StreamSupport.stream(propertyCodes.spliterator(), false);
		propertyCodeStream = propertyCodeStream.filter(Objects::nonNull);
		propertyCodeStream = propertyCodeStream.sorted(Comparator.comparing(pc -> {
			return pc.getMethodName();
		}));
		List<String> lines = new ArrayList<>();
		propertyCodeStream.forEach(pc -> {
			var name = pc.getName();
			String propertyDefinition = "@PropertyDefinition";
			var pdAttributes = propertyDefinitionAttributes;
			var key = pc.getKey();
			if (!name.equals(key))
				pdAttributes += String.format(" alias=\"%s\"", key);
			if (Utils.Strings.isNotBlank(pdAttributes))
				propertyDefinition += String.format("(%s)", pdAttributes.trim());
			lines.add(propertyDefinition);
			lines.add(String.format("private%s %s %s;", finalModifier ? " final" : "", pc.getType().getSimpleName(),
					name));
			lines.add("");
		});
		var out = lines.stream().collect(Collectors.joining("\n"));
		if (!out.isBlank())
			out += "\n";
		System.out.println(out);
	}

}
