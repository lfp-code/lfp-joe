package com.lfp.joe.beans.joda;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;
import java.util.stream.Stream;

import org.joda.beans.Bean;
import org.joda.beans.BeanBuilder;
import org.joda.beans.MetaBean;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.MethodRequest;
import com.lfp.joe.core.classpath.Instances.Autowired;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.introspector.PropertyDescriptor;
import com.lfp.joe.introspector.PropertyDescriptorTreeFilter;
import com.lfp.joe.introspector.PropertyDescriptorTreeNode;

@Autowired
public enum JodaPropertyDescriptorTreeFilter implements PropertyDescriptorTreeFilter {
	INSTANCE;

	@Override
	public boolean test(PropertyDescriptorTreeNode parentNode, PropertyDescriptor propertyDescriptor) {
		Iterator<Method> methodIter;
		{
			var readMethod = propertyDescriptor.getReadMethod();
			var methodStream = CoreReflections.streamTypes(readMethod.getDeclaringClass())
					.filter(ct -> !ct.equals(readMethod.getDeclaringClass()))
					.map(ct -> {
						var method = MemberCache.tryGetMethod(MethodRequest.of(ct, readMethod.getReturnType(),
								readMethod.getName(), readMethod.getParameterTypes()), true).orElse(null);
						return method;
					});
			methodStream = Stream.concat(Stream.of(readMethod), methodStream);
			methodStream = methodStream.filter(Objects::nonNull);
			methodIter = methodStream.iterator();
		}
		while (methodIter.hasNext()) {
			var method = methodIter.next();
			for (var ct : Arrays.asList(method.getReturnType(), method.getDeclaringClass())) {
				if (CoreReflections.isAssignableFrom(MetaBean.class, ct))
					return false;
				if (CoreReflections.isAssignableFrom(BeanBuilder.class, ct))
					return false;
				if (Bean.class.equals(ct))
					return false;
			}
		}
		return true;
	}
}
