package test;

import java.util.Date;
import java.util.function.Consumer;

import com.lfp.joe.events.CentralBus;

public class CentralBusTest {

	public static void main(String[] args) throws InterruptedException {
		CentralBus.INSTANCE.subscribe((Consumer<Date>) d -> {
			System.out.println("date:" + d);
		});
		CentralBus.INSTANCE.subscribe(String.class, v -> {
			System.out.println("string:" + v);
		});
		CentralBus.INSTANCE.publish("hello");
		CentralBus.INSTANCE.publish(new Date());
		Thread.sleep(10 * 1000);
	}
}
