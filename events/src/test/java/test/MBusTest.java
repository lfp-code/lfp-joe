package test;

import java.util.Date;
import java.util.Objects;
import java.util.function.Consumer;

import com.lfp.joe.events.bobbus.BobBus;

public class MBusTest {

	public static void main(String[] args) {
		BobBus<Date> bus = new BobBus<Date>();
		var listener1 = bus.subscribe(d -> {
			System.out.println("event consumer 1: " + d);
		});
		Consumer<Date> listener2 = d -> {
			System.out.println("event consumer 2: " + d);
		};
		bus.subscribe(listener2);

		Consumer<Date> listener3 = new Consumer<Date>() {

			@Override
			public void accept(Date t) {
				System.out.println("event consumer 3: " + t);
				throw new RuntimeException(Objects.toString(t));
			}
		};
		bus.subscribe(listener3);
		System.out.println("should print 1 & 2 & 3");
		bus.post(new Date()).now();
		bus.unsubscribe(listener1);
		System.out.println("should print 2");
		bus.post(new Date()).now();
		bus.unsubscribe(listener2);
		System.out.println("should print none");
		bus.post(new Date()).asynchronously();
		System.err.println("done");
	}

}
