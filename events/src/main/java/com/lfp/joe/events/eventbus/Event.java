package com.lfp.joe.events.eventbus;

import java.util.Objects;

public interface Event<X> {

	X value();

	Registration registration();

	public static class Impl<X> implements Event<X> {

		private final X value;
		private final Registration registration;

		public Impl(X value, Registration registration) {
			this.value = Objects.requireNonNull(value);
			this.registration = Objects.requireNonNull(registration);
		}

		@Override
		public X value() {
			return value;
		}

		@Override
		public Registration registration() {
			return registration;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((registration == null) ? 0 : registration.hashCode());
			result = prime * result + ((value == null) ? 0 : value.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			@SuppressWarnings("rawtypes")
			Impl other = (Impl) obj;
			if (registration == null) {
				if (other.registration != null)
					return false;
			} else if (!registration.equals(other.registration))
				return false;
			if (value == null) {
				if (other.value != null)
					return false;
			} else if (!value.equals(other.value))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "IMPL [value=" + value + ", registration=" + registration + "]";
		}

	}
}
