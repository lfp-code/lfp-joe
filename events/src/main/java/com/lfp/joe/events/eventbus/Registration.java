package com.lfp.joe.events.eventbus;

public interface Registration extends AutoCloseable {

	void unregister();

	@Override
	default void close() {
		unregister();
	}
}
