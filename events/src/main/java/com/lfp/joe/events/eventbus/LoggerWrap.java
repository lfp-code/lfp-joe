package com.lfp.joe.events.eventbus;

import java.util.Objects;
import java.util.concurrent.CancellationException;
import java.util.logging.Level;

import org.greenrobot.eventbus.Logger;

import com.lfp.joe.core.log.Logging;
import com.lfp.joe.utils.Utils;

public class LoggerWrap implements Logger {

	private final org.slf4j.Logger logger;
	private boolean suppressCancelExceptions;

	public LoggerWrap(org.slf4j.Logger logger) {
		this(logger, false);
	}

	public LoggerWrap(org.slf4j.Logger logger, boolean suppressCancelExceptions) {
		this.logger = Objects.requireNonNull(logger);
		this.suppressCancelExceptions = suppressCancelExceptions;
	}

	@Override
	public void log(Level level, String msg, Throwable th) {
		if (suppressCancelExceptions && Utils.Exceptions.isCancelException(th))
			return;
		Logging.levelConsumer(logger, toLevel(level)).accept(msg, new Object[] { th });
	}

	@Override
	public void log(Level level, String msg) {
		Logging.levelConsumer(logger, toLevel(level)).accept(msg, new Object[0]);

	}

	private ch.qos.logback.classic.Level toLevel(Level level) {
		return Logging.toLevel(level == null ? null : level.toString()).orElse(ch.qos.logback.classic.Level.WARN);

	}

}
