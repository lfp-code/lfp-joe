package com.lfp.joe.events.bobbus;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

import net.jodah.typetools.TypeResolver;

public class ConsumerListenerContext<X> {

	private final Consumer<? extends X> listener;
	private Class<?> _eventType;
	private SubscribeOptions _subscribeOptions;

	public ConsumerListenerContext(Consumer<? extends X> listener, Class<X> eventType,
			SubscribeOptions subscribeOptions) {
		this.listener = Objects.requireNonNull(listener);
		if (eventType != null)
			this._eventType = eventType;
		if (subscribeOptions != null)
			this._subscribeOptions = subscribeOptions;
	}

	public Consumer<? extends X> getListener() {
		return listener;
	}

	public SubscribeOptions getSubscribeOptions() {
		if (this._subscribeOptions == null)
			synchronized (this) {
				if (this._subscribeOptions == null)
					this._subscribeOptions = SubscribeOptions.builder().build();
			}
		return this._subscribeOptions;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Optional<Class<X>> getEventType() {
		if (this._eventType == null)
			synchronized (this) {
				if (this._eventType == null) {
					var rawArgument = TypeResolver.resolveRawArgument(Consumer.class, this.getListener().getClass());
					if (rawArgument == null)
						rawArgument = TypeResolver.Unknown.class;
					this._eventType = rawArgument;
				}
			}
		if (TypeResolver.Unknown.class.equals(this._eventType))
			return Optional.empty();
		return Optional.of((Class) this._eventType);
	}

}
