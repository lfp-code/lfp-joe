package com.lfp.joe.events;

import java.util.function.Consumer;

import org.threadly.concurrent.wrapper.traceability.ThreadRenamingExecutor;

import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.events.bobbus.BobBus;
import com.lfp.joe.events.bobbus.SubscribeOptions;
import com.lfp.joe.threads.executor.SubmitterSchedulerLFP;

import net.engio.mbassy.bus.IMessagePublication;
import net.engio.mbassy.bus.config.Feature;
import net.engio.mbassy.listener.Invoke;

public enum CentralBus {
	INSTANCE;

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	private final BobBus<Object> ebus;

	private CentralBus() {
		ebus = new BobBus<>(ops -> {
			var asyncInvoker = ops.getFeature(Feature.AsynchronousHandlerInvocation.class);
			var executor = new ThreadRenamingExecutor(CoreTasks.executor(), this.getClass().getSimpleName(), false);
			var executorService = SubmitterSchedulerLFP.from(executor);
			asyncInvoker.setExecutor(executorService);
		});
		Runtime.getRuntime().addShutdownHook(new Thread(ebus::close));
	}

	public <X extends Object, L extends X> Consumer<L> subscribe(Consumer<L> listener) {
		return subscribe(null, listener, null);
	}

	public <X extends Object, L extends X> Consumer<L> subscribe(Consumer<L> listener,
			SubscribeOptions subscribeOptions) {
		return subscribe(null, listener, subscribeOptions);
	}

	public <X extends Object, L extends X> Consumer<L> subscribe(Class<X> eventType, Consumer<L> listener) {
		return subscribe(eventType, listener, null);
	}

	public <X extends Object, L extends X> Consumer<L> subscribe(Class<X> eventType, Consumer<L> listener,
			SubscribeOptions subscribeOptions) {
		if (subscribeOptions == null)
			subscribeOptions = SubscribeOptions.builder().delivery(Invoke.Asynchronously).build();
		return ebus.subscribe(eventType, listener, subscribeOptions);
	}

	public IMessagePublication publish(Object message) {
		return ebus.publishAsync(message);
	}

	public boolean unsubscribe(Object listener) {
		return ebus.unsubscribe(listener);
	}

	public boolean hasPendingMessages() {
		return ebus.hasPendingMessages();
	}

}
