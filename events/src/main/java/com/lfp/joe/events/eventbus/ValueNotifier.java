package com.lfp.joe.events.eventbus;

import java.util.Optional;

public interface ValueNotifier<X> {

	EventBusLFP<Optional<X>> getEventBus();

	Optional<X> getValue();

	static interface Writeable<X> extends ValueNotifier<X> {

		void set(X value);
	}

	public static class Impl<X> implements ValueNotifier<X> {
		private final EventBusLFP<Optional<X>> eventBus = new EventBusLFP<>();
		protected X value;

		@Override
		public EventBusLFP<Optional<X>> getEventBus() {
			return eventBus;
		}

		@Override
		public Optional<X> getValue() {
			return Optional.ofNullable(value);
		}

	}

	public static class WriteableImpl<X> extends Impl<X> implements Writeable<X> {

		@Override
		public void set(X value) {
			this.value = value;
			this.getEventBus().post(this.getValue());
		}

	}

	public static <X> ValueNotifier<X> create() {
		return new ValueNotifier.Impl<X>();
	}

	public static <X> ValueNotifier.Writeable<X> createWriteable() {
		return new ValueNotifier.WriteableImpl<X>();
	}
	
	public static <X> ValueNotifier.Writeable<X> createWriteable(X initialValue) {
		var result= new ValueNotifier.WriteableImpl<X>();
		result.set(initialValue);
		return result;
	}
}
