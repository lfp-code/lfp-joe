package com.lfp.joe.events.eventbus;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Future;
import java.util.function.Consumer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.EventBusBuilder;
import org.greenrobot.eventbus.Logger;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.threads.Threads;

import com.lfp.joe.utils.Utils;

import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public class EventBusLFP<X> implements Consumer<X> {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	private static final Logger DEFAULT_LOGGER = new LoggerWrap(logger);
	@SuppressWarnings("unchecked")
	private static final Field EventBus_subscriptionsByEventType_FIELD = JavaCode.Reflections.getFieldUnchecked(
			EventBus.class, false, f -> "subscriptionsByEventType".equals(f.getName()),
			f -> Map.class.isAssignableFrom(f.getType()));

	public static EventBusBuilder defaultBuilder() {
		return EventBus.builder().executorService(com.lfp.joe.threads.Threads.Pools.centralPool()).logger(DEFAULT_LOGGER)
				.logNoSubscriberMessages(false).throwSubscriberException(true);
	}

	private final EventBus eventBus;

	public EventBusLFP() {
		this(defaultBuilder().build());
	}

	public EventBusLFP(EventBus delegate) {
		this.eventBus = Objects.requireNonNull(delegate);
	}

	@SuppressWarnings("unchecked")
	protected Map<Class<?>, CopyOnWriteArrayList<?>> getSubscriptionsByEventType() {
		try {
			return (Map<Class<?>, CopyOnWriteArrayList<?>>) EventBus_subscriptionsByEventType_FIELD
					.get(this.getDelegate());
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
					? java.lang.RuntimeException.class.cast(e)
					: new java.lang.RuntimeException(e);
		}
	}

	public void unregisterAll() {
		streamSubscriptions().values().forEach(v -> getDelegate().unregister(v));
	}

	public EntryStream<Class<?>, Object> streamSubscriptions() {
		StreamEx<StreamEx<Entry<Class<?>, Object>>> streams = Utils.Lots.stream(getSubscriptionsByEventType())
				.nonNullKeys().nonNullValues().map(ent -> {
					return Utils.Lots.stream(ent.getValue()).map(v -> Utils.Lots.entry(ent.getKey(), v));
				});
		return Utils.Lots.flatMap(streams).mapToEntry(Entry::getKey, Entry::getValue);
	}

	public long getSubscriptionCount() {
		return Utils.Lots.stream(getSubscriptionsByEventType()).values().nonNull().mapToLong(v -> new Long(v.size()))
				.sum();
	}

	public EventBus getDelegate() {
		return eventBus;
	}

	public Registration register(Consumer<Event<X>> consumer) {
		return this.register(consumer, false, null);
	}

	public Registration register(Consumer<Event<X>> consumer, boolean async) {
		return this.register(consumer, async, null);
	}

	public Registration register(Consumer<Event<X>> consumer, Future<?> unregisterLinkedFuture) {
		return this.register(consumer, false, unregisterLinkedFuture);
	}

	public Registration register(Consumer<Event<X>> consumer, boolean async, Future<?> unregisterLinkedFuture) {
		AbstractSubscriber<X> subscriber;
		if (async)
			subscriber = new SubscriberAsync<X>(this.eventBus, consumer);
		else
			subscriber = new Subscriber<X>(this.eventBus, consumer);
		this.eventBus.register(subscriber);
		Registration registration = () -> this.eventBus.unregister(subscriber);
		if (unregisterLinkedFuture != null)
			Threads.Futures.callback(unregisterLinkedFuture, () -> registration.unregister());
		return registration;
	}

	public void post(X event) {
		this.eventBus.post(event);
	}

	@Override
	public void accept(X event) {
		post(event);
	}

	protected static abstract class AbstractSubscriber<X> implements Registration {

		private EventBus eventBus;
		private final Consumer<Event<X>> consumer;

		private AbstractSubscriber(EventBus eventBus, Consumer<Event<X>> consumer) {
			this.eventBus = Objects.requireNonNull(eventBus);
			this.consumer = Objects.requireNonNull(consumer);
		}

		public Consumer<Event<X>> getConsumer() {
			return consumer;
		}

		@Override
		public void unregister() {
			this.eventBus.unregister(this);
		}

		@SuppressWarnings("unchecked")
		protected Event<X> toEvent(Object event) {
			if (event instanceof org.greenrobot.eventbus.SubscriberExceptionEvent)
				event = ((org.greenrobot.eventbus.SubscriberExceptionEvent) event).throwable;
			if (event instanceof Throwable)
				throw Utils.Exceptions.asRuntimeException((Throwable) event);
			X eventValue = (X) event;
			return new Event.Impl<>(eventValue, AbstractSubscriber.this);
		}
	}

	public static final class Subscriber<X> extends AbstractSubscriber<X> {

		private Subscriber(EventBus eventBus, Consumer<Event<X>> consumer) {
			super(eventBus, consumer);
		}

		@Subscribe(threadMode = ThreadMode.MAIN)
		public void subscribe(Object event) {
			getConsumer().accept(toEvent(event));
		}

	}

	public static final class SubscriberAsync<X> extends AbstractSubscriber<X> {

		private SubscriberAsync(EventBus eventBus, Consumer<Event<X>> consumer) {
			super(eventBus, consumer);
		}

		@Subscribe(threadMode = ThreadMode.ASYNC)
		public void subscribe(Object event) {
			getConsumer().accept(toEvent(event));
		}
	}

}
