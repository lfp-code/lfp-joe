package com.lfp.joe.events.bobbus;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.reflections.reflection.FieldAccessor;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.threads.executor.SubmitterSchedulerLFP;
import com.lfp.joe.utils.Utils;

import net.engio.mbassy.bus.AbstractSyncAsyncMessageBus;
import net.engio.mbassy.bus.BusRuntime;
import net.engio.mbassy.bus.IMessagePublication;
import net.engio.mbassy.bus.MBassador;
import net.engio.mbassy.bus.MessagePublication;
import net.engio.mbassy.bus.config.BusConfiguration;
import net.engio.mbassy.bus.config.Feature;
import net.engio.mbassy.bus.config.IBusConfiguration;
import net.engio.mbassy.bus.error.IPublicationErrorHandler;
import net.engio.mbassy.bus.error.InternalPublicationError;
import net.engio.mbassy.bus.error.PublicationError;
import net.engio.mbassy.common.ReflectionUtils;
import net.engio.mbassy.listener.Enveloped;
import net.engio.mbassy.listener.Handler;
import net.engio.mbassy.listener.IMessageFilter;
import net.engio.mbassy.listener.Listener;
import net.engio.mbassy.listener.MessageHandler;
import net.engio.mbassy.listener.MessageListener;
import net.engio.mbassy.listener.MetadataReader;

@SuppressWarnings("rawtypes")
public class BobBus<T> extends MBassador<T> implements AutoCloseable {

	public static <T> BobBus<T> create() {
		return new BobBus<>();
	}

	public static <T> BobBus<T> create(Executor executor) {
		return new BobBus<>(ops -> {
			var asyncInvoker = ops.getFeature(Feature.AsynchronousHandlerInvocation.class);
			asyncInvoker.setExecutor(SubmitterSchedulerLFP.from(executor));
		});
	}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	@SuppressWarnings("unchecked")
	private static final FieldAccessor<MessageListener, Listener> MessageListener_listenerAnnotation_FA = JavaCode.Reflections
			.getFieldAccessorUnchecked(MessageListener.class, true, m -> "listenerAnnotation".equals(m.getName()),
					m -> Listener.class.isAssignableFrom(m.getType()));
	@SuppressWarnings("unchecked")
	private static final FieldAccessor<MessagePublication, BusRuntime> MessagePublication_runtime_FA = JavaCode.Reflections
			.getFieldAccessorUnchecked(MessagePublication.class, true, m -> "runtime".equals(m.getName()),
					m -> BusRuntime.class.isAssignableFrom(m.getType()));

	private static final Object CONSUMER_ACCEPT_METHOD_NAME = "accept";
	private static final Map<Thread, ConsumerListenerContext<?>> CONSUMER_LISTENER_CONTEXT_THREAD_MAP = new ConcurrentHashMap<>();

	private static final MemoizedSupplier<BlockingQueue<IMessagePublication>> PENDING_MESSAGE_QUEUE_SUPPLIER = Utils.Functions
			.memoize(() -> {
				var pendingMessages = new LinkedBlockingQueue<IMessagePublication>();
				CoreTasks.executor().execute(() -> {
					consumePendingMessages(pendingMessages);
				});
				return pendingMessages;
			});

	/**
	 * Default constructor using default setup. super() will also add a default
	 * publication error logger
	 */
	public BobBus() {
		this(null, null);
	}

	/**
	 * Construct with fully specified configuration
	 *
	 * @param configuration
	 */
	public BobBus(IBusConfiguration configuration) {
		this(configuration, null);
	}

	public BobBus(Consumer<IBusConfiguration> configurationSetup) {
		this(null, configurationSetup);
	}

	protected BobBus(IBusConfiguration configuration, Consumer<IBusConfiguration> configurationSetup) {
		super(modifyConfiguration(configuration, configurationSetup));
	}

	public <X extends T, L extends X> Consumer<L> subscribe(Consumer<L> listener) {
		return subscribe(null, listener, null);
	}

	public <X extends T, L extends X> Consumer<L> subscribe(Consumer<L> listener, SubscribeOptions subscribeOptions) {
		return subscribe(null, listener, subscribeOptions);
	}

	public <X extends T, L extends X> Consumer<L> subscribe(Class<X> eventType, Consumer<L> listener) {
		return subscribe(eventType, listener, null);
	}

	public <X extends T, L extends X> Consumer<L> subscribe(Class<X> eventType, Consumer<L> listener,
			SubscribeOptions subscribeOptions) {
		if (listener == null) {
			Object listenerObj = listener;
			subscribe(listenerObj);
			return listener;
		}
		var consumerListenerContext = new ConsumerListenerContext<>(listener, eventType, subscribeOptions);
		var thread = Thread.currentThread();
		CONSUMER_LISTENER_CONTEXT_THREAD_MAP.put(thread, consumerListenerContext);
		try {
			Object listenerObj = listener;
			subscribe(listenerObj);
		} finally {
			CONSUMER_LISTENER_CONTEXT_THREAD_MAP.remove(thread);
		}
		return listener;
	}

	@Override
	public void close() {
		try {
			super.finalize();
		} catch (Throwable e) {
			throw Utils.Exceptions.asRuntimeException(e);
		}
	}

	private static IBusConfiguration modifyConfiguration(IBusConfiguration configuration,
			Consumer<IBusConfiguration> configurationSetup) {
		if (configuration == null) {
			var asyncInvoker = Feature.AsynchronousHandlerInvocation.Default().setExecutor(Threads.Pools.centralPool());
			var asyncDispatcher = Feature.AsynchronousMessageDispatch.Default()
					.setNumberOfMessageDispatchers(Utils.Machine.logicalProcessorCount());
			configuration = new BusConfiguration().addFeature(Feature.SyncPubSub.Default()).addFeature(asyncInvoker)
					.addFeature(asyncDispatcher);
		}
		if (configurationSetup != null)
			configurationSetup.accept(configuration);
		// handled by central pending message queue
		var asynchronousMessageDispatch = configuration.getFeature(Feature.AsynchronousMessageDispatch.class);
		asynchronousMessageDispatch.setNumberOfMessageDispatchers(0);
		asynchronousMessageDispatch.setMessageQueue(PENDING_MESSAGE_QUEUE_SUPPLIER.get());
		var syncPubSub = configuration.getFeature(Feature.SyncPubSub.class);
		if (syncPubSub == null) {
			syncPubSub = Feature.SyncPubSub.Default();
			configuration.addFeature(syncPubSub);
		}
		var delegate = syncPubSub.getMetadataReader();
		syncPubSub.setMetadataReader(new MetadataReader() {

			@Override
			public MessageListener getMessageListener(Class target) {
				MessageListener messageListener;
				if (delegate != null)
					messageListener = delegate.getMessageListener(target);
				else
					messageListener = super.getMessageListener(target);
				return modifyMessageListener(target, messageListener);
			}
		});
		configuration.addPublicationErrorHandler(new IPublicationErrorHandler() {

			@Override
			public void handleError(PublicationError error) {
				var errorMessage = String.format("publication error. message:%s handler:%s listener:%s",
						error.getMessage(), error.getHandler(), error.getListener());
				logger.error(errorMessage, error.getCause());
			}
		});
		return configuration;
	}

	private static MessageListener modifyMessageListener(Class target, MessageListener messageListener) {
		if (!Consumer.class.isAssignableFrom(target))
			return messageListener;
		var handlers = messageListener.getHandlers();
		if (handlers != null && handlers.length > 0)
			return messageListener;
		MessageHandler messageHandler = getConsumerMessageHandler(target, messageListener);
		if (messageHandler != null)
			messageListener.addHandler(messageHandler);
		return messageListener;
	}

	private static MessageHandler getConsumerMessageHandler(Class target, MessageListener messageListener) {
		var consumerListenerContext = CONSUMER_LISTENER_CONTEXT_THREAD_MAP.get(Thread.currentThread());
		if (consumerListenerContext == null)
			return null;
		if (consumerListenerContext.getEventType().isEmpty())
			return null;
		var applyMethods = getConsumerAcceptMethods(target, consumerListenerContext.getEventType().get());
		if (applyMethods.length == 0)
			return null;
		var handler = applyMethods[0];
		Handler handlerConfig = consumerListenerContext.getSubscribeOptions().toHandler();
		Enveloped enveloped = new Enveloped() {

			@Override
			public Class<? extends Annotation> annotationType() {
				return Enveloped.class;
			}

			@Override
			public Class[] messages() {
				return new Class[] { consumerListenerContext.getEventType().get() };
			}
		};
		if (!handlerConfig.enabled())
			return null;
		MessageListener_listenerAnnotation_FA.set(messageListener,
				consumerListenerContext.getSubscribeOptions().toListener());
		var handlerProperties = MessageHandler.Properties.Create(handler, handlerConfig, enveloped,
				new IMessageFilter[0], messageListener);
		handlerProperties.put(MessageHandler.Properties.Enveloped, false);
		MessageHandler handlerMetadata = new MessageHandler(handlerProperties);

		return handlerMetadata;
	}

	private static Method[] getConsumerAcceptMethods(Class target, Class rawArgument) {
		var methods = ReflectionUtils.getMethods(m -> {
			if (Modifier.isAbstract(m.getModifiers()))
				return false;
			if (!CONSUMER_ACCEPT_METHOD_NAME.equals(m.getName()))
				return false;
			if (m.getParameterCount() != 1)
				return false;
			if (!m.getParameterTypes()[0].isAssignableFrom(rawArgument))
				return false;
			return true;
		}, target);
		return methods;
	}

	private static void consumePendingMessages(LinkedBlockingQueue<IMessagePublication> pendingMessages) {
		var thread = Thread.currentThread();
		var currentName = thread.getName();
		try {
			thread.setName(THIS_CLASS.getSimpleName() + " MsgDispatcher");
			while (!thread.isInterrupted()) {
				IMessagePublication publication;
				try {
					publication = pendingMessages.take();
				} catch (InterruptedException e) {
					logger.trace("publication take interrupted", e);
					continue;
				}
				CoreTasks.executor().execute(() -> {
					try {
						publication.execute();
					} catch (Throwable t) {
						handlePublicationError(publication, t);
					}
				});
			}
		} finally {
			thread.setName(currentName);
		}
	}

	@SuppressWarnings("unchecked")
	private static void handlePublicationError(IMessagePublication publication, Throwable failure) {
		String msg = "Error in asynchronous dispatch";
		boolean logError = true;
		if (publication instanceof MessagePublication) {
			var runtime = MessagePublication_runtime_FA.get((MessagePublication) publication);
			Collection<IPublicationErrorHandler> registeredErrorHandlers = Optional.ofNullable(runtime).map(v -> {
				return v.getProvider();
			}).map(v -> {
				return Utils.Types.tryCast(v, AbstractSyncAsyncMessageBus.class).orElse(null);
			}).map(v -> {
				return v.getRegisteredErrorHandlers();
			}).orElse(Collections.emptyList());
			if (!registeredErrorHandlers.isEmpty()) {
				var error = new InternalPublicationError(failure, msg, publication);
				for (IPublicationErrorHandler errorHandler : registeredErrorHandlers) {
					try {
						errorHandler.handleError(error);
						logError = false;
					} catch (Throwable t) {
						logger.error("Error in error handler", t);
					}
				}
			}
		}
		if (logError)
			logger.error(msg, failure);
	}

}
