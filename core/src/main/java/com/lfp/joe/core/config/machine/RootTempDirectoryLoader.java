package com.lfp.joe.core.config.machine;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Stream;

public class RootTempDirectoryLoader extends MachineConfigLoader<File> {

	public static final RootTempDirectoryLoader INSTANCE = new RootTempDirectoryLoader();

	private static final String TEMP_DIR_PROPERTY_NAME = "java.io.tmpdir";

	private RootTempDirectoryLoader() {}

	@Override
	protected Iterable<? extends Callable<? extends File>> loaders() {
		List<Callable<File>> loaders = new ArrayList<>();
		loaders.add(() -> pathToDir(System.getenv(TEMP_DIR_PROPERTY_NAME)));
		loaders.add(() -> pathToDir(System.getProperty(TEMP_DIR_PROPERTY_NAME)));
		loaders.add(() -> {
			var lines = execLinesOut("mktemp");
			lines = lines.flatMap(nonNullOrBlankFlatMap());
			var mktempFile = lines.findFirst().map(File::new).orElse(null);
			if (mktempFile == null || !mktempFile.exists())
				return null;
			mktempFile.delete();
			return mktempFile.getParentFile();
		});
		loaders.add(() -> {
			var lines = execLinesOut("echo", "%temp%");
			lines = lines.flatMap(nonNullOrBlankFlatMap());
			return lines.findFirst().map(File::new).orElse(null);
		});
		return loaders;
	}

	@Override
	protected Stream<File> loadStream(Stream<File> stream) {
		return stream.map(v -> new File(v, "java-temp")).peek(File::mkdirs);
	}

	protected static File pathToDir(String path) {
		if (path == null || path.isBlank())
			return null;
		return new File(path);
	}

	public static void main(String[] args) {
		System.out.println(RootTempDirectoryLoader.INSTANCE.get());
	}
}
