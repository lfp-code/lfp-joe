package com.lfp.joe.core.lock;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.lfp.joe.core.function.Muto.MutoBoolean;
import com.lfp.joe.core.function.Registration;
import com.lfp.joe.core.process.future.InterruptCancellationException;

public class ThreadRegistry implements Registration {

	private static final boolean INTERRUPT_ON_CLOSE_DEFAULT = true;
	private final ReentrantReadWriteLock threadMapLock = new ReentrantReadWriteLock();
	private ConcurrentMap<Thread, Integer> threadMap = new ConcurrentHashMap<>();
	private boolean interrupt;

	public Registration register() {
		return register(Thread.currentThread());
	}

	public Registration register(Thread thread) {
		Objects.requireNonNull(thread);
		validateNotClosed(thread);
		threadMapLock.readLock().lock();
		try {
			validateNotClosed(thread);
			threadMap.compute(thread, (nil, count) -> count == null ? 1 : count + 1);
			return () -> unregister(thread, null);
		} finally {
			threadMapLock.readLock().unlock();
		}
	}

	public boolean unregister() {
		return unregister(Thread.currentThread());
	}

	public boolean unregister(Thread thread) {
		if (thread == null)
			return false;
		var resultRef = MutoBoolean.create();
		unregister(thread, resultRef::setTrue);
		return resultRef.getAsBoolean();
	}

	protected void unregister(Thread thread, Runnable callback) {
		if (thread == null)
			return;
		Map<Thread, Integer> m;
		if ((m = threadMap) == null)
			return;
		m.computeIfPresent(thread, (nil, count) -> {
			Optional.ofNullable(callback).ifPresent(Runnable::run);
			return count <= 1 ? null : count - 1;
		});
	}

	public boolean isClosed() {
		return threadMap == null;
	}

	@Override
	public void close() {
		close(INTERRUPT_ON_CLOSE_DEFAULT);
	}

	public void close(boolean interrupt) {
		if (interrupt)
			this.interrupt = true;
		if (isClosed())
			return;
		threadMapLock.writeLock().lock();
		try {
			if (isClosed())
				return;
			if (interrupt)
				threadMap.keySet().forEach(Thread::interrupt);
			threadMap.clear();
			threadMap = null;
		} finally {
			threadMapLock.writeLock().unlock();
		}

	}

	public void validateNotClosed() {
		validateNotClosed(Thread.currentThread());
	}

	protected void validateNotClosed(Thread thread) {
		Objects.requireNonNull(thread);
		if (!isClosed())
			return;
		throw createClosedFailure();
	}

	protected RuntimeException createClosedFailure() {
		if (interrupt)
			throw new InterruptCancellationException(this.getClass().getSimpleName() + " interrupted");
		return new CancellationException(this.getClass().getSimpleName() + " closed");
	}

	public static void main(String[] args) {
		var threadRegistry = new ThreadRegistry();
		var num = 10;
		for (int i = 0; i < num; i++)
			threadRegistry.register();
		System.out.println(threadRegistry.threadMap);
		for (int i = 0; i < num; i++)
			threadRegistry.unregister();
		System.out.println(threadRegistry.threadMap);

	}
}
