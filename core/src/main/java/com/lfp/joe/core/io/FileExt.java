package com.lfp.joe.core.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.ImmutableMemberCache.MethodRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.function.Muto;
import com.lfp.joe.core.function.Registration;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

public class FileExt extends File implements BasicFileAttributes, Scrapable {

	private static final long serialVersionUID = -3300474786189250260L;

	public static FileExt from(java.io.File file) {
		return CoreReflections.tryCast(file, FileExt.class).orElseGet(() -> new FileExt(file));
	}

	private final Scrapable.Impl _scrapable = new Scrapable.Impl();
	private Object deleteAllRegistrationMutex = new Object();
	private Registration deleteAllRegistration;
	private int hashCode;

	public FileExt(String pathname) {
		super(pathname);
	}

	public FileExt(File parent, String pathname) {
		super(parent, pathname);
	}

	public FileExt(URI uri) {
		super(uri);
	}

	protected FileExt(File file) {
		super("");
		Objects.requireNonNull(file);
		for (var fieldName : List.of("path", "prefixLength")) {
			var request = FieldRequest.of(File.class, null, fieldName);
			var value = MemberCache.getFieldValue(request, file);
			MemberCache.setFieldValue(request, this, value);
		}
	}

	public String getUniquePath() {
		return getUniquePath(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (super.equals(obj))
			return true;
		if (!(obj instanceof File))
			return false;
		return getUniquePath().equals(getUniquePath((File) obj));
	}

	public FileExt mkdir(boolean parentDirectories) {
		if (parentDirectories)
			mkdirs();
		else
			mkdir();
		return this;
	}

	public FileExt child(String... children) {
		if (children == null || children.length == 0)
			return this;
		return new FileExt(this, Stream.of(children).collect(Collectors.joining(File.separator)));
	}

	@Override
	public int hashCode() {
		if (hashCode == 0)
			hashCode = new File(getUniquePath()).hashCode();
		return hashCode;
	}

	public FileExt deleteAllOnScrap(boolean deleteAllOnScrap) {
		if (deleteAllOnScrap) {
			if (deleteAllRegistration == null)
				synchronized (deleteAllRegistrationMutex) {
					if (deleteAllRegistration == null)
						deleteAllRegistration = this.onScrap(this::deleteAll);
				}
		} else {
			if (deleteAllRegistration != null)
				synchronized (deleteAllRegistrationMutex) {
					if (deleteAllRegistration != null) {
						deleteAllRegistration.close();
						deleteAllRegistration = null;
					}
				}
		}
		return this;
	}

	public boolean isDeleteAllOnScrap() {
		return deleteAllRegistration != null;
	}

	public long deleteAll() {
		var path = this.toPath();
		Function<Path, Optional<Boolean>> deleteFunction = p -> {
			try {
				return Optional.of(Files.deleteIfExists(p));
			} catch (IOException e) {
				// suppress
				return Optional.empty();
			}
		};
		if (Files.isRegularFile(path)) {
			var resultOp = deleteFunction.apply(path);
			if (resultOp.isPresent())
				return resultOp.get() ? 1 : 0;
		}
		var total = Muto.create(0);
		var fileVisitor = new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				deleteFunction.apply(file).filter(v -> v).ifPresent(nil -> total.updateAndGet(v -> v + 1));
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				deleteFunction.apply(dir).filter(v -> v).ifPresent(nil -> total.updateAndGet(v -> v + 1));
				return FileVisitResult.CONTINUE;
			}
		};
		try {
			Files.walkFileTree(path, fileVisitor);
		} catch (IOException e) {
			// suppress
		}
		return total.get();
	}

	// *** readers and writers ***

	public InputStreamExt inputStream(OpenOption... openOptions) throws IOException {
		return inputStream(null, null, null, openOptions);
	}

	public InputStreamExt inputStream(Charset charset, Integer bufferSize, Boolean disableCloseLink,
			OpenOption... openOptions) throws IOException {
		var openOptionsNormalizedOp = OpenOptions.normalize(openOptions);
		if (openOptionsNormalizedOp.isPresent())
			return inputStream(charset, bufferSize, disableCloseLink, openOptionsNormalizedOp.get());
		ThrowingSupplier<InputStream, IOException> loader = () -> {
			InputStream inputStream;
			if (openOptions.length == 0)
				inputStream = new FileInputStream(this);
			else
				inputStream = Files.newInputStream(this.toPath(), openOptions);
			return inputStream;
		};
		var result = new InputStreamExt(loader) {

			private final Scrapable closeLinkRegistration;
			{
				if (Boolean.TRUE.equals(disableCloseLink))
					closeLinkRegistration = null;
				else
					closeLinkRegistration = FileExt.this.onScrap(this, Throws.consumerUnchecked(AutoCloseable::close));
			}

			@Override
			public void close() throws IOException {
				try {
					super.close();
				} finally {
					if (closeLinkRegistration != null)
						closeLinkRegistration.scrap();
				}
			}
		};
		return result;
	}

	public OutputStreamExt outputStream(OpenOption... openOptions) throws IOException {
		return outputStream(null, null, null, openOptions);
	}

	public OutputStreamExt outputStream(Charset charset, Integer bufferSize, Boolean disableCloseLink,
			OpenOption... openOptions) throws IOException {
		var openOptionsNormalizedOp = OpenOptions.normalize(openOptions);
		if (openOptionsNormalizedOp.isPresent())
			return outputStream(charset, bufferSize, disableCloseLink, openOptionsNormalizedOp.get());
		ThrowingSupplier<OutputStream, IOException> loader = () -> {
			OutputStream outputStream;
			if (openOptions.length == 0)
				outputStream = new FileOutputStream(this);
			else
				outputStream = Files.newOutputStream(this.toPath(), openOptions);
			return outputStream;
		};
		var result = new OutputStreamExt(loader) {

			private final Scrapable closeLinkRegistration;
			{
				if (Boolean.TRUE.equals(disableCloseLink))
					closeLinkRegistration = null;
				else
					closeLinkRegistration = FileExt.this.onScrap(this, Throws.consumerUnchecked(AutoCloseable::close));
			}

			@Override
			public void close() throws IOException {
				try {
					super.close();
				} finally {
					if (closeLinkRegistration != null)
						closeLinkRegistration.scrap();
				}
			}
		};
		return result;
	}

	public byte[] hash(MessageDigest messageDigest, OpenOption... openOptions) throws IOException {
		return digest(messageDigest, openOptions).digest();
	}

	public MessageDigest digest(MessageDigest messageDigest, OpenOption... openOptions) throws IOException {
		try (var is = this.inputStream(openOptions);
				var dos = new DigestOutputStream(OutputStream.nullOutputStream(), messageDigest)) {
			is.transferTo(dos);
			return dos.getMessageDigest();
		}
	}

	// *** basic file attributes ***

	protected Optional<BasicFileAttributes> getBasicFileAttributes() {
		BasicFileAttributes attr;
		try {
			attr = Files.readAttributes(this.toPath(), BasicFileAttributes.class);
		} catch (IOException e) {
			// suppress
			return Optional.empty();
		}
		return Optional.ofNullable(attr);
	}

	public boolean lastModifiedElapsed(Duration duration) {
		return elapsed(lastModifiedTime(), duration);
	}

	public boolean creationTimeElapsed(Duration duration) {
		return elapsed(creationTime(), duration);
	}

	public boolean lastAccessTimeElapsed(Duration duration) {
		return elapsed(lastAccessTime(), duration);
	}

	@Override
	public FileTime lastModifiedTime() {
		var attr = getBasicFileAttributes().orElse(null);
		if (attr != null) {
			for (var ft : Arrays.asList(attr.lastModifiedTime(), attr.creationTime())) {
				if (ft == null)
					continue;
				if (ft.toMillis() > 0)
					return ft;
			}
		}
		var lastModified = this.lastModified();
		if (lastModified > 0)
			return FileTime.fromMillis(lastModified);
		return FileTime.fromMillis(0);
	}

	@Override
	public FileTime lastAccessTime() {
		return getBasicFileAttributes().map(BasicFileAttributes::lastAccessTime)
				.orElseGet(() -> FileTime.fromMillis(0));
	}

	@Override
	public FileTime creationTime() {
		return getBasicFileAttributes().map(BasicFileAttributes::creationTime).orElseGet(() -> FileTime.fromMillis(0));
	}

	@Override
	public boolean isRegularFile() {
		return getBasicFileAttributes().map(BasicFileAttributes::isRegularFile).orElse(false);
	}

	@Override
	public boolean isSymbolicLink() {
		return getBasicFileAttributes().map(BasicFileAttributes::isRegularFile).orElse(false);
	}

	@Override
	public boolean isOther() {
		return getBasicFileAttributes().map(BasicFileAttributes::isRegularFile).orElse(false);
	}

	@Override
	public long size() {
		return getBasicFileAttributes().map(BasicFileAttributes::size).orElse(-1l);
	}

	@Override
	public Object fileKey() {
		return getBasicFileAttributes().map(BasicFileAttributes::fileKey).orElse(null);
	}
	// *** scrapable ***

	protected <X extends Scrapable, T extends Throwable> X scrapSync(X scrapable, Boolean disableAutoClose,
			OpenOption... openOptions) throws T {
		Objects.requireNonNull(scrapable);
		if (disableAutoClose == null || !disableAutoClose) {
			var scrapRegistration = this.onScrap(scrapable);
			scrapable.onScrap(scrapRegistration);
		}
		return scrapable;
	}

	@Override
	public boolean isScrapped() {
		return _scrapable.isScrapped();
	}

	@Override
	public Scrapable onScrap(Runnable task) {
		return _scrapable.onScrap(task);
	}

	@Override
	public boolean scrap() {
		return _scrapable.scrap();
	}

	// *** utils ***/

	public static String getUniquePath(File file) {
		Objects.requireNonNull(file);
		if (!isInvalid(file)) {
			String canonicalPath;
			try {
				canonicalPath = file.getCanonicalPath();
			} catch (IOException e) {
				throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e) : new RuntimeException(e);
			}
			if (canonicalPath != null && !canonicalPath.isBlank())
				return canonicalPath;
		}
		return file.getAbsolutePath();
	}

	protected static boolean isInvalid(File file) {
		if (file == null)
			return true;
		var req = MethodRequest.of(File.class, boolean.class, "isInvalid");
		try {
			var method = MemberCache.getMethod(req);
			return (boolean) method.invoke(file);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException e) {
			throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e) : new RuntimeException(e);
		}
	}

	private static boolean elapsed(FileTime fileTime, Duration duration) {
		if (fileTime == null || duration == null)
			return false;
		var fileTimeMillis = fileTime.toMillis();
		if (fileTimeMillis <= 0)
			return false;
		var elapsed = Duration.ofMillis(System.currentTimeMillis() - fileTimeMillis);
		var result = elapsed.compareTo(duration) > 0;
		return result;
	}

	public static void main(String[] args) {
		var dudation = Duration.ofSeconds(2);
		var elapsed = Duration.ofSeconds(10);
		System.out.println(elapsed.compareTo(dudation));
		var file = new File("temp");
		System.out.println(file.getAbsolutePath());
		var fileExt = FileExt.from(file);
		System.out.println(fileExt.getAbsolutePath());
	}

}