package com.lfp.joe.core.function;

public interface Registration extends AutoCloseable {

	@Override
	void close();

	default Registration with(Registration onClose) {
		return () -> {
			try (onClose) {
				this.close();
			}
		};
	}

	public static Registration complete() {
		return Const.COMPLETE;
	}

	static enum Const {
		;

		private static final Registration COMPLETE = () -> {};

	}

}
