package com.lfp.joe.core.function;

public class NumeralFunctions {

	private NumeralFunctions() {}

	public static interface TriFunction<A, B, C, R> {

		R apply(A a, B b, C c);
	}

	public static interface QuadFunction<A, B, C, D, R> {

		R apply(A a, B b, C c, D d);
	}

	public static interface QuintFunction<A, B, C, D, E, R> {

		R apply(A a, B b, C c, D d, E e);
	}

	public static interface SextFunction<A, B, C, D, E, F, R> {

		R apply(A a, B b, C c, D d, E e, F f);
	}

	public static interface SeptFunction<A, B, C, D, E, F, G, R> {

		R apply(A a, B b, C c, D d, E e, F f, G g);
	}

	public static interface OctoFunction<A, B, C, D, E, F, G, H, R> {

		R apply(A a, B b, C c, D d, E e, F f, G g, H h);
	}

	public static interface TriConsumer<A, B, C> {

		void accept(A a, B b, C c);
	}

	public static interface QuadConsumer<A, B, C, D> {

		void accept(A a, B b, C c, D d);
	}

	public static interface QuintConsumer<A, B, C, D, E> {

		void accept(A a, B b, C c, D d, E e);
	}

	public static interface SextConsumer<A, B, C, D, E, F> {

		void accept(A a, B b, C c, D d, E e, F f);
	}

	public static interface SeptConsumer<A, B, C, D, E, F, G> {

		void accept(A a, B b, C c, D d, E e, F f, G g);
	}

	public static interface OctoConsumer<A, B, C, D, E, F, G, H> {

		void accept(A a, B b, C c, D d, E e, F f, G g, H h);
	}
}
