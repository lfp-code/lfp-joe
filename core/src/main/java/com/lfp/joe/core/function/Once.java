package com.lfp.joe.core.function;

import java.util.Objects;
import java.util.function.Supplier;

public interface Once {

	boolean isDone();

	void release();

	static abstract class Abs<T, R> implements Once {

		private static final String RECURSIVE_LOAD_ERROR = "recursive load - thread:";
		private Object state = InitialState.VOID;

		protected Abs(T initialValue) {
			if (initialValue != null) {
				var initialState = new InitialState<T>();
				initialState.initialValue = initialValue;
				this.state = initialState;
			}
		}

		@Override
		public boolean isDone() {
			return getInitialState() == null;
		}

		@Override
		public void release() {
			state = null;
		}

		@SuppressWarnings("unchecked")
		protected R getState() {
			Object r;
			if ((r = state) == null)
				return null;
			else if (r instanceof InitialState)
				return null;
			else
				return (R) r;
		}

		protected final boolean load() {
			InitialState<T> initialState;
			if ((initialState = getInitialState()) == null)
				return false;
			synchronized (initialState) {
				if ((initialState = getInitialState()) == null)
					return false;
				if (initialState.loading)
					throw new IllegalStateException(RECURSIVE_LOAD_ERROR + Thread.currentThread().getName());
				initialState.loading = true;
				try {
					this.state = loadOnce(initialState.initialValue);
					return true;
				} finally {
					initialState.loading = false;
				}
			}
		}

		protected abstract R loadOnce(T initialValue);

		@SuppressWarnings("unchecked")
		private final InitialState<T> getInitialState() {
			if (state == null)
				return null;
			Object r = state;
			if (!(r instanceof InitialState))
				return null;
			return (InitialState<T>) r;
		}

		private static final class InitialState<T> {

			private static final InitialState<Void> VOID = new InitialState<>();

			private T initialValue;
			private boolean loading;

			private InitialState() {}
		}

	}

	public static interface RunnableOnce extends Runnable, Once {

		public static RunnableOnce of(Runnable runnable) {
			return new RunnableOnce.Impl(runnable);
		}

		static class Impl extends Once.Abs<Runnable, Void> implements RunnableOnce {

			protected Impl(Runnable runnable) {
				super(Objects.requireNonNull(runnable));
			}

			@Override
			public void run() {
				load();
			}

			@Override
			protected Void loadOnce(Runnable initialValue) {
				initialValue.run();
				return null;
			}

		}

	}

	public static interface SupplierOnce<T> extends Supplier<T>, Once {

		public static <U> SupplierOnce<U> of(Supplier<U> supplier) {
			return new SupplierOnce.Impl<U>(supplier);
		}

		static class Impl<T> extends Once.Abs<Supplier<T>, T> implements SupplierOnce<T> {

			protected Impl(Supplier<T> supplier) {
				super(Objects.requireNonNull(supplier));
			}

			@Override
			public T get() {
				load();
				return getState();
			}

			@Override
			protected T loadOnce(Supplier<T> initialValue) {
				return initialValue.get();
			}

		}

	}

}
