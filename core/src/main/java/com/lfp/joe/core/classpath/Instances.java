package com.lfp.joe.core.classpath;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import org.slf4j.Logger;

import de.adito.picoservice.PicoService;

@SuppressWarnings("unchecked")
public final class Instances {

	private Instances() {}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final Map<Object, CacheValue<?>> CACHE = new ConcurrentHashMap<>();

	private static final Object CLASS_NAME_VALID_CACHE_KEY = new Object();

	public static Stream<? extends Object> streamAutowired() {
		return streamAutowired(Object.class);
	}

	public static <U> Stream<U> streamAutowired(Class<? extends U> classType) {
		var ctAnnoStream = CoreServiceRegistry.instance().stream(classType, Autowired.class, (ent1, ent2) -> {
			Integer p1 = ent1.getValue().priority();
			Integer p2 = ent2.getValue().priority();
			var compareTo = p1.compareTo(p2);
			return -1 * compareTo;
		});
		return ctAnnoStream.map(ent -> {
			var ct = ent.getKey();
			var anno = ent.getValue();
			var classNameValidKey = List.of(CLASS_NAME_VALID_CACHE_KEY, ct, Arrays.asList(anno.requiredClassNames()));
			boolean[] classNameCheckLoaded = new boolean[] { false };
			var classNameValid = get(classNameValidKey, () -> {
				for (var className : anno.requiredClassNames())
					if (CoreReflections.tryForName(className).isEmpty())
						return false;
				classNameCheckLoaded[0] = true;
				return true;
			});
			if (!classNameValid)
				return null;
			var instance = get(ct);
			if (classNameCheckLoaded[0])
				getLogger().info("{} type:{} instance:{}", Autowired.class.getSimpleName(), classType.getName(),
						ct.getName());
			return instance;
		}).filter(Objects::nonNull);
	}

	public static <U> U get() {
		Class<U> callingClass = (Class<U>) CoreReflections.getCallingClass(THIS_CLASS);
		return get(callingClass);
	}

	public static <U> U get(Callable<? extends U> loader) {
		Class<U> callingClass = (Class<U>) CoreReflections.getCallingClass(THIS_CLASS);
		return get(callingClass, loader);
	}

	public static <U> U get(Class<U> classType) {
		return get(classType, () -> CoreReflections.newInstanceUnchecked(classType));
	}

	public static <U> U get(Class<U> classType, Callable<? extends U> loader) {
		Object key = classType;
		return get(key, loader == null ? null : () -> {
			var instance = loader.call();
			if (instance == null)
				return null;
			if (!CoreReflections.isInstance(classType, instance)) {
				var message = String.format("invalid instance. classType:%s instanceType:%s", classType.getName(),
						instance.getClass().getName());
				throw new IllegalArgumentException(message);
			}
			return instance;
		});
	}

	public static <K, U> U get(K key, Callable<? extends U> loader) {
		if (key instanceof Class)
			key = (K) CoreReflections.getNamedClassType((Class<?>) key);
		Objects.requireNonNull(key);
		Objects.requireNonNull(loader);
		var cacheValue = (CacheValue<U>) CACHE.computeIfAbsent(key, nil -> new CacheValue<>());
		try {
			return cacheValue.get(loader);
		} catch (Throwable t) {
			CACHE.remove(key, cacheValue);
			throw RuntimeException.class.isInstance(t) ? RuntimeException.class.cast(t) : new RuntimeException(t);
		}
	}

	private static Logger getLogger() {
		return org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target({ ElementType.TYPE })
	@PicoService
	public static @interface Autowired {

		// alias for value
		String[] requiredClassNames() default {};

		int priority() default 0;

	}

	private static class CacheValue<X> {

		private final Object mutex = new Object();
		private X value;

		public X get(Callable<? extends X> loader) throws Exception {
			if (value == null)
				synchronized (mutex) {
					if (value == null)
						value = Objects.requireNonNull(loader.call());
				}
			return value;
		}

	}

	public static void main(String[] args) {
		var arr = (Object[]) Array.newInstance(Object.class, 32);
		var arr2 = new Date[] { new Date(0) };
		arr[0] = new Date(0);
		System.out.println(Arrays.toString(arr));
		System.out.println(Objects.equals(arr, arr2));
		System.out.println(arr.getClass());
		System.out.println(arr2.getClass());
		System.out.println(arr.getClass().isArray());
		var list1 = List.of(1, 2, 3, List.of(new Date(0), 2));
		var list2 = new ArrayList<Object>(
				Arrays.asList(Integer.valueOf(1), Integer.valueOf(2), Integer.valueOf(3), List.of(new Date(0), 2)));
		System.out.println(list1.hashCode());
		System.out.println(list2.hashCode());
		System.out.println(list1.toArray().hashCode());
		System.out.println(list2.toArray().hashCode());
		System.out.println(list1.equals(list2));
		System.out.println(list2.equals(list1));
	}

}
