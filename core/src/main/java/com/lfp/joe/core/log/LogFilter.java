package com.lfp.joe.core.log;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Nullable;

import org.immutables.value.Value;

import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.log.ImmutableLogFilter.FilterContextImpl;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.filter.ThresholdFilter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.IThrowableProxy;
import ch.qos.logback.core.spi.FilterReply;

@ValueLFP.Style
@Value.Enclosing
public class LogFilter extends ThresholdFilter {

	private static final MemoizedSupplier<Field> LEVEL_FIELD_S = MemoizedSupplier.create(() -> {
		Stream<Field> stream = Arrays.asList(ThresholdFilter.class.getDeclaredFields()).stream();
		stream = stream.filter(f -> "level".equals(f.getName()));
		stream = stream.filter(f -> ch.qos.logback.classic.Level.class.isAssignableFrom(f.getType()));
		List<Field> fields = stream.collect(Collectors.toList());
		if (fields.size() != 1)
			throw new IllegalArgumentException("unable to find level field:" + fields);
		Field field = fields.get(0);
		field.setAccessible(true);
		return field;
	});

	private static final MemoizedSupplier<List<Function<FilterContext, FilterReply>>> FILTER_LIST_S = MemoizedSupplier
			.create(() -> new CopyOnWriteArrayList<>());

	public static boolean addFilter(Function<FilterContext, FilterReply> filter) {
		if (filter == null)
			return false;
		return FILTER_LIST_S.get().add(filter);
	}

	public static boolean removeFilter(Function<FilterContext, FilterReply> filter) {
		if (filter == null)
			return false;
		List<Function<FilterContext, FilterReply>> list = FILTER_LIST_S.getIfLoaded();
		if (list == null)
			return false;
		return list.remove(filter);
	}

	public Level getLevel() {
		try {
			return (Level) LEVEL_FIELD_S.get().get(this);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e) : new RuntimeException(e);
		}
	}

	@Override
	public FilterReply decide(ILoggingEvent event) {
		FilterReply superDecide = super.decide(event);
		if (FilterReply.DENY.equals(superDecide))
			return superDecide;
		if (event == null)
			return FilterReply.NEUTRAL;
		var filterReplyStream = Stream.ofNullable(FILTER_LIST_S.getIfLoaded())
				.flatMap(Collection::stream)
				.map(filter -> {
					try {
						return filter.apply(FilterContextImpl.of(getLevel(), event));
					} catch (Throwable t) {
						t.printStackTrace();
						throw RuntimeException.class.isInstance(t) ? RuntimeException.class.cast(t)
								: new RuntimeException(t);
					}
				});
		return filterReplyStream.filter(Objects::nonNull)
				.filter(Predicate.not(FilterReply.NEUTRAL::equals))
				.findFirst()
				.orElse(FilterReply.NEUTRAL);
	}

	public static interface FilterContext {

		@Value.Parameter(order = 0)
		@Nullable
		public abstract Level getLoggerLevel();

		@Value.Parameter(order = 1)
		@Nullable
		public abstract ILoggingEvent getEvent();

		default Stream<IThrowableProxy> streamThrowableProxies() {
			ILoggingEvent event = getEvent();
			if (event == null)
				return Stream.empty();
			var hasMore = new Predicate<IThrowableProxy>() {

				private List<IThrowableProxy> distinct;

				@Override
				public boolean test(IThrowableProxy throwableProxy) {
					if (throwableProxy == null)
						return false;
					if (distinct == null) {
						distinct = Collections.singletonList(throwableProxy);
						return true;
					}
					if (distinct.stream().anyMatch(v -> throwableProxy == v))
						return false;
					if (distinct.size() == 1)
						distinct = new ArrayList<>(distinct);
					distinct.add(throwableProxy);
					return true;
				}
			};
			return Stream.iterate(event.getThrowableProxy(), hasMore, v -> v.getCause());
		}
	}

	@Value.Immutable
	static abstract class AbstractFilterContextImpl implements FilterContext {}
}
