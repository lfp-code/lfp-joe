package com.lfp.joe.core.config.machine;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class DefaultCharsetLoader extends JavaToolOptionsMachineConfigLoader<Charset> {

	public static final DefaultCharsetLoader INSTANCE = new DefaultCharsetLoader();

	private DefaultCharsetLoader() {
		super("file.encoding", StandardCharsets.UTF_8);
	}

	@Override
	protected Charset parse(String value) throws Exception {
		return Charset.forName(value);
	}

}
