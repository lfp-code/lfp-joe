package com.lfp.joe.core.config;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.Duration;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

enum MachineProperties {
	INSTANCE;

	private final Map<String, PropertyContext<?>> properties = new ConcurrentHashMap<>();
	private boolean logAccess;

	private MachineProperties() {
		var classType = MachineProperties.class;
		var stream = Stream.of(classType.getMethods());
		stream = stream.filter(v -> Modifier.isPublic(v.getModifiers()));
		stream = stream.filter(v -> !Modifier.isStatic(v.getModifiers()));
		stream = stream.filter(v -> v.getParameterCount() == 0);
		stream = stream.filter(v -> !Objects.equals(Object.class, v.getDeclaringClass()));
		stream.forEach(method -> {
			try {
				method.invoke(this);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
						? java.lang.RuntimeException.class.cast(e)
						: new java.lang.RuntimeException(e);
			}
		});
		logAccess = true;
	}

	private <X> X load(String key, Callable<X> loader) {
		return load(key, loader, null);
	}

	@SuppressWarnings("unchecked")
	private <X> X load(String key, Callable<X> loader, Predicate<X> loggingEnabledPredicate) {
		Objects.requireNonNull(loader);
		if (key == null || key.isBlank())
			throw new IllegalArgumentException("non blank key required");
		PropertyContext<X> propertyContext = (PropertyContext<X>) properties.computeIfAbsent(key,
				nil -> new PropertyContext<X>(loader) {

					@Override
					protected boolean logLoad(Optional<X> valueOp) {
						if (!logAccess)
							return false;
						var value = valueOp.orElse(null);
						if (loggingEnabledPredicate != null) {
							var loggingEnabled = loggingEnabledPredicate.test(value);
							if (!loggingEnabled)
								return true;
						}
						var logStr = Objects.toString(value);
						logStr = logStr.replaceAll("\\n", "\\\\n");
						logStr = logStr.replaceAll("\\r", "\\\\r");
						org.slf4j.LoggerFactory.getLogger(MachineProperties.class).info("{}={}", key, logStr);
						return true;
					}
				});
		return propertyContext.getValue();
	}



	public String getLanguageOutput() {
		return load("languageOutput", () -> {
			Locale locale = Locale.getDefault();
			String language = Optional.ofNullable(locale).map(Locale::getLanguage).filter(v -> !v.isBlank())
					.map(String::toLowerCase).orElse("en");
			String country = Optional.ofNullable(locale).map(Locale::getCountry).filter(v -> !v.isBlank())
					.map(String::toUpperCase).orElse("US");
			String charset = Optional.ofNullable(Charset.defaultCharset()).map(Charset::toString)
					.map(String::toUpperCase).orElse("UTF-8");
			String result = String.format("%s_%s.%s", language, country, charset);
			return result;
		});
	}

	public Optional<Class<?>> getMainClass() {
		return load("mainClass", () -> {
			var mainClassList = getMainClassList();
			if (mainClassList.isEmpty())
				return Optional.empty();
			return Optional.of(mainClassList.get(0));
		});
	}

	public List<Class<?>> getMainClassList() {
		return load("mainClassList", () -> {
			Stream<Entry<Thread, List<StackTraceElement>>> entryStream;
			{
				var stream = Thread.getAllStackTraces().entrySet().stream();
				stream = stream.filter(ent -> Objects.nonNull(ent.getKey()));
				stream = stream.filter(ent -> Objects.nonNull(ent.getValue()));
				entryStream = stream.map(ent -> {
					Thread key = ent.getKey();
					List<StackTraceElement> value = Stream.of(ent.getValue()).filter(Objects::nonNull)
							.collect(Collectors.toList());
					return new SimpleEntry<>(key, value);
				});
				entryStream = entryStream.filter(ent -> !ent.getValue().isEmpty());

			}
			{
				entryStream = entryStream.filter(ent -> !ent.getValue().isEmpty());
				Comparator<Thread> threadComparator = Comparator.comparing(v -> 0);
				threadComparator = threadComparator.thenComparing(v -> {
					var threadName = v.getName();
					if (threadName == null)
						return Integer.MAX_VALUE;
					if (threadName.equals("main"))
						return 0;
					if (threadName.equalsIgnoreCase("main"))
						return 1;
					if (threadName.contains("main"))
						return 2;
					if (threadName.toLowerCase().contains("main".toLowerCase()))
						return 3;
					return Integer.MAX_VALUE;
				});
				threadComparator = threadComparator.thenComparing(v -> v.getId());
				threadComparator = threadComparator.thenComparing(v -> v.getPriority() * -1);
				Comparator<Thread> threadComparatorF = threadComparator;
				entryStream = entryStream.sorted((v1, v2) -> {
					return threadComparatorF.compare(v1.getKey(), v2.getKey());
				});
			}
			Map<Thread, List<StackTraceElement>> allStackTraces;
			{
				allStackTraces = new LinkedHashMap<>();
				entryStream.forEach(ent -> allStackTraces.put(ent.getKey(), ent.getValue()));
			}
			Predicate<Method> mainMethodPredicate = Objects::nonNull;
			mainMethodPredicate = mainMethodPredicate.and(v -> v.getName().equals("main"));
			mainMethodPredicate = mainMethodPredicate.and(v -> Modifier.isStatic(v.getModifiers()));
			mainMethodPredicate = mainMethodPredicate.and(v -> v.getParameterCount() == 1);
			mainMethodPredicate = mainMethodPredicate.and(v -> String[].class.equals(v.getParameterTypes()[0]));
			List<Class<?>> mainClassList = new ArrayList<>();
			Supplier<Boolean> shouldQuit = () -> {
				for (var list : allStackTraces.values())
					if (!list.isEmpty())
						return false;
				return true;
			};
			while (!shouldQuit.get())
				for (Thread thread : allStackTraces.keySet()) {
					var stackTrace = allStackTraces.get(thread);
					if (stackTrace.isEmpty())
						continue;
					while (!stackTrace.isEmpty()) {
						StackTraceElement stackElement = stackTrace.remove(stackTrace.size() - 1);
						String stackClassName = stackElement.getClassName();
						if (stackClassName == null)
							continue;
						int splitAt = stackClassName.lastIndexOf("$");
						if (splitAt > 0)
							stackClassName = stackClassName.substring(0, splitAt);
						if (MachineProperties.class.getName().equals(stackClassName))
							continue;
						try {
							Class<?> stackClassType = Class.forName(stackClassName, false,
									Thread.currentThread().getContextClassLoader());
							var mainMethodOp = Stream.of(stackClassType.getDeclaredMethods())
									.filter(mainMethodPredicate).findFirst();
							if (mainMethodOp.isPresent()) {
								mainClassList.add(stackClassType);
								break;
							}
						} catch (Exception ex) {}
					}
				}
			mainClassList = mainClassList.stream().distinct().collect(Collectors.toUnmodifiableList());
			return mainClassList;
		});

	}

	private File getIsDeveloperCacheFile() {
		File file = new File(MachineConfig.getTempDirectory(),
				this.getClass().getName() + File.separator + "is_developer.txt");
		return file;
	}

	public boolean readIsDeveloperCache() {
		return load("readIsDeveloperCache", () -> {
			var file = getIsDeveloperCacheFile();
			if (!file.exists())
				return false;
			if (file.length() <= 0) {
				file.delete();
				return false;
			}
			long elapsed = System.currentTimeMillis() - file.lastModified();
			if (elapsed <= 0 || elapsed > Duration.ofHours(1).toMillis()) {
				file.delete();
				return false;
			}
			String value;
			try {
				value = Files.readString(file.toPath(), StandardCharsets.UTF_8);
			} catch (IOException e) {
				// suppress
				file.delete();
				return false;
			}
			return Boolean.TRUE.toString().equalsIgnoreCase(value);
		}, success -> success);
	}


	private static abstract class PropertyContext<X> {

		private final AtomicBoolean lock = new AtomicBoolean();
		private boolean logComplete;
		private Callable<X> loader;
		private Optional<X> valueOp;

		public PropertyContext(Callable<X> loader) {
			super();
			this.loader = Objects.requireNonNull(loader);
		}

		public X getValue() {
			if (valueOp == null)
				synchronized (this) {
					if (valueOp == null)
						valueOp = loadValue();
				}
			if (!logComplete)
				synchronized (this) {
					if (!logComplete)
						logComplete = logLoad(valueOp);
				}
			return valueOp.orElse(null);
		}

		private Optional<X> loadValue() {
			if (!lock.compareAndSet(false, true))
				throw new IllegalStateException("recursive load");
			Optional<X> result;
			try {
				result = Optional.ofNullable(this.loader.call());
				logLoad(result);
			} catch (Exception e) {
				throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e) : new RuntimeException(e);
			} finally {
				loader = null;
			}
			return result;
		}

		protected abstract boolean logLoad(Optional<X> valueOp);
	}

	public static void main(String[] args) throws IOException {
		System.out.println(MachineConfig.getDefaultCharset());
		System.out.println(MachineConfig.logicalCoreCount());
		System.out.println(MachineConfig.getHostname());
		System.out.println(MachineConfig.getDefaultLineSeperator());
	}

}
