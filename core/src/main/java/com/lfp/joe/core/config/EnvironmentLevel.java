package com.lfp.joe.core.config;

import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Stream;

public enum EnvironmentLevel {
	LOCAL(1000), DEVELOPMENT(2000), INTEGRATION(3000), TESTING(4000), STAGING(5000), PRODUCTION(6000);

	private final int level;

	private EnvironmentLevel(int level) {
		this.level = level;
	}

	public int getLevel() {
		return level;
	}

	public static Stream<EnvironmentLevel> streamValuesDecreasing() {
		return streamValues(Comparator.comparing(el -> el.getLevel() * -1));
	}

	public static Stream<EnvironmentLevel> streamValuesIncreasing() {
		return streamValues(Comparator.comparing(el -> el.getLevel()));
	}

	public static Stream<EnvironmentLevel> streamValues() {
		return streamValues(null);
	}

	public static Stream<EnvironmentLevel> streamValues(Comparator<EnvironmentLevel> sorter) {
		Stream<EnvironmentLevel> stream = Arrays.asList(values()).stream();
		if (sorter != null)
			stream = stream.sorted(sorter);
		return stream;
	}
}
