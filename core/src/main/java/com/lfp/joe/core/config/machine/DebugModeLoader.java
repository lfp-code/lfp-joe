package com.lfp.joe.core.config.machine;

import java.lang.management.ManagementFactory;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class DebugModeLoader extends MachineConfigLoader<Boolean> {

	public static final DebugModeLoader INSTANCE = new DebugModeLoader();

	private DebugModeLoader() {}

	@Override
	protected Iterable<? extends Callable<? extends Boolean>> loaders() {
		return List.of(() -> load());
	}

	private static boolean load() {
		return streamInputArguments().anyMatch(ent -> filter(ent.getKey(), ent.getValue()));
	}

	private static boolean filter(String key, String value) {
		if ("agentlib".equalsIgnoreCase(key) && value.contains("jdwp"))
			return true;
		return false;
	}

	private static Stream<Entry<String, String>> streamInputArguments() {
		var inputArguments = ManagementFactory.getRuntimeMXBean().getInputArguments();
		return Stream.ofNullable(inputArguments).flatMap(Collection::stream).flatMap(v -> parseEntries(v));
	}

	private static Stream<Entry<String, String>> parseEntries(String argument) {
		if (argument == null)
			return Stream.empty();
		var sb = Stream.<Entry<String, String>>builder();
		argument = argument.trim();
		for (var prefixSplit : Map.of("--", "=", "-", ":").entrySet()) {
			var prefix = prefixSplit.getKey();
			var split = prefixSplit.getValue();
			if (!argument.startsWith(prefix))
				continue;
			var splitAt = argument.indexOf(split);
			if (splitAt < 0)
				continue;
			var key = argument.substring(prefix.length(), splitAt);
			if (key.isEmpty())
				continue;
			for (var value : argument.substring(splitAt + split.length()).split(Pattern.quote(","))) {
				value = value.trim();
				if (value.isEmpty())
					continue;
				sb.add(Map.entry(key, value));
			}
		}
		return sb.build();
	}

	public static void main(String[] args) {
		System.out.println(DebugModeLoader.load());
	}

}
