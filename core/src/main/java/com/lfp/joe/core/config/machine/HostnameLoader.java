package com.lfp.joe.core.config.machine;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class HostnameLoader extends MachineConfigLoader<String> {

	public static final HostnameLoader INSTANCE = new HostnameLoader();

	private HostnameLoader() {}

	@Override
	protected Iterable<? extends Callable<? extends String>> loaders() {
		List<Callable<String>> loaders = new ArrayList<>();
		loaders.add(() -> execLinesOut("hostname").flatMap(nonNullOrBlankFlatMap()).map(String::trim).findFirst()
				.orElse(null));
		loaders.add(() -> InetAddress.getLocalHost().getHostName());
		return loaders;

	}

}
