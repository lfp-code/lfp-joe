package com.lfp.joe.core.classpath;

import java.lang.annotation.Annotation;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ServiceLoader.Provider;
import java.util.WeakHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.lfp.joe.core.function.Once.SupplierOnce;

import de.adito.picoservice.IPicoRegistration;
import de.adito.picoservice.IPicoRegistry;

@SuppressWarnings("unchecked")
public class CoreServiceRegistry implements IPicoRegistry {

	private static final SupplierOnce<CoreServiceRegistry> INSTANCE_ONCE = SupplierOnce.of(CoreServiceRegistry::new);

	public static CoreServiceRegistry instance() {
		return INSTANCE_ONCE.get();
	}

	private final Map<Entry<Class<?>, Class<? extends Annotation>>, List<Entry<Class<?>, ? extends Annotation>>> annotationCache = new WeakHashMap<>();
	private final Iterable<Provider<IPicoRegistration>> registrationProviders = new ServiceLoaderIterable<>(
			IPicoRegistration.class);

	protected CoreServiceRegistry() {}

	public Stream<Class<?>> stream() {
		return stream(Object.class);
	}

	public <C> Stream<Class<? extends C>> stream(final Class<C> pSearchedType) {
		if (pSearchedType == null)
			return Stream.empty();
		for (var nct = CoreReflections.getNamedClassType(pSearchedType); !pSearchedType.equals(nct);)
			return stream(nct);
		var registrations = StreamSupport.stream(registrationProviders.spliterator(), false).map(Provider::get);
		var annotatedClassTypes = registrations.map(IPicoRegistration::getAnnotatedClass).distinct();
		annotatedClassTypes = annotatedClassTypes.filter(ct -> {
			return CoreReflections.isAssignableFrom(pSearchedType, ct);
		});
		return annotatedClassTypes.map(v -> (Class<? extends C>) v);
	}

	public <C, A extends Annotation> Stream<Entry<Class<? extends C>, A>> stream(Class<C> pSearchedType,
			Class<A> pAnnotationClass) {
		return stream(pSearchedType, pAnnotationClass, null);
	}

	@SuppressWarnings("rawtypes")
	public <C, A extends Annotation> Stream<Entry<Class<? extends C>, A>> stream(Class<C> pSearchedType,
			Class<A> pAnnotationClass, Comparator<Entry<Class<? extends C>, A>> sorter) {
		if (pSearchedType == null || pAnnotationClass == null)
			return Stream.empty();
		for (var nct = CoreReflections.getNamedClassType(pSearchedType); !pSearchedType.equals(nct);)
			return stream(nct, pAnnotationClass, sorter);
		var ctAnnoList = annotationCache.computeIfAbsent(Map.entry(pSearchedType, pAnnotationClass), nil -> {
			var ctStream = stream(pSearchedType);
			var ctAnnoStream = ctStream.flatMap(ct -> {
				return Stream.ofNullable(ct.getAnnotation(pAnnotationClass))
						.<Entry<Class<?>, ? extends Annotation>>map(v -> {
							return Map.entry(ct, v);
						});
			});
			return ctAnnoStream.collect(Collectors.toUnmodifiableList());
		});
		var estream = ctAnnoList.stream().<Entry<Class<? extends C>, A>>map(v -> (Entry) v);
		if (sorter != null)
			estream = estream.sorted(sorter);
		return estream;
	}

	@Override
	public <T, C> Stream<T> find(Class<C> pSearchedType, Function<Class<? extends C>, T> pResolverFunction) {
		if (pResolverFunction == null)
			return Stream.empty();
		return stream(pSearchedType).map(pResolverFunction);
	}

	@Override
	public <C, A extends Annotation> Map<Class<? extends C>, A> find(Class<C> pSearchedType,
			Class<A> pAnnotationClass) {
		return stream(pSearchedType, pAnnotationClass)
				.collect(Collectors.toUnmodifiableMap(Entry::getKey, Entry::getValue));
	}

}
