package com.lfp.joe.core.config;

import java.io.File;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Optional;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.config.machine.DebugModeLoader;
import com.lfp.joe.core.config.machine.DefaultCharsetLoader;
import com.lfp.joe.core.config.machine.DeveloperLoader;
import com.lfp.joe.core.config.machine.DockerSupportLoader;
import com.lfp.joe.core.config.machine.HostnameLoader;
import com.lfp.joe.core.config.machine.LineSeperatorLoader;
import com.lfp.joe.core.config.machine.LogicalCoreCountLoader;
import com.lfp.joe.core.config.machine.OSNameLoader;
import com.lfp.joe.core.config.machine.RootDirectoryLoader;
import com.lfp.joe.core.config.machine.RootTempDirectoryLoader;
import com.lfp.joe.core.config.machine.TempDirectoryLoader;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.annotation.LocalOnly;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.core.properties.converter.EnumConverter;

@LocalOnly
public interface MachineConfig extends Config {

	static int logicalCoreCount() {
		return LogicalCoreCountLoader.INSTANCE.get();
	}

	static Charset getDefaultCharset() {
		return DefaultCharsetLoader.INSTANCE.get();
	}

	static String getDefaultLineSeperator() {
		return LineSeperatorLoader.INSTANCE.get();
	}

	static Optional<String> getHostname() {
		return Optional.ofNullable(HostnameLoader.INSTANCE.get());
	}

	static boolean isDocker() {
		return DockerSupportLoader.INSTANCE.get();
	}

	static File getRootDirectory() {
		return RootDirectoryLoader.INSTANCE.get();
	}

	static File getTempDirectory() {
		return getTempDirectory(false);
	}

	static File getTempDirectory(boolean root) {
		if (root)
			return RootTempDirectoryLoader.INSTANCE.get();
		return TempDirectoryLoader.INSTANCE.get();
	}

	static boolean isUnix() {
		return OSNameLoader.INSTANCE.isUnix();
	}

	static boolean isWindows() {
		return OSNameLoader.INSTANCE.isWindows();
	}

	static String getLanguageOutput() {
		return MachineProperties.INSTANCE.getLanguageOutput();
	}

	static Optional<Class<?>> getMainClass() {
		return MachineProperties.INSTANCE.getMainClass();
	}

	static List<Class<?>> getMainClassList() {
		return MachineProperties.INSTANCE.getMainClassList();
	}

	static boolean isDeveloper() {
		return DeveloperLoader.INSTANCE.get();
	}

	static boolean isJavaDebugWireProtocol() {
		return DebugModeLoader.INSTANCE.get();
	}

	@ConverterClass(EnumConverter.class)
	@DefaultValue("local")
	EnvironmentLevel environmentLevel();

	@DefaultValue("false")
	boolean disableDevelopment();

	public static void main(String[] args) {
		System.out.println(Charset.defaultCharset());
		Configs.printProperties(PrintOptions.properties());
	}

}