package com.lfp.joe.core.properties.factory;

import java.util.Map;
import java.util.Objects;
import java.util.Properties;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.ConfigCache;
import org.aeonbits.owner.ConfigFactory;
import org.aeonbits.owner.Converter;
import org.aeonbits.owner.Factory;
import org.aeonbits.owner.loaders.Loader;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.properties.loader.EnvironmentVariableLoader;
import com.lfp.joe.core.properties.loader.Loaders;
import com.lfp.joe.core.properties.loader.MultiLoader;
import com.lfp.joe.core.properties.loader.PropertiesFileLoader;
import com.lfp.joe.core.properties.loader.SystemPropertiesLoader;

public class ConfigFactoryLFP implements Factory {

	private static final MemoizedSupplier<ConfigFactoryLFP> INSTANCE_S = MemoizedSupplier.create(ConfigFactoryLFP::new);
	private static final MemoizedSupplier<org.slf4j.Logger> LOGGER_S = MemoizedSupplier.create(() -> {
		Class<?> classType = new Object() {}.getClass().getEnclosingClass();
		return org.slf4j.LoggerFactory.getLogger(classType);
	});

	public static ConfigFactoryLFP get() {
		return INSTANCE_S.get();
	}

	private final MemoizedSupplier<Factory> delegateSupplier = MemoizedSupplier.create(ConfigFactory::newInstance);
	private final MultiLoader multiLoader = new MultiLoader();

	protected ConfigFactoryLFP() {
		System.setProperty("org.aeonbits.owner.property.editor.disabled", Objects.toString(true));
		this.registerLoader(multiLoader);
		// order matters. autowired, files, environment variables
		multiLoader.registerClassTypeLoader(EnvironmentVariableLoader.INSTANCE);
		multiLoader.registerClassTypeLoader(SystemPropertiesLoader.INSTANCE);
		multiLoader.registerClassTypeLoader(PropertiesFileLoader.INSTANCE);
		Loaders.streamAutowiredLoaders().forEach(v -> {
			this.registerLoader(v);
		});
		Loaders.streamAutowiredMultiLoaderClassTypeLoaders().forEach(v -> {
			this.multiLoader.registerClassTypeLoader(v);
		});
	}

	protected Factory getDelegate() {
		return delegateSupplier.get();
	}

	public <T extends Config> T getOrCreate(Class<? extends T> clazz, Map<?, ?>... imports) {
		return ConfigCache.getOrCreate(this, clazz, clazz, imports);
	}

	public <T extends Config> T getOrCreate(Object key, Class<? extends T> clazz, Map<?, ?>... imports) {
		return ConfigCache.getOrCreate(this, key, clazz, imports);
	}

	// delegates

	@Override
	public <T extends Config> T create(Class<? extends T> clazz, Map<?, ?>... imports) {
		return getDelegate().create(clazz, imports);
	}

	@Override
	public String getProperty(String key) {
		return getDelegate().getProperty(key);
	}

	@Override
	public String setProperty(String key, String value) {
		return getDelegate().setProperty(key, value);
	}

	@Override
	public String clearProperty(String key) {
		return getDelegate().clearProperty(key);
	}

	@Override
	public Properties getProperties() {
		return getDelegate().getProperties();
	}

	@Override
	public void setProperties(Properties properties) {
		getDelegate().setProperties(properties);
	}

	@Override
	public void registerLoader(Loader loader) {
		getDelegate().registerLoader(loader);
	}

	@Override
	public void setTypeConverter(Class<?> type, Class<? extends Converter<?>> converter) {
		getDelegate().setTypeConverter(type, converter);
	}

	@Override
	public void removeTypeConverter(Class<?> type) {
		getDelegate().removeTypeConverter(type);
	}

}
