package com.lfp.joe.core.function;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.MethodRequest;
import com.lfp.joe.core.classpath.MemberCache;

public class FNV {

	private static final int FNV_32_INIT = 0x811c9dc5;
	private static final int FNV_32_PRIME = 0x01000193;

	private static final long FNV_64_INIT = 0xcbf29ce484222325L;
	private static final long FNV_64_PRIME = 0x100000001b3L;

	private static final Function<Serializable, Optional<byte[]>> BYTES_CONVERTER;
	static {
		var className = "at.favre.lib.bytes.Bytes";
		var classType = CoreReflections.tryForName(className).orElse(null);
		if (classType == null)
			BYTES_CONVERTER = v -> Optional.empty();
		else {
			var method = MemberCache.tryGetMethod(MethodRequest.of(classType, byte[].class, "array"), false).get();
			BYTES_CONVERTER = v -> {
				if (v == null || !classType.isInstance(v))
					return Optional.empty();
				byte[] barr = (byte[]) Throws.unchecked(() -> method.invoke(v));
				return Optional.of(barr);
			};
		}
	}

	protected FNV() {}

	public static int hash32(Serializable... serializables) {
		return hash32(serializables == null || serializables.length == 0 ? null : Arrays.asList(serializables));
	}

	public static int hash32(Iterable<? extends Serializable> serializables) {
		if (serializables == null)
			return FNV_32_INIT;
		int rv = FNV_32_INIT;
		for (var serializable : serializables) {
			if (serializable == null)
				continue;
			var barr = toByteArray(serializable);
			final int len = barr.length;
			for (int i = 0; i < len; i++) {
				rv ^= barr[i];
				rv *= FNV_32_PRIME;
			}
		}
		return rv;
	}

	public static long hash64(Serializable... serializables) {
		return hash64(serializables == null || serializables.length == 0 ? null : Arrays.asList(serializables));
	}

	public static long hash64(Iterable<? extends Serializable> serializables) {
		if (serializables == null)
			return FNV_64_INIT;
		long rv = FNV_64_INIT;
		for (var serializable : serializables) {
			if (serializable == null)
				continue;
			var barr = toByteArray(serializable);
			final int len = barr.length;
			for (int i = 0; i < len; i++) {
				rv ^= barr[i];
				rv *= FNV_64_PRIME;
			}
		}
		return rv;
	}

	public static MessageDigest messageDigest() {
		return new FNVMessageDigest();
	}

	private static byte[] toByteArray(Serializable serializable) {
		Objects.requireNonNull(serializable);
		if (serializable instanceof byte[])
			return (byte[]) serializable;
		{
			var barr = BYTES_CONVERTER.apply(serializable).orElse(null);
			if (barr != null)
				return barr;
		}
		try (var baos = new ByteArrayOutputStream(); var oos = new ObjectOutputStream(baos)) {
			oos.writeObject(serializable);
			oos.close();
			baos.close();
			return baos.toByteArray();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static byte[] toByteArray(long l) {
		byte[] result = new byte[8];
		for (int i = 7; i >= 0; i--) {
			result[i] = (byte) (l & 0xFF);
			l >>= 8;
		}
		return result;
	}

	private static class FNVMessageDigest extends MessageDigest {

		private long rv;

		protected FNVMessageDigest() {
			super(FNV.class.getSimpleName());
			engineReset();
		}

		@Override
		protected void engineUpdate(byte input) {
			rv ^= input;
			rv *= FNV_64_PRIME;
		}

		@Override
		protected void engineUpdate(byte[] input, int offset, int len) {
			if (offset >= input.length)
				return;
			var limit = Math.min(input.length - offset, len);
			for (int i = offset; i < limit; i++)
				engineUpdate(input[i]);
		}

		@Override
		protected byte[] engineDigest() {
			byte[] barr = toByteArray(rv);
			engineReset();
			return barr;
		}

		@Override
		protected void engineReset() {
			rv = FNV_64_INIT;
		}

	}

	public static void main(String[] args) {
		var hash0 = FNV.hash64(5);
		System.out.println(hash0);
		var hash1 = FNV.hash64(new Date(0), new Date(19));
		System.out.println(hash1);
		System.out.println(Base64.getEncoder().encodeToString(toByteArray(hash1)));
		var md = messageDigest();
		md.update(toByteArray(new Date(0)));
		md.update(toByteArray(new Date(19)));
		System.out.println(Base64.getEncoder().encodeToString(md.digest()));
	}

}
