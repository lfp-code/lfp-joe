package com.lfp.joe.core.config.machine;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class OSNameLoader extends MachineConfigLoader<String> {

	public static final OSNameLoader INSTANCE = new OSNameLoader();

	private OSNameLoader() {}

	@Override
	protected Iterable<? extends Callable<? extends String>> loaders() {
		List<Callable<String>> loaders = new ArrayList<>();
		loaders.add(() -> {
			return System.getProperty("os.name");
		});
		loaders.add(() -> {
			if (execLinesOut("uname", "-a").iterator().hasNext())
				return "Linux";
			return null;
		});
		loaders.add(() -> {
			if (execLinesOut("cmd.exe", "/c", "dir").iterator().hasNext())
				return "Windows";
			return null;
		});
		return loaders;
	}

	@Override
	protected Stream<String> loadStream(Stream<String> stream) {
		return super.loadStream(stream).flatMap(nonNullOrBlankFlatMap());
	}

	private final AtomicReference<Boolean> isUnix = new AtomicReference<>();

	public boolean isUnix() {
		return contains(isUnix, () -> List.of("nix", "nux", "aix"));
	}

	private final AtomicReference<Boolean> isWindows = new AtomicReference<>();

	public boolean isWindows() {
		return contains(isWindows, () -> List.of("windows"));
	}

	private boolean contains(AtomicReference<Boolean> propertyReference, Supplier<Iterable<String>> filterSupplier) {
		return propertyReference.updateAndGet(v -> {
			if (v != null)
				return v;
			var osName = Optional.ofNullable(get()).map(String::toLowerCase).orElse(null);
			if (osName == null)
				return false;
			return stream(filterSupplier.get()).flatMap(nonNullOrBlankFlatMap()).map(String::toLowerCase)
					.anyMatch(filter -> osName.contains(filter));
		});
	}

	public static void main(String[] args) {
		System.out.println(System.getProperty("os.name"));
		System.out.println(OSNameLoader.INSTANCE.get());
		System.out.println(OSNameLoader.INSTANCE.isWindows());
	}
}
