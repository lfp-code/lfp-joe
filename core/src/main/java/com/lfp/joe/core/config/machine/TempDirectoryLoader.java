package com.lfp.joe.core.config.machine;

import java.io.File;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.concurrent.Callable;

public class TempDirectoryLoader extends MachineConfigLoader<File> {

	public static final TempDirectoryLoader INSTANCE = new TempDirectoryLoader();

	private TempDirectoryLoader() {}

	@Override
	protected Iterable<? extends Callable<? extends File>> loaders() {
		return List.of(() -> loadInternal());
	}

	private static File loadInternal() throws NoSuchAlgorithmException {
		var rootTempDir = RootTempDirectoryLoader.INSTANCE.get();
		if (rootTempDir == null)
			return null;
		var executeDir = new File(new File("").getAbsolutePath());
		String hashAppend;
		{
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(executeDir.getAbsolutePath().getBytes(StandardCharsets.UTF_8));
			var hashAppendBuilder = new StringBuilder();
			for (byte aByte : md.digest())
				hashAppendBuilder.append(String.format("%02x", aByte));
			hashAppend = hashAppendBuilder.toString();
		}
		var executeDirNamePrefix = executeDir.getName();
		if (executeDirNamePrefix != null && !executeDirNamePrefix.isBlank()) {
			executeDirNamePrefix = URLEncoder.encode(executeDirNamePrefix, StandardCharsets.UTF_8);
			if (executeDirNamePrefix.length() > 32)
				executeDirNamePrefix = executeDirNamePrefix.substring(0, 32);
			executeDirNamePrefix += "_";
		} else
			executeDirNamePrefix = "";
		var tempDir = new File(rootTempDir, "ephemeral" + File.separator + executeDirNamePrefix + hashAppend);
		tempDir.mkdirs();
		return tempDir;
	}

	public static void main(String[] args) {
		System.out.println(TempDirectoryLoader.INSTANCE.get());
	}
}
