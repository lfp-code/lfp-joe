package com.lfp.joe.core.properties.loader;

import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.aeonbits.owner.loaders.Loader;

import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.core.properties.loader.MultiLoader.ClassTypeLoader;

public class Loaders {

	public static Stream<? extends Loader> streamAutowiredLoaders() {
		return Instances.streamAutowired(Loader.class);
	}

	public static Stream<? extends ClassTypeLoader> streamAutowiredMultiLoaderClassTypeLoaders() {
		return Instances.streamAutowired(MultiLoader.ClassTypeLoader.class);
	}

	public static boolean putAll(Properties result, Iterable<Entry<Method, Set<String>>> publicNoArgMethodEntries,
			Map<?, ?> map) {
		if (result == null || map == null || map.isEmpty())
			return false;
		AtomicBoolean mod = new AtomicBoolean();
		for (Entry<Method, Set<String>> ent : publicNoArgMethodEntries) {
			Set<String> variations = ent.getValue();
			Object value = null;
			for (String variation : variations) {
				Object checkValue = map.get(variation);
				if (Loaders.isValidValue(checkValue)) {
					value = checkValue;
					break;
				}
			}
			if (value == null)
				continue;
			if (Loaders.put(result, variations, value))
				mod.set(true);
		}
		return mod.get();
	}

	public static boolean put(Properties result, Iterable<String> variations, Object value) {
		if (result == null || variations == null || !isValidValue(value))
			return false;
		boolean mod = false;
		Boolean allEqual = null;
		for (String variation : variations)
			synchronized (result) {
				Object currentValue = result.get(variation);
				if (Objects.equals(currentValue, value)) {
					if (allEqual == null)
						allEqual = true;
					continue;
				}
				allEqual = false;
				if (currentValue != null)
					continue;
				result.put(variation, value);
				mod = true;
			}
		if (mod)
			return true;
		if (Boolean.TRUE.equals(allEqual))
			return true;
		return false;
	}

	public static boolean isValidValue(Object value) {
		if (value instanceof Iterable) {
			Iterable<?> ible = (Iterable<?>) value;
			Iterator<?> iter = ible.iterator();
			value = iter == null || !iter.hasNext() ? null : iter.next();
		}
		if ((value == null) || (value instanceof Enum) || (value instanceof String && ((String) value).isEmpty()))
			return false;
		return true;
	}

	public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			var list = Loaders.streamAutowiredLoaders().collect(Collectors.toList());
			System.out.println(list);
		}
	}
}
