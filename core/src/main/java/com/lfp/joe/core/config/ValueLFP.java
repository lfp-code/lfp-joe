package com.lfp.joe.core.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.immutables.value.Value;
import org.immutables.value.Value.Style.BuilderVisibility;
import org.immutables.value.Value.Style.ImplementationVisibility;

public @interface ValueLFP {

	static final String TYPE_ABSTRACT_DEF = "*Def";
	static final String TYPE_ABSTRACT_ABSTRACT = "Abstract*";
	static final String TYPE_IMMUTABLE = "*";

	@Target({ ElementType.PACKAGE, ElementType.TYPE })
	@Retention(RetentionPolicy.CLASS)
	@Value.Style(typeAbstract = { TYPE_ABSTRACT_DEF,
			TYPE_ABSTRACT_ABSTRACT }, typeImmutable = TYPE_IMMUTABLE, visibility = ImplementationVisibility.PUBLIC, builderVisibility = BuilderVisibility.PUBLIC)
	public static @interface Style {

	}

	@Target({ ElementType.PACKAGE, ElementType.TYPE })
	@Retention(RetentionPolicy.CLASS)
	@Value.Style(typeAbstract = { TYPE_ABSTRACT_DEF,
			TYPE_ABSTRACT_ABSTRACT }, typeImmutable = TYPE_IMMUTABLE, visibility = ImplementationVisibility.PUBLIC, builderVisibility = BuilderVisibility.PUBLIC, stagedBuilder = true)
	public static @interface StyleStaged {

	}

	@Target({ ElementType.METHOD })
	@Retention(RetentionPolicy.CLASS)
	public static @interface AllowNulls {

	}

	@Target({ ElementType.METHOD })
	@Retention(RetentionPolicy.CLASS)
	public static @interface SkipNulls {

	}

}
