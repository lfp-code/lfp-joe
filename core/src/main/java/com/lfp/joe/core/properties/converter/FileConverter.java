package com.lfp.joe.core.properties.converter;

import java.io.File;
import java.lang.reflect.Method;

import org.aeonbits.owner.Converter;

public class FileConverter implements Converter<File> {

	@Override
	public File convert(Method method, String input) {
		if (input == null || input.isEmpty())
			return null;
		return new File(input);
	}

}
