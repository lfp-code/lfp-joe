package com.lfp.joe.core.io;

import java.nio.file.OpenOption;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

public class OpenOptions {

	protected OpenOptions() {

	}

	public static Stream<OpenOption> stream(OpenOption... openOptions) {
		return stream(null, openOptions);
	}

	public static Stream<OpenOption> stream(OpenOption openOption, OpenOption... openOptions) {
		Stream<OpenOption> stream = Stream.of(openOption);
		if (openOptions != null)
			stream = Stream.concat(stream, Stream.of(openOptions));
		return stream.filter(Objects::nonNull).distinct();
	}

	public static OpenOption[] array(OpenOption... openOptions) {
		return array(null, openOptions);
	}

	public static OpenOption[] array(OpenOption openOption, OpenOption... openOptions) {
		return stream(openOption, openOptions).toArray(OpenOption[]::new);
	}

	public static Optional<OpenOption[]> normalize(OpenOption... openOptions) {
		var arr = array(openOptions);
		if (openOptions == null || !Arrays.equals(arr, openOptions))
			return Optional.of(arr);
		return Optional.empty();
	}

	public static boolean anyMatch(Class<? extends OpenOption> classType, OpenOption... openOptions) {
		return select(classType, openOptions).findFirst().isPresent();
	}

	public static boolean anyMatch(OpenOption openOption, OpenOption... openOptions) {
		return select(openOption, openOptions).findFirst().isPresent();
	}

	public static <O extends OpenOption> Stream<O> select(Class<? extends O> classType, OpenOption... openOptions) {
		if (classType == null)
			return Stream.empty();
		return stream(openOptions).map(v -> {
			if (!classType.isInstance(v))
				return null;
			return (O) classType.cast(v);
		}).filter(Objects::nonNull);
	}

	@SuppressWarnings("unchecked")
	public static <O extends OpenOption> Stream<O> select(O openOption, OpenOption... openOptions) {
		if (openOption == null)
			return Stream.empty();
		return stream(openOptions).map(v -> {
			if (!openOption.getClass().isInstance(v))
				return null;
			if (!openOption.equals(v))
				return null;
			return (O) v;
		}).filter(Objects::nonNull);
	}

	public static OpenOption[] remove(Class<? extends OpenOption> classType, OpenOption... openOptions) {
		return stream(openOptions).filter(v -> {
			if (classType == null)
				return true;
			if (!classType.isInstance(v))
				return true;
			return false;
		}).toArray(OpenOption[]::new);
	}

	public static void main(String[] args) {
		System.out.println(System.getProperty("sun.java.command"));
		System.out.println(System.getProperty("file.encoding"));
	}
}
