package com.lfp.joe.core.classpath;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.ServiceLoader;
import java.util.ServiceLoader.Provider;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.IntStream;

public class ServiceLoaderIterable<S> implements Iterable<Provider<S>> {

	private final ReentrantReadWriteLock providerLock = new ReentrantReadWriteLock();
	private final List<Optional<Provider<S>>> providerList = new ArrayList<>();
	private final Class<S> serviceType;
	private Iterator<Provider<S>> providerIterator;

	public ServiceLoaderIterable(Class<S> serviceType) {
		super();
		this.serviceType = Objects.requireNonNull(serviceType);
	}

	@Override
	public Iterator<Provider<S>> iterator() {
		return IntStream.range(0, Integer.MAX_VALUE)
				.mapToObj(i -> getNext(i, null))
				.takeWhile(Optional::isPresent)
				.map(Optional::get)
				.iterator();
	}

	private Optional<Provider<S>> getNext(final int index, final Lock writeLock) {
		var lock = Optional.ofNullable(writeLock).orElseGet(providerLock::readLock);
		lock.lock();
		try {
			if (providerList.size() > index)
				return providerList.get(index);
			if (writeLock != null) {
				if (providerIterator == null) {
					var types = new HashSet<Class<?>>();
					providerIterator = CoreReflections.streamDefaultClassLoaders().flatMap(classLoader -> {
						return ServiceLoader.load(serviceType, classLoader).stream();
					}).filter(Objects::nonNull).filter(v -> {
						if (types.add(v.type()))
							return true;
						return false;
					}).iterator();
				}
				Optional<Provider<S>> nextOp;
				if (providerIterator.hasNext())
					nextOp = Optional.ofNullable(providerIterator.next());
				else {
					nextOp = Optional.empty();
					providerIterator = Collections.emptyIterator();
				}
				providerList.add(nextOp);
				return nextOp;
			}
		} finally {
			lock.unlock();
		}
		return getNext(index, providerLock.writeLock());
	}

}
