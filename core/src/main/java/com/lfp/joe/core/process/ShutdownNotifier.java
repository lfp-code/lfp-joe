package com.lfp.joe.core.process;

import java.time.Duration;

import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.function.Throws;

public enum ShutdownNotifier {
	INSTANCE;

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private Scrapable.Impl _scrapable;

	public boolean isShuttingDown() {
		return getScrapable(true).isScrapped();
	}

	public boolean addListener(AutoCloseable listener) {
		if (listener == null)
			return false;
		getScrapable(true).onScrap(listener, Throws.consumerUnchecked(AutoCloseable::close));
		return true;
	}

	public boolean removeListener(AutoCloseable listener) {
		if (listener == null)
			return false;
		var scrapable = getScrapable(false);
		if (scrapable == null)
			return false;
		return scrapable.tryRemove(listener).isPresent();
	}

	private Scrapable.Impl getScrapable(boolean create) {
		if (create && _scrapable == null)
			synchronized (this) {
				if (_scrapable == null) {
					var scrapable = new Scrapable.Impl();
					try {
						Runtime.getRuntime().addShutdownHook(new Thread(scrapable::scrap));
					} catch (IllegalStateException e) {
						scrapable.scrap();
					}
					_scrapable = scrapable;
				}
			}
		return _scrapable;
	}

	public static void main(String[] args) throws InterruptedException {
		AutoCloseable listener = () -> {
			System.out.println("shutting down 1");
		};
		ShutdownNotifier.INSTANCE.addListener(listener);
		ShutdownNotifier.INSTANCE.addListener(() -> {
			System.out.println("shutting down 2");
		});
		Thread.sleep(Duration.ofSeconds(1).toMillis());
		ShutdownNotifier.INSTANCE.removeListener(listener);
		System.exit(0);
	}

}
