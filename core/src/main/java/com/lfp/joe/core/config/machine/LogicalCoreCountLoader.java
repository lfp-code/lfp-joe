package com.lfp.joe.core.config.machine;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class LogicalCoreCountLoader extends MachineConfigLoader<Integer> {

	public static final LogicalCoreCountLoader INSTANCE = new LogicalCoreCountLoader();

	private LogicalCoreCountLoader() {}

	@Override
	protected Iterable<? extends Callable<? extends Integer>> loaders() {
		var availableProcessors = Runtime.getRuntime().availableProcessors();
		var loaders = new ArrayList<Entry<String, Callable<Integer>>>();
		loaders.add(Map.entry("cpuinfo", () -> loadCPUInfo()));
		loaders.add(Map.entry("nproc", () -> loadNProc()));
		loaders.add(Map.entry("env", () -> loadNumberOfProcessors()));
		loaders.add(Map.entry("cpuset", () -> loadCpuSet()));
		loaders.add(Map.entry("jvm", () -> availableProcessors));
		return List.of(() -> {
			Stream<Entry<String, Integer>> estream = loaders.stream().map(ent -> {
				try {
					var result = ent.getValue().call();
					if (filter(result))
						return Map.entry(ent.getKey(), result);
				} catch (Exception e) {
					// suppress
				}
				return null;
			});
			estream = estream.filter(Objects::nonNull);
			Comparator<Entry<String, Integer>> sorter = Comparator.comparing(ent -> -1 * ent.getValue());
			sorter = sorter.thenComparing(ent -> ent.getKey().toLowerCase());
			estream = estream.sorted(sorter);
			var elist = estream.collect(Collectors.toList());
			logSystemOut(elist.stream().map(ent -> String.format("%s:%s", ent.getKey(), ent.getValue()))
					.collect(Collectors.joining(" ")));
			return elist.get(0).getValue();
		});
	}

	@Override
	protected Stream<Integer> loadStream(Stream<Integer> stream) {
		stream = stream.sorted(Comparator.reverseOrder());
		return stream;
	}

	private static Integer loadCPUInfo() throws IOException {
		var lines = execLinesOut("cat", absolutePath("/proc/cpuinfo"));
		lines = lines.map(String::trim);
		var matchPredicate = Pattern.compile("processor\\s*?:.*").asMatchPredicate();
		lines = lines.filter(matchPredicate);
		return (int) lines.count();
	}

	private static Integer loadNProc() throws IOException {
		var lines = execLinesOut("nproc");
		var resultOp = lines.flatMap(parseIntFlatMap()).mapToInt(Integer::intValue).max();
		return resultOp.isPresent() ? resultOp.getAsInt() : null;
	}

	private static Integer loadNumberOfProcessors() throws IOException {
		return parseInt(System.getenv("NUMBER_OF_PROCESSORS"));
	}

	private static Integer loadCpuSet() throws IOException {
		var stream = execLinesOut("cat", absolutePath("/sys/fs/cgroup/cpuset/cpuset.cpus"));
		stream = stream.map(String::trim);
		stream = stream.flatMap(v -> Stream.of(v.split(",")));
		stream = stream.map(String::trim);
		Function<String, Integer> parseIndexRange = v -> {
			var parts = v.split("-");
			if (parts.length == 0 || parts.length > 2)
				return null;
			var sb = Stream.<Integer>builder();
			sb.add(parseInt(parts[0]));
			if (parts.length == 2)
				sb.add(parseInt(parts[1]));
			var indexArr = sb.build().filter(Objects::nonNull).distinct().mapToInt(Integer::intValue).toArray();
			if (indexArr.length == 0)
				return null;
			if (indexArr.length == 1)
				return 1;
			var minIndex = IntStream.of(indexArr).min().getAsInt();
			var maxIndex = IntStream.of(indexArr).max().getAsInt();
			return (maxIndex - minIndex) + 1;
		};
		var coreStream = stream.map(parseIndexRange).filter(Objects::nonNull).mapToInt(Integer::intValue);
		return coreStream.max().orElse(-1);
	}

	private static boolean filter(Integer value) {
		if (value == null || value <= 0)
			return false;
		if (value >= (Runtime.getRuntime().availableProcessors() * 10))
			return false;
		return true;
	}

	public static void main(String[] args) {
		System.out.println(LogicalCoreCountLoader.INSTANCE.get());
	}

}
