package com.lfp.joe.core.process.future;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;

import com.lfp.joe.core.function.Once.SupplierOnce;

public interface CSFuture<X> extends Future<X>, CompletionStage<X> {

	default Executor defaultExecutor() {
		return Const.DEFAULT_EXECUTOR_SUPPLIER.get();
	}

	public static class Completable<X> extends CompletableFuture<X> implements CSFuture<X> {

		private final boolean originCancellation;

		public Completable() {
			this(false);
		}

		protected Completable(boolean originCancellation) {
			super();
			this.originCancellation = originCancellation;
		}

		@Override
		public <U> CSFuture.Completable<U> newIncompleteFuture() {
			return newIncompleteFuture(this.originCancellation);
		}

		public CSFuture.Completable<X> withOriginCancellation(boolean originCancellation) {
			if (this.originCancellation == originCancellation)
				return this;
			var dependent = this.<X>newIncompleteFuture(originCancellation);
			this.whenComplete((v, t) -> {
				com.lfp.joe.core.process.future.Completion.of(v, t)
						.complete(dependent::complete, dependent::completeExceptionally);
			});
			return dependent;
		}

		protected <U> CSFuture.Completable<U> newIncompleteFuture(boolean originCancellation) {
			if (!originCancellation)
				return new CSFuture.Completable<U>();
			var dependent = new CSFuture.Completable<U>(true);
			dependent.whenComplete((v, t) -> {
				com.lfp.joe.core.process.future.Completion.of(v, t)
						.complete(dependent::complete, dependent::completeExceptionally);
			});
			return dependent;
		}

		@Override
		public boolean cancel(boolean mayInterruptIfRunning) {
			if (mayInterruptIfRunning && super.completeExceptionally(new InterruptCancellationException()))
				return true;
			return super.cancel(mayInterruptIfRunning);
		}

		@Override
		public CSFuture.Completable<X> toCompletableFuture() {
			return this;
		}

	}

	static enum Const {
		;

		private static final SupplierOnce<Executor> DEFAULT_EXECUTOR_SUPPLIER = SupplierOnce.of(() -> {
			return CompletableFuture.completedFuture(null).defaultExecutor();
		});

	}

}
