package com.lfp.joe.core.config;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;

import com.lfp.joe.core.classpath.CoreReflections;

public class DevLogger {

	public static DevLogger create() {
		return create(null);
	}

	public static DevLogger create(String name) {
		return new DevLogger(name, null);
	}

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger DEFAULT_LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private final Class<?> callingClass;
	private Boolean _enabled;
	private Optional<String> _nameOp;
	private Logger _logger;
	private long startedAt;
	private Long lastLog;

	public DevLogger(String name, Logger logger) {
		name = trimToNull(name);
		if (name != null)
			this._nameOp = Optional.of(name);
		this._logger = logger;
		if (MachineConfig.isDeveloper() && (this._nameOp == null || this._logger == null))
			this.callingClass = CoreReflections.getCallingClass(THIS_CLASS);
		else
			this.callingClass = null;
		this.startedAt = System.currentTimeMillis();
	}

	protected Optional<String> getName() {
		if (this._nameOp == null)
			synchronized (this) {
				if (this._nameOp == null) {
					String name = null;
					if (this.callingClass != null)
						name = this.callingClass.getSimpleName();
					name = trimToNull(name);
					this._nameOp = Optional.ofNullable(name);
				}
			}
		return this._nameOp;
	}

	protected Logger getLogger() {
		if (_logger == null && this.callingClass != null)
			synchronized (this) {
				if (_logger == null && this.callingClass != null)
					this._logger = org.slf4j.LoggerFactory.getLogger(this.callingClass);
			}
		if (this._logger == null)
			return DEFAULT_LOGGER;
		return this._logger;
	}

	public <X> X log(X passThrough, boolean reset, String message, Object... arguments) {
		if (reset)
			this.reset();
		this.log(message, arguments);
		return passThrough;
	}

	public DevLogger log(String message, Object... arguments) {
		if (!this.isEnabled())
			return this;
		long now = System.currentTimeMillis();
		Stream<String> messageParts = Stream.empty();
		if (this.getName().isPresent())
			messageParts = Stream.concat(messageParts, Stream.of(String.format("[%s]", this.getName().get())));
		messageParts = Stream.concat(messageParts, Stream.of(message));
		long elapsedTotal = now - startedAt;
		messageParts = Stream.concat(messageParts, Stream.of(String.format("elapsedTotal:%s", elapsedTotal)));
		Long elapsedDelta = lastLog == null ? null : (now - lastLog);
		if (elapsedDelta != null)
			messageParts = Stream.concat(messageParts, Stream.of(String.format("elapsedDelta:%s", elapsedDelta)));
		var logMessage = messageParts.map(v -> trimToNull(v)).filter(Predicate.not(Objects::isNull))
				.collect(Collectors.joining(" "));
		getLogger().info(logMessage, arguments);
		lastLog = now;
		return this;
	}

	public DevLogger reset() {
		this.startedAt = System.currentTimeMillis();
		return this;
	}

	public boolean isEnabled() {
		var result = this._enabled;
		if (result == null)
			result = MachineConfig.isDeveloper();
		return result;
	}

	public DevLogger disable() {
		this._enabled = false;
		return this;
	}

	public DevLogger enable() {
		this._enabled = true;
		return this;
	}

	private static String trimToNull(String str) {
		return Optional.ofNullable(str).map(String::trim).filter(v -> !v.isEmpty()).orElse(null);
	}

}
