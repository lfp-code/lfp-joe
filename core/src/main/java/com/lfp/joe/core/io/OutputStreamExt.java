package com.lfp.joe.core.io;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Optional;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

public class OutputStreamExt extends OutputStream {

	public static OutputStreamExt from(OutputStream outputStream) {
		if (outputStream instanceof OutputStreamExt)
			return (OutputStreamExt) outputStream;
		return new OutputStreamExt(() -> outputStream, true);
	}

	private final ThrowingSupplier<OutputStream, IOException> outputStreamSupplier;
	private final boolean disableNullOutputStreamOnClose;
	private boolean closeDisabled = false;
	private boolean closed;
	private OutputStream _delegateOutputStream;

	public OutputStreamExt(ThrowingSupplier<OutputStream, IOException> outputStreamSupplier) {
		this(outputStreamSupplier, false);
	}

	protected OutputStreamExt(ThrowingSupplier<OutputStream, IOException> outputStreamSupplier,
			boolean disableNullOutputStreamOnClose) {
		super();
		this.outputStreamSupplier = outputStreamSupplier;
		this.disableNullOutputStreamOnClose = disableNullOutputStreamOnClose;
	}

	protected OutputStream getDelegateOutputStream() throws IOException {
		if (_delegateOutputStream == null)
			synchronized (this) {
				if (_delegateOutputStream == null) {
					var delegate = outputStreamSupplier == null ? null : outputStreamSupplier.get();
					if (delegate == null)
						delegate = OutputStream.nullOutputStream();
					_delegateOutputStream = delegate;
				}
			}
		return _delegateOutputStream;
	}

	@Override
	public void close() throws IOException {
		if (isCloseDisabled())
			return;
		this.closed = true;
		if (!disableNullOutputStreamOnClose && _delegateOutputStream == null)
			synchronized (this) {
				if (!disableNullOutputStreamOnClose && _delegateOutputStream == null)
					_delegateOutputStream = OutputStream.nullOutputStream();
			}
		_delegateOutputStream.close();
	}

	public boolean isClosed() {
		return closed;
	}

	public boolean isCloseDisabled() {
		return closeDisabled;
	}

	public void setCloseDisabled(boolean closeDisabled) {
		this.closeDisabled = closeDisabled;
	}

	public BufferedWriterExt writer() {
		return writer(null, null);
	}

	public BufferedWriterExt writer(Charset charset, Integer bufferSize) {
		if (charset == null)
			charset = MachineConfig.getDefaultCharset();
		var writeer = new OutputStreamWriter(this, charset);
		BufferedWriterExt bufferedWriter;
		if (bufferSize == null)
			bufferedWriter = new BufferedWriterExt(writeer);
		else
			bufferedWriter = new BufferedWriterExt(writeer, bufferSize);
		return bufferedWriter;
	}

	// *** delegates ***

	@Override
	public void write(int b) throws IOException {
		getDelegateOutputStream().write(b);
	}

	@Override
	public void write(byte[] b) throws IOException {
		getDelegateOutputStream().write(b);
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		getDelegateOutputStream().write(b, off, len);
	}

	@Override
	public void flush() throws IOException {
		getDelegateOutputStream().flush();
	}

	public static class BufferedWriterExt extends BufferedWriter {

		public BufferedWriterExt(Writer out) {
			super(out);
		}

		public BufferedWriterExt(Writer out, int sz) {
			super(out, sz);
		}

		public void lines(Iterable<? extends CharSequence> lineIterable) throws IOException {
			var lineIterator = Optional.ofNullable(lineIterable).map(Iterable::iterator)
					.orElseGet(Collections::emptyIterator);
			while (lineIterator.hasNext()) {
				var line = Optional.ofNullable(lineIterator.next()).map(Object::toString).orElse(null);
				this.write(line);
				this.newLine();
			}
		}
	}

}
