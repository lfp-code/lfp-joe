package com.lfp.joe.core.process.executor;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinPool.ForkJoinWorkerThreadFactory;
import java.util.concurrent.ForkJoinWorkerThread;
import java.util.concurrent.ThreadFactory;
import java.util.function.Consumer;

import org.immutables.value.Value;

import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.process.ThreadRenamer;

@ValueLFP.Style
@Value.Immutable
abstract class AbstractThreadFunction implements java.util.concurrent.ThreadFactory, ForkJoinWorkerThreadFactory {

	@Value.Default
	public ThreadFactory threadFactory() {
		return Executors.defaultThreadFactory();
	}

	@Value.Default
	public ForkJoinWorkerThreadFactory forkJoinWorkerThreadFactory() {
		return ForkJoinPool.defaultForkJoinWorkerThreadFactory;
	}

	@Value.Default
	public List<Consumer<Thread>> threadModifiers() {
		var threadRenamer = ThreadRenamer.of(ThreadFactory.class.getSimpleName());
		return List.of(threadRenamer);
	}

	@Override
	public Thread newThread(Runnable runnable) {
		var thread = threadFactory().newThread(runnable);
		configure(thread);
		return thread;
	}

	@Override
	public ForkJoinWorkerThread newThread(ForkJoinPool pool) {
		var thread = forkJoinWorkerThreadFactory().newThread(pool);
		configure(thread);
		return thread;
	}

	protected void configure(Thread thread) {
		thread.setDaemon(true);
		threadModifiers().forEach(v -> v.accept(thread));
	}

}
