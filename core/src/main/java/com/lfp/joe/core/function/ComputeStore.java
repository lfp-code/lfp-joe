package com.lfp.joe.core.function;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.WeakHashMap;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

import com.lfp.joe.core.lock.KeyLock;

public interface ComputeStore<K, V> {

	default V computeIfAbsent(K key, Supplier<? extends V> mappingSupplier) {
		return computeIfAbsent(key, mappingSupplier == null ? null : nil -> mappingSupplier.get());
	}

	V computeIfAbsent(K key, Function<? super K, ? extends V> mappingFunction);

	default V computeIfPresent(K key, Supplier<? extends V> mappingSupplier) {
		return computeIfPresent(key, mappingSupplier == null ? null : (nilk, nilv) -> mappingSupplier.get());
	}

	V computeIfPresent(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction);

	V compute(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction);

	/*
	 * creators
	 */

	public static <K, V> ComputeStore<K, V> create() {
		return from(new HashMap<>());
	}

	public static <K, V> ComputeStore<K, V> from(Map<K, V> map) {
		BiFunction<? super K, ? super V, ? extends V> identityFunction = (k, v) -> v;
		return from(map, identityFunction, identityFunction);
	}

	public static <K, V, R extends Reference<V>> ComputeStore<K, V> from(Map<K, R> map,
			Function<? super V, ? extends R> referenceFunction) {
		Objects.requireNonNull(referenceFunction);
		return from(map, (k, r) -> r.get(), (k, v) -> {
			return v == null ? null : referenceFunction.apply(v);
		});
	}

	public static <K, V, S> ComputeStore<K, V> from(Map<K, S> map,
			BiFunction<? super K, ? super S, ? extends V> readFunction,
			BiFunction<? super K, ? super V, ? extends S> writeFunction) {
		Objects.requireNonNull(map);
		Objects.requireNonNull(readFunction);
		Objects.requireNonNull(writeFunction);
		return new Abs<K, V>() {

			@Override
			protected Optional<V> read(K key) {
				var valueStore = map.get(key);
				if (valueStore == null)
					return null;
				return Optional.ofNullable(readFunction.apply(key, valueStore));
			}

			@Override
			protected void write(K key, V value) {
				var valueStore = writeFunction.apply(key, value);
				if (valueStore == null)
					map.remove(key);
				else
					map.put(key, valueStore);
			}
		};
	}

	/*
	 * implementations
	 */

	static abstract class Abs<K, V> implements ComputeStore<K, V> {

		private final KeyLock<K> keyLock;

		protected Abs() {
			this(KeyLock.create());
		}

		protected Abs(KeyLock<K> keyLock) {
			super();
			this.keyLock = Objects.requireNonNull(keyLock);
		}

		@Override
		public V computeIfAbsent(K key, Function<? super K, ? extends V> mappingFunction) {
			Objects.requireNonNull(mappingFunction);
			var valueOp = read(key);
			if (valueOp != null && valueOp.isPresent())
				return valueOp.get();
			return compute(key, (k, v) -> {
				if (v != null)
					return v;
				return mappingFunction.apply(k);
			});
		}

		@Override
		public V computeIfPresent(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
			Objects.requireNonNull(remappingFunction);
			var valueOp = read(key);
			if (valueOp == null)
				return null;
			// compute on empty optional (ex: Soft/Weak reference)
			return compute(key, (k, v) -> {
				if (v == null)
					return null;
				return remappingFunction.apply(k, v);
			});
		}

		@Override
		public V compute(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
			Objects.requireNonNull(remappingFunction);
			var lock = keyLock.writeLock(key);
			lock.lock();
			try {
				var valueOp = read(key);
				var newValue = remappingFunction.apply(key, valueOp == null ? null : valueOp.orElse(null));
				if (newValue == null) {
					// delete mapping
					if (valueOp != null) {
						// something to remove
						write(key, null);
						return null;
					} else
						// nothing to do. Leave things as they were.
						return null;
				} else {
					// add or replace old mapping
					write(key, newValue);
					return newValue;
				}
			} finally {
				lock.unlock();
			}
		}

		protected abstract Optional<V> read(K key);

		protected abstract void write(K key, V value);

	}

	public static void main(String[] args) {
		var computeStore = ComputeStore.from(new WeakHashMap<String, Reference<Date>>(), SoftReference::new);
		System.out.println(computeStore.computeIfAbsent("neat", nil -> {
			System.out.println("computing 0");
			return new Date(0);
		}));
		System.gc();
		Runtime.getRuntime().gc();
		System.out.println(computeStore.computeIfAbsent("neat", nil -> {
			System.out.println("computing 1");
			return new Date();
		}));
		System.out.println(computeStore);
	}
}
