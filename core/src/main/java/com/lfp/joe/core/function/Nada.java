package com.lfp.joe.core.function;

import java.util.Objects;

public final class Nada {

	private static final Nada INSTANCE = new Nada();

	public static Nada get() {
		return INSTANCE;
	}

	public static <X> Nada map(X object) {
		return Nada.get();
	}

	private Nada() {
	}

	@Override
	public int hashCode() {
		return Objects.hash((Object) null);
	}

	@Override
	public boolean equals(Object obj) {
		if ((obj == null) || (obj == this) || (obj instanceof Nada))
			return true;
		return false;
	}

	@Override
	public String toString() {
		return "[nada]";
	}

}
