package com.lfp.joe.core.log;

import org.slf4j.Logger;
import org.slf4j.Marker;


public interface DelegateLogger extends Logger {

	Logger getDelegate();

	@Override
	default String getName() {
		return getDelegate().getName();
	}

	@Override
	default boolean isTraceEnabled() {
		return getDelegate().isTraceEnabled();
	}

	@Override
	default void trace(String msg) {
		getDelegate().trace(msg);
	}

	@Override
	default void trace(String format, Object arg) {
		getDelegate().trace(format, arg);
	}

	@Override
	default void trace(String format, Object arg1, Object arg2) {
		getDelegate().trace(format, arg1, arg2);
	}

	@Override
	default void trace(String format, Object... arguments) {
		getDelegate().trace(format, arguments);
	}

	@Override
	default void trace(String msg, Throwable t) {
		getDelegate().trace(msg, t);
	}

	@Override
	default boolean isTraceEnabled(Marker marker) {
		return getDelegate().isTraceEnabled(marker);
	}

	@Override
	default void trace(Marker marker, String msg) {
		getDelegate().trace(marker, msg);
	}

	@Override
	default void trace(Marker marker, String format, Object arg) {
		getDelegate().trace(marker, format, arg);
	}

	@Override
	default void trace(Marker marker, String format, Object arg1, Object arg2) {
		getDelegate().trace(marker, format, arg1, arg2);
	}

	@Override
	default void trace(Marker marker, String format, Object... argArray) {
		getDelegate().trace(marker, format, argArray);
	}

	@Override
	default void trace(Marker marker, String msg, Throwable t) {
		getDelegate().trace(marker, msg, t);
	}

	@Override
	default boolean isDebugEnabled() {
		return getDelegate().isDebugEnabled();
	}

	@Override
	default void debug(String msg) {
		getDelegate().debug(msg);
	}

	@Override
	default void debug(String format, Object arg) {
		getDelegate().debug(format, arg);
	}

	@Override
	default void debug(String format, Object arg1, Object arg2) {
		getDelegate().debug(format, arg1, arg2);
	}

	@Override
	default void debug(String format, Object... arguments) {
		getDelegate().debug(format, arguments);
	}

	@Override
	default void debug(String msg, Throwable t) {
		getDelegate().debug(msg, t);
	}

	@Override
	default boolean isDebugEnabled(Marker marker) {
		return getDelegate().isDebugEnabled(marker);
	}

	@Override
	default void debug(Marker marker, String msg) {
		getDelegate().debug(marker, msg);
	}

	@Override
	default void debug(Marker marker, String format, Object arg) {
		getDelegate().debug(marker, format, arg);
	}

	@Override
	default void debug(Marker marker, String format, Object arg1, Object arg2) {
		getDelegate().debug(marker, format, arg1, arg2);
	}

	@Override
	default void debug(Marker marker, String format, Object... arguments) {
		getDelegate().debug(marker, format, arguments);
	}

	@Override
	default void debug(Marker marker, String msg, Throwable t) {
		getDelegate().debug(marker, msg, t);
	}

	@Override
	default boolean isInfoEnabled() {
		return getDelegate().isInfoEnabled();
	}

	@Override
	default void info(String msg) {
		getDelegate().info(msg);
	}

	@Override
	default void info(String format, Object arg) {
		getDelegate().info(format, arg);
	}

	@Override
	default void info(String format, Object arg1, Object arg2) {
		getDelegate().info(format, arg1, arg2);
	}

	@Override
	default void info(String format, Object... arguments) {
		getDelegate().info(format, arguments);
	}

	@Override
	default void info(String msg, Throwable t) {
		getDelegate().info(msg, t);
	}

	@Override
	default boolean isInfoEnabled(Marker marker) {
		return getDelegate().isInfoEnabled(marker);
	}

	@Override
	default void info(Marker marker, String msg) {
		getDelegate().info(marker, msg);
	}

	@Override
	default void info(Marker marker, String format, Object arg) {
		getDelegate().info(marker, format, arg);
	}

	@Override
	default void info(Marker marker, String format, Object arg1, Object arg2) {
		getDelegate().info(marker, format, arg1, arg2);
	}

	@Override
	default void info(Marker marker, String format, Object... arguments) {
		getDelegate().info(marker, format, arguments);
	}

	@Override
	default void info(Marker marker, String msg, Throwable t) {
		getDelegate().info(marker, msg, t);
	}

	@Override
	default boolean isWarnEnabled() {
		return getDelegate().isWarnEnabled();
	}

	@Override
	default void warn(String msg) {
		getDelegate().warn(msg);
	}

	@Override
	default void warn(String format, Object arg) {
		getDelegate().warn(format, arg);
	}

	@Override
	default void warn(String format, Object... arguments) {
		getDelegate().warn(format, arguments);
	}

	@Override
	default void warn(String format, Object arg1, Object arg2) {
		getDelegate().warn(format, arg1, arg2);
	}

	@Override
	default void warn(String msg, Throwable t) {
		getDelegate().warn(msg, t);
	}

	@Override
	default boolean isWarnEnabled(Marker marker) {
		return getDelegate().isWarnEnabled(marker);
	}

	@Override
	default void warn(Marker marker, String msg) {
		getDelegate().warn(marker, msg);
	}

	@Override
	default void warn(Marker marker, String format, Object arg) {
		getDelegate().warn(marker, format, arg);
	}

	@Override
	default void warn(Marker marker, String format, Object arg1, Object arg2) {
		getDelegate().warn(marker, format, arg1, arg2);
	}

	@Override
	default void warn(Marker marker, String format, Object... arguments) {
		getDelegate().warn(marker, format, arguments);
	}

	@Override
	default void warn(Marker marker, String msg, Throwable t) {
		getDelegate().warn(marker, msg, t);
	}

	@Override
	default boolean isErrorEnabled() {
		return getDelegate().isErrorEnabled();
	}

	@Override
	default void error(String msg) {
		getDelegate().error(msg);
	}

	@Override
	default void error(String format, Object arg) {
		getDelegate().error(format, arg);
	}

	@Override
	default void error(String format, Object arg1, Object arg2) {
		getDelegate().error(format, arg1, arg2);
	}

	@Override
	default void error(String format, Object... arguments) {
		getDelegate().error(format, arguments);
	}

	@Override
	default void error(String msg, Throwable t) {
		getDelegate().error(msg, t);
	}

	@Override
	default boolean isErrorEnabled(Marker marker) {
		return getDelegate().isErrorEnabled(marker);
	}

	@Override
	default void error(Marker marker, String msg) {
		getDelegate().error(marker, msg);
	}

	@Override
	default void error(Marker marker, String format, Object arg) {
		getDelegate().error(marker, format, arg);
	}

	@Override
	default void error(Marker marker, String format, Object arg1, Object arg2) {
		getDelegate().error(marker, format, arg1, arg2);
	}

	@Override
	default void error(Marker marker, String format, Object... arguments) {
		getDelegate().error(marker, format, arguments);
	}

	@Override
	default void error(Marker marker, String msg, Throwable t) {
		getDelegate().error(marker, msg, t);
	}
}
