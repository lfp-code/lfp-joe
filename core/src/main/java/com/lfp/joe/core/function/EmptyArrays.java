package com.lfp.joe.core.function;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.Primitives;

public class EmptyArrays {

	protected EmptyArrays() {}

	private static final Map<Class<?>, Object> INSTANCE_MAP = new ConcurrentHashMap<>();

	@SuppressWarnings("unchecked")
	public static <U> U[] of(Class<U> componentType) {
		Objects.requireNonNull(componentType);
		Class<U> namedClassType = CoreReflections.getNamedClassTypeIfVaries(componentType).orElse(null);
		if (namedClassType != null)
			return of(namedClassType);
		var wrapperTypeOp = Primitives.getWrapperType(componentType).filter(Predicate.not(componentType::equals));
		if (wrapperTypeOp.isPresent())
			return (U[]) of(wrapperTypeOp.get());
		return (U[]) INSTANCE_MAP.computeIfAbsent(componentType, nil -> Array.newInstance(componentType, 0));
	}

	public static boolean[] ofBoolean() {
		return (boolean[]) INSTANCE_MAP.computeIfAbsent(boolean.class, nil -> {
			return new boolean[0];
		});
	}

	public static byte[] ofByte() {
		return (byte[]) INSTANCE_MAP.computeIfAbsent(byte.class, nil -> {
			return new byte[0];
		});
	}

	public static char[] ofChar() {
		return (char[]) INSTANCE_MAP.computeIfAbsent(char.class, nil -> {
			return new char[0];
		});
	}

	public static double[] ofDouble() {
		return (double[]) INSTANCE_MAP.computeIfAbsent(double.class, nil -> {
			return new double[0];
		});
	}

	public static float[] ofFloat() {
		return (float[]) INSTANCE_MAP.computeIfAbsent(float.class, nil -> {
			return new float[0];
		});
	}

	public static int[] ofInt() {
		return (int[]) INSTANCE_MAP.computeIfAbsent(int.class, nil -> {
			return new int[0];
		});
	}

	public static long[] ofLong() {
		return (long[]) INSTANCE_MAP.computeIfAbsent(long.class, nil -> {
			return new long[0];
		});
	}

	public static short[] ofShort() {
		return (short[]) INSTANCE_MAP.computeIfAbsent(short.class, nil -> {
			return new short[0];
		});
	}

	public static void main(String[] args) {
		System.out.println(Arrays.toString(ofLong()) + " - " + ofLong().getClass());
		System.out.println(Arrays.toString(of(String.class)) + " - " + of(String.class).getClass());
	}

}
