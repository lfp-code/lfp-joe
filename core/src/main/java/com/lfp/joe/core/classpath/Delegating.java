package com.lfp.joe.core.classpath;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.lfp.joe.core.classpath.ImmutableInvocationHandlerLFP.InvocationHandlerRequest;
import com.lfp.joe.core.function.DistinctInstancePredicate;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

public interface Delegating {

	List<Object> getTargets();

	default <U> Stream<? extends Object> streamTargets() {
		return streamTargets(Object.class);
	}

	default <U> Stream<? extends U> streamTargets(Class<U> classType) {
		return streamTargets(this, classType, DistinctInstancePredicate.create());
	}

	public static Optional<List<Object>> getTargets(Object object) {
		if (!(object instanceof Delegating))
			return Optional.empty();
		return Optional.of(((Delegating) object).getTargets());
	}

	public static Stream<? extends Object> streamTargets(Object object) {
		return streamTargets(object, Object.class);
	}

	public static <U> Stream<? extends U> streamTargets(Object object, Class<U> classType) {
		if (!(object instanceof Delegating))
			return Stream.empty();
		return ((Delegating) object).streamTargets(classType);
	}

	private static <U> Stream<? extends U> streamTargets(Delegating delegating, Class<U> classType,
			DistinctInstancePredicate<U> distinctFilter) {
		if (classType == null)
			return Stream.empty();
		var targets = delegating.getTargets();
		if (targets == null)
			return Stream.empty();
		Supplier<Stream<? extends U>> targetStreamer = () -> targets.stream()
				.map(v -> CoreReflections.tryCast(v, classType).orElse(null))
				.filter(Objects::nonNull);
		var targetStream = targetStreamer.get().filter(distinctFilter);
		var targetStreamAppend = targetStreamer.get()
				.map(v -> CoreReflections.tryCast(v, Delegating.class).orElse(null))
				.filter(Objects::nonNull)
				.<U>flatMap(v -> streamTargets(delegating, classType, distinctFilter));
		return Stream.concat(targetStream, targetStreamAppend);
	}

	public static boolean isGetTargetsMethod(Method method) {
		if (method == null)
			return false;
		if (!Delegating.class.isAssignableFrom(method.getDeclaringClass()))
			return false;
		return CoreReflections.compatibleMethodInvoke(method, Const.getTargets_METHOD);
	}

	public static DelegatingInvocationHandler invocationHandler(Object... targets) {
		return invocationHandler(Optional.ofNullable(targets).map(Arrays::asList).orElse(null));
	}

	public static DelegatingInvocationHandler invocationHandler(Iterable<? extends Object> targets) {
		return new DelegatingInvocationHandler(targets);
	}

	public static class DelegatingInvocationHandler implements InvocationHandlerLFP {

		private final List<Object> targets;

		@SuppressWarnings("unchecked")
		public DelegatingInvocationHandler(Iterable<? extends Object> targets) {
			super();
			if (targets == null)
				this.targets = List.of();
			else if (!(targets instanceof List))
				this.targets = StreamSupport.stream(targets.spliterator(), false)
						.collect(Collectors.toUnmodifiableList());
			else
				this.targets = Collections.unmodifiableList((List<Object>) targets);
		}

		@Override
		public ThrowingSupplier<Object, Throwable> getInvoker(InvocationHandlerRequest request) throws Throwable {
			if (isGetTargetsMethod(request.method()))
				return () -> targets;
			var declaringClass = request.method().getDeclaringClass();
			for (var target : targets)
				if (declaringClass.isInstance(target))
					return () -> request.invoke(target);
			return null;
		}

		public <U> U newProxyInstance() {
			var ifaceSet = new InterfaceSet();
			if (targets != null)
				targets.forEach(v -> {
					if (v != null)
						ifaceSet.add(v);
				});
			return newProxyInstance(ifaceSet.toArray());
		}

	}

	static enum Const {
		;

		private static final Method getTargets_METHOD = Throws
				.unchecked(() -> Delegating.class.getMethod("getTargets"));

	}

}
