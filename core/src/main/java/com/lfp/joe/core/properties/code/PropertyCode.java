package com.lfp.joe.core.properties.code;

import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class PropertyCode<X> implements Comparable<PropertyCode<X>>, Supplier<String> {

	private final Class<X> type;
	private final String name;

	private String key;
	private Object defaultValue;
	private String seeLink;

	private boolean getPattern;

	public PropertyCode(Method method) {
		this(getType(method), getName(method));
		this.seeLink(String.format("%s#%s", method.getDeclaringClass().getName(), method.getName()));
	}

	public PropertyCode(Class<X> type, String name) {
		super();
		this.type = Objects.requireNonNull(type, "type required");
		this.name = Optional.ofNullable(name)
				.filter(Predicate.not(String::isBlank))
				.orElseThrow(() -> new IllegalArgumentException("invalid name:" + name));
	}

	@Override
	public String get() {
		List<String> lines = new ArrayList<>();
		getSeeLink().ifPresent(v -> {
			lines.add(String.format("/** @see {@link %s} */", v));
		});
		var defaultValue = getDefaultValue().orElse(null);
		if (defaultValue == null && getType().isPrimitive())
			defaultValue = Objects.toString(Array.get(Array.newInstance(getType(), 1), 0));
		if (defaultValue != null)
			lines.add(String.format("@DefaultValue(\"%s\")", defaultValue));
		var key = getKey();
		if (name.equals(key))
			key = null;
		var methodName = getMethodName();
		if (key == null && !methodName.equals(getName()))
			key = getName();
		if (key != null)
			lines.add(String.format("@Key(\"%s\")", key));
		lines.add(String.format("%s %s();", this.getType().getSimpleName(), methodName));
		return lines.stream().collect(Collectors.joining("\n")) + "\n";
	}

	@Override
	public int compareTo(PropertyCode<X> other) {
		Comparator<PropertyCode<X>> comparator = (v0, v1) -> 0;
		comparator = comparator.thenComparing(pc -> pc.getDefaultValue().isPresent() ? 0 : 1);
		comparator = comparator.thenComparing(PropertyCode::getName);
		comparator = comparator.thenComparing(pc -> pc.getKey());
		return Comparator.nullsLast(comparator).compare(this, other);
	}

	public String getMethodName() {
		var methodName = this.getName();
		if (!isGetPattern())
			return methodName;

		boolean prepend = true;
		for (var checkPrefix : List.of("get", "is")) {
			if (checkPrefix.length() >= methodName.length())
				continue;
			if (!methodName.startsWith(checkPrefix))
				continue;
			var nextChar = methodName.charAt(checkPrefix.length());
			if (!Character.isUpperCase(nextChar))
				continue;
			prepend = false;
			break;
		}
		if (prepend) {
			methodName = Character.toUpperCase(methodName.charAt(0)) + methodName.substring(1);
			if (boolean.class.isAssignableFrom(getType()))
				methodName = "is" + methodName;
			else
				methodName = "get" + methodName;
		}
		return methodName;
	}

	@SuppressWarnings("rawtypes")
	public Optional<String> getDefaultValue() {
		if (defaultValue instanceof Collection && ((Collection) defaultValue).isEmpty())
			defaultValue = null;
		if (defaultValue instanceof Map && ((Map) defaultValue).isEmpty())
			defaultValue = null;
		if (defaultValue instanceof CharSequence && ((CharSequence) defaultValue).toString().isBlank())
			defaultValue = null;
		if (defaultValue == null)
			return Optional.empty();
		var defaultValueStr = Objects.toString(defaultValue);
		if (defaultValueStr == null || defaultValueStr.isBlank())
			return Optional.empty();
		return Optional.of(defaultValueStr);
	}

	public String getKey() {
		if (key == null || key.isBlank())
			return name;
		return key;
	}

	public Optional<String> getSeeLink() {
		return Optional.ofNullable(seeLink);
	}

	public PropertyCode<X> defaultValue(Object newDefaultValue) {
		setDefaultValue(newDefaultValue);
		return this;
	}

	public void setDefaultValue(Object defaultValue) {
		this.defaultValue = defaultValue;
	}

	public PropertyCode<X> key(String newKey) {
		setKey(newKey);
		return this;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public Class<X> getType() {
		return type;
	}

	public PropertyCode<X> seeLink(String newSeeLink) {
		setSeeLink(newSeeLink);
		return this;
	}

	public void setSeeLink(String seeLink) {
		this.seeLink = seeLink;
	}

	public boolean isGetPattern() {
		return getPattern;
	}

	public PropertyCode<X> getPattern(boolean newGetPattern) {
		setGetPattern(newGetPattern);
		return this;
	}

	public void setGetPattern(boolean getPattern) {
		this.getPattern = getPattern;
	}

	/*
	 * utils
	 */

	@SuppressWarnings("unchecked")
	private static <X> Class<X> getType(Method method) {
		if (!isValid(method))
			throw new IllegalArgumentException("type lookup failed:" + method);
		return (Class<X>) method.getReturnType();
	}

	private static String getName(Method method) {
		if (!isValid(method))
			throw new IllegalArgumentException("name lookup failed:" + method);
		return method.getName();
	}

	private static boolean isValid(Method method) {
		if (method == null)
			return false;
		else if (method.getParameterCount() != 0)
			return false;
		else if (Void.TYPE.equals(method.getReturnType()))
			return false;
		else if (Modifier.isStatic(method.getModifiers()))
			return false;
		else
			return true;
	}

	public static void main(String[] args) throws NoSuchMethodException, SecurityException {
		var pc = new PropertyCode<>(Date.class.getMethod("getTime")).getPattern(true);
		System.out.println(pc.get());

	}

}
