package com.lfp.joe.core.process.future;

import java.time.Duration;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import javax.annotation.Nullable;

import org.immutables.value.Value;

import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.Durations;

@ValueLFP.Style
@Value.Immutable
abstract class AbstractCompletion<X> {

	@Value.Parameter(order = 0)
	@Nullable
	public abstract X result();

	@Value.Parameter(order = 1)
	@Nullable
	public abstract Throwable failure();

	public X resultOrThrow() {
		var failure = failure();
		if (failure != null) {
			if (failure instanceof CancellationException)
				throw (CancellationException) failure;
			throw new CompletionException(failure);
		}
		return result();
	}

	public Optional<Throwable> tryGetFailure() {
		return Optional.ofNullable(failure());
	}

	public boolean isSuccess() {
		return !isFailure();
	}

	public boolean isFailure() {
		return failure() != null;
	}

	public boolean isCancellation() {
		return failure() instanceof CancellationException;
	}

	public boolean isInterruption() {
		return tryGetFailure().map(failure -> {
			if (failure instanceof InterruptCancellationException)
				return true;
			if (failure instanceof InterruptedException)
				return true;
			return false;
		}).orElse(false);
	}

	/*
	 * forward completions
	 */

	public boolean complete(BiConsumer<? super X, ? super Throwable> action) {
		return complete(action, null);
	}

	public boolean complete(BiConsumer<? super X, ? super Throwable> action, Executor executor) {
		if (action == null)
			return false;
		return complete(v -> action.accept(v, null), t -> action.accept(null, t), null, executor);
	}

	public boolean complete(Consumer<? super X> resultAction, Consumer<? super Throwable> failureAction) {
		return complete(resultAction, failureAction, null);
	}

	public boolean complete(Consumer<? super X> resultAction, Consumer<? super Throwable> failureAction,
			BiConsumer<Boolean, ? super CancellationException> cancelAction) {
		return complete(resultAction, failureAction, cancelAction, null);
	}

	public boolean complete(Consumer<? super X> resultAction, Consumer<? super Throwable> failureAction,
			BiConsumer<Boolean, ? super CancellationException> cancelAction, Executor executor) {
		Predicate<Runnable> executorAction = command -> {
			Optional.ofNullable(executor).ifPresentOrElse(v -> v.execute(command), command);
			return true;
		};
		Function<X, Boolean> resultMapper = result -> {
			return Optional.ofNullable(resultAction)
					.map(v -> executorAction.test(() -> v.accept(result)))
					.orElse(false);
		};
		Function<Throwable, Boolean> failureMapper = failure -> {
			return Optional.ofNullable(failureAction)
					.map(v -> executorAction.test(() -> v.accept(failure)))
					.orElse(false);
		};
		BiFunction<Boolean, ? super CancellationException, Boolean> cancelMapper = (interrupt, failure) -> {
			return Optional.ofNullable(cancelAction)
					.map(v -> executorAction.test(() -> v.accept(interrupt, failure)))
					.orElseGet(() -> failureMapper.apply(failure));
		};
		return map(resultMapper, failureMapper, cancelMapper);
	}

	public <U> U map(Function<? super X, ? extends U> resultMapper, Function<? super Throwable, U> failureMapper) {
		return map(resultMapper, failureMapper, failureMapper == null ? null : (nil, t) -> failureMapper.apply(t));
	}

	public <U> U map(Function<? super X, ? extends U> resultMapper, Function<? super Throwable, U> failureMapper,
			BiFunction<Boolean, ? super CancellationException, ? extends U> cancelMapper) {
		Objects.requireNonNull(resultMapper);
		Objects.requireNonNull(failureMapper);
		Objects.requireNonNull(cancelMapper);
		if (isCancellation())
			return cancelMapper.apply(isInterruption(), (CancellationException) failure());
		else if (isFailure())
			return failureMapper.apply(failure());
		else
			return resultMapper.apply(result());
	}

	/*
	 * CompletableFuture functions
	 */

	public CompletableFuture<X> toFuture() {
		return map(CompletableFuture::completedFuture, CompletableFuture::failedFuture);
	}

	@Override
	public String toString() {
		return "Completion ["
				+ Optional.ofNullable(failure()).map(v -> "failure=" + v).orElseGet(() -> "result=" + result()) + "]";
	}

	@Value.Check
	protected AbstractCompletion<X> check() {
		if (result() != null && failure() != null)
			throw new IllegalArgumentException("result must be null in failed state");
		return Completion.copyOf(this);
	}

	public static <U> Completion<U> of(U result) {
		return Completion.of(result, null);
	}

	public static <U> Completion<U> of(Throwable failure) {
		Objects.requireNonNull(failure);
		return Completion.of(null, failure);
	}

	public static <U> Completion<U> join(Future<U> future) {
		try {
			return get(future);

		} catch (InterruptedException e) {
			throw new CompletionException(e);
		}
	}

	public static <U> Completion<U> get(Future<U> future) throws InterruptedException {
		return get(future, Durations.max()).get();
	}

	public static <U> Completion<U> getNow(Future<U> future) throws IllegalStateException {
		var completionOp = tryGet(future);
		if (completionOp.isPresent())
			return completionOp.get();
		throw new IllegalStateException("incomplete state");
	}

	public static <U> Optional<Completion<U>> tryGet(Future<U> future) {
		try {
			return get(future, Duration.ZERO);
		} catch (InterruptedException e) {
			// should not happen
			throw new RuntimeException(e);
		}
	}

	public static <U> Optional<Completion<U>> get(Future<U> future, Duration duration) throws InterruptedException {
		return Optional.ofNullable(getInternal(future, duration));
	}

	protected static <U> Completion<U> getInternal(Future<U> future, Duration duration) throws InterruptedException {
		Objects.requireNonNull(future);
		duration = Optional.ofNullable(duration).orElse(Duration.ZERO);
		var futureDone = future.isDone();
		if (!futureDone && Durations.isNegativeOrZero(duration))
			return null;
		var join = futureDone || Durations.max().equals(duration);
		if (join && future instanceof CompletableFuture) {
			try {
				return Completion.of(((CompletableFuture<U>) future).join());
			} catch (CompletionException e) {
				var cause = e.getCause();
				if (cause instanceof InterruptedException)
					throw (InterruptedException) cause;
				return Completion.of(cause);
			} catch (CancellationException e) {
				return Completion.of(e);
			}

		} else {
			try {
				if (join)
					return Completion.of(future.get());
				else {
					var timeUnitEntry = Durations.toTimeUnitEntry(duration);
					return Completion.of(future.get(timeUnitEntry.getKey(), timeUnitEntry.getValue()));
				}
			} catch (ExecutionException e) {
				return Completion.of(e.getCause());
			} catch (CancellationException e) {
				return Completion.of(e);
			} catch (TimeoutException e) {
				return null;
			}
		}
	}

}
