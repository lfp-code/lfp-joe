package com.lfp.joe.core.config.machine;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class DockerSupportLoader extends MachineConfigLoader<Boolean> {

	public static final DockerSupportLoader INSTANCE = new DockerSupportLoader();

	private DockerSupportLoader() {}

	@Override
	protected Iterable<? extends Callable<? extends Boolean>> loaders() {
		List<Callable<Boolean>> loaders = new ArrayList<>();
		loaders.add(() -> {
			return new File("/.dockerenv").exists() ? true : null;
		});
		loaders.add(() -> {
			var split = "/docker/";
			var lines = execLinesOut("cat", absolutePath("/proc/1/cgroup"));
			lines = lines.map(v -> {
				var splitAt = v.indexOf(split);
				if (splitAt < 0)
					return null;
				v = v.substring(splitAt + split.length());
				return v;
			});
			lines = lines.flatMap(nonNullOrBlankFlatMap());
			return lines.findFirst().isPresent();
		});
		loaders.add(() -> false);
		return loaders;
	}

	public static void main(String[] args) {
		System.out.println(DockerSupportLoader.INSTANCE.get());
	}
}
