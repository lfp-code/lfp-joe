package com.lfp.joe.core.properties.loader;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;

import org.aeonbits.owner.Config;

public enum SystemPropertiesLoader implements MultiLoader.ClassTypeLoader {
	INSTANCE;

	@Override
	public void load(String instanceId, Properties result, Class<Config> classType,
			Iterable<Entry<Method, Set<String>>> publicNoArgMethodEntries) {
		Map<Object, Object> objMap = System.getProperties();
		if (objMap == null)
			return;
		Map<String, String> map = new HashMap<>();
		objMap.entrySet().stream().filter(e -> e.getKey() != null).filter(e -> e.getValue() != null).forEach(ent -> {
			map.put(Objects.toString(ent.getKey()), Objects.toString(ent.getValue()));
		});
		Loaders.putAll(result, publicNoArgMethodEntries, map);
	}

}
