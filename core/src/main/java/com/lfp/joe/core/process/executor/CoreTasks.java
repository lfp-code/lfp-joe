package com.lfp.joe.core.process.executor;

import java.lang.reflect.Field;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;

import org.immutables.value.Value;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.Durations;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.process.ThreadRenamer;

import net.robinfriedli.threadpool.ThreadPool;

@ValueLFP.Style
@Value.Enclosing
public class CoreTasks {

	private CoreTasks() {}

	private static final ThreadFunction THREAD_FACTORY = ThreadFunction.builder()
			.addThreadModifiers(ThreadRenamer.of(CoreTasks.class.getSimpleName()))
			.build();
	private static final Field CompletableFuture_AsyncRun_dep_FIELD = getDepField("AsyncRun");
	private static final Field CompletableFuture_AsyncSupply_dep_FIELD = getDepField("AsyncSupply");
	private static final ThreadPool THREAD_POOL;
	static {
		var threadPoolBuilder = ThreadPoolFactory.builder();
		threadPoolBuilder = threadPoolBuilder.maxSize(Integer.MAX_VALUE);
		threadPoolBuilder = threadPoolBuilder.threadFactory(THREAD_FACTORY);
		THREAD_POOL = threadPoolBuilder.build().get();
	}
	private static final Executor EXECUTOR = new Executor() {

		@Override
		public void execute(Runnable command) {
			if (isDoneFuture(command))
				return;
			THREAD_POOL.execute(command);
		}

		@Override
		public String toString() {
			return CoreTasks.class.getSimpleName() + "-executor";
		}
	};

	public static Executor executor() {
		return EXECUTOR;
	}

	public static Executor delayedExecutor(Duration delay) {
		if (delay == null || Durations.isNegativeOrZero(delay))
			return executor();
		return CompletableFuture.delayedExecutor(Durations.getTimeAmount(delay), Durations.getTimeUnit(delay),
				executor());
	}

	public static ForkJoinPool forkJoinPool() {
		return forkJoinPool(MachineConfig.logicalCoreCount());
	}

	public static ForkJoinPool forkJoinPool(int parallelism) {
		return new ForkJoinPool(parallelism, THREAD_FACTORY, null, false);
	}

	public static ThreadFunction threadFactory() {
		return THREAD_FACTORY;
	}

	private static boolean isDoneFuture(Runnable command) {
		if (!(command instanceof Future))
			return false;
		var future = (Future<?>) command;
		if (future.isDone())
			return true;
		for (var field : List.of(CompletableFuture_AsyncRun_dep_FIELD, CompletableFuture_AsyncSupply_dep_FIELD)) {
			if (!field.getDeclaringClass().isInstance(future))
				continue;
			var dep = (Future<?>) Throws.unchecked(() -> field.get(future));
			if (dep != null && dep.isDone()) {
				future.cancel(false);
				return true;
			}
		}
		return false;
	}

	private static Field getDepField(String className) {
		return Throws.unchecked(() -> {
			var ct = CoreReflections.tryForName(CompletableFuture.class.getName() + "." + className).get();
			return MemberCache.getField(FieldRequest.of(ct, Future.class, "dep"));
		});
	}

}
