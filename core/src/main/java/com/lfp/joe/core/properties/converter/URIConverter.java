package com.lfp.joe.core.properties.converter;

import java.lang.reflect.Method;
import java.net.URI;

import org.aeonbits.owner.Converter;

public class URIConverter implements Converter<URI> {

	@Override
	public URI convert(Method method, String input) {
		if (input == null || input.isEmpty())
			return null;
		return URI.create(input);
	}

}
