package com.lfp.joe.core.classpath;

import java.lang.invoke.MethodHandle;
import java.lang.ref.SoftReference;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.WeakHashMap;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.annotation.Nullable;

import org.immutables.value.Value;

import com.lfp.joe.core.classpath.ImmutableMemberCache.ConstructorRequest;
import com.lfp.joe.core.classpath.ImmutableMemberCache.ConstructorsRequest;
import com.lfp.joe.core.classpath.ImmutableMemberCache.DefaultMethodHandleRequest;
import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldsRequest;
import com.lfp.joe.core.classpath.ImmutableMemberCache.MethodRequest;
import com.lfp.joe.core.classpath.ImmutableMemberCache.MethodsRequest;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.ComputeStore;
import com.lfp.joe.core.function.EmptyArrays;
import com.lfp.joe.core.function.Throws;

@SuppressWarnings("unchecked")
@ValueLFP.Style
@Value.Enclosing
public class MemberCache {

	private MemberCache() {}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final ComputeStore<Object, List<? extends Object>> CACHE = ComputeStore.from(new WeakHashMap<>(),
			SoftReference::new);

	/*
	 * constructors
	 */

	public static <I> Optional<Constructor<I>> tryGetConstructor(ConstructorRequest<I> request, boolean cacheMiss) {
		var members = computeIfAbsent(request, () -> {
			var memberStream = streamConstructors(request);
			memberStream = memberStream.filter(v -> {
				return CoreReflections.parameterTypesAssignableFrom(v, request.parameterTypes());
			});
			var constructor = memberStream.findFirst().orElse(null);
			if (constructor == null)
				return cacheMiss ? List.of() : null;
			return List.of(CoreReflections.setAccessible(constructor));
		});
		if (members == null)
			return Optional.empty();
		return members.stream().map(v -> (Constructor<I>) v).findFirst();
	}

	public static <I> Constructor<I> getConstructor(ConstructorRequest<I> request) throws NoSuchMethodException {
		var constructorOp = tryGetConstructor(request, false);
		if (constructorOp.isEmpty())
			throw request.notFoundFailure();
		return constructorOp.get();
	}

	public static <I> Instantiator<I> getConstructorInstantiator(ConstructorRequest<I> request) {
		var constructor = Throws.unchecked(() -> getConstructor(request));
		return args -> Throws.unchecked(() -> constructor.newInstance(args));
	}

	public static <I> I newInstance(ConstructorRequest<I> request, Object... args) {
		return Throws.unchecked(() -> getConstructor(request).newInstance(args));
	}

	public static List<Constructor<?>> getConstructors(ConstructorsRequest<?> request) {
		return computeIfAbsent(request, () -> {
			var memberStream = streamConstructors(request);
			return memberStream::iterator;
		});
	}

	private static Stream<Constructor<?>> streamConstructors(MemberRequestDef<?> request) {
		return CoreReflections.streamConstructors(request.declaringClass(), request.allTypes())
				.peek(v -> CoreReflections.moduleAddOpens(v.getDeclaringClass(), THIS_CLASS))
				.peek(CoreReflections::setAccessible);
	}

	/*
	 * fields
	 */

	public static Optional<Field> tryGetField(FieldRequest<?, ?> request, boolean cacheMiss) {
		var members = computeIfAbsent(request, () -> {
			var memberStream = streamFields(request);
			memberStream = memberStream.filter(v -> {
				if (request.name() == null)
					return true;
				return request.name().equals(v.getName());
			});
			memberStream = memberStream.filter(v -> {
				if (request.type() == null)
					return true;
				return CoreReflections.isAssignableFrom(request.type(), v.getType());
			});
			var field = memberStream.findFirst().orElse(null);
			if (field == null)
				return cacheMiss ? List.of() : null;
			return List.of(CoreReflections.setAccessible(field));
		});
		if (members == null)
			return Optional.empty();
		return members.stream().map(v -> v).findFirst();
	}

	public static Field getField(FieldRequest<?, ?> request) throws NoSuchFieldException {
		var fieldOp = tryGetField(request, false);
		if (fieldOp.isEmpty())
			throw request.notFoundFailure();
		return fieldOp.get();
	}

	public static <I, U> Accessor<I, U> getFieldAccessor(FieldRequest<I, U> request) {
		var field = Throws.unchecked(() -> getField(request));
		return new Accessor<I, U>() {

			@Override
			public U apply(I i) {
				return (U) Throws.unchecked(() -> field.get(i));
			}

			@Override
			public void accept(I i, U u) {
				Throws.unchecked(() -> field.set(i, u));

			}
		};
	}

	public static <I, U> U getFieldValue(FieldRequest<I, U> request, I instance) {
		return (U) Throws.unchecked(() -> getField(request).get(instance));
	}

	public static <I, U> void setFieldValue(FieldRequest<I, U> request, I instance, U value) {
		Throws.unchecked(() -> getField(request).set(instance, value));
	}

	public static List<Field> getFields(FieldsRequest<?> request) {
		return computeIfAbsent(request, () -> {
			var memberStream = streamFields(request);
			return memberStream::iterator;
		});
	}

	private static Stream<Field> streamFields(MemberRequestDef<?> request) {
		return CoreReflections.streamFields(request.declaringClass(), request.allTypes())
				.peek(v -> CoreReflections.moduleAddOpens(v.getDeclaringClass(), THIS_CLASS))
				.peek(CoreReflections::setAccessible);
	}

	/*
	 * methods
	 */

	public static Optional<Method> tryGetMethod(MethodRequest<?, ?> request, boolean cacheMiss) {
		var members = computeIfAbsent(request, () -> {
			var method = lookupMethod(request).orElse(null);
			if (method == null)
				return cacheMiss ? List.of() : null;
			return List.of(CoreReflections.setAccessible(method));
		});
		if (members == null)
			return Optional.empty();
		return members.stream().map(v -> v).findFirst();
	}

	public static Method getMethod(MethodRequest<?, ?> request) throws NoSuchMethodException {
		var methodOp = tryGetMethod(request, false);
		if (methodOp.isEmpty())
			throw request.notFoundFailure();
		return methodOp.get();
	}

	public static Optional<MethodHandle> tryGetDefaultMethodHandle(DefaultMethodHandleRequest<?, ?> request,
			boolean cacheMiss) {
		var members = computeIfAbsent(request, () -> {
			var methodHandle = lookupMethod(request).flatMap(method -> {
				return CoreReflections.streamSpecialMethodHandles(method).findFirst();
			}).orElse(null);
			if (methodHandle == null)
				return cacheMiss ? List.of() : null;
			return List.of(methodHandle);
		});
		if (members == null)
			return Optional.empty();
		return members.stream().map(v -> v).findFirst();
	}

	public static MethodHandle getDefaultMethodHandle(DefaultMethodHandleRequest<?, ?> request)
			throws NoSuchMethodException {
		var methodHandleOp = tryGetDefaultMethodHandle(request, false);
		if (methodHandleOp.isEmpty())
			throw request.notFoundFailure();
		return methodHandleOp.get();
	}

	private static Optional<Method> lookupMethod(BaseMethodRequestDef<?, ?> request) {
		var memberStream = streamMethods(request);
		memberStream = memberStream.filter(v -> {
			if (request.name() == null)
				return true;
			return request.name().equals(v.getName());
		});
		memberStream = memberStream.filter(v -> {
			if (request.returnType() == null)
				return true;
			return CoreReflections.isAssignableFrom(request.returnType(), v.getReturnType());
		});
		memberStream = memberStream.filter(v -> {
			return CoreReflections.parameterTypesAssignableFrom(v, request.parameterTypes());
		});
		return memberStream.findFirst();
	}

	public static <I, U> Invoker<I, U> getMethodInvoker(MethodRequest<I, U> request) {
		var method = Throws.unchecked(() -> getMethod(request));
		return (i, args) -> (U) Throws.unchecked(() -> method.invoke(i, args));
	}

	public static <I, U> U invokeMethod(MethodRequest<I, U> request, I instance, Object... args) {
		return (U) Throws.unchecked(() -> getMethod(request).invoke(instance, args));
	}

	public static List<Method> getMethods(MethodsRequest<?> request) {
		return computeIfAbsent(request, () -> {
			var memberStream = streamMethods(request);
			return memberStream::iterator;
		});
	}

	private static Stream<Method> streamMethods(MemberRequestDef<?> request) {
		return CoreReflections.streamMethods(request.declaringClass(), request.allTypes()).peek(v -> {
			CoreReflections.moduleAddOpens(v.getDeclaringClass(), THIS_CLASS);
		}).peek(CoreReflections::setAccessible);
	}

	private static <U> List<U> computeIfAbsent(Object key, Supplier<Iterable<? extends U>> loader) {
		Objects.requireNonNull(key);
		Objects.requireNonNull(loader);
		Supplier<List<U>> listLoader = () -> {
			var spliterator = Optional.ofNullable(loader.get()).map(Iterable::spliterator).orElse(null);
			if (spliterator == null)
				return null;
			return StreamSupport.stream(spliterator, false)
					.filter(Objects::nonNull)
					.distinct()
					.collect(Collectors.toUnmodifiableList());
		};
		var cacheValue = CACHE.computeIfAbsent(key, nil -> listLoader.get());
		return (List<U>) cacheValue;
	}

	public static interface Instantiator<X> {

		X newInstance(Object... arguments);
	}

	public static interface Accessor<X, Y> extends Function<X, Y>, Supplier<Y>, BiConsumer<X, Y>, Consumer<Y> {

		@Override
		default Y get() {
			return apply(null);
		}

		@Override
		default void accept(Y value) {
			accept(null, value);
		}
	}

	public static interface Invoker<X, Y> {

		default Y invoke(Object... arguments) {
			return invoke(null, arguments);
		}

		default Y invoke(X instance) {
			return invoke(instance, EmptyArrays.of(Object.class));
		}

		Y invoke(X instance, Object... arguments);

	}

	static abstract class MemberRequestDef<X> {

		@Value.Parameter(order = 0)
		public abstract Class<X> declaringClass();

		@Value.Default
		public boolean allTypes() {
			return true;
		}

		abstract MemberRequestDef<X> check();

		static <U, R extends MemberRequestDef<U>> R checkDeclaringClass(R request,
				BiFunction<R, Class<U>, R> withDeclaringClassFunction) {
			var namedClassType = CoreReflections.getNamedClassTypeIfVaries(request.declaringClass()).orElse(null);
			if (namedClassType != null)
				return withDeclaringClassFunction.apply(request, namedClassType);
			return request;
		}
	}

	static interface NotFoundFailureRequestDef {

		Throwable notFoundFailure();
	}

	static interface NameRequestDef {

		@Nullable
		@Value.Parameter(order = 3)
		String name();
	}

	static interface ExecutableRequestDef {

		@Value.Parameter(order = 4)
		List<Class<?>> parameterTypes();

		static <U, R extends ExecutableRequestDef> R checkParameterTypes(R request,
				BiFunction<R, List<Class<?>>, R> withParameterTypesFunction) {
			var parameterTypes = request.parameterTypes()
					.stream()
					.<Class<?>>map(v -> Primitives.getWrapperType(v).orElse(v))
					.collect(Collectors.toList());
			if (!Objects.equals(parameterTypes, request.parameterTypes()))
				return withParameterTypesFunction.apply(request, parameterTypes);
			return request;
		}
	}

	@Value.Immutable
	static abstract class ConstructorRequestDef<X> extends MemberRequestDef<X>
			implements NotFoundFailureRequestDef, ExecutableRequestDef {

		@Override
		public NoSuchMethodException notFoundFailure() {
			return new NoSuchMethodException(this.toString());
		}

		public static <X, Y> ConstructorRequest<X> of(Class<X> declaringClassType, Class<?>... parameterTypes) {
			return ConstructorRequest.of(declaringClassType,
					() -> Stream.ofNullable(parameterTypes).flatMap(Stream::of).iterator());
		}

		@Override
		@Value.Check
		ConstructorRequestDef<X> check() {
			var result = MemberRequestDef.checkDeclaringClass(this,
					(v, ct) -> ConstructorRequest.copyOf(v).withDeclaringClass(ct));
			result = ExecutableRequestDef.checkParameterTypes(this,
					(v, ptypes) -> ConstructorRequest.copyOf(this).withParameterTypes(ptypes));
			return result;
		}
	}

	@Value.Immutable
	static abstract class FieldRequestDef<X, Y> extends MemberRequestDef<X>
			implements NotFoundFailureRequestDef, NameRequestDef {

		@Nullable
		@Value.Parameter(order = 1)
		public abstract Class<? super Y> type();

		@Override
		public NoSuchFieldException notFoundFailure() {
			return new NoSuchFieldException(this.toString());
		}

		@Override
		@Value.Check
		FieldRequestDef<X, Y> check() {
			var result = MemberRequestDef.checkDeclaringClass(this,
					(v, ct) -> FieldRequest.copyOf(v).withDeclaringClass(ct));
			return result;
		}

	}

	static abstract class BaseMethodRequestDef<X, Y> extends MemberRequestDef<X>
			implements NotFoundFailureRequestDef, NameRequestDef, ExecutableRequestDef {

		@Nullable
		@Value.Parameter(order = 1)
		public abstract Class<? extends Y> returnType();

		@Override
		public NoSuchMethodException notFoundFailure() {
			return new NoSuchMethodException(this.toString());
		}

	}

	@Value.Immutable
	static abstract class MethodRequestDef<X, Y> extends BaseMethodRequestDef<X, Y> {

		@Override
		@Value.Check
		MethodRequestDef<X, Y> check() {
			var result = MemberRequestDef.checkDeclaringClass(this,
					(v, ct) -> MethodRequest.copyOf(v).withDeclaringClass(ct));
			result = ExecutableRequestDef.checkParameterTypes(this,
					(v, ptypes) -> MethodRequest.copyOf(this).withParameterTypes(ptypes));
			return result;
		}

		public static <X, Y> MethodRequest<X, Y> of(Class<X> declaringClassType, Class<? extends Y> returnType,
				String name, Class<?>... parameterTypes) {
			return MethodRequest.of(declaringClassType, returnType, name,
					() -> Stream.ofNullable(parameterTypes).flatMap(Stream::of).iterator());
		}

	}

	@Value.Immutable
	static abstract class DefaultMethodHandleRequestDef<X, Y> extends BaseMethodRequestDef<X, Y> {

		@Override
		@Value.Check
		DefaultMethodHandleRequestDef<X, Y> check() {
			var result = DefaultMethodHandleRequest.checkDeclaringClass(this,
					(v, ct) -> DefaultMethodHandleRequest.copyOf(v).withDeclaringClass(ct));
			result = ExecutableRequestDef.checkParameterTypes(this,
					(v, ptypes) -> DefaultMethodHandleRequest.copyOf(this).withParameterTypes(ptypes));
			return result;
		}

		public static DefaultMethodHandleRequest<?, ?> of(Method method) {
			Objects.requireNonNull(method);
			return DefaultMethodHandleRequest.of(method.getDeclaringClass(), method.getReturnType(), method.getName(),
					Arrays.asList(method.getParameterTypes()));
		}
	}

	@Value.Immutable
	static abstract class ConstructorsRequestDef<X> extends MemberRequestDef<X> {

		@Override
		@Value.Check
		ConstructorsRequestDef<X> check() {
			var result = MemberRequestDef.checkDeclaringClass(this,
					(v, ct) -> ConstructorsRequest.copyOf(v).withDeclaringClass(ct));
			return result;
		}
	}

	@Value.Immutable
	static abstract class FieldsRequestDef<X> extends MemberRequestDef<X> {

		@Override
		@Value.Check
		FieldsRequestDef<X> check() {
			var result = MemberRequestDef.checkDeclaringClass(this,
					(v, ct) -> FieldsRequest.copyOf(v).withDeclaringClass(ct));
			return result;
		}
	}

	@Value.Immutable
	static abstract class MethodsRequestDef<X> extends MemberRequestDef<X> {

		@Override
		@Value.Check
		MethodsRequestDef<X> check() {
			var result = MemberRequestDef.checkDeclaringClass(this,
					(v, ct) -> MethodsRequest.copyOf(v).withDeclaringClass(ct));
			return result;
		}
	}

}
