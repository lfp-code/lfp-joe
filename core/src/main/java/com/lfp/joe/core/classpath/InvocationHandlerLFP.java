package com.lfp.joe.core.classpath;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.immutables.value.Value;

import com.lfp.joe.core.classpath.ImmutableInvocationHandlerLFP.InvocationHandlerRequest;
import com.lfp.joe.core.classpath.ImmutableMemberCache.DefaultMethodHandleRequest;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.EmptyArrays;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

@SuppressWarnings("unchecked")
@Value.Enclosing
@ValueLFP.Style
public interface InvocationHandlerLFP extends InvocationHandler {

	@Override
	default Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		if (args == null)
			return invoke(proxy, method, EmptyArrays.of(Object.class));
		var request = InvocationHandlerRequest.builder().proxy(proxy).method(method).args(args).build();
		Object result;
		try {
			ThrowingSupplier<Object, Throwable> invoker = getInvoker(request);
			if (invoker == null)
				invoker = getDefaultInvoker(request);
			if (invoker == null)
				throw new UnsupportedOperationException(request.summary());
			result = invoker.get();
		} catch (Throwable t) {
			t = Throws.unwrap(t, InvocationTargetException.class);
			if (IllegalArgumentException.class.equals(t.getClass())) {
				var message = Stream.of(t.getMessage(), request.summary())
						.filter(Objects::nonNull)
						.filter(Predicate.not(String::isBlank))
						.collect(Collectors.joining(" "));
				t = new IllegalArgumentException(message, t);
			}
			throw t;
		}
		if (result == null
				&& Optional.ofNullable(request.method().getReturnType()).map(Class::isPrimitive).orElse(false))
			return Primitives.getDefaultValue(request.method().getReturnType());
		return result;
	}

	ThrowingSupplier<Object, Throwable> getInvoker(InvocationHandlerRequest request) throws Throwable;

	default ThrowingSupplier<Object, Throwable> getDefaultInvoker(InvocationHandlerRequest request) {
		if (!request.method().isDefault())
			return null;
		var defaultMethodHandle = MemberCache
				.tryGetDefaultMethodHandle(DefaultMethodHandleRequest.of(request.method()), true)
				.orElse(null);
		if (defaultMethodHandle != null)
			return () -> defaultMethodHandle.bindTo(request.proxy()).invokeWithArguments(request.args());
		return null;
	}

	default <X> X newProxyInstance(Class<?>... interfaces) {
		var ifaceStream = Stream.ofNullable(interfaces).flatMap(Stream::of);
		ifaceStream = ifaceStream.filter(Objects::nonNull);
		ifaceStream = ifaceStream.filter(Predicate.not(Proxy::isProxyClass));
		ifaceStream = ifaceStream.distinct();
		var result = (X) Proxy.newProxyInstance(CoreReflections.streamDefaultClassLoaders().findFirst().get(),
				ifaceStream.toArray(Class<?>[]::new), this);
		return result;
	}

	default InvocationHandlerLFP append(InvocationHandler invocationHandler) {
		if (invocationHandler == null || invocationHandler == this)
			return this;
		return request -> {
			var invoker = getInvoker(request);
			if (invoker != null)
				return invoker;
			if (invocationHandler instanceof InvocationHandlerLFP)
				return ((InvocationHandlerLFP) invocationHandler).getInvoker(request);
			return () -> invocationHandler.invoke(request.proxy(), request.method(), request.args());
		};
	}

	static interface InvocationHandlerRequestDef {

		Object proxy();

		Method method();

		Object[] args();

		Map<Object, Object> attributes();

		default Object invoke(Object target)
				throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
			return method().invoke(target, args());
		}

		default String summary() {
			return String.format("interfaces:%s method:%s args:%s", Arrays.toString(proxy().getClass().getInterfaces()),
					method(), Arrays.toString(args()));
		}
	}

	@Value.Immutable
	static abstract class AbstractInvocationHandlerRequest implements InvocationHandlerRequestDef {

		@Override
		@Value.Lazy
		public Map<Object, Object> attributes() {
			return new ConcurrentHashMap<>();
		}

	}

	public static void main(String[] args) {
		var delegate = new HashMap<>();
		InvocationHandlerLFP invocationHandler = request -> {
			if ("getOrDefault".equals(request.method().getName()) && request.args().length == 2)
				return () -> request.invoke(request.proxy());
			return () -> request.invoke(delegate);
		};
		Map<Object, Object> map = invocationHandler.newProxyInstance(Map.class);
		System.out.println(map.size());
		System.out.println(map.getOrDefault("cool", "dude"));
		;
	}

}