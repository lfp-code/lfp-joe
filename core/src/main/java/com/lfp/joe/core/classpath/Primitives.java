package com.lfp.joe.core.classpath;

import java.lang.invoke.MethodType;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Stream;

import javax.lang.model.type.TypeKind;

public class Primitives {

	protected Primitives() {}

	private static final String TYPE_FIELD_NAME = "TYPE";
	private static final String VALUE_OF_METHOD_NAME = "valueOf";

	public static Map<String, Class<?>> getPrimitiveNameToTypeMap() {
		return PrimitiveMapping.INSTANCE.nameToPrimitive;
	}

	public static Optional<Class<?>> getPrimitiveType(String name) {
		if (name == null)
			return Optional.empty();
		var primitiveType = PrimitiveMapping.INSTANCE.nameToPrimitive.get(name);
		if (primitiveType != null)
			return Optional.of(primitiveType);
		for (var ent : PrimitiveMapping.INSTANCE.nameToPrimitive.entrySet())
			if (name.equalsIgnoreCase(ent.getKey()))
				return Optional.of(ent.getValue());
		return Optional.empty();
	}

	public static Optional<Class<?>> getPrimitiveType(Class<?> classType) {
		if (classType == null)
			return Optional.empty();
		if (!classType.isPrimitive()) {
			var primitiveType = PrimitiveMapping.INSTANCE.wrapperToPrimitive.get(classType);
			return Optional.ofNullable(primitiveType);
		}
		return Optional.of(classType);
	}

	public static Optional<Class<?>> getWrapperType(Class<?> classType) {
		if (classType == null)
			return Optional.empty();
		if (!classType.isPrimitive()) {
			var primitiveType = PrimitiveMapping.INSTANCE.wrapperToPrimitive.get(classType);
			return getWrapperType(primitiveType);
		}
		return Optional.ofNullable(PrimitiveMapping.INSTANCE.primitiveToWrapper.get(classType));
	}

	public static Object getDefaultValue(Class<?> classType) {
		classType = getPrimitiveType(classType).orElse(null);
		var valueOp = PrimitiveMapping.INSTANCE.primitiveToDefault.get(classType);
		return Optional.ofNullable(valueOp).flatMap(Function.identity()).orElse(null);
	}

	public static Object valueOf(Class<?> classType, Object input) {
		if (input == null)
			return null;
		var wrapperType = getWrapperType(classType).orElse(null);
		if (Boolean.class.equals(wrapperType) && input instanceof String) {
			// too error prone, overriding default which makes anything not "true" == false
			var str = (String) input;
			if (Boolean.TRUE.toString().equalsIgnoreCase(str))
				return Boolean.TRUE;
			if (Boolean.FALSE.toString().equalsIgnoreCase(str))
				return Boolean.FALSE;
			return null;
		}
		var valueOfMethods = PrimitiveMapping.INSTANCE.wrapperToValueOfMethods.get(wrapperType);
		if (valueOfMethods == null || valueOfMethods.isEmpty())
			return null;
		var method = valueOfMethods.get(input.getClass());
		if (method == null) {
			for (var ent : valueOfMethods.entrySet()) {
				if (ent.getKey().isAssignableFrom(input.getClass())) {
					method = ent.getValue();
					break;
				}
			}
		}
		if (method == null) {
			if (input instanceof CharSequence && !(input instanceof String))
				return valueOf(classType, input.toString());
			return null;
		}
		try {
			return method.invoke(null, input);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e) : new RuntimeException(e);
		}
	}

	private static Class<?> lookupPrimitive(String className) {
		if (className == null || className.isBlank())
			return null;
		var classNameLookup = Void.class.getPackageName() + "." + className.substring(0, 1).toUpperCase()
				+ className.substring(1);
		Class<?> classType;
		try {
			classType = Class.forName(classNameLookup);
		} catch (ClassNotFoundException e) {
			return null;
		}
		Class<?> primitiveType;
		try {
			primitiveType = (Class<?>) classType.getField(TYPE_FIELD_NAME).get(null);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
			return null;
		}
		if (primitiveType == null || !primitiveType.isPrimitive())
			return null;
		return primitiveType;
	}

	private static enum PrimitiveMapping {
		INSTANCE;

		public final Map<String, Class<?>> nameToPrimitive;
		public final Map<Class<?>, Class<?>> primitiveToWrapper;
		public final Map<Class<?>, Class<?>> wrapperToPrimitive;
		public final Map<Class<?>, Optional<Object>> primitiveToDefault;
		public final Map<Class<?>, Map<Class<?>, Method>> wrapperToValueOfMethods;

		private PrimitiveMapping() {
			Map<String, Class<?>> nameToPrimitive = new LinkedHashMap<>();
			Map<Class<?>, Class<?>> primitiveToWrapper = new LinkedHashMap<>();
			Map<Class<?>, Class<?>> wrapperToPrimitive = new LinkedHashMap<>();
			Map<Class<?>, Optional<Object>> primitiveToDefault = new LinkedHashMap<>();
			Map<Class<?>, Map<Class<?>, Method>> wrapperToValueOfMethods = new LinkedHashMap<>();
			BiConsumer<String, Class<?>> nameTypeConsumer = (name, type) -> {
				if (name == null || name.isBlank())
					throw new IllegalArgumentException("invalid name:" + name);
				Objects.requireNonNull(type, "type required");
				var wrapperType = MethodType.methodType(type).wrap().returnType();
				if (type.equals(wrapperType))
					throw new IllegalArgumentException(String.format("type:%s == wrapperType:%s", type, wrapperType));
				nameToPrimitive.put(name, type);
				primitiveToWrapper.put(type, wrapperType);
				wrapperToPrimitive.put(wrapperType, type);
				Object defaultValue;
				try {
					defaultValue = Array.get(Array.newInstance(type, 1), 0);
				} catch (IllegalArgumentException e) {
					defaultValue = null;
				}
				primitiveToDefault.put(type, Optional.ofNullable(defaultValue));
				Map<Class<?>, Method> valueOfMethods = new LinkedHashMap<>();
				{
					Stream<Method> methodStream = Stream.of(wrapperType.getMethods(), wrapperType.getDeclaredMethods())
							.flatMap(Stream::of);
					methodStream = methodStream.distinct();
					methodStream = methodStream.filter(v -> {
						if (!Modifier.isPublic(v.getModifiers()))
							return false;
						if (!Modifier.isStatic(v.getModifiers()))
							return false;
						if (!VALUE_OF_METHOD_NAME.equals(v.getName()))
							return false;
						if (v.getParameterCount() != 1)
							return false;
						if (!wrapperType.isAssignableFrom(v.getReturnType()))
							return false;
						return true;
					});
					methodStream.forEach(method -> {
						var ptypes = method.getParameterTypes();
						valueOfMethods.put(ptypes[0], method);
					});
				}
				wrapperToValueOfMethods.put(wrapperType, valueOfMethods);
			};
			List<Class<?>> primitiveTypes = List.of(void.class, boolean.class, byte.class, char.class, short.class,
					int.class, float.class, double.class, long.class);
			for (Class<?> primitiveType : primitiveTypes)
				nameTypeConsumer.accept(primitiveType.getName(), primitiveType);
			var typeKindValues = TypeKind.values();
			if (typeKindValues != null) {
				for (var typeKindValue : typeKindValues) {
					if (typeKindValue == null || !typeKindValue.isPrimitive())
						continue;
					var className = typeKindValue.toString().toLowerCase();
					if (nameToPrimitive.containsKey(className))
						continue;
					var primitiveType = lookupPrimitive(className);
					if (primitiveType == null || primitiveToWrapper.containsKey(primitiveType))
						continue;
					nameTypeConsumer.accept(className, primitiveType);
				}
			}
			this.nameToPrimitive = Collections.unmodifiableMap(nameToPrimitive);
			this.primitiveToWrapper = Collections.unmodifiableMap(primitiveToWrapper);
			this.wrapperToPrimitive = Collections.unmodifiableMap(wrapperToPrimitive);
			this.primitiveToDefault = Collections.unmodifiableMap(primitiveToDefault);
			this.wrapperToValueOfMethods = Collections.unmodifiableMap(wrapperToValueOfMethods);
		}

	}

	public static void main(String[] args) {
		System.out.println(lookupPrimitive("integer"));
		System.out.println(getPrimitiveType(int.class));
		System.out.println(getWrapperType(int.class));
		System.out.println(getPrimitiveType(Integer.class));
		System.out.println(getWrapperType(Integer.class));
		System.out.println(getPrimitiveType(Void.class));
		System.out.println(getWrapperType(void.class));
		System.out.println(getPrimitiveType(String.class));
		System.out.println(getWrapperType(String.class));
		System.out.println(getDefaultValue(Integer.class));
		System.out.println(getDefaultValue(Boolean.class));
		System.out.println(getDefaultValue(Void.class));
		System.out.println(getPrimitiveType("VoID"));
		System.out.println(valueOf(boolean.class, new StringBuilder("true")));

	}

}
