package com.lfp.joe.core.lock;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.OpenOption;
import java.nio.file.StandardOpenOption;
import java.util.Comparator;
import java.util.Objects;
import java.util.stream.Stream;

import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.function.Throws.ThrowingBiConsumer;
import com.lfp.joe.core.function.Throws.ThrowingBiFunction;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.io.FileExt;

public class FileLocks {

	private static final KeyLock<String> JVM_LOCK = KeyLock.create();

	private FileLocks() {}

	public static <U> U read(File file, ThrowingBiFunction<FileChannel, InputStream, U, IOException> accessor,
			OpenOption... openOptions) throws IOException {
		openOptions = normalizeOpenOptions(openOptions, StandardOpenOption.READ);
		return access(file, accessor == null ? null : fc -> {
			return accessor.apply(fc, Channels.newInputStream(fc));
		}, openOptions);
	}

	public static void write(File file, ThrowingBiConsumer<FileChannel, OutputStream, IOException> accessor,
			OpenOption... openOptions) throws IOException {
		write(file, accessor == null ? null : (fc, os) -> {
			accessor.accept(fc, os);
			return null;
		}, openOptions);
	}

	public static <U> U write(File file, ThrowingBiFunction<FileChannel, OutputStream, U, IOException> accessor,
			OpenOption... openOptions) throws IOException {
		openOptions = normalizeOpenOptions(openOptions, StandardOpenOption.WRITE);
		return access(file, accessor == null ? null : fc -> {
			return accessor.apply(fc, Channels.newOutputStream(fc));
		}, openOptions);
	}

	public static <U> U access(File file, ThrowingFunction<FileChannel, U, IOException> accessor,
			OpenOption... openOptions) throws IOException {
		Objects.requireNonNull(file);
		Objects.requireNonNull(accessor);
		var key = FileExt.getUniquePath(file);
		var write = containsWriteOpenOption(openOptions);
		var lock = write ? JVM_LOCK.writeLock(key) : JVM_LOCK.readLock(key);
		lock.lock();
		try {
			FileChannel fileChannel = null;
			FileLock fileLock = null;
			try {
				fileChannel = openFileChannel(file, openOptions);
				fileLock = lock(file, fileChannel, openOptions);
				return accessor.apply(fileChannel);
			} finally {
				var failure = close(fileLock, fileChannel);
				if (failure != null) {
					if (failure instanceof IOException)
						throw (IOException) failure;
					throw Throws.unchecked(failure);
				}
			}
		} finally {
			lock.unlock();
		}
	}

	private static FileChannel openFileChannel(File file, OpenOption... openOptions) throws IOException {
		// ensure write, because even read will need a new file
		openOptions = normalizeOpenOptions(openOptions, StandardOpenOption.WRITE);
		return FileChannel.open(file.toPath(), openOptions);
	}

	private static FileLock lock(File file, FileChannel fileChannel, OpenOption... openOptions) throws IOException {
		if (containsWriteOpenOption(openOptions)) {
			var lock = fileChannel.lock();
			return lock;
		}
		// can't be shared on JVM
		// return fileChannel.lock(0, Long.MAX_VALUE, true);
		return null;
	}

	private static OpenOption[] normalizeOpenOptions(OpenOption[] openOptionsArr, OpenOption... openOptions) {
		var sb = Stream.<OpenOption>builder();
		sb.add(StandardOpenOption.CREATE);
		boolean writeFound = false;
		var ooIter = Stream.of(openOptionsArr, openOptions).filter(Objects::nonNull).flatMap(Stream::of).iterator();
		while (ooIter.hasNext()) {
			var openOption = ooIter.next();
			sb.add(openOption);
			if (Objects.equals(StandardOpenOption.WRITE, openOption))
				writeFound = true;
		}
		if (!writeFound)
			sb.add(StandardOpenOption.READ);
		var ooStream = sb.build();
		ooStream = ooStream.filter(Objects::nonNull);
		ooStream = ooStream.distinct();
		ooStream = ooStream.sorted(Comparator.comparing(Objects::toString));
		return ooStream.toArray(OpenOption[]::new);
	}

	private static boolean containsWriteOpenOption(OpenOption... openOptions) {
		if (openOptions != null)
			for (var openOption : openOptions)
				if (Objects.equals(StandardOpenOption.WRITE, openOption))
					return true;
		return false;
	}

	private static Exception close(FileLock fileLock, FileChannel fileChannel, AutoCloseable... autoCloseables) {
		var sb = Stream.<AutoCloseable>builder();
		if (fileLock != null)
			sb.add(() -> {
				if (fileLock.isValid())
					fileLock.close();
			});
		if (fileChannel != null)
			sb.add(() -> {
				if (fileChannel.isOpen())
					fileChannel.close();
			});
		Stream.ofNullable(autoCloseables).map(Stream::of).forEach(sb);
		Throwable failure = null;
		var autoCloseableIter = sb.build().filter(Objects::nonNull).distinct().iterator();
		while (autoCloseableIter.hasNext()) {
			try {
				autoCloseableIter.next().close();
			} catch (Throwable t) {
				failure = Throws.combine(failure, t);
			}
		}
		if (failure == null)
			return null;
		if (failure instanceof Exception)
			return (Exception) failure;
		return new Exception(failure);
	}

}
