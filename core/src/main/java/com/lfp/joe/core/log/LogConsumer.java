package com.lfp.joe.core.log;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public interface LogConsumer extends BiConsumer<String, Object[]> {

	public static final LogConsumer NO_OP = (f, args) -> {};

	default void log(String format, Object... arguments) {
		if (arguments == null)
			arguments = Const.EMPTY_ARGS;
		accept(format, arguments);
	}

	public static void log(LogConsumer logConsumer, Consumer<LogConsumer> action) {
		log(logConsumer, null, action == null ? null : (l, nil) -> action.accept(l));
	}

	public static <U> void log(LogConsumer logConsumer, U input, BiConsumer<LogConsumer, U> action) {
		if (logConsumer == null || logConsumer == NO_OP || action == null)
			return;
		action.accept(logConsumer, input);
	}

	static enum Const {
		;

		private static final Object[] EMPTY_ARGS = new Object[0];
	}

}
