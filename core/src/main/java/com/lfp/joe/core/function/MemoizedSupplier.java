package com.lfp.joe.core.function;

import java.time.Duration;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

public interface MemoizedSupplier<X> extends Supplier<X> {

	@Override
	X get();

	X getIfLoaded();

	X get(Duration timeToLive);

	Optional<Date> loadedAt();

	boolean clear();

	default Optional<Duration> elapsedSinceLoad() {
		return loadedAt().map(d -> Duration.ofMillis(System.currentTimeMillis() - d.getTime()));
	}

	default boolean isLoaded() {
		return loadedAt().isPresent();
	}

	public static MemoizedSupplier<Void> create(Runnable runnable) {
		return create(runnable, false);
	}

	public static MemoizedSupplier<Void> create(Runnable runnable, boolean releaseSupplierOnLoad) {
		return create(runnable == null ? null : () -> {
			runnable.run();
			return null;
		}, releaseSupplierOnLoad);
	}

	public static <X> MemoizedSupplier<X> create(Supplier<X> supplier) {
		return create(supplier, false);
	}

	public static <X> MemoizedSupplier<X> create(Supplier<X> supplier, boolean releaseSupplierOnLoad) {
		return new MemoizedSupplier.Impl<>(supplier, releaseSupplierOnLoad);
	}

	public static class Impl<X> implements MemoizedSupplier<X> {

		private static final String RECURSIVE_LOAD_ERROR_MESSAGE = "recursive load";

		private final Object lock = new Object();
		private final boolean releaseSupplierOnLoad;
		private Supplier<X> supplier;
		private Entry<Optional<X>, Long> valueEntry;
		private boolean loading;

		public Impl(Supplier<X> supplier) {
			this(supplier, false);
		}

		public Impl(Supplier<X> supplier, boolean releaseSupplierOnLoad) {
			super();
			this.supplier = supplier;
			this.releaseSupplierOnLoad = releaseSupplierOnLoad;
		}

		@Override
		public X getIfLoaded() {
			return Optional.ofNullable(valueEntry).flatMap(Entry::getKey).orElse(null);
		}

		@Override
		public Optional<Date> loadedAt() {
			return Optional.ofNullable(valueEntry).map(Entry::getValue).map(Date::new);
		}

		@Override
		public boolean isLoaded() {
			return valueEntry != null;
		}

		@Override
		public X get() {
			return get(null);
		}

		@Override
		public X get(Duration timeToLive) {
			if (shouldLoad(timeToLive))
				synchronized (lock) {
					if (shouldLoad(timeToLive)) {
						if (loading)
							throw new IllegalStateException(RECURSIVE_LOAD_ERROR_MESSAGE);
						loading = true;
						try {
							var valueOp = Optional.ofNullable(supplier).map(Supplier::get);
							valueEntry = Map.entry(valueOp, System.currentTimeMillis());
						} finally {
							loading = false;
							if (releaseSupplierOnLoad)
								supplier = null;
						}
					}
				}
			return Optional.ofNullable(valueEntry).map(Entry::getKey).flatMap(Function.identity()).orElse(null);
		}

		@Override
		public boolean clear() {
			if (valueEntry != null)
				synchronized (lock) {
					if (valueEntry != null) {
						valueEntry = null;
						return true;
					}
				}
			return false;
		}

		protected boolean shouldLoad(Duration timeToLive) {
			if (valueEntry == null)
				return true;
			if (timeToLive == null)
				return false;
			var ttlMillis = timeToLive.toMillis();
			if (ttlMillis <= 0)
				return true;
			var elapsed = System.currentTimeMillis() - valueEntry.getValue();
			if (elapsed <= ttlMillis)
				return false;
			return true;
		}

	}

}
