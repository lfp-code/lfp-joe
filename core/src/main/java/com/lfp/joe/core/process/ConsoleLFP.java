package com.lfp.joe.core.process;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Scanner;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

import com.lfp.joe.core.function.MemoizedSupplier;

public enum ConsoleLFP {
	;

	private static final Supplier<Scanner> CONSOLE_SCANNER_S = MemoizedSupplier.create(() -> new Scanner(System.in));
	private static final List<String> TRUE_VALUES_DEFAULT = List.of("true", "yes", "y");

	public static String readLine(String fmt, Object... args) {
		System.out.println(String.format(fmt, args));
		return readLine();
	}

	public static String readLine() {
		try {
			return CONSOLE_SCANNER_S.get().nextLine();
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	public static Stream<String> readLines() {
		var lineStream = Stream.iterate(readLine(), v -> v != null && !v.isBlank(), nil -> readLine());
		return lineStream;
	}

	public static Stream<String> readLines(String fmt, Object... args) {
		var line = readLine(fmt, args);
		if (line == null || line.isBlank())
			return Stream.empty();
		var stream = Stream.concat(Stream.of(line), readLines());
		return stream;
	}

	public static boolean readBoolean() {
		return parseBoolean(readLine());
	}

	public static boolean readBoolean(String fmt, Object... args) {
		return parseBoolean(readLine(fmt, args));
	}

	private static boolean parseBoolean(String line, String... trueValues) {
		Function<String, Optional<String>> trimToNullOptional = v -> {
			return Optional.ofNullable(v).map(String::trim).filter(Predicate.not(String::isEmpty));
		};
		Iterable<String> trueValueIterable = () -> {
			return Stream.ofNullable(trueValues).flatMap(Stream::of).map(trimToNullOptional).flatMap(Optional::stream)
					.iterator();
		};
		if (!trueValueIterable.iterator().hasNext())
			trueValueIterable = TRUE_VALUES_DEFAULT;
		line = trimToNullOptional.apply(line).orElse(null);
		if (line == null)
			return false;
		for (var trueValue : trueValueIterable) {
			trueValue = trimToNullOptional.apply(trueValue).orElse(null);
			if (line.equalsIgnoreCase(trueValue))
				return true;
		}
		return false;
	}

	public static void main(String[] args) {
		ConsoleLFP.readLines("hey").forEach(v -> {
			System.out.println(v);
		});
		System.out.println(ConsoleLFP.readBoolean("hi?"));
	}

}