package com.lfp.joe.core.process.executor;

import java.time.Duration;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.function.Supplier;

import org.immutables.value.Value;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.Durations;

import net.robinfriedli.threadpool.RejectedExecutionHandler;
import net.robinfriedli.threadpool.ThreadPool;

@Value.Immutable
@Value.Enclosing
@ValueLFP.Style
abstract class AbstractThreadPoolFactory implements Supplier<ThreadPool> {

	@Value.Default
	public int coreSize() {
		return MachineConfig.logicalCoreCount();
	}

	@Value.Default
	public int maxSize() {
		var coreSize = coreSize();
		return Math.max(coreSize * 4, Math.max(coreSize * 2, coreSize));
	}

	@Value.Default
	public Duration keepAlive() {
		return Duration.ofSeconds(15);
	}

	@Value.Default
	public BlockingQueue<Runnable> workQueue() {
		return new LinkedBlockingQueue<>();
	}

	@Value.Default
	public ThreadFactory threadFactory() {
		return ThreadFunction.builder().build();
	}

	@Value.Default
	public RejectedExecutionHandler rejectedExecutionHandler() {
		return RejectedExecutionHandler.ABORT_POLICY;
	}

	@Override
	public ThreadPool get() {
		var keepAliveEntry = Durations.toTimeUnitEntry(keepAlive());
		return new ThreadPool(coreSize(), maxSize(), keepAliveEntry.getKey(), keepAliveEntry.getValue(), workQueue(),
				threadFactory(), rejectedExecutionHandler());
	}

}
