package com.lfp.joe.core.function;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

@SuppressWarnings("unchecked")
public class EmptyFunctions {

	protected EmptyFunctions() {}

	private static final Map<Object, Object> INSTANCE_MAP = new ConcurrentHashMap<>();

	public static Runnable runnable() {
		return computeIfAbsent(Runnable.class, () -> () -> {});
	}

	public static <Z> Supplier<Z> supplier() {
		return computeIfAbsent(Supplier.class, () -> () -> null);
	}

	public static <X> Consumer<X> consumer() {
		return computeIfAbsent(Consumer.class, () -> x -> {});
	}

	public static <X, Y> BiConsumer<X, Y> biConsumer() {
		return computeIfAbsent(BiConsumer.class, () -> (x, y) -> {});
	}

	public static <X, Z> Function<X, Z> function() {
		return computeIfAbsent(Function.class, () -> x -> null);
	}

	public static <X, Y, Z> BiFunction<X, Y, Z> biFunction() {
		return computeIfAbsent(BiFunction.class, () -> (x, y) -> null);
	}

	public static <X> Comparator<X> comparator() {
		return computeIfAbsent(Comparator.class, () -> (v1, v2) -> 0);
	}

	public static <X> Predicate<X> predicateFalse() {
		return computeIfAbsent(List.of(Predicate.class, false), () -> x -> false);
	}

	public static <X> Predicate<X> predicateTrue() {
		return computeIfAbsent(List.of(Predicate.class, true), () -> x -> true);
	}

	public static <X, Y> BiPredicate<X, Y> biPredicateFalse() {
		return computeIfAbsent(List.of(BiPredicate.class, false), () -> (x, y) -> false);
	}

	public static <X, Y> BiPredicate<X, Y> biPredicateTrue() {
		return computeIfAbsent(List.of(BiPredicate.class, true), () -> (x, y) -> true);
	}

	protected static <T> T computeIfAbsent(Object key, Supplier<T> loader) {
		return (T) INSTANCE_MAP.computeIfAbsent(key, loader == null ? null : nil -> loader.get());
	}

	public static void main(String[] args) {
		runnable().run();
		biConsumer().accept(null, 55);
		System.out.println(biFunction().apply(69, 420));
		System.err.println("done");
	}

}
