package com.lfp.joe.core.properties.converter;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Vector;

import org.aeonbits.owner.Converter;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.MemberCache;

public class ClassConverter implements Converter<Class<?>> {

	private static final String CLASS_LOOKUP_FAILED_MSG_TEMPLATE = "classType lookup failed:{} reason:{}";
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();

	protected static org.slf4j.Logger getLogger() {
		return org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	}

	@Override
	public Class<?> convert(Method method, String input) {
		var classType = CoreReflections.tryForName(input).orElse(null);
		if (classType != null)
			return classType;
		List<Class<?>> ctList;
		if (input == null || input.isBlank())
			ctList = List.of();
		else if (!input.matches("\\w+"))
			ctList = List.of();
		else
			ctList = lookupSimpleName(input);
		if (ctList.isEmpty()) {
			getLogger().warn(CLASS_LOOKUP_FAILED_MSG_TEMPLATE, input, "no matches");
			return null;
		}
		if (ctList.size() != 1) {
			getLogger().warn(CLASS_LOOKUP_FAILED_MSG_TEMPLATE, input, "multiple matches - " + ctList);
			return null;
		}
		return ctList.get(0);
	}

	private static List<Class<?>> lookupSimpleName(String input) {
		var fieldRequest = FieldRequest.of(ClassLoader.class, Vector.class, "classes").withAllTypes(false);
		Class<?> ctMatch = null;
		var classLoadIter = CoreReflections.streamDefaultClassLoaders().iterator();
		while (classLoadIter.hasNext()) {
			var ctVector = MemberCache.getFieldValue(fieldRequest, classLoadIter.next());
			for (var ctObj : ctVector) {
				var ct = (Class<?>) ctObj;
				if (ct.getSimpleName().equals(input)) {
					if (ctMatch == null)
						ctMatch = ct;
					else if (!ctMatch.equals(ct))
						return List.of(ctMatch, ct);
				}
			}
		}
		return ctMatch != null ? List.of(ctMatch) : List.of();
	}

}
