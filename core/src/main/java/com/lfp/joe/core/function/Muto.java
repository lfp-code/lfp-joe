package com.lfp.joe.core.function;

import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.FutureTask;
import java.util.function.BinaryOperator;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.process.future.Completion;

public interface Muto<T> extends Supplier<T>, Consumer<T> {

	/**
	 * @return
	 * @see java.util.concurrent.atomic.AtomicReference#get()
	 */
	@Override
	T get();

	default Optional<T> tryGet() {
		return Optional.ofNullable(this.get());
	}

	/**
	 * @param newValue
	 * @see java.util.concurrent.atomic.AtomicReference#set(java.lang.Object)
	 */
	void set(T newValue);

	@Override
	default void accept(T value) {
		set(value);
	}

	/**
	 * @param newValue
	 * @return
	 * @see java.util.concurrent.atomic.AtomicReference#getAndSet(java.lang.Object)
	 */
	default T getAndSet(T newValue) {
		return getAndUpdate(v -> newValue);
	}

	/*
	 * getAndUpdate
	 */

	default T getAndUpdate(Supplier<T> supplier) {
		return getAndUpdate(asUnaryOperator(supplier));
	}

	default T getAndUpdate(UnaryOperator<T> updateFunction) {
		return getAndUpdate(updateFunction, null);
	}

	/**
	 * @param updateFunction
	 * @return
	 * @see java.util.concurrent.atomic.AtomicReference#getAndUpdate(java.util.function.UnaryOperator)
	 */
	default T getAndUpdate(UnaryOperator<T> updateFunction, Predicate<? super T> filter) {
		return update(updateFunction, filter, true, false);
	}

	/*
	 * updateAndGet
	 */

	default T updateAndGet(Supplier<T> supplier) {
		return updateAndGet(asUnaryOperator(supplier));
	}

	default T updateAndGet(UnaryOperator<T> updateFunction) {
		return updateAndGet(updateFunction, null);
	}

	/**
	 * @param updateFunction
	 * @return
	 * @see java.util.concurrent.atomic.AtomicReference#getAndUpdate(java.util.function.UnaryOperator)
	 */
	default T updateAndGet(UnaryOperator<T> updateFunction, Predicate<? super T> filter) {
		return update(updateFunction, filter, false, false);
	}
	/*
	 * update
	 */

	default Muto<T> update(Supplier<T> supplier) {
		return update(asUnaryOperator(supplier));
	}

	default Muto<T> update(UnaryOperator<T> operator) {
		return update(operator, null);
	}

	default Muto<T> update(UnaryOperator<T> operator, Predicate<? super T> filter) {
		Objects.requireNonNull(operator);
		updateAndGet(operator, filter);
		return this;
	}

	/*
	 * apply
	 */

	default <U> U apply(Function<T, U> function) {
		return apply(function, null);
	}

	default <U> U apply(Function<T, U> function, Predicate<? super T> filter) {
		Objects.requireNonNull(function);
		var reference = new Const.Reference<U>();
		updateAndGet(v -> {
			reference.value = function.apply(v);
			return v;
		}, filter);
		return reference.value;
	}

	/*
	 * atomic reference methods
	 */

	default T getAndAccumulate(T input, BinaryOperator<T> accumulatorFunction) {
		return getAndAccumulate(input, accumulatorFunction, null);
	}

	/**
	 * @param x
	 * @param accumulatorFunction
	 * @return
	 * @see java.util.concurrent.atomic.AtomicReference#getAndAccumulate(java.lang.Object,
	 *      java.util.function.BinaryOperator)
	 */
	default T getAndAccumulate(T input, BinaryOperator<T> accumulatorFunction, Predicate<? super T> filter) {
		Objects.requireNonNull(accumulatorFunction);
		return getAndUpdate(v -> accumulatorFunction.apply(v, input), filter);
	}

	default T accumulateAndGet(T input, BinaryOperator<T> accumulatorFunction) {
		return accumulateAndGet(input, accumulatorFunction, null);
	}

	/**
	 * @param x
	 * @param accumulatorFunction
	 * @return
	 * @see java.util.concurrent.atomic.AtomicReference#accumulateAndGet(java.lang.Object,
	 *      java.util.function.BinaryOperator)
	 */
	default T accumulateAndGet(T input, BinaryOperator<T> accumulatorFunction, Predicate<? super T> filter) {
		Objects.requireNonNull(accumulatorFunction);
		return updateAndGet(v -> accumulatorFunction.apply(v, input), filter);
	}

	/**
	 * @param expectedValue
	 * @param newValue
	 * @return
	 * @see java.util.concurrent.atomic.AtomicReference#compareAndExchange(java.lang.Object,
	 *      java.lang.Object)
	 */
	default T compareAndExchange(T expectedValue, T newValue) {
		return getAndUpdate(v -> {
			if (!is(v, expectedValue))
				return v;
			return newValue;
		});
	}

	/**
	 * @param expectedValue
	 * @param newValue
	 * @return
	 * @see java.util.concurrent.atomic.AtomicReference#compareAndSet(java.lang.Object,
	 *      java.lang.Object)
	 */
	default boolean compareAndSet(T expectedValue, T newValue) {
		var reference = new Const.Reference<Boolean>();
		updateAndGet(v -> {
			if (!is(v, expectedValue))
				return v;
			reference.value = true;
			return newValue;
		});
		return isTrue(reference.value);
	}

	/*
	 * synchronized updates
	 */

	private T update(UnaryOperator<T> updateFunction, Predicate<? super T> filter, boolean returnPreviousValue,
			boolean synced) {
		if (!synced) {
			Objects.requireNonNull(updateFunction);
			if (filter != null) {
				var previousValue = get();
				if (!filter.test(previousValue))
					return previousValue;
			}
			synchronized (this) {
				return update(updateFunction, filter, returnPreviousValue, true);
			}
		}
		var previousValue = get();
		if (filter != null && !filter.test(previousValue))
			return previousValue;
		var newValue = updateFunction.apply(previousValue);
		set(newValue);
		return returnPreviousValue ? previousValue : newValue;
	}

	private String toStringDefault() {
		Class<?> classType;
		if (Impl.class.equals(this.getClass()))
			classType = Muto.class;
		else
			classType = CoreReflections.getNamedClassType(this.getClass());
		return classType.getSimpleName() + "[" + get() + "]";
	}

	/*
	 * creators
	 */

	public static <T> Muto<T> create() {
		return create(null);
	}

	public static <T> Muto<T> create(T value) {
		return new Muto.Impl<>(value);
	}

	/*
	 * utils
	 */

	private static <U> UnaryOperator<U> asUnaryOperator(Supplier<U> supplier) {
		Objects.requireNonNull(supplier);
		return nil -> supplier.get();
	}

	private static boolean is(Object v1, Object v2) {
		if (v1 != null && v2 != null && (v1.getClass().isPrimitive() || v2.getClass().isPrimitive()))
			return Objects.equals(v1, v2);
		return v1 == v2;
	}

	private static boolean isTrue(Boolean value) {
		return value == null ? false : value;
	}

	public static class Impl<T> implements Muto<T> {

		private T _value;

		public Impl(final T value) {
			set(value);
		}

		@Override
		public T get() {
			return _value;
		}

		@Override
		public void set(T newValue) {
			this._value = newValue;
		}

		@Override
		public int hashCode() {
			return Objects.hash(_value);
		}

		@SuppressWarnings("rawtypes")
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Impl other = (Impl) obj;
			return Objects.equals(_value, other._value);
		}

		@Override
		public String toString() {
			return Muto.super.toStringDefault();
		}
	}

	public static final class MutoBoolean implements Muto<Boolean>, BooleanSupplier {

		private boolean _value;

		private MutoBoolean(Boolean value) {
			set(value);
		}

		@Override
		public boolean getAsBoolean() {
			return _value;
		}

		@Override
		public Boolean get() {
			return getAsBoolean();
		}

		@Override
		public void set(Boolean newValue) {
			_value = Muto.isTrue(newValue);
		}

		@Override
		public Boolean compareAndExchange(Boolean expectedValue, Boolean newValue) {
			return Muto.super.compareAndExchange(Muto.isTrue(expectedValue), newValue);
		}

		@Override
		public boolean compareAndSet(Boolean expectedValue, Boolean newValue) {
			return Muto.super.compareAndSet(Muto.isTrue(expectedValue), newValue);
		}

		public boolean isTrue() {
			return getAsBoolean();
		}

		public boolean isFalse() {
			return !isTrue();
		}

		public void setTrue() {
			set(true);
		}

		public void setFalse() {
			set(false);
		}

		@Override
		public String toString() {
			return Muto.super.toStringDefault();
		}
		/*
		 * creators
		 */

		public static MutoBoolean create() {
			return create(false);
		}

		public static MutoBoolean create(Boolean value) {
			return new MutoBoolean(value);
		}

	}

	static enum Const {
		;

		private static class Reference<T> {

			public T value;

		}

	}

	public static void main(String[] args) throws InterruptedException {
		var m = MutoBoolean.create(null);
		System.out.println(m);
		System.out.println(m.get());
		m.set(null);
		System.out.println(m.get());
		System.out.println(m.compareAndSet(null, true));
		System.out.println(m.compareAndSet(Boolean.TRUE, true));
		var muto1 = Muto.create(0l);
		System.out.println(muto1.updateAndGet(v -> v + 1));
		System.out.println(muto1.updateAndGet(v -> v + 1));
		System.out.println(muto1.get());
		var muto = Muto.create(0l);
		System.out.println(muto.getAndUpdate(v -> v + 1));
		System.out.println(muto.get());
		var csList = IntStream.range(0, 10).mapToObj(index -> {
			var ft = new FutureTask<>(() -> {
				muto.updateAndGet(() -> {
					System.out.println("starting:" + index);
					Throws.unchecked(() -> Thread.sleep(3_000));
					System.out.println("finishing:" + index);
					return new Date().getTime();
				});
				return null;
			});
			new Thread(ft).start();
			return ft;
		}).collect(Collectors.toList());
		for (var csf : csList) {
			Completion.get(csf);
		}
		System.out.println("done");

	}

}