package com.lfp.joe.core.process.future;

import java.io.IOException;
import java.util.concurrent.CancellationException;

import com.lfp.joe.core.function.Throws;

public class InterruptCancellationException extends CancellationException implements Throws.ModifiableCause {
	private static final long serialVersionUID = -4413436261025908168L;

	public InterruptCancellationException() {
		super();
	}

	public InterruptCancellationException(String message) {
		super(message);
	}

	public InterruptCancellationException(Throwable cause) {
		super();
		setCause(cause);
	}

	public InterruptCancellationException(String message, Throwable cause) {
		super(message);
		setCause(cause);
	}

	public static void main(String[] args) {
		var e = new InterruptCancellationException(new IOException());
		e.printStackTrace();
	}
}
