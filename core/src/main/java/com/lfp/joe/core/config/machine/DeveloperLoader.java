package com.lfp.joe.core.config.machine;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.function.BooleanSupplier;

import com.lfp.joe.core.config.EnvironmentLevel;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.properties.Configs;

public class DeveloperLoader extends MachineConfigLoader<Boolean> {

	public static final DeveloperLoader INSTANCE = new DeveloperLoader();

	private DeveloperLoader() {}

	@Override
	protected Iterable<? extends Callable<? extends Boolean>> loaders() {
		return List.of(this::load);

	}

	private boolean load() {
		var cacheFile = new File(TempDirectoryLoader.INSTANCE.get(), this.getClass().getName() + ".cache");
		Optional<Boolean> cachedValue = Optional.empty();
		if (cacheFile.exists()) {
			try {
				var cachedContents = Files.readString(cacheFile.toPath(), StandardCharsets.UTF_8);
				if (Boolean.TRUE.toString().equalsIgnoreCase(cachedContents))
					cachedValue = Optional.of(true);
				else if (Boolean.FALSE.toString().equalsIgnoreCase(cachedContents))
					cachedValue = Optional.of(false);
			} catch (IOException e) {
				// suppress
			}
		}
		var loaderFuture = loadAsync(cacheFile, cachedValue);
		if (loaderFuture.isDone() || cachedValue.isEmpty())
			return loaderFuture.join();
		loaderFuture.whenComplete((v, t) -> {
			if (t != null) {
				logSystemErr("load error. cacheFile:%s", cacheFile.getAbsolutePath());
				t.printStackTrace();
			}
		});
		return cachedValue.get();
	}

	private CompletableFuture<Boolean> loadAsync(File cacheFile, Optional<Boolean> cachedValue) {
		BooleanSupplier task = () -> {
			var cfg = Configs.get(MachineConfig.class);
			boolean developer;
			if (!EnvironmentLevel.DEVELOPMENT.equals(cfg.environmentLevel()))
				developer = false;
			else if (cfg.disableDevelopment())
				developer = false;
			else
				developer = true;
			if (!Objects.equals(developer, cachedValue.orElse(null)))
				try {
					Files.writeString(cacheFile.toPath(), Objects.toString(developer), StandardCharsets.UTF_8);
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			return developer;
		};
		var executor = executorService(1);
		var cfuture = CompletableFuture.supplyAsync(task::getAsBoolean, executor);
		cfuture.whenComplete((v, t) -> executor.shutdown());
		return cfuture;
	}

	public static void main(String[] args) throws InterruptedException {
		System.out.println(DeveloperLoader.INSTANCE.get());
		Thread.currentThread().join();
	}

}
