package com.lfp.joe.core.function;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

//borrowed from reactor disposable
public interface Scrapable extends Registration {

	boolean isScrapped();

	boolean scrap();

	Scrapable onScrap(Runnable runnable);

	default Scrapable onScrap(Scrapable scrapable) {
		requireNonNull(scrapable, "scrapable");
		return onScrap(Scrapable.Impl.ScrapTask.of(scrapable));
	}

	// useful for when inputs must be removed
	default <U> Scrapable onScrap(U input, Consumer<? super U> action) {
		return onScrap(Scrapable.Impl.ScrapTask.of(input, action));
	}

	default void validateNotScrapped() {
		if (isScrapped())
			throw new IllegalStateException("scrapable is in a scrapped state");
	}

	@Override
	default void close() {
		this.scrap();
	}

	/**
	 * creators
	 */

	public static Scrapable create() {
		return new Scrapable.Impl();
	}

	public static Scrapable create(Runnable... tasks) {
		return new Scrapable.Impl(tasks);
	}

	public static Scrapable create(Scrapable... scrapables) {
		return new Scrapable.Impl(scrapables);
	}

	public static Scrapable complete() {
		return Const.COMPLETE;
	}

	public static Scrapable noOp() {
		return Const.NO_OP;
	}

	/**
	 * types
	 */

	public static interface Delegating extends Scrapable {

		Scrapable _delegateScrapable();

		@Override
		default boolean isScrapped() {
			return _delegateScrapable().isScrapped();
		}

		@Override
		default Scrapable onScrap(Runnable runnable) {
			return _delegateScrapable().onScrap(runnable);
		}

		@Override
		default boolean scrap() {
			return _delegateScrapable().scrap();
		}

		@Override
		default void validateNotScrapped() {
			_delegateScrapable().validateNotScrapped();
		}

		@Override
		default void close() {
			_delegateScrapable().close();
		}
	}

	public static class Impl implements Scrapable {

		private Collection<ScrapTask<?>> _scrapTaskStore;
		private volatile boolean _scrapped;

		public Impl() {}

		public Impl(Runnable... tasks) {
			this(Optional.ofNullable(tasks)
					.filter(v -> v.length > 0)
					.map(Stream::of)
					.map(stream -> stream.filter(Objects::nonNull).map(ScrapTask::of))
					.map(Stream::iterator)
					.orElse(null));
		}

		public Impl(Scrapable... scrapables) {
			this(Optional.ofNullable(scrapables)
					.filter(v -> v.length > 0)
					.map(Stream::of)
					.map(stream -> stream.filter(Objects::nonNull).map(ScrapTask::of))
					.map(Stream::iterator)
					.orElse(null));
		}

		protected Impl(Iterator<? extends ScrapTask<?>> scrapTaskIterator) {
			while (scrapTaskIterator != null && scrapTaskIterator.hasNext()) {
				var scrapTask = scrapTaskIterator.next();
				if (scrapTask == null)
					continue;
				getOrCreateScrapTaskStore().add(scrapTask);
			}
		}

		@Override
		public String toString() {
			return "Scrapable.Impl ["
					+ (_scrapped ? "scrapped" : ("count=" + tryGetScrapTaskStore().map(Collection::size).orElse(0)))
					+ "]";
		}

		@Override
		public boolean isScrapped() {
			return _scrapped;
		}

		@Override
		public boolean scrap() {
			return this.<Boolean>accessScrapTaskStore(nil -> true, false, true).orElse(false);
		}

		@Override
		public Scrapable onScrap(Runnable runnable) {
			return onScrap(ScrapTask.of(runnable));
		}

		public Scrapable onScrap(ScrapTask<?> scrapTask) {
			Scrapable.requireNonNull(scrapTask, "scrapTask");
			return this.<Scrapable>accessScrapTaskStore(stsOp -> {
				stsOp.get().add(scrapTask);
				return new Scrapable.Impl(() -> tryRemove(scrapTask));
			}, true, false).orElseGet(() -> {
				scrapTask.run();
				return Scrapable.complete();
			});
		}

		public Optional<ScrapTask<?>> tryRemove(Object item) {
			if (item == null)
				return Optional.empty();
			return this.<Optional<ScrapTask<?>>>accessScrapTaskStore(stsOp -> {
				if (stsOp.isEmpty())
					return Optional.empty();
				var sts = stsOp.get();
				if (item instanceof ScrapTask) {
					if (sts.remove(item))
						return Optional.of((ScrapTask<?>) item);
					else
						return Optional.empty();
				} else {
					var stsIter = sts.iterator();
					while (stsIter.hasNext()) {
						var st = stsIter.next();
						if (Objects.equals(st.input(), item)) {
							stsIter.remove();
							return Optional.of(st);
						}
					}
					return Optional.empty();
				}
			}, false, false).flatMap(Function.identity());
		}

		protected <U> Optional<U> accessScrapTaskStore(
				Function<? super Optional<Collection<ScrapTask<?>>>, ? extends U> accessor,
				boolean createScrapTaskStore, boolean scrap) {
			Scrapable.requireNonNull(accessor, "accessor");
			if (isScrapped())
				return Optional.empty();
			final U result;
			final Collection<ScrapTask<?>> scrapTaskStore;
			synchronized (this) {
				if (isScrapped())
					return Optional.empty();
				if (scrap)
					markScrapped();
				scrapTaskStore = createScrapTaskStore ? getOrCreateScrapTaskStore()
						: tryGetScrapTaskStore().orElse(null);
				result = accessor.apply(Optional.ofNullable(scrapTaskStore));
				cleanupScrapStaskStore(scrap);
			}
			if (scrap)
				scrap(scrapTaskStore);
			Scrapable.requireNonNull(result, "result");
			return Optional.of(result);
		}

		// set scrapped flag to true;
		protected void markScrapped() {
			this._scrapped = true;
		}

		// get scrapTaskStore if non null
		protected Optional<Collection<ScrapTask<?>>> tryGetScrapTaskStore() {
			return Optional.ofNullable(_scrapTaskStore);
		}

		// get scrapTaskStore or create if null
		protected Collection<ScrapTask<?>> getOrCreateScrapTaskStore() {
			return tryGetScrapTaskStore().orElseGet(() -> {
				var scrapTaskStore = Scrapable.requireNonNull(createScrapTaskStore(), "scrapTaskStore");
				_scrapTaskStore = scrapTaskStore;
				return scrapTaskStore;
			});
		}

		// release empty scrapTaskStore
		protected boolean cleanupScrapStaskStore(boolean force) {
			return tryGetScrapTaskStore().filter(v -> force || v.isEmpty()).map(nil -> {
				_scrapTaskStore = null;
				return true;
			}).orElse(false);
		}

		// override to configuring backing collection
		protected Collection<ScrapTask<?>> createScrapTaskStore() {
			return new LinkedList<>();
		}

		protected void scrap(Collection<? extends ScrapTask<?>> scrapTasks) {
			if (scrapTasks == null)
				return;
			List<Throwable> errors = null;
			for (ScrapTask<?> o : scrapTasks) {
				try {
					o.run();
				} catch (Throwable ex) {
					Scrapable.throwIfFatalFailure(ex);
					if (errors == null)
						errors = new ArrayList<>();
					errors.add(ex);
				}
			}
			if (errors != null) {
				if (errors.size() == 1)
					throw Scrapable.propagateFailure(errors.get(0));
				throw Scrapable.multipleFailures(errors);
			}
		}

		protected static class ScrapTask<X> implements Runnable {

			private static final Consumer<Runnable> RUNNABLE_CONSUMER = Runnable::run;
			private static final Consumer<Scrapable> SCRAPABLE_CONSUMER = Scrapable::scrap;

			public static ScrapTask<Runnable> of(Runnable runnable) {
				return of(runnable, RUNNABLE_CONSUMER);
			}

			public static ScrapTask<Scrapable> of(Scrapable scrapable) {
				return of(scrapable, SCRAPABLE_CONSUMER);
			}

			public static <U> ScrapTask<U> of(U input, Consumer<? super U> consumer) {
				return new ScrapTask<>(input, consumer);
			}

			private X input;
			private Consumer<? super X> action;

			protected ScrapTask(X input, Consumer<? super X> consumer) {
				super();
				this.input = Scrapable.requireNonNull(input, "input");
				this.action = Scrapable.requireNonNull(consumer, "consumer");
			}

			@Override
			public void run() {
				try {
					action.accept(input);
				} finally {
					input = null;
					action = null;
				}

			}

			public X input() {
				return input;
			}

			public Consumer<? super X> action() {
				return action;
			}

			@Override
			public String toString() {
				return "ScrapTask [" + Optional.ofNullable(input).map(v -> "input=" + v).orElse("completed") + "]";
			}

		}

	}

	/**
	 * utils
	 */

	private static <U> U requireNonNull(U value, String name) {
		return Objects.requireNonNull(value, () -> Objects.toString(name) + " is null");
	}

	private static RuntimeException propagateFailure(Throwable throwable) {
		if (throwable instanceof RuntimeException)
			throw (RuntimeException) throwable;
		return new RuntimeException("scrap failed", throwable);
	}

	private static RuntimeException multipleFailures(List<Throwable> errors) {
		var multiple = new RuntimeException("scrap failed, multiple errors");
		// noinspection ConstantConditions
		if (errors != null)
			for (Throwable t : errors)
				// this is ok, multiple is always a new non-singleton instance
				multiple.addSuppressed(t);
		return multiple;
	}

	private static void throwIfFatalFailure(Throwable ex) {
		if (ex instanceof VirtualMachineError)
			throw (VirtualMachineError) ex;
		if (ex instanceof ThreadDeath)
			throw (ThreadDeath) ex;
		if (ex instanceof LinkageError)
			throw (LinkageError) ex;
	}

	static enum Const {
		;

		private static final Scrapable COMPLETE = new Scrapable() {
			@Override
			public boolean isScrapped() {
				return true;
			}

			@Override
			public boolean scrap() {
				return false;
			}

			@Override
			public Scrapable onScrap(Runnable runnable) {
				runnable.run();
				return this;
			}

			@Override
			public String toString() {
				return Scrapable.class.getSimpleName() + " [complete]";
			}
		};
		private static final Scrapable NO_OP = new Scrapable() {
			@Override
			public boolean isScrapped() {
				return false;
			}

			@Override
			public boolean scrap() {
				return false;
			}

			@Override
			public Scrapable onScrap(Runnable runnable) {
				return Scrapable.complete();
			}

			@Override
			public String toString() {
				return Scrapable.class.getSimpleName() + " [no-op]";
			}
		};

	}

	@SuppressWarnings("unlikely-arg-type")
	public static void main(String[] args) {
		var test = new Scrapable.Impl() {
			@Override
			protected Collection<ScrapTask<?>> createScrapTaskStore() {
				if (true)
					return super.createScrapTaskStore();
				return Collections.newSetFromMap(new ConcurrentHashMap<>());
			}
		};
		Runnable listener0 = () -> System.out.println("try remove:" + new Date());
		test.onScrap(listener0);
		System.out.println(test.tryRemove(listener0));
		System.out.println(test.tryRemove(listener0));
		test.onScrap(() -> {
			System.out.println("first");
		});
		test.onScrap(Scrapable.create(() -> {
			System.out.println("scrappy");
		}));
		System.out.println("test - " + test);
		Date now = new Date();
		var second = test.onScrap(now, d -> {
			System.out.println("second:" + d);
		});
		Date now2 = new Date();
		var delTest = test.onScrap(now2, d -> {
			System.out.println("del:" + now2);
		});

		delTest.scrap();
		delTest.scrap();
		test.tryRemove(delTest);
		test.tryRemove(48);
		System.out.println("test - " + test);
		test.tryRemove(now2);
		System.out.println("test - " + test);
		AtomicReference<Scrapable> fourthRef = new AtomicReference<>();
		test.onScrap(() -> {
			System.out.println("third");
			fourthRef.get().scrap();
		});
		fourthRef.set(test.onScrap(() -> {
			System.out.println("forth");
		}));
		Scrapable listener = test.onScrap(() -> {
			System.out.println("cancel");
		});
		System.out.println("test - " + test);
		System.out.println("listener - " + listener);
		System.out.println(listener.scrap());
		System.out.println(listener.scrap());
		System.out.println("test - " + test);
		System.out.println("listener - " + listener);
		test.scrap();
		System.out.println("test - " + test);
		test.onScrap(() -> {
			System.out.println("fifth");
		});
		System.out.println("test - " + test);
	}

}
