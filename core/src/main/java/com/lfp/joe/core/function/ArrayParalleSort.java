package com.lfp.joe.core.function;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;
import java.util.concurrent.CountedCompleter;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.function.BiFunction;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.config.MachineConfig;

@SuppressWarnings("unchecked")
public class ArrayParalleSort {

	protected ArrayParalleSort() {}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();

	private static final Object ArraysParallelSortHelpers_FJObject_Sorter_CONSTRUCTOR_MUTEX = new Object();
	private static Constructor<? extends ForkJoinTask<?>> _ArraysParallelSortHelpers_FJObject_Sorter_CONSTRUCTOR;
	// 8192
	// private static final int MIN_ARRAY_SORT_GRAN = 1 << 13;
	// 2048
	private static final int MIN_ARRAY_SORT_GRAN = 1 << 11;

	public static <T> void accept(T[] a, Comparator<? super T> cmp) {
		accept(a, cmp, (Integer) null);
	}

	public static <T> void accept(T[] a, Comparator<? super T> cmp, Integer parallelism) {
		if (parallelism == null) {
			accept(a, cmp, MachineConfig.logicalCoreCount());
			return;
		}
		try {
			acceptInternal(a, cmp, parallelism, () -> new ForkJoinPool(parallelism), true);
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static <T> void accept(T[] a, Comparator<? super T> cmp, ForkJoinPool fjp) {
		try {
			acceptInternal(a, cmp, fjp.getParallelism(), () -> fjp, false);
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected static <T> void acceptInternal(T[] a, Comparator<? super T> cmp, int parallelism,
			Supplier<ForkJoinPool> forkJoinPoolSupplier, boolean shutdownForkJoinPool)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException,
			NegativeArraySizeException, NoSuchMethodException, SecurityException {
		Objects.requireNonNull(forkJoinPoolSupplier);
		if (cmp == null)
			cmp = (Comparator<? super T>) Comparator.naturalOrder();
		int n = a.length, p, g;
		if (n <= MIN_ARRAY_SORT_GRAN || (p = parallelism) == 1)
			Arrays.sort(a, cmp);
		else {
			ForkJoinTask<?> sortTask = getFJObjectSorterConstructor().newInstance(null, a,
					Array.newInstance(a.getClass().getComponentType(), n), 0, n, 0,
					((g = n / (p << 2)) <= MIN_ARRAY_SORT_GRAN) ? MIN_ARRAY_SORT_GRAN : g, cmp);
			var fjp = Objects.requireNonNull(forkJoinPoolSupplier.get());
			try {
				fjp.invoke(sortTask);
			} finally {
				if (shutdownForkJoinPool)
					fjp.shutdownNow();
			}
		}

	}

	protected static Constructor<? extends ForkJoinTask<?>> getFJObjectSorterConstructor()
			throws NoSuchMethodException, SecurityException {
		if (_ArraysParallelSortHelpers_FJObject_Sorter_CONSTRUCTOR == null)
			synchronized (ArraysParallelSortHelpers_FJObject_Sorter_CONSTRUCTOR_MUTEX) {
				if (_ArraysParallelSortHelpers_FJObject_Sorter_CONSTRUCTOR == null) {
					var classTypeFJObject = CoreReflections
							.tryForName(Arrays.class.getPackageName() + ".ArraysParallelSortHelpers.FJObject")
							.get();
					var classType = Stream.of(classTypeFJObject.getDeclaredClasses())
							.filter(v -> "Sorter".equals(v.getSimpleName()))
							.findFirst()
							.get();
					CoreReflections.moduleAddOpens(classType, THIS_CLASS);
					var ctor = classType.getDeclaredConstructor(CountedCompleter.class, Object[].class, Object[].class,
							int.class, int.class, int.class, int.class, Comparator.class);
					CoreReflections.setAccessible(ctor);
					_ArraysParallelSortHelpers_FJObject_Sorter_CONSTRUCTOR = (Constructor<? extends ForkJoinTask<?>>) ctor;
				}
			}
		return _ArraysParallelSortHelpers_FJObject_Sorter_CONSTRUCTOR;
	}

	public static void main(String[] args) {
		BiFunction<Integer, Integer, Integer> randomGen = (min, max) -> min + (int) (Math.random() * ((max - min) + 1));
		var array = IntStream.range(0, 10_000).boxed().map(v -> randomGen.apply(1, 10_000)).toArray(Integer[]::new);
		System.out.println(Arrays.toString(array));
		ArrayParalleSort.accept(array, Comparator.naturalOrder(), 4);
		System.out.println(Arrays.toString(array));
	}

}
