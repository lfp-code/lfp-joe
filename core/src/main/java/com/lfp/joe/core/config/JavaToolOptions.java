package com.lfp.joe.core.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class JavaToolOptions {

	protected JavaToolOptions() {}

	private static final List<String> KEYS = List.of("JAVA_TOOL_OPTIONS", "sun.java.command");

	private static final Object KEY_VALUE_MM_LOCK = new Object();
	private static Map<String, Set<String>> KEY_VALUE_MM;

	public static Stream<String> streamValues(String name) {
		if (name == null || name.isBlank())
			return Stream.empty();
		Stream<Entry<Boolean, String>> estream = Stream.empty();
		BiFunction<Boolean, Collection<? extends String>, Stream<Entry<Boolean, String>>> streamer = (ignoreCase,
				values) -> {
			if (values == null)
				return Stream.empty();
			return values.stream().filter(Objects::nonNull).map(v -> Map.entry(ignoreCase, v));
		};
		{
			var values = get().get(name);
			estream = Stream.concat(estream, streamer.apply(false, values));
		}
		{
			for (var key : get().keySet()) {
				if (!key.equals(name) && key.equalsIgnoreCase(name)) {
					var values = get().get(key);
					estream = Stream.concat(estream, streamer.apply(true, values));
				}
			}
		}
		estream = estream.distinct();
		Comparator<Entry<Boolean, String>> sorter = Comparator.comparing(ent -> {
			return ent.getValue().isBlank() ? 0 : Integer.MAX_VALUE;
		});
		sorter = sorter.thenComparing(ent -> {
			return ent.getKey() ? 1 : 0;
		});
		estream = estream.sorted(sorter);
		return estream.map(Entry::getValue);
	}

	public static Map<String, Set<String>> get() {
		if (KEY_VALUE_MM == null)
			synchronized (KEY_VALUE_MM_LOCK) {
				if (KEY_VALUE_MM == null)
					KEY_VALUE_MM = parseKeyValueMultimap();
			}
		return KEY_VALUE_MM;
	}

	protected static Map<String, Set<String>> parseKeyValueMultimap() {
		Map<String, Set<String>> mm = new LinkedHashMap<>();
		for (var key : KEYS) {
			putAll(System.getenv(key), mm);
			putAll(System.getProperty(key), mm);
		}
		for (var key : mm.keySet()) {
			mm.computeIfPresent(key, (k, v) -> {
				return Collections.unmodifiableSet(v);
			});
		}
		return Collections.unmodifiableMap(mm);
	}

	private static void putAll(String value, Map<String, Set<String>> keyValueMultimap) {
		for (var ent : parseArgumentEntries(value)) {
			keyValueMultimap.computeIfAbsent(ent.getKey(), nil -> new LinkedHashSet<>()).add(ent.getValue());
		}
	}

	private static List<Entry<String, String>> parseArgumentEntries(String input) {
		if (input == null)
			return List.of();
		input = input.trim();
		if (input.isEmpty())
			return List.of();
		List<Entry<String, String>> entryList = new ArrayList<>();
		var dparts = input.split("(^|\\s)-D");
		if (dparts.length == 1)
			return List.of();
		for (var dpart : dparts) {
			if (dpart.isEmpty())
				continue;
			if (Character.isWhitespace(dpart.charAt(0)))
				continue;
			dpart = dpart.trim();
			var splitAt = dpart.indexOf("=");
			var key = splitAt < 0 ? dpart : dpart.substring(0, splitAt);
			if (key.isBlank())
				continue;
			var value = splitAt < 0 ? null : dpart.substring(splitAt + 1);
			value = parseArguments(value).stream().findFirst().orElse("");
			entryList.add(Map.entry(key, value));
		}
		return entryList;
	}

	private static List<String> parseArguments(String input) {
		if (input == null || input.isBlank())
			return List.of();
		List<String> matchList = new ArrayList<String>();
		Pattern regex = Pattern.compile("[^\\s\"']+|\"([^\"]*)\"|'([^']*)'");
		Matcher regexMatcher = regex.matcher(input);
		while (regexMatcher.find()) {
			if (regexMatcher.group(1) != null) {
				// Add double-quoted string without the quotes
				matchList.add(regexMatcher.group(1));
			} else if (regexMatcher.group(2) != null) {
				// Add single-quoted string without the quotes
				matchList.add(regexMatcher.group(2));
			} else {
				// Add unquoted word
				matchList.add(regexMatcher.group());
			}
		}
		return Collections.unmodifiableList(matchList);
	}

	public static void main(String[] args) {
		String input = "-D";
		System.out.println(parseArgumentEntries(input));
		System.out.println(get());
		get().get("sun.jnu.encoding").iterator().remove();
	}
}
