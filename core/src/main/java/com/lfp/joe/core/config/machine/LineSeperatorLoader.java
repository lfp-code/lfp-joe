package com.lfp.joe.core.config.machine;

public class LineSeperatorLoader extends JavaToolOptionsMachineConfigLoader<String> {

	public static final LineSeperatorLoader INSTANCE = new LineSeperatorLoader();

	private LineSeperatorLoader() {
		super("line.separator", "\n");
	}

	@Override
	protected String parse(String value) throws Exception {
		return value == null || value.isBlank() ? null : value;
	}

	@Override
	protected void logLoad(String value) {
		super.logLoad(value == null ? value : value.replaceAll("(\\r|\\n|\\r\\n)+", "\\\\n"));
	}
}
