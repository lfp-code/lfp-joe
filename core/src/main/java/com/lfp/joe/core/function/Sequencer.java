package com.lfp.joe.core.function;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class Sequencer {

	private static final Object INSTANCE_MUTEX = new Object();
	private static Sequencer _INSTANCE;

	public static Sequencer instance() {
		if (_INSTANCE == null)
			synchronized (INSTANCE_MUTEX) {
				if (_INSTANCE == null)
					_INSTANCE = new Sequencer();
			}
		return _INSTANCE;
	}

	private long[] store;

	public SequenceId next() {
		final long[] value;
		synchronized (this) {
			if (store == null || store.length == 0)
				store = new long[] { Long.MIN_VALUE };
			else {
				var index = store.length - 1;
				long next;
				if (store[index] == Long.MAX_VALUE) {
					index++;
					store = Arrays.copyOf(store, index + 1);
					next = Long.MIN_VALUE;
				} else
					next = store[index] + 1;
				store[index] = next;
			}
			value = Arrays.copyOf(store, store.length);
		}
		return new SequenceId(value);
	}

	public static class SequenceId implements Comparable<SequenceId> {

		private final long[] value;

		protected SequenceId(long[] value) {
			super();
			this.value = value;
		}

		@Override
		public int compareTo(SequenceId other) {
			if (other == null)
				return -1;
			var len = this.value.length;
			if (len == 0 && other.value.length == 0)
				return 0;
			else if (len < other.value.length)
				return -1;
			else if (len > other.value.length)
				return 1;
			var v1 = this.value[len - 1];
			var v2 = other.value[len - 1];
			var comparison = ((Long) v1).compareTo(v2);
			return comparison < 0 ? -1 : comparison > 0 ? 1 : 0;
		}

		public LongStream values() {
			return LongStream.of(value);
		}

		@Override
		public String toString() {
			return "SequenceId [value=" + Arrays.toString(value) + "]";
		}

	}

	public static void main(String[] args) {
		Sequencer.instance().store = new long[] { Long.MAX_VALUE, Long.MAX_VALUE - 5 };
		var ids = IntStream.range(0, 10).boxed().map(nil -> Sequencer.instance().next()).collect(Collectors.toList());
		Collections.shuffle(ids);
		Collections.sort(ids);
		ids.forEach(v -> {
			System.out.println(v);
		});
	}

}
