package com.lfp.joe.core.classpath;

import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.Executor;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.RunnableScheduledFuture;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class InterfaceSet extends AbstractSet<Class<?>> {

	private final List<Class<?>> backingList = new ArrayList<>();
	private final Predicate<Class<?>> filter;

	public InterfaceSet(Object... interfaceSources) {
		this(null, interfaceSources);
	}

	public InterfaceSet(Predicate<Class<?>> filter, Object... interfaceSources) {
		this.filter = filter;
		this.add(interfaceSources);
	}

	public boolean isAssignableFromAll(Class<?> classType) {
		if (classType == null)
			return false;
		return this.stream().allMatch(v -> v.isAssignableFrom(classType));
	}

	public boolean isInstanceAll(Object input) {
		if (input == null)
			return false;
		return this.stream().allMatch(v -> v.isInstance(input));
	}

	@Override
	public Iterator<Class<?>> iterator() {
		return Collections.unmodifiableList(backingList).iterator();
	}

	@Override
	public int size() {
		return backingList.size();
	}

	@Override
	public boolean add(Class<?> classType) {
		Objects.requireNonNull(classType);
		if (Object.class.equals(classType))
			return false;
		var ifaceStream = Stream.of(classType.getInterfaces());
		boolean addSuperClass;
		if (classType.isInterface()) {
			ifaceStream = Stream.concat(Stream.of(classType), ifaceStream);
			addSuperClass = false;
		} else
			addSuperClass = true;
		var ifaceIter = ifaceStream.iterator();
		var mod = false;
		while (ifaceIter.hasNext()) {
			var iface = ifaceIter.next();
			if (filter != null && !filter.test(iface))
				continue;
			// skip if iface is super class of existing
			if (backingList.stream().anyMatch(iface::isAssignableFrom))
				continue;
			// remove existing if super of iface
			var removed = backingList.removeIf(v -> v.isAssignableFrom(iface));
			// add
			var added = backingList.add(iface);
			if (removed || added)
				mod = true;
		}
		if (addSuperClass) {
			var superClass = classType.getSuperclass();
			if (superClass != null && add(superClass))
				mod = true;
		}
		return mod;
	}

	public boolean add(Object... interfaceSources) {
		if (interfaceSources == null || interfaceSources.length == 0)
			return false;
		boolean mod = false;
		for (var interfaceSource : interfaceSources) {
			Objects.requireNonNull(interfaceSource);
			if (interfaceSource instanceof Class) {
				if (add((Class<?>) interfaceSource))
					mod = true;
			} else {
				if (add(interfaceSource.getClass()))
					mod = true;
			}
		}
		return mod;
	}

	@Override
	public Class<?>[] toArray() {
		return toArray(false);
	}

	public Class<?>[] toArray(boolean disableSort) {
		if (disableSort)
			return backingList.toArray(Class<?>[]::new);
		return backingList.stream().sorted(Comparator.comparing(Class::getName)).toArray(Class<?>[]::new);
	}

	@Override
	public String toString() {
		return backingList.toString();
	}

	public static void main(String[] args) {
		var ifaceSet = new InterfaceSet();
		ifaceSet.add(RunnableFuture.class);
		System.out.println(ifaceSet.size() + " " + ifaceSet.toString());
		ifaceSet.add(RunnableScheduledFuture.class);
		System.out.println(ifaceSet.size() + " " + ifaceSet.toString());
		ifaceSet.add(Executor.class);
		System.out.println(ifaceSet.size() + " " + ifaceSet.toString());
		ifaceSet.add(AbstractExecutorService.class);
		System.out.println(ifaceSet.size() + " " + ifaceSet.toString());
	}

}
