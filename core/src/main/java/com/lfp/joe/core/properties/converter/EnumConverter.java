package com.lfp.joe.core.properties.converter;

import java.lang.reflect.Method;

import org.aeonbits.owner.Converter;

public class EnumConverter<E extends Enum<E>> implements Converter<E> {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public E convert(Method method, String input) {
		if (input == null || input.isEmpty())
			return null;
		Class<?> returnType = method.getReturnType();
		if (returnType == null || !Enum.class.isAssignableFrom(returnType))
			return null;
		try {
			return (E) Enum.valueOf((Class) returnType, input);
		} catch (Exception e) {
		}
		try {
			return (E) Enum.valueOf((Class) returnType, input.toUpperCase());
		} catch (Exception e) {
		}
		for (Object enumObj : returnType.getEnumConstants()) {
			E enumVal = (E) enumObj;
			String name = enumVal.name();
			if (name != null && name.equalsIgnoreCase(input))
				return enumVal;
		}
		return null;
	}

}