package com.lfp.joe.core.properties.loader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.function.MemoizedSupplier;

public enum PropertiesFileLoader implements MultiLoader.ClassTypeLoader {
	INSTANCE;

	private final static MemoizedSupplier<org.slf4j.Logger> LOGGER_S = MemoizedSupplier.create(() -> {
		Class<?> classType = new Object() {}.getClass().getEnclosingClass();
		return org.slf4j.LoggerFactory.getLogger(classType);
	});

	// ordering is important - prioritize secret vals
	private static final List<String> DEFAULT_LOAD_LOCATIONS = List.of("config-secret/application.properties",
			"config-private/application.properties", "application.properties");
	// ordering is important - prioritize local files
	private static final List<String> PREFIXES = List.of("src/main/resources/", "");
	private static final MemoizedSupplier<Iterable<Properties>> PROPERTIES_ITERABLE_S = MemoizedSupplier
			.create(() -> createPropertiesIterable());

	@Override
	public void load(String instanceId, Properties result, Class<Config> classType,
			Iterable<Entry<Method, Set<String>>> publicNoArgMethodEntries) {
		PROPERTIES_ITERABLE_S.get().forEach(p -> {
			Loaders.putAll(result, publicNoArgMethodEntries, p);
		});
	}

	private static Iterable<Properties> createPropertiesIterable() {
		Map<String, File> fileMap = new LinkedHashMap<>();
		for (String prefix : PREFIXES)
			for (String location : DEFAULT_LOAD_LOCATIONS) {
				String lookup = prefix + location;
				File file = new File(lookup);
				if (file == null || !file.exists()) {
					URL resource = Thread.currentThread().getContextClassLoader().getResource(lookup);
					if (resource != null)
						try {
							file = new File(resource.getFile());
						} catch (Exception e) {
							// suppress
						}
				}
				if (file == null || !file.exists())
					continue;
				fileMap.putIfAbsent(file.getAbsolutePath(), file);
			}
		Map<File, Properties> loadMap = new ConcurrentHashMap<>();
		return () -> {
			Stream<Properties> stream = fileMap.keySet().stream().map(v -> fileMap.get(v)).map(file -> {
				return loadMap.computeIfAbsent(file, nil -> {
					Properties properties = new Properties();
					loadPropertiesUnchecked(properties, file);
					if (properties.size() > 0)
						LOGGER_S.get().info("loaded local properties:{}", file.getAbsolutePath());
					return properties;
				});
			}).filter(v -> v.size() > 0);
			return stream.iterator();
		};
	}

	private static void loadPropertiesUnchecked(Properties properties, File file) {
		try (InputStream is = new FileInputStream(file)) {
			properties.load(is);
		} catch (IOException e) {
			throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e) : new RuntimeException(e);
		}
	}

}
