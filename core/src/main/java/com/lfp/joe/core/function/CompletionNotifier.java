package com.lfp.joe.core.function;

import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

import com.lfp.joe.core.process.future.Completion;

public interface CompletionNotifier<X> {

	boolean complete(X result);

	boolean isComplete();

	boolean addListener(Consumer<? super X> listener);

	boolean removeListener(Consumer<? super X> listener);

	default X block() throws InterruptedException {
		try {
			return block(Durations.max());
		} catch (TimeoutException e) {
			throw new IllegalStateException(e);
		}
	}

	default X block(long timeout, TimeUnit unit) throws InterruptedException, TimeoutException {
		return block(Durations.from(timeout, unit));
	}

	X block(Duration timeout) throws InterruptedException, TimeoutException;

	public static <X> CompletionNotifier<X> create() {
		return new CompletionNotifier.Impl<>();
	}

	public static class Impl<X> extends Scrapable.Impl implements CompletionNotifier<X> {

		private final CompletableFuture<X> future = new CompletableFuture<>();

		public Impl() {
			this.future.whenComplete((v, t) -> this.scrap());
			this.onScrap(() -> this.future.complete(null));
		}

		@Override
		public boolean complete(X result) {
			return future.complete(result);
		}

		@Override
		public boolean isComplete() {
			return future.isDone();
		}

		@Override
		public boolean addListener(Consumer<? super X> listener) {
			if (listener == null)
				return false;
			onScrap(listener, v -> v.accept(Completion.getNow(future).resultOrThrow()));
			return true;
		}

		@Override
		public boolean removeListener(Consumer<? super X> listener) {
			return this.tryRemove(listener).isPresent();
		}

		@Override
		public X block(Duration timeout) throws InterruptedException, TimeoutException {
			return Completion.get(future, timeout)
					.orElseThrow(() -> new TimeoutException("timeout:" + timeout))
					.resultOrThrow();
		}

	}

}
