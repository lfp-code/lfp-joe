package com.lfp.joe.core.properties;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.util.AbstractMap.SimpleEntry;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.Key;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.MethodSorter;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.core.properties.factory.ConfigFactoryLFP;

public class Configs {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final MemoizedSupplier<org.slf4j.Logger> LOGGER_S = MemoizedSupplier.create(() -> {
		return org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	});
	private static final boolean SCAN_SUPER_CLASS_ANNOTATIONS = true;

	public static <X extends Config> X get(Class<X> configClassType) {
		Objects.requireNonNull(configClassType);
		X result = ConfigFactoryLFP.get().getOrCreate(configClassType);
		return result;
	}

	public static Stream<String> streamKeyAnnotationValues(Method method) {
		if (method == null)
			return Stream.empty();
		var methodStream = Stream.iterate(Stream.empty(), Objects::nonNull, new UnaryOperator<Stream<Method>>() {

			private int index = -1;

			@Override
			public Stream<Method> apply(Stream<Method> prev) {
				index++;
				if (index == 0)
					return Stream.of(method);
				else if (index == 1 && SCAN_SUPER_CLASS_ANNOTATIONS) {
					return CoreReflections.streamMethods(method.getDeclaringClass(), true).filter(m -> {
						if (!method.getName().equals(m.getName()))
							return false;
						if (method.getParameterCount() != m.getParameterCount())
							return false;
						var methodPtypes = method.getParameterTypes();
						var mPtypes = method.getParameterTypes();
						for (int i = 0; i < methodPtypes.length; i++) {
							if (!mPtypes[i].isAssignableFrom(methodPtypes[i]))
								return false;
						}
						return true;
					});
				}
				return null;
			}
		}).flatMap(Function.identity()).distinct();
		var valueStream = methodStream.map(m -> {
			Key[] annos = m.getAnnotationsByType(Key.class);
			return streamKeyAnnotationValues(annos);
		}).flatMap(Function.identity());
		return valueStream.distinct();
	}

	public static Stream<String> streamKeyAnnotationValues(Key... keys) {
		if (keys == null)
			return Stream.empty();
		return streamKeyAnnotationValues(Arrays.asList(keys));
	}

	public static Stream<String> streamKeyAnnotationValues(Iterable<? extends Annotation> annos) {
		if (annos == null)
			return Stream.empty();
		var stream = StreamSupport.stream(annos.spliterator(), false);
		stream = stream.filter(Objects::nonNull);
		var keyStream = stream.filter(Key.class::isInstance).map(Key.class::cast);
		return keyStream.map(Key::value).filter(Objects::nonNull).filter(Predicate.not(String::isBlank)).distinct();
	}

	public static Stream<Entry<String, Object>> streamProperties(Config config) {
		return streamPropertyMethods(config == null ? null : config).map(ent -> {
			var key = ent.getKey();
			var value = Throws.unchecked(() -> ent.getValue().invoke(config));
			return new SimpleEntry<String, Object>(key, value);
		});

	}

	public static Stream<Entry<String, Method>> streamPropertyMethods(Config config) {
		var estream = streamPropertyMethods(config == null ? null : config.getClass()).map(method -> {
			var keyValues = streamKeyAnnotationValues(method);
			keyValues = Stream.concat(keyValues, Stream.of(method.getName()));
			keyValues = keyValues.filter(Objects::nonNull);
			keyValues = keyValues.distinct();
			return keyValues.map(key -> {
				Entry<String, Method> entry = new SimpleEntry<>(key, method);
				return entry;
			});
		}).flatMap(Function.identity());
		return estream;
	}

	@SuppressWarnings("unchecked")
	public static Stream<Method> streamPropertyMethods(Class<? extends Config> configClass) {
		if (configClass == null)
			return Stream.empty();
		Predicate<Method> methodPredicate;
		{
			Predicate<Method> predicate = v -> true;
			predicate = predicate.and(v -> v.getParameterCount() == 0);
			predicate = predicate.and(v -> Modifier.isAbstract(v.getModifiers()));
			predicate = predicate.and(v -> !v.isDefault());
			predicate = predicate.and(v -> !Object.class.equals(v.getDeclaringClass()));
			methodPredicate = predicate;
		}
		var configClassTypeStream = CoreReflections.streamTypes(configClass)
				.filter(Predicate.not(Proxy::isProxyClass))
				.filter(Config.class::isAssignableFrom)
				.filter(Predicate.not(Config.class::equals));
		var methodStream = configClassTypeStream.flatMap(ct -> {
			return CoreReflections.streamMethods(ct, false).filter(methodPredicate);
		});
		return methodStream;
	}

	@SuppressWarnings("unchecked")
	public static void printProperties() {
		Class<?> callingClass = CoreReflections.getCallingClass(THIS_CLASS);
		printProperties((Class<? extends Config>) callingClass, null);
	}

	@SuppressWarnings("unchecked")
	public static void printProperties(PrintOptions printOptions) {
		Class<?> callingClass = CoreReflections.getCallingClass(THIS_CLASS);
		printProperties((Class<? extends Config>) callingClass, printOptions);
	}

	public static void printProperties(Class<? extends Config> configClassType, PrintOptions printOptions) {
		Objects.requireNonNull(configClassType);
		if (printOptions == null) {
			printProperties(configClassType, PrintOptions.builder().build());
			return;
		}
		Comparator<Method> methodSorter;
		if (printOptions.isSortByName())
			methodSorter = Comparator.comparing(v -> getMethodKey(v).toLowerCase());
		else
			methodSorter = new MethodSorter(configClassType);
		MemoizedSupplier<Config> configInstance = MemoizedSupplier.create(() -> Configs.get(configClassType));
		Iterable<Method> methodIble = () -> {
			Stream<Method> stream = streamPropertyMethods(configClassType);
			return stream.sorted(methodSorter).iterator();
		};
		Class<?> jsonOutputClassType;
		if (!printOptions.isJsonOutput())
			jsonOutputClassType = null;
		else {
			jsonOutputClassType = CoreReflections.tryForName("com.google.gson.GsonBuilder").orElse(null);
			if (jsonOutputClassType == null)
				LOGGER_S.get().warn("json print requested, but com.google.gson.Gson not found on classpath");
		}
		Map<String, String> output = new LinkedHashMap<>();
		for (Method method : methodIble) {
			MemoizedSupplier<String> valueSuppier = MemoizedSupplier.create(() -> {
				Object value = Throws.unchecked(() -> method.invoke(configInstance.get()));
				if (value == null)
					return null;
				var valueStr = Objects.toString(value);
				if (valueStr.isBlank())
					return null;
				return valueStr;
			});
			if (printOptions.isSkipPopulated() && valueSuppier.get() != null)
				continue;
			String key = getKey(configClassType, printOptions, jsonOutputClassType != null, output, method);
			if (key == null)
				continue;
			var valueStr = Optional.ofNullable(printOptions.isIncludeValues() ? valueSuppier.get() : null).orElse("");
			output.put(key, valueStr);
		}
		Callable<String> formatter = null;
		if (jsonOutputClassType != null) {
			System.out.println(String.format("\n%s\n", configClassType.getName()));
			formatter = () -> {
				Object gsonBuilder = CoreReflections.newInstanceUnchecked(jsonOutputClassType);
				gsonBuilder.getClass().getMethod("setPrettyPrinting").invoke(gsonBuilder);
				Object gson = gsonBuilder.getClass().getMethod("create").invoke(gsonBuilder);
				Method method = gson.getClass().getMethod("toJson", Object.class);
				Object json = method.invoke(gson, output);
				return (String) json;
			};
		} else {
			System.out.println(String.format("\n#--- %s ---\n", configClassType.getName()));
			formatter = () -> {
				StringBuilder out = new StringBuilder();
				output.entrySet().forEach(e -> out.append(String.format("%s=%s\n", e.getKey(), e.getValue())));
				return out.toString();
			};
		}
		System.out.println(Throws.unchecked(formatter::call));
	}

	private static String getKey(Class<? extends Config> configClassType, PrintOptions printOptions,
			boolean disableMethodComment, Map<String, String> output, Method method) {
		for (var key : Stream.of(getMethodKey(method), method.getName())
				.collect(Collectors.toCollection(LinkedHashSet::new))) {
			String methodComment;
			if (!disableMethodComment && !key.equals(method.getName())) {
				methodComment = method.getName();
				if (printOptions.isPrependClassNames())
					methodComment = configClassType.getName() + "_" + methodComment;
				methodComment = "#" + methodComment;
			} else
				methodComment = null;
			if (printOptions.isPrependClassNames())
				key = configClassType.getName() + "_" + key;
			if (output.containsKey(key))
				continue;
			if (methodComment != null)
				output.put(methodComment, "");
			return key;
		}
		return null;
	}

	private static String getMethodKey(Method method) {
		return streamKeyAnnotationValues(method).findFirst().orElse(method.getName());
	}

}
