package com.lfp.joe.core.properties.code;

import javax.annotation.Generated;

public class PrintOptions {

	public static PrintOptions properties() {
		return propertiesBuilder().build();
	}

	public static PrintOptions.Builder propertiesBuilder() {
		return PrintOptions.builder().withJsonOutput(false).withPrependClassNames(true);
	}

	public static PrintOptions json() {
		return jsonBuilder().build();
	}

	public static PrintOptions.Builder jsonBuilder() {
		return PrintOptions.builder().withJsonOutput(true).withPrependClassNames(false);
	}

	private final boolean skipPopulated;

	private final boolean includeValues;

	private final boolean prependClassNames;

	private final boolean sortByName;

	private final boolean jsonOutput;

	@Generated("SparkTools")
	private PrintOptions(Builder builder) {
		this.skipPopulated = builder.skipPopulated;
		this.includeValues = builder.includeValues;
		this.prependClassNames = builder.prependClassNames;
		this.sortByName = builder.sortByName;
		this.jsonOutput = builder.jsonOutput;
	}

	public boolean isSkipPopulated() {
		return skipPopulated;
	}

	public boolean isIncludeValues() {
		return includeValues;
	}

	public boolean isPrependClassNames() {
		return prependClassNames;
	}

	public boolean isSortByName() {
		return sortByName;
	}

	public boolean isJsonOutput() {
		return jsonOutput;
	}

	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	@Generated("SparkTools")
	public static Builder builderFrom(PrintOptions printOptions) {
		return new Builder(printOptions);
	}

	@Generated("SparkTools")
	public static final class Builder {
		private boolean skipPopulated;
		private boolean includeValues;
		private boolean prependClassNames;
		private boolean sortByName;
		private boolean jsonOutput;

		private Builder() {
		}

		private Builder(PrintOptions printOptions) {
			this.skipPopulated = printOptions.skipPopulated;
			this.includeValues = printOptions.includeValues;
			this.prependClassNames = printOptions.prependClassNames;
			this.sortByName = printOptions.sortByName;
			this.jsonOutput = printOptions.jsonOutput;
		}

		public Builder withSkipPopulated(boolean skipPopulated) {
			this.skipPopulated = skipPopulated;
			return this;
		}

		public Builder withIncludeValues(boolean includeValues) {
			this.includeValues = includeValues;
			return this;
		}

		public Builder withPrependClassNames(boolean prependClassNames) {
			this.prependClassNames = prependClassNames;
			return this;
		}

		public Builder withSortByName(boolean sortByName) {
			this.sortByName = sortByName;
			return this;
		}

		public Builder withJsonOutput(boolean jsonOutput) {
			this.jsonOutput = jsonOutput;
			return this;
		}

		public PrintOptions build() {
			return new PrintOptions(this);
		}
	}

}
