package com.lfp.joe.core.lock;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Function;
import java.util.function.Supplier;

import com.lfp.joe.core.function.Throws.ThrowingFunction;

public class KeyLock<K> implements Function<K, ReadWriteLock> {

	public static <U> KeyLock<U> create() {
		return new KeyLock<>();
	}

	private final ConcurrentMap<K, LockContext> lockContexts;
	private final Supplier<ReadWriteLock> readWriteLockFactory;

	protected KeyLock() {
		this(new ConcurrentHashMap<>(), ReentrantReadWriteLock::new);
	}

	protected KeyLock(ConcurrentMap<K, LockContext> lockContexts, Supplier<ReadWriteLock> readWriteLockFactory) {
		super();
		this.lockContexts = Objects.requireNonNull(lockContexts);
		this.readWriteLockFactory = Objects.requireNonNull(readWriteLockFactory);
	}

	@Override
	public ReadWriteLock apply(K key) {
		return new ReadWriteLock() {

			@Override
			public Lock readLock() {
				return KeyLock.this.readLock(key);
			}

			@Override
			public Lock writeLock() {
				return KeyLock.this.writeLock(key);
			}
		};
	}

	public Lock readLock(K key) {
		return lock(key, false);
	}

	public Lock writeLock(K key) {
		return lock(key, true);
	}

	public Lock lock(K key, boolean write) {
		return new Lock() {

			@Override
			public void lock() {
				acquireLock(key, write, lock -> {
					lock.lock();
					return true;
				});
			}

			@Override
			public void lockInterruptibly() throws InterruptedException {
				acquireLock(key, write, lock -> {
					lock.lockInterruptibly();
					return true;
				});
			}

			@Override
			public boolean tryLock() {
				return acquireLock(key, write, lock -> {
					return lock.tryLock();
				});
			}

			@Override
			public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
				return acquireLock(key, write, lock -> {
					return lock.tryLock(time, unit);
				});
			}

			@Override
			public Condition newCondition() {
				return accessLockContext(key, ctx -> {
					return ctx.getLock(write).newCondition();
				});
			}

			@Override
			public void unlock() {
				releaseLock(key, write);
			}

		};
	}

	@Override
	public String toString() {
		Map<K, Integer> summary = null;
		for (var key : lockContexts.keySet()) {
			var lockContext = lockContexts.get(key);
			if (lockContext == null)
				continue;
			var count = lockContext.getCounter().get();
			if (summary == null)
				summary = new LinkedHashMap<>();
			summary.put(key, count);
		}
		if (summary == null)
			summary = Map.of();
		return summary.toString();
	}

	protected <U, E extends Exception> U accessLockContext(K key, ThrowingFunction<LockContext, U, E> accessor)
			throws E {
		var lockContext = lockContexts.compute(key, (nil, ctx) -> {
			if (ctx == null)
				ctx = new LockContext(readWriteLockFactory.get());
			ctx.getCounter().incrementAndGet();
			return ctx;
		});
		try {
			return accessor.apply(lockContext);
		} catch (Throwable t) {
			releaseLockContext(key, lockContext);
			throw t;
		}
	}

	protected void releaseLockContext(K key, LockContext lockContext) {
		if (lockContext.getCounter().decrementAndGet() > 0)
			return;
		lockContexts.computeIfPresent(key, (nil, ctx) -> {
			return ctx.getCounter().get() > 0 ? ctx : null;
		});
	}

	protected <E extends Exception> boolean acquireLock(K key, boolean write,
			ThrowingFunction<Lock, Boolean, E> acquireAction) throws E {
		return accessLockContext(key, lockContext -> {
			if (acquireAction.apply(lockContext.getLock(write)))
				return true;
			releaseLockContext(key, lockContext);
			return false;
		});
	}

	protected void releaseLock(K key, boolean write) {
		var lockContext = lockContexts.get(key);
		if (lockContext == null)
			throw lockContextNotFoundFailure(key, write);
		lockContext.getLock(write).unlock();
		releaseLockContext(key, lockContext);
	}

	protected RuntimeException lockContextNotFoundFailure(K key, boolean write) {
		var message = String.format("lock context not found. key:%s write:%s thread:%s", key, write,
				Thread.currentThread().getName());
		return new IllegalMonitorStateException(message);
	}

	protected RuntimeException reentryFailure(K key, boolean write) {
		var message = String.format("lock reentry. key:%s write:%s thread:%s", key, write,
				Thread.currentThread().getName());
		return new IllegalMonitorStateException(message);
	}

	protected static class LockContext {

		private final AtomicInteger counter = new AtomicInteger();
		private final ReadWriteLock readWriteLock;

		public LockContext(ReadWriteLock readWriteLock) {
			super();
			this.readWriteLock = Objects.requireNonNull(readWriteLock);
		}

		public AtomicInteger getCounter() {
			return counter;
		}

		public Lock getLock(boolean write) {
			return write ? readWriteLock.writeLock() : readWriteLock.readLock();
		}

	}
}
