package com.lfp.joe.core.classpath;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import de.adito.picoservice.PicoService;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
@PicoService
public @interface FactoryMethod {}
