package com.lfp.joe.core.function;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;

public class DistinctInstancePredicate<X> implements Predicate<X> {

	public static <U> DistinctInstancePredicate<U> create() {
		return new DistinctInstancePredicate<>();
	}

	private final Object distinctMapMutex = new Object();
	private Map<Optional<X>, List<X>> _distinctMap;

	@Override
	public boolean test(X value) {
		var resultRef = new boolean[] { false };
		distinctMap().compute(Optional.ofNullable(value), (nilk1, instances) -> {
			return test(instances, value, () -> resultRef[0] = true);
		});
		return resultRef[0];
	}

	protected Map<Optional<X>, List<X>> distinctMap() {
		if (_distinctMap == null)
			synchronized (distinctMapMutex) {
				if (_distinctMap == null)
					_distinctMap = new ConcurrentHashMap<>();
			}
		return _distinctMap;
	}

	protected static <U> List<U> test(List<U> instances, U value, Runnable successCallback) {
		if (instances == null) {
			successCallback.run();
			if (value == null)
				// empty list represents null
				return List.of();
			return List.of(value);
		}
		// null already added with empty list
		if (value == null)
			return instances;
		if (instances.stream().anyMatch(v -> value == v))
			return instances;
		successCallback.run();
		if (instances.size() == 1) {
			var copy = new ArrayList<U>(2);
			copy.addAll(instances);
			instances = copy;
		}
		instances.add(value);
		return instances;
	}
}
