package com.lfp.joe.core.config.machine;

import java.io.File;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class RootDirectoryLoader extends MachineConfigLoader<File> {

	public static final RootDirectoryLoader INSTANCE = new RootDirectoryLoader();

	@Override
	protected Iterable<? extends Callable<? extends File>> loaders() {
		List<Callable<File>> loaders = new ArrayList<>();
		loaders.add(() -> loadFromWorkingDirectory());
		loaders.add(() -> loadFirst());
		return loaders;
	}

	private File loadFromWorkingDirectory() {
		var workingPath = tryGetPath(new File("")).orElse(null);
		if (workingPath == null)
			return null;
		Entry<File, String> rootEntry = null;
		var rootDirectoryIter = streamRootDirectories().iterator();
		while (rootDirectoryIter.hasNext()) {
			var rootDirectory = rootDirectoryIter.next();
			var rootDirectoryPath = tryGetPath(rootDirectory).orElse(null);
			if (rootDirectoryPath == null)
				continue;
			if (workingPath.indexOf(rootDirectoryPath) != 0)
				continue;
			if (rootEntry == null || rootDirectoryPath.length() > rootEntry.getValue().length())
				rootEntry = Map.entry(rootDirectory, rootDirectoryPath);
		}
		return Optional.ofNullable(rootEntry).map(Entry::getKey).orElse(null);
	}

	private File loadFirst() {
		return streamRootDirectories().findFirst().orElse(null);
	}

	private static Stream<File> streamRootDirectories() {
		var pathStream = StreamSupport.stream(FileSystems.getDefault().getRootDirectories().spliterator(), false);
		pathStream = pathStream.filter(Objects::nonNull);
		var fileStream = pathStream.map(v -> {
			try {
				return v.toFile();
			} catch (Throwable t) {
				return null;
			}
		});
		fileStream = fileStream.filter(Objects::nonNull);
		return fileStream;

	}

	private static Optional<String> tryGetPath(File file) {
		if (file == null)
			return Optional.empty();
		String path;
		try {
			path = file.getAbsolutePath();
		} catch (Throwable t) {
			return Optional.empty();
		}
		boolean mod;
		do {
			mod = false;
			if (path == null || path.isEmpty())
				break;
			if (path.endsWith("/")) {
				path = path.substring(0, path.length() - 1);
				mod = true;
				continue;
			}
			return Optional.of(path);
		} while (mod);
		return Optional.empty();
	}

	public static void main(String[] args) {
		System.out.println(RootDirectoryLoader.INSTANCE.get().getAbsolutePath());
	}

}
