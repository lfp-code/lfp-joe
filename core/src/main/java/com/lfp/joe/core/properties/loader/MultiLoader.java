package com.lfp.joe.core.properties.loader;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URI;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.loaders.Loader;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.properties.Configs;

@SuppressWarnings("serial")
public class MultiLoader implements Loader {

	private static final String SCHEME = "lfpconfig";
	private static final String HOST = UUID.randomUUID().toString().replaceAll("\\W+", "");
	private static final Map<String, Optional<Class<?>>> CLASS_TYPE_FROM_NAME_CACHE = new ConcurrentHashMap<>();

	private final String instanceId = UUID.randomUUID().toString().replaceAll("\\W+", "");
	private final List<ClassTypeLoader> classTypeLoaders = Collections.synchronizedList(new ArrayList<>());

	public boolean registerClassTypeLoader(ClassTypeLoader classTypeLoader) {
		if (classTypeLoader == null)
			return false;
		classTypeLoaders.add(classTypeLoader);
		return true;
	}

	@Override
	public boolean accept(URI uri) {
		if ((uri == null) || !SCHEME.equalsIgnoreCase(uri.getScheme()) || !HOST.equalsIgnoreCase(uri.getHost()))
			return false;
		return true;
	}

	@Override
	public void load(Properties result, URI uri) throws IOException {
		Class<Config> classType = fromURI(uri);
		if (classType == null)
			return;

		loadSync(result, classType);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public String defaultSpecFor(String uriPrefix) {
		if (!uriPrefix.startsWith("classpath:"))
			return null;
		uriPrefix = uriPrefix.substring(uriPrefix.indexOf(":") + 1);
		uriPrefix = uriPrefix.replaceAll("/", ".");
		Class<?> classType = forName(uriPrefix, true);
		if ((classType == null) || !Config.class.isAssignableFrom(classType))
			return null;
		return toURI((Class) classType).toString();
	}

	@SuppressWarnings("unchecked")
	private void loadSync(Properties result, Class<Config> classType) {
		MemoizedSupplier<Set<Entry<Method, Set<String>>>> methodEntriesSupplier = MemoizedSupplier.create(() -> {
			Set<String> uniqueTracker = new HashSet<>();
			Stream<Method> publicNoArgMethods = CoreReflections.streamMethods(classType, true);
			publicNoArgMethods = publicNoArgMethods.filter(m -> !m.isDefault());
			publicNoArgMethods = publicNoArgMethods.filter(m -> m.getParameterCount() == 0);
			publicNoArgMethods = publicNoArgMethods.filter(m -> Modifier.isPublic(m.getModifiers()));
			publicNoArgMethods = publicNoArgMethods.filter(m -> !Modifier.isStatic(m.getModifiers()));
			publicNoArgMethods = publicNoArgMethods.filter(m -> uniqueTracker.add(m.getName()));
			Set<Entry<Method, Set<String>>> methodEntries = publicNoArgMethods
					.map(m -> new SimpleEntry<>(m, getNameVariations(classType, m)))
					.collect(Collectors.toCollection(() -> new LinkedHashSet<>()));
			return Collections.unmodifiableSet(methodEntries);
		});
		classTypeLoaders
				.forEach(l -> l.load(instanceId, result, classType, () -> methodEntriesSupplier.get().iterator()));
	}

	private Set<String> getNameVariations(Class<Config> classType, Method method) {
		Map<String, Boolean> nameToKeyDerived = new LinkedHashMap<>();
		Map<String, Boolean> pathSeperators;
		{
			pathSeperators = new LinkedHashMap<>();
			pathSeperators.put(".", false);
			pathSeperators.put("_", true);
		}
		List<String> keyValues = Configs.streamKeyAnnotationValues(method).collect(Collectors.toList());
		for (var ent : pathSeperators.entrySet()) {
			var seperator = ent.getKey();
			var dotReplace = ent.getValue();
			List<String> prefixes = new ArrayList<>();
			prefixes.add(classType.getName());
			prefixes.add(classType.getSimpleName());
			prefixes.add(null);
			for (var dotReplaceName : dotReplace ? List.of(false, true) : List.of(false)) {
				for (var dotReplacePrefix : dotReplace ? List.of(false, true) : List.of(false)) {
					BiFunction<String, String, String> nameFunction = (prefix, name) -> {
						if (name == null || name.isBlank())
							return null;
						if (dotReplaceName)
							name = name.replaceAll(Pattern.quote("."), seperator);
						if (prefix == null || prefix.isBlank())
							return name;
						if (dotReplacePrefix)
							prefix = prefix.replaceAll(Pattern.quote("."), seperator);
						return prefix + seperator + name;
					};
					for (var prefix : prefixes) {

						for (var keyValue : keyValues) {
							var name = nameFunction.apply(prefix, keyValue);
							if (name != null)
								nameToKeyDerived.put(name, true);

						}
						var name = nameFunction.apply(prefix, method.getName());
						if (name != null)
							nameToKeyDerived.put(name, false);
					}
				}
			}
		}
		Comparator<Entry<String, Boolean>> sorter = Comparator.comparing(ent -> {
			return ent.getValue() ? 0 : Integer.MAX_VALUE;
		});
		sorter = sorter.thenComparing(ent -> {
			return ent.getKey().length() * -1;
		});
		sorter = sorter.thenComparing(ent -> {
			var key = ent.getKey();
			var dots = key.chars().mapToObj(v -> (char) v).filter(v -> Objects.equals('.', v)).count();
			return dots * -1;
		});

		var nameStream = nameToKeyDerived.entrySet().stream().sorted(sorter).map(Entry::getKey);
		nameStream = nameStream.filter(Objects::nonNull).filter(Predicate.not(String::isBlank));
		LinkedHashSet<String> nameVariations = nameStream.collect(Collectors.toCollection(LinkedHashSet::new));
		return Collections.unmodifiableSet(nameVariations);
	}

	protected <T extends Config> URI toURI(Class<T> classType) {
		if (classType == null)
			return null;
		try {
			return URI.create(SCHEME + "://" + instanceId + "@" + HOST + "/"
					+ URLEncoder.encode(classType.getName(), StandardCharsets.UTF_8.name()));
		} catch (UnsupportedEncodingException e) {
			throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
					? java.lang.RuntimeException.class.cast(e)
					: new java.lang.RuntimeException(e);
		}
	}

	@SuppressWarnings("unchecked")
	protected <T extends Config> Class<T> fromURI(URI uri) {
		if ((uri == null) || !SCHEME.equalsIgnoreCase(uri.getScheme()) || !HOST.equalsIgnoreCase(uri.getHost()))
			return null;
		String classTypeStr = uri.getPath();
		if (classTypeStr == null || classTypeStr.isEmpty())
			return null;
		while (classTypeStr.startsWith("/"))
			classTypeStr = classTypeStr.substring(1);
		try {
			classTypeStr = URLDecoder.decode(classTypeStr, StandardCharsets.UTF_8.name());
		} catch (UnsupportedEncodingException e) {
			throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
					? java.lang.RuntimeException.class.cast(e)
					: new java.lang.RuntimeException(e);
		}
		if (classTypeStr == null || classTypeStr.isEmpty())
			return null;
		Class<?> classType = forName(classTypeStr, false);
		if (classType == null)
			return null;
		if (!Config.class.isAssignableFrom(classType))
			return null;
		return (Class<T>) classType;
	}

	private Class<?> forName(String classTypeStr, boolean storeNull) {
		Optional<Class<?>> classTypeOp = CLASS_TYPE_FROM_NAME_CACHE.computeIfAbsent(classTypeStr, nil -> {
			Optional<Class<?>> op = CoreReflections.tryForName(classTypeStr, this.getClass().getClassLoader());
			if (op.isPresent() || storeNull)
				return op;
			return null;
		});
		return classTypeOp == null || !classTypeOp.isPresent() ? null : classTypeOp.get();
	}

	public String getInstanceId() {
		return instanceId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((instanceId == null) ? 0 : instanceId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if ((obj == null) || (getClass() != obj.getClass()))
			return false;
		MultiLoader other = (MultiLoader) obj;
		if (instanceId == null) {
			if (other.instanceId != null)
				return false;
		} else if (!instanceId.equals(other.instanceId))
			return false;
		return true;
	}

	public static interface ClassTypeLoader {

		void load(String instanceId, Properties result, Class<Config> classType,
				Iterable<Entry<Method, Set<String>>> publicNoArgMethodEntries);

	}

	public static void main(String[] args) {
		EnvironmentVariableLoader val = CoreReflections
				.newInstanceUnchecked(EnvironmentVariableLoader.INSTANCE.getClass());
	}
}
