package com.lfp.joe.core.process.future;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletableFuture.AsynchronousCompletionTask;
import java.util.concurrent.RunnableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.lfp.joe.core.classpath.ImmutableMemberCache.ConstructorRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.function.Throws;

public interface AsyncRun extends RunnableFuture<Void>, AsynchronousCompletionTask {

	static enum Const {
		;

		private static final Constructor<?> CompletableFuture_AsyncRun_CONSTRUCTOR = Throws.unchecked(() -> {
			var ctStream = Stream.of(CompletableFuture.class.getDeclaredClasses())
					.filter(Runnable.class::isAssignableFrom)
					.filter(AsynchronousCompletionTask.class::isAssignableFrom);
			var ctorStream = ctStream.map(classType -> {
				var request = ConstructorRequest.of(classType, CompletableFuture.class, Runnable.class);
				return MemberCache.tryGetConstructor(request, false).orElse(null);
			});
			var ctors = ctorStream.filter(Objects::nonNull).limit(2).collect(Collectors.toList());
			if (ctors.size() != 1) {
				var message = String.format("%s %s constructor lookup failed:%s",
						CompletableFuture.class.getSimpleName(), Runnable.class.getSimpleName(), ctors);
				throw new IllegalArgumentException(message);
			}
			return ctors.get(0);
		});

		private static class AsyncRunInvocationHandler implements InvocationHandler {

			private final Object delegate;

			public AsyncRunInvocationHandler(CompletableFuture<?> completableFuture, Runnable command)
					throws InstantiationException, IllegalAccessException, IllegalArgumentException,
					InvocationTargetException {
				Objects.requireNonNull(completableFuture);
				Objects.requireNonNull(command);
				delegate = CompletableFuture_AsyncRun_CONSTRUCTOR.newInstance(completableFuture, command);
			}

			@Override
			public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
				return method.invoke(delegate, args);
			}

		}
	}

}
