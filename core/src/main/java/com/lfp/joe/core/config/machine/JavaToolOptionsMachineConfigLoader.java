package com.lfp.joe.core.config.machine;

import java.util.concurrent.Callable;
import java.util.stream.Stream;

import com.lfp.joe.core.config.JavaToolOptions;

public abstract class JavaToolOptionsMachineConfigLoader<X> extends MachineConfigLoader<X> {

	private final String key;
	private final X fallback;

	public JavaToolOptionsMachineConfigLoader(String key, X fallback) {
		super();
		this.key = key;
		this.fallback = fallback;
	}

	@Override
	protected Iterable<? extends Callable<? extends X>> loaders() {
		return () -> {
			Stream<Callable<? extends X>> loaderStream = JavaToolOptions.streamValues(key)
					.flatMap(nonNullOrBlankFlatMap()).map(value -> () -> parse(value));
			loaderStream = Stream.concat(loaderStream, Stream.of(() -> fallback));
			return loaderStream.iterator();
		};
	}

	protected abstract X parse(String value) throws Exception;

}