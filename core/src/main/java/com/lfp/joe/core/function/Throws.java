package com.lfp.joe.core.function;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.SocketTimeoutException;
import java.net.http.HttpTimeoutException;
import java.util.Arrays;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.ConstructorRequest;
import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.ImmutableMemberCache.MethodRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.process.ShutdownNotifier;

@SuppressWarnings("unchecked")
public interface Throws<F, R, T extends Throwable> {

	default F unchecked() {
		return onError((Function<T, R>) null);
	}

	default F onError(Consumer<T> errorConsumer) {
		return onError(errorConsumer == null ? null : t -> {
			errorConsumer.accept(t);
			return null;
		});
	}

	F onError(Function<T, R> errorConsumer);

	public static boolean isHalt(Throwable throwable) {
		if (throwable == null)
			return false;
		if (CancellationException.class.isInstance(throwable))
			return true;
		if (InterruptedException.class.isInstance(throwable))
			return true;
		if (ShutdownNotifier.INSTANCE.isShuttingDown())
			return true;
		return false;
	}

	public static boolean isTimeout(Throwable throwable) {
		if (throwable == null)
			return false;
		// common jdk timeouts
		if (TimeoutException.class.isInstance(throwable))
			return true;
		if (SocketTimeoutException.class.isInstance(throwable))
			return true;
		if (HttpTimeoutException.class.isInstance(throwable))
			return true;
		// check class name
		var simpleName = throwable.getClass().getSimpleName();
		for (var prefixAppend : Arrays.asList(null, "d")) {
			var search = Arrays.asList("time", prefixAppend, "out")
					.stream()
					.filter(Objects::nonNull)
					.collect(Collectors.joining());
			if (simpleName.toLowerCase().contains(search.toLowerCase()))
				return true;
		}
		return isTimeout(throwable.getCause());
	}

	@SafeVarargs
	public static Throwable unwrap(Throwable error, Class<? extends Throwable>... classTypes) {
		Objects.requireNonNull(error);
		var errorIter = streamCauses(error).iterator();
		while (true) {
			var t = errorIter.next();
			if (!errorIter.hasNext())
				return t;
			boolean unwrap = Stream.ofNullable(classTypes)
					.flatMap(Stream::of)
					.anyMatch(v -> CoreReflections.isInstance(v, t));
			if (!unwrap)
				return t;
		}
	}

	public static Stream<Throwable> streamCauses(Throwable error) {
		if (error == null)
			return Stream.empty();
		var stream = Stream.iterate(error, Objects::nonNull, prevError -> {
			var cause = prevError.getCause();
			if (cause == null)
				return null;
			return cause;
		});
		return stream;
	}

	public static <X> Stream<X> streamCauses(Throwable error, Class<? super X> classType) {
		if (classType == null)
			return Stream.empty();
		return streamCauses(error).flatMap(v -> CoreReflections.tryCast(v, classType).stream()).map(v -> (X) v);
	}

	public static RuntimeException unchecked(Throwable... errors) {
		Throwable error = combine(errors);
		if (error == null)
			return new RuntimeException();
		if (error instanceof RuntimeException)
			return (RuntimeException) error;
		return new RuntimeException(error);
	}

	public static Throwable combine(Throwable... errors) {
		if (errors == null || errors.length == 0)
			return null;
		BiPredicate<Throwable, Throwable> shouldSuppress = (e, suppress) -> {
			if (e == null || suppress == null)
				return false;
			if (e == suppress)
				return false;
			if (e.getCause() == suppress)
				return false;
			if (Stream.ofNullable(e.getSuppressed()).flatMap(Stream::of).anyMatch(suppress::equals))
				return false;
			return true;
		};
		Throwable result = null;
		for (var error : errors) {
			if (result == null) {
				result = error;
				continue;
			}
			if (!shouldSuppress.test(result, error))
				continue;
			result.addSuppressed(error);
		}
		return result;
	}

	public static boolean setCause(Throwable throwable, Throwable cause) {
		if (throwable == cause)
			return false;
		if (throwable.getCause() == cause)
			return false;
		var detailMessageRequest = FieldRequest.of(Throwable.class, String.class, "detailMessage");
		var defailMessage = MemberCache.getFieldValue(detailMessageRequest, throwable);
		if (defailMessage == null)
			MemberCache.setFieldValue(detailMessageRequest, throwable, cause.toString());
		var causeRequest = FieldRequest.of(Throwable.class, Throwable.class, "cause");
		MemberCache.setFieldValue(causeRequest, throwable, cause);
		return true;
	}

	public static void unchecked(ThrowingRunnable<?> runnable) {
		runnable.unchecked().run();
	}

	public static <X> X unchecked(ThrowingSupplier<X, ?> supplier) {
		return supplier.unchecked().get();
	}

	public static <X> void unchecked(X input, ThrowingConsumer<X, ?> consumer) {
		consumer.unchecked().accept(input);
	}

	public static <X, Y> void unchecked(X input0, Y input1, ThrowingBiConsumer<X, Y, ?> biConsumer) {
		biConsumer.unchecked().accept(input0, input1);
	}

	public static <X, Y> Y unchecked(X input, ThrowingFunction<X, Y, ?> function) {
		return function.unchecked().apply(input);
	}

	public static <X, Z, Y> Y unchecked(X input0, Z input1, ThrowingBiFunction<X, Z, Y, ?> biFunction) {
		return biFunction.unchecked().apply(input0, input1);
	}

	public static <T extends Throwable> void onError(boolean invokeAll, Consumer<T> errorConsumer,
			ThrowingRunnable<T>... runnables) {
		onErrorInternal(errorConsumer, invokeAll, runnables, v -> {
			v.run();
			return null;
		});
	}

	public static <X, T extends Throwable> X onError(Consumer<T> errorConsumer, ThrowingSupplier<X, T>... suppliers) {
		return onError(errorConsumer, false, suppliers);
	}

	public static <X, T extends Throwable> X onError(Consumer<T> errorConsumer, boolean invokeAll,
			ThrowingSupplier<X, T>... suppliers) {
		return onErrorInternal(errorConsumer, invokeAll, suppliers, ThrowingSupplier::get);
	}

	public static <X, T extends Throwable> void onError(Consumer<T> errorConsumer, X input, boolean invokeAll,
			ThrowingConsumer<X, T>... consumers) {
		onErrorInternal(errorConsumer, invokeAll, consumers, v -> {
			v.accept(input);
			return null;
		});
	}

	public static <X, Y, T extends Throwable> void onError(Consumer<T> errorConsumer, X input0, Y input1,
			ThrowingBiConsumer<X, Y, T>... biConsumers) {
		onError(errorConsumer, input0, input1, false, biConsumers);
	}

	public static <X, Y, T extends Throwable> void onError(Consumer<T> errorConsumer, X input0, Y input1,
			boolean invokeAll, ThrowingBiConsumer<X, Y, T>... biConsumers) {
		onErrorInternal(errorConsumer, invokeAll, biConsumers, v -> {
			v.accept(input0, input1);
			return null;
		});
	}

	public static <X, Y, T extends Throwable> Y onError(Consumer<T> errorConsumer, X input,
			ThrowingFunction<X, Y, T>... functions) {
		return onError(errorConsumer, input, false, functions);
	}

	public static <X, Y, T extends Throwable> Y onError(Consumer<T> errorConsumer, X input, boolean invokeAll,
			ThrowingFunction<X, Y, T>... functions) {
		return onErrorInternal(errorConsumer, invokeAll, functions, v -> {
			return v.apply(input);
		});
	}

	public static <X, Z, Y, T extends Throwable> Y onError(Consumer<T> errorConsumer, X input0, Z input1,
			ThrowingBiFunction<X, Z, Y, T>... biFunctions) {
		return onError(errorConsumer, input0, input1, false, biFunctions);
	}

	public static <X, Z, Y, T extends Throwable> Y onError(Consumer<T> errorConsumer, X input0, Z input1,
			boolean invokeAll, ThrowingBiFunction<X, Z, Y, T>... biFunctions) {
		return onErrorInternal(errorConsumer, invokeAll, biFunctions, v -> {
			return v.apply(input0, input1);
		});
	}

	private static <X, I, T extends Throwable> X onErrorInternal(Consumer<T> errorConsumer, boolean invokeAll,
			I[] inputs, ThrowingFunction<I, X, T> mapper) {
		var inputIter = Stream.ofNullable(inputs).flatMap(Stream::of).filter(Objects::nonNull).iterator();
		while (inputIter.hasNext())
			try {
				var result = mapper.apply(inputIter.next());
				if (!invokeAll)
					return result;
			} catch (Throwable t) {
				if (errorConsumer == null)
					throw unchecked(t);
				errorConsumer.accept((T) t);
			}
		return null;
	}

	public static Optional<Nada> attempt(ThrowingRunnable<?> runnable) {
		if (runnable != null)
			try {
				runnable.run();
				return Optional.of(Nada.get());
			} catch (Throwable t) {
				// suppress
			}
		return Optional.empty();
	}

	public static <X> Optional<X> attempt(ThrowingSupplier<X, ?> supplier) {
		if (supplier != null)
			try {
				return Optional.ofNullable(supplier.get());
			} catch (Throwable t) {
				// suppress
			}
		return Optional.empty();
	}

	public static <X> Optional<Nada> attempt(X input, ThrowingConsumer<X, ?> consumer) {
		if (consumer != null)
			try {
				consumer.accept(input);
				return Optional.of(Nada.get());
			} catch (Throwable t) {
				// suppress
			}
		return Optional.empty();
	}

	public static <X, Z> Optional<Nada> attempt(X input0, Z input1, ThrowingBiConsumer<X, Z, ?> biConsumer) {
		if (biConsumer != null)
			try {
				biConsumer.accept(input0, input1);
				return Optional.of(Nada.get());
			} catch (Throwable t) {
				// suppress
			}
		return Optional.empty();
	}

	public static <X, Y> Optional<Y> attempt(X input, ThrowingFunction<X, Y, ?> function) {
		if (function != null)
			try {
				return Optional.ofNullable(function.apply(input));
			} catch (Throwable t) {
				// suppress
			}
		return Optional.empty();
	}

	public static <X, Z, Y> Optional<Y> attempt(X input0, Z input1, ThrowingBiFunction<X, Z, Y, ?> biFunction) {
		if (biFunction != null)
			try {
				return Optional.ofNullable(biFunction.apply(input0, input1));
			} catch (Throwable t) {
				// suppress
			}
		return Optional.empty();
	}

	public static Runnable runnableUnchecked(ThrowingRunnable<?> runnable) {
		return runnable.unchecked();
	}

	public static <X> Supplier<X> supplierUnchecked(ThrowingSupplier<X, ?> supplier) {
		return supplier.unchecked();
	}

	public static <X> Consumer<X> consumerUnchecked(ThrowingConsumer<X, ?> consumer) {
		return consumer.unchecked();
	}

	public static <X, Y> BiConsumer<X, Y> biConsumerUnchecked(ThrowingBiConsumer<X, Y, ?> biConsumer) {
		return biConsumer.unchecked();
	}

	public static <X, Y> Function<X, Y> functionUnchecked(ThrowingFunction<X, Y, ?> function) {
		return function.unchecked();
	}

	public static <X, Z, Y> BiFunction<X, Z, Y> biFunctionUnchecked(ThrowingBiFunction<X, Z, Y, ?> biFunction) {
		return biFunction.unchecked();
	}

	public static String stackTraceToString(StackTraceElement[] stackTraceElements, Throwable[] suppressedExceptions,
			Throwable cause) {
		try (var baos = new ByteArrayOutputStream()) {
			stackTraceWrite(baos, stackTraceElements, suppressedExceptions, cause);
			return new String(baos.toByteArray(), MachineConfig.getDefaultCharset());
		} catch (IOException e) {
			throw Throws.unchecked(e);
		}
	}

	public static void stackTraceWrite(OutputStream outputStream, StackTraceElement[] stackTraceElements,
			Throwable[] suppressedExceptions, Throwable cause) {
		Objects.requireNonNull(outputStream);
		PrintStream printStream = CoreReflections.tryCast(outputStream, PrintStream.class)
				.orElseGet(() -> new PrintStream(outputStream, true, MachineConfig.getDefaultCharset()));
		try {
			Object wrappedPrintStream;
			if (Const.Throwable_WrappedPrintStream_CLASS_TYPE.isInstance(printStream))
				wrappedPrintStream = printStream;
			else
				wrappedPrintStream = MemberCache.newInstance(
						ConstructorRequest.of(Const.Throwable_WrappedPrintStream_CLASS_TYPE, PrintStream.class),
						printStream);
			BiFunction<Object[], IntFunction<Object[]>, Object[]> nonNullArray = (arr, generator) -> {
				if (arr != null) {
					boolean nonNull = Stream.of(arr).allMatch(Objects::nonNull);
					if (nonNull)
						return arr;
				}
				return Stream.ofNullable(arr).flatMap(Stream::of).filter(Objects::nonNull).toArray(generator);
			};
			stackTraceElements = (StackTraceElement[]) nonNullArray.apply(stackTraceElements, StackTraceElement[]::new);
			suppressedExceptions = (Throwable[]) nonNullArray.apply(suppressedExceptions, Throwable[]::new);
			for (var traceElement : stackTraceElements)
				printStream.println("\tat " + traceElement);
			Set<Throwable> dejaVu = null;
			for (var throwable : suppressedExceptions)
				dejaVu = printEnclosedStackTrace(throwable, wrappedPrintStream, stackTraceElements,
						Const.SUPPRESSED_CAPTION, "\t", dejaVu);
			if (cause != null)
				dejaVu = printEnclosedStackTrace(cause, wrappedPrintStream, stackTraceElements, Const.CAUSE_CAPTION, "",
						dejaVu);
		} finally {
			printStream.flush();
		}
	}

	private static Set<Throwable> printEnclosedStackTrace(Throwable throwable, /* PrintStreamOrWriter */ Object s,
			StackTraceElement[] enclosingTrace, String caption, String prefix, Set<Throwable> dejaVu) {
		Objects.requireNonNull(throwable);
		if (dejaVu == null)
			dejaVu = Collections.newSetFromMap(new IdentityHashMap<>());
		var req = MethodRequest.of(Throwable.class, null, "printEnclosedStackTrace",
				Const.Throwable_WrappedPrintStream_CLASS_TYPE, StackTraceElement[].class, String.class, String.class,
				Set.class);
		MemberCache.invokeMethod(req, throwable, s, enclosingTrace, caption, prefix, dejaVu);
		return dejaVu;
	}

	private static <X, T extends Throwable> X onError(Function<T, X> errorFunction, Throwable error) {
		if (errorFunction == null)
			errorFunction = t -> {
				throw unchecked(t);
			};
		try {
			return errorFunction.apply((T) error);
		} catch (ClassCastException e) {
			org.slf4j.LoggerFactory.getLogger(Throws.class).error("error consume failed", error);
			throw e;
		}

	}

	public static interface ThrowingRunnable<T extends Throwable> extends Throws<Runnable, Void, T> {

		@Override
		default Runnable onError(Function<T, Void> errorConsumer) {
			return () -> {
				try {
					this.run();
				} catch (Throwable t) {
					Throws.onError(errorConsumer, t);
				}
			};
		}

		default ThrowingSupplier<Void, T> asSupplier() {
			return () -> {
				this.run();
				return null;
			};
		}

		public void run() throws T;
	}

	public static interface ThrowingSupplier<X, T extends Throwable> extends Throws<Supplier<X>, X, T> {

		@Override
		default Supplier<X> onError(Function<T, X> errorConsumer) {
			return () -> {
				try {
					return this.get();
				} catch (Throwable t) {
					return Throws.onError(errorConsumer, t);
				}
			};
		}

		default <U> ThrowingFunction<U, X, T> asFunction() {
			return nil -> {
				return this.get();
			};
		}

		public X get() throws T;

	}

	public static interface ThrowingConsumer<X, T extends Throwable> extends Throws<Consumer<X>, Void, T> {

		@Override
		default Consumer<X> onError(Function<T, Void> errorConsumer) {
			return i -> {
				try {
					accept(i);
				} catch (Throwable t) {
					Throws.onError(errorConsumer, t);
				}
			};
		}

		public void accept(X value) throws T;

	}

	public static interface ThrowingFunction<X, Y, T extends Throwable> extends Throws<Function<X, Y>, Y, T> {

		@Override
		default Function<X, Y> onError(Function<T, Y> errorConsumer) {
			return i -> {
				try {
					return this.apply(i);
				} catch (Throwable t) {
					return Throws.onError(errorConsumer, t);
				}
			};
		}

		public Y apply(X input) throws T;

	}

	public static interface ThrowingBiConsumer<X, Y, T extends Throwable> extends Throws<BiConsumer<X, Y>, Void, T> {

		@Override
		default BiConsumer<X, Y> onError(Function<T, Void> errorConsumer) {
			return (i, o) -> {
				try {
					this.accept(i, o);
				} catch (Throwable t) {
					Throws.onError(errorConsumer, t);
				}
			};
		}

		public void accept(X value1, Y value2) throws T;

	}

	public static interface ThrowingBiFunction<X, Z, Y, T extends Throwable> extends Throws<BiFunction<X, Z, Y>, Y, T> {

		@Override
		default BiFunction<X, Z, Y> onError(Function<T, Y> errorConsumer) {
			return (i, o) -> {
				try {
					return this.apply(i, o);
				} catch (Throwable t) {
					return Throws.onError(errorConsumer, t);
				}
			};
		}

		public Y apply(X input1, Z input2) throws T;

	}

	public static interface ModifiableCause {

		default boolean setCause(Throwable cause) {
			if (!(this instanceof Throwable))
				throw new IllegalStateException("instance is not:" + Throwable.class.getName());
			return Throws.setCause((Throwable) this, cause);
		}

	}

	static enum Const {
		;

		private static final String SUPPRESSED_CAPTION = MemberCache
				.getFieldValue(FieldRequest.of(Throwable.class, String.class, "SUPPRESSED_CAPTION"), null);
		private static final String CAUSE_CAPTION = MemberCache
				.getFieldValue(FieldRequest.of(Throwable.class, String.class, "CAUSE_CAPTION"), null);
		private static final Class<?> Throwable_WrappedPrintStream_CLASS_TYPE = Stream
				.of(Throwable.class.getDeclaredClasses())
				.filter(v -> v.getSimpleName().equals("WrappedPrintStream"))
				.findFirst()
				.get();

	}

	public static void main(String[] args) {
		var exception = new Exception(new IllegalArgumentException());
		exception.addSuppressed(new IOException());
		exception.printStackTrace();
		var str = Throws.stackTraceToString(exception.getStackTrace(), null, exception.getCause());
		System.err.println(str);

		var e = new ExecutionException(new RuntimeException(new IOException()));
		System.out.println(unwrap(e, Throwable.class).getClass());
		Throws.setCause(e, new InterruptedException());
		e.printStackTrace();
	}

}
