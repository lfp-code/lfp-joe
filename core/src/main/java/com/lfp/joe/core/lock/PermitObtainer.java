package com.lfp.joe.core.lock;

import java.util.Optional;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.lfp.joe.core.function.Registration;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.core.process.future.CSFutureTask;
import com.lfp.joe.core.process.future.Completion;

public interface PermitObtainer extends AutoCloseable {

	default Registration obtain() throws InterruptedException {
		return obtain(1);
	}

	default Optional<Registration> tryObtain(long timeout, TimeUnit unit) throws InterruptedException {
		return tryObtain(1, timeout, unit);
	}

	Registration obtain(int permits) throws InterruptedException;

	Optional<Registration> tryObtain(int permits, long timeout, TimeUnit unit) throws InterruptedException;

	@Override
	void close();

	public static PermitObtainer unlimited() {
		return Const.UNLIMITED;
	}

	public static PermitObtainer create(int maxPermits) {
		return create(maxPermits, false, false);
	}

	public static PermitObtainer create(int maxPermits, boolean fair, boolean interruptOnClose) {
		return new PermitObtainer.Impl(maxPermits, fair, interruptOnClose);
	}

	@SuppressWarnings("serial")
	public static class Impl extends Semaphore implements PermitObtainer {

		private final AtomicInteger maxPermits;
		private final ThreadRegistry threadRegistry;

		public Impl(int maxPermits, boolean fair, boolean interruptOnClose) {
			super(validateMaxPermits(maxPermits), fair);
			this.maxPermits = new AtomicInteger(maxPermits);
			if (interruptOnClose)
				threadRegistry = new ThreadRegistry();
			else
				threadRegistry = null;
		}

		public void addPermits(int permits) {
			setMaxPermits(this.getMaxPermits() + permits);
		}

		public int getMaxPermits() {
			return maxPermits.get();
		}

		public void setMaxPermits(int maxPermits) {
			validateMaxPermits(maxPermits);
			var previousMaxPermits = this.maxPermits.getAndSet(maxPermits);
			int delta = maxPermits - previousMaxPermits;
			if (delta == 0)
				return;
			if (delta > 0)
				// new max is higher, so release that many permits
				this.release(delta);
			else
				// delta < 0.
				// reducePermits needs a positive #, though.
				this.reducePermits(-1 * delta);
		}

		@Override
		public Registration obtain(int permits) throws InterruptedException {
			if (threadRegistry == null)
				return obtainInternal(permits);
			threadRegistry.validateNotClosed();
			if (tryAcquire(permits))
				return () -> release(permits);
			try (var r = threadRegistry.register()) {
				super.acquire(permits);
				return () -> release(permits);
			}
		}

		protected Registration obtainInternal(int permits) throws InterruptedException {
			super.acquire(permits);
			return () -> release(permits);
		}

		@Override
		public Optional<Registration> tryObtain(int permits, long timeout, TimeUnit unit) throws InterruptedException {
			if (threadRegistry == null)
				return tryObtainInternal(permits, timeout, unit);
			threadRegistry.validateNotClosed();
			if (tryAcquire(permits))
				return Optional.of(() -> release(permits));
			try (var r = threadRegistry.register()) {
				return tryObtainInternal(permits, timeout, unit);
			}
		}

		protected Optional<Registration> tryObtainInternal(int permits, long timeout, TimeUnit unit)
				throws InterruptedException {
			if (!super.tryAcquire(permits, timeout, unit))
				return Optional.empty();
			return Optional.of(() -> release(permits));
		}

		@Override
		public void close() {
			Optional.ofNullable(threadRegistry).ifPresent(Registration::close);
		}

		@Override
		public String toString() {
			return String.format("%s[%s[MaxPermits = %s]]", this.getClass().getSimpleName(), super.toString(),
					this.maxPermits.get());
		}

		private static int validateMaxPermits(int maxPermits) {
			if (maxPermits <= 0)
				throw new IllegalArgumentException("invalid maxPermits:" + maxPermits);
			return maxPermits;
		}

		/*
		 * semaphore delegates
		 */

		@Override
		public void acquire() throws InterruptedException {
			obtain();
		}

		@Override
		public boolean tryAcquire(long timeout, TimeUnit unit) throws InterruptedException {
			return tryObtain(timeout, unit).isPresent();
		}

		@Override
		public void acquire(int permits) throws InterruptedException {
			obtain(permits);
		}

		@Override
		public boolean tryAcquire(int permits, long timeout, TimeUnit unit) throws InterruptedException {
			return tryObtain(permits, timeout, unit).isPresent();
		}

	}

	static enum Const {
		;

		private static final PermitObtainer UNLIMITED = new PermitObtainer() {

			@Override
			public Registration obtain(int permits) throws InterruptedException {
				return Registration.complete();
			}

			@Override
			public Optional<Registration> tryObtain(int permits, long timeout, TimeUnit unit)
					throws InterruptedException {
				return Optional.of(Registration.complete());
			}

			@Override
			public void close() {}
		};
	}

	public static void main(String[] args) throws InterruptedException {
		var permitObtainer = PermitObtainer.create(2, false, true);
		IntStream.range(0, 50).boxed().map(i -> {
			var task = new CSFutureTask<Void>(() -> {
				System.out.println("requesting:" + i);
				try (var r = permitObtainer.obtain();) {
					System.out.println("obtained:" + i);
					Thread.sleep(1_000);
				}
				return null;
			});
			task.whenComplete((v, t) -> {
				var completion = Completion.of(v, t);
				System.out.println(completion + " - " + completion.isInterruption());
			});
			CoreTasks.executor().execute(task);
			return task;
		}).collect(Collectors.toList());
		Thread.sleep(4_000);
		permitObtainer.close();
		permitObtainer.obtain();
	}

}