package com.lfp.joe.core.properties.loader;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.aeonbits.owner.Config;

public enum EnvironmentVariableLoader implements MultiLoader.ClassTypeLoader {
	INSTANCE;

	@Override
	public void load(String instanceId, Properties result, Class<Config> classType,
			Iterable<Entry<Method, Set<String>>> publicNoArgMethodEntries) {
		Map<String, String> map = System.getenv();
		if (map == null)
			return;
		Loaders.putAll(result, publicNoArgMethodEntries, map);
	}

}
