package com.lfp.joe.core.function;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.time.Duration;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.immutables.value.Value;

import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.ImmutableFileLoader.FileLoaderRequest;
import com.lfp.joe.core.function.Muto.MutoBoolean;
import com.lfp.joe.core.function.Throws.ThrowingBiFunction;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.io.FileExt;
import com.lfp.joe.core.io.InputStreamExt;
import com.lfp.joe.core.io.OutputStreamExt;
import com.lfp.joe.core.lock.FileLocks;
import com.lfp.joe.core.process.executor.CoreTasks;

@ValueLFP.Style
@Value.Enclosing
public class FileLoader {

	private static final Map<String, FutureTask<?>> REFRESH_FUTURES = new ConcurrentHashMap<>();

	public static <X> Future<X> get(FileLoaderRequest<X> fileLoaderRequest) {
		Objects.requireNonNull(fileLoaderRequest);
		try {
			return getInternal(fileLoaderRequest);
		} catch (Throwable t) {
			return CompletableFuture.failedFuture(t);
		}
	}

	@SuppressWarnings("unchecked")
	private static <X> Future<X> getInternal(FileLoaderRequest<X> fileLoaderRequest) throws IOException {
		fileLoaderRequest.file().getParentFile().mkdirs();
		var refreshSync = MutoBoolean.create();
		var refreshASync = MutoBoolean.create();
		X result = FileLocks.read(fileLoaderRequest.file(), (fc, is) -> {
			if (shouldRefreshSync(fileLoaderRequest, fc)) {
				refreshSync.set(true);
				return null;
			}
			if (shouldRefreshASync(fileLoaderRequest, fc))
				refreshASync.set(true);
			return read(fileLoaderRequest, is);
		});
		if (refreshSync.get() || refreshASync.get()) {
			var loaded = MutoBoolean.create();
			var futureTask = REFRESH_FUTURES.computeIfAbsent(fileLoaderRequest.file().getUniquePath(), nil -> {
				loaded.set(true);
				return refreshTask(fileLoaderRequest);
			});
			if (loaded.get()) {
				try {
					if (refreshSync.get())
						futureTask.run();
					else
						fileLoaderRequest.asyncExecutor().execute(futureTask);
				} catch (Throwable t) {
					// jic something gets interrupted
					removeRefreshTask(fileLoaderRequest, futureTask);
					throw t;
				}
			}
			if (refreshSync.get())
				return (Future<X>) futureTask;
		}
		return CompletableFuture.completedFuture(result);

	}

	private static <X> X read(FileLoaderRequest<X> fileLoaderRequest, InputStream inputStream) throws IOException {
		return fileLoaderRequest.reader().apply(InputStreamExt.from(inputStream));
	}

	private static <X> FutureTask<X> refreshTask(FileLoaderRequest<X> fileLoaderRequest) {
		Callable<X> task = () -> {
			var result = fileLoaderRequest.loader().call();
			return FileLocks.write(fileLoaderRequest.file(), (fc, os) -> {
				return fileLoaderRequest.writer().apply(result, OutputStreamExt.from(os));
			});
		};
		return new FutureTask<X>(task) {

			@Override
			protected void done() {
				removeRefreshTask(fileLoaderRequest, this);
				super.done();
			}

		};
	}

	private static void removeRefreshTask(FileLoaderRequest<?> fileLoaderRequest, FutureTask<?> futureTask) {
		Objects.requireNonNull(fileLoaderRequest);
		if (futureTask == null)
			return;
		REFRESH_FUTURES.computeIfPresent(fileLoaderRequest.file().getUniquePath(), (nil, f) -> {
			if (futureTask == f)
				return null;
			return f;
		});

	}

	private static boolean shouldRefreshSync(FileLoaderRequest<?> fileLoaderRequest, FileChannel fileChannel)
			throws IOException {
		return shouldRefresh(fileLoaderRequest, fileChannel, fileLoaderRequest.refreshInterval());
	}

	private static boolean shouldRefreshASync(FileLoaderRequest<?> fileLoaderRequest, FileChannel fileChannel)
			throws IOException {
		return shouldRefresh(fileLoaderRequest, fileChannel, fileLoaderRequest.refreshAsyncInterval());
	}

	private static boolean shouldRefresh(FileLoaderRequest<?> fileLoaderRequest, FileChannel fileChannel,
			Optional<Duration> intervalOp) throws IOException {
		if (fileChannel.size() <= 0)
			return true;
		if (intervalOp == null || intervalOp.isEmpty() || Durations.max().equals(intervalOp.get()))
			return false;
		return fileLoaderRequest.file().lastModifiedElapsed(intervalOp.get());
	}

	@Value.Immutable
	public static abstract class FileLoaderRequestDef<X> {

		public abstract FileExt file();

		public abstract Optional<Duration> refreshInterval();

		public abstract Optional<Duration> refreshAsyncInterval();

		@Value.Default
		public Executor asyncExecutor() {
			return CoreTasks.executor();
		}

		public abstract Callable<X> loader();

		public abstract ThrowingFunction<InputStreamExt, X, IOException> reader();

		public abstract ThrowingBiFunction<X, OutputStreamExt, X, IOException> writer();

		public Future<X> get() {
			return FileLoader.get((FileLoaderRequest<X>) this);
		}

		@Value.Check
		void validate() {
			if (file().isDirectory())
				throw new IllegalArgumentException("requested file is directory:" + file().getAbsolutePath());
		}

		public static <U> FileLoaderRequest.Builder<Collection<U>> builder(Function<U, String> toString,
				Function<String, U> fromString) {
			Objects.requireNonNull(toString);
			Objects.requireNonNull(fromString);
			var blder = FileLoaderRequest.<Collection<U>>builder();
			blder.reader(is -> {
				try (var reader = is.reader()) {
					return reader.lines().skip(1).map(fromString).collect(Collectors.toList());
				}
			});
			blder.writer((coll, os) -> {
				Iterable<String> lineIterable = () -> coll.stream().map(toString).iterator();
				try (var writer = os.writer()) {
					writer.newLine();
					writer.lines(lineIterable);
				}
				return coll;
			});
			return blder;
		}
	}

	public static void main(String[] args) {
		var file = new File("temp/" + UUID.randomUUID().toString());
		System.out.println(file.isFile());
		System.out.println(file.isDirectory());
	}

}
