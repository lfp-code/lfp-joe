package com.lfp.joe.core.properties.converter;

import java.lang.reflect.Method;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import org.aeonbits.owner.Converter;

public class EntryListConverter implements Converter<List<Entry<String, String>>> {

	@Override
	public List<Entry<String, String>> convert(Method method, String input) {
		if (input == null || input.isEmpty())
			return Collections.emptyList();
		List<Entry<String, String>> result = new ArrayList<>();
		String[] chunks = input.split(Pattern.quote(","));
		String key = null;
		var indexIter = IntStream.range(0, chunks.length).iterator();
		while (indexIter.hasNext()) {
			int i = indexIter.nextInt();
			if (i % 2 == 0) {
				key = chunks[i];
				if (!indexIter.hasNext())
					result.add(new SimpleEntry<>(key, null));
			} else
				result.add(new SimpleEntry<>(key, chunks[i]));
		}
		return Collections.unmodifiableList(result);
	}

}
