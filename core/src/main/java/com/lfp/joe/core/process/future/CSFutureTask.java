package com.lfp.joe.core.process.future;

import java.lang.reflect.Constructor;
import java.time.Duration;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RunnableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.lfp.joe.core.classpath.ImmutableMemberCache.ConstructorRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.process.executor.CoreTasks;

@SuppressWarnings("unchecked")
public class CSFutureTask<X> extends CSFuture.Completable<X> implements RunnableFuture<X> {

	@SuppressWarnings("rawtypes")
	private static final Callable NO_OP_TASK = () -> null;
	private static final Constructor<? extends Runnable> CompletableFuture_RUNNABLE_CONSTRUCTOR = (Constructor<? extends Runnable>) Throws
			.unchecked(() -> {
				var ctStream = Stream.of(CompletableFuture.class.getDeclaredClasses())
						.filter(Runnable.class::isAssignableFrom)
						.filter(AsynchronousCompletionTask.class::isAssignableFrom);
				var ctorStream = ctStream.map(classType -> {
					var request = ConstructorRequest.of(classType, CompletableFuture.class, Runnable.class);
					return MemberCache.tryGetConstructor(request, false).orElse(null);
				});
				var ctors = ctorStream.filter(Objects::nonNull).limit(2).collect(Collectors.toList());
				if (ctors.size() != 1) {
					var message = String.format("%s %s constructor lookup failed:%s",
							CompletableFuture.class.getSimpleName(), Runnable.class.getSimpleName(), ctors);
					throw new IllegalArgumentException(message);
				}
				return ctors.get(0);
			});
	private Runnable task;

	public CSFutureTask() {
		this(NO_OP_TASK);
	}

	public CSFutureTask(Runnable runnable) {
		this(runnable, null);
	}

	public CSFutureTask(Runnable runnable, X result) {
		this(Executors.callable(runnable, null));
	}

	public CSFutureTask(Callable<X> callable) {
		if (NO_OP_TASK != callable) {
			var futureTask = new FutureTask<X>(callable) {

				@Override
				protected void set(X v) {
					super.set(v);
				}

				@Override
				protected void setException(Throwable t) {
					super.setException(t);
				}

				@Override
				protected void done() {
					if (CSFutureTask.this.isDone())
						return;
					var completion = com.lfp.joe.core.process.future.Completion.getNow(this);
					completion.complete(CSFutureTask.this::complete, CSFutureTask.this::completeExceptionally);
				}
			};
			this.whenComplete((v, t) -> {
				if (futureTask.isDone())
					return;
				var completion = com.lfp.joe.core.process.future.Completion.of(v, t);
				completion.complete(futureTask::set, futureTask::setException,
						(interrupt, nil) -> futureTask.cancel(interrupt));
			});
			this.task = futureTask;
		}

	}

	@Override
	public void run() {
		Optional.ofNullable(task).ifPresentOrElse(r -> {
			task = null;
			r.run();
		}, () -> {
			complete(null);
		});

	}

	public static void main(String[] args) throws InterruptedException {
		System.out.println(CSFutureTask.CompletableFuture_RUNNABLE_CONSTRUCTOR);
		var ft = new CSFutureTask<Integer>(() -> {
			try {
				System.out.println("running:" + Thread.currentThread());
				Thread.sleep(20_000);
				return 0;
			} catch (Throwable t) {
				System.err.println(t.getMessage());
				throw t;
			} finally {
				System.out.println("thread done");
			}
		});
		var reg = ft.whenComplete((v, t) -> {
			System.out.println("listener 1");
		});
		ft.whenComplete((v, t) -> {
			System.out.println("listener 2:" + ft.isCancelled());
		});
		ft.whenComplete((v, t) -> {
			System.out.println("listener 3:" + ft.isCancelled());
		});
		CoreTasks.executor().execute(ft);
		reg.cancel(true);
		CoreTasks.delayedExecutor(Duration.ofSeconds(1)).execute(() -> {
			// System.out.println("thread: " + ft.cancel(true));
			System.out.println(ft.isCancelled());
		});
		ft.whenComplete((v, t) -> {
			System.out.println("yooo");
		});
		Thread.sleep(2_000);
		var next = ft.withOriginCancellation(true).thenCompose(nil -> {
			var ft2 = new CSFutureTask<>(() -> System.out.println("ok"));
			CoreTasks.delayedExecutor(Duration.ofSeconds(2)).execute(ft2);
			return ft2;
		});
		next.whenComplete((v, t) -> {
			System.out.println("next done:" + next.isCancelled());
		});
		// next.cancel(true);
		Thread.sleep(3_000);
		// fThread.sleep(5_000);
		// ft.complete(1);
		System.out.println("main: " + ft.cancel(true));
		System.out.println(ft.isCancelled());
		ft.whenComplete((v, t) -> {
			System.out.println("post cancel 0");
		});
		var registrationPostCancel = ft.whenComplete((v, t) -> {
			System.out.println("post cancel 1");
		});
		System.out.println(registrationPostCancel);
		Thread.currentThread().join();
	}

}
