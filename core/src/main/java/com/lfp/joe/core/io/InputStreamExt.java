package com.lfp.joe.core.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

public class InputStreamExt extends InputStream {

	public static InputStreamExt from(InputStream inputStream) {
		if (inputStream instanceof InputStreamExt)
			return (InputStreamExt) inputStream;
		return new InputStreamExt(() -> inputStream, true);
	}

	private final ThrowingSupplier<InputStream, IOException> inputStreamSupplier;
	private final boolean disableNullInputStreamOnClose;
	private boolean closeDisabled = false;
	private boolean closed;
	private InputStream _delegateInputStream;

	public InputStreamExt(ThrowingSupplier<InputStream, IOException> inputStreamSupplier) {
		this(inputStreamSupplier, false);
	}

	protected InputStreamExt(ThrowingSupplier<InputStream, IOException> inputStreamSupplier,
			boolean disableNullInputStreamOnClose) {
		super();
		this.inputStreamSupplier = inputStreamSupplier;
		this.disableNullInputStreamOnClose = disableNullInputStreamOnClose;
	}

	protected InputStream getDelegateInputStream() throws IOException {
		if (_delegateInputStream == null)
			synchronized (this) {
				if (_delegateInputStream == null) {
					var delegate = inputStreamSupplier == null ? null : inputStreamSupplier.get();
					if (delegate == null)
						delegate = InputStream.nullInputStream();
					_delegateInputStream = delegate;
				}
			}
		return _delegateInputStream;
	}

	@Override
	public void close() throws IOException {
		if (isCloseDisabled())
			return;
		this.closed = true;
		if (!disableNullInputStreamOnClose && _delegateInputStream == null)
			synchronized (this) {
				if (!disableNullInputStreamOnClose && _delegateInputStream == null)
					_delegateInputStream = InputStream.nullInputStream();
			}
		InputStream inputStream;
		if ((inputStream = _delegateInputStream) != null)
			inputStream.close();
	}

	public boolean isClosed() {
		return closed;
	}

	public boolean isCloseDisabled() {
		return closeDisabled;
	}

	public void setCloseDisabled(boolean closeDisabled) {
		this.closeDisabled = closeDisabled;
	}

	public long transferToNullOutputStream() throws IOException {
		return transferTo(OutputStream.nullOutputStream());
	}

	public BufferedReaderExt reader() {
		return reader(null, null);
	}

	public BufferedReaderExt reader(Charset charset, Integer bufferSize) {
		if (charset == null)
			charset = MachineConfig.getDefaultCharset();
		var reader = new InputStreamReader(this, charset);
		BufferedReaderExt bufferedReader;
		if (bufferSize == null)
			bufferedReader = new BufferedReaderExt(reader);
		else
			bufferedReader = new BufferedReaderExt(reader, bufferSize);
		return bufferedReader;
	}

	// *** delegates ***

	@Override
	public boolean markSupported() {
		try {
			return getDelegateInputStream().markSupported();
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	@Override
	public void mark(int readlimit) {
		try {
			getDelegateInputStream().mark(readlimit);
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	@Override
	public int read() throws IOException {
		return getDelegateInputStream().read();
	}

	@Override
	public int read(byte[] b) throws IOException {
		return getDelegateInputStream().read(b);
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		return getDelegateInputStream().read(b, off, len);
	}

	@Override
	public byte[] readAllBytes() throws IOException {
		return getDelegateInputStream().readAllBytes();
	}

	@Override
	public byte[] readNBytes(int len) throws IOException {
		return getDelegateInputStream().readNBytes(len);
	}

	@Override
	public int readNBytes(byte[] b, int off, int len) throws IOException {
		return getDelegateInputStream().readNBytes(b, off, len);
	}

	@Override
	public long skip(long n) throws IOException {
		return getDelegateInputStream().skip(n);
	}

	@Override
	public int available() throws IOException {
		return getDelegateInputStream().available();
	}

	@Override
	public void reset() throws IOException {
		getDelegateInputStream().reset();
	}

	@Override
	public long transferTo(OutputStream out) throws IOException {
		return getDelegateInputStream().transferTo(out);
	}

	// *** utils ***

	private static Stream<String> lines(BufferedReader bufferedReader, Stream<String> lines) {
		return lines.onClose(() -> {
			try {
				bufferedReader.close();
			} catch (IOException e) {
				throw new UncheckedIOException(e);
			}
		});
	}

	public static class BufferedReaderExt extends BufferedReader {

		public BufferedReaderExt(Reader in) {
			super(in);
		}

		public BufferedReaderExt(Reader in, int sz) {
			super(in, sz);
		}

		@Override
		public Stream<String> lines() {
			return lines(false);
		}

		public Stream<String> lines(boolean closeOnComplete) {
			Stream<String> lines;
			if (!closeOnComplete)
				lines = super.lines();
			else {
				Predicate<Optional<String>> hasNext = lineOp -> {
					if (lineOp != null)
						return true;
					closeUnchecked();
					return false;
				};
				lines = Stream.<Optional<String>>iterate(Optional.empty(), hasNext, nil -> {
					String line;
					try {
						line = readLine();
					} catch (IOException e) {
						try {
							close();
						} catch (Throwable t) {
							e.addSuppressed(t);
						}
						throw new RuntimeException(e);
					}
					return line == null ? null : Optional.of(line);
				}).map(v -> v.orElse(null)).filter(Objects::nonNull);
			}
			return lines.onClose(this::closeUnchecked);
		}

		public <X> X lines(Function<Stream<String>, X> mapper) {
			return lines(mapper, false);
		}

		public String readAll() {
			return lines().collect(Collectors.joining(MachineConfig.getDefaultLineSeperator()));
		}

		public <X> X lines(Function<Stream<String>, X> mapper, boolean closeOnComplete) {
			Objects.requireNonNull(mapper);
			var lines = lines(closeOnComplete);
			return mapper.apply(lines);
		}

		protected void closeUnchecked() {
			try {
				this.close();
			} catch (IOException e) {
				throw new UncheckedIOException(e);
			}

		}
	}

}
