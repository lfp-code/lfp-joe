package com.lfp.joe.core.classpath;

import java.io.Serializable;
import java.lang.StackWalker.Option;
import java.lang.StackWalker.StackFrame;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.invoke.MethodType;
import java.lang.ref.SoftReference;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Constructor;
import java.lang.reflect.Executable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.Spliterators;
import java.util.WeakHashMap;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.ImmutableMemberCache.MethodRequest;
import com.lfp.joe.core.classpath.MemberCache.Accessor;
import com.lfp.joe.core.classpath.MemberCache.Invoker;
import com.lfp.joe.core.function.ComputeStore;
import com.lfp.joe.core.function.Durations;
import com.lfp.joe.core.function.FNV;
import com.lfp.joe.core.function.Once.SupplierOnce;
import com.lfp.joe.core.process.future.CSFutureTask;

//limit external classes!
@SuppressWarnings({ "unchecked" })
public interface CoreReflections {

	public static Class<?> getCallingClass(Class<?>... skipClassTypes) {
		BiPredicate<Integer, StackFrame> filter = (i, sf) -> {
			if (i == 0)
				return false;
			if (skipClassTypes != null)
				for (var skipClassType : skipClassTypes)
					if (skipClassType != null && skipClassType.getName().equals(sf.getClassName()))
						return false;
			return true;
		};
		return getCallingClass(filter);
	}

	public static Class<?> getCallingClass(BiPredicate<Integer, StackFrame> filter, Option... options) {
		Predicate<StackFrame> namedClassTypeFilter = sf -> {
			var declaringClass = sf.getDeclaringClass();
			return isNamedClassType(declaringClass);
		};
		Predicate<StackFrame> indexFilter;
		if (filter == null)
			indexFilter = null;
		else
			indexFilter = new Predicate<StackFrame>() {
				private int index = -1;

				@Override
				public boolean test(StackFrame sf) {
					index++;
					return filter.test(index, sf);
				}
			};
		var optionsStream = Stream.ofNullable(options).flatMap(Stream::of);
		optionsStream = optionsStream.filter(Objects::nonNull);
		optionsStream = Stream.concat(Stream.of(Option.RETAIN_CLASS_REFERENCE), optionsStream);
		return stackWalk(stream -> {
			return stream.map(StackFrame::getDeclaringClass).findFirst().orElse(null);
		}, optionsStream.collect(Collectors.toSet()), null, false, namedClassTypeFilter, indexFilter);
	}

	public static <X> X stackWalk(Function<? super Stream<StackFrame>, ? extends X> stackFrameStreamFunction,
			Set<Option> options, Integer estimateDepth, boolean disableThisClassSkip,
			Predicate<StackFrame>... stackFrameFilters) {
		Objects.requireNonNull(stackFrameStreamFunction);
		if (options == null)
			options = Set.of();
		final Function<StackFrame, StackFrame> stackFrameMapper;
		{
			Function<StackFrame, StackFrame> mapper = Function.identity();
			if (!options.contains(Option.RETAIN_CLASS_REFERENCE))
				mapper = mapper.andThen(sf -> {
					return new StackFrame() {

						private Optional<Class<?>> declaringClassTypeOp;

						@Override
						public Class<?> getDeclaringClass() {
							if (declaringClassTypeOp == null)
								declaringClassTypeOp = CoreReflections.tryForName(getClassName());
							if (declaringClassTypeOp.isPresent())
								return declaringClassTypeOp.get();
							return sf.getDeclaringClass();
						}

						@Override
						public MethodType getMethodType() {
							return sf.getMethodType();
						}

						@Override
						public String getClassName() {
							return sf.getClassName();
						}

						@Override
						public String getMethodName() {
							return sf.getMethodName();
						}

						@Override
						public String getDescriptor() {
							return sf.getDescriptor();
						}

						@Override
						public int getByteCodeIndex() {
							return sf.getByteCodeIndex();
						}

						@Override
						public String getFileName() {
							return sf.getFileName();
						}

						@Override
						public int getLineNumber() {
							return sf.getLineNumber();
						}

						@Override
						public boolean isNativeMethod() {
							return sf.isNativeMethod();
						}

						@Override
						public StackTraceElement toStackTraceElement() {
							return sf.toStackTraceElement();
						}

					};
				});
			stackFrameMapper = mapper;
		}
		final Predicate<StackFrame> stackFrameFilter;
		{
			Predicate<StackFrame> filter = v -> true;
			if (!disableThisClassSkip)
				filter = filter.and(new Predicate<StackFrame>() {

					private final String thisClassName = CoreReflections.class.getName();
					private Optional<Boolean> thisClassComplete = Optional.empty();

					@Override
					public boolean test(StackFrame stackFrame) {
						if (thisClassComplete.orElse(false))
							return true;
						var thisClassStackFrame = thisClassName.equals(stackFrame.getClassName());
						if (thisClassStackFrame) {
							if (thisClassComplete.isEmpty())
								// starting thisClass
								thisClassComplete = Optional.of(false);
							return false;
						} else {
							if (thisClassComplete.isEmpty())
								// before thisClass
								return false;
							// done thisClass
							thisClassComplete = Optional.of(true);
							return true;
						}
					}
				});
			if (stackFrameFilters != null)
				for (

				var stackFrameFilterAppend : stackFrameFilters) {
					if (stackFrameFilterAppend != null)
						filter = filter.and(stackFrameFilterAppend);
				}
			stackFrameFilter = filter;
		}
		StackWalker stackWalker;
		if (estimateDepth == null)
			stackWalker = StackWalker.getInstance(options);
		else
			stackWalker = StackWalker.getInstance(options, estimateDepth);
		return stackWalker.walk(stream -> {
			var sfIter = stream.map(stackFrameMapper).iterator();
			stream = Stream.empty();
			// ensure at least one stack frame
			while (sfIter.hasNext()) {
				var sf = sfIter.next();
				if (!stackFrameFilter.test(sf) && sfIter.hasNext())
					continue;
				stream = Stream.concat(Stream.of(sf), stream);
				break;
			}
			var append = StreamSupport.stream(Spliterators.spliterator(sfIter, Long.MAX_VALUE, 0), false);
			append = append.filter(stackFrameFilter);
			stream = Stream.concat(stream, append);
			return stackFrameStreamFunction.apply(stream);
		});
	}

	public static Predicate<Object> isInstance(Class<?> toClass) {
		return v -> isInstance(toClass, v);
	}

	public static boolean isInstance(Class<?> toClass, Object object) {
		return isAssignableFrom(toClass, Optional.ofNullable(object).map(Object::getClass).orElse(null));
	}

	public static boolean isAssignableFrom(Class<?> toClass, Class<?> cls) {
		if (toClass == null || cls == null)
			return false;
		if (Objects.equals(toClass, Object.class) || Objects.equals(toClass, cls))
			return true;
		Function<Class<?>, Class<?>> normalize = ct -> !ct.isPrimitive() ? ct : Primitives.getWrapperType(ct).get();
		toClass = normalize.apply(toClass);
		cls = normalize.apply(cls);
		return toClass.isAssignableFrom(cls);
	}

	public static <U> List<Class<? extends U>> getDistinctSuperClassTypes(
			Iterable<? extends Class<? extends U>> classTypes) {
		if (classTypes == null)
			return List.of();
		var ctList = new ArrayList<Class<? extends U>>();
		for (var ct : classTypes) {
			if (ct == null)
				continue;
			boolean add = true;
			var ctCheckIter = ctList.iterator();
			while (ctCheckIter.hasNext()) {
				var ctCheck = ctCheckIter.next();
				if (ctCheck.isAssignableFrom(ct)) {
					add = false;
					break;
				}
				if (ct.isAssignableFrom(ctCheck))
					ctCheckIter.remove();
			}
			if (add)
				ctList.add(ct);
		}
		return Collections.unmodifiableList(ctList);
	}

	public static <X> Optional<X> tryCast(Object input, Class<X> castType) {
		return tryCast(input, castType, true, true);
	}

	public static <X> Optional<X> tryCast(Object input, Class<X> castType, boolean primitiveDefaults) {
		return tryCast(input, castType, primitiveDefaults, true);
	}

	public static <X> Optional<X> tryCast(Object input, Class<X> castType, boolean primitiveDefaults,
			boolean primitiveAdapt) {
		if (castType == null)
			return Optional.empty();
		if (input == null) {
			if (primitiveDefaults)
				return Optional.ofNullable((X) Primitives.getDefaultValue(castType));
			return Optional.empty();
		}
		if (castType.isInstance(input))
			return Optional.of((X) input);
		if (primitiveAdapt) {
			Class<?> wrapperType = Primitives.getWrapperType(castType).orElse(null);
			if (wrapperType != null)
				return (Optional<X>) tryCast(input, wrapperType, primitiveDefaults, false);
			Class<?> primtiveType = Primitives.getPrimitiveType(castType).orElse(null);
			if (primtiveType != null)
				return (Optional<X>) tryCast(input, primtiveType, primitiveDefaults, false);
		}
		return Optional.empty();
	}

	public static boolean isLambdaClassType(Class<?> classType) {
		if (classType == null)
			return false;
		if (!classType.isSynthetic())
			return false;
		return classType.getName().contains("$$Lambda");
	}

	public static boolean isInstanceClassType(Class<?> classType) {
		if (classType == null)
			return false;
		if (classType.isInterface())
			return false;
		if (Modifier.isAbstract(classType.getModifiers()))
			return false;
		if (!isNamedClassType(classType))
			return false;
		return true;
	}

	public static boolean isNamedClassType(Class<?> classType) {
		if (classType == null)
			return false;
		if (classType.isAnonymousClass())
			return false;
		if (classType.isSynthetic())
			return false;
		if (Proxy.isProxyClass(classType))
			return false;
		return true;
	}

	public static <U> Optional<Class<U>> getNamedClassTypeIfVaries(Class<? extends U> classType) {
		Class<U> namedClassType = getNamedClassType(classType);
		if (!Objects.equals(classType, namedClassType))
			return Optional.of(namedClassType);
		return Optional.empty();
	}

	public static <U> Class<U> getNamedClassType(Class<? extends U> classType) {
		Objects.requireNonNull(classType);
		if (isNamedClassType(classType))
			return (Class<U>) classType;
		var ifaces = classType.getInterfaces();
		if (ifaces.length == 0) {
			var superclass = classType.getSuperclass();
			if (superclass != null)
				return (Class<U>) getNamedClassType(superclass);
		} else if (ifaces.length == 1)
			return (Class<U>) getNamedClassType(ifaces[0]);
		// if proxy/system class this is the best we can do
		return (Class<U>) classType;
	}

	public static boolean isBootModuleClassType(Class<?> classType) {
		return Optional.ofNullable(classType)
				.map(v -> getNamedClassType(v))
				.map(Class::getModule)
				.filter(ModuleLayer.boot().modules()::contains)
				.isPresent();
	}

	@SafeVarargs
	public static Stream<Class<?>> streamTypes(Class<?> classType, Predicate<Class<?>>... visitFilters) {
		return streamTypes(classType, true, visitFilters);
	}

	@SafeVarargs
	public static Stream<Class<?>> streamTypes(Class<?> classType, boolean all, Predicate<Class<?>>... visitFilters) {
		if (classType == null)
			return Stream.empty();
		return Stream.of(0).flatMap(nil -> {
			// this has to be FAST
			var spliterator = new Spliterators.AbstractSpliterator<Class<?>>(Long.MAX_VALUE, 0) {

				private final Set<Class<?>> visited = new HashSet<>();
				private Entry<Class<?>, Iterator<Class<?>>> currentEntry = nextEntry(classType);

				@Override
				public boolean tryAdvance(Consumer<? super Class<?>> action) {
					while (true) {
						var ctIter = currentEntry.getValue();
						while (ctIter.hasNext()) {
							var ct = ctIter.next();
							var skipVisit = Stream.ofNullable(visitFilters)
									.flatMap(Stream::of)
									.filter(Objects::nonNull)
									.anyMatch(v -> !v.test(ct));
							if (!skipVisit && visited.add(ct)) {
								action.accept(ct);
								return true;
							}
						}
						if (!all)
							return false;
						var superClass = currentEntry.getKey().getSuperclass();
						if (superClass == null)
							return false;
						currentEntry = nextEntry(superClass);
					}
				}

				private Entry<Class<?>, Iterator<Class<?>>> nextEntry(Class<?> nextClassType) {
					var ifaces = !all ? new Class<?>[0] : nextClassType.getInterfaces();
					var ctIter = new Iterator<Class<?>>() {

						private int index = -1;

						@Override
						public boolean hasNext() {
							return index < ifaces.length;
						}

						@Override
						public Class<?> next() {
							Class<?> next;
							if (index == -1)
								next = nextClassType;
							else
								next = ifaces[index];
							index++;
							return next;
						}
					};
					return Map.entry(nextClassType, ctIter);
				}
			};
			return StreamSupport.stream(spliterator, false);

		});
	}

	@SafeVarargs
	public static Stream<Constructor<?>> streamConstructors(Class<?> classType, boolean all,
			Predicate<Class<?>>... visitFilters) {
		return streamMembers(classType, all, List.<Function<Class<?>, Callable<Constructor<?>[]>>>of(
				ct -> ct::getDeclaredConstructors, ct -> ct::getConstructors), visitFilters).distinct();
	}

	@SafeVarargs
	public static Stream<Field> streamFields(Class<?> classType, boolean all, Predicate<Class<?>>... visitFilters) {
		return streamMembers(classType, all, List.of(ct -> ct::getDeclaredFields), visitFilters);
	}

	@SafeVarargs
	public static Stream<Method> streamMethods(Class<?> classType, boolean all, Predicate<Class<?>>... visitFilters) {
		return streamMembers(classType, all,
				List.<Function<Class<?>, Callable<Method[]>>>of(ct -> ct::getDeclaredMethods, ct -> ct::getMethods),
				visitFilters).distinct();
	}

	private static <M extends Member> Stream<M> streamMembers(Class<?> classType, boolean all,
			List<Function<Class<?>, Callable<M[]>>> memberLoaderSuppliers, Predicate<Class<?>>... visitFilters) {
		if (classType == null)
			return Stream.empty();
		var ctStream = streamTypes(classType, all, visitFilters);
		return ctStream.flatMap(ct -> {
			Stream<M> stream = Stream.empty();
			for (var memberLoaderSupplier : memberLoaderSuppliers) {
				if (memberLoaderSupplier == null)
					continue;
				var memberLoader = memberLoaderSupplier.apply(ct);
				if (memberLoader == null)
					continue;
				M[] memberArr;
				try {
					memberArr = memberLoader.call();
				} catch (NoClassDefFoundError e) {
					// suppress
					continue;
				} catch (RuntimeException e) {
					throw e;
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
				if (memberArr != null)
					stream = Stream.concat(stream, Stream.of(memberArr));
			}
			return stream;
		});
	}

	public static Stream<MethodHandles.Lookup> streamMethodHandleLookups(Class<?>... classTypes) {
		Stream<Class<?>> classTypeStream = classTypes == null ? Stream.empty() : Stream.of(classTypes);
		classTypeStream = classTypeStream.filter(Objects::nonNull).distinct();
		var mhLookupStream = classTypeStream.flatMap(v -> Const.METHOD_HANDLES_LOOKUP_STREAM_FUNCTION.apply(v));
		return mhLookupStream;
	}

	public static Stream<MethodHandle> streamSpecialMethodHandles(Method method) {
		if (method == null)
			return Stream.empty();
		var mhStream = streamMethodHandleLookups(method.getDeclaringClass())
				.flatMap(mhLookup -> streamSpecialMethodHandles(method, mhLookup));
		mhStream = mhStream.distinct();
		return mhStream;
	}

	private static Stream<MethodHandle> streamSpecialMethodHandles(Method method, MethodHandles.Lookup mhLookup) {
		if (method == null || mhLookup == null)
			return Stream.empty();
		Stream<Callable<MethodHandle>> callableStream = Stream.of(() -> {
			return mhLookup.findSpecial(method.getDeclaringClass(), method.getName(),
					MethodType.methodType(method.getReturnType(), method.getParameterTypes()),
					method.getDeclaringClass());
		}, () -> {
			return mhLookup.unreflectSpecial(method, method.getDeclaringClass());
		});
		return callableStream.map(v -> {
			try {
				return v.call();
			} catch (Exception e) {
				// suppress
				return null;
			}
		}).filter(Objects::nonNull).distinct();
	}

	public static Comparator<? super Member> memberComparator() {
		final Comparator<Class<?>> classTypeComparator;
		{
			Comparator<Class<?>> comparator = (v1, v2) -> {
				var v1Super = v1.isAssignableFrom(v2);
				var v2Super = v2.isAssignableFrom(v1);
				if (v1Super == v2Super)
					return 0;
				if (v1Super)
					return 1;
				return -1;
			};
			classTypeComparator = Comparator.nullsLast(comparator);
		}
		final Comparator<Method> methodComparator;
		{
			Comparator<Method> comparator = Comparator.comparing(Method::getParameterCount);
			comparator = comparator.thenComparing(Method::getReturnType, classTypeComparator);
			comparator = comparator.thenComparing(v -> v.getParameterTypes(), (v1, v2) -> {
				return Arrays.compare(v1, v2, classTypeComparator);
			});
			methodComparator = Comparator.nullsLast(comparator);
		}
		final Comparator<Member> memberComparator;
		{
			Comparator<Member> comparator = Comparator.comparing(Member::getName);
			comparator = comparator.thenComparing(Member::getDeclaringClass, classTypeComparator);
			comparator = comparator.thenComparing(v -> {
				return v instanceof Method ? (Method) v : null;
			}, methodComparator);
			comparator = comparator.thenComparing(Member::getModifiers);
			comparator = comparator.thenComparing(Object::hashCode);
			memberComparator = Comparator.nullsLast(comparator);
		}
		return memberComparator;
	}

	public static boolean compatibleMethodInvoke(Method method1, Method method2) {
		if (method1 == null || method2 == null)
			return false;
		if (Objects.equals(method1, method2))
			return true;
		if (Modifier.isStatic(method1.getModifiers()) != Modifier.isStatic(method2.getModifiers()))
			return false;
		if (!method1.getName().equals(method2.getName()))
			return false;
		return parameterTypesAssignableFrom(method1, Arrays.asList(method2.getParameterTypes()));
	}

	public static boolean parameterTypesAssignableFrom(Executable executable,
			Iterable<? extends Class<?>> parameterTypes) {
		if (executable == null || parameterTypes == null)
			return false;
		var executableParameterCount = executable.getParameterCount();
		if (parameterTypes instanceof Collection && executableParameterCount != ((Collection<?>) parameterTypes).size())
			return false;
		final Iterator<? extends Class<?>> parameterTypesIterator;
		if (parameterTypes instanceof List)
			parameterTypesIterator = null;
		else
			parameterTypesIterator = Optional.ofNullable(parameterTypes.iterator())
					.orElseGet(Collections::emptyIterator);
		Class<?>[] executableParameterTypes = null;
		for (int i = 0; i < executableParameterCount; i++) {
			Class<?> parameterType;
			if (parameterTypesIterator != null) {
				if (!parameterTypesIterator.hasNext())
					return false;
				parameterType = parameterTypesIterator.next();
			} else
				parameterType = ((List<? extends Class<?>>) parameterTypes).get(i);
			if (executableParameterTypes == null)
				executableParameterTypes = executable.getParameterTypes();
			if (!isAssignableFrom(executableParameterTypes[i], parameterType))
				return false;
		}
		return true;
	}

	public static Optional<Class<?>> tryForName(final String className, ClassLoader... classLoaders) {
		if (className == null || className.isEmpty())
			return Optional.empty();
		if (Character.isWhitespace(className.charAt(0))
				|| Character.isWhitespace(className.charAt(className.length() - 1)))
			return Optional.empty();
		var primitiveType = Primitives.getPrimitiveNameToTypeMap().get(className);
		if (primitiveType != null)
			return Optional.of(primitiveType);
		Iterator<ClassLoader> clIter;
		{
			var clStream = Stream.ofNullable(classLoaders).flatMap(Stream::of);
			clStream = Stream.concat(clStream, streamDefaultClassLoaders());
			clStream = clStream.filter(Objects::nonNull).distinct();
			clIter = clStream.iterator();
		}
		var splitChars = Arrays.asList(null, '.', '$');
		while (clIter.hasNext()) {
			var classLoader = clIter.next();
			for (var splitChar : splitChars) {
				var classTypeOp = tryForName(classLoader, className, splitChar);
				if (classTypeOp.isPresent())
					return classTypeOp;
			}
		}
		return Optional.empty();
	}

	private static Optional<Class<?>> tryForName(ClassLoader classLoader, String className, Character splitChar,
			ClassLoader... classLoaders) {
		String subClassSimpleName;
		if (splitChar == null)
			subClassSimpleName = null;
		else {
			var splitAt = className.lastIndexOf(splitChar);
			if (splitAt <= 0)
				return Optional.empty();
			subClassSimpleName = className.substring(splitAt + 1, className.length());
			className = className.substring(0, splitAt);
		}
		Class<?> declaringClassType;
		try {
			var ct = Class.forName(className, false, classLoader);
			if (ct == null)
				return Optional.empty();
			if (subClassSimpleName == null)
				return Optional.of(ct);
			declaringClassType = ct;
		} catch (Throwable t) {
			// suppress
			return Optional.empty();
		}
		if (declaringClassType.isEnum()) {
			var enumConstants = declaringClassType.getEnumConstants();
			if (enumConstants.length > 0) {
				for (var ignoreCase : List.of(false, true)) {
					for (var enumConstant : enumConstants) {
						var enumName = ((Enum<?>) enumConstant).name();
						if ((!ignoreCase && subClassSimpleName.equals(enumName))
								|| (ignoreCase && subClassSimpleName.equalsIgnoreCase(enumName)))
							return Optional.of(enumConstant.getClass());
					}
				}
			}
			return Optional.empty();
		}
		var subClassTypeIter = Stream
				.<Supplier<Class<?>[]>>of(declaringClassType::getClasses, declaringClassType::getDeclaredClasses)
				.map(Supplier::get)
				.filter(Objects::nonNull)
				.flatMap(Stream::of)
				.filter(Objects::nonNull)
				.distinct()
				.iterator();
		while (subClassTypeIter.hasNext()) {
			var subClassType = subClassTypeIter.next();
			if (Objects.equals(subClassSimpleName, subClassType.getSimpleName()))
				return Optional.of(subClassType);
		}
		return Optional.empty();
	}

	public static ClassLoader getDefaultClassLoader() {
		return streamDefaultClassLoaders().findFirst().get();
	}

	public static Stream<ClassLoader> streamDefaultClassLoaders() {
		Stream<Supplier<ClassLoader>> clSupplierStream = Stream.of(Thread.currentThread()::getContextClassLoader,
				CoreReflections.class::getClassLoader, ClassLoader::getSystemClassLoader,
				ClassLoader::getPlatformClassLoader);
		return clSupplierStream.map(Supplier::get).filter(Objects::nonNull).distinct();
	}

	public static Optional<Class<?>> findLoadedClass(ClassLoader classLoader, String name) {
		if (classLoader == null || name == null || name.isEmpty())
			return Optional.empty();
		var classType = Const.ClassLoader_findLoadedClass_INVOKER_ONCE.get().invoke(classLoader, name);
		return Optional.ofNullable(classType);
	}

	public static <U> Optional<U> tryGetStaticInstance(Class<U> classType) {
		if (classType == null)
			return Optional.empty();
		Class<U> namedClassType;
		if (!classType.equals(namedClassType = CoreReflections.getNamedClassType(classType)))
			return tryGetStaticInstance(namedClassType);
		if (classType.isEnum() && classType.getEnumConstants().length == 1)
			return Optional.of(classType.getEnumConstants()[0]);
		{
			var methodList = Stream.ofNullable(classType.getDeclaredMethods()).flatMap(Stream::of).filter(method -> {
				return method.getParameterCount() == 0;
			}).filter(method -> {
				var modifiers = method.getModifiers();
				return Modifier.isPublic(modifiers) && Modifier.isStatic(modifiers);
			}).filter(method -> {
				return CoreReflections.isAssignableFrom(classType, method.getReturnType());
			}).filter(method -> {
				return Const.PROVIDER_METHOD_NAME.equals(method.getName())
						|| method.getAnnotation(FactoryMethod.class) != null;
			}).limit(2).collect(Collectors.toList());
			if (methodList.size() == 1)
				try {
					return Optional.of((U) methodList.get(0).invoke(null));
				} catch (RuntimeException e) {
					throw e;
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
		}
		{
			var fieldList = Stream.ofNullable(classType.getDeclaredFields()).flatMap(Stream::of).filter(field -> {
				var modifiers = field.getModifiers();
				return Modifier.isPublic(modifiers) && Modifier.isStatic(modifiers);
			}).filter(field -> {
				return CoreReflections.isAssignableFrom(classType, field.getType());
			}).limit(2).collect(Collectors.toList());
			if (fieldList.size() == 1)
				try {
					return Optional.of((U) fieldList.get(0).get(null));
				} catch (RuntimeException e) {
					throw e;
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
		}
		return Optional.empty();
	}

	public static <U> U newInstanceUnchecked(Class<U> classType, Object... args) {
		Objects.requireNonNull(classType);
		if (args == null)
			return newInstanceUnchecked(classType, new Object[0]);
		classType = CoreReflections.getNamedClassType(classType);
		if (args.length == 0) {
			var instance = tryGetStaticInstance(classType).orElse(null);
			if (instance != null)
				return instance;
		}
		boolean constructorPublic = false;
		Constructor<?> constructor = null;
		var ctorIter = CoreReflections.streamConstructors(classType, false).iterator();
		while (ctorIter.hasNext()) {
			var ctor = ctorIter.next();
			var ptypes = ctor.getParameterTypes();
			if (ptypes.length != args.length)
				continue;
			boolean compatible = true;
			for (int i = 0; i < ptypes.length; i++) {
				var ptype = ptypes[i];
				var arg = args[i];
				if (!CoreReflections.isAssignableFrom(ptype,
						Optional.ofNullable(arg).map(Object::getClass).orElse(null))) {
					compatible = false;
					break;
				}
			}
			if (!compatible)
				continue;
			if (constructor == null) {
				constructor = ctor;
				if (Modifier.isPublic(constructor.getModifiers())) {
					constructorPublic = true;
					break;
				}
			} else
				throw new IllegalArgumentException(
						String.format("multiple constructors found. classType:%s args:%s", classType, args));

		}
		if (constructor == null)
			throw new IllegalArgumentException(
					String.format("constructor not found. classType:%s args:%s", classType, args));
		if (!constructorPublic)
			setAccessible(constructor, true);
		return (U) newInstanceUnchecked(constructor, args);
	}

	public static <X> X newInstanceUnchecked(Constructor<X> constructor, Object... args) {
		try {
			return constructor.newInstance(args);
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static long hashMembers(Member... members) {
		var serializableStream = Stream.ofNullable(members)
				.flatMap(Stream::of)
				.filter(Objects::nonNull)
				.flatMap(v -> Const.MEMBER_HASH_INPUT_STREAMER.apply(v));
		return FNV.hash64(() -> serializableStream.iterator());
	}

	public static <X extends AccessibleObject> X setAccessible(X accessibleObject) {
		return setAccessible(accessibleObject, true);
	}

	public static <X extends AccessibleObject> X setAccessible(X accessibleObject, boolean flag) {
		Const.ACCESSIBLE_OBJECT_MODIFIER.accept(accessibleObject, flag);
		return accessibleObject;
	}

	public static void moduleAddOpens(Class<?> addOpensClassType) {
		var callerClassType = StackWalker.getInstance(Option.RETAIN_CLASS_REFERENCE).getCallerClass();
		moduleAddOpens(addOpensClassType, callerClassType);
	}

	public static void moduleAddOpens(Class<?> addOpensClassType, Class<?> callerClassType) {
		Const.MODULE_ADD_OPENS_MODIFIER.accept(addOpensClassType, callerClassType);
	}

	static enum Const {
		;

		private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
		// instances
		private static final String PROVIDER_METHOD_NAME = "provider";

		private static final SupplierOnce<Invoker<ClassLoader, Class<?>>> ClassLoader_findLoadedClass_INVOKER_ONCE = SupplierOnce
				.of(() -> {
					var request = MethodRequest.of(ClassLoader.class, Class.class, "findLoadedClass", String.class);
					return MemberCache.getMethodInvoker((MethodRequest) request);
				});
		private static final SupplierOnce<Accessor<Method, String>> Method_signature_ACCESSOR_ONCE = SupplierOnce
				.of(() -> {
					var request = FieldRequest.of(Method.class, String.class, "signature");
					return MemberCache.getFieldAccessor(request);
				});

		private static final BiConsumer<Class<?>, Class<?>> MODULE_ADD_OPENS_MODIFIER = new BiConsumer<Class<?>, Class<?>>() {

			private final ComputeStore<Entry<Class<?>, Class<?>>, Optional<Void>> addOpensCache = ComputeStore
					.from(new WeakHashMap<>(), SoftReference::new);

			@Override
			public void accept(Class<?> addOpensClassType, Class<?> callerClassType) {
				Objects.requireNonNull(addOpensClassType);
				Objects.requireNonNull(callerClassType);
				addOpensClassType = getNamedClassType(addOpensClassType);
				callerClassType = getNamedClassType(callerClassType);
				if (Objects.equals(addOpensClassType, callerClassType))
					return;
				acceptInternal(addOpensClassType, callerClassType);

			}

			private void acceptInternal(Class<?> addOpensClassType, Class<?> callerClassType) {
				addOpensCache.computeIfAbsent(Map.entry(addOpensClassType, callerClassType), () -> {
					var callerClassTypeModule = callerClassType.getModule();
					if (!callerClassTypeModule.isNamed())
						addOpensClassType.getModule()
								.addOpens(addOpensClassType.getPackageName(), callerClassTypeModule);
					return Optional.empty();
				});
			}

		};

		private static final Function<Member, Stream<Serializable>> MEMBER_HASH_INPUT_STREAMER = new Function<Member, Stream<Serializable>>() {

			private final ComputeStore<Class<?>, List<Method>> hashMethodCache = ComputeStore.from(new WeakHashMap<>(),
					SoftReference::new);
			private final Predicate<Method> hashMethodPredicate;
			{
				Predicate<Method> predicate = v -> Member.class.isAssignableFrom(v.getDeclaringClass());
				predicate = predicate.and(v -> Modifier.isPublic(v.getModifiers()));
				predicate = predicate.and(v -> !Modifier.isStatic(v.getModifiers()));
				predicate = predicate.and(v -> v.getParameterCount() == 0);
				predicate = predicate.and(v -> {
					var returnType = v.getReturnType();
					return returnType.isPrimitive() || Serializable.class.isAssignableFrom(returnType);
				});
				predicate = predicate.and(v -> {
					var returnType = v.getReturnType();
					var name = v.getName();
					if (String.class.isAssignableFrom(returnType) && name.startsWith("to") && name.endsWith("String"))
						return false;
					if (Class.class.isAssignableFrom(returnType) && name.equals("getDeclaringClass"))
						return false;
					return true;
				});
				var objectMethods = streamMethods(Object.class, true).collect(Collectors.toUnmodifiableList());
				predicate = predicate.and(v -> {
					for (var objectMethod : objectMethods)
						if (compatibleMethodInvoke(v, objectMethod))
							return false;
					return true;
				});
				hashMethodPredicate = predicate;
			}

			private final Comparator<Method> hashMethodComparator;
			{
				Comparator<Method> comparator = Comparator
						.comparing(v -> CoreReflections.getNamedClassType(v.getDeclaringClass()).getName());
				comparator = comparator.thenComparing(v -> v.getName());
				comparator = comparator.thenComparing(v -> v.getParameterCount());
				comparator = comparator.thenComparing(v -> v.getReturnType().getName());
				comparator = comparator.thenComparing(v -> {
					return Stream.of(v.getParameterTypes()).map(Class::getName).collect(Collectors.joining("#"));
				});
				comparator = comparator.thenComparing(v -> {
					var signature = Method_signature_ACCESSOR_ONCE.get().apply(v);
					return Optional.ofNullable(signature).orElse("");
				});
				comparator = comparator.thenComparing(v -> v.toGenericString());
				hashMethodComparator = comparator;
			}

			@Override
			public Stream<Serializable> apply(Member member) {
				Objects.requireNonNull(member);
				var memberClassType = CoreReflections.getNamedClassType(member.getClass());
				var methodList = hashMethodCache.computeIfAbsent(memberClassType, () -> {
					var methodStream = streamTypes(memberClassType).map(Class::getMethods).flatMap(Stream::of);
					methodStream = methodStream.distinct();
					methodStream = methodStream.filter(hashMethodPredicate);
					methodStream = methodStream.sorted(hashMethodComparator);
					return methodStream.collect(Collectors.toUnmodifiableList());
				});
				var serializables = methodList.stream().map(method -> {
					try {
						var serializable = (Serializable) method.invoke(member);
						return serializable;
					} catch (IllegalAccessException | InvocationTargetException e) {
						throw new RuntimeException(e);
					}
				});
				serializables = Stream.concat(Stream.of(getNamedClassType(member.getDeclaringClass())), serializables);
				return serializables;
			}

		};

		private static final Function<Class<?>, Stream<MethodHandles.Lookup>> METHOD_HANDLES_LOOKUP_STREAM_FUNCTION = new Function<Class<?>, Stream<MethodHandles.Lookup>>() {

			private Optional<Constructor<MethodHandles.Lookup>> _constructorOp;

			@Override
			public Stream<MethodHandles.Lookup> apply(Class<?> classType) {
				if (classType == null)
					return Stream.empty();
				Stream<Callable<MethodHandles.Lookup>> stream = Stream.empty();
				stream = Stream.concat(stream, Stream.of(() -> {
					return MethodHandles.lookup();
				}));
				stream = Stream.concat(stream, Stream.of(() -> {
					if (_constructorOp == null)
						synchronized (this) {
							if (_constructorOp == null)
								_constructorOp = loadConstructor();
						}
					if (_constructorOp.isEmpty())
						return null;
					return _constructorOp.get().newInstance(classType, -1);
				}));
				return stream.map(v -> {
					try {
						return v.call();
					} catch (Exception e) {
						return null;
					}
				}).filter(Objects::nonNull).distinct();
			}

			private Optional<Constructor<Lookup>> loadConstructor() {
				CoreReflections.moduleAddOpens(MethodHandles.Lookup.class, THIS_CLASS);
				Constructor<Lookup> ctor;
				try {
					ctor = MethodHandles.Lookup.class.getDeclaredConstructor(Class.class, int.class);
					ctor.setAccessible(true);
					return Optional.of(ctor);
				} catch (Exception e) {
					return Optional.empty();
				}

			}
		};

		private static final BiConsumer<AccessibleObject, Boolean> ACCESSIBLE_OBJECT_MODIFIER = new BiConsumer<AccessibleObject, Boolean>() {

			private Field _Field_modifiersField_FIELD;

			@SuppressWarnings("deprecation")
			@Override
			public void accept(AccessibleObject accessibleObject, Boolean flag) {
				Objects.requireNonNull(accessibleObject);
				flag = Optional.ofNullable(flag).orElse(false);
				if (flag && isAccessibleMember(accessibleObject))
					return;
				if (Objects.equals(accessibleObject.isAccessible(), flag))
					return;
				if (accessibleObject instanceof Member)
					CoreReflections.moduleAddOpens(((Member) accessibleObject).getDeclaringClass(), THIS_CLASS);
				accessibleObject.setAccessible(flag);
				if (!flag || !(accessibleObject instanceof Field))
					return;
				Field field = (Field) accessibleObject;
				if (!Modifier.isFinal(field.getModifiers()))
					return;
				try {
					if (_Field_modifiersField_FIELD == null)
						synchronized (this) {
							if (_Field_modifiersField_FIELD == null) {
								CoreReflections.moduleAddOpens(Field.class, THIS_CLASS);
								Field modifiersField = Field.class.getDeclaredField("modifiers");
								modifiersField.setAccessible(true);
								_Field_modifiersField_FIELD = modifiersField;

							}
						}
					_Field_modifiersField_FIELD.setInt(field, field.getModifiers() & ~Modifier.FINAL);
				} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException
						| SecurityException e) {
					throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e)
							: new RuntimeException(e);
				}

			}

			private boolean isAccessibleMember(AccessibleObject accessibleObject) {
				if (!(accessibleObject instanceof Member))
					return false;
				var member = (Member) accessibleObject;
				if (!Modifier.isPublic(member.getModifiers()))
					return false;
				if (member instanceof Field && Modifier.isFinal(member.getModifiers()))
					return false;
				var declaringClass = member.getDeclaringClass();
				if (declaringClass == null || !Modifier.isPublic(declaringClass.getModifiers()))
					return false;
				return true;

			}

		};

	}

	public static void main(String[] args) throws NoSuchMethodException, SecurityException, InterruptedException {
		System.out.println(findLoadedClass(CoreReflections.getDefaultClassLoader(), Long.class.getName()));
		System.out.println(Const.Method_signature_ACCESSOR_ONCE.get());
		long value = 77;
		Object obj = value;
		System.out.println(Long.class.isAssignableFrom(obj.getClass()));
		System.out.println(
				long.class.isAssignableFrom(((Object) LongStream.range(0, 1).findFirst().getAsLong()).getClass()));

		var date1 = new Date(0) {

			@Override
			public long getTime() {
				return 0;
			}

			public String test() {
				return null;
			}

			@Override
			public int hashCode() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public String toString() {
				// TODO Auto-generated method stub
				return "";
			}

		};

		var method = Date.class.getMethod("getTime");
		System.out.println(MemberCache.getFieldValue(FieldRequest.of(Method.class, String.class, "signature"), method));
		System.out.println(method.toGenericString());
		System.out.println(MemberCache.getFieldValue(FieldRequest.of(Method.class, String.class, "signature"), method));
		var date2 = new Date(1);
		System.out.println(hashMembers(date1.getClass().getMethod("getTime")));
		System.out.println(hashMembers(date2.getClass().getMethod("getTime")));
		System.out.println(hashMembers(date1.getClass().getMethod("test")));
		System.out.println(hashMembers(date1.getClass().getMethod("test")));
		System.out.println(hashMembers(Date.class.getDeclaredMethod("getTime")));

		System.out.println(CoreReflections.streamTypes(CSFutureTask.class, v -> !v.isInterface()).count());
		System.out.println(CoreReflections.streamTypes(CSFutureTask.class).count());
		var enumClassName = TimeUnit.class.getName() + "." + TimeUnit.SECONDS.name();
		System.out.println(tryForName(enumClassName));
		System.out.println(tryForName(
				Spliterators.class.getName() + "." + Spliterators.AbstractSpliterator.class.getSimpleName()));
		System.out.println(tryForName(Spliterators.AbstractSpliterator.class.getName() + " "));
		{
			for (var input : List.of(Spliterators.AbstractSpliterator.class.getName(),
					Spliterators.class.getName() + "." + Spliterators.AbstractSpliterator.class.getSimpleName(),
					enumClassName, "gabagool.neat$wow")) {
				var successCount = new AtomicLong();
				var averageNanos = LongStream.range(0, 15_000).map(i -> {
					if (i == 0)
						System.out.println(input);
					var startedAt = System.nanoTime();
					var classTypeOp = tryForName(input);
					var elapsed = System.nanoTime() - startedAt;
					if (classTypeOp.isPresent())
						successCount.incrementAndGet();
					return elapsed;
				}).average().getAsDouble();
				System.out.println(
						Durations.toMillis(Duration.ofNanos((long) averageNanos)) + " - " + successCount.get());
			}
		}
		System.out.println(CoreReflections.getDefaultClassLoader());
		System.out.println(newInstanceUnchecked(String.class, new byte[0]));

		var future = CompletableFuture.completedFuture(null);
		System.out.println(isNamedClassType(future.getClass()));
		System.out.println(getNamedClassType(future.getClass()));

		System.out.println(tryForName(CoreReflections.class.getName()));
		streamTypes(ThreadPoolExecutor.class).forEach(v -> {
			System.out.println(String.format("types: %s", v));
		});
		streamFields(Date.class, true).forEach(v -> {
			System.out.println(String.format("fields: %s", v));
		});
		streamMethods(Date.class, true).forEach(v -> {
			System.out.println(String.format("methods: %s", v));
		});
		System.out.println(newInstanceUnchecked(Date.class, 1_000_000_000_000l));
		streamFields(AbstractExecutorService.class, true).forEach(v -> {
			System.out.println(String.format("fields: %s", v));
		});
	}

}
