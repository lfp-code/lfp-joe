package com.lfp.joe.core.log;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.MemoizedSupplier;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;

public class Logging {

	private static final String DEBUG_ID_LOG_TEMPLATE = "debugId:{}";
	private static final String ERROR_MESSAGE_LOG_TEMPLATE = " - {}";
	private static final MemoizedSupplier<org.slf4j.Logger> LOGGER_S = MemoizedSupplier.create(() -> {
		Class<?> classType = new Object() {}.getClass().getEnclosingClass();
		return org.slf4j.LoggerFactory.getLogger(classType);
	});

	private static final MemoizedSupplier<Optional<Field>> loggerCache_FIELD_S = MemoizedSupplier.create(() -> {
		Stream<Field> stream = Arrays.asList(LoggerContext.class.getDeclaredFields()).stream();
		stream = stream.filter(f -> Map.class.isAssignableFrom(f.getType()));
		stream = stream.filter(f -> "loggerCache".equals(f.getName()));
		Optional<Field> op = stream.findFirst().map(f -> {
			f.setAccessible(true);
			return f;
		});
		return op;
	});

	public static Logger logger() {
		return logger((Predicate<Class<?>>) null);
	}

	public static Logger logger(Predicate<Class<?>> callingClassTypeFilter) {
		var callingClassType = CoreReflections.getCallingClass((i, sf) -> {
			var ct = sf.getDeclaringClass();
			if (Logging.class.isAssignableFrom(ct))
				return false;
			if (!CoreReflections.isNamedClassType(ct))
				return false;
			if (callingClassTypeFilter != null && !callingClassTypeFilter.test(ct))
				return false;
			return true;
		});
		return org.slf4j.LoggerFactory.getLogger(callingClassType);
	}

	public static Logger logger(Object obj) {
		return logger(obj == null ? null : () -> obj);
	}

	public static Logger logger(Supplier<Object> loggerSupplier) {
		if (loggerSupplier == null)
			return LOGGER_S.get();
		return new DelegateLogger() {

			private final Object loggerMutex = new Object();
			private Logger _logger;

			@Override
			public Logger getDelegate() {
				if (_logger == null)
					synchronized (loggerMutex) {
						if (_logger == null)
							_logger = createLogger();
					}
				return _logger;
			}

			private Logger createLogger() {
				var obj = loggerSupplier.get();
				if (obj == null)
					return LOGGER_S.get();
				if (obj instanceof Logger)
					return (Logger) obj;
				if (obj instanceof Class) {
					var classType = CoreReflections.getNamedClassType((Class<?>) obj);
					return org.slf4j.LoggerFactory.getLogger(classType);
				}
				return org.slf4j.LoggerFactory.getLogger(Objects.toString(obj));
			}
		};
	}

	public static void log(Logger logger, Level level, String message, Object... arguments) {
		levelConsumer(logger, level).accept(message, arguments);
	}

	public static Optional<LogConsumer> tryGetLevelConsumer(Logger logger, Level level) {
		if (logger == null)
			return Optional.empty();
		LogConsumer result = levelConsumerInternal(logger, level);
		if (result == null)
			return Optional.empty();
		return Optional.of(result);
	}

	public static LogConsumer levelConsumer(Logger logger, Level level) {
		LogConsumer result = levelConsumerInternal(logger, level);
		if (result == null)
			return LogConsumer.NO_OP;
		return result;
	}

	private static LogConsumer levelConsumerInternal(Logger logger, Level level) {
		Objects.requireNonNull(logger);
		if (level == null)
			return levelConsumerInternal(logger, Level.ERROR);
		LogConsumer biConsumer = null;
		switch (level.toInt()) {
		case Level.DEBUG_INT:
			if (logger.isDebugEnabled())
				biConsumer = (s, args) -> logger.debug(s, args);
			break;
		case Level.INFO_INT:
			if (logger.isInfoEnabled())
				biConsumer = (s, args) -> logger.info(s, args);
			break;
		case Level.TRACE_INT:
			if (logger.isTraceEnabled())
				biConsumer = (s, args) -> logger.trace(s, args);
			break;
		case Level.WARN_INT:
			if (logger.isWarnEnabled())
				biConsumer = (s, args) -> logger.warn(s, args);
			break;
		case Level.ERROR_INT:
			if (logger.isErrorEnabled())
				biConsumer = (s, args) -> logger.error(s, args);
			break;
		case Level.OFF_INT:
			return null;
		default:
			// catches to error
			biConsumer = (s, args) -> logger.error(s, args);
		}
		return biConsumer;
	}

	public static void logStackTraceDebug(Logger logger, Level level, String message, Object... arguments) {
		if (Objects.equals(level, Level.DEBUG)) {
			log(logger, level, message, arguments);
			return;
		}
		int errorIndex = -1;
		for (int i = 0; arguments != null && i < arguments.length; i++) {
			var argument = arguments[i];
			if (argument instanceof Throwable)
				errorIndex = i;
		}
		if (errorIndex == -1) {
			log(logger, level, message, arguments);
			return;
		}
		var argumentList = Stream.of(arguments).collect(Collectors.toList());
		Throwable error = (Throwable) argumentList.remove(errorIndex);
		var debugId = "log" + UUID.randomUUID().toString().replaceAll("\\W+", "").toLowerCase();
		argumentList.add(debugId);
		var argumentLiteList = new ArrayList<>(argumentList.size() + 1);
		argumentLiteList.addAll(argumentList);
		argumentList.add(error);
		String errorMessage = null;
		while (error != null && (errorMessage == null || errorMessage.isEmpty())) {
			errorMessage = error.getMessage();
			error = error.getCause();
		}
		argumentLiteList.add(errorMessage);
		if (message == null)
			message = "";
		else if (!message.isBlank())
			message += " ";
		message += DEBUG_ID_LOG_TEMPLATE;
		log(logger, level, message + ERROR_MESSAGE_LOG_TEMPLATE, argumentLiteList.toArray());
		log(logger, Level.DEBUG, message, argumentList.toArray());
	}

	@SuppressWarnings("unchecked")
	public static boolean destroy(Logger logger) {
		if (logger == null)
			return false;
		ILoggerFactory factory = LoggerFactory.getILoggerFactory();
		if (!(factory instanceof LoggerContext))
			return false;
		Optional<Field> op = loggerCache_FIELD_S.get();
		if (!op.isPresent())
			return false;
		Map<String, Logger> loggerCache;
		try {
			loggerCache = (Map<String, Logger>) op.get().get(factory);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
					? java.lang.RuntimeException.class.cast(e)
					: new java.lang.RuntimeException(e);
		}
		boolean removed = loggerCache.remove(logger.getName()) != null;
		return removed;
	}

	/**
	 * FINEST->TRACE, FINER->DEBUG, FINE->DEBUG, INFO->INFO, WARNING->WARN,
	 * SEVERE->ERROR
	 *
	 **/
	public static Optional<Level> toLevel(String levelStr) {
		if (levelStr == null || levelStr.isEmpty())
			return Optional.empty();
		Level level = Level.toLevel(levelStr, null);
		if (level != null)
			return Optional.of(level);
		levelStr = levelStr.toUpperCase();
		switch (levelStr) {
		case "FINEST": {
			return Optional.of(Level.TRACE);
		}
		case "FINER": {
			return Optional.of(Level.DEBUG);
		}
		case "FINE": {
			return Optional.of(Level.DEBUG);
		}
		case "WARNING": {
			return Optional.of(Level.WARN);
		}
		case "SEVERE": {
			return Optional.of(Level.ERROR);
		}
		case "FATAL": {
			return Optional.of(Level.ERROR);
		}
		}
		return Optional.empty();
	}

	public static void main(String[] args) {
		Object[] objArr = new Object[] { "neat", null, "cool" };
		System.out.println(Stream.of(objArr).count());

	}

}
