package com.lfp.joe.core.properties.converter;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.aeonbits.owner.Converter;

public class MultimapConverter implements Converter<Map<String, List<String>>> {

	private final EntryListConverter entryListConverter = new EntryListConverter();

	@Override
	public Map<String, List<String>> convert(Method method, String input) {
		var entries = entryListConverter.convert(method, input);
		if (entries == null || entries.isEmpty())
			return Collections.emptyMap();
		Map<String, List<String>> result = new LinkedHashMap<>();
		entries.forEach(ent -> result.computeIfAbsent(ent.getKey(), nil -> new ArrayList<>()).add(ent.getValue()));
		return Collections.unmodifiableMap(result);
	}

}
