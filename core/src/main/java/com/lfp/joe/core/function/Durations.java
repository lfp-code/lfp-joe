package com.lfp.joe.core.function;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class Durations {

	private static final Duration MAX = Duration.ofSeconds(Long.MAX_VALUE, // Max allowed seconds
			999999999L // Max nanoseconds less than a second
	);
	private static final Duration MAX_BY_NANOS = Duration.ofNanos(Long.MAX_VALUE);
	private static final Duration MAX_BY_MILLIS = Duration.ofMillis(Long.MAX_VALUE);
	private static final int NANOS_PER_MILLI = 1_000_000;

	protected Durations() {}

	public static Duration max() {
		return MAX;
	}

	public static Duration max(Duration duration, Duration... durations) {
		return stream(duration, durations).sorted(Comparator.reverseOrder())
				.findFirst()
				.orElseThrow(NoSuchElementException::new);
	}

	public static Duration min(Duration duration, Duration... durations) {
		return stream(duration, durations).sorted().findFirst().orElseThrow(NoSuchElementException::new);
	}

	public static boolean isMax(Duration duration) {
		if (duration == null)
			return false;
		return isGreaterThanOrEqual(duration, MAX);
	}

	public static boolean isNegativeOrZero(Duration duration) {
		Objects.requireNonNull(duration);
		return duration.isNegative() || duration.isZero();
	}

	public static boolean isPostive(Duration duration) {
		return !isNegativeOrZero(duration);
	}

	public static boolean isGreaterThan(Duration duration, Duration other) {
		Objects.requireNonNull(duration);
		if (duration == other)
			return false;
		return duration.compareTo(other) > 0;
	}

	public static boolean isGreaterThanOrEqual(Duration duration, Duration other) {
		Objects.requireNonNull(duration);
		if (duration == other)
			return true;
		return duration.compareTo(other) >= 0;
	}

	public static boolean isLessThan(Duration duration, Duration other) {
		return !isGreaterThanOrEqual(duration, other);
	}

	public static boolean isLessThanOrEqual(Duration duration, Duration other) {
		return !isGreaterThan(duration, other);
	}

	public static Duration from(long amount, TimeUnit unit) {
		Objects.requireNonNull(unit);
		return Duration.of(amount, toChronoUnit(unit));
	}

	public static Entry<Long, TimeUnit> toTimeUnitEntry(Duration duration) {
		Objects.requireNonNull(duration);
		return Map.entry(getTimeAmount(duration), getTimeUnit(duration));
	}

	public static long getTimeAmount(Duration duration) {
		Objects.requireNonNull(duration);
		if (duration.compareTo(MAX_BY_NANOS) < 0)
			return duration.toNanos();
		else if (duration.compareTo(MAX_BY_MILLIS) < 0)
			return duration.toMillis();
		else
			return duration.toSeconds();
	}

	public static TimeUnit getTimeUnit(Duration duration) {
		Objects.requireNonNull(duration);
		if (duration.compareTo(MAX_BY_NANOS) < 0)
			return TimeUnit.NANOSECONDS;
		else if (duration.compareTo(MAX_BY_MILLIS) < 0)
			return TimeUnit.MILLISECONDS;
		else
			return TimeUnit.SECONDS;
	}

	public static TimeUnit getTimeUnit(Duration duration, Duration... durations) {
		var durationStream = stream(duration, durations);
		Comparator<Duration> sorter;
		{
			Comparator<Duration> comparator = Comparator.comparing(v -> TimeUnit.SECONDS.convert(v));
			comparator = comparator.thenComparing(v -> TimeUnit.MILLISECONDS.convert(v));
			comparator = comparator.thenComparing(v -> TimeUnit.NANOSECONDS.convert(v));
			sorter = comparator;
		}
		durationStream = durationStream.sorted((v1, v2) -> -1 * sorter.compare(v1, v2));
		var largestDuration = durationStream.findFirst()
				.orElseThrow(() -> new IllegalArgumentException("duration required"));
		return getTimeUnit(largestDuration);
	}

	public static Number toMillis(Duration duration) {
		Objects.requireNonNull(duration);
		if (duration.compareTo(MAX_BY_NANOS) >= 0)
			return duration.toMillis();
		var nanos = duration.toNanos();
		if (nanos <= Double.MAX_VALUE)
			return ((double) nanos) / NANOS_PER_MILLI;
		return new BigDecimal(nanos).divide(new BigDecimal(NANOS_PER_MILLI));
	}

	private static Stream<Duration> stream(Duration duration, Duration... durations) {
		return Stream.concat(Stream.ofNullable(duration), Stream.ofNullable(durations).flatMap(Stream::of))
				.filter(Objects::nonNull);
	}

	private static ChronoUnit toChronoUnit(TimeUnit unit) {
		switch (unit) {
		case NANOSECONDS:
			return ChronoUnit.NANOS;
		case MICROSECONDS:
			return ChronoUnit.MICROS;
		case MILLISECONDS:
			return ChronoUnit.MILLIS;
		case SECONDS:
			return ChronoUnit.SECONDS;
		case MINUTES:
			return ChronoUnit.MINUTES;
		case HOURS:
			return ChronoUnit.HOURS;
		case DAYS:
			return ChronoUnit.DAYS;
		default:
			throw new IllegalArgumentException("Unknown TimeUnit constant");
		}
	}

	public static void main(String[] args) {
		System.out.println(Durations.max(Duration.ofHours(1), Duration.ofDays(1), Duration.ofSeconds(2)));
		System.out.println(Durations.min(Duration.ofHours(1), Duration.ofDays(1), Duration.ofSeconds(2)));
		var d1 = Duration.ofSeconds(10);
		var d2 = Duration.ofSeconds(9);
		System.out.println(d1.compareTo(d2) > 0);
		System.out.println(d2.compareTo(d1) > 0);
		System.out.println(Durations.isMax(Durations.max()));
		System.out.println(Durations.isMax(Durations.max().minusNanos(1)));
		System.out.println(Durations.isMax(Durations.max().minusNanos(1).plusNanos(1)));
		System.out.println(Durations.isMax(Duration.ofMillis(Long.MAX_VALUE)));
	}

}
