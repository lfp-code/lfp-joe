package com.lfp.joe.core.function;

import java.util.Date;
import java.util.Map.Entry;

import javax.annotation.Nullable;

import org.immutables.value.Value;

import com.lfp.joe.core.config.ValueLFP;

@Value.Immutable
@ValueLFP.Style
abstract class AbstractImmutableEntry<K, V> implements Entry<K, V> {

	@Value.Parameter(order = 0)
	@Nullable
	@Override
	public abstract K getKey();

	@Value.Parameter(order = 1)
	@Nullable
	@Override
	public abstract V getValue();

	@Deprecated
	@Override
	public V setValue(V value) {
		throw new UnsupportedOperationException();
	}

	public ImmutableEntry<V, K> invert() {
		return ImmutableEntry.of(getValue(), getKey());
	}

	public static <K, V> ImmutableEntry.Builder<K, V> builder(K key) {
		return builder(key, null);
	}

	public static <K, V> ImmutableEntry.Builder<K, V> builder(K key, V value) {
		return ImmutableEntry.<K, V>builder().key(key).value(value);
	}

	public static <K, V> ImmutableEntry<K, V> of(K key) {
		return ImmutableEntry.of(key, null);
	}

	public static void main(String[] args) {
		var eq = ImmutableEntry.of(new Date(0), null).equals(ImmutableEntry.of(new Date(0), null));
		System.out.println(eq);
	}

}
