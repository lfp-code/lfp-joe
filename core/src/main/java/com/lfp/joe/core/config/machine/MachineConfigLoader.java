package com.lfp.joe.core.config.machine;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Spliterators;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.lfp.joe.core.function.Once.SupplierOnce;

public abstract class MachineConfigLoader<X> implements Supplier<X> {

	private static final File TEMP_CONFIG = new File(
			Stream.of("temp", "config").collect(Collectors.joining(File.separator)));
	private static final SupplierOnce<ThreadFactory> DEFAULT_THREAD_FACOTRY_ONCE = SupplierOnce.of(() -> {
		var defaultThreadFactory = Executors.defaultThreadFactory();
		return r -> {
			var thread = defaultThreadFactory.newThread(r);
			var threadName = String.format("%s[%s]", MachineConfigLoader.class.getSimpleName(), thread.getName());
			thread.setName(threadName);
			return thread;
		};
	});

	private final Object valueMutex = new Object();
	private Optional<X> valueOp;

	@Override
	public X get() {
		if (valueOp == null)
			synchronized (valueMutex) {
				if (valueOp == null) {
					valueOp = loadStream().findFirst();
					logLoad(valueOp.orElse(null));
				}
			}
		return valueOp.orElse(null);
	}

	protected Stream<X> loadStream() {
		var loaderStream = stream(loaders()).filter(Objects::nonNull);
		Stream<X> loadStream = loaderStream.map(v -> {
			try {
				return v.call();
			} catch (Throwable t) {
				return null;
			}
		}).filter(Objects::nonNull).map(v -> v);
		loadStream = loadStream(loadStream);
		return loadStream == null ? Stream.empty() : loadStream;
	}

	protected Stream<X> loadStream(Stream<X> stream) {
		return stream;
	}

	protected abstract Iterable<? extends Callable<? extends X>> loaders();

	protected void logLoad(X value) {
		logSystemOut("loaded:%s", value);
	}

	protected void logSystemOut(String template, Object... arguments) {
		logSystem(System.out, template, arguments);
	}

	protected void logSystemErr(String template, Object... arguments) {
		logSystem(System.err, template, arguments);
	}

	protected void logSystem(PrintStream printStream, String template, Object... arguments) {
		String out;
		if (arguments == null || arguments.length == 0)
			out = template;
		else
			out = String.format(template, arguments);
		out = this.getClass().getName() + " - " + out;
		printStream.println(out);
	}

	protected ExecutorService executorService(int nThreads) {
		return Executors.newFixedThreadPool(nThreads, r -> {
			var thread = DEFAULT_THREAD_FACOTRY_ONCE.get().newThread(r);
			thread.setName(String.format("%s[%s]", this.getClass().getSimpleName(), thread.getName()));
			return thread;
		});
	}

	protected static Stream<String> execLinesOut(String... commands) throws IOException {
		return execLines(commands).filter(v -> !v.getKey()).map(Entry::getValue);
	}

	protected static Stream<String> execLinesErr(String... commands) throws IOException {
		return execLines(commands).filter(v -> v.getKey()).map(Entry::getValue);
	}

	protected static Stream<Entry<Boolean, String>> execLines(String... commands) throws IOException {
		if (commands == null || commands.length == 0)
			return Stream.empty();
		List<String> lineList = catFile(commands);
		if (lineList != null)
			return lineList.stream().map(v -> Map.entry(false, v));
		List<List<String>> commandLists = new ArrayList<>();
		commandLists.add(Arrays.asList(commands));
		String commandsJoined = Stream.of(commands).collect(Collectors.joining(" "));
		commandLists.add(Arrays.asList("sh", "-c", commandsJoined));
		commandLists.add(Arrays.asList("cmd.exe", "/c", commandsJoined));
		var successFound = new AtomicBoolean();
		Stream<Stream<Entry<Boolean, String>>> estreams = commandLists.stream().map(commandList -> {
			if (successFound.get())
				return null;
			List<String> out;
			List<String> err;
			int exitValue;
			try {
				var process = new ProcessBuilder(commandList).start();
				out = lines(process.getInputStream()).collect(Collectors.toList());
				err = lines(process.getErrorStream()).collect(Collectors.toList());
				exitValue = process.waitFor();
			} catch (Exception e) {
				// suppress
				return Stream.empty();
			}
			if (exitValue == 0 && out.stream().anyMatch(v -> !v.isBlank()))
				successFound.set(true);
			return Stream.concat(out.stream().map(v -> Map.entry(false, v)), err.stream().map(v -> Map.entry(true, v)));
		});
		estreams = estreams.takeWhile(Objects::nonNull);
		return estreams.flatMap(Function.identity());
	}

	private static List<String> catFile(String... commands) {
		if (commands.length != 2)
			return null;
		boolean isCat = "cat".equals(commands[0])
				|| ("type".equalsIgnoreCase(commands[0]) || "type.exe".equalsIgnoreCase(commands[0]));
		if (!isCat)
			return null;
		try {
			var file = Optional.ofNullable(commands[1]).map(File::new).orElse(null);
			if (file == null)
				return null;
			if (!file.exists() || !file.canRead())
				return null;
			return Files.readAllLines(file.toPath());
		} catch (Exception e) {
			// suppress
		}
		return null;
	}

	protected static String absolutePath(String path) {
		if (path == null)
			return null;
		if (path.isBlank())
			return path;
		for (var file : Arrays.asList(new File(path), new File(TEMP_CONFIG, path))) {
			if (file.exists())
				return file.getAbsolutePath();
		}
		return path;
	}

	protected static <U> Stream<U> stream(Iterable<? extends U> iterable) {
		Stream<? extends U> stream;
		if (iterable instanceof Collection)
			stream = ((Collection<? extends U>) iterable).stream();
		else
			stream = Optional.ofNullable(iterable)
					.map(Iterable::spliterator)
					.map(v -> StreamSupport.stream(v, false))
					.orElseGet(Stream::empty);
		return stream.map(v -> v);
	}

	protected static <U> Stream<U> stream(Iterator<? extends U> iterator) {
		return Optional.ofNullable(iterator)
				.map(v -> Spliterators.spliterator(v, Long.MAX_VALUE, 0))
				.map(v -> StreamSupport.stream(v, false))
				.orElseGet(Stream::empty)
				.map(v -> v);
	}

	protected static <U> Function<U, Stream<U>> nonNullOrBlankFlatMap() {
		return v -> {
			if (v == null)
				return Stream.empty();
			if (v instanceof CharSequence && v.toString().isBlank())
				return Stream.empty();
			return Stream.of(v);
		};
	}

	protected static Function<String, Stream<Integer>> parseIntFlatMap() {
		return v -> Stream.ofNullable(parseInt(v));
	}

	protected static Integer parseInt(String input) {
		if (input == null)
			return null;
		input = input.trim();
		if (input.isEmpty())
			return null;
		if (input.chars().anyMatch(v -> !Character.isDigit(v)))
			return null;
		try {
			return Integer.parseInt(input);
		} catch (NumberFormatException e) {
			return null;
		}
	}

	private static Stream<String> lines(InputStream is) {
		return is != null ? new BufferedReader(new InputStreamReader(is)).lines() : Stream.empty();
	}

}
