package com.lfp.joe.core.process;

import java.util.Objects;
import java.util.concurrent.ThreadFactory;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.immutables.value.Value;

import com.lfp.joe.core.config.ValueLFP;

@Value.Immutable
@ValueLFP.Style
abstract class AbstractThreadRenamer implements Function<Runnable, Runnable>, Consumer<Thread> {

	@Value.Parameter(order = 0)
	abstract String name();

	@Value.Default
	boolean replace() {
		return false;
	}

	@Override
	public void accept(Thread thread) {
		acceptInternal(thread);
	}

	@Override
	public Runnable apply(Runnable runnable) {
		return () -> {
			try (var r = acceptInternal(Thread.currentThread())) {
				runnable.run();
			}
		};
	}

	public <Y> Supplier<Y> apply(Supplier<Y> supplier) {
		return () -> {
			try (var r = acceptInternal(Thread.currentThread())) {
				return supplier.get();
			}
		};
	}

	public <X, Y> Function<X, Y> apply(Function<X, Y> function) {
		return input -> {
			try (var r = acceptInternal(Thread.currentThread())) {
				return function.apply(input);
			}
		};
	}

	public ThreadFactory apply(ThreadFactory threadFactory) {
		return apply(threadFactory, null);
	}

	public ThreadFactory apply(ThreadFactory threadFactory, Consumer<? super Thread> threadModifier) {
		Objects.requireNonNull(threadFactory);
		return r -> {
			var thread = threadFactory.newThread(r);
			accept(thread);
			if (threadModifier != null)
				threadModifier.accept(thread);
			return thread;
		};
	}

	private Registration acceptInternal(Thread thread) {
		Objects.requireNonNull(thread);
		var originalThreadName = thread.getName();
		if (replace() || originalThreadName == null)
			thread.setName(name());
		else
			thread.setName(name() + '[' + originalThreadName + ']');
		return () -> thread.setName(originalThreadName);
	}

	private static interface Registration extends AutoCloseable {

		@Override
		void close();
	}
}
