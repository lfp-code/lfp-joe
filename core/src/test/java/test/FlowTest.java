package test;

import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;
import java.util.concurrent.SubmissionPublisher;

public class FlowTest {

	public static void main(String[] args) {
		var publisher = new SubmissionPublisher<String>(Runnable::run, Integer.MAX_VALUE);
		publisher.subscribe(new Subscriber<String>() {

			private Subscription subscription;

			@Override
			public void onSubscribe(Subscription subscription) {
				boolean error = false;
				if (error)
					throw new RuntimeException("error:onSubscribe");
				this.subscription = subscription;
				this.subscription.request(1);
			}

			@Override
			public void onNext(String item) {
				boolean error = true;
				if (error)
					throw new RuntimeException("error:onNext");
				System.out.println(String.format("onNext:%s", item));

			}

			@Override
			public void onError(Throwable throwable) {
				boolean error = false;
				if (error)
					throw new RuntimeException("error:onError");
				System.err.println("onError");
				throwable.printStackTrace();
			}

			@Override
			public void onComplete() {
				System.out.println("onComplete");

			}
		});
		System.out.println("starting:" + publisher.getNumberOfSubscribers());
		publisher.submit("test msg");
		publisher.submit("test msg");
		System.out.println("done:" + publisher.getNumberOfSubscribers());
	}

}
