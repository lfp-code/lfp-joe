package test;

import com.lfp.joe.core.function.Scrapable;

public class ScrapableTest {

	public static void main(String[] args) {
		var scrapable = Scrapable.create();
		for (int i = 0; i < 10; i++) {
			var index = i;
			scrapable.onScrap(() -> {
				if (index > 6)
					throw new RuntimeException("shit " + index);
				System.out.println(index);
			});
		}
		scrapable.scrap();
	}
}
