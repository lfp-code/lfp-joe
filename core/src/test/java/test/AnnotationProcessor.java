package test;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AnnotationProcessor {
	private static final String REGISTRATION_TEMPLATE = "package {0};\n" + "\n"
			+ "import de.adito.picoservice.PicoRegistration;\nimport java.util.Optional;\nimport java.util.concurrent.Callable;\n"
			+ "\n" + "import {4};\n" + "\n"
			+ "@Generated(value = \"de.adito.picoservice.processor.AnnotationProcessorPico\", date = \"{3}\")\n"
			+ "public class {1} implements PicoRegistration\n" + "'{'\n" + "  @Override\n"
			+ "  public Class<? extends {2}> getAnnotatedClass()\n" + "  '{'\n" + "    return {2}.class;\n" + "  '}'\n"
			+ "'}'";

	public static void main(String[] args) {
		System.out.println(REGISTRATION_TEMPLATE);
		String date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ").format(new Date());
		String importString = 11 >= 9 ? "javax.annotation.processing.Generated" : "javax.annotation.Generated";
		String content = MessageFormat.format(REGISTRATION_TEMPLATE, "com.test.package", "DateService", "Date", date,
				importString);
		System.out.println(content);
	}

}
