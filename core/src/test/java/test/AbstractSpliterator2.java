package test;

import java.util.Iterator;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;

import com.lfp.joe.core.function.Muto;

public abstract class AbstractSpliterator2<T> implements Spliterator<T> {
	static final int BATCH_UNIT = 1 << 10; // batch array size increment
	static final int MAX_BATCH = 1 << 25; // max batch array size;
	private final int characteristics;
	private int batch; // batch size for splits
	private long estimateSizeOffset;

	/**
	 * Creates a spliterator reporting the given estimated size and characteristics.
	 *
	 * @param est             the estimated size of this spliterator if known,
	 *                        otherwise {@code Long.MAX_VALUE}.
	 * @param characteristics properties of this spliterator's source or elements.
	 *                        If {@code SIZED} is reported then this spliterator
	 *                        will additionally report {@code SUBSIZED}.
	 */
	protected AbstractSpliterator2(int characteristics) {
		this.characteristics = ((characteristics & Spliterator.SIZED) != 0) ? characteristics | Spliterator.SUBSIZED
				: characteristics;
	}

	/**
	 * {@inheritDoc}
	 **/
	@Override
	public boolean tryAdvance(Consumer<? super T> consumer) {
		Action<? super T> action;
		if (consumer == null)
			action = null;
		else if (consumer instanceof Action)
			action = (Action<? super T>) consumer;
		else
			action = consumer::accept;
		return tryAdvanceInternal(action);
	}

	/**
	 * {@inheritDoc}
	 *
	 * This implementation permits limited parallelism.
	 */
	@Override
	public Spliterator<T> trySplit() {
		/*
		 * Split into arrays of arithmetically increasing batch sizes. This will only
		 * improve parallel performance if per-element Consumer actions are more costly
		 * than transferring them into an array. The use of an arithmetic progression in
		 * split sizes provides overhead vs parallelism bounds that do not particularly
		 * favor or penalize cases of lightweight vs heavyweight element operations,
		 * across combinations of #elements vs #cores, whether or not either are known.
		 * We generate O(sqrt(#elements)) splits, allowing O(sqrt(#cores)) potential
		 * speedup.
		 */
		Muto<T> holder = Muto.create();
		long est = estimateSize();
		long s = est;
		if (s > 1 && tryAdvance(holder)) {
			int n = batch + BATCH_UNIT;
			if (n > s)
				n = (int) s;
			if (n > MAX_BATCH)
				n = MAX_BATCH;
			Object[] a = new Object[n];
			int j = 0;
			do {
				a[j] = holder.get();
			} while (++j < n && tryAdvance(holder));
			batch = j;
			estimateSizeOffset += j;
			return Spliterators.spliterator(a, 0, j, characteristics());

		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @implSpec This implementation returns the estimated size as reported when
	 *           created and, if the estimate size is known, decreases in size when
	 *           split.
	 */
	@Override
	public long estimateSize() {
		var est = Optional.ofNullable(estimateSizeInternal()).filter(v -> v >= 0).orElse(Long.MAX_VALUE);
		if (est != Long.MAX_VALUE)
			est -= estimateSizeOffset;
		return est;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @implSpec This implementation returns the characteristics as reported when
	 *           created.
	 */
	@Override
	public int characteristics() {
		return characteristics;
	}

	protected abstract boolean tryAdvanceInternal(Action<? super T> action);

	protected abstract Long estimateSizeInternal();

	public static interface Action<T> extends Consumer<T> {

		default boolean advance(T value) {
			this.accept(value);
			return true;
		}

		default boolean end() {
			return false;
		}

	}

	public static abstract class Multi<T> extends AbstractSpliterator2<T> {

		private BlockingQueue<Optional<T>> queue;
		private boolean ended;

		protected Multi(int characteristics) {
			super(characteristics);
		}

		@Override
		protected boolean tryAdvanceInternal(Action<? super T> action) {
			if (!ended) {
				boolean advance = tryAdvanceMultiInternal(value -> {
					if (queue == null)
						synchronized (this) {
							if (queue == null)
								queue = createQueue();
						}
					queue.add(Optional.ofNullable(value));
				});
				if (!advance)
					ended = true;
			}
			var value = queue.poll();
			if (value == null)
				return false;
			action.accept(value.orElse(null));
			return true;
		}

		protected BlockingQueue<Optional<T>> createQueue() {
			return new LinkedBlockingQueue<>();
		}

		protected abstract boolean tryAdvanceMultiInternal(MultiAction<? super T> action);

		public static interface MultiAction<T> extends Action<T> {

			@SuppressWarnings("unchecked")
			default boolean advance(T... values) {
				if (values != null)
					for (var value : values)
						accept(value);
				return true;
			}

			default boolean advance(Iterator<? extends T> values) {
				if (values != null)
					values.forEachRemaining(this::accept);
				return true;
			}

			default boolean advance(Iterable<? extends T> values) {
				if (values != null)
					values.forEach(this::accept);
				return true;
			}

			@SuppressWarnings("unchecked")
			default boolean end(T... values) {
				if (values != null)
					for (var value : values)
						accept(value);
				return false;
			}

			default boolean end(Iterator<? extends T> values) {
				if (values != null)
					values.forEachRemaining(this::accept);
				return false;
			}

			default boolean end(Iterable<? extends T> values) {
				if (values != null)
					values.forEach(this::accept);
				return false;
			}

		}
	}
}
