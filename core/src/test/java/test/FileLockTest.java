package test;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.Files;
import java.time.Duration;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import java.util.stream.Stream;

import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.io.FileExt;
import com.lfp.joe.core.lock.FileLocks;

public class FileLockTest {

	private static final Map<String, CompletableFuture<Void>> KEY_LOCK = new ConcurrentHashMap<>();

	public static void main(String[] args) throws InterruptedException, IOException {
		var file = new FileExt("temp/" + UUID.randomUUID() + ".bin");
		Files.writeString(file.toPath(), new Date().toString());
		file.getParentFile().mkdirs();
		var runAt = System.nanoTime() + Duration.ofSeconds(2).toNanos();
		for (int i = 0; i < 10; i++) {
			var index = i;
			new Thread(() -> {
				try {
					System.out.println("starting thread");
					Throws.unchecked(() -> Thread.sleep(Duration.ofNanos(runAt - System.nanoTime()).toMillis()));
					// lockTest0(file);
					// lockTest1(file);
					// lockTest2(file);
					lockTest3(file, index);
				} catch (Throwable t) {
					t.printStackTrace();
				}
			}).start();
		}
		System.err.println("scheduled");
		Thread.currentThread().join();
	}

	private static void lockTest0(FileExt file) throws IOException {
		try (var raf = new RandomAccessFile(file, "rw");
				var channel = raf.getChannel();
				var fileLock = channel.lock();) {
			System.out.println(channel.size());
		}
	}

	private static void lockTest1(FileExt file) {
		try {
			FileLocks.write(file, (fc, os) -> {
				System.out.println(fc.size());
			});
		} catch (IOException e) {
			throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e) : new RuntimeException(e);
		}

	}

	private static void lockTest2(FileExt file) {
		System.out.println("loading:" + Thread.currentThread().getName());
		Consumer<Future<?>> removeTask = removeFuture -> {
			KEY_LOCK.computeIfPresent(file.getUniquePath(), (nil, f) -> removeFuture == f ? null : f);
		};
		var future = new CompletableFuture<Void>();
		{
			var runningFuture = KEY_LOCK.putIfAbsent(file.getUniquePath(), future);
			if (runningFuture != null) {
				runningFuture.whenComplete((v, t) -> {
					removeTask.accept(runningFuture);
					lockTest2(file);
				});
				return;
			}
		}
		future.whenComplete((v, t) -> {
			removeTask.accept(future);
		});
		RandomAccessFile raf = null;
		FileChannel fileChannel = null;
		FileLock fileLock = null;
		boolean disableClose = false;
		try {
			raf = new RandomAccessFile(file, "rw");
			fileChannel = raf.getChannel();
			fileLock = fileChannel.lock();
			lockTest2Process(future, raf, fileChannel, fileLock);
			disableClose = true;
		} catch (Throwable t) {
			t.printStackTrace();
		} finally {
			if (!disableClose)
				closeAll(fileLock, fileChannel, raf);
		}
	}

	private static void lockTest2Process(CompletableFuture<Void> future, RandomAccessFile raf, FileChannel fileChannel,
			FileLock fileLock) {
		new Thread(() -> {
			Throwable failure = null;
			try {
				Thread.sleep(1_000);
				System.out.println(fileChannel.size());
			} catch (Throwable t) {
				failure = t;
			} finally {
				closeAll(fileLock, fileChannel, raf);
			}
			if (failure != null)
				future.completeExceptionally(failure);
			else
				future.complete(null);
		}).start();

	}

	private static void lockTest3(FileExt file, int index) throws IOException, InterruptedException {
		if (index == 0) {
			for (int i = 0; i < 20; i++) {
				if (i > 0)
					Thread.sleep(250);
				try (var raf = new RandomAccessFile(file, "rw");
						var channel = raf.getChannel();
						var fileLock = channel.lock();) {
					System.out.println("write:" + channel.size());
				}
			}
		} else if (index == 1) {
			for (int i = 0; i < 20; i++) {
				if (i > 0)
					Thread.sleep(250);
				try (var raf = new RandomAccessFile(file, "r");
						var channel = raf.getChannel();
						var fileLock = channel.lock();) {
					System.out.println("read:" + channel.size());
				}
			}
		}

	}

	private static void closeAll(AutoCloseable... autoCloseables) {
		Stream.of(autoCloseables).filter(Objects::nonNull).forEach(Throws.consumerUnchecked(AutoCloseable::close));

	}
}
