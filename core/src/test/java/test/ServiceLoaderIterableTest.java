package test;

import com.lfp.joe.core.classpath.ServiceLoaderIterable;

import de.adito.picoservice.IPicoRegistration;

public class ServiceLoaderIterableTest {

	public static void main(String[] args) {
		var iterable = new ServiceLoaderIterable<>(IPicoRegistration.class);
		for (int i = 0; i < 10; i++)
			iterable.forEach(v -> {
				System.out.println(v);
			});
	}
}
