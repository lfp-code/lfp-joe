package test;

import org.aeonbits.owner.Config;

public interface OwnerTestConfig extends Config {

	@DefaultValue("100")
	int oneHundred();

	default Integer sum(Integer a, Integer b) {
		return Integer.sum(a, b);
	}

}
