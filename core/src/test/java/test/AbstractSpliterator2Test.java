package test;

import java.util.Random;
import java.util.Spliterators;
import java.util.stream.IntStream;

public class AbstractSpliterator2Test {

	public static void main(String[] args) {
		int min = 10;
		int max = 25;
		Random random = new Random();
		var spliterator = new AbstractSpliterator2.Multi<String>(0) {

			int passIndex = -1;

			@Override
			protected boolean tryAdvanceMultiInternal(MultiAction<? super String> action) {
				passIndex++;
				var messageIterator = IntStream.range(0, random.nextInt(max - min) + min)
						.mapToObj(v -> String.format("%s-message-%s", passIndex, v)).iterator();
				if (passIndex < 10)
					return action.advance(messageIterator);
				return action.end(messageIterator);
			}

			@Override
			protected Long estimateSizeInternal() {
				return null;
			}
		};
		var iter = Spliterators.iterator(spliterator);
		while (iter.hasNext()) {
			System.out.println(iter.next());
		}
	}
}
