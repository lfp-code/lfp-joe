package test;

import com.lfp.joe.core.config.MachineConfig;

public class CorePrint {

	public static void main(String[] args) {
		System.out.println(Runtime.getRuntime().availableProcessors());
		System.out.println(MachineConfig.logicalCoreCount());
	}
}
