package test;

import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

import com.lfp.joe.core.function.Throws;


public class CompletableFutureTest {

	public static void main(String[] args) {
		var cf0 = new CompletableFuture<String>();
		var cf1 = new CompletableFuture<String>();
		new Thread(() -> {
			try {
				Thread.sleep(1_000);
			} catch (InterruptedException e) {
				throw Throws.unchecked(e);
			}
			cf0.complete("thread 0");
		}).start();
		new Thread(() -> {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				throw Throws.unchecked(e);
			}
			cf1.complete("thread 1");
		}).start();
		var cf = cf0.applyToEither(cf1, Function.identity());
		System.out.println(cf.join());
	}
}
