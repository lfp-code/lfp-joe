package test;

import java.lang.StackWalker.Option;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

public class FutureTaskTest {

	public static void main(String[] args) throws InterruptedException {
		var exec = Executors.newCachedThreadPool();
		{
			var ft = new FutureTaskExt<String>("set_result", () -> {
				Thread.sleep(5_000);
				return "callable_source";
			});
			exec.submit(ft);
			Thread.sleep(1_000);
			ft.set("set_source");
			await(ft);
		}
	}

	private static void await(Future<?> future) {
		try {
			future.get();
		} catch (Throwable t) {
			// supress
		}
	}

	private static class FutureTaskExt<V> extends FutureTask<V> {

		private String id;

		public FutureTaskExt(String id, Callable<V> callable) {
			super(callable);
			this.id = id;
		}

		@Override
		public void run() {
			try {
				System.out.println(String.format("%s: run start", id));
				super.run();
			} finally {
				System.out.println(String.format("%s: run finish", id));
			}
		}

		@Override
		public V get() throws InterruptedException, ExecutionException {
			try {
				System.out.println(String.format("%s: get start", id));
				return super.get();
			} finally {
				System.out.println(String.format("%s: get finish", id));
			}
		}

		@Override
		protected void set(V v) {
			super.set(v);
		}

		@Override
		protected void setException(Throwable t) {
			super.setException(t);

		}

		@Override
		protected void done() {
			var sfOp = StackWalker.getInstance(Option.RETAIN_CLASS_REFERENCE).walk(stream -> {
				stream = stream.filter(sf -> {
					if ("set".equals(sf.getMethodName()))
						return true;
					if ("setException".equals(sf.getMethodName()))
						return true;
					return false;
				});
				return stream.findFirst();
			});
			V result;
			Throwable failure;
			try {
				result = super.get();
				failure = null;
			} catch (Throwable t) {
				result = null;
				failure = t;
			}
			System.out.println(String.format("%s: done method:%s result:%s failure:%s", id,
					sfOp.map(v -> v.getMethodName()).orElse(null), result, failure));
		}
	}

}
