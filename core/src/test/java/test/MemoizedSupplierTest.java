package test;

import java.time.Duration;
import java.util.concurrent.atomic.AtomicLong;

import com.lfp.joe.core.function.MemoizedSupplier;

public class MemoizedSupplierTest {

	private static MemoizedSupplier<Long> sup;

	public static void main(String[] args) throws InterruptedException {
		var counter = new AtomicLong(0);
		sup = new MemoizedSupplier.Impl<Long>(() -> {
			if (counter.incrementAndGet() > 3)
				throw new RuntimeException("error");
			System.out.println("load");
			return System.currentTimeMillis();
		});
		System.out.println(sup.getIfLoaded());
		System.out.println(sup.get());
		Thread.sleep(1);
		System.out.println(sup.get(Duration.ZERO));
		System.out.println(sup.get());
		System.out.println(sup.get(Duration.ofMillis(10)));
		Thread.sleep(1000);
		System.out.println(sup.get(Duration.ofMillis(999)));
		sup.clear();
		System.out.println(sup.get());
	}

}
