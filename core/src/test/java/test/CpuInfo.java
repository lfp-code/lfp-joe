package test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import com.lfp.joe.core.config.MachineConfig;

public class CpuInfo {

	public static void main(String[] args) {
		System.out.println(System.getenv("NUMBER_OF_PROCESSORS"));
		System.out.println(MachineConfig.logicalCoreCount());
	}
}
