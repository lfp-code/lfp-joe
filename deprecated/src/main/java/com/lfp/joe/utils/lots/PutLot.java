package com.lfp.joe.utils.lots;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.lot.AbstractLot;

public class PutLot<T> extends AbstractLot<T> {

	public static <T> PutLot<T> create() {
		return new PutLot<>();
	}

	private final BlockingQueue<Optional<Optional<T>>> queue;

	public PutLot() {
		this(new LinkedBlockingQueue<>());
	}

	protected PutLot(BlockingQueue<Optional<Optional<T>>> queue) {
		super();
		this.queue = Objects.requireNonNull(queue);
		this.onScrap(() -> {
			putInternal(Optional.empty(), true);
		});
	}

	public boolean put(T value) {
		return putInternal(Optional.of(Optional.ofNullable(value)), false);
	}

	protected boolean putInternal(Optional<Optional<T>> valueOpOp, boolean skipScrappedCheck) {
		Objects.requireNonNull(valueOpOp);
		if (skipScrappedCheck || !isScrapped()) {
			Throws.unchecked(() -> queue.put(valueOpOp));
			return true;
		}
		return false;

	}

	@Override
	protected T computeNext() {
		synchronized (queue) {
			Optional<Optional<T>> valueOpOp;
			if (this.isEnded())
				valueOpOp = queue.poll();
			else
				valueOpOp = Throws.unchecked(() -> queue.take());
			if (valueOpOp == null || valueOpOp.isEmpty()) {
				queue.clear();
				return this.end();
			} else
				return valueOpOp.get().orElse(null);
		}
	}



}
