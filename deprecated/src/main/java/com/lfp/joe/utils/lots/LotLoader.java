package com.lfp.joe.utils.lots;

import com.lfp.joe.core.lot.AbstractLot;

@SuppressWarnings("unchecked")
public interface LotLoader<T> {

	long getIndex();

	T end();

	T end(T... endValues);

	T end(Iterable<? extends T> endValues);

	public static abstract class Abs<T> extends AbstractLot.Indexed<T> implements LotLoader<T> {

		@Override
		public T end() {
			return super.end();
		}

		@Override
		public T end(T... endValues) {
			return super.end(endValues);
		}

		@Override
		public T end(Iterable<? extends T> endValues) {
			return super.end(endValues);
		}

	}
}