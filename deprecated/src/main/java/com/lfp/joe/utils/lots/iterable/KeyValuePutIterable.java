package com.lfp.joe.utils.lots.iterable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.lfp.joe.core.function.ImmutableEntry;
import com.lfp.joe.core.lot.AbstractLot;

public class KeyValuePutIterable<K, V> implements Iterable<ImmutableEntry<K, V>> {

	private final List<K> keyOrder = new ArrayList<>();
	private final Map<Integer, V> mapping = new ConcurrentHashMap<>();

	@Override
	public Iterator<ImmutableEntry<K, V>> iterator() {
		return new AbstractLot.Indexed<>() {

			@Override
			protected ImmutableEntry<K, V> computeNext(long index) {
				if (keyOrder.size() <= index)
					return this.end();
				K key = keyOrder.get((int) index);
				return ImmutableEntry.of(key, mapping.get(toInternalKey(key)));
			}

		};
	}

	private Integer toInternalKey(K key) {
		return key == null ? null : key.hashCode();
	}

	public int size() {
		return Math.max(mapping.size(), keyOrder.size());
	}

	public void put(K key, V value) {
		mapping.compute(toInternalKey(key), (k, v) -> {
			keyOrder.add(key);
			return value;
		});
	}

	public V get(K key) {
		return mapping.get(toInternalKey(key));
	}

	public boolean containsKey(K key) {
		return mapping.containsKey(toInternalKey(key));
	}

}