package com.lfp.joe.core.lot;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.Spliterator;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.lfp.joe.core.function.Muto;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.producer.AbstractProducer;

public abstract class AbstractLot<T> implements Lot<T>, Scrapable {

	private static final int CHARACTERISTICS_DEFAULT = 0;
	private final Scrapable scrapable = Scrapable.create();
	private final OptionalLong estimatedSize;
	private final int characteristics;
	private final Object endValuesMutex = new Object();
	private List<T> endValues;
	protected final AbstractProducer<T> producer;

	public AbstractLot() {
		this(null, null);
	}

	public AbstractLot(Long estimatedSize, Integer characteristics) {
		this.estimatedSize = estimatedSize == null ? OptionalLong.empty() : OptionalLong.of(estimatedSize);
		this.characteristics = Optional.ofNullable(characteristics).orElse(CHARACTERISTICS_DEFAULT);
		this.producer = new AbstractProducer<T>() {

			@Override
			public boolean tryProduce(Consumer<? super T> action) {
				return tryProduceInternal(action);
			}
		};

	}

	@Override
	public void close() {
		this.scrap();
	}

	@Override
	public int characteristics() {
		return this.characteristics;
	}

	@Override
	public boolean hasNext() {
		return this.producer.peek() != null;
	}

	@Override
	public T next() {
		return this.producer.produce();
	}

	@Override
	public T peek() {
		var peekOp = this.producer.peek();
		return peekOp == null ? null : peekOp.orElse(null);
	}

	protected Stream<T> stream() {
		return this.producer.stream(estimateSize(), this.characteristics, false).onClose(this::scrap);
	}

	@Override
	public boolean tryAdvance(Consumer<? super T> action) {
		var nextOp = this.producer.tryProduce(true);
		if (nextOp == null)
			return false;
		action.accept(nextOp.orElse(null));
		return true;
	}

	@Override
	public Spliterator<T> trySplit() {
		return null;
	}

	@Override
	public long estimateSize() {
		var size = estimateSizeInternal();
		if (size == null || size < 0)
			return Long.MAX_VALUE;
		return size;
	}

	protected Long estimateSizeInternal() {
		if (this.estimatedSize.isPresent())
			return this.estimatedSize.getAsLong();
		return null;
	}

	protected T end() {
		end(List.of());
		return null;
	}

	protected boolean isEnded() {
		return endValues != null;
	}

	@SuppressWarnings("unchecked")
	protected T end(T... endValues) {
		return end(Optional.ofNullable(endValues).map(Arrays::asList).orElse(null));
	}

	protected T end(Iterable<? extends T> endValues) {
		var endValueList = Stream.ofNullable(endValues)
				.map(Iterable::spliterator)
				.flatMap(v -> StreamSupport.stream(v, false))
				.filter(Objects::nonNull)
				.<T>map(Function.identity())
				.collect(Collectors.toList());
		synchronized (endValuesMutex) {
			if (this.endValues == null)
				this.endValues = endValueList;
			else
				this.endValues.addAll(endValueList);
		}
		return null;
	}

	protected boolean tryProduceInternal(Consumer<? super T> action) {
		var next = computeNext();
		return Optional.ofNullable(this.endValues).map(values -> {
			while (!values.isEmpty())
				action.accept(values.remove(0));
			return false;
		}).orElseGet(() -> {
			action.accept(next);
			return true;
		});
	}

	protected T computeNext(Spliterator<? extends T> spliterator) {
		if (spliterator == null)
			return end();
		var nextRef = Muto.<T>create();
		if (!spliterator.tryAdvance(nextRef))
			return end();
		return nextRef.get();
	}

	protected abstract T computeNext();

	public static abstract class Indexed<T> extends AbstractLot<T> implements Lot.Indexed<T> {

		public Indexed() {
			super();
		}

		public Indexed(Long estimatedSize, Integer characteristics) {
			super(estimatedSize, characteristics);
		}

		@Override
		public long getIndex() {
			return this.producer.getRequestIndex();
		}

		@Override
		public long getExactSizeIfKnown() {
			var exactSizeOp = tryGetExactSize();
			if (exactSizeOp.isPresent())
				return exactSizeOp.getAsLong();
			return super.getExactSizeIfKnown();
		}

		@Override
		public long getExactOrEstimateSize() {
			var exactSizeOp = tryGetExactSize();
			if (exactSizeOp.isPresent())
				return exactSizeOp.getAsLong();
			return super.estimateSize();
		}

		@Override
		protected T computeNext() {
			return computeNext(this.producer.getRequestIndex());
		}

		protected OptionalLong tryGetExactSize() {
			if (!this.producer.isProduceComplete())
				return OptionalLong.empty();
			return OptionalLong.of(this.producer.getProduceCount());
		}

		protected abstract T computeNext(long index);

	}

	/*
	 * scrapable delegates
	 */

	@Override
	public boolean isScrapped() {
		return scrapable.isScrapped();
	}

	@Override
	public boolean scrap() {
		return scrapable.scrap();
	}

	@Override
	public Scrapable onScrap(Runnable runnable) {
		return scrapable.onScrap(runnable);
	}

	/*
	 * stream delegates
	 */

	@Override
	public boolean isParallel() {
		return stream().isParallel();
	}

	@Override
	public Stream<T> sequential() {
		return stream().sequential();
	}

	@Override
	public Stream<T> parallel() {
		return stream().parallel();
	}

	@Override
	public Stream<T> unordered() {
		return stream().unordered();
	}

	@Override
	public Stream<T> filter(Predicate<? super T> predicate) {
		return stream().filter(predicate);
	}

	@Override
	public <R> Stream<R> map(Function<? super T, ? extends R> mapper) {
		return stream().map(mapper);
	}

	@Override
	public IntStream mapToInt(ToIntFunction<? super T> mapper) {
		return stream().mapToInt(mapper);
	}

	@Override
	public LongStream mapToLong(ToLongFunction<? super T> mapper) {
		return stream().mapToLong(mapper);
	}

	@Override
	public DoubleStream mapToDouble(ToDoubleFunction<? super T> mapper) {
		return stream().mapToDouble(mapper);
	}

	@Override
	public <R> Stream<R> flatMap(Function<? super T, ? extends Stream<? extends R>> mapper) {
		return stream().flatMap(mapper);
	}

	@Override
	public IntStream flatMapToInt(Function<? super T, ? extends IntStream> mapper) {
		return stream().flatMapToInt(mapper);
	}

	@Override
	public LongStream flatMapToLong(Function<? super T, ? extends LongStream> mapper) {
		return stream().flatMapToLong(mapper);
	}

	@Override
	public DoubleStream flatMapToDouble(Function<? super T, ? extends DoubleStream> mapper) {
		return stream().flatMapToDouble(mapper);
	}

	@Override
	public Stream<T> distinct() {
		return stream().distinct();
	}

	@Override
	public Stream<T> sorted() {
		return stream().sorted();
	}

	@Override
	public Stream<T> sorted(Comparator<? super T> comparator) {
		return stream().sorted(comparator);
	}

	@Override
	public Stream<T> peek(Consumer<? super T> action) {
		return stream().peek(action);
	}

	@Override
	public Stream<T> limit(long maxSize) {
		return stream().limit(maxSize);
	}

	@Override
	public Stream<T> skip(long n) {
		return stream().skip(n);
	}

	@Override
	public Stream<T> takeWhile(Predicate<? super T> predicate) {
		return stream().takeWhile(predicate);
	}

	@Override
	public Stream<T> dropWhile(Predicate<? super T> predicate) {
		return stream().dropWhile(predicate);
	}

	@Override
	public void forEachOrdered(Consumer<? super T> action) {
		stream().forEachOrdered(action);
	}

	@Override
	public Object[] toArray() {
		return stream().toArray();
	}

	@Override
	public <A> A[] toArray(IntFunction<A[]> generator) {
		return stream().toArray(generator);
	}

	@Override
	public T reduce(T identity, BinaryOperator<T> accumulator) {
		return stream().reduce(identity, accumulator);
	}

	@Override
	public Optional<T> reduce(BinaryOperator<T> accumulator) {
		return stream().reduce(accumulator);
	}

	@Override
	public <U> U reduce(U identity, BiFunction<U, ? super T, U> accumulator, BinaryOperator<U> combiner) {
		return stream().reduce(identity, accumulator, combiner);
	}

	@Override
	public <R> R collect(Supplier<R> supplier, BiConsumer<R, ? super T> accumulator, BiConsumer<R, R> combiner) {
		return stream().collect(supplier, accumulator, combiner);
	}

	@Override
	public <R, A> R collect(Collector<? super T, A, R> collector) {
		return stream().collect(collector);
	}

	@Override
	public Optional<T> min(Comparator<? super T> comparator) {
		return stream().min(comparator);
	}

	@Override
	public Optional<T> max(Comparator<? super T> comparator) {
		return stream().max(comparator);
	}

	@Override
	public long count() {
		return stream().count();
	}

	@Override
	public boolean anyMatch(Predicate<? super T> predicate) {
		return stream().anyMatch(predicate);
	}

	@Override
	public boolean allMatch(Predicate<? super T> predicate) {
		return stream().allMatch(predicate);
	}

	@Override
	public boolean noneMatch(Predicate<? super T> predicate) {
		return stream().noneMatch(predicate);
	}

	@Override
	public Optional<T> findFirst() {
		return stream().findFirst();
	}

	@Override
	public Optional<T> findAny() {
		return stream().findAny();
	}

}
