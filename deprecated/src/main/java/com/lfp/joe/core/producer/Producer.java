package com.lfp.joe.core.producer;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators.AbstractSpliterator;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.LongSupplier;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public interface Producer<T> extends Iterable<T> {

	long getRequestIndex();

	long getProduceCount();

	long getConsumeCount();

	Optional<T> peek();

	T produce();

	boolean tryProduce(Consumer<? super T> action);

	boolean isComplete();

	@Override
	default Iterator<T> iterator() {
		return new Iterator<T>() {

			@Override
			public boolean hasNext() {
				return peek() != null;
			}

			@Override
			public T next() {
				return produce();
			}
		};
	}

	default <X> X chain(Function<Producer<T>, X> chainFunction) {
		return Objects.requireNonNull(chainFunction).apply(this);
	}

	@Override
	default Spliterator<T> spliterator() {
		return spliterator(Long.MAX_VALUE, 0);
	}

	default Spliterator<T> spliterator(long estimatedSize, int characteristics) {
		return new AbstractSpliterator<T>(estimatedSize, characteristics) {

			@Override
			public boolean tryAdvance(Consumer<? super T> action) {
				return Producer.this.tryProduce(action);
			}
		};
	}

	default Spliterator<T> spliterator(LongSupplier estimatedSizeSupplier, int characteristics) {
		if (estimatedSizeSupplier == null)
			return spliterator(() -> Long.MAX_VALUE, characteristics);
		return new Spliterator<T>() {

			@Override
			public boolean tryAdvance(Consumer<? super T> action) {
				return Producer.this.tryProduce(action);
			}

			@Override
			public long estimateSize() {
				return estimatedSizeSupplier.getAsLong();
			}

			@Override
			public int characteristics() {
				return characteristics;
			}

			@Override
			public Spliterator<T> trySplit() {
				return null;
			}
		};
	}

	default Stream<T> stream() {
		return stream(Long.MAX_VALUE, 0, false);
	}

	default Stream<T> stream(long estimatedSize, int characteristics, boolean parallel) {
		return StreamSupport.stream(spliterator(estimatedSize, characteristics), parallel);
	}

	default Stream<T> stream(LongSupplier estimatedSizeSupplier, int characteristics, boolean parallel) {
		return StreamSupport.stream(spliterator(estimatedSizeSupplier, characteristics), parallel);
	}

	public static <T> Producer<T> empty() {
		return new Producer<T>() {

			private final AtomicLong requestIndex = new AtomicLong(-1);

			@Override
			public Optional<T> peek() {
				return null;
			}

			@Override
			public T produce() {
				throw new NoSuchElementException();
			}

			@Override
			public boolean tryProduce(Consumer<? super T> action) {
				requestIndex.incrementAndGet();
				return false;
			}

			@Override
			public long getRequestIndex() {
				return requestIndex.get();
			}

			@Override
			public long getProduceCount() {
				return 0;
			}

			@Override
			public long getConsumeCount() {
				return 0;
			}

			@Override
			public boolean isComplete() {
				return true;
			}

		};
	}

	public static <T> Producer<T> create(Predicate<Consumer<? super T>> actionPredicate) {
		return create(actionPredicate == null ? null : (p, action) -> actionPredicate.test(action));
	}

	public static <T> Producer<T> create(BiPredicate<Producer<T>, Consumer<? super T>> actionPredicate) {
		if (actionPredicate == null)
			return empty();
		return new AbstractProducer<T>() {

			@Override
			public boolean tryProduce(Consumer<? super T> action) {
				if (!actionPredicate.test(this, action))
					return false;
				else
					return true;
			}

		};
	}

}