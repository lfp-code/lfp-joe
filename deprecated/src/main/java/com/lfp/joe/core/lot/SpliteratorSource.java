package com.lfp.joe.core.lot;

import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.BaseStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.function.Throws;

public interface SpliteratorSource<T, I> extends Spliterator<T>, Scrapable {

	I getSource();

	boolean isLoaded();

	boolean isComplete();

	@SuppressWarnings("unchecked")
	public static <T, I extends Iterable<? extends T>> SpliteratorSource<T, I> create(I input) {
		if (CoreReflections.isLambdaClassType(Optional.ofNullable(input).map(Object::getClass).orElse(null)))
			return create(input, v -> {
				Iterator<? extends T> iterator;
				if (v == null)
					iterator = null;
				else if (v instanceof Iterator)
					iterator = (Iterator<? extends T>) v;
				else
					iterator = v.iterator();
				if (iterator == null)
					return null;
				return Spliterators.spliteratorUnknownSize(iterator, 0);
			});
		return create(input, v -> {
			if (v != null)
				return v.spliterator();
			return null;
		});
	}

	public static <T, I extends Iterator<? extends T>> SpliteratorSource<T, I> create(I input) {
		return create(input, v -> {
			if (v != null)
				return Spliterators.spliteratorUnknownSize(v, 0);
			return null;
		});
	}

	public static <T, I extends Spliterator<? extends T>> SpliteratorSource<T, I> create(I input) {
		return create(input, v -> {
			if (v != null)
				return v;
			return null;
		});
	}

	public static <T, I extends Stream<? extends T>> SpliteratorSource<T, I> create(I input) {
		return create(input, v -> {
			if (v != null)
				return v.spliterator();
			return null;
		});
	}

	public static <T, I> SpliteratorSource<T, I> create(I input, Function<I, Spliterator<? extends T>> mapper) {
		return new SpliteratorSource.Impl<T, I>(input, mapper);
	}

	public static class Impl<T, I> extends Scrapable.Impl implements SpliteratorSource<T, I> {

		private final Object spliteratorMutex = new Object();
		private Spliterator<T> spliterator;
		private final I source;
		private final Function<I, Spliterator<? extends T>> mapper;

		public Impl(I source, Function<I, Spliterator<? extends T>> mapper) {
			super();
			this.source = scrapSync(source);
			this.mapper = Objects.requireNonNull(mapper);
		}

		@Override
		public I getSource() {
			return source;
		}

		@Override
		public boolean isLoaded() {
			return spliterator != null;
		}

		@Override
		public boolean isComplete() {
			return Optional.ofNullable(spliterator).map(Spliterators.emptySpliterator()::equals).orElse(false);
		}

		@Override
		public boolean tryAdvance(Consumer<? super T> action) {
			if (isComplete())
				return false;
			if (!getSpliterator().tryAdvance(action)) {
				spliterator = Spliterators.emptySpliterator();
				return false;
			}
			return true;
		}

		@Override
		public Spliterator<T> trySplit() {
			return getSpliterator().trySplit();
		}

		@Override
		public long estimateSize() {
			return getSpliterator().estimateSize();
		}

		@Override
		public int characteristics() {
			return getSpliterator().characteristics();
		}

		@SuppressWarnings("unchecked")
		protected Spliterator<T> getSpliterator() {
			if (spliterator == null)
				synchronized (spliteratorMutex) {
					if (spliterator == null) {
						var s = (Spliterator<T>) this.mapper.apply(source);
						if (!Objects.equals(s, source))
							s = scrapSync(s);
						spliterator = Optional.ofNullable(s).orElseGet(Spliterators::emptySpliterator);
					}
				}
			return spliterator;
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		protected <X> X scrapSync(X input) {
			if (input == null || input == this)
				return input;
			else if (input instanceof BaseStream) {
				BaseStream stream = (BaseStream) input;
				input = (X) stream.onClose(this::close);
			} else
				CoreReflections.tryCast(input, AutoCloseable.class)
						.ifPresent(Throws.consumerUnchecked(AutoCloseable::close));
			return input;
		}

	}

	public static void main(String[] args) {
		var ss = SpliteratorSource.create((Iterable<Integer>) () -> IntStream.range(0, 10).boxed().iterator());
		StreamSupport.stream(ss, false).forEach(v -> {
			System.out.println(v);
		});
	}

}
