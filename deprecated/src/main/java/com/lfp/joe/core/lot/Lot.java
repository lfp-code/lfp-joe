package com.lfp.joe.core.lot;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Spliterator;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

import com.lfp.joe.core.function.Scrapable;

public interface Lot<T> extends Spliterator<T>, Iterator<T>, Iterable<T>, Stream<T>, Scrapable {

	T peek();

	@Override
	default Iterator<T> iterator() {
		return this;
	}

	@Override
	default Spliterator<T> spliterator() {
		return this;
	}

	@Override
	default void forEachRemaining(Consumer<? super T> action) {
		Spliterator.super.forEachRemaining(action);
	}

	@Override
	default void forEach(Consumer<? super T> action) {
		forEachRemaining(action);
	}

	@Override
	default Lot<T> onClose(Runnable closeHandler) {
		this.onScrap(closeHandler);
		return this;
	}

	@Override
	default void close() {
		Scrapable.super.close();
	}

	default <U> U chain(Function<? super Lot<T>, U> mapper) {
		Objects.requireNonNull(mapper, "non null mapper required");
		return mapper.apply(this);
	}

	default <U> Lot<U> chainStream(Function<Stream<T>, Stream<U>> mapper) {
		return Lot.create(getSpliteratorSource(this, mapper));
	}

	public static <T> Lot<T> create(Iterable<? extends T> iterable) {
		return create(SpliteratorSource.create(iterable));
	}

	public static <T> Lot<T> create(SpliteratorSource<? extends T, ?> spliteratorSource) {
		var lot = new AbstractLot<T>() {

			@Override
			protected T computeNext() {
				return computeNext(spliteratorSource);
			}

		};
		spliteratorSource.onScrap(lot);
		lot.onScrap(spliteratorSource);
		return lot;
	}

	public static interface Indexed<T> extends Lot<T> {

		long getIndex();

		long getExactOrEstimateSize();

		@Override
		default <U> Lot.Indexed<U> chainStream(Function<Stream<T>, Stream<U>> mapper) {
			return Lot.Indexed.create(getSpliteratorSource(this, mapper), this::estimateSize, null);
		}

		public static <T> Indexed<T> empty() {
			return create(null);
		}

		public static <T> Indexed<T> create(Iterable<? extends T> iterable) {
			return create(iterable, null, null);
		}

		public static <T> Indexed<T> create(Iterable<? extends T> iterable, Supplier<Long> estimateSizeSupplier) {
			return create(iterable, estimateSizeSupplier, null);
		}

		public static <T> Indexed<T> create(Iterable<? extends T> iterable,
				BiFunction<Long, T, Long> estimateSizeFunction) {
			return create(iterable, null, estimateSizeFunction);
		}

		public static <T> Indexed<T> create(Iterable<? extends T> iterable, Supplier<Long> estimateSizeSupplier,
				BiFunction<Long, T, Long> estimateSizeFunction) {
			return create(SpliteratorSource.create(iterable), estimateSizeSupplier, estimateSizeFunction);
		}

		public static <T> Indexed<T> create(SpliteratorSource<? extends T, ?> spliteratorSource,
				Supplier<Long> estimateSizeSupplier, BiFunction<Long, T, Long> estimateSizeFunction) {
			var lot = new AbstractLot.Indexed<T>() {

				private Long estimatedSize;

				@Override
				protected T computeNext(long index) {
					var next = computeNext(spliteratorSource);
					if (!isEnded() && estimateSizeFunction != null) {
						var size = estimateSizeFunction.apply(index, next);
						if (size != null && size >= 0)
							this.estimatedSize = size;
					}
					return next;
				}

				@Override
				protected Long estimateSizeInternal() {
					List<Supplier<Long>> sizeSuppliers = List.of(() -> {
						return this.estimatedSize;
					}, () -> {
						return estimateSizeSupplier == null ? null : estimateSizeSupplier.get();
					}, () -> {
						if (estimateSizeFunction != null && this.producer.getRequestIndex() < 0)
							this.hasNext();
						return this.estimatedSize;
					}, () -> {
						return super.estimateSizeInternal();
					});
					for (var sizeSupplier : sizeSuppliers) {
						var size = sizeSupplier.get();
						if (size != null && size >= 0 && size < Long.MAX_VALUE)
							return size;
					}
					return null;
				}
			};
			spliteratorSource.onScrap(lot);
			lot.onScrap(spliteratorSource);
			return lot;
		}
	}

	private static <T, U> SpliteratorSource<U, Lot<T>> getSpliteratorSource(Lot<T> lot,
			Function<Stream<T>, Stream<U>> mapper) {
		Objects.requireNonNull(lot, "lot required");
		Objects.requireNonNull(mapper, "mapper required");
		return SpliteratorSource.create(lot, nil -> {
			var mappedStream = Optional.ofNullable(mapper.apply(lot)).orElseGet(Stream::empty);
			return mappedStream.spliterator();
		});
	}

	public static void main(String[] args) {
		var lot = Lot.create(List.of(1, 2, 3));
		System.out.println(lot.peek());
		System.out.println(lot.peek());
		lot.forEach(v -> {
			System.out.println(v);
		});
		System.out.println(lot.peek());
	}

}
