package com.lfp.joe.core.function;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.IllegalFormatException;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class Asserts {

	private static final String ASSERTION_NAME_REQUIRED = "assertionName required";
	private static final String ASSERTION_FAILED_TEMPLATE = "%s failed:%s";

	protected Asserts() {}

	public static <X> X nonNull(X value, Object... exceptionArguments) throws AssertException {
		return nonNull(value, tofailureCallback(exceptionArguments));
	}

	public static <X> X nonNull(X value, Consumer<FailureContext<X>> failureCallback) throws AssertException {
		return assertion("nonNull", value, Objects::nonNull, failureCallback);
	}

	public static <X> X nonEmpty(X value, Object... exceptionArguments) throws AssertException {
		return nonEmpty(value, tofailureCallback(exceptionArguments));
	}

	public static <X> X nonEmpty(X value, Consumer<FailureContext<X>> failureCallback) throws AssertException {
		return assertion("nonEmpty", value, v -> !isEmpty(v), failureCallback);
	}

	public static <X> X nonBlank(X value, Object... exceptionArguments) throws AssertException {
		return nonBlank(value, tofailureCallback(exceptionArguments));
	}

	public static <X> X nonBlank(X value, Consumer<FailureContext<X>> failureCallback) throws AssertException {
		return assertion("nonBlank", value, v -> !isBlank(v), failureCallback);
	}

	public static void isTrue(Boolean expression, Object... exceptionArguments) throws AssertException {
		isTrue(expression, tofailureCallback(exceptionArguments));
	}

	public static void isTrue(Boolean expression, Consumer<FailureContext<Boolean>> failureCallback)
			throws AssertException {
		assertion("isTrue", expression, bool -> bool != null && bool, failureCallback);

	}

	public static <X> X validate(X value, Predicate<X> validator, Object... exceptionArguments) throws AssertException {
		return validate(value, validator, tofailureCallback(exceptionArguments));
	}

	public static <X> X validate(X value, Predicate<X> validator, Consumer<FailureContext<X>> failureCallback)
			throws AssertException {
		return assertion("validate", value, validator, failureCallback);
	}

	public static AssertException createException(Object... arguments) {
		Throwable cause = getCause(arguments).orElse(null);
		int start = 0;
		int end = arguments == null ? 0 : arguments.length - (cause == null ? 0 : 1);
		if (start >= end)
			return AssertException.create(null, cause);
		var message = Optional.ofNullable(argumentAt(arguments, start, end, 0)).map(Objects::toString).orElse(null);
		start++;
		if (start < end) {
			var formatArguments = Arrays.copyOfRange(arguments, start, end);
			try {
				message = String.format(message, formatArguments);
			} catch (IllegalFormatException e) {
				message = message + " " + Arrays.toString(formatArguments)
						+ String.format(" (%s)", e.getClass().getName());
			}
		}
		return AssertException.create(message, cause);
	}

	protected static <X> Consumer<FailureContext<X>> tofailureCallback(Object... exceptionArguments) {
		if (exceptionArguments == null || exceptionArguments.length == 0)
			return null;
		return fctx -> fctx.exceptionArguments(exceptionArguments);
	}

	protected static <X> X assertion(String assertionName, X input, Predicate<X> predicate,
			Consumer<FailureContext<X>> failureCallback) {
		Objects.requireNonNull(assertionName, ASSERTION_NAME_REQUIRED);
		if (predicate != null && predicate.test(input))
			return input;
		Object[] arguments;
		if (failureCallback != null) {
			var argumentsRef = Muto.<Object[]>create();
			FailureContext<X> failureContext = new FailureContext<X>(assertionName, input) {

				@Override
				public void exceptionArguments(Object... arguments) {
					if (Stream.ofNullable(arguments).flatMap(Stream::of).anyMatch(Objects::nonNull))
						argumentsRef.set(arguments);
				}
			};
			failureCallback.accept(failureContext);
			arguments = argumentsRef.get();
		} else
			arguments = null;
		Optional<Throwable> causeOp;
		if (arguments != null && arguments.length == 1) {
			causeOp = Asserts.getCause(arguments);
			if (causeOp.isPresent())
				arguments = null;
		} else
			causeOp = Optional.empty();
		if (arguments == null) {
			arguments = new Object[3 + (causeOp.isPresent() ? 1 : 0)];
			arguments[0] = ASSERTION_FAILED_TEMPLATE;
			arguments[1] = assertionName;
			arguments[2] = input;
			if (causeOp.isPresent())
				arguments[3] = causeOp.get();
		}
		throw Asserts.createException(arguments);
	}

	protected static boolean isBlank(Object value) {
		if (value instanceof CharSequence && value.toString().isBlank())
			return true;
		return isEmptyData(value);
	}

	protected static boolean isEmpty(Object value) {
		if (value instanceof CharSequence && value.toString().isEmpty())
			return true;
		return isEmptyData(value);
	}

	protected static boolean isEmptyData(Object value) {
		if (value == null)
			return true;
		if (value instanceof Collection && ((Collection<?>) value).isEmpty())
			return true;
		if (value instanceof Map && ((Map<?, ?>) value).isEmpty())
			return true;
		if (value.getClass().isArray() && Array.getLength(value) == 0)
			return true;
		return false;
	}

	private static Optional<Throwable> getCause(Object... arguments) {
		if (arguments == null || arguments.length == 0)
			return Optional.empty();
		return Optional.ofNullable(arguments[arguments.length - 1])
				.filter(Throwable.class::isInstance)
				.map(Throwable.class::cast);
	}

	private static Object argumentAt(Object[] arguments, int start, int end, int i) {
		if (i < start || i >= end)
			throw new IndexOutOfBoundsException(i);
		var argument = arguments[i];
		if (argument instanceof Supplier)
			argument = ((Supplier<?>) argument).get();
		if (argument != null && argument.getClass().isArray())
			argument = Arrays.toString((Object[]) argument);
		return argument;
	}

	public static abstract class FailureContext<X> {

		private final String assertionName;
		private final X input;

		public FailureContext(String assertionName, X input) {
			super();
			this.assertionName = assertionName;
			this.input = input;
		}

		public String getAssertionName() {
			return assertionName;
		}

		public X getInput() {
			return input;
		}

		public abstract void exceptionArguments(Object... arguments);

	}

	public static class AssertException extends IllegalArgumentException {

		private static final long serialVersionUID = 5086210699623384547L;

		public static AssertException create(String message, Throwable cause) {
			if (message == null && cause == null)
				return new AssertException();
			if (message == null)
				return new AssertException(cause);
			if (cause == null)
				return new AssertException(message);
			return new AssertException(message, cause);
		}

		public AssertException() {
			super();

		}

		public AssertException(String message, Throwable cause) {
			super(message, cause);

		}

		public AssertException(String s) {
			super(s);

		}

		public AssertException(Throwable cause) {
			super(cause);
		}

		public void throwNow() {
			throw this;
		}

	}

	public static class Ext extends Asserts {

		public static <X> X assertion(String assertionName, X input, Predicate<X> predicate,
				Consumer<FailureContext<X>> failureCallback) {
			return Asserts.assertion(assertionName, input, predicate, failureCallback);
		}
	}

	public static void main(String[] args) {
		Asserts.validate(50, Integer.valueOf(50)::equals, fc -> fc.exceptionArguments(new IOException()));
		// Asserts.isTrue(null);
		Asserts.createException("cool:%s", 99, new IOException());
		Asserts.createException("cool:%s %s %s", 88, 99, new IOException());
		Asserts.createException("cool", new IOException());
		Asserts.createException(new IOException());
		Asserts.createException("cool", new IOException()).throwNow();

	}

}
