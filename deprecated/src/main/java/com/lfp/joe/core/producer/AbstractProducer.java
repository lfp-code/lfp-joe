package com.lfp.joe.core.producer;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

public abstract class AbstractProducer<T> implements Producer<T> {

	private final AtomicLong requestIndex = new AtomicLong(-1);
	private final AtomicLong produceCount = new AtomicLong();
	private final AtomicLong consumeCount = new AtomicLong();
	private final Object queueLock = new Object();
	private List<T> queue = new ArrayList<>();
	private boolean produceComplete = false;

	@Override
	public long getRequestIndex() {
		return requestIndex.get();
	}

	@Override
	public long getProduceCount() {
		return produceCount.get();
	}

	@Override
	public long getConsumeCount() {
		return consumeCount.get();
	}

	@Override
	public boolean isComplete() {
		return queue == null;
	}

	public boolean isProduceComplete() {
		return produceComplete;
	}

	@Override
	public Optional<T> peek() {
		return tryProduce(false);
	}

	@Override
	public T produce() {
		var nextOp = tryProduce(true);
		if (nextOp == null)
			throw new NoSuchElementException();
		return nextOp.orElse(null);
	}

	@SuppressWarnings("unchecked")
	public Optional<T> tryProduce(boolean take) {
		if (queue != null)
			synchronized (queueLock) {
				if (queue != null) {
					Optional<T>[] nextRef = new Optional[1];
					queue = tryProduce(queue, take, v -> nextRef[0] = Optional.ofNullable(v));
					return nextRef[0];
				}
			}
		return null;
	}

	protected List<T> tryProduce(List<T> queue, boolean take, Consumer<T> action) {
		while (true) {
			if (!queue.isEmpty()) {
				var next = take ? queue.remove(0) : queue.get(0);
				consumeCount.incrementAndGet();
				action.accept(next);
				return queue;
			}
			if (produceComplete)
				return null;
			requestIndex.incrementAndGet();
			var produced = tryProduce(v -> {
				produceCount.incrementAndGet();
				queue.add(v);
			});
			if (!produced)
				produceComplete = true;
		}
	}

}
