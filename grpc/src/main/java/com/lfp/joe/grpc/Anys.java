package com.lfp.joe.grpc;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;

import com.google.protobuf.Any;
import com.lfp.joe.utils.Utils;

public class Anys {

	public static Any of(Object value) {
		return of(value, false);
	}

	public static Any of(Object value, boolean enablePrimitiveDefaults) {
		return tryOf(value, enablePrimitiveDefaults).orElseThrow(() -> new NoSuchElementException(
				"any covert failed. valueType:" + (value == null ? null : value.getClass())));
	}

	public static Optional<Any> tryOf(Object value) {
		return tryOf(value, false);
	}

	public static Optional<Any> tryOf(Object value, boolean enablePrimitiveDefaults) {
		if (value instanceof Any)
			return Optional.of((Any) value);
		return Messages.tryOf(value, enablePrimitiveDefaults).map(m -> Any.pack(m));
	}

	@SuppressWarnings("unchecked")
	public static <X> Optional<X> tryToObject(Any any) {
		if (any == null)
			return Optional.empty();
		try (var mtStream = Messages.streamMessageTypes(any.getTypeUrl())) {
			for (var mt : mtStream) {
				var message = Utils.Functions.unchecked(() -> any.unpack(mt));
				try (var valueStream = Messages.streamValues(message)) {
					for (var value : valueStream)
						return Optional.of((X) value);
				}

			}
		}
		return Optional.empty();
	}

	public static <X> X toObject(Any any) {
		Objects.requireNonNull(any, "any required");
		Optional<X> op = tryToObject(any);
		return op.orElseThrow(
				() -> new IllegalArgumentException("unable to get any value. type_url:" + any.getTypeUrl()));
	}

	public static void main(String[] args) {
		System.out.println(Anys.tryOf(999, true).get());
		System.out.println(Anys.tryToObject(Anys.of(69)).toString());
	}
}
