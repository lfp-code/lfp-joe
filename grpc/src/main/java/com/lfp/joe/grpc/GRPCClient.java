package com.lfp.joe.grpc;

import java.net.InetSocketAddress;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

import org.slf4j.Logger;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.Once.SupplierOnce;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.log.Logging;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.stub.AbstractStub;
import io.grpc.stub.MetadataUtils;

public abstract class GRPCClient<S extends AbstractStub<S>> extends Scrapable.Impl {

	private static final String PASSWORD_KEY = "password";

	private final Logger serviceLogger = Logging.logger();
	private final SupplierOnce<S> serviceSupplier;

	private Consumer<ManagedChannelBuilder<?>> channelModifier;

	public GRPCClient(InetSocketAddress serverAddress, String password) {
		this(serverAddress, password, null);
	}

	public GRPCClient(InetSocketAddress serverAddress, String password,
			Consumer<ManagedChannelBuilder<?>> channelModifier) {
		Objects.requireNonNull(serverAddress);
		this.channelModifier = channelModifier;
		this.serviceSupplier = SupplierOnce.of(() -> {
			return createService(serverAddress, Optional.ofNullable(password));
		});
		this.onScrap(() -> {
			serviceLogger.info("scrapping client. serverAddress:{}", serverAddress);
		});
	}

	public S getService() {
		return serviceSupplier.get();
	}

	protected S createService(InetSocketAddress serverAddress, Optional<String> passwordOp) {
		serviceLogger.info("creating client. serverAddress:{}", serverAddress);
		ManagedChannel channel;
		{
			var channelBuilder = ManagedChannelBuilder
					.forAddress(serverAddress.getHostString(), serverAddress.getPort())
					.enableRetry();
			Optional.ofNullable(this.channelModifier).ifPresent(c -> {
				this.channelModifier = null;
				c.accept(channelBuilder);
			});
			channel = channelBuilder.build();
			this.onScrap(channel::shutdownNow);
		}
		Metadata header = new Metadata();
		if (passwordOp.isPresent()) {
			Metadata.Key<String> key = Metadata.Key.of(PASSWORD_KEY, Metadata.ASCII_STRING_MARSHALLER);
			header.put(key, passwordOp.get());
		}
		S service = MetadataUtils.attachHeaders(createService(channel), header);
		this.onScrap(() -> {
			CoreReflections.tryCast(service.getChannel(), ManagedChannel.class)
					.ifPresentOrElse(ManagedChannel::shutdownNow, () -> {
						CoreReflections.tryCast(service.getChannel(), AutoCloseable.class)
								.ifPresent(Throws.consumerUnchecked(AutoCloseable::close));
					});
		});
		return service;
	}

	protected abstract S createService(ManagedChannel channel);
}
