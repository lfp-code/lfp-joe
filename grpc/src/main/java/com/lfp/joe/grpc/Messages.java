package com.lfp.joe.grpc;

import java.lang.reflect.Modifier;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.common.hash.Hashing;
import com.google.protobuf.Descriptors;
import com.google.protobuf.Descriptors.Descriptor;
import com.google.protobuf.Int64Value;
import com.google.protobuf.Message;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.reflections.reflection.MethodPredicate;
import com.lfp.joe.reflections.reflection.ModifierPredicate;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.Requires;

import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

@SuppressWarnings({ "unchecked" })
public class Messages {

	private static final List<Predicate<Class<?>>> PREFERENCE_PREDICATES;
	static {
		List<Predicate<Class<?>>> preferencePredicates = new ArrayList<>();
		preferencePredicates.add(long.class::equals);
		preferencePredicates.add(int.class::equals);
		preferencePredicates.add(double.class::equals);
		preferencePredicates.add(v -> Utils.Types.isAssignableFrom(Number.class, v));
		preferencePredicates.add(Class::isPrimitive);
		preferencePredicates.add(v -> Modifier.isFinal(v.getModifiers()));
		PREFERENCE_PREDICATES = Collections.unmodifiableList(preferencePredicates);
	}
	private static final LoadingCache<Nada, List<Class<? extends Message>>> MESSAGE_TYPES_CACHE = Caches
			.newCaffeineBuilder(-1, null, Duration.ofSeconds(5)).build(nil -> {
				var stream = JavaCode.Reflections.streamSubTypesOf(Message.class);
				var ctFilter = ModifierPredicate.createType().withModifierNotINTERFACE().withModifierNotABSTRACT();
				stream = stream.filter(ctFilter);
				var packageName = Message.class.getPackageName();
				Comparator<Class<? extends Message>> sorter = Comparator.comparing(v -> {
					if (!v.getPackageName().equals(packageName))
						return Integer.MAX_VALUE;
					return Integer.MIN_VALUE;
				});

				sorter = sorter.thenComparing(v -> {
					int multiple = Utils.Strings.startsWithIgnoreCase(v.getSimpleName(), "u") ? 10 : 1;
					try (var javaTypes = streamOfs(v).keys();) {
						for (var javaType : javaTypes) {
							javaType = Utils.Types.wrapperToPrimitive(javaType);
							for (int i = 0; i < PREFERENCE_PREDICATES.size(); i++) {
								var preferencePredicate = PREFERENCE_PREDICATES.get(i);
								if (preferencePredicate.test(javaType)) {
									return (i + 1) * multiple;
								}
							}
						}
					}
					return Integer.MAX_VALUE;
				});
				sorter = sorter.thenComparing(Utils.Strings.comparatorIgnoreCase(Class::getSimpleName));
				return stream.sorted(sorter).toImmutableList();
			});

	public static StreamEx<Class<? extends Message>> streamMessageTypes() {
		return Utils.Lots.stream(MESSAGE_TYPES_CACHE.get(Nada.get()));
	}

	private static final LoadingCache<String, List<Class<? extends Message>>> TYPE_URL_CACHE = Caches
			.newCaffeineBuilder(-1, null, Duration.ofSeconds(5)).build(typeUrl -> {
				StreamEx<Class<? extends Message>> stream = StreamEx.empty();
				{
					var append = streamMessageTypes().filter(v -> {
						return streamDescriptors(v).anyMatch(descriptor -> {
							if (Utils.Strings.endsWith(typeUrl, "/" + descriptor.getFullName()))
								return true;
							if (Utils.Strings.equals(typeUrl, descriptor.getFullName()))
								return true;
							return false;
						});
					});
					stream = stream.append(append);
				}
				{
					for (var afterSlash : List.of(true, false)) {
						for (var prefix : List.of("com.", "")) {
							stream = stream.append(Utils.Lots.defer(() -> {
								var className = typeUrl;
								if (afterSlash)
									className = Utils.Strings.substringAfterLast(className, "/");
								className = prefix + className;
								var classType = (Class<? extends Message>) JavaCode.Reflections.tryForName(className)
										.orElse(null);
								return Arrays.asList(classType);
							}));
						}
					}
				}
				stream = stream.nonNull().distinct();
				var list = stream.toImmutableList();
				if (list.isEmpty())
					return null;
				return list;
			});

	public static StreamEx<Class<? extends Message>> streamMessageTypes(String typeUrl) {
		if (Utils.Strings.isBlank(typeUrl))
			return StreamEx.empty();
		return Utils.Lots.stream(TYPE_URL_CACHE.get(typeUrl));

	}

	private static final LoadingCache<Class<? extends Message>, List<Entry<Class<?>, Function<Object, Message>>>> MESSAGE_TYPE_OF_CACHE = Caches
			.newCaffeineBuilder(-1, null, Duration.ofSeconds(5)).build(messageType -> {
				var methodPredicate = MethodPredicate.create().withModifierSTATIC().withModifierPUBLIC()
						.withParametersCount(1).withReturnTypeAssignableTo(messageType).withName("of");
				var methodStream = JavaCode.Reflections.streamMethods(messageType, true, methodPredicate);
				EntryStream<Class<?>, Function<Object, Message>> estream = methodStream
						.mapToEntry(v -> v.getParameterTypes()[0], v -> {
							Function<Object, Message> func = o -> (Message) Throws.unchecked(() -> v.invoke(null, o));
							return func;
						});
				return estream.toImmutableList();
			});

	public static EntryStream<Class<?>, Function<Object, Message>> streamOfs(Class<? extends Message> messageType) {
		if (messageType == null)
			return EntryStream.empty();
		return Utils.Lots.stream(MESSAGE_TYPE_OF_CACHE.get(messageType)).chain(Utils.Lots::streamEntries);
	}

	private static final LoadingCache<Class<? extends Message>, List<Function<Message, Object>>> MESSAGE_TYPE_GET_VALUE_CACHE = Caches
			.newCaffeineBuilder(-1, null, Duration.ofSeconds(5)).build(messageType -> {
				var methodPredicate = MethodPredicate.create().withModifierNotSTATIC().withModifierPUBLIC()
						.withParametersCount(0).withName("getValue");
				var methodStream = JavaCode.Reflections.streamMethods(messageType, true, methodPredicate);
				return methodStream.map(method -> {
					Function<Message, Object> getter = v -> Throws.unchecked(() -> method.invoke(v));
					return getter;
				}).toImmutableList();

			});

	public static StreamEx<Object> streamValues(Message message) {
		if (message == null)
			return StreamEx.empty();
		return Utils.Lots.stream(MESSAGE_TYPE_GET_VALUE_CACHE.get(message.getClass())).map(v -> v.apply(message));
	}

	public static Message of(Object value) {
		return of(value, false);
	}

	public static Message of(Object value, boolean enablePrimitiveDefaults) {
		return tryOf(value, enablePrimitiveDefaults).orElseThrow(() -> new NoSuchElementException(
				"message covert failed. valueType:" + (value == null ? null : value.getClass())));
	}

	public static Optional<Message> tryOf(Object value) {
		return tryOf(value, false);
	}

	public static Optional<Message> tryOf(Object value, boolean enablePrimitiveDefaults) {
		if (value instanceof Message)
			return Optional.of((Message) value);
		if (value == null && !enablePrimitiveDefaults)
			return Optional.empty();
		try (var mtStream = streamMessageTypes()) {
			for (var mt : mtStream) {
				try (var typeToOfs = streamOfs(mt)) {
					for (var typeToOf : typeToOfs) {
						if (value == null) {
							var valuePrim = Utils.Types.getPrimitiveDefault(typeToOf.getKey()).orElse(null);
							if (valuePrim == null)
								continue;
							return Optional.of(typeToOf.getValue().apply(valuePrim));
						}
						if (Utils.Types.isAssignableFrom(typeToOf.getKey(), value.getClass()))
							return Optional.of(typeToOf.getValue().apply(value));
					}
				}
			}
		}
		return Optional.empty();
	}

	public static EntryStream<Descriptors.FieldDescriptor, Object> streamFields(Message message) {
		return streamFields(message, null, false, false);
	}

	public static EntryStream<Descriptors.FieldDescriptor, Object> streamFields(Message message, String fieldName) {
		return streamFields(message, fieldName, false, false);
	}

	public static EntryStream<Descriptors.FieldDescriptor, Object> streamFields(Message message, String fieldName,
			boolean nonNull) {
		return streamFields(message, fieldName, nonNull, false);
	}

	public static EntryStream<Descriptors.FieldDescriptor, Object> streamFields(Message message, String fieldName,
			boolean nonNull, boolean nonBlank) {
		var allFields = message == null ? null : message.getAllFields();
		var estream = Utils.Lots.stream(allFields);
		if (fieldName != null)
			estream = estream.filterKeys(v -> fieldName.equals(v.getName()));
		if (nonNull)
			estream = estream.nonNullValues();
		if (nonBlank)
			estream = estream.filterValues(v -> {
				if (!(v instanceof CharSequence))
					return true;
				return Utils.Strings.isNotBlank((CharSequence) v);
			});
		return estream;
	}

	private static final MethodPredicate<?> GET_DEFAULT_INSTANCE_BASE_METHOD_PREDICATE = MethodPredicate.create()
			.withModifierSTATIC().withModifierPUBLIC().withParametersCount(0).withName("getDefaultInstance");

	public static <M extends Message> StreamEx<M> streamDefaultInstances(Class<? extends M> messageType) {
		if (messageType == null)
			return StreamEx.empty();
		var methodPredicate = GET_DEFAULT_INSTANCE_BASE_METHOD_PREDICATE.withReturnTypeAssignableTo(messageType);
		var methodStream = JavaCode.Reflections.streamMethods(messageType, true, methodPredicate);
		return methodStream.map(v -> Throws.unchecked(() -> v.invoke(null))).map(v -> (M) v);
	}

	private static final MethodPredicate<?> GET_DESCRIPTOR_METHOD_PREDICATE = MethodPredicate.create()
			.withModifierSTATIC().withModifierPUBLIC().withParametersCount(0)
			.withReturnTypeAssignableTo(Descriptor.class).withName("getDescriptor");

	public static StreamEx<Descriptor> streamDescriptors(Class<? extends Message> messageType) {
		if (messageType == null)
			return StreamEx.empty();
		var methodStream = JavaCode.Reflections.streamMethods(messageType, true, GET_DESCRIPTOR_METHOD_PREDICATE);
		return methodStream.map(v -> Throws.unchecked(() -> v.invoke(null))).map(v -> (Descriptor) v);
	}

	public static Int64Value Hash(Object value) {
		var bytes = Utils.Bits.tryFrom(value).orElse(null);
		bytes = Requires.notNull(bytes, "unable to read bytes from type:%s", value.getClass());
		var hash = Hashing.farmHashFingerprint64().hashBytes(bytes.array());
		return Int64Value.of(hash.asLong());
	}

	public static void main(String[] args) {
		streamMessageTypes().forEach(v -> {
			System.out.println(v.getName());
		});
		streamMessageTypes().forEach(v -> {
			for (var ent : streamOfs(v)) {
				System.out.println(String.format("%s - %s", v, ent.getKey()));
			}
		});
		var any = Anys.of(69);
		var typeUrl = any.getTypeUrl();
		streamMessageTypes(typeUrl).forEach(v -> {
			System.out.println(String.format("%s - %s", typeUrl, v));
		});

		streamMessageTypes().forEach(v -> {
			for (var d : streamDescriptors(v)) {
				System.out.println(String.format("%s - %s", v, d.getFullName()));
			}
		});

	}

}
