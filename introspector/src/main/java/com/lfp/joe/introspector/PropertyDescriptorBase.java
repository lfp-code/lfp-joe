package com.lfp.joe.introspector;

import java.lang.reflect.Method;

import javax.annotation.Nullable;

import org.immutables.value.Value;

interface PropertyDescriptorBase {

	public static enum Source {
		BEAN, BEAN_REF;
	}

	String getName();

	@Nullable
	Method getReadMethod();

	@Nullable
	Method getWriteMethod();

	@Value.Default
	default Source getSource() {
		return Source.BEAN;
	}

	default Class<?> getPropertyType() {
		if (getReadMethod() != null) {
			return getReadMethod().getReturnType();
		} else if (getWriteMethod() != null) {
			Class<?>[] parameterTypes = getWriteMethod().getParameterTypes();
			return parameterTypes[0];
		}
		return null;
	}
}