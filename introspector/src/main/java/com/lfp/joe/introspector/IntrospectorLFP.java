package com.lfp.joe.introspector;

import java.lang.ref.SoftReference;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.TreeMap;
import java.util.WeakHashMap;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.github.throwable.beanref.BeanProperty;
import com.github.throwable.beanref.BeanRef;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.ComputeStore;
import com.lfp.joe.core.function.EmptyArrays;
import com.lfp.joe.core.function.FNV;

public class IntrospectorLFP {

	protected IntrospectorLFP() {}

	private static final ComputeStore<String, BeanInfo> BEAN_INFO_CACHE = ComputeStore.from(new WeakHashMap<>(),
			SoftReference::new);
	private static final ComputeStore<Entry<String, Long>, PropertyDescriptorTreeNode> BEAN_INFO_TREE_CACHE = ComputeStore
			.from(new WeakHashMap<>(), SoftReference::new);

	public static BeanInfo getBeanInfo(Class<?> beanClass) {
		return getBeanInfo(beanClass, Object.class);
	}

	public static BeanInfo getBeanInfo(Class<?> beanClass, Class<?> stopClass) {
		{
			var namedBeanClass = CoreReflections.getNamedClassTypeIfVaries(beanClass).orElse(null);
			if (namedBeanClass != null)
				return getBeanInfo(namedBeanClass, stopClass);
		}
		String beanInfoSignature = beanInfoSignature(beanClass, stopClass);
		return BEAN_INFO_CACHE.computeIfAbsent(beanInfoSignature, () -> loadBeanInfo(beanClass, stopClass));
	}

	protected static BeanInfo loadBeanInfo(Class<?> beanClass, Class<?> stopClass) {
		var beanInfo = construct(beanClass, stopClass);
		var beanRefPropertyDescriptors = BeanRef.$(beanClass)
				.all()
				.stream()
				.filter(BeanProperty.class::isInstance)
				.map(BeanProperty.class::cast)
				.filter(v -> beanInfo.tryGetPropertyDescriptor(v.getName()).isEmpty())
				.map(PropertyDescriptor::of)
				.collect(Collectors.toList());
		if (beanRefPropertyDescriptors.isEmpty())
			return beanInfo;
		var pdArray = Stream.concat(Stream.of(beanInfo.getPropertyDescriptors()), beanRefPropertyDescriptors.stream())
				.toArray(PropertyDescriptor[]::new);
		return beanInfo.withPropertyDescriptors(pdArray);
	}

	public static PropertyDescriptorTreeNode getBeanInfoTree(Class<?> beanClass) {
		return getBeanInfoTree(beanClass, Object.class);
	}

	public static PropertyDescriptorTreeNode getBeanInfoTree(Class<?> beanClass, Class<?> stopClass) {
		return getBeanInfoTree(beanClass, stopClass, null);
	}

	public static PropertyDescriptorTreeNode getBeanInfoTree(Class<?> beanClass, Class<?> stopClass,
			PropertyDescriptorTreeFilter filter) {
		if (filter == null)
			return getBeanInfoTree(beanClass, stopClass, PropertyDescriptorTreeFilter.Default.INSTANCE);
		{
			var namedBeanClass = CoreReflections.getNamedClassTypeIfVaries(beanClass).orElse(null);
			if (namedBeanClass != null)
				return getBeanInfoTree(namedBeanClass, stopClass, filter);
		}
		var beanInfoSignature = beanInfoSignature(beanClass, stopClass);
		var cacheKey = Map.entry(beanInfoSignature, FNV.hash64(filter));
		return BEAN_INFO_TREE_CACHE.computeIfAbsent(cacheKey, () -> {
			var fjp = new ForkJoinPool(MachineConfig.logicalCoreCount());
			try {
				return fjp.submit(() -> {
					var rootBeanInfo = getBeanInfo(beanClass, stopClass);
					var rootNode = PropertyDescriptorTreeNode.builder().name("$").build();
					rootNode = appendChildNodes(rootNode, rootBeanInfo, stopClass, filter);
					return rootNode;
				}).join();
			} finally {
				fjp.shutdownNow();
			}
		});
	}

	private static PropertyDescriptorTreeNode appendChildNodes(PropertyDescriptorTreeNode parent, BeanInfo beanInfo,
			Class<?> stopClass, PropertyDescriptorTreeFilter filter) {
		var propertyDescriptors = Stream.of(beanInfo.getPropertyDescriptors()).parallel().filter(pd -> {
			return filter.test(parent, pd);
		});
		var childNodes = propertyDescriptors.map(pd -> {
			return PropertyDescriptorTreeNode.builder().from(pd).parent(parent).build();
		}).map(childNode -> {
			var childBeanInfo = getBeanInfo(childNode.getPropertyType());
			return appendChildNodes(childNode, childBeanInfo, stopClass, filter);
		});
		return parent.withChildren(() -> childNodes.iterator());
	}

	protected static String beanInfoSignature(Class<?> beanClass, Class<?> stopClass) {
		beanClass = Optional.ofNullable(beanClass).map(CoreReflections::getNamedClassType).orElse(null);
		stopClass = Optional.ofNullable(stopClass).map(CoreReflections::getNamedClassType).orElse(null);
		return (beanClass != null ? beanClass.getName() : "") + "," + (stopClass != null ? stopClass.getName() : "");
	}

	protected static BeanInfo construct(Class<?> beanType, Class<?> stopType) {
		FieldOrderComparator comparator = new FieldOrderComparator();
		TreeMap<String, PropertyDescriptor> propsByName = new TreeMap<>(comparator);
		introspect(beanType, stopType, propsByName, comparator);
		BeanDescriptor beanDescriptor = BeanDescriptor.of(beanType);
		PropertyDescriptor[] propertyDescriptors;
		if (propsByName.isEmpty())
			propertyDescriptors = EmptyArrays.of(PropertyDescriptor.class);
		else
			propertyDescriptors = propsByName.values().toArray(new PropertyDescriptor[propsByName.size()]);
		return BeanInfo.of(beanDescriptor, propertyDescriptors);
	}

	protected static void introspect(Class<?> currType, Class<?> stopType, Map<String, PropertyDescriptor> props,
			FieldOrderComparator comparator) {
		if (currType == null)
			return;
		if (stopType != null && currType == stopType)
			return;
		introspect(currType.getSuperclass(), stopType, props, comparator);
		comparator.addFields(currType.getDeclaredFields());
		for (Method m : currType.getDeclaredMethods()) {
			final int flags = m.getModifiers();
			if (Modifier.isStatic(flags) || m.isSynthetic() || m.isBridge())
				continue;
			Class<?> argTypes[] = m.getParameterTypes();
			if (argTypes.length == 0) {
				if (!Modifier.isPublic(flags))
					continue;
				Class<?> resultType = m.getReturnType();
				if (resultType == Void.class)
					continue;
				String name = m.getName();
				if (name.startsWith("get")) {
					if (resultType == void.class)
						continue;
					if (name.length() > 3) {
						name = decapitalize(name.substring(3));
						propertyDescriptor(props, name, m, null);
					}
				} else if (name.startsWith("is")) {
					if (resultType == void.class)
						continue;

					if (name.length() > 2) {
						name = decapitalize(name.substring(2));
						propertyDescriptor(props, name, m, null);
					}
				}
			} else if (argTypes.length == 1) {
				if (!Modifier.isPublic(flags))
					continue;
				String name = m.getName();
				if (!name.startsWith("set") || name.length() == 3)
					continue;
				name = decapitalize(name.substring(3));
				propertyDescriptor(props, name, null, m);
			}
		}

	}

	protected static PropertyDescriptor propertyDescriptor(Map<String, PropertyDescriptor> props, String name,
			Method readMethod, Method writeMethod) {
		return props.compute(name, (nil, pd) -> {
			if (pd == null)
				pd = PropertyDescriptor.builder().name(name).readMethod(readMethod).writeMethod(writeMethod).build();
			else {
				if (readMethod != null)
					pd = pd.withReadMethod(readMethod);
				if (writeMethod != null)
					pd = pd.withWriteMethod(writeMethod);
			}
			return pd;
		});
	}

	protected static String decapitalize(String name) {
		if (name == null || name.length() == 0)
			return name;
		if (name.length() > 1 && Character.isUpperCase(name.charAt(1)) && Character.isUpperCase(name.charAt(0)))
			return name;
		char[] chars = name.toCharArray();
		chars[0] = Character.toLowerCase(chars[0]);
		return new String(chars);
	}

	/**
	 * Keeps properties ordered by fields found in class (which is the same as in
	 * Java code, even though not guaranteed by specification). If similarly named
	 * field not found, put to the end of list (at the level of class hierarchy) in
	 * alphabetical order.
	 */
	private static class FieldOrderComparator implements Comparator<String> {
		private List<String> order = new ArrayList<>();

		@Override
		public int compare(String o1, String o2) {
			int index1 = order.indexOf(o1);
			int index2 = order.indexOf(o2);
			if (index1 == -1) {
				if (index2 == -1)
					return o1.compareTo(o2);
				else
					return 1;
			}
			return index1 - index2;
		}

		public void addFields(Field... fields) {
			for (Field f : fields)
				order.add(f.getName());
		}
	}

}
