package com.lfp.joe.introspector;

import java.util.Optional;

import org.immutables.value.Value;

import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.EmptyArrays;

@Value.Immutable
@ValueLFP.Style
abstract class AbstractBeanInfo {

	public abstract BeanDescriptor getBeanDescriptor();

	public abstract PropertyDescriptor[] getPropertyDescriptors();

	public Optional<PropertyDescriptor> tryGetPropertyDescriptor(String name) {
		if (name == null || name.isBlank())
			return Optional.empty();
		for (var pd : getPropertyDescriptors())
			if (pd.getName().equals(name))
				return Optional.of(pd);
		return Optional.empty();
	}

	public static BeanInfo of(BeanDescriptor beanDescriptor, PropertyDescriptor... propertyDescriptors) {
		return BeanInfo.builder()
				.beanDescriptor(beanDescriptor)
				.propertyDescriptors(Optional.ofNullable(propertyDescriptors)
						.orElseGet(() -> EmptyArrays.of(PropertyDescriptor.class)))
				.build();
	}

}
