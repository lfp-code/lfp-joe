package com.lfp.joe.introspector;

import java.util.Objects;

import org.immutables.value.Value;

import com.github.throwable.beanref.BeanProperty;
import com.lfp.joe.core.config.ValueLFP;

@Value.Immutable
@ValueLFP.Style
abstract class AbstractPropertyDescriptor implements FeatureDescriptor, PropertyDescriptorBase {

	@Value.Check
	AbstractPropertyDescriptor check() {
		if (getName().isBlank())
			throw new IllegalArgumentException("name required");
		if (getWriteMethod() != null && getWriteMethod().getParameterCount() != 1)
			throw new IllegalArgumentException("invalid write method:" + getWriteMethod());
		return this;
	}

	public static PropertyDescriptor of(BeanProperty<?, ?> beanProperty) {
		Objects.requireNonNull(beanProperty);
		return PropertyDescriptor.builder()
				.name(beanProperty.getName())
				.readMethod(beanProperty.tryGetReadMethod().orElse(null))
				.writeMethod(beanProperty.tryGetWriteMethod().orElse(null))
				.source(Source.BEAN_REF)
				.build();
	}

}
