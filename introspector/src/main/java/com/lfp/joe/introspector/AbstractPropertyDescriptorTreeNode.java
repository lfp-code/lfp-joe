package com.lfp.joe.introspector;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import org.immutables.value.Value;

import com.lfp.joe.core.config.ValueLFP;

@Value.Immutable
@ValueLFP.Style
abstract class AbstractPropertyDescriptorTreeNode implements PropertyDescriptorBase {

	abstract Optional<PropertyDescriptorTreeNode> parent();

	abstract List<PropertyDescriptorTreeNode> children();

	public boolean isRoot() {
		return parent().isEmpty();
	}

	public boolean isLeaf() {
		return children().isEmpty();
	}

	public Stream<PropertyDescriptorTreeNode> streamAncestors() {
		return Stream.<PropertyDescriptorTreeNode>iterate(PropertyDescriptorTreeNode.copyOf(this), Objects::nonNull,
				v -> v.parent().orElse(null));
	}

}
