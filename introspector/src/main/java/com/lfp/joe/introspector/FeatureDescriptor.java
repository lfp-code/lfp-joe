package com.lfp.joe.introspector;

public interface FeatureDescriptor {

	String getName();

}
