package com.lfp.joe.introspector;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.stream.BaseStream;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.Instances;

public interface PropertyDescriptorTreeFilter
		extends BiPredicate<PropertyDescriptorTreeNode, PropertyDescriptor>, Serializable {

	public static enum Default implements PropertyDescriptorTreeFilter {
		INSTANCE;

		private final List<Predicate<Method>> readMethodFilters = Stream.of("build", "builder").flatMap(filter -> {
			return Stream.<Predicate<Method>>of(method -> {
				return !method.getName().equals(filter);
			}, new Predicate<Method>() {

				private String suffix = filter.length() <= 0 ? null
						: (filter.substring(0, 1).toUpperCase() + filter.substring(1));

				@Override
				public boolean test(Method method) {
					return suffix == null || !method.getName().endsWith(suffix);
				}
			});
		}).collect(Collectors.toUnmodifiableList());

		@Override
		public boolean test(PropertyDescriptorTreeNode parent, PropertyDescriptor pd) {
			{// readMethod present and not cyclical
				if (pd.getReadMethod() == null)
					return false;
				if (parent.streamAncestors().anyMatch(v -> pd.getReadMethod().equals(v.getReadMethod())))
					return false;
			}
			{// declaring class
				var readMethodDeclaringClass = pd.getReadMethod().getDeclaringClass();
				if (readMethodDeclaringClass.equals(pd.getReadMethod().getReturnType()))
					return false;
				if (CoreReflections.isBootModuleClassType(readMethodDeclaringClass))
					return false;
				else if (Map.class.isAssignableFrom(readMethodDeclaringClass))
					return false;
				else if (Iterable.class.isAssignableFrom(readMethodDeclaringClass))
					return false;
				else if (Iterator.class.isAssignableFrom(readMethodDeclaringClass))
					return false;
				else if (BaseStream.class.isAssignableFrom(readMethodDeclaringClass))
					return false;
			}
			if (!readMethodFilters.stream().allMatch(v -> v.test(pd.getReadMethod())))
				return false;
			if (!Instances.streamAutowired(PropertyDescriptorTreeFilter.class).allMatch(v -> v.test(parent, pd)))
				return false;
			return true;
		}
	}

}
