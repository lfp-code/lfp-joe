package com.lfp.joe.introspector;

import java.lang.ref.WeakReference;
import java.util.Objects;

import org.immutables.value.Value;

import com.lfp.joe.core.config.ValueLFP;

@Value.Immutable
@ValueLFP.Style
abstract class AbstractBeanDescriptor implements FeatureDescriptor {

	@Override
	public abstract String getName();

	abstract WeakReference<Class<?>> beanClassReference();

	public Class<?> getBeanClass() {
		return beanClassReference().get();
	}

	public static BeanDescriptor of(Class<?> classType) {
		Objects.requireNonNull(classType);
		return BeanDescriptor.builder()
				.name(classType.getSimpleName())
				.beanClassReference(new WeakReference<>(classType))
				.build();
	}

	@Override
	public String toString() {
		return "BeanDescriptor{" + "name=" + getName() + ", beanClass=" + getBeanClass() + "}";
	}
}
