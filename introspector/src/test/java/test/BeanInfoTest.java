package test;

import java.sql.Date;

import com.lfp.joe.introspector.BeanInfo;
import com.lfp.joe.introspector.IntrospectorLFP;

public class BeanInfoTest {

	public static void main(String[] args) throws InterruptedException {
		for (int i = 0; i < 100; i++) {
			if (i > 0) {
				System.gc();
				Runtime.getRuntime().gc();
				Thread.sleep(1_000);
			}
			BeanInfo beanInfo = IntrospectorLFP.getBeanInfo(Date.class);
			System.out.println(beanInfo);
		}
	}

}
