package test;

import com.github.throwable.beanref.lfp.BeanRefCache;

public class BeanRefCacheLFPTest {

	public static void main(String[] args) throws InterruptedException {
		for (int i = 0; i < 100; i++) {
			if (i > 0) {
				System.gc();
				Runtime.getRuntime().gc();
				Thread.sleep(1_000);
			}
			BeanRefCache.instance().get("neat", nil -> new Object());
		}
	}
}
