package test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.github.throwable.beanref.BeanRef;

public class BeanRefTest {

	public static void main(String[] args) {
		System.out.println(List.of(Date.class, "cool").equals(Arrays.asList(Date.class, "cool")));
		System.out.println(
				Stream.of(Date.class, "cool").collect(Collectors.toList()).equals(Arrays.asList(Date.class, "cool")));
		var bp = BeanRef.$(Date::getTime);
		System.out.println(bp.getPath());
	}
}
