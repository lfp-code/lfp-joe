package test;

import java.util.Date;

import com.lfp.joe.serial.Serials;
import com.lfp.joe.serial.gson.FlattenTypeAdapterFactory.Flatten;

import io.gsonfire.annotations.PostDeserialize;
import io.gsonfire.annotations.PreSerialize;

public class AnnoTest {
	public static class Doc {

		public final Date date = new Date();
		public int version = 0;
		@Flatten
		public Name name;

		@PreSerialize
		public void preSerialize() {
			System.out.println("pre serialize");
		}

		@PostDeserialize
		public void postDeserialize() {
			System.out.println("post deserialize");
		}

		@Override
		public String toString() {
			return "Doc [date=" + date + ", version=" + version + ", name=" + name + "]";
		}
	}

	public static class Name {

		public String first;
		public String last;

		@Override
		public String toString() {
			return "Name [first=" + first + ", last=" + last + "]";
		}
	}

	public static void main(String[] args) {
		var doc = new Doc();
		doc.name = new Name();
		doc.name.first = "joe";
		doc.name.last = "byron";
		var json = Serials.Gsons.get().toJson(doc);
		System.out.println(json);
		System.out.println(Serials.Gsons.get().fromJson(json, Doc.class));
	}
}
