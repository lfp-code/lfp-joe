package test;

import java.util.Date;

import com.lfp.joe.serial.Serials;
import com.lfp.joe.serial.gson.StreamTypeAdapterFactory;
import com.lfp.joe.utils.Utils;

import one.util.streamex.EntryStream;
import one.util.streamex.IntStreamEx;

public class StreamTest {

	public static void main(String[] args) {
		var gson = Serials.Gsons.getBuilder().setPrettyPrinting()
				.registerTypeAdapterFactory(new StreamTypeAdapterFactory(true, false)).create();
		var test = new Test();
		var json = gson.toJson(test);
		System.out.println(json);
		var test2 = gson.fromJson(json, Test.class);
		System.out.println(test2.name);
		System.out.println(test2.stream.toList());
	}

	private static class Test {

		public final String name = "Name " + Utils.Crypto.getRandomString();

		public final EntryStream<Integer, Date> stream = IntStreamEx.range(10).mapToObj(v -> v)
				.mapToEntry(nil -> new Date());
	}
}
