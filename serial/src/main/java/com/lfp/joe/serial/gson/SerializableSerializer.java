package com.lfp.joe.serial.gson;

import java.io.Serializable;
import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;

public class SerializableSerializer implements JsonSerializer<Serializable>, JsonDeserializer<Serializable> {

	public static final SerializableSerializer INSTANCE = new SerializableSerializer();

	protected SerializableSerializer() {

	}

	@Override
	public Serializable deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		if (json == null || json.isJsonNull())
			return null;
		Bytes bytes = context.deserialize(json, Bytes.class);
		return Utils.Bits.deserialize(bytes);
	}

	@Override
	public JsonElement serialize(Serializable src, Type typeOfSrc, JsonSerializationContext context) {
		if (src == null)
			return JsonNull.INSTANCE;
		return context.serialize(Utils.Bits.serialize(src), Bytes.class);
	}

}
