package com.lfp.joe.serial.gson;

import java.lang.reflect.Type;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;

import org.apache.commons.lang3.Validate;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.ImmutableEntry;

public enum EntryJsonSerializer implements JsonSerializer<Entry<?, ?>>, JsonDeserializer<Entry<?, ?>> {
	INSTANCE;

	private static final String KEY_NAME = "key";
	private static final String VALUE_NAME = "value";

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Entry<?, ?> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		if (json == null || json.isJsonNull())
			return null;
		JsonObject jo = json.getAsJsonObject();
		if (jo == null)
			return null;
		JsonElement keyJe = jo.get(KEY_NAME);
		JsonElement valueJe = jo.get(VALUE_NAME);
		Type[] typeArguments = JavaCode.Reflections.getTypeArguments(typeOfT, Entry.class, 2);
		Validate.isTrue(typeArguments != null && typeArguments.length == 2, "could not find entry type arguments:%s",
				typeOfT);
		Object key = context.deserialize(keyJe, typeArguments[0]);
		Object value = context.deserialize(valueJe, typeArguments[1]);
		Class<?> classType = Utils.Types.toClass(typeOfT);
		if (classType != null && ImmutableEntry.class.isAssignableFrom(classType))
			return Utils.Lots.entry(key, value);
		return new SimpleEntry(key, value);
	}

	@Override
	public JsonElement serialize(Entry<?, ?> src, Type typeOfSrc, JsonSerializationContext context) {
		if (src == null)
			return JsonNull.INSTANCE;
		Type[] typeArguments = JavaCode.Reflections.getTypeArguments(typeOfSrc, Entry.class, 2);
		JsonObject jo = new JsonObject();
		if (typeArguments != null && typeArguments.length == 2) {
			jo.add(KEY_NAME, context.serialize(src.getKey(), typeArguments[0]));
			jo.add(VALUE_NAME, context.serialize(src.getValue(), typeArguments[1]));
		} else {
			jo.add(KEY_NAME, context.serialize(src.getKey()));
			jo.add(VALUE_NAME, context.serialize(src.getValue()));
		}
		return jo;
	}

}
