package com.lfp.joe.serial.gson.serializer.selector;

import com.google.gson.JsonElement;

import io.gsonfire.TypeSelector;

public interface TypeSelectorLFP<T> extends TypeSelector<T> {

	default Class<? extends T> getClassForElement(JsonElement readElement) {
		return getClassForElement(null, readElement);
	}

	Class<? extends T> getClassForElement(Class<? extends T> clazz, JsonElement readElement);

}
