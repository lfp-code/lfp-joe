package com.lfp.joe.serial.gson;

import java.lang.reflect.Constructor;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.github.throwable.beanref.BeanRef;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

public enum ThrowableJsonSerializer implements JsonSerializer<Throwable>, JsonDeserializer<Throwable> {
	INSTANCE;

	public JsonElement serialize(Throwable src) {
		Type typeOfSrc = src != null ? src.getClass() : Throwable.class;
		return serialize(src, typeOfSrc, Serials.Gsons.serializationContext(Serials.Gsons.get()));
	}

	@Override
	public JsonElement serialize(Throwable src, Type typeOfSrc, JsonSerializationContext context) {
		return serializeInternal(src, context);
	}

	private JsonElement serializeInternal(Throwable t, JsonSerializationContext context) {
		if (t == null)
			return JsonNull.INSTANCE;
		JsonObject jo = new JsonObject();
		jo.addProperty(Serials.Gsons.CLASS_TYPE_FIELD_NAME, t.getClass().getName());
		jo.addProperty(BeanRef.$(Exception::getMessage).getPath(), t.getMessage());
		JsonArray steArr;
		{
			steArr = new JsonArray();
			Utils.Lots.stream(t.getStackTrace()).nonNull().map(v -> serialize(v, context)).nonNull()
					.forEach(v -> steArr.add(v));
		}
		if (steArr.size() > 0)
			jo.add(BeanRef.$(Exception::getStackTrace).getPath(), steArr);
		jo.add(BeanRef.$(Exception::getCause).getPath(), serializeInternal(t.getCause(), context));
		return jo;

	}

	private JsonElement serialize(StackTraceElement stackTraceElement, JsonSerializationContext context) {
		if (stackTraceElement == null)
			return JsonNull.INSTANCE;
		var ste = context.serialize(stackTraceElement, StackTraceElement.class);
		return ste;
	}

	@Override
	public Throwable deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		return deserializeInternal(json, context);
	}

	private Throwable deserializeInternal(JsonElement json, JsonDeserializationContext context) {
		if (json == null || !json.isJsonObject())
			return null;
		var classTypeStr = Serials.Gsons.tryGetAsString(json, Serials.Gsons.CLASS_TYPE_FIELD_NAME).orElse(null);
		var message = Serials.Gsons.tryGetAsString(json, BeanRef.$(Exception::getMessage).getPath()).orElse(null);
		List<StackTraceElement> stackTraceElements;
		{
			JsonArray steArr = Serials.Gsons.tryGetAsJsonArray(json, BeanRef.$(Exception::getStackTrace).getPath())
					.orElse(null);
			if (steArr == null || steArr.size() == 0)
				stackTraceElements = Collections.emptyList();
			else {
				stackTraceElements = new ArrayList<>();
				for (var steJe : steArr) {
					StackTraceElement ste = Utils.Functions
							.catching(() -> context.deserialize(steJe, StackTraceElement.class), t -> null);
					if (ste != null)
						stackTraceElements.add(ste);
				}
			}
		}
		var causeJe = Serials.Gsons.tryGetAsJsonObject(json, BeanRef.$(Exception::getCause).getPath()).orElse(null);
		var cause = deserializeInternal(causeJe, context);
		Throwable result = null;
		for (var ct : Arrays.asList(JavaCode.Reflections.tryForName(classTypeStr).orElse(null), Exception.class)) {
			if (ct == null)
				continue;
			Constructor<?> constructor = Utils.Functions.catching(() -> ct.getConstructor(String.class), t -> null);
			if (constructor == null)
				continue;
			result = Utils.Functions.catching(() -> (Throwable) constructor.newInstance(message), t -> null);
			if (result != null)
				break;
		}
		if (result == null)
			return null;
		if (stackTraceElements.size() > 0)
			result.setStackTrace(Utils.Lots.stream(stackTraceElements).toArray(StackTraceElement.class));
		if (cause != null)
			result.initCause(cause);
		return result;

	}

}
