package com.lfp.joe.serial.gson;

import com.google.gson.GsonBuilder;

public interface GsonBuilderModifier {

	public GsonBuilder apply(GsonBuilder gsonBuilder);
}
