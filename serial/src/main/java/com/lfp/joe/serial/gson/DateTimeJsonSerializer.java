package com.lfp.joe.serial.gson;

import java.lang.reflect.Type;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.joestelmach.natty.DateGroup;
import com.joestelmach.natty.Parser;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.log.LogFilter;
import com.lfp.joe.utils.Utils;

import ch.qos.logback.classic.Level;
import ch.qos.logback.core.spi.FilterReply;

public enum DateTimeJsonSerializer implements JsonSerializer<Date>, JsonDeserializer<Date> {
	INSTANCE;

	public static final String DATE_KEY = "$date";
	private static final ZoneId GMT_ZONE_ID = ZoneId.of("GMT");
	// to make compatible with mongo
	private static final DateTimeFormatter GMT_DATE_TIME_FORMATTER = DateTimeFormatter
			.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(GMT_ZONE_ID);
	private static final ZoneId UTC_ZONE_ID = ZoneId.of("UTC");
	private static final DateTimeFormatter ISO_DATE_TIME_FORMATTER = DateTimeFormatter
			.ofPattern("yyyy-MM-dd'T'HH:mm:ssX").withZone(UTC_ZONE_ID);
	private static final DateTimeFormatter COMMON_DATE_01 = DateTimeFormatter.ofPattern("MMM d, yyyy h:mm:ss a")
			.withZone(UTC_ZONE_ID);
	private static final MemoizedSupplier<Parser> DATE_PARSER_S = Utils.Functions.memoize(() -> {
		LogFilter.addFilter(ctx -> {
			if (ctx.getEvent().getLoggerName().equals(Parser.class.getName())
					&& ctx.getEvent().getLevel().equals(Level.INFO))
				return FilterReply.DENY;
			return FilterReply.NEUTRAL;
		});
		return new Parser();
	});

	@Override
	public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
		if (src == null)
			return null;
		JsonObject jsonObject = new JsonObject();
		ZonedDateTime zdt = ZonedDateTime.ofInstant(src.toInstant(), GMT_ZONE_ID);
		jsonObject.addProperty(DATE_KEY, GMT_DATE_TIME_FORMATTER.format(zdt));
		return jsonObject;
	}

	@Override
	public Date deserialize(JsonElement jsonElement, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		if (jsonElement == null || jsonElement.isJsonNull())
			// null only
			return null;
		Date result = deserializeNonNull(jsonElement);
		if (result == null)
			throw new JsonParseException("Could not parse date from: " + jsonElement);
		return result;
	}

	private Date deserializeNonNull(JsonElement jsonElement) {
		String str = getAsNonBlankString(jsonElement);
		if (str == null)
			return null;
		for (DateTimeFormatter dtf : Arrays.asList(GMT_DATE_TIME_FORMATTER, ISO_DATE_TIME_FORMATTER, COMMON_DATE_01)) {
			Instant instant;
			try {
				TemporalAccessor ta = dtf.parse(str);
				ZonedDateTime zdt = ZonedDateTime.from(ta);
				instant = zdt.toInstant();
			} catch (Exception e) {
				// suppress
				instant = null;
			}
			if (instant != null)
				return Date.from(Instant.from(instant));
		}
		boolean numeric = true;
		for (char c : str.toCharArray()) {
			if (!Character.isDigit(c)) {
				numeric = false;
				break;
			}
		}
		if (numeric)
			try {
				return new Date(Long.parseLong(str));
			} catch (Exception e) {
				// suppress
			}
		List<DateGroup> dateGroups = null;
		try {
			// last ditch expensive effort
			dateGroups = DATE_PARSER_S.get().parse(str);
		} catch (Exception e) {
			// suppress
		}
		Date parsed = Utils.Lots.stream(dateGroups).nonNull().map(dg -> dg.getDates()).nonNull().flatCollection(c -> c)
				.nonNull().findFirst().orElse(null);
		return parsed;
	}

	private static String getAsNonBlankString(JsonElement jsonElement) {
		if (jsonElement == null)
			return null;
		if (jsonElement.isJsonObject()) {
			JsonElement dateJe = jsonElement.getAsJsonObject().get(DATE_KEY);
			String str = getAsNonBlankString(dateJe);
			if (str != null)
				return str;
		}
		if (!jsonElement.isJsonPrimitive())
			return null;
		String str = jsonElement.getAsString();
		if (str == null)
			return null;
		str = str.trim();
		if (str.isEmpty())
			return null;
		return str;
	}

}