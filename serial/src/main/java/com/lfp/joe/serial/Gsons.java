package com.lfp.joe.serial;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.regex.Pattern;

import org.apache.commons.lang3.Validate;

import com.akivamu.jsonmerger.JsonMerger;
import com.github.throwable.beanref.BeanProperty;
import com.google.common.collect.Maps;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.annotations.SerializedName;
import com.google.gson.internal.bind.ReflectiveTypeAdapterFactory;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.ImmutableMemberCache.MethodRequest;
import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.serial.gson.GsonBuilders;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Functions;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

@SuppressWarnings({ "unchecked" })
public class Gsons {

	public static final String CLASS_TYPE_FIELD_NAME = "@class_type";
	public static final String TYPE_FIELD_NAME = "@type";
	public static final String VALUE_FIELD_NAME = "@value";

	private static final String GSON_INSTANCE_KEY = Streams.of(Gson.class.getName(), UUID.randomUUID()).joining("_");
	private static final String GSON_INSTANCE_PRETTY_KEY = Streams.of(Gson.class.getName(), "pretty", UUID.randomUUID())
			.joining("_");
	private static final String GSON_INSTANCE_STANDARD_KEY = Streams
			.of(Gson.class.getName(), "standard", UUID.randomUUID())
			.joining("_");
	private static final String GSON_INSTANCE_LENIENT_KEY = Streams
			.of(Gson.class.getName(), "lenient", UUID.randomUUID())
			.joining("_");
	private static final String JSON_PARSER_INSTANCE_KEY = Streams.of(JsonParser.class.getName(), UUID.randomUUID())
			.joining("_");

	private static final Class<?>[] GSON_PRIMITIVE_TYPES = { int.class, long.class, short.class, float.class,
			double.class, byte.class, boolean.class, char.class, Integer.class, Long.class, Short.class, Float.class,
			Double.class, Byte.class, Boolean.class, Character.class };
	private static final Field Gson_factories_FIELD = MemberCache
			.tryGetField(FieldRequest.of(Gson.class, List.class, "factories"), false)
			.get();
	private static final Method ReflectiveTypeAdapterFactory_getFieldNames_METHOD = MemberCache.tryGetMethod(
			MethodRequest.of(ReflectiveTypeAdapterFactory.class, List.class, "getFieldNames", Field.class), false)
			.get();
	private static final Field JsonPrimitive_value_FIELD = MemberCache
			.tryGetField(FieldRequest.of(JsonPrimitive.class, Object.class, "value"), false)
			.get();
	private static final MemoizedSupplier<Set<Constructor<? extends JsonPrimitive>>> JsonPrimitive_CONSTRUCTORS_S = Functions
			.memoize(() -> {
				Set<Constructor<? extends JsonPrimitive>> result = new HashSet<>();
				JavaCode.Reflections.getAllConstructors(JsonPrimitive.class, c -> c.getParameterCount() == 1)
						.forEach(ctor -> {
							ctor.setAccessible(true);
							result.add(ctor);
						});
				return Collections.unmodifiableSet(result);
			});
	private static final MemoizedSupplier<Optional<Method>> JsonPrimitive_isPrimitiveOrString_FIELD_S = Utils.Functions
			.memoize(() -> {
				String methodName = "isPrimitiveOrString";
				List<Method> methods = JavaCode.Reflections
						.streamMethods(JsonPrimitive.class, true, m -> methodName.equals(m.getName()),
								m -> m.getParameterCount() == 1)
						.limit(2)
						.toList();
				Validate.isTrue(methods.size() <= 1, "could not find method:{}", methodName);
				if (methods.size() == 0)
					return Optional.empty();
				var method = methods.get(0);
				method.setAccessible(true);
				return Optional.of(method);
			});

	public static GsonBuilder getBuilder() {
		return GsonBuilders.builderDefault();
	}

	public static com.google.gson.Gson get() {
		return Instances.get(GSON_INSTANCE_KEY, () -> GsonBuilders.builderDefault().create());
	}

	public static com.google.gson.Gson getPretty() {
		return Instances.get(GSON_INSTANCE_PRETTY_KEY,
				() -> GsonBuilders.builderDefault().setPrettyPrinting().create());
	}

	public static com.google.gson.Gson getStandard() {
		return Instances.get(GSON_INSTANCE_STANDARD_KEY, Gson::new);
	}

	public static com.google.gson.Gson getLenient() {
		return Instances.get(GSON_INSTANCE_LENIENT_KEY, () -> GsonBuilders.builderDefault().setLenient().create());
	}

	@SuppressWarnings("deprecation")
	public static JsonParser getJsonParser() {
		return Instances.get(JSON_PARSER_INSTANCE_KEY, JsonParser::new);
	}

	public static <X extends JsonElement> X deepKeySort(X je) {
		if (je == null)
			return je;
		if (je.isJsonNull())
			return je;
		if (!je.isJsonObject())
			return je;
		JsonObject jo = je.getAsJsonObject();
		JsonObject copy = new JsonObject();
		Utils.Lots.stream(jo.keySet()).sorted().forEach(k -> copy.add(k, deepKeySort(jo.get(k))));
		return (X) copy;
	}

	public static <X> Bytes toBytes(Object object) {
		return toBytes(object, null);
	}

	public static <X> Bytes toBytes(com.google.gson.Gson gson, Object object) {
		return toBytes(gson, object, null);
	}

	public static <X> Bytes toBytes(Object object, TypeToken<X> typeToken) {
		return toBytes(get(), object, typeToken);
	}

	public static <X> Bytes toBytes(com.google.gson.Gson gson, Object object, TypeToken<X> typeToken) {
		Objects.requireNonNull(gson);
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
				OutputStreamWriter osw = new OutputStreamWriter(baos, Utils.Bits.getDefaultCharset());) {
			if (typeToken == null)
				gson.toJson(object, osw);
			else
				gson.toJson(object, typeToken.getType(), osw);
			osw.flush();
			return Utils.Bits.from(baos.toByteArray());
		} catch (IOException e) {
			throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
					? java.lang.RuntimeException.class.cast(e)
					: new java.lang.RuntimeException(e);
		}
	}

	public static <X> X fromBytes(Bytes bytes, Class<X> classType) {
		return fromBytes(get(), bytes, classType);
	}

	public static <X> X fromBytes(Bytes bytes, TypeToken<X> typeToken) {
		return fromBytes(get(), bytes, typeToken);
	}

	public static <X> X fromBytes(com.google.gson.Gson gson, Bytes bytes, Class<X> classType) {
		return fromBytes(gson, bytes, classType == null ? null : TypeToken.of(classType));
	}

	public static <X> X fromBytes(com.google.gson.Gson gson, Bytes bytes, TypeToken<X> typeToken) {
		try {
			return fromStream(gson, bytes == null ? null : bytes.inputStream(), typeToken);
		} catch (IOException e) {
			throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
					? java.lang.RuntimeException.class.cast(e)
					: new java.lang.RuntimeException(e);
		}
	}

	public static <X> X fromStream(InputStream inputStream, Class<X> classType) throws IOException {
		return fromStream(get(), inputStream, classType);
	}

	public static <X> X fromStream(InputStream inputStream, TypeToken<X> typeToken) throws IOException {
		return fromStream(get(), inputStream, typeToken);
	}

	public static <X> X fromStream(com.google.gson.Gson gson, InputStream inputStream, Class<X> classType)
			throws IOException {
		return fromStream(gson, inputStream, classType == null ? null : TypeToken.of(classType));
	}

	public static <X> X fromStream(com.google.gson.Gson gson, InputStream inputStream, TypeToken<X> typeToken)
			throws IOException {
		return fromStream(gson, inputStream, typeToken, false);
	}

	public static <X> X fromStream(com.google.gson.Gson gson, InputStream inputStream, TypeToken<X> typeToken,
			boolean disableClose) throws IOException {
		if (inputStream == null)
			inputStream = Utils.Bits.emptyInputStream();
		Reader reader = Utils.Bits.bufferedReader(inputStream);
		try {
			Objects.requireNonNull(gson);
			return gson.fromJson(reader, typeToken == null ? null : typeToken.getType());
		} finally {
			if (!disableClose) {
				reader.close();
				inputStream.close();
			}
		}
	}

	public static <X> void toStream(X src, OutputStream outputStream) throws IOException {
		toStream(get(), src, outputStream);
	}

	public static <C, X extends C> void toStream(X src, Class<C> classType, OutputStream outputStream)
			throws IOException {
		toStream(get(), src, classType, outputStream);
	}

	public static <C, X extends C> void toStream(X src, TypeToken<C> typeToken, OutputStream outputStream)
			throws IOException {
		toStream(get(), src, typeToken, outputStream);
	}

	public static <X> void toStream(com.google.gson.Gson gson, X src, OutputStream outputStream) throws IOException {
		toStream(gson, src, (TypeToken<X>) null, outputStream);
	}

	public static <C, X extends C> void toStream(com.google.gson.Gson gson, X src, Class<C> classType,
			OutputStream outputStream) throws IOException {
		toStream(gson, src, classType == null ? null : TypeToken.of(classType), outputStream);
	}

	public static <C, X extends C> void toStream(com.google.gson.Gson gson, X src, TypeToken<C> typeToken,
			OutputStream outputStream) throws IOException {
		try (var writer = Utils.Bits.bufferedWriter(outputStream)) {
			if (typeToken == null)
				gson.toJson(src, writer);
			else
				gson.toJson(src, typeToken.getType(), writer);
		}
	}

	public static JsonElement sortJsonObjectKeys(JsonElement je) {
		if (je == null)
			return je;
		if (je.isJsonArray()) {
			for (JsonElement val : je.getAsJsonArray())
				sortJsonObjectKeys(val);
		}
		if (je.isJsonObject()) {
			Map<String, JsonElement> temp = Maps.newHashMap();
			JsonObject jo = je.getAsJsonObject();
			while (jo.size() > 0) {
				String key = jo.keySet().iterator().next();
				JsonElement val = jo.remove(key);
				sortJsonObjectKeys(val);
				temp.put(key, val);
			}
			Set<String> sortedKeys = Utils.Lots.stream(temp.keySet()).sorted().toSet();
			for (String key : sortedKeys)
				jo.add(key, temp.remove(key));
		}
		return je;
	}

	public static Optional<JsonElement> tryGet(JsonElement je, Object... keys) {
		if (je == null)
			return Optional.empty();
		if (je.isJsonNull())
			return Optional.of(je);
		if (keys == null || keys.length == 0)
			return Optional.of(je);
		{
			boolean dotAdjust = false;
			for (Object k : keys) {
				String str = Utils.Types.tryCast(k, String.class).orElse(null);
				if (str != null && str.contains(".")) {
					dotAdjust = true;
					break;
				}
			}
			if (dotAdjust)
				keys = Utils.Lots.stream(keys).map(k -> {
					String str = Utils.Types.tryCast(k, String.class).orElse(null);
					if (str != null && str.contains(".")) {
						return Utils.Lots.stream(str.split(Pattern.quote(".")));
					}
					return StreamEx.of(k);
				}).flatMap(s -> s).toArray(Object.class);
		}
		Object obj = keys[0];
		if (obj instanceof BeanProperty)
			obj = ((BeanProperty<?, ?>) obj).getName();
		Object[] keysF = keys;
		Supplier<Object[]> nextKeys = () -> keysF.length == 1 ? new Object[0]
				: Arrays.copyOfRange(keysF, 1, keysF.length);
		if (obj instanceof Number) {
			int index = ((Number) obj).intValue();
			if (index < 0)
				return Optional.empty();
			if (!je.isJsonArray())
				return Optional.empty();
			JsonArray jarr = je.getAsJsonArray();
			if (jarr.size() <= index)
				return Optional.empty();
			return tryGet(jarr.get(index), nextKeys.get());
		} else if (obj instanceof String) {
			String key = (String) obj;
			if (Utils.Strings.isBlank(key))
				return Optional.empty();
			if (!je.isJsonObject())
				return Optional.empty();
			return tryGet(je.getAsJsonObject().get(key), nextKeys.get());
		}
		return Optional.empty();
	}

	public static Optional<JsonObject> tryGetAsJsonObject(JsonElement je, Object... keys) {
		return tryGet(je, keys).filter(JsonElement::isJsonObject).map(JsonElement::getAsJsonObject);
	}

	public static Optional<JsonArray> tryGetAsJsonArray(JsonElement je, Object... keys) {
		return tryGet(je, keys).filter(JsonElement::isJsonArray).map(JsonElement::getAsJsonArray);
	}

	public static Optional<JsonPrimitive> tryGetAsPrimitive(JsonElement je, Object... keys) {
		return tryGet(je, keys).filter(JsonElement::isJsonPrimitive).map(JsonElement::getAsJsonPrimitive);
	}

	public static Optional<String> tryGetAsString(JsonElement je, Object... keys) {
		return tryGetAsPrimitive(je, keys).filter(JsonPrimitive::isString).map(JsonElement::getAsString);
	}

	public static Optional<Number> tryGetAsNumber(JsonElement je, Object... keys) {
		return tryGetAsPrimitive(je, keys).filter(JsonPrimitive::isNumber).map(JsonElement::getAsNumber);
	}

	public static Object getPrimitiveValue(JsonPrimitive jsonPrimitive) {
		Objects.requireNonNull(jsonPrimitive);
		return Throws.unchecked(() -> JsonPrimitive_value_FIELD.get(jsonPrimitive));
	}

	public static <U> U setPrimitiveValue(JsonPrimitive jsonPrimitive, U value) {
		Objects.requireNonNull(jsonPrimitive);
		Objects.requireNonNull(value);
		Throws.unchecked(() -> JsonPrimitive_value_FIELD.set(jsonPrimitive, value));
		return value;
	}

	public static Optional<JsonPrimitive> tryCreateJsonPrimitive(Object object) {
		if (object == null)
			return Optional.empty();
		var objectClassType = Utils.Types.primitiveToWrapper(object.getClass());
		for (Constructor<? extends JsonPrimitive> ctor : JsonPrimitive_CONSTRUCTORS_S.get()) {
			var paramClassType = ctor.getParameterTypes()[0];
			paramClassType = Utils.Types.primitiveToWrapper(paramClassType);
			if (paramClassType.isAssignableFrom(objectClassType)) {
				JsonPrimitive jp = Utils.Functions.unchecked(() -> ctor.newInstance(object));
				return Optional.of(jp);
			}
		}
		return Optional.empty();
	}

	public static String getMemberName(Member member) {
		return getMemberNames(member).findFirst().orElseThrow(() -> {
			throw new IllegalArgumentException("member name not found:" + member);
		});
	}

	public static StreamEx<String> getMemberNames(Member member) {
		return getMemberNames(null, member);
	}

	public static StreamEx<String> getMemberNames(Gson gson, Member member) {
		if (member == null)
			return Streams.empty();
		var nameStream = getMemberAnnotationNames(gson, member, null);
		nameStream = nameStream.append(member.getName());
		nameStream = nameStream.filter(Utils.Strings::isNotBlank).distinct();
		return nameStream;
	}

	@SuppressWarnings("resource")
	private static StreamEx<String> getMemberAnnotationNames(Gson gson, Member member,
			Set<Class<?>> visitedClassTypes) {
		StreamEx<String> nameStream = Streams.empty();
		if (member instanceof Field) {
			Field field = (Field) member;
			nameStream = nameStream.append(Streams.of(() -> {
				StreamEx<ReflectiveTypeAdapterFactory> rtafStream = Streams.of(Throws.supplierUnchecked(() -> {
					var fieldValue = Gson_factories_FIELD.get(Optional.ofNullable(gson).orElseGet(Serials.Gsons::get));
					return (List<TypeAdapterFactory>) fieldValue;
				})).select(ReflectiveTypeAdapterFactory.class);
				rtafStream = rtafStream.nonNull();
				return rtafStream.flatCollection(Throws.functionUnchecked(v -> {
					return (List<String>) ReflectiveTypeAdapterFactory_getFieldNames_METHOD.invoke(v, field);
				}));
			}));
		}
		nameStream = nameStream.append(Streams.of(() -> {
			Annotation[] annos;
			if (member instanceof AnnotatedElement)
				annos = ((AnnotatedElement) member).getAnnotations();
			else
				annos = null;
			StreamEx<SerializedName> serializedNames = Streams.of(annos).select(SerializedName.class);
			return serializedNames.map(SerializedName::value);
		}));
		if (!(member instanceof Method))
			return nameStream;
		{// check if we found method annos
			var nameIter = nameStream.iterator();
			if (nameIter.hasNext())
				return Streams.of(nameIter);
		}
		// check supers
		Method method = (Method) member;
		var declaringClassType = method.getDeclaringClass();
		if (visitedClassTypes == null) {
			visitedClassTypes = new HashSet<>();
			visitedClassTypes.add(declaringClassType);
		}
		for (var type : Streams.of(CoreReflections.streamTypes(declaringClassType))) {
			if (!visitedClassTypes.add(type))
				continue;
			var superMethod = MemberCache.tryGetMethod(MethodRequest.of(type, null, method.getName()), true)
					.orElse(null);
			if (superMethod == null || !superMethod.getReturnType().isAssignableFrom(method.getReturnType()))
				continue;
			return getMemberAnnotationNames(gson, superMethod, visitedClassTypes);
		}
		return Streams.empty();
	}

	public static boolean isJsonPrimitive(Object target) {
		if (target instanceof Number)
			return true;
		if (target instanceof String)
			return true;
		var method = JsonPrimitive_isPrimitiveOrString_FIELD_S.get().orElse(null);
		if (method != null)
			return (boolean) Utils.Functions.unchecked(() -> method.invoke(null, target));
		Class<?> classOfPrimitive = target.getClass();
		for (Class<?> standardPrimitive : GSON_PRIMITIVE_TYPES)
			if (standardPrimitive.isAssignableFrom(classOfPrimitive))
				return true;
		return false;
	}

	public static EntryStream<Optional<String>, JsonElement> streamElements(JsonElement jsonElement) {
		return streamElementsInternal(Optional.empty(), jsonElement).mapToEntry(Entry::getKey, Entry::getValue);
	}

	protected static StreamEx<Entry<Optional<String>, JsonElement>> streamElementsInternal(Optional<String> keyOp,
			JsonElement valueElement) {
		if (valueElement == null)
			return StreamEx.empty();
		StreamEx<StreamEx<Entry<Optional<String>, JsonElement>>> streams = Utils.Lots.stream(il -> {
			if (il.getIndex() == 0)
				return StreamEx.of(Utils.Lots.entry(keyOp, valueElement));
			if (il.getIndex() == 1) {
				List<StreamEx<Entry<Optional<String>, JsonElement>>> result = new ArrayList<>();
				if (valueElement.isJsonArray()) {
					JsonArray jarr = valueElement.getAsJsonArray();
					for (int i = 0; i < jarr.size(); i++) {
						JsonElement subJe = jarr.get(i);
						result.add(streamElementsInternal(Optional.of(String.format("[%s]", i)), subJe));
					}
				} else if (valueElement.isJsonObject()) {
					JsonObject jo = valueElement.getAsJsonObject();
					for (String joKey : jo.keySet()) {
						JsonElement subJe = jo.get(joKey);
						result.add(streamElementsInternal(Optional.of(joKey), subJe));
					}
				}
				return Utils.Lots.flatMap(result);
			}
			return il.end();
		});
		return Utils.Lots.flatMap(streams);
	}

	public static JsonObject merge(JsonObject... jsonObjects) {
		return merge(null, jsonObjects);
	}

	public static JsonObject merge(Consumer<JsonMerger> jsonMergerConfig, JsonObject... jsonObjects) {
		List<JsonObject> remaining = new ArrayList<>();
		JsonObject originObject = null;
		JsonObject extObject = null;
		for (int i = 0; jsonObjects != null && i < jsonObjects.length; i++) {
			if (i == 0)
				originObject = jsonObjects[i];
			else if (i == 1)
				extObject = jsonObjects[i];
			else
				remaining.add(jsonObjects[i]);
		}
		if (originObject == null)
			originObject = new JsonObject();
		if (extObject == null)
			extObject = new JsonObject();
		var jsonMerger = new JsonMerger();
		if (jsonMergerConfig != null)
			jsonMergerConfig.accept(jsonMerger);
		JsonObject merged = jsonMerger.merge(originObject, extObject);
		if (remaining.isEmpty())
			return merged;
		return merge(jsonMergerConfig, Utils.Lots.stream(merged).append(remaining).toArray(JsonObject.class));
	}

	public static Bytes deepHash(JsonElement je) {
		var md = Utils.Crypto.getMessageDigestMD5();
		deepHash(md, je);
		return Bytes.from(md.digest());
	}

	public static void deepHash(MessageDigest messageDigest, JsonElement je) {
		if (je == null || je.isJsonNull())
			return;
		if (je.isJsonPrimitive()) {
			Object val = getPrimitiveValue(je.getAsJsonPrimitive());
			if (val == null)
				return;
			var bytes = Utils.Bits.tryFrom(val).get();
			messageDigest.update(bytes.array());
		} else if (je.isJsonArray()) {
			for (JsonElement subJe : je.getAsJsonArray())
				deepHash(messageDigest, subJe);
		} else if (je.isJsonObject()) {
			JsonObject jo = je.getAsJsonObject();
			for (String key : StreamEx.of(jo.keySet()).sorted()) {
				messageDigest.update(key.getBytes(Utils.Bits.getDefaultCharset()));
				deepHash(messageDigest, jo.get(key));
			}
		}
	}

	public static void serializeClassType(JsonElement jsonElement, Class<?> classType, boolean overwrite) {
		org.apache.commons.lang3.Validate.isTrue(jsonElement != null && jsonElement.isJsonObject(),
				"invalid json element");
		Objects.requireNonNull(classType);
		if (!overwrite) {
			var classTypeStr = tryGetAsString(jsonElement, CLASS_TYPE_FIELD_NAME).orElse(null);
			if (Utils.Strings.isNotBlank(classTypeStr))
				return;
		}
		jsonElement.getAsJsonObject().addProperty(CLASS_TYPE_FIELD_NAME, classType.getName());
	}

	public static Optional<Class<?>> deserializeClassType(JsonElement jsonElement) {
		var classTypeStr = tryGetAsString(jsonElement, CLASS_TYPE_FIELD_NAME).orElse(null);
		return JavaCode.Reflections.tryForName(classTypeStr);
	}

	public static JsonSerializationContext serializationContext(Gson gson) {
		Objects.requireNonNull(gson);
		return new JsonSerializationContext() {

			@Override
			public JsonElement serialize(Object src) {
				return gson.toJsonTree(src);
			}

			@Override
			public JsonElement serialize(Object src, Type typeOfSrc) {
				return gson.toJsonTree(src, typeOfSrc);
			}
		};
	}

	public static JsonDeserializationContext deserializationContext(Gson gson) {
		Objects.requireNonNull(gson);
		return new JsonDeserializationContext() {

			@Override
			public <T> T deserialize(JsonElement json, Type typeOfT) throws JsonParseException {
				return gson.fromJson(json, typeOfT);
			}
		};
	}

	public static void main(String[] args) {
		var bool = new AtomicBoolean();
		for (int i = 0; i < 10; i++) {
			var mod = bool.compareAndSet(false, true);
			System.out.println(mod);
		}
	}
}
