package com.lfp.joe.serial.gson;

import java.lang.reflect.Type;

import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import at.favre.lib.bytes.Bytes;

public enum BytesSerializer implements JsonSerializer<Bytes>, JsonDeserializer<Bytes> {
	INSTANCE;

	@Override
	public Bytes deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		if (json == null || json.isJsonNull())
			return null;
		var str = Serials.Gsons.tryGetAsString(json).orElse(null);
		if (Utils.Strings.isBlank(str))
			return null;
		return Utils.Bits.parseBase64(str);
	}

	@Override
	public JsonElement serialize(Bytes src, Type typeOfSrc, JsonSerializationContext context) {
		if (src == null)
			return JsonNull.INSTANCE;
		return new JsonPrimitive(src.encodeBase64());
	}

}
