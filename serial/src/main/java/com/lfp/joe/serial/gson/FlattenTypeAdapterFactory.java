package com.lfp.joe.serial.gson;

import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.apache.commons.lang3.Validate;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

import one.util.streamex.EntryStream;

public class FlattenTypeAdapterFactory implements TypeAdapterFactory {

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.FIELD)
	public static @interface Flatten {}

	public static final FlattenTypeAdapterFactory INSTANCE = new FlattenTypeAdapterFactory();

	private static final Cache<Class<?>, List<Entry<String, Field>>> NAME_TO_FIELDS_CACHE = Caffeine.newBuilder()
			.softValues()
			.build();

	private FlattenTypeAdapterFactory() {}

	@Override
	public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
		List<Entry<String, Field>> nameToFields = streamNameToFields(type).toImmutableList();
		if (nameToFields.isEmpty())
			return null;
		var delegateAdapter = gson.getDelegateAdapter(this, type);
		var jsonElementAdapterSupplier = Utils.Functions.memoize(() -> gson.getAdapter(JsonElement.class));
		return new TypeAdapter<T>() {

			@Override
			public void write(JsonWriter out, T value) throws IOException {
				var je = delegateAdapter.toJsonTree(value);
				flatten(type, nameToFields, je);
				jsonElementAdapterSupplier.get().write(out, je);
			}

			@Override
			public T read(JsonReader in) throws IOException {
				var je = jsonElementAdapterSupplier.get().read(in);
				var value = delegateAdapter.fromJsonTree(je);
				unflatten(gson, type, nameToFields, je, value);
				return value;
			}

		};
	}

	private EntryStream<String, Field> streamNameToFields(TypeToken<?> type) {
		var classType = Optional.ofNullable(type)
				.map(TypeToken::getRawType)
				.map(CoreReflections::getNamedClassType)
				.orElse(null);
		if (classType == null)
			return EntryStream.empty();
		var elist = NAME_TO_FIELDS_CACHE.get(classType, k -> loadEntries(k));
		return EntryStreams.of(elist);
	}

	private <T> void unflatten(Gson gson, TypeToken<T> type, Iterable<Entry<String, Field>> nameToFieldIble,
			JsonElement je, T value) {
		if (je == null || !je.isJsonObject() || value == null)
			return;
		var jo = je.getAsJsonObject();
		Map<Class<?>, TypeAdapter<?>> cache = new HashMap<>();
		for (var ent : nameToFieldIble) {
			var field = ent.getValue();
			var fieldAdapter = cache.computeIfAbsent(field.getType(), k -> gson.getAdapter(k));
			var fieldValue = fieldAdapter.fromJsonTree(jo);
			Utils.Functions.unchecked(() -> field.set(value, fieldValue));
		}
	}

	private void flatten(TypeToken<?> type, Iterable<Entry<String, Field>> nameToFieldIble, JsonElement je) {
		if (je == null || !je.isJsonObject())
			return;
		var jo = je.getAsJsonObject();
		for (var ent : nameToFieldIble) {
			var name = ent.getKey();
			flatten(type, name, jo);
		}
	}

	private void flatten(TypeToken<?> type, String name, JsonObject jo) {
		if (!jo.has(name))
			return;
		var value = jo.remove(name);
		if (value == null || value.isJsonNull())
			return;
		Validate.isTrue(value.isJsonObject(),
				"flatten failed, non json object. type:%s property:%s object:%s propertyValue:%s", type, name, jo,
				value);
		var valueJo = value.getAsJsonObject();
		for (var nestedKey : valueJo.keySet()) {
			Validate.isTrue(!jo.has(nestedKey),
					"flatten failed, property exists. type:%s property:%s object:%s propertyValue:%s", type, name, jo,
					value);
			var nestedValue = valueJo.get(nestedKey);
			jo.add(nestedKey, nestedValue);
		}
	}


	private static List<Entry<String, Field>> loadEntries(Class<?> classType) {
		return Streams.of(CoreReflections.streamFields(classType, true))
				.filter(v -> !Modifier.isStatic(v.getModifiers()))
				.filter(v -> v.getAnnotation(Flatten.class) != null)
				.peek(CoreReflections::setAccessible)
				.mapToEntry(v -> Serials.Gsons.getMemberName(v))
				.invert()
				.toImmutableList();
	}

}