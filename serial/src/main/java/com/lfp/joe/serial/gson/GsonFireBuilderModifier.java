package com.lfp.joe.serial.gson;

import io.gsonfire.GsonFireBuilder;

public interface GsonFireBuilderModifier {

	public GsonFireBuilder apply(GsonFireBuilder gsonFireBuilder);
}
