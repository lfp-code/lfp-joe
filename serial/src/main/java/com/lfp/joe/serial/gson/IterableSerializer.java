package com.lfp.joe.serial.gson;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;
import com.lfp.joe.reflections.JavaCode;

public enum IterableSerializer implements JsonSerializer<Iterable<?>>, JsonDeserializer<Iterable<?>> {
	INSTANCE;

	@Override
	public Iterable<?> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		if (json == null || json.isJsonNull())
			return null;
		var typeArgument = getTypeArgument(typeOfT).orElse(null);
		if (typeArgument == null)
			return context.deserialize(json, typeOfT);
		var listTT = TypeToken.getParameterized(List.class, typeArgument);
		return context.deserialize(json, listTT.getType());
	}

	@Override
	public JsonElement serialize(Iterable<?> src, Type typeOfSrc, JsonSerializationContext context) {
		if (src == null)
			return JsonNull.INSTANCE;
		var typeArgument = getTypeArgument(typeOfSrc).orElse(null);
		if (typeArgument == null)
			return context.serialize(src, typeOfSrc);
		var listTT = TypeToken.getParameterized(List.class, typeArgument);
		return context.serialize(src, listTT.getType());
	}

	private static Optional<Type> getTypeArgument(Type type) {
		var typeArgs = JavaCode.Reflections.getTypeArguments(type, Iterable.class, 1);
		if (typeArgs == null)
			return Optional.empty();
		return Optional.of(typeArgs[0]);
	}

}
