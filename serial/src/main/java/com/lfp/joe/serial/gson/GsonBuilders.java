package com.lfp.joe.serial.gson;

import java.lang.reflect.Constructor;
import java.lang.reflect.Type;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.ServiceLoader;
import java.util.ServiceLoader.Provider;

import com.github.throwable.beanref.BeanPath;
import com.google.common.collect.Multimap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonSerializer;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.lfp.joe.core.classpath.ImmutableMemberCache.ConstructorRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.serial.gson.serializer.selector.TypeSelectorTypeAdapterFactoryLFP;
import com.lfp.joe.serial.serializer.Serializer;
import com.lfp.joe.serial.serializer.SerializerContext;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import io.gsonfire.GsonFireBuilder;
import io.gsonfire.PostProcessor;
import io.gsonfire.PreProcessor;
import io.gsonfire.TypeSelector;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class GsonBuilders {

	protected GsonBuilders() {};

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String AUTO_WIRED_LOG_MESSAGE = String.format("%s classType:",
			Serializer.class.getSimpleName());
	private static final Constructor<GsonBuilder> GsonBuilder_Gson_CONSTRUCTOR = MemberCache
			.tryGetConstructor(ConstructorRequest.of(GsonBuilder.class, Gson.class), false)
			.get();
	private static final Map<Type, Object> DEFAULT_TYPE_ADAPTERS;
	static {
		var map = new LinkedHashMap<Type, Object>();
		map.put(Duration.class, DurationSerializer.INSTANCE);
		map.put(Class.class, ClassTypeSerializer.INSTANCE);
		map.put(Date.class, DateTimeJsonSerializer.INSTANCE);
		map.put(ByteBuffer.class, ByteBufferSerializer.INSTANCE);
		map.put(InetSocketAddress.class, InetSocketAddressJsonSerializer.INSTANCE);
		map.put(BeanPath.class, BeanPathSerializer.INSTANCE);
		DEFAULT_TYPE_ADAPTERS = Utils.Lots.unmodifiable(map);
	}
	private static final Map<Class<?>, Object> DEFAULT_TYPE_HIERARCHY_ADAPTERS;
	static {
		var map = new LinkedHashMap<Class<?>, Object>();
		map.put(Throwable.class, ThrowableJsonSerializer.INSTANCE);
		map.put(Multimap.class, MultimapJsonSerializer.INSTANCE);
		map.put(Entry.class, EntryJsonSerializer.INSTANCE);
		map.put(Bytes.class, BytesSerializer.INSTANCE);
		DEFAULT_TYPE_HIERARCHY_ADAPTERS = Utils.Lots.unmodifiable(map);
	}
	private static final List<Object> DEFAULT_ADAPTERS;
	static {
		var list = new ArrayList<Object>();
		list.add(EnumTypeAdapterFactory.INSTANCE);
		list.add(AnnotationMethodCaller.INSTANCE);
		list.add(InputStreamSourceSerializer.INSTANCE);
		list.add(FlattenTypeAdapterFactory.INSTANCE);
		list.add(StreamTypeAdapterFactory.INSTANCE);
		DEFAULT_ADAPTERS = Utils.Lots.unmodifiable(list);
	}
	private static final MemoizedSupplier<Gson> DEFAULT_SCAN_GSON_S = MemoizedSupplier.create(() -> {
		var gsonFireBuilder = createGsonFireBuilder();
		var serializerContexts = SerializerContext.stream().map(ctx -> {
			LOGGER.info("{}{}{}", AUTO_WIRED_LOG_MESSAGE, ctx.getAnnotatedType().getName(),
					Streams.of(ctx.getAnnotation().value()).map(Class::getName).toList());
			return ctx;
		}).toImmutableList();
		for (SerializerContext ctx : serializerContexts)
			gsonFireBuilder = modifyGsonFireBuilder(gsonFireBuilder, ctx);
		var gsonBuilder = createGsonBuilder(gsonFireBuilder);
		gsonBuilder = modifyGsonBuilder(gsonBuilder, ServiceLoader.load(TypeAdapterFactory.class));
		for (var ctx : serializerContexts)
			gsonBuilder = modifyGsonBuilder(gsonBuilder, ctx);
		TypeSelectorTypeAdapterFactoryLFP.replaceTypeSelectorTypeAdapters(gsonBuilder);
		return gsonBuilder.create();
	});

	public static GsonBuilder builderDefault() {
		return builder(DEFAULT_SCAN_GSON_S.get());
	}

	public static GsonBuilder builder(Gson gson) {
		if (gson == null)
			return new GsonBuilder();
		return Throws.unchecked(() -> GsonBuilder_Gson_CONSTRUCTOR.newInstance(gson));
	}

	public static GsonBuilder copy(GsonBuilder gsonBuilder) {
		return builder(Optional.ofNullable(gsonBuilder).map(GsonBuilder::create).orElse(null));
	}

	private static GsonFireBuilder createGsonFireBuilder() {
		var gsonFireBuilder = new GsonFireBuilder();
		for (var modifier : Streams.of(DEFAULT_ADAPTERS).select(GsonFireBuilderModifier.class))
			gsonFireBuilder = Optional.ofNullable(modifier.apply(gsonFireBuilder)).orElse(gsonFireBuilder);
		return gsonFireBuilder;
	}

	private static GsonFireBuilder modifyGsonFireBuilder(GsonFireBuilder gsonFireBuilder, SerializerContext ctx) {
		{
			var op = ctx.tryGetSerializerInstance(GsonFireBuilderModifier.class);
			if (op.isPresent())
				gsonFireBuilder = Optional.ofNullable(op.get().apply(gsonFireBuilder)).orElse(gsonFireBuilder);
		}
		{
			var op = ctx.tryGetSerializerInstance(PreProcessor.class);
			if (op.isPresent())
				for (Class<?> mt : ctx.getAnnotation().value())
					gsonFireBuilder = gsonFireBuilder.registerPreProcessor(mt, op.get());
		}
		{
			var op = ctx.tryGetSerializerInstance(PostProcessor.class);
			if (op.isPresent())
				for (Class<?> mt : ctx.getAnnotation().value())
					gsonFireBuilder = gsonFireBuilder.registerPostProcessor(mt, op.get());
		}
		{
			var op = ctx.tryGetSerializerInstance(TypeSelector.class);
			if (op.isPresent())
				for (Class<?> mt : ctx.getAnnotation().value())
					gsonFireBuilder = gsonFireBuilder.registerTypeSelector(mt, op.get());
		}
		return gsonFireBuilder;
	}

	private static GsonBuilder createGsonBuilder(GsonFireBuilder gsonFireBuilder) {
		var gsonBuilder = gsonFireBuilder.createGsonBuilder();
		gsonBuilder.enableComplexMapKeySerialization();
		gsonBuilder.disableHtmlEscaping();
		{
			for (var ent : EntryStreams.of(DEFAULT_TYPE_ADAPTERS))
				gsonBuilder = gsonBuilder.registerTypeAdapter(ent.getKey(), ent.getValue());
		}
		{
			for (var ent : EntryStreams.of(DEFAULT_TYPE_HIERARCHY_ADAPTERS))
				gsonBuilder = gsonBuilder.registerTypeHierarchyAdapter(ent.getKey(), ent.getValue());
		}
		{
			for (var modifier : Streams.of(DEFAULT_ADAPTERS).select(GsonBuilderModifier.class))
				gsonBuilder = Optional.ofNullable(modifier.apply(gsonBuilder)).orElse(gsonBuilder);
		}
		{
			for (var taf : Streams.of(DEFAULT_ADAPTERS).select(TypeAdapterFactory.class))
				gsonBuilder = gsonBuilder.registerTypeAdapterFactory(taf);
		}

		return gsonBuilder;
	}

	private static GsonBuilder modifyGsonBuilder(GsonBuilder gsonBuilder,
			ServiceLoader<TypeAdapterFactory> serviceLoader) {
		for (var tafService : Streams.of(serviceLoader.stream()).distinct(Provider::type).map(Provider::get)) {
			LOGGER.info("{}{}", AUTO_WIRED_LOG_MESSAGE, tafService.getClass().getName());
			gsonBuilder = gsonBuilder.registerTypeAdapterFactory(tafService);
		}
		return gsonBuilder;

	}

	private static GsonBuilder modifyGsonBuilder(GsonBuilder gsonBuilder, SerializerContext ctx) {
		{
			var op = ctx.tryGetSerializerInstance(GsonBuilderModifier.class);
			if (op.isPresent())
				gsonBuilder = Optional.ofNullable(op.get().apply(gsonBuilder)).orElse(gsonBuilder);
		}
		{
			var op = ctx.tryGetSerializerInstance(TypeAdapterFactory.class);
			if (op.isPresent())
				gsonBuilder = gsonBuilder.registerTypeAdapterFactory(op.get());
		}
		for (var ct : List.of(JsonSerializer.class, JsonDeserializer.class, TypeAdapter.class)) {
			var op = ctx.tryGetSerializerInstance(ct);
			if (op.isPresent()) {
				for (Class<?> mappedType : ctx.getAnnotation().value()) {
					if (ctx.getAnnotation().hierarchy())
						gsonBuilder.registerTypeHierarchyAdapter(mappedType, op.get());
					else
						gsonBuilder.registerTypeAdapter(mappedType, op.get());
				}
				break;
			}
		}
		return gsonBuilder;
	}

	public static void main(String[] args) {
		var blder = Serials.Gsons.getBuilder();
		System.out.println(blder);
		var copy = copy(blder);
		System.out.println(copy);
	}

}