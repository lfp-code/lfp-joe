package com.lfp.joe.serial.gson;

import java.lang.reflect.Type;
import java.net.InetSocketAddress;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.lfp.joe.utils.Utils;

public enum InetSocketAddressJsonSerializer
		implements JsonSerializer<InetSocketAddress>, JsonDeserializer<InetSocketAddress> {
	INSTANCE;

	@Override
	public JsonElement serialize(InetSocketAddress src, Type typeOfSrc, JsonSerializationContext context) {
		var str = toString(src);
		if (str == null)
			return JsonNull.INSTANCE;
		return new JsonPrimitive(str);
	}

	@Override
	public InetSocketAddress deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		if (json == null || json.isJsonNull() || !json.isJsonPrimitive())
			return null;
		return fromString(json.getAsString());
	}

	public String toString(InetSocketAddress src) {
		if (src == null)
			return null;
		StringBuilder sb = new StringBuilder();
		if (Utils.Strings.isNotBlank(src.getHostString()))
			sb.append(src.getHostString());
		sb.append(":");
		sb.append(src.getPort());
		return sb.toString();
	}

	public InetSocketAddress fromString(String input) {
		if (input == null)
			return null;
		int splitAt = input.lastIndexOf(":");
		if (splitAt < 0)
			return null;
		String hostString = input.substring(0, splitAt);
		String portStr = input.substring(splitAt + 1);
		if (!Utils.Strings.isNumeric(portStr))
			return null;
		Integer port = Utils.Functions.catching(() -> Integer.valueOf(portStr), t -> null);
		if (port == null)
			return null;
		return new InetSocketAddress(hostString, port);
	}
}