package com.lfp.joe.serial.gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.bytes.InputStreamSource;
import com.lfp.joe.utils.function.ReadWriteAccessor;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializer;

import io.gsonfire.GsonFireBuilder;
import io.gsonfire.PostProcessor;
import io.gsonfire.TypeSelector;

@SuppressWarnings("unchecked")
public enum InputStreamSourceSerializer implements GsonFireBuilderModifier, GsonBuilderModifier {
	INSTANCE;

	private final JsonSerializer<InputStreamSource.Impl> serializer = (src, typeOfSrc, context) -> {
		if (src == null)
			return JsonNull.INSTANCE;
		var bytes = Utils.Bits.from(Utils.Functions.unchecked(() -> src.openStream()));
		return new JsonPrimitive(bytes.encodeBase64());
	};

	private final JsonDeserializer<InputStreamSource.Impl> deserializer = (json, typeOfT, context) -> {
		if (json == null || json.isJsonNull())
			return null;
		var str = Serials.Gsons.tryGetAsString(json).orElse(null);
		if (str == null)
			return null;
		return new InputStreamSource.Impl(() -> {
			if (!Utils.Bits.isBase64(str))
				return Utils.Bits.emptyInputStream();
			return Utils.Bits.parseBase64(str).inputStream();
		});
	};

	private final ReadWriteAccessor<List<Function<JsonElement, Class<? extends InputStreamSource>>>> typeSelectors = new ReadWriteAccessor<>(
			new ArrayList<>());

	private InputStreamSourceSerializer() {
		this.addTypeSelector(json -> {
			var str = Serials.Gsons.tryGetAsString(json).orElse(null);
			if (Utils.Bits.isBase64(str))
				return InputStreamSource.Impl.class;
			return null;
		});
		this.addTypeSelector(json -> {
			var classType = (Class<? extends InputStreamSource>) Serials.Gsons.deserializeClassType(json).orElse(null);
			return classType;
		});
	}

	public Scrapable addTypeSelector(Function<JsonElement, Class<? extends InputStreamSource>> typeSelector) {
		Objects.requireNonNull(typeSelector);
		this.typeSelectors.writeAccess(list -> {
			list.add(0, typeSelector);
		});
		return Scrapable.create(() -> {
			this.typeSelectors.writeAccess(list -> {
				list.removeIf(v -> v == typeSelector);
			});
		});
	}

	@Override
	public GsonBuilder apply(GsonBuilder gsonBuilder) {
		gsonBuilder.registerTypeAdapter(InputStreamSource.Impl.class, serializer);
		gsonBuilder.registerTypeAdapter(InputStreamSource.Impl.class, deserializer);
		return gsonBuilder;
	}

	@Override
	public GsonFireBuilder apply(GsonFireBuilder gsonFireBuilder) {
		gsonFireBuilder.registerPostProcessor(InputStreamSource.class, new PostProcessor<InputStreamSource>() {

			@Override
			public void postDeserialize(InputStreamSource result, JsonElement src, Gson gson) {
			}

			@Override
			public void postSerialize(JsonElement result, InputStreamSource src, Gson gson) {
				if (src != null)
					Serials.Gsons.serializeClassType(result, src.getClass(), false);

			}
		});
		gsonFireBuilder.registerTypeSelector(InputStreamSource.class, new TypeSelector<InputStreamSource>() {

			@Override
			public Class<? extends InputStreamSource> getClassForElement(JsonElement readElement) {
				return typeSelectors.readAccess(list -> {
					for (var typeSelector : list) {
						var classType = typeSelector.apply(readElement);
						if (classType != null)
							return classType;
					}
					return null;
				});
			}
		});
		return gsonFireBuilder;
	}

}
