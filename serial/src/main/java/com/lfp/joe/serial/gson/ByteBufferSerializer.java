package com.lfp.joe.serial.gson;

import java.lang.reflect.Type;
import java.nio.ByteBuffer;
import java.util.Base64;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;

public enum ByteBufferSerializer implements JsonSerializer<ByteBuffer>, JsonDeserializer<ByteBuffer> {
	INSTANCE;

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	@Override
	public ByteBuffer deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		if (json == null || json.isJsonNull() || !json.isJsonPrimitive())
			return null;
		byte[] barr;
		try {
			barr = Base64.getDecoder().decode(json.getAsString());
		} catch (Exception e) {
			logger.warn("unable to decode:{}", json);
			return null;
		}
		return Bytes.from(barr).buffer();
	}

	@Override
	public JsonElement serialize(ByteBuffer src, Type typeOfSrc, JsonSerializationContext context) {
		if (src == null)
			return JsonNull.INSTANCE;
		Bytes bytes = Utils.Bits.from(src);
		return new JsonPrimitive(Base64.getEncoder().encodeToString(bytes.array()));
	}

}