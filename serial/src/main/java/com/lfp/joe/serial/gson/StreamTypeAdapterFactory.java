package com.lfp.joe.serial.gson;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Stream;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.utils.Utils;

import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class StreamTypeAdapterFactory implements TypeAdapterFactory {

	public static final StreamTypeAdapterFactory INSTANCE = new StreamTypeAdapterFactory(false, false);

	private final boolean writeStreams;
	private final boolean allowNullReads;

	public StreamTypeAdapterFactory(boolean writeStreams, boolean allowNullReads) {
		this.writeStreams = writeStreams;
		this.allowNullReads = allowNullReads;
	}

	@Override
	public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
		if (!isStreamType(type))
			return null;
		TypeAdapter delegate = gson.getDelegateAdapter(this, type);
		TypeToken listTypeToken = getListTypeToken(type);
		TypeAdapter listDelegate = gson.getDelegateAdapter(this, listTypeToken);
		return new TypeAdapter<T>() {

			@Override
			public void write(JsonWriter out, T value) throws IOException {
				if (!writeStreams)
					delegate.write(out, value);
				else {
					var stream = Utils.Lots.tryStream(value).orElse(null);
					if (stream == null)
						out.nullValue();
					else
						listDelegate.write(out, stream.toList());
				}
			}

			@Override
			public T read(JsonReader in) throws IOException {
				List list = (List) listDelegate.read(in);
				if (list == null && allowNullReads)
					return null;
				return (T) fromList(type, list);
			}
		};
	}

	protected boolean isStreamType(TypeToken<?> type) {
		if (type == null)
			return false;
		return Stream.class.isAssignableFrom(type.getRawType());
	}

	protected boolean isEntryStream(TypeToken<?> type) {
		return type != null && EntryStream.class.isAssignableFrom(type.getRawType());
	}

	protected TypeToken getListTypeToken(TypeToken<?> type) {
		Type[] typeArguments = null;
		if (isEntryStream(type)) {
			var entryTypeArguments = JavaCode.Reflections.getTypeArguments(type.getType(), EntryStream.class, 2);
			if (entryTypeArguments != null)
				typeArguments = new Type[] { TypeToken.getParameterized(Entry.class, entryTypeArguments).getType() };
		} else
			typeArguments = JavaCode.Reflections.getTypeArguments(type.getType(), Iterable.class, 1);
		if (typeArguments != null)
			return TypeToken.getParameterized(List.class, typeArguments);
		else
			return TypeToken.get(List.class);
	}

	protected Object fromList(TypeToken<?> type, List list) {
		var stream = Utils.Lots.stream(list);
		if (isEntryStream(type)) {
			StreamEx<Entry> selectStream = stream.select(Entry.class);
			return selectStream.mapToEntry(v -> v.getKey(), v -> v.getValue());
		}
		return stream;
	}

}
