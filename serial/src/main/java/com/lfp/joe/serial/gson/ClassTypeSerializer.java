package com.lfp.joe.serial.gson;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.lfp.joe.reflections.JavaCode;

public enum ClassTypeSerializer implements JsonSerializer<Class<?>>, JsonDeserializer<Class<?>> {
	INSTANCE;

	@Override
	public Class<?> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		if (json == null || json.isJsonNull() || !json.isJsonPrimitive())
			return null;
		return JavaCode.Reflections.tryForName(json.getAsString()).orElse(null);
	}

	@Override
	public JsonElement serialize(Class<?> src, Type typeOfSrc, JsonSerializationContext context) {
		if (src == null)
			return JsonNull.INSTANCE;
		return new JsonPrimitive(src.getName());
	}

}