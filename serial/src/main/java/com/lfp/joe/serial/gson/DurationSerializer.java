package com.lfp.joe.serial.gson;

import java.lang.reflect.Type;
import java.time.Duration;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public enum DurationSerializer implements JsonSerializer<Duration>, JsonDeserializer<Duration> {
	INSTANCE;

	@Override
	public Duration deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		if (json == null || json.isJsonNull() || !json.isJsonPrimitive())
			return null;
		long millis = json.getAsLong();
		return Duration.ofMillis(millis);
	}

	@Override
	public JsonElement serialize(Duration src, Type typeOfSrc, JsonSerializationContext context) {
		if (src == null)
			return JsonNull.INSTANCE;
		return new JsonPrimitive(src.toMillis());
	}

}