package com.lfp.joe.serial.gson;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.time.Duration;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import org.reflections8.ReflectionUtils;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.SortedSetMultimap;
import com.google.common.reflect.TypeParameter;
import com.google.common.reflect.TypeToken;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.lfp.joe.reflections.JavaCode;

public enum MultimapJsonSerializer implements JsonSerializer<Multimap<?, ?>>, JsonDeserializer<Multimap<?, ?>> {
	INSTANCE;

	private final Cache<String, Optional<Method>> createMethodCache = Caffeine.newBuilder()
			.expireAfterAccess(Duration.ofSeconds(1)).build();

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Multimap<?, ?> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		if (json == null || json.isJsonNull())
			return null;
		Map<?, ?> map = context.deserialize(json, multimapTypeToMapType(typeOfT));
		if (map == null)
			return null;
		Multimap mm = null;
		Type processType = typeOfT;
		if (processType instanceof ParameterizedType)
			processType = ((ParameterizedType) processType).getRawType();
		if (processType instanceof Class) {
			Class<?> classType = (Class<?>) processType;
			if (!classType.isInterface())
				try {
					mm = (Multimap<?, ?>) classType.newInstance();
				} catch (InstantiationException | IllegalAccessException e) {
					// suppress
				}
			if (mm == null) {
				Optional<Method> methodOp;
				methodOp = createMethodCache.get(classType.getName(), nil -> {
					Set<Method> methods = ReflectionUtils.getMethods(classType, ReflectionUtils.withParametersCount(0),
							ReflectionUtils.withName("create"), ReflectionUtils.withModifier(Modifier.STATIC),
							ReflectionUtils.withModifier(Modifier.PUBLIC));
					if (methods.size() != 1)
						return Optional.empty();
					return Optional.of(methods.iterator().next());
				});
				if (methodOp.isPresent())
					try {
						mm = (Multimap) methodOp.get().invoke(null);
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						throw new java.lang.RuntimeException(e);
					}
			}
			if (mm == null && ListMultimap.class.isAssignableFrom(classType))
				mm = ArrayListMultimap.create();
			if (mm == null && SetMultimap.class.isAssignableFrom(classType))
				mm = HashMultimap.create();
			if (mm == null && SortedSetMultimap.class.isAssignableFrom(classType))
				mm = LinkedHashMultimap.create();
		}
		if (mm == null)
			mm = ArrayListMultimap.create();
		for (Object key : map.keySet()) {
			Object val = map.get(key);
			if (val == null || !(val instanceof Iterable))
				continue;
			for (Object v : ((Iterable<?>) val))
				mm.put(key, v);
		}
		return mm;
	}

	@Override
	public JsonElement serialize(Multimap<?, ?> src, Type typeOfSrc, JsonSerializationContext context) {
		if (src == null)
			return JsonNull.INSTANCE;
		final Map<?, ?> map = src.asMap();
		return context.serialize(map);
	}

	@SuppressWarnings({ "unchecked", "serial" })
	private <V> Type multimapTypeToMapType(Type type) {
		final Type[] typeArguments = JavaCode.Reflections.getTypeArguments(type, Multimap.class, 2);
		Objects.requireNonNull(typeArguments,
				() -> "unable to find typeArguments:" + (type == null ? null : type.getTypeName()));
		final TypeToken<Map<String, Collection<V>>> mapTypeToken = new TypeToken<Map<String, Collection<V>>>() {
		}.where(new TypeParameter<V>() {
		}, (TypeToken<V>) TypeToken.of(typeArguments[1]));
		return mapTypeToken.getType();
	}
}
