package com.lfp.joe.serial.serializer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import de.adito.picoservice.PicoService;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
@PicoService
public @interface Serializer {

	// mapped types
	Class<?>[] value() default {};

	boolean hierarchy() default false;

}
