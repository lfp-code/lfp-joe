package com.lfp.joe.serial.gson;

import java.lang.reflect.Type;

import com.github.throwable.beanref.BeanPath;
import com.github.throwable.beanref.BeanRef;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.utils.Utils;

public enum BeanPathSerializer implements JsonSerializer<BeanPath<?, ?>>, JsonDeserializer<BeanPath<?, ?>> {
	INSTANCE;

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	@Override
	public BeanPath<?, ?> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		if (json == null || json.isJsonNull() || !json.isJsonPrimitive())
			return null;
		String str = json.getAsString();
		int splitAt = Utils.Strings.indexOf(str, "_");
		if (splitAt < 0) {
			logger.warn("unable to parse bean path split:{}", str);
			return null;
		}
		var classname = str.substring(0, splitAt);
		var classType = JavaCode.Reflections.tryForName(classname).orElse(null);
		if (classType == null) {
			logger.warn("unable to parse bean path class type:{}", str);
			return null;
		}
		var path = str.substring(splitAt + 1, str.length());
		return BeanRef.$(classType).$(path);
	}

	@Override
	public JsonElement serialize(BeanPath<?, ?> src, Type typeOfSrc, JsonSerializationContext context) {
		if (src == null)
			return JsonNull.INSTANCE;
		String str = String.format("%s_%s", src.getBeanClass().getName(), src.getPath());
		return new JsonPrimitive(str);
	}

}
