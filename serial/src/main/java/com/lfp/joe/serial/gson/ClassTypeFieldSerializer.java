package com.lfp.joe.serial.gson;

import java.lang.reflect.Type;

import com.lfp.joe.serial.Serials;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class ClassTypeFieldSerializer implements JsonSerializer<Object>, JsonDeserializer<Object> {

	public static final ClassTypeFieldSerializer INSTANCE = new ClassTypeFieldSerializer();

	protected ClassTypeFieldSerializer() {
	}

	@Override
	public JsonElement serialize(Object src, Type typeOfSrc, JsonSerializationContext context) {
		if (typeOfSrc == null && src != null)
			typeOfSrc = src.getClass();
		var result = context.serialize(src, typeOfSrc);
		if (src != null && result != null && !result.isJsonNull() && result.isJsonObject())
			Serials.Gsons.serializeClassType(result, src.getClass(), false);
		return result;
	}

	@Override
	public Object deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		var classType = Serials.Gsons.deserializeClassType(json).orElse(null);
		var result = context.deserialize(json, classType == null ? typeOfT : classType);
		return result;
	}

}