package com.lfp.joe.serial.gson;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.immutables.value.Value;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.serial.gson.ImmutableAnnotationMethodCaller.CacheKey;
import com.lfp.joe.stream.Streams;

import io.gsonfire.GsonFireBuilder;
import io.gsonfire.PostProcessor;
import io.gsonfire.annotations.PostDeserialize;
import io.gsonfire.annotations.PreSerialize;
import one.util.streamex.StreamEx;

@Value.Enclosing
public enum AnnotationMethodCaller implements GsonFireBuilderModifier, TypeAdapterFactory {
	INSTANCE;

	private static final Cache<CacheKey, List<Method>> ANNOTATED_METHODS_CACHE = Caffeine.newBuilder()
			.softValues()
			.build();

	@Override
	public GsonFireBuilder apply(GsonFireBuilder gsonFireBuilder) {
		gsonFireBuilder.registerPostProcessor(Object.class, new PostProcessor<Object>() {

			@Override
			public void postDeserialize(Object result, JsonElement src, Gson gson) {
				invokePostDeserialize(result);
			}

			@Override
			public void postSerialize(JsonElement result, Object src, Gson gson) {}
		});
		return gsonFireBuilder;
	}

	public void invokePostDeserialize(Object value) {
		if (value == null)
			return;
		StreamEx<Method> methods = streamMethods(value.getClass(), PostDeserialize.class);
		invokeMethods(methods, value);
	}

	@Override
	public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
		List<Method> preSerializeMethods = streamMethods(type, PreSerialize.class).toImmutableList();
		if (preSerializeMethods.isEmpty())
			return null;
		var delegateAdapter = gson.getDelegateAdapter(this, type);
		return new TypeAdapter<T>() {

			@Override
			public void write(JsonWriter out, T value) throws IOException {
				invokeMethods(preSerializeMethods, value);
				delegateAdapter.write(out, value);
			}

			@Override
			public T read(JsonReader in) throws IOException {
				return delegateAdapter.read(in);
			}
		};
	}

	private StreamEx<Method> streamMethods(TypeToken<?> type, Class<? extends Annotation> annotationClass) {
		var classType = Optional.ofNullable(type).map(TypeToken::getRawType).orElse(null);
		return streamMethods(classType, annotationClass);
	}

	private StreamEx<Method> streamMethods(Class<?> classType, Class<? extends Annotation> annotationClass) {
		if (classType == null || annotationClass == null)
			return Streams.empty();
		var methods = ANNOTATED_METHODS_CACHE.get(CacheKey.of(classType, annotationClass), k -> loadMethods(k));
		return Streams.of(methods);
	}

	private static List<Method> loadMethods(CacheKey cacheKey) {
		return CoreReflections.streamMethods(cacheKey.classType(), true)
				.filter(v -> v.getAnnotation(cacheKey.annotationClassType()) != null)
				.map(CoreReflections::setAccessible)
				.collect(Collectors.toUnmodifiableList());
	}

	private static void invokeMethods(Iterable<Method> methods, Object result) {
		if (methods == null || result == null)
			return;
		for (var method : methods) {
			if (method == null)
				continue;
			Throws.unchecked(() -> method.invoke(result));
		}
	}

	@Value.Immutable
	static abstract class AbstractCacheKey {

		@Value.Parameter(order = 0)
		abstract Class<?> classType();

		@Value.Parameter(order = 1)
		abstract Class<? extends Annotation> annotationClassType();

		@Value.Check
		protected AbstractCacheKey check() {
			{
				var classType = CoreReflections.getNamedClassTypeIfVaries(classType()).orElse(null);
				if (classType != null)
					return CacheKey.copyOf(this).withClassType(classType);
			}
			{
				var annotationClassType = CoreReflections.getNamedClassTypeIfVaries(annotationClassType()).orElse(null);
				if (annotationClassType != null)
					return CacheKey.copyOf(this).withAnnotationClassType(annotationClassType);
			}
			return this;
		}

	}

}