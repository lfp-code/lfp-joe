package com.lfp.joe.serial.serializer;

import java.util.Optional;

import org.immutables.value.Value;
import org.immutables.value.Value.Style.BuilderVisibility;
import org.immutables.value.Value.Style.ImplementationVisibility;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.CoreServiceRegistry;
import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.stream.Streams;

import one.util.streamex.StreamEx;

@Value.Immutable
@Value.Style(typeImmutable = "*", stagedBuilder = true, visibility = ImplementationVisibility.PUBLIC, builderVisibility = BuilderVisibility.PUBLIC)
public abstract class AbstractSerializerContext {

	public static StreamEx<SerializerContext> stream() {
		return Streams.of(CoreServiceRegistry.instance().stream(Object.class, Serializer.class)).map(ent -> {
			var type = ent.getKey();
			var anno = ent.getValue();
			return SerializerContext.builder().annotatedType(type).annotation(anno).build();
		});
	}

	public abstract Class<?> getAnnotatedType();

	// the annotation
	public abstract Serializer getAnnotation();

	public Object getSerializerInstance() {
		return Instances.get(getAnnotatedType());
	}

	@SuppressWarnings("unchecked")
	public <X> Optional<X> tryGetSerializerInstance(Class<X> classType) {
		if (classType == null)
			return Optional.empty();
		if (!CoreReflections.isAssignableFrom(classType, getAnnotatedType()))
			return Optional.empty();
		return Optional.of((X) getSerializerInstance());
	}

}
