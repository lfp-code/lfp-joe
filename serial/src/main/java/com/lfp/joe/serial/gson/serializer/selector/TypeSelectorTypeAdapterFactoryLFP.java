package com.lfp.joe.serial.gson.serializer.selector;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Set;

import org.reflections8.ReflectionUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.utils.Utils;

import io.gsonfire.ClassConfig;
import io.gsonfire.TypeSelector;
import io.gsonfire.gson.NullableTypeAdapter;
import io.gsonfire.gson.TypeSelectorTypeAdapterFactory;
import io.gsonfire.util.JsonUtils;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class TypeSelectorTypeAdapterFactoryLFP<T> implements TypeAdapterFactory {

	private static final boolean DISABLED = false;
	private static final MemoizedSupplier<Field> GsonBuilder_factories_FIELD = Utils.Functions.memoize(() -> {
		Field field = JavaCode.Reflections.getFieldUnchecked(GsonBuilder.class, true,
				ReflectionUtils.withTypeAssignableTo(List.class), ReflectionUtils.withName("factories"));
		field.setAccessible(true);
		return field;
	});
	private static final MemoizedSupplier<Field> TypeSelectorTypeAdapterFactory_classConfig_FIELD = Utils.Functions
			.memoize(() -> {
				Field field = JavaCode.Reflections.getFieldUnchecked(TypeSelectorTypeAdapterFactory.class, true,
						ReflectionUtils.withTypeAssignableTo(ClassConfig.class),
						ReflectionUtils.withName("classConfig"));
				field.setAccessible(true);
				return field;
			});
	private static final MemoizedSupplier<Field> TypeSelectorTypeAdapterFactory_alreadyResolvedTypeTokensRegistry_FIELD = Utils.Functions
			.memoize(() -> {
				Field field = JavaCode.Reflections.getFieldUnchecked(TypeSelectorTypeAdapterFactory.class, true,
						ReflectionUtils.withTypeAssignableTo(Set.class),
						ReflectionUtils.withName("alreadyResolvedTypeTokensRegistry"));
				field.setAccessible(true);
				return field;
			});

	public static boolean replaceTypeSelectorTypeAdapters(GsonBuilder gsonBuilder) {
		try {
			return replaceTypeSelectorTypeAdaptersInternal(gsonBuilder);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
					? java.lang.RuntimeException.class.cast(e)
					: new java.lang.RuntimeException(e);
		}
	}

	protected static boolean replaceTypeSelectorTypeAdaptersInternal(GsonBuilder gsonBuilder)
			throws IllegalArgumentException, IllegalAccessException {
		if (gsonBuilder == null)
			return false;
		if (DISABLED)
			return false;
		List<TypeAdapterFactory> factories = (List<TypeAdapterFactory>) Utils.Functions
				.unchecked(() -> GsonBuilder_factories_FIELD.get().get(gsonBuilder));
		boolean mod = false;
		for (int i = 0; i < factories.size(); i++) {
			TypeSelectorTypeAdapterFactory typeSelectorTypeAdapterFactory = Utils.Types
					.tryCast(factories.get(i), TypeSelectorTypeAdapterFactory.class).orElse(null);
			if (typeSelectorTypeAdapterFactory == null)
				continue;
			ClassConfig classConfig = (ClassConfig) TypeSelectorTypeAdapterFactory_classConfig_FIELD.get()
					.get(typeSelectorTypeAdapterFactory);
			Set<TypeToken> alreadyResolvedTypeTokensRegistry = (Set<TypeToken>) TypeSelectorTypeAdapterFactory_alreadyResolvedTypeTokensRegistry_FIELD
					.get().get(typeSelectorTypeAdapterFactory);
			TypeSelectorTypeAdapterFactoryLFP typeAdapterFactoryLFP = new TypeSelectorTypeAdapterFactoryLFP(classConfig,
					alreadyResolvedTypeTokensRegistry);
			factories.set(i, typeAdapterFactoryLFP);
			mod = true;
		}
		return mod;
	}

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	private final ClassConfig<T> classConfig;
	private final Set<TypeToken> alreadyResolvedTypeTokensRegistry;

	public TypeSelectorTypeAdapterFactoryLFP(ClassConfig<T> classConfig,
			Set<TypeToken> alreadyResolvedTypeTokensRegistry) {
		this.classConfig = classConfig;
		this.alreadyResolvedTypeTokensRegistry = alreadyResolvedTypeTokensRegistry;
	}

	@Override
	public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
		if (alreadyResolvedTypeTokensRegistry.contains(type)) {
			return null;
		}
		if (classConfig.getConfiguredClass().isAssignableFrom(type.getRawType())) {
			TypeAdapter<T> fireTypeAdapter = new NullableTypeAdapter<T>(
					new TypeSelectorTypeAdapter<T>(type.getRawType(), classConfig.getTypeSelector(), gson));
			return fireTypeAdapter;
		} else {
			return null;
		}
	}

	private class TypeSelectorTypeAdapter<T> extends TypeAdapter<T> {

		private final Class superClass;
		private final TypeSelector typeSelector;
		private final Gson gson;

		private TypeSelectorTypeAdapter(Class superClass, TypeSelector typeSelector, Gson gson) {
			this.superClass = superClass;
			this.typeSelector = typeSelector;
			this.gson = gson;
		}

		@Override
		public void write(JsonWriter out, T value) throws IOException {
			TypeAdapter otherTypeAdapter = gson.getDelegateAdapter(TypeSelectorTypeAdapterFactoryLFP.this,
					TypeToken.get(value.getClass()));
			otherTypeAdapter.write(out, value);
		}

		@Override
		public T read(JsonReader in) throws IOException {
			JsonElement json = new JsonParser().parse(in);
			Class deserialize;
			if (this.typeSelector instanceof TypeSelectorLFP)
				deserialize = ((TypeSelectorLFP) this.typeSelector).getClassForElement(superClass, json);
			else
				deserialize = this.typeSelector.getClassForElement(json);
			if (deserialize == null) {
				deserialize = superClass;
			}

			TypeToken typeToken = TypeToken.get(deserialize);
			alreadyResolvedTypeTokensRegistry.add(typeToken);
			TypeAdapter<T> otherTypeAdapter;
			try {
				if (deserialize != superClass) {
					otherTypeAdapter = gson.getAdapter(typeToken);
				} else {
					otherTypeAdapter = gson.getDelegateAdapter(TypeSelectorTypeAdapterFactoryLFP.this, typeToken);
				}
			} finally {
				alreadyResolvedTypeTokensRegistry.remove(typeToken);
			}
			return JsonUtils.fromJsonTree(otherTypeAdapter, in, json);
		}
	}

}
