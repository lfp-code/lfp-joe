package com.lfp.joe.serial.gson;

import java.util.List;
import java.util.Objects;

import com.github.throwable.beanref.BeanPath;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import io.gsonfire.GsonFireBuilder;
import io.gsonfire.PostProcessor;

public class AbstractBeanPathSerializer<X> implements GsonFireBuilderModifier {
	private final boolean overwrite;
	private final List<BeanPath<X, ?>> beanPaths;
	private final Class<X> beanType;

	@SuppressWarnings("unchecked")
	public AbstractBeanPathSerializer(boolean overwrite, BeanPath<X, ?>... beanPaths) {
		this.overwrite = overwrite;
		this.beanPaths = Utils.Lots.stream(beanPaths).nonNull().distinct().toImmutableList();
		Class<X> beanType = null;
		for (var beanPath : this.beanPaths) {
			var checkBeanType = beanPath.getBeanClass();
			if (beanType == null)
				beanType = checkBeanType;
			else if (Objects.equals(beanType, checkBeanType))
				continue;
			else if (beanType.isAssignableFrom(checkBeanType))
				continue;
			else if (checkBeanType.isAssignableFrom(beanType))
				beanType = checkBeanType;
			else
				throw new IllegalArgumentException("incompatible bean types:" + List.of(beanType, checkBeanType));
		}
		this.beanType = Objects.requireNonNull(beanType);
	}

	@Override
	public GsonFireBuilder apply(GsonFireBuilder gsonFireBuilder) {
		gsonFireBuilder.registerPostProcessor(beanType, new PostProcessor<X>() {

			@Override
			public void postDeserialize(X result, JsonElement src, Gson gson) {

			}

			@Override
			public void postSerialize(JsonElement result, X src, Gson gson) {
				for (var bp : beanPaths)
					appendClassType(result, src, bp);
			}
		});
		return gsonFireBuilder;
	}

	protected void appendClassType(JsonElement result, X src, BeanPath<X, ?> beanPath) {
		var fieldJo = Serials.Gsons.tryGetAsJsonObject(result, beanPath.getPath()).orElse(null);
		if (fieldJo == null)
			return;
		var classTypeStr = Serials.Gsons.tryGetAsString(fieldJo, Serials.Gsons.CLASS_TYPE_FIELD_NAME).orElse(null);
		if (!Utils.Strings.isNotBlank(classTypeStr)) {
			if (overwrite)
				fieldJo.remove(Serials.Gsons.CLASS_TYPE_FIELD_NAME);
			else
				return;
		}

		var value = beanPath.get(src);
		if (value == null)
			return;
		var classType = value.getClass();
		fieldJo.addProperty(Serials.Gsons.CLASS_TYPE_FIELD_NAME, classType.getName());
	}

}
