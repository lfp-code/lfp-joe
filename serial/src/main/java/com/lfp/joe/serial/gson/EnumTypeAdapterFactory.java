package com.lfp.joe.serial.gson;

import java.io.IOException;
import java.time.Duration;
import java.util.Optional;

import com.lfp.joe.core.function.Nada;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.ImmutableEntry;

import javax.annotation.Generated;

import org.apache.commons.lang3.StringUtils;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class EnumTypeAdapterFactory implements TypeAdapterFactory {

	public static final EnumTypeAdapterFactory INSTANCE = new EnumTypeAdapterFactory(null);

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	private final Cache<String, Nada> logRateLimiter = Caffeine.newBuilder().expireAfterWrite(Duration.ofMinutes(1))
			.build();
	private final LoadingCache<ImmutableEntry<Class<?>, String>, Optional<Enum<?>>> parseCache = Caffeine.newBuilder()
			.expireAfterAccess(Duration.ofSeconds(1)).build(ent -> {
				Class<?> enumType = ent.getKey();
				String enumVal = ent.getValue();
				try {
					return Optional.of(Enum.valueOf((Class) enumType, enumVal));
				} catch (Exception e) {
					for (Object enumConstantObj : enumType.getEnumConstants()) {
						Enum<?> enumConstant = (Enum<?>) enumConstantObj;
						if (Utils.Strings.equalsIgnoreCase(enumConstant.name(), enumVal))
							return Optional.of(enumConstant);
						if (Utils.Strings.equalsIgnoreCase(enumConstant.toString(), enumVal))
							return Optional.of(enumConstant);
					}
				}
				return Optional.empty();

			});

	private final Options options;

	public EnumTypeAdapterFactory(Options options) {
		this.options = options != null ? options : Options.builder().build();
	}

	@Override
	public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
		final Class enumClassType = (Class) Optional.ofNullable(type).map(TypeToken::getRawType).filter(Class::isEnum)
				.orElse(null);
		if (enumClassType == null)
			return null;
		return new TypeAdapter<T>() {

			public void write(JsonWriter out, T value) throws IOException {
				EnumTypeAdapterFactory.this.write(out, (Enum) value);
			}

			public T read(JsonReader reader) throws IOException {
				return (T) EnumTypeAdapterFactory.this.read(enumClassType, reader);
			}
		};
	}

	protected <E extends Enum<?>> E read(Class<E> enumClassType, JsonReader reader) throws IOException {
		JsonToken jsonToken = reader.peek();
		if (jsonToken == JsonToken.NULL) {
			reader.nextNull();
			return null;
		}
		String enumVal;
		if (jsonToken == JsonToken.BEGIN_OBJECT) {
			reader.beginObject();
			String name = reader.nextName();
			if (!StringUtils.equals(name, "name")) {
				logger.warn("nextName does not equal 'name':{}", name);
				enumVal = null;
			} else
				enumVal = reader.nextString();
		} else
			enumVal = reader.nextString();
		if (Utils.Strings.isBlank(enumVal))
			return null;
		E enumConstant = (E) parseCache.get(Utils.Lots.entry(enumClassType, enumVal)).orElse(null);
		if (enumConstant == null) {
			logRateLimiter.get(enumClassType.getName() + "#" + enumVal, nil -> {
				logger.warn("could not parse enum constant. type:{} value:{}", enumClassType.getName(), enumVal);
				return Nada.get();
			});
		}
		return enumConstant;
	}

	protected <E extends Enum<?>> void write(JsonWriter out, E value) throws IOException {
		if (value == null)
			out.nullValue();
		else {
			Enum<?> e = ((Enum<?>) value);
			String str = options.serializeWithToString ? e.toString() : e.name();
			if (options.forceLowerCase)
				str = Utils.Strings.lowerCase(str);
			out.value(str);
		}
	}

	public static class Options {

		public final boolean forceLowerCase;
		public final boolean serializeWithToString;

		@Generated("SparkTools")
		private Options(Builder builder) {
			this.forceLowerCase = builder.forceLowerCase;
			this.serializeWithToString = builder.serializeWithToString;
		}

		/**
		 * Creates builder to build {@link Options}.
		 * 
		 * @return created builder
		 */
		@Generated("SparkTools")
		public static Builder builder() {
			return new Builder();
		}

		/**
		 * Creates a builder to build {@link Options} and initialize it with the given
		 * object.
		 * 
		 * @param options to initialize the builder with
		 * @return created builder
		 */
		@Generated("SparkTools")
		public static Builder builderFrom(Options options) {
			return new Builder(options);
		}

		/**
		 * Builder to build {@link Options}.
		 */
		@Generated("SparkTools")
		public static final class Builder {
			private boolean forceLowerCase;
			private boolean serializeWithToString;

			private Builder() {
			}

			private Builder(Options options) {
				this.forceLowerCase = options.forceLowerCase;
				this.serializeWithToString = options.serializeWithToString;
			}

			public Builder withForceLowerCase(boolean forceLowerCase) {
				this.forceLowerCase = forceLowerCase;
				return this;
			}

			public Builder withSerializeWithToString(boolean serializeWithToString) {
				this.serializeWithToString = serializeWithToString;
				return this;
			}

			public Options build() {
				return new Options(this);
			}
		}
	}

}