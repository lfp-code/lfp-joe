package com.lfp.joe.reflections.reflection;

import java.lang.reflect.Method;
import java.util.function.Predicate;

import org.reflections8.ReflectionUtils;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class MethodPredicate<P extends MethodPredicate<P>> extends MemberPredicate<Method, P> {

	public static MethodPredicate<?> create() {
		return new MethodPredicate(v -> true);
	}

	protected MethodPredicate(Predicate<Method> predicate) {
		super(predicate);
	}

	public <T> P withReturnType(final Class<T> type) {
		return with(ReflectionUtils.withReturnType(type));
	}

	public <T> P withReturnTypeAssignableTo(final Class<T> type) {
		return with(ReflectionUtils.withReturnTypeAssignableTo(type));
	}

	public static void main(String[] args) throws NoSuchMethodException, SecurityException {
		var method = String.class.getMethod("toString");
		var result = MethodPredicate.create().withModifierPUBLIC().withPrefix("to").withParametersCount(0)
				.withReturnTypeAssignableTo(String.class).test(method);
		System.out.println(result);
	}
}
