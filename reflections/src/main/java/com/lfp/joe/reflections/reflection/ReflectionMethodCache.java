package com.lfp.joe.reflections.reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Predicate;

import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.utils.Utils;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

import one.util.streamex.StreamEx;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class ReflectionMethodCache<X> {

	private static final Cache<String, Method> DEFAULT_CACHE = Caffeine.newBuilder()
			.expireAfterAccess(Duration.ofSeconds(1)).build();

	public static <X> ReflectionMethodCache<X> getDefault(Class<X> classType) {
		return new ReflectionMethodCache<X>(classType, DEFAULT_CACHE);
	}

	private final Class<X> classType;
	private final Cache<String, Method> cache;

	public ReflectionMethodCache(Class<X> classType, Cache<String, Method> cache) {
		this.classType = Objects.requireNonNull(classType);
		this.cache = Objects.requireNonNull(cache);
	}

	private Method getMethod(Class<?> returnType, String methodName, Iterable<Class<?>> argumentTypeIble) {
		var argumentTypes = com.lfp.joe.stream.Streams.of(argumentTypeIble).map(Utils.Types::wrapperToPrimitive).toList();
		var keyStream = StreamEx.of(classType, returnType, methodName).append(com.lfp.joe.stream.Streams.of(argumentTypes));
		var key = keyStream.map(v -> {
			if (v == null)
				return "";
			if (v instanceof Class)
				return ((Class) v).getName();
			return v.toString();
		}).joining("#");
		return cache.get(key, nil -> {
			List<Predicate<? super Method>> predicates = new ArrayList<>();
			if (returnType != null) {
				if (Void.class.equals(returnType))
					predicates.add(v -> Void.TYPE.equals(v.getReturnType()));
				else
					predicates.add(v -> {
						var methodReturnType = Utils.Types.wrapperToPrimitive(v.getReturnType());
						if (methodReturnType.isAssignableFrom(returnType))
							return true;
						return false;
					});
			}
			predicates.add(v -> {
				if (methodName.equals(v.getName()))
					return true;
				return false;
			});
			predicates.add(v -> {
				if (v.getParameterCount() == argumentTypes.size())
					return true;
				return false;
			});
			predicates.add(v -> {
				for (int i = 0; i < argumentTypes.size(); i++) {
					var checkPT = argumentTypes.get(i);
					if (checkPT == null)
						continue;
					var methodPT = Utils.Types.wrapperToPrimitive(v.getParameterTypes()[i]);
					if (!methodPT.isAssignableFrom(checkPT))
						return false;
				}
				return true;
			});
			StreamEx<Method> stream = JavaCode.Reflections.streamMethods(classType, true,
					predicates.toArray(Predicate[]::new));
			return stream.findFirst()
					.orElseThrow(() -> new NoSuchElementException("could not find method. key:" + key));
		});
	}

	public Method getMethod(Class<?> returnType, String methodName, Class<?>... argumentTypes) {
		return getMethod(returnType, methodName, com.lfp.joe.stream.Streams.of(argumentTypes));
	}

	public Method getMethodVoid(String methodName, Class<?>... argumentTypes) {
		return getMethod(Void.class, methodName, argumentTypes);
	}

	public <V> V invoke(Class<?> returnType, String methodName, X instance, Object... arguments) {
		StreamEx<Class<?>> argumentTypes = com.lfp.joe.stream.Streams.of(arguments).map(v -> v == null ? null : v.getClass());
		try {
			return (V) getMethod(returnType, methodName, argumentTypes).invoke(instance, arguments);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw Utils.Exceptions.asRuntimeException(e);
		}
	}

	public <V> V invokeVoid(String methodName, X instance, Object... arguments) {
		return invoke(Void.class, methodName, instance, arguments);
	}
}
