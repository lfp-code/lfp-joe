package com.lfp.joe.reflections.maven;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Redirect;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.re2j.Pattern;
import com.lfp.joe.core.function.ImmutableFileLoader.FileLoaderRequest;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.io.FileExt;
import com.lfp.joe.utils.Utils;

public class MavenOrgSearchService {

	private static final MemoizedSupplier<MavenOrgSearchService> DEFAULT_INSTANCE_S = Utils.Functions
			.memoize(() -> new MavenOrgSearchService(null, "default"));

	public static MavenOrgSearchService get() {
		return DEFAULT_INSTANCE_S.get();
	}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 1;
	private final LoadingCache<VersionCacheKey, Collection<String>> versionCache;

	protected MavenOrgSearchService(String... cacheIdParts) {
		var versionCacheFileParent = Utils.Files.tempFile(com.lfp.joe.stream.Streams.of(cacheIdParts)
				.prepend("version-cache", Objects.toString(VERSION)).toArray(Object.class));
		versionCache = Caffeine.newBuilder().expireAfterAccess(Duration.ofSeconds(1)).build(key -> {
			var blder = FileLoaderRequest.builder(Function.identity(), Function.identity());
			blder.file(new FileExt(versionCacheFileParent, key.groupId + key.artifactId));
			blder.refreshInterval(Duration.ofHours(24));
			blder.refreshAsyncInterval(Duration.ofHours(1));
			blder.loader(() -> loadFresh(key));
			return blder.build().get().get();
		});

	}

	private List<String> loadFresh(VersionCacheKey key) {
		String url = String.format("https://search.maven.org/solrsearch/select?q=g:%s+AND+a:%s", key.groupId,
				key.artifactId);
		String json;
		try (var is = getResponseBody(url);) {
			json = Utils.Bits.from(is).encodeCharset(Utils.Bits.getDefaultCharset());
		} catch (IOException | InterruptedException e) {
			throw Utils.Exceptions.asRuntimeException(e);
		}
		if (Utils.Strings.isBlank(json))
			return List.of();
		var latestVersion = Utils.Strings.substringAfter(json, "\"latestVersion\"");
		latestVersion = Utils.Strings.substringAfter(latestVersion, ":");
		latestVersion = Utils.Strings.substringAfter(latestVersion, "\"");
		latestVersion = Utils.Strings.substringBefore(latestVersion, "\"");
		latestVersion = Utils.Strings.trimToNull(latestVersion);
		if (Utils.Strings.isBlank(latestVersion))
			return List.of();
		return List.of(latestVersion);
	}

	public Optional<URI> getURI(String groupId, String artifactId) {
		return getURI(groupId, artifactId, getLatestVersion(groupId, artifactId).orElse(null));
	}

	public Optional<URI> getURI(String groupId, String artifactId, String version) {
		groupId = Utils.Strings.trimToNull(groupId);
		artifactId = Utils.Strings.trimToNull(artifactId);
		version = Utils.Strings.trimToNull(version);
		if (Arrays.asList(groupId, artifactId, version).contains(null))
			return Optional.empty();
		groupId = groupId.replaceAll(Pattern.quote("."), "/");
		var url = String.format("https://search.maven.org/remotecontent?filepath=%s/%s/%s/%s-%s.jar", groupId,
				artifactId, version, artifactId, version);
		return Optional.of(URI.create(url));
	}

	public Optional<String> getLatestVersion(String groupId, String artifactId) {
		var versionCacheKey = VersionCacheKey.tryCreate(groupId, artifactId).orElse(null);
		if (versionCacheKey == null)
			return Optional.empty();
		return com.lfp.joe.stream.Streams.of(this.versionCache.get(versionCacheKey)).nonNull().filter(Utils.Strings::isNotBlank)
				.findFirst();
	}

	private static InputStream getResponseBody(String url) throws IOException, InterruptedException {
		var request = HttpRequest.newBuilder(URI.create(url)).build();
		var response = getHttpClient().send(request, BodyHandlers.ofInputStream());
		if (response.statusCode() / 100 != 2) {
			response.body().close();
			throw new IOException("invalid response" + response);
		}
		return response.body();
	}

	private static HttpClient getHttpClient() {
		return HttpClient.newBuilder().followRedirects(Redirect.NORMAL).build();
	}

	private static class VersionCacheKey implements Serializable {

		public static Optional<VersionCacheKey> tryCreate(String groupId, String artifactId) {
			groupId = Utils.Strings.trimToNull(groupId);
			artifactId = Utils.Strings.trimToNull(artifactId);
			if (Arrays.asList(groupId, artifactId).contains(null))
				return Optional.empty();
			return Optional.of(new VersionCacheKey(groupId, artifactId));
		}

		public final String groupId;
		public final String artifactId;

		private VersionCacheKey(String groupId, String artifactId) {
			super();
			this.groupId = groupId;
			this.artifactId = artifactId;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((artifactId == null) ? 0 : artifactId.hashCode());
			result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			VersionCacheKey other = (VersionCacheKey) obj;
			if (artifactId == null) {
				if (other.artifactId != null)
					return false;
			} else if (!artifactId.equals(other.artifactId))
				return false;
			if (groupId == null) {
				if (other.groupId != null)
					return false;
			} else if (!groupId.equals(other.groupId))
				return false;
			return true;
		}

	}

	public static void main(String[] args) {
		System.out.println(MavenOrgSearchService.get().getLatestVersion("com.sun.mail", "javax.mailasdf"));
		System.out.println(MavenOrgSearchService.get().getLatestVersion("com.sun.mail", "javax.mail"));
		System.out.println(MavenOrgSearchService.get().getURI("com.sun.mail", "javax.mail"));
	}

}
