package com.lfp.joe.reflections.javassist;

import java.util.Objects;

import org.objenesis.instantiator.ObjectInstantiator;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.lfp.joe.bytecode.ObjectInstantiatorCache;
import com.lfp.joe.bytecode.Objenesi;
import com.lfp.joe.core.classpath.Instances.Autowired;

@Autowired
public class CaffeineObjectInstantiatorCache implements ObjectInstantiatorCache {

	private final Cache<Class<?>, ObjectInstantiator<?>> cache = Caffeine.newBuilder().softValues().build();

	@SuppressWarnings("unchecked")
	@Override
	public <T> ObjectInstantiator<T> get(Class<T> classType) {
		Objects.requireNonNull(classType);
		return (ObjectInstantiator<T>) cache.get(classType, nil -> {
			return Objenesi.getDefault().getInstantiatorOf(classType);
		});
	}

}
