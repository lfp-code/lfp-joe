package com.lfp.joe.reflections.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import org.reflections8.ReflectionUtils;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.reflections.JavaCode;

import javassist.Modifier;

public class ReflectionPredicates {

	public static class Methods {
		public static final Predicate<Method> STATIC = v -> v != null && Modifier.isStatic(v.getModifiers());
		public static final Predicate<Method> NON_STATIC = v -> v != null && !Modifier.isStatic(v.getModifiers());
		public static final Predicate<Method> NO_ARGUMENTS = v -> v != null && v.getParameterCount() == 0;
		public static final Predicate<Method> PUBLIC = v -> v != null && Modifier.isPublic(v.getModifiers());
		public static final Predicate<Method> FINAL = v -> v != null && Modifier.isFinal(v.getModifiers());
		public static final Predicate<Method> GETTER = PUBLIC.and(NO_ARGUMENTS).and(v -> v.getName().startsWith("get"));
		public static final Predicate<Method> NOT_FUNCTION = v -> {
			Class<?> returnType = v.getReturnType();
			String name = returnType.getName();
			return !name.startsWith("java.util.function.");
		};
		public static final Predicate<Method> NOT_DEFAULT_OBJECT = new Predicate<Method>() {

			@SuppressWarnings("unchecked")
			private MemoizedSupplier<Set<Method>> objectMethodsSupplier = MemoizedSupplier.create(() -> {
				var methods = ReflectionUtils.getMethods(Object.class, PUBLIC, NO_ARGUMENTS, NON_STATIC);
				return methods;
			});

			@Override
			public boolean test(Method m) {
				var checkMethods = objectMethodsSupplier.get();
				for (Method checkMethod : checkMethods) {
					if (!checkMethod.getName().equals(m.getName()))
						continue;
					if (checkMethod.getParameterCount() != m.getParameterCount())
						continue;
					if (!checkMethod.getReturnType().isAssignableFrom(m.getReturnType()))
						continue;
					return false;
				}
				return true;
			}
		};
	}

	public static class Fields {
		public static final Predicate<Field> PUBLIC = v -> v != null && Modifier.isPublic(v.getModifiers());
		public static final Predicate<Field> FINAL = v -> v != null && Modifier.isFinal(v.getModifiers());
	}
}
