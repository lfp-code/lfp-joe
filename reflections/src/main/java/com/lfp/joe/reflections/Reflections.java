package com.lfp.joe.reflections;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.net.URL;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.RunnableFuture;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.jar.JarFile;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.time.StopWatch;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.reflections8.ReflectionUtils;
import org.threadly.concurrent.future.ListenableFutureTask;
import org.threadly.concurrent.future.ListenableRunnableFuture;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.CacheLoader;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.github.throwable.beanref.BeanPath;
import com.github.throwable.beanref.BeanRef;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldsRequest;
import com.lfp.joe.core.classpath.ImmutableMemberCache.MethodsRequest;
import com.lfp.joe.core.classpath.InterfaceSet;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Muto;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.introspector.BeanInfo;
import com.lfp.joe.introspector.IntrospectorLFP;
import com.lfp.joe.reflections.classgraph.ClassGraphs;
import com.lfp.joe.reflections.reflection.FieldAccessor;
import com.lfp.joe.reflections.reflection.MethodPredicate;
import com.lfp.joe.reflections.reflection.MethodUtils;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

import io.github.classgraph.ClassInfo;
import javassist.Modifier;
import one.util.streamex.StreamEx;

@SuppressWarnings({ "unchecked" })
public class Reflections extends ReflectionUtils implements CoreReflections {

	protected Reflections() {}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final int VERSION = 0;
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final Duration DEV_CACHE_TTL = Duration.ofMinutes(15);
	private static final LoadingCache<Class<? extends Object>, CacheValue> SUB_TYPES_OF_CACHE = createClassGraphCache(
			"SUB_TYPES_OF_CACHE", ct -> loadSubTypesOf(ct));

	private static final LoadingCache<Class<? extends Annotation>, CacheValue> TYPES_ANNOTATED_WITH_CACHE = createClassGraphCache(
			"TYPES_ANNOTATED_WITH_CACHE", ct -> loadTypesAnnotatedWith(ct));

	public static Optional<Class<?>> tryForName(String name, ClassLoader... classLoaders) {
		return CoreReflections.tryForName(name, classLoaders);
	}

	@SafeVarargs
	public static StreamEx<Class<?>> streamSuperTypes(Class<?> classType, boolean getAll,
			Predicate<? super Class<?>>... predicates) {
		if (classType == null)
			return Streams.empty();
		StreamEx<Class<?>> ctStream;
		if (getAll)
			ctStream = Streams.of(CoreReflections.streamTypes(classType)).filter(Predicate.not(classType::equals));
		else
			ctStream = Streams.<Class<?>>ofNullable(classType.getSuperclass())
					.append(Streams.of(classType.getInterfaces()))
					.nonNull();
		ctStream = ctStream.filter(concatPredicates(predicates));
		return ctStream;
	}

	@SafeVarargs
	public static StreamEx<Field> streamFields(Class<?> classType, boolean getAll,
			Predicate<? super Field>... predicates) {
		if (classType == null)
			return Streams.empty();
		var memberStream = Streams.of(MemberCache.getFields(FieldsRequest.of(classType).withAllTypes(getAll)));
		memberStream = memberStream.filter(concatPredicates(predicates));
		return Streams.of(memberStream);

	}

	@SafeVarargs
	public static StreamEx<Method> streamMethods(Class<?> classType, boolean getAll,
			Predicate<? super Method>... predicates) {
		if (classType == null)
			return Streams.empty();
		var memberStream = Streams.of(MemberCache.getMethods(MethodsRequest.of(classType).withAllTypes(getAll)));
		memberStream = memberStream.filter(concatPredicates(predicates));
		return Streams.of(memberStream);
	}

	@SafeVarargs
	public static StreamEx<Annotation> streamAnnotations(Class<?> classType, boolean getAll,
			Predicate<? super Annotation>... predicates) {
		if (classType == null)
			return Streams.empty();
		StreamEx<Class<?>> ctStream;
		if (!getAll)
			ctStream = Streams.of(classType);
		else
			ctStream = Streams.of(CoreReflections.streamTypes(classType));
		var annoStream = ctStream.flatMap(ct -> Streams.of(ct.getAnnotations()));
		annoStream = annoStream.nonNull().distinct();
		annoStream = annoStream.filter(concatPredicates(predicates));
		return annoStream;
	}

	@SafeVarargs
	public static StreamEx<Class<?>> streamInterfaces(Class<?> classType, Predicate<? super Class<?>>... predicates) {
		return Streams.of(CoreReflections.streamTypes(classType))
				.filter(Class::isInterface)
				.filter(concatPredicates(predicates));
	}

	public static StreamEx<Class<?>> streamInterfacesDistinct(Class<?> classType,
			Predicate<? super Class<?>>... predicates) {
		return streamInterfacesDistinct(streamInterfaces(classType, predicates));
	}

	public static StreamEx<Class<?>> streamInterfacesDistinct(Stream<Class<?>> classTypeStream) {
		if (classTypeStream == null)
			return Streams.empty();
		var ifaceSet = new InterfaceSet();
		Streams.of(classTypeStream).nonNull().distinct().forEach(ifaceSet::add);
		return Streams.of(ifaceSet);
	}

	@SuppressWarnings("resource")
	public static <X> StreamEx<Class<? extends X>> streamSubTypesOf(Class<X> classType) {
		if (classType == null)
			return Streams.empty();
		{
			var namedClassType = CoreReflections.getNamedClassTypeIfVaries(classType).orElse(null);
			if (namedClassType != null)
				return streamSubTypesOf(namedClassType);
		}
		Class<X> namedClassType;
		if (!(namedClassType = CoreReflections.getNamedClassType(classType)).equals(classType))
			return streamSubTypesOf(namedClassType);
		var asyncRefreshRef = Muto.<RunnableFuture<?>>create();
		var cacheValue = SUB_TYPES_OF_CACHE.get(classType);
		var stream = cacheValue.classTypes.stream();
		if (asyncRefreshRef.get() != null)
			CoreTasks.executor().execute(asyncRefreshRef.get());
		return Streams.of(stream).map(v -> (Class<? extends X>) v);
	}

	protected static List<Class<?>> loadSubTypesOf(Class<?> classType) {
		LOGGER.info("sub type scan started:{}", classType);
		var startedAt = System.currentTimeMillis();
		var classNameList = ClassGraphs.apply(sr -> {
			var className = classType.getName();
			return Streams.of(sr.getAllClasses()).filter(ci -> {
				if (className.equals(ci.getName()))
					return true;
				else if (classType.isInterface())
					return ci.implementsInterface(classType);
				else if (ci.extendsSuperclass(classType))
					return true;
				else
					return false;
			}).map(ClassInfo::getName).collect(Collectors.toList());
		});
		var classTypeList = Streams.of(classNameList).mapPartial(CoreReflections::tryForName).toImmutableList();
		LOGGER.info("sub type scan complete:{} count:{} elapsed:{}ms", classType, classTypeList.size(),
				System.currentTimeMillis() - startedAt);
		return classTypeList;
	}

	@SuppressWarnings("resource")
	public static <A extends Annotation> StreamEx<Class<?>> streamTypesAnnotatedWith(Class<A> classType) {
		if (classType == null)
			return Streams.empty();
		{
			var namedClassType = CoreReflections.getNamedClassTypeIfVaries(classType).orElse(null);
			if (namedClassType != null)
				return streamTypesAnnotatedWith(namedClassType);
		}
		var cacheValue = TYPES_ANNOTATED_WITH_CACHE.get(classType);
		var stream = cacheValue.classTypes.stream();
		return Streams.of(stream);
	}

	protected static <A extends Annotation> List<Class<?>> loadTypesAnnotatedWith(Class<A> classType) {
		LOGGER.info("annotated type scan started:{}", classType);
		var startedAt = System.currentTimeMillis();
		var classNameList = ClassGraphs.apply(sr -> {
			return sr.getAllClasses().stream().filter(ci -> {
				return ci.hasAnnotation(classType);
			}).map(ClassInfo::getName).collect(Collectors.toList());
		});
		var classTypeList = Streams.of(classNameList).mapPartial(CoreReflections::tryForName).toImmutableList();
		LOGGER.info("annotated type scan complete:{} count:{} elapsed:{}ms", classType, classTypeList.size(),
				System.currentTimeMillis() - startedAt);
		return classTypeList;
	}

	public static void setPathUnchecked(Object bean, Object value, String... paths) throws IllegalArgumentException {
		Supplier<RuntimeException> errorSupplier = () -> {
			String msg = String.format("unable to find invoker. type:%s paths:%s", bean.getClass(), paths);
			return new IllegalArgumentException(msg);
		};
		if (bean == null)
			throw errorSupplier.get();
		if (paths == null || paths.length == 0)
			throw errorSupplier.get();
		var valueClassType = value == null ? null : Utils.Types.wrapperToPrimitive(value.getClass());
		Predicate<Class<?>> typeMatch = ct -> {
			if (ct == null)
				return false;
			if (valueClassType == null)
				return true;
			ct = Utils.Types.wrapperToPrimitive(ct);
			return ct.isAssignableFrom(valueClassType);
		};
		{// check public methods
			var methodMatches = JavaCode.Reflections
					.streamMethods(bean.getClass(), true, v -> Modifier.isPublic(v.getModifiers()),
							v -> v.getParameterCount() == 1, v -> typeMatch.test(v.getParameterTypes()[0]), v -> {
								for (var path : paths) {
									if (Utils.Strings.isBlank(path))
										continue;
									if (v.getName().equalsIgnoreCase("set" + path))
										return true;
								}
								return false;
							})
					.limit(2)
					.toList();
			if (methodMatches.size() == 1) {
				Utils.Functions.unchecked(() -> methodMatches.get(0).invoke(bean, value));
				return;
			}
		}
		{// check private fields
			var fieldMatches = JavaCode.Reflections
					.streamFields(bean.getClass(), true, v -> typeMatch.test(v.getType()), v -> {
						for (var path : paths) {
							if (Utils.Strings.isNotBlank(path) && v.getName().equalsIgnoreCase(path))
								return true;
						}
						return false;
					})
					.limit(2)
					.toList();
			if (fieldMatches.size() == 1) {
				var field = fieldMatches.get(0);
				field.setAccessible(true);
				Utils.Functions.unchecked(() -> field.set(bean, value));
				return;
			}
		}
		throw errorSupplier.get();
	}

	public static Field getFieldUnchecked(Class<?> classType, boolean getAll, String fieldName, Class<?> fieldType) {
		StreamEx<Field> stream = streamFields(classType, getAll, v -> {
			return fieldName == null || fieldName.equals(v.getName());
		}, v -> {
			return fieldType == null || fieldType.equals(v.getType());
		});
		return com.lfp.joe.utils.Utils.Lots.one(stream);
	}

	public static Field getFieldUnchecked(Class<?> classType, boolean getAll, Predicate<? super Field>... predicates) {
		StreamEx<Field> stream = streamFields(classType, getAll, predicates);
		return com.lfp.joe.utils.Utils.Lots.one(stream);
	}

	public static <X, Y> FieldAccessor<X, Y> getFieldAccessorUnchecked(Class<X> classType, boolean getAll,
			String fieldName, Class<Y> fieldType) {
		return new FieldAccessor<>(getFieldUnchecked(classType, getAll, fieldName, fieldType));
	}

	public static <X, Y> FieldAccessor<X, Y> getFieldAccessorUnchecked(Class<X> classType, boolean getAll,
			Predicate<? super Field>... predicates) {
		return new FieldAccessor<>(getFieldUnchecked(classType, getAll, predicates));
	}

	private static final MemoizedSupplier<List<Method>> OBJECT_OR_PROXY_METHOD_S = Utils.Functions.memoize(() -> {
		Set<Method> methods = new HashSet<>();
		for (var ct : List.of(Object.class, Proxy.class, javassist.util.proxy.Proxy.class)) {
			methods.addAll(com.lfp.joe.stream.Streams.of(ct.getMethods()).nonNull().toList());
			methods.addAll(com.lfp.joe.stream.Streams.of(ct.getDeclaredMethods()).nonNull().toList());
		}
		return List.copyOf(methods);
	});

	public static boolean isObjectOrProxyMethod(Method method) {
		if (method == null)
			return false;
		var declaringClass = method.getDeclaringClass();
		if (declaringClass == null)
			return true;
		var methodPredicate = MethodPredicate.create()
				.withName(method.getName())
				.withParametersCount(method.getParameterCount())
				.withParameterTypes(method.getParameterTypes());
		for (var checkMethod : OBJECT_OR_PROXY_METHOD_S.get())
			if (methodPredicate.test(checkMethod))
				return true;
		return false;
	}

	public static <T> StreamEx<BeanPath<T, ?>> streamBeanPaths(Class<T> classType) {
		return streamBeanPaths(classType, null);
	}

	public static <T> StreamEx<BeanPath<T, ?>> streamBeanPaths(Class<T> classType,
			Function<Class<?>, Iterable<String>> additionalPathFunction) {
		if (classType == null || classType.getPackage() == null)
			return Streams.empty();
		if (Utils.Strings.isBlank(classType.getPackageName()))
			return Streams.empty();
		if (Utils.Strings.startsWithAny(classType.getPackageName(), "java.", "sun."))
			return Streams.empty();
		Set<BeanPath<T, ?>> beanPaths = new LinkedHashSet<>();
		var beanRoot = BeanRef.$(classType);
		var beanRootAll = beanRoot.all();
		BiFunction<BeanPath<T, ?>, String, Optional<BeanPath<T, ?>>> lookupBeanPath = (parentBeanPath, name) -> {
			if (Utils.Strings.isBlank(name))
				return Optional.empty();
			var bp = Utils.Functions.catching(() -> {
				if (parentBeanPath != null)
					return parentBeanPath.$(name);
				else
					return beanRoot.$(name);
			}, t -> null);
			if (bp != null)
				return Optional.of(bp);
			var bpStream = com.lfp.joe.stream.Streams.of(parentBeanPath != null ? parentBeanPath.all() : beanRootAll);
			var bps = bpStream.filter(v -> Utils.Strings.equalsIgnoreCase(v.getPath(), name)).limit(2).toList();
			if (bps.size() == 1)
				return Optional.of(bps.get(0));
			return Optional.empty();
		};
		BeanInfo beanInfo = IntrospectorLFP.getBeanInfo(classType);
		if (beanInfo != null) {
			for (var pd : beanInfo.getPropertyDescriptors())
				lookupBeanPath.apply(null, pd.getName()).ifPresent(beanPaths::add);
		}
		if (additionalPathFunction != null)
			com.lfp.joe.stream.Streams.of(additionalPathFunction.apply(classType))
					.nonNull()
					.distinct()
					.forEach(name -> {
						lookupBeanPath.apply(null, name).ifPresent(beanPaths::add);
					});
		return com.lfp.joe.stream.Streams.of(beanPaths).map(bp -> {
			StreamEx<BeanPath<T, ?>> subStream = Streams.of(Arrays.asList(bp));
			if (!bp.getType().isAssignableFrom(classType)) {
				subStream = subStream.append(Utils.Lots.defer(() -> {
					return streamBeanPaths(bp.getType()).mapPartial(v -> lookupBeanPath.apply(bp, v.getPath()));
				}));
			}
			return subStream;
		}).chain(Utils.Lots::flatMap).distinct();
	}

	public static Type[] getTypeArguments(Type type, Class<?> classTypeLimit, int desiredCount) {
		if (type == null)
			return null;
		Validate.isTrue(desiredCount > 0 || desiredCount == -1, "invalid desiredCount:%s", desiredCount);
		if (type instanceof ParameterizedType) {
			ParameterizedType pt = (ParameterizedType) type;
			Type[] actualTypeArguments = pt.getActualTypeArguments();
			if (actualTypeArguments != null && (desiredCount == -1 || actualTypeArguments.length == desiredCount))
				return actualTypeArguments;
		}
		if (!(type instanceof Class))
			return null;
		Type superType = ((Class<?>) type).getGenericSuperclass();
		Class<?> superClass = Utils.Types.toClass(superType);
		if (superClass == null)
			return null;
		if (classTypeLimit != null && !classTypeLimit.isAssignableFrom(superClass))
			return null;
		return getTypeArguments(superType, classTypeLimit, desiredCount);
	}

	public static Stream<Method> streamMethodsAnnotatedWith(Class<?> classType,
			Class<? extends Annotation> annotationClassType) {
		if (classType == null || annotationClassType == null)
			return Stream.empty();
		return streamMethods(classType, true, ReflectionUtils.withAnnotation(annotationClassType));
	}

	public static Stream<Class<?>> streamSuperTypesAnnotatedWith(Class<?> classType,
			Class<? extends Annotation> annotationClassType) {
		if (classType == null || annotationClassType == null)
			return Stream.empty();
		Set<Class<?>> members = ReflectionUtils.getAllSuperTypes(classType,
				ReflectionUtils.withAnnotation(annotationClassType));
		return members.stream();
	}

	public static Comparator<Method> sortMethods(Class<?> classType) {
		Method[] arr = MethodUtils.getDeclaredMethodsInOrder(classType);
		List<Method> declaredMethodsInOrder = Arrays.asList(arr);
		return Comparator.comparing(m -> {
			if (m == null)
				return Integer.MAX_VALUE;
			int index = declaredMethodsInOrder.indexOf(m);
			if (index < 0)
				return Integer.MAX_VALUE - 1;
			return index;
		});
	}

	public static boolean isAssignableFrom(Method m1, Method m2) {
		return isAssignableFrom(m1, m2, false);
	}

	public static boolean isAssignableFrom(Method m1, Method m2, boolean strictPrimitiveChecking) {
		if (m1 == null && m2 == null)
			return true;
		if (m1 == null || m2 == null)
			return false;
		if (m1 == m2)
			return true;
		if (m1.equals(m2))
			return true;
		if (!m1.getName().equals(m2.getName()))
			return false;
		{
			Class<?>[] pt1 = m1.getParameterTypes();
			Class<?>[] pt2 = m2.getParameterTypes();
			if (pt1.length != pt2.length)
				return false;
			for (int i = 0; i < pt1.length; i++) {
				int index = i;
				Function<Class<?>[], Class<?>> ptAtFunc = arr -> {
					if (strictPrimitiveChecking)
						return arr[index];
					return Utils.Types.wrapperToPrimitive(arr[index]);
				};
				if (!ptAtFunc.apply(pt1).isAssignableFrom(ptAtFunc.apply(pt2)))
					return false;
			}
		}
		if (!m1.getReturnType().isAssignableFrom(m2.getReturnType()))
			return false;
		return true;
	}

	public static void setStaticFinalField(Field field, Object value) {
		CoreReflections.setAccessible(field);
		Throws.unchecked(() -> field.set(null, value));
	}

	public static void addJarToClassPath(File jar) {
		addJarToClassPath(jar, null);
	}

	public static synchronized <X> X addJarToClassPath(File jar, Callable<X> validator) {
		Function<ClassLoader, StreamEx<Callable<Nada>>> classLoaderToInvokerStream = classLoader -> {
			if (classLoader == null)
				return Streams.empty();
			StreamEx<Callable<Nada>> invokerStream = Streams.empty();
			{// addURL
				var append = Utils.Lots.defer(() -> {
					var methods = streamMethods(classLoader.getClass(), true, v -> {
						return "addURL".equals(v.getName());
					}, v -> {
						return v.getParameterCount() == 1;
					}, v -> {
						return v.getParameterTypes()[0].isAssignableFrom(URL.class);
					});
					return methods.map(method -> {
						return (Callable<Nada>) () -> {
							method.setAccessible(true);
							method.invoke(classLoader, jar.toURI().toURL());
							return Nada.get();
						};
					});
				});
				invokerStream = invokerStream.append(append);
			}
			{// appendToClassPathForInstrumentation
				var append = Utils.Lots.defer(() -> {
					var methods = streamMethods(classLoader.getClass(), true, v -> {
						return "appendToClassPathForInstrumentation".equals(v.getName());
					}, v -> {
						return v.getParameterCount() == 1;
					}, v -> {
						return v.getParameterTypes()[0].isAssignableFrom(String.class);
					});
					return methods.map(method -> {
						return (Callable<Nada>) () -> {
							method.setAccessible(true);
							method.invoke(classLoader, jar.getAbsolutePath());
							return Nada.get();
						};
					});
				});
				invokerStream = invokerStream.append(append);
			}
			return invokerStream;
		};
		List<Throwable> errors = new ArrayList<>();
		int attemptIndex = -1;
		var stream = CoreReflections.streamDefaultClassLoaders().map(classLoaderToInvokerStream);
		var invokerIter = Utils.Lots.flatMap(com.lfp.joe.stream.Streams.of(stream)).iterator();
		while (invokerIter.hasNext()) {
			var invoker = invokerIter.next();
			attemptIndex++;
			try {
				invoker.call();
				if (validator == null)
					return null;
				var result = validator.call();
				if (result != null)
					return result;
			} catch (Throwable t) {
				errors.add(t);
			}
		}
		String summary;
		{
			Map<String, Object> map = new LinkedHashMap<>();
			map.put("errorCount", errors.size());
			map.put("attemptCount", attemptIndex + 1);
			map.put("jarPath", jar.getAbsolutePath());
			var errorMessages = com.lfp.joe.stream.Streams.of(errors)
					.map(Throwable::getMessage)
					.filter(Utils.Strings::isNotBlank)
					.distinct()
					.toList();
			map.put("errorMessages", errorMessages);
			summary = EntryStreams.of(map).map(ent -> {
				var key = ent.getKey();
				var value = ent.getValue();
				var valueStr = value == null ? null : value.toString();
				if (Utils.Strings.isBlank(valueStr))
					return null;
				return String.format("%s=%s", key, valueStr);
			}).nonNull().joining(" ");
		}
		throw new NoClassDefFoundError(summary);
	}

	public static Optional<JarFile> getJarFile(Class<?> clazz) {
		List<Callable<File>> loaders = new ArrayList<>();
		loaders.add(() -> {
			var name = clazz.getProtectionDomain().getPermissions().elements().nextElement().getName();
			return new File(name);
		});
		loaders.add(() -> {
			var uri = clazz.getProtectionDomain().getCodeSource().getLocation().toURI();
			return new File(uri);
		});
		for (var loader : loaders) {
			try {
				File file = loader.call();
				if (file == null || !file.exists() || !file.isFile())
					continue;
				JarFile jarFile = new JarFile(file);
				return Optional.of(jarFile);
			} catch (Exception e) {
				// ignore
			}
		}
		return Optional.empty();
	}

	@SuppressWarnings("deprecation")
	private static <U> LoadingCache<Class<? extends U>, CacheValue> createClassGraphCache(String identifier,
			ThrowingFunction<Class<? extends U>, List<Class<?>>, ? extends Exception> loader) {
		Validate.notBlank(identifier);
		Objects.requireNonNull(loader);
		var cacheBuilder = Caffeine.newBuilder()
				.softValues()
				.<Class<? extends U>, CacheValue>removalListener((key, value, cause) -> {
					Optional.ofNullable(value).ifPresent(CacheValue::close);
				});
		if (!MachineConfig.isDeveloper())
			return cacheBuilder.build(classType -> {
				return new CacheValue(loader.apply(classType));
			});
		var cacheLoader = new CacheLoader<Class<? extends U>, CacheValue>() {
			private Cache<Class<? extends U>, CacheValue> cache;

			@Override
			public @Nullable CacheValue load(@NonNull Class<? extends U> classType) throws Exception {
				var cacheFile = Utils.Files.tempFile(
						Streams.<Object>of(identifier, classType.getName()).prepend(THIS_CLASS, VERSION).toArray());
				ThrowingSupplier<List<Class<?>>, ? extends Exception> loaderWriter = () -> {
					var list = loader.apply(classType);
					try (var writer = cacheFile.outputStream().writer()) {
						writer.lines(Streams.of(list).map(Class::getName));
					}
					return list;
				};
				if (!cacheFile.exists() || cacheFile.lastModifiedElapsed(DEV_CACHE_TTL))
					return new CacheValue(loaderWriter.get());
				List<Class<?>> result;
				try (var reader = cacheFile.inputStream().reader()) {
					result = reader.lines()
							.map(v -> CoreReflections.tryForName(v).orElse(null))
							.filter(Objects::nonNull)
							.collect(Collectors.toUnmodifiableList());
				} catch (Throwable t) {
					LOGGER.warn("cache read error:{}", cacheFile, t);
					cacheFile.delete();
					return new CacheValue(loaderWriter.get());
				}
				var cacheValue = new CacheValue(result);
				cacheValue.asyncUpdate = new ListenableFutureTask<>(() -> {
					var classTypes = loaderWriter.get();
					if (cache != null)
						cache.put(classType, new CacheValue(classTypes));
					return Nada.get();
				});
				cacheValue.asyncUpdate.failureCallback(t -> {
					if (!Throws.isHalt(t))
						LOGGER.warn("async update error. classType:{}", classType);
				});
				cacheValue.asyncUpdate.listener(cacheValue::close);
				CoreTasks.executor().execute(cacheValue.asyncUpdate);
				return cacheValue;
			}
		};
		var cache = cacheBuilder.build(cacheLoader);
		cacheLoader.cache = cache;
		return cache;
	}

	private static <U> Predicate<U> concatPredicates(Predicate<? super U>... predicates) {
		Predicate<U> basePredicate = v -> v != null;
		if (predicates == null || predicates.length == 0)
			return basePredicate;
		return basePredicate.and(v -> {
			for (var predicate : predicates)
				if (predicate != null && !predicate.test(v))
					return false;
			return true;
		});
	}

	private static class CacheValue implements AutoCloseable {

		public ListenableRunnableFuture<?> asyncUpdate;
		public final List<Class<?>> classTypes;

		public CacheValue(List<Class<?>> classTypes) {
			super();
			this.classTypes = classTypes;
		}

		@Override
		public void close() {
			Optional.ofNullable(asyncUpdate).ifPresent(v -> {
				this.asyncUpdate = null;
				v.cancel(true);
			});

		}

	}

	public static void main(String[] args) throws FileNotFoundException, IOException, InterruptedException {
		for (int i = 0; i < 10; i++) {
			var sw = StopWatch.createStarted();
			streamSubTypesOf(Future.class).forEach(v -> {
				System.out.println(v.getName());
			});
			System.out.println(i + " - " + sw.getTime());
		}
		Thread.sleep(30_000);
	}

}
