package com.lfp.joe.reflections.reflection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.function.Predicate;

import org.reflections8.ReflectionUtils;

import com.lfp.joe.utils.Utils;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class MemberPredicate<T extends Member, P extends MemberPredicate<T, P>> extends ModifierPredicate<T, P> {

	public static <T extends Member> MemberPredicate<T, ?> create() {
		return new MemberPredicate(v -> true);
	}

	protected MemberPredicate(Predicate<T> predicate) {
		super(predicate, Member::getModifiers);
	}

	public P withParameterTypes(final Class... types) {
		if (types == null)
			return withParameterTypes(new Class[0]);
		return with(member -> {
			if (member == null)
				return false;
			var parameterTypes = parameterTypes(member);
			if (parameterTypes.length != types.length)
				return false;
			for (int i = 0; i < parameterTypes.length; i++) {
				var parameterType = parameterTypes[i];
				var type = types[i];
				if (type == null) {
					if (parameterType.isPrimitive())
						return false;
					continue;
				}
				if (!Utils.Types.isAssignableFrom(parameterType, type))
					return false;
			}
			return true;
		});
	}

	public P withName(final String name) {
		return with(ReflectionUtils.withName(name));
	}

	public P withName(final String name, boolean ignoreCase) {
		if (!ignoreCase)
			return withName(name);
		return with(input -> input != null && input.getName().equalsIgnoreCase(name));
	}

	public P withPrefix(final String prefix) {
		return with(ReflectionUtils.withPrefix(prefix));
	}

	public P withParameters(final Class<?>... types) {
		return with(ReflectionUtils.withParameters(types));
	}

	public P withParametersCount(final int count) {
		return with(ReflectionUtils.withParametersCount(count));
	}

	public P withAnyParameterAnnotation(final Class<? extends Annotation> annotationClass) {
		return with(ReflectionUtils.withAnyParameterAnnotation(annotationClass));
	}

	public P withAnyParameterAnnotation(final Annotation annotation) {
		return with(ReflectionUtils.withAnyParameterAnnotation(annotation));
	}

	protected static Class[] parameterTypes(Member member) {
		return member != null
				? (member.getClass() == Method.class ? ((Method) member).getParameterTypes()
						: (member.getClass() == Constructor.class ? ((Constructor) member).getParameterTypes() : null))
				: null;
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) throws NoSuchMethodException, SecurityException {
		if (false) {
			var method = String.class.getMethod("toString");
			var result = MemberPredicate.create().withModifierPUBLIC().withPrefix("to").withParametersCount(0)
					.test(method);
			System.out.println(result);
		}
		if (false) {
			var method = AtomicLong.class.getMethod("addAndGet", long.class);
			var pred = MemberPredicate.create().withParametersCount(1).withParameterTypes((Class<?>) null);
			System.out.println(pred.test(method));
		}
		if (false) {
			var method = Function.class.getMethod("apply", Object.class);
			var pred = MemberPredicate.create().withParametersCount(1).withParameterTypes(int.class);
			System.out.println(pred.test(method));
		}
		if (true) {
			var method = Object.class.getMethod("toString");
			var pred = MemberPredicate.create().withParametersCount(0).withParameterTypes();
			System.out.println(pred.test(method));
		}
	}
}
