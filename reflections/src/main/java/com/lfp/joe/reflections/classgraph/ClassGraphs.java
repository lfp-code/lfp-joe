package com.lfp.joe.reflections.classgraph;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.StampedLock;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.function.Throws.ThrowingConsumer;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.core.process.future.CSFutureTask;
import com.lfp.joe.introspector.IntrospectorLFP;
import com.lfp.joe.stream.Streams;

import io.github.classgraph.ClassGraph;
import io.github.classgraph.ScanResult;
import one.util.streamex.IntStreamEx;

public class ClassGraphs {

	protected ClassGraphs() {}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final Duration SCAN_CLOSE_DELAY = Duration.ofMinutes(5);
	private static final String JAVA_BASE_MODULE_NAME = "java.base";
	private static final String[] REJECT_PACKAGES = new String[] { "java.lang" };
	private static final AtomicReference<Future<?>> SCAN_CLOSE_FUTURE = new AtomicReference<>();
	private static final StampedLock SCAN_LOCK = new StampedLock();
	private static ScanResult SCAN_RESULT;

	public static <T extends Throwable> void accept(ThrowingConsumer<ScanResult, T> scanResultAction) throws T {
		apply(scanResultAction == null ? null : sr -> {
			scanResultAction.accept(sr);
			return null;
		});
	}

	public static <U, T extends Throwable> U apply(ThrowingFunction<ScanResult, U, T> scanResultAction) throws T {
		ThrowingSupplier<U, T> scanResultSupplier = () -> {
			try {
				return scanResultAction.apply(SCAN_RESULT);
			} finally {
				scheduleClose();
			}
		};
		while (true) {
			var stamp = SCAN_LOCK.readLock();
			try {
				if (SCAN_RESULT != null)
					return scanResultSupplier.get();
				var writeStamp = SCAN_LOCK.tryConvertToWriteLock(stamp);
				if (writeStamp == 0)
					continue;
				stamp = writeStamp;
				if (SCAN_RESULT != null)
					continue;
				LOGGER.info("{} scan started", ClassGraph.class.getSimpleName());
				var startedAt = System.currentTimeMillis();
				SCAN_RESULT = createClassGraph().scan();
				LOGGER.info("{} scan complete - {}ms", ClassGraph.class.getSimpleName(),
						System.currentTimeMillis() - startedAt);
			} finally {
				SCAN_LOCK.unlock(stamp);
			}
		}
	}

	protected static void scheduleClose() {
		var closeTask = new FutureTask<>(() -> runClose(), null) {

			@Override
			protected void done() {
				super.done();
				SCAN_CLOSE_FUTURE.compareAndSet(this, null);
			}
		};
		var previousCloseTask = SCAN_CLOSE_FUTURE.getAndSet(closeTask);
		if (previousCloseTask != null)
			previousCloseTask.cancel(true);
		CoreTasks.delayedExecutor(SCAN_CLOSE_DELAY).execute(closeTask);
	}

	protected static void runClose() {
		var stamp = SCAN_LOCK.tryWriteLock();
		if (stamp == 0)
			return;
		try {
			if (SCAN_RESULT == null)
				return;
			SCAN_RESULT.close();
			System.gc();
			LOGGER.info("{} scan closed", ClassGraph.class.getSimpleName());
		} catch (Throwable t) {
			LOGGER.warn("{} scan close error", ClassGraph.class.getSimpleName(), t);
		} finally {
			SCAN_RESULT = null;
			SCAN_LOCK.unlock(stamp);
		}
	}

	protected static ClassGraph createClassGraph() {
		var modules = ModuleLayer.boot().modules();
		Module javaBase = modules.stream()
				.filter(v -> JAVA_BASE_MODULE_NAME.equals(v.getName()))
				.findFirst()
				.orElse(null);
		var cg = new ClassGraph();
		cg.setMaxBufferedJarRAMSize(32 * 1024 * 1024);
		cg.enableAllInfo();
		cg.enableSystemJarsAndModules();
		cg.acceptModules("*");
		var rejectModules = modules.stream()
				.filter(module -> !module.equals(javaBase))
				.map(Module::getName)
				.toArray(String[]::new);
		cg.rejectModules(rejectModules);
		cg.rejectPackages(REJECT_PACKAGES);
		return cg;
	}

	public static void main(String[] args) throws InterruptedException {
		var threads = 10;
		var latch = new CountDownLatch(threads);
		var futures = new ArrayList<CompletableFuture<?>>();
		for (int i = 0; i < threads; i++) {
			var pass = i;
			var future = new CSFutureTask<>(() -> {
				latch.countDown();
				latch.await();
				Thread.sleep(100 * pass);
				var name = ClassGraphs.apply(sr -> {
					var ciList = sr.getAllClasses();
					var indexes = IntStreamEx.range(0, ciList.size()).boxed().toList();
					Collections.shuffle(indexes);
					var index = indexes.get(0);
					var ci = ciList.get(index);
					ci.getInterfaces();
					return index + " - " + ci.getName();
				});
				System.out.println(name);
				return name;
			});
			futures.add(future.toCompletableFuture());
			CoreTasks.executor().execute(future);
		}
		CompletableFuture.allOf(futures.toArray(CompletableFuture[]::new)).join();
		Thread.sleep(2_000);
		System.gc();
		System.out.println("gc done");
		Thread.sleep(30_000);
		var printHeapInfo = new Runnable() {

			@Override
			public void run() {
				long heapSize = Runtime.getRuntime().totalMemory();

				// Get maximum size of heap in bytes. The heap cannot grow beyond this size.//
				// Any attempt will result in an OutOfMemoryException.
				long heapMaxSize = Runtime.getRuntime().maxMemory();

				// Get amount of free memory within the heap in bytes. This size will increase
				// // after garbage collection and decrease as new objects are created.
				long heapFreeSize = Runtime.getRuntime().freeMemory();

				System.out.println("heap size: " + formatSize(heapSize));
				System.out.println("heap max size: " + formatSize(heapMaxSize));
				System.out.println("heap free size: " + formatSize(heapFreeSize));

			}

			private String formatSize(long v) {
				if (v < 1024)
					return v + " B";
				int z = (63 - Long.numberOfLeadingZeros(v)) / 10;
				return String.format("%.1f %sB", (double) v / (1L << (z * 10)), " KMGTPE".charAt(z));
			}
		};

		ClassGraphs.accept(sr -> {
			var allClasses = sr.getAllClasses();
			System.out.println(allClasses.size());
			for (int i = 0; i < allClasses.size(); i++) {
				var ct = CoreReflections.tryForName(allClasses.get(i).getName()).orElse(null);
				if (ct == null)
					continue;
				try {
					var pds = Streams.of(IntrospectorLFP.getBeanInfo(ct).getPropertyDescriptors()).count();
					if (pds <= 0)
						continue;
					System.out.println(String.format("%s - %s", ct, pds));
				} catch (Throwable t) {
					if (Throws.streamCauses(t).filter(NoClassDefFoundError.class::isInstance).findFirst().isPresent())
						continue;
					throw t;
				}
				if (i % 100 == 0) {
					printHeapInfo.run();
				}
			}
			printHeapInfo.run();
		});
		System.err.println("done");
	}

}
