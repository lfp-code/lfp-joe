package com.lfp.joe.reflections.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Objects;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.utils.Utils;

public class FieldAccessor<X, Y> {

	private final MemoizedSupplier<Field> fieldSupplier;

	public FieldAccessor(Field field) {
		Objects.requireNonNull(field);
		this.fieldSupplier = Utils.Functions.memoize(() -> {
			field.setAccessible(true);
			return field;
		});
	}

	@SuppressWarnings("unchecked")
	public Y get(X object) {
		var field = this.fieldSupplier.get();
		if (!Modifier.isStatic(field.getModifiers()))
			Objects.requireNonNull(object);
		return (Y) Utils.Functions.unchecked(() -> field.get(object));
	}

	public void set(X object, Y value) {
		var field = this.fieldSupplier.get();
		var isStatic = Modifier.isStatic(field.getModifiers());
		if (isStatic && Modifier.isFinal(field.getModifiers())) {
			JavaCode.Reflections.setStaticFinalField(field, value);
			return;
		}
		if (!isStatic)
			Objects.requireNonNull(object);
		Utils.Functions.unchecked(() -> field.set(object, value));
	}

}
