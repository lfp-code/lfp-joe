package com.lfp.joe.reflections.javassist;

import java.lang.reflect.Method;
import java.util.List;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.MethodRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.stream.Streams;

import javassist.util.proxy.MethodHandler;

public class DelegatingMethodHandler implements MethodHandler {

	private final List<Object> delegates;

	public DelegatingMethodHandler(Iterable<Object> delegates) {
		this.delegates = com.lfp.joe.stream.Streams.of(delegates).nonNull().distinct().toImmutableList();
	}

	@Override
	public Object invoke(Object self, Method thisMethod, Method proceed, Object[] args) throws Throwable {
		if (proceed != null)
			return proceed.invoke(self, args);
		var declaringClass = thisMethod.getDeclaringClass();
		for (var delegate : delegates) {
			if (CoreReflections.isInstance(declaringClass, delegate))
				return thisMethod.invoke(delegate, args);
		}
		for (var delegate : delegates) {
			var delegateMethod = MemberCache
					.tryGetMethod(MethodRequest.of(delegate.getClass(), thisMethod.getReturnType(),
							thisMethod.getName(), thisMethod.getParameterTypes()), true)
					.orElse(null);
			if (delegateMethod != null)
				return delegateMethod.invoke(delegate, args);
		}
		throw new NoSuchMethodException(String.format("delegates:%s method:%s",
				Streams.of(delegates).map(Object::getClass).toList(), thisMethod));
	}

	public List<Object> getDelegates() {
		return delegates;
	}

}
