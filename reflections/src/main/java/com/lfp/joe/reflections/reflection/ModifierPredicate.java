package com.lfp.joe.reflections.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Modifier;
import java.time.Duration;
import java.util.Arrays;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.utils.Utils;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class ModifierPredicate<T, P extends ModifierPredicate<T, P>> implements Predicate<T> {

	public static <T extends Member> ModifierPredicate<T, ?> createMember() {
		return new ModifierPredicate(v -> true, v -> ((T) v).getModifiers());
	}

	public static ModifierPredicate<Class<?>, ?> createType() {
		return new ModifierPredicate(v -> true, v -> ((Class<?>) v).getModifiers());
	}

	private static final Cache<Class<?>, Function<Object[], ? extends ModifierPredicate>> FACTORY_CACHE = Caffeine
			.newBuilder().expireAfterAccess(Duration.ofSeconds(5)).build();

	private final Predicate<T> predicate;
	private ToIntFunction<T> modifierFunction;

	protected ModifierPredicate(Predicate<T> predicate, ToIntFunction<T> modifierFunction) {
		this.predicate = Objects.requireNonNull(predicate);
		this.modifierFunction = Objects.requireNonNull(modifierFunction);
	}

	@Override
	public boolean test(T value) {
		return predicate.test(value);
	}

	public P with(Predicate<? super T> predicate) {
		if (predicate == null)
			return (P) this;
		var thisClass = this.getClass();
		var factory = FACTORY_CACHE.get(thisClass, nil -> {
			var constructor = lookupConstructor(thisClass);
			return initargs -> {
				return Throws.unchecked(() -> constructor
						.newInstance(Arrays.copyOfRange(initargs, 0, constructor.getParameterCount())));
			};
		});
		return (P) factory.apply(new Object[] { this.predicate.and(predicate), this.modifierFunction });
	}

	public P withModifier(final int mod) {
		return with(input -> {
			return input != null && (this.modifierFunction.applyAsInt(input) & mod) != 0;
		});
	}

	public P withModifierPUBLIC() {
		return withModifier(Modifier.PUBLIC);
	};

	public P withModifierPRIVATE() {
		return withModifier(Modifier.PRIVATE);
	};

	public P withModifierPROTECTED() {
		return withModifier(Modifier.PROTECTED);
	};

	public P withModifierSTATIC() {
		return withModifier(Modifier.STATIC);
	};

	public P withModifierFINAL() {
		return withModifier(Modifier.FINAL);
	};

	public P withModifierSYNCHRONIZED() {
		return withModifier(Modifier.SYNCHRONIZED);
	};

	public P withModifierVOLATILE() {
		return withModifier(Modifier.VOLATILE);
	};

	public P withModifierTRANSIENT() {
		return withModifier(Modifier.TRANSIENT);
	};

	public P withModifierNATIVE() {
		return withModifier(Modifier.NATIVE);
	};

	public P withModifierINTERFACE() {
		return withModifier(Modifier.INTERFACE);
	};

	public P withModifierABSTRACT() {
		return withModifier(Modifier.ABSTRACT);
	};

	public P withModifierSTRICT() {
		return withModifier(Modifier.STRICT);
	};

	public P withModifierNotPUBLIC() {
		return with(Predicate.not(withModifier(Modifier.PUBLIC)));
	};

	public P withModifierNotPRIVATE() {
		return with(Predicate.not(withModifier(Modifier.PRIVATE)));
	};

	public P withModifierNotPROTECTED() {
		return with(Predicate.not(withModifier(Modifier.PROTECTED)));
	};

	public P withModifierNotSTATIC() {
		return with(Predicate.not(withModifier(Modifier.STATIC)));
	};

	public P withModifierNotFINAL() {
		return with(Predicate.not(withModifier(Modifier.FINAL)));
	};

	public P withModifierNotSYNCHRONIZED() {
		return with(Predicate.not(withModifier(Modifier.SYNCHRONIZED)));
	};

	public P withModifierNotVOLATILE() {
		return with(Predicate.not(withModifier(Modifier.VOLATILE)));
	};

	public P withModifierNotTRANSIENT() {
		return with(Predicate.not(withModifier(Modifier.TRANSIENT)));
	};

	public P withModifierNotNATIVE() {
		return with(Predicate.not(withModifier(Modifier.NATIVE)));
	};

	public P withModifierNotINTERFACE() {
		return with(Predicate.not(withModifier(Modifier.INTERFACE)));
	};

	public P withModifierNotABSTRACT() {
		return with(Predicate.not(withModifier(Modifier.ABSTRACT)));
	};

	public P withModifierNotSTRICT() {
		return with(Predicate.not(withModifier(Modifier.STRICT)));
	};

	private static <X extends ModifierPredicate> Constructor<X> lookupConstructor(Class<X> classType) {
		Objects.requireNonNull(classType, "class type required");
		var ctor = Utils.Functions
				.catching(() -> classType.getDeclaredConstructor(Predicate.class, ToIntFunction.class), t -> null);
		if (ctor != null)
			return ctor;
		ctor = Utils.Functions.catching(() -> classType.getDeclaredConstructor(Predicate.class), t -> null);
		return Objects.requireNonNull(ctor, () -> "constructor lookup failed:" + classType.getName());
	}

	public static void main(String[] args) throws NoSuchMethodException, SecurityException {
		var method = String.class.getMethod("toString");
		var result = ModifierPredicate.createMember().withModifierPUBLIC().test(method);
		System.out.println(result);
	}
}
