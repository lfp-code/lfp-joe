package com.lfp.joe.reflections.reflection;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Redirect;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.lock.FileLocks;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

import org.apache.commons.lang3.Validate;

@SuppressWarnings("unchecked")
public abstract class LazyJar<X> {

	private static final String LOG_MESSAGE_TEMPLATE = "lazy loading jar. disk:{} uri:{}";
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 1;

	public X setup() throws IOException {
		return setup(null);
	}

	public X setup(Duration refreshDuration) throws IOException {
		return setup(refreshDuration, false);
	}

	protected X setup(Duration refreshDuration, boolean synced) throws IOException {
		X result;
		try {
			result = getResult();
		} catch (Exception e) {
			throw Utils.Exceptions.as(e, IOException.class);
		}
		if (result != null)
			return result;
		if (!synced) {
			synchronized (this) {
				return setup(refreshDuration, true);
			}
		}
		var downloadURI = Objects.requireNonNull(getDownloadURI());
		File dir = Utils.Files.tempFile(Utils.Crypto.hashMD5(THIS_CLASS, VERSION, downloadURI).encodeHex());
		dir.mkdirs();
		File lock = new File(dir, "lock");
		File jarFile = new File(dir, "downloaded.jar");
		var resultRef = new AtomicReference<X>();
		FileLocks.write(lock, (nil1, nil2) -> {
			try {
				resultRef.set(setup(refreshDuration, downloadURI, jarFile));
			} catch (InterruptedException e) {
				throw Utils.Exceptions.as(e, IOException.class);
			}
		});
		return resultRef.get();
	}

	protected X setup(Duration refreshDuration, URI downloadURI, File jarFile)
			throws IOException, InterruptedException {
		if (refreshDuration != null && jarFile.exists()) {
			long elapsed = System.currentTimeMillis() - refreshDuration.toMillis();
			if (elapsed > refreshDuration.toMillis())
				jarFile.delete();
		}
		if (!jarFile.exists()) {
			logger.info(LOG_MESSAGE_TEMPLATE, false, downloadURI);
			HttpClient client = HttpClient.newBuilder().followRedirects(Redirect.NORMAL).build();
			HttpRequest request = HttpRequest.newBuilder().uri(downloadURI).build();
			try (var is = client.send(request, BodyHandlers.ofInputStream()).body();
					var fos = new FileOutputStream(jarFile)) {
				Utils.Bits.copy(is, fos);
			}
		} else
			logger.info(LOG_MESSAGE_TEMPLATE, true, downloadURI);
		return JavaCode.Reflections.addJarToClassPath(jarFile, () -> getResult());
	}

	protected abstract X getResult() throws Exception;

	protected abstract URI getDownloadURI() throws IOException;

	public static <X> LazyJar<Class<X>> create(String className,
			ThrowingSupplier<URI, IOException> downloadURISupplier) {
		Validate.notBlank(className);
		return create(() -> {
			return (Class<X>) CoreReflections.tryForName(className).orElse(null);
		}, downloadURISupplier);
	}

	public static <X> LazyJar<Nada> create(ThrowingSupplier<URI, IOException> downloadURISupplier) {
		return create(() -> Nada.get(), downloadURISupplier);
	}

	public static <X> LazyJar<X> create(Callable<X> resultGetter,
			ThrowingSupplier<URI, IOException> downloadURISupplier) {
		Objects.requireNonNull(resultGetter);
		Objects.requireNonNull(downloadURISupplier);
		return new LazyJar<X>() {

			@Override
			protected X getResult() throws Exception {
				return resultGetter.call();
			}

			@Override
			protected URI getDownloadURI() throws IOException {
				return downloadURISupplier.get();
			}
		};
	}
}
