package com.lfp.joe.reflections;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Nullable;

import org.immutables.value.Value;
import org.objenesis.Objenesis;
import org.objenesis.ObjenesisStd;

import com.lfp.joe.bytecode.ProxyFactoryInterfaceSet;
import com.lfp.joe.bytecode.ProxyFactoryLFP;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.EmptyArrays;
import com.lfp.joe.reflections.ImmutableProxies.DelegatingInstanceRequest;
import com.lfp.joe.reflections.javassist.DelegatingMethodHandler;
import com.lfp.joe.stream.Streams;

import javassist.util.proxy.MethodHandler;
import javassist.util.proxy.Proxy;
import javassist.util.proxy.ProxyFactory;
import javassist.util.proxy.ProxyObject;

@Value.Enclosing
@ValueLFP.Style
@SuppressWarnings({ "unchecked" })
public class Proxies {

	public static final String GENERATED_CLASS_TOKEN = "_$$_";

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static Objenesis getObjenesis() {
		return Instances.get(ObjenesisStd.class);
	}

	public static ProxyFactoryLFP createFactory(Class<?> superclass, Class<?>... interfaces) {
		return createFactory(superclass, Streams.of(interfaces));
	}

	public static ProxyFactoryLFP createFactory(Class<?> superclass, Iterable<Class<?>> interfaces) {
		var factory = new ProxyFactoryLFP();
		if (superclass != null) {
			if (!superclass.isInterface())
				factory.setSuperclass(superclass);
			else
				interfaces = Streams.of(interfaces).prepend(superclass);
		}
		factory.setInterfaces(interfaces);
		return factory;
	}

	public static <X> X createInstance(ProxyFactory factory, MethodHandler methodHandler) throws NoSuchMethodException,
			IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
		var instance = factory.create(EmptyArrays.of(Class.class), EmptyArrays.of(Object.class));
		((Proxy) instance).setHandler(methodHandler);
		return (X) instance;
	}

	public static <X> X createDelegatingInstance(DelegatingInstanceRequest<X> request) throws NoSuchMethodException,
			IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
		Objects.requireNonNull(request);
		var factory = createFactory(request.superclass(), request.interfaces());
		factory.addInterfaces(Streams.of(request.delegates()).map(Object::getClass));
		var instance = factory.create();
		((Proxy) instance).setHandler(new DelegatingMethodHandler(request.delegates()));
		return (X) instance;
	}

	public static Optional<MethodHandler> tryGetMethodHandler(Object proxy) {
		return CoreReflections.tryCast(proxy, ProxyObject.class).map(ProxyObject::getHandler);
	}

	public static void ensureInterfaces(ProxyFactory factory, Class<?>... classTypes) {
		Objects.requireNonNull(factory);
		if (classTypes == null || classTypes.length == 0)
			return;
		if (factory instanceof ProxyFactoryLFP) {
			((ProxyFactoryLFP) factory).addInterfaces(classTypes);
			return;
		}
		var ifaceSet = new ProxyFactoryInterfaceSet(factory);
		boolean mod = false;
		for (var classType : classTypes)
			if (classType != null && ifaceSet.add(classType))
				mod = true;
		if (!mod)
			return;
		factory.setInterfaces(ifaceSet.toArray());
	}

	@Value.Immutable
	static abstract class AbstractDelegatingInstanceRequest<X> {

		@Nullable
		public abstract Class<X> superclass();

		public abstract List<Class<?>> interfaces();

		public abstract List<Object> delegates();

	}

}
