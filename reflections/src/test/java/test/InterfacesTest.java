package test;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.commons.lang3.ClassUtils;
import org.immutables.value.Value;
import org.threadly.concurrent.future.ImmediateResultListenableFuture;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.Durations;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.stream.Streams;

@ValueLFP.Style
@Value.Enclosing
public class InterfacesTest {

	public static void main(String[] args) {
		var classType = ImmediateResultListenableFuture.class;
		List<Supplier<List<Class<?>>>> suppliers = List.of(() -> {
			return CoreReflections.streamTypes(classType).filter(Class::isInterface).collect(Collectors.toList());
		}, () -> {
			return JavaCode.Reflections.streamInterfaces(classType).collect(Collectors.toList());
		}, () -> {
			return Streams.of(ClassUtils.getAllInterfaces(classType), ClassUtils.getAllSuperclasses(classType))
					.flatMap(Streams::of)
					.filter(Class::isInterface)
					.distinct()
					.toList();
		}, () -> {
			return streamAllTypes(classType).filter(Class::isInterface).collect(Collectors.toList());
		});
		var indexes = IntStream.range(0, suppliers.size()).boxed().collect(Collectors.toList());
		for (int pass = 0; pass < 10; pass++) {
			for (var index : indexes) {
				var supplier = suppliers.get(index);
				var size = supplier.get().size();
				Double average = LongStream.range(0, 100_000).map(v -> {
					var startedAt = System.nanoTime();
					supplier.get();
					var elapsed = System.nanoTime() - startedAt;
					return elapsed;
				}).average().getAsDouble();
				var averageMillis = Durations.toMillis(Duration.ofNanos(average.longValue()));
				System.out.println(String.format("pass:%s index:%s size:%s averageMillis:%s", pass, index, size,
						new BigDecimal(averageMillis.toString()).stripTrailingZeros().toPlainString()));
			}
		}

	}

	private static Stream<Class<?>> streamAllTypes(Class<?> classType) {
		if (classType == null)
			return Stream.empty();
		var spliterator = new Spliterators.AbstractSpliterator<Class<?>>(Long.MAX_VALUE, 0) {

			private final Set<Class<?>> visited = new HashSet<>();
			private Entry<Class<?>, Iterator<Class<?>>> currentEntry = nextEntry(classType);

			@Override
			public boolean tryAdvance(Consumer<? super Class<?>> action) {
				while (true) {
					var ctIter = currentEntry.getValue();
					while (ctIter.hasNext()) {
						var ct = ctIter.next();
						if (visited.add(ct)) {
							action.accept(ct);
							return true;
						}
					}
					var superClass = currentEntry.getKey().getSuperclass();
					if (superClass == null)
						return false;
					currentEntry = nextEntry(superClass);
				}
			}

			private Entry<Class<?>, Iterator<Class<?>>> nextEntry(Class<?> nextClassType) {
				var ifaces = nextClassType.getInterfaces();
				var ctIter = new Iterator<Class<?>>() {

					private int index = -1;

					@Override
					public boolean hasNext() {
						return index < ifaces.length;
					}

					@Override
					public Class<?> next() {
						Class<?> next;
						if (index == -1)
							next = nextClassType;
						else
							next = ifaces[index];
						index++;
						return next;
					}
				};
				return Map.entry(nextClassType, ctIter);
			}
		};
		return StreamSupport.stream(spliterator, false);
	}

}
