package test;

import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Supplier;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;

public class CompletableFutureTest {

	public static void main(String[] args) throws NotFoundException, CannotCompileException {
		var ctClass = ClassPool.getDefault().getCtClass("java.util.concurrent.CompletableFuture");
		CtMethod ctMethod = ctClass.getDeclaredMethod("supplyAsync",
				new CtClass[] { toCtClass(Supplier.class), toCtClass(Executor.class) });
		var methodMody = String.format("{%s}", "System.out.println(\"hey there\"); return null;");
		ctMethod.setBody(methodMody);
		ctClass.toClass(java.util.concurrent.ThreadPoolExecutor.class);
		var fut = CompletableFuture.supplyAsync(() -> {
			System.out.println("sup");
			return new Date();
		}, ForkJoinPool.commonPool());
		System.out.println(fut.join());
	}

	private static CtClass toCtClass(Class<?> classType) throws NotFoundException {
		var cpool = ClassPool.getDefault();
		return cpool.getCtClass(classType.getName());
	}
}
