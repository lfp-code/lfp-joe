package test;

import java.time.Duration;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import org.threadly.concurrent.future.ImmediateResultListenableFuture;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.Durations;
import com.lfp.joe.reflections.JavaCode;

public class SuperTypesCacheTest {

	public static void main(String[] args) {
		var classType = ImmediateResultListenableFuture.class;
		List<Supplier<List<Class<?>>>> suppliers = List.of(() -> {
			return CoreReflections.streamTypes(classType).collect(Collectors.toList());
		}, () -> {
			return JavaCode.Reflections.streamSuperTypes(classType, true).collect(Collectors.toList());
		});
		for (int i = 0; i < suppliers.size(); i++) {
			var supplier = suppliers.get(i);
			System.out.println(String.format("%s - %s", i, supplier.get().size()));
			Double average = LongStream.range(0, 250_000).map(v -> {
				var startedAt = System.nanoTime();
				supplier.get();
				var elapsed = System.nanoTime() - startedAt;
				return elapsed;
			}).average().getAsDouble();
			System.out.println(Durations.toMillis(Duration.ofNanos(average.longValue())));
		}

	}
}
