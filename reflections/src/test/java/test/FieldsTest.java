package test;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.Duration;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import org.reflections8.ReflectionUtils;
import org.threadly.concurrent.future.ImmediateResultListenableFuture;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.Durations;
import com.lfp.joe.reflections.JavaCode;

public class FieldsTest {

	public static void main(String[] args) {

		var classType = ImmediateResultListenableFuture.class;
		List<Supplier<List<Field>>> suppliers = List.of(() -> {
			return CoreReflections.streamFields(classType, true).collect(Collectors.toList());
		}, () -> {
			return JavaCode.Reflections.streamFields(classType, true).distinct().collect(Collectors.toList());
		}, () -> {
			return ReflectionUtils.getAllFields(classType).stream().collect(Collectors.toList());
		});
		var indexes = IntStream.range(0, suppliers.size()).boxed().collect(Collectors.toList());
		for (int pass = 0; pass < 10; pass++) {
			for (var index : indexes) {
				var supplier = suppliers.get(index);
				var size = supplier.get().size();
				Double average = LongStream.range(0, 50_000).map(v -> {
					var startedAt = System.nanoTime();
					supplier.get();
					var elapsed = System.nanoTime() - startedAt;
					return elapsed;
				}).average().getAsDouble();
				var averageMillis = Durations.toMillis(Duration.ofNanos(average.longValue()));
				System.out.println(String.format("pass:%s index:%s size:%s averageMillis:%s", pass, index, size,
						new BigDecimal(averageMillis.toString()).stripTrailingZeros().toPlainString()));
			}
		}

	}
}
