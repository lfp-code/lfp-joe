package test;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.time.Duration;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import org.reflections8.ReflectionUtils;
import org.threadly.concurrent.future.ImmediateResultListenableFuture;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.MethodsRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.function.Durations;
import com.lfp.joe.reflections.JavaCode;

public class MethodTest {

	public static void main(String[] args) {
		int passes = 3;
		int runs = 10_000;
		boolean print = false;
		var classType = ImmediateResultListenableFuture.class;
		List<Supplier<List<Method>>> suppliers = List.of(() -> {
			return MemberCache.getMethods(MethodsRequest.of(classType));
		}, () -> {
			return CoreReflections.streamMethods(classType, true).collect(Collectors.toList());
		}, () -> {
			return JavaCode.Reflections.streamMethods(classType, true).collect(Collectors.toList());
		}, () -> {
			return ReflectionUtils.getAllMethods(classType).stream().collect(Collectors.toList());
		});
		var indexes = IntStream.range(0, suppliers.size()).boxed().collect(Collectors.toList());
		for (int pass = 0; pass < passes; pass++) {
			if (pass + 1 >= passes)
				System.gc();
			for (var index : indexes) {
				var supplier = suppliers.get(index);
				var size = supplier.get().size();
				Double average = LongStream.range(0, runs).map(v -> {
					var startedAt = System.nanoTime();
					var list = supplier.get();
					if (print)
						System.out.println(list);
					var elapsed = System.nanoTime() - startedAt;
					return elapsed;
				}).average().getAsDouble();
				var averageMillis = Durations.toMillis(Duration.ofNanos(average.longValue()));
				System.out.println(String.format("pass:%s index:%s size:%s averageMillis:%s", pass, index, size,
						new BigDecimal(averageMillis.toString()).stripTrailingZeros().toPlainString()));
			}
		}

	}
}
